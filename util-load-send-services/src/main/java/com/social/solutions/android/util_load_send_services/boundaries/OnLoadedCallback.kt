package com.social.solutions.android.util_load_send_services.boundaries

interface OnLoadedCallback {
    fun onCancel(filePath: String)
    fun onLoaded(filePath: String, fileLoadedId: String)
    fun onLoaded(filePath: String, fileLoadedId: String, chatType: Int)
}
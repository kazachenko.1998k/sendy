package com.social.solutions.android.util_load_send_services.voice.send.receivers

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import com.social.solutions.android.util_load_send_services.voice.send.services.SendVoiceService

class SendVoiceReceiver(private val mCallback: SendVoiceCallback): BroadcastReceiver() {
    override fun onReceive(context: Context?, intent: Intent?) {
        intent?:return
        if (intent.action == SendVoiceService.BROADCAST_ACTION_VOICE) {
            val status = intent.getIntExtra(SendVoiceService.EXTENDED_DATA_VOICE_STATUS, -1)
            val voicePath = intent.getStringExtra(SendVoiceService.VOICE_PATH) ?: return

            if (status != -1 ) {
                if (status == 100)
                    mCallback.onLoaded(voicePath)
                else
                    mCallback.onStatusUpdate(voicePath, status)
            }
        }
    }
}
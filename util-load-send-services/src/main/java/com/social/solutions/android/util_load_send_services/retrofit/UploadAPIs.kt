package com.social.solutions.android.util_load_send_services.retrofit

import io.reactivex.Observable
import okhttp3.MultipartBody
import okhttp3.ResponseBody
import retrofit2.http.Header
import retrofit2.http.Multipart
import retrofit2.http.POST
import retrofit2.http.Part

interface UploadAPIs {
    @Multipart
    @POST("api/v1.0/uploadFile/")
    fun uploadFile(
        @Part file: MultipartBody.Part,
        @Header("X-Content-Id") contentId: String,
        @Header("X-User-Id") userId: Long
        //@Header("Content-Type") contentType: String = "multipart/form-data"
    ): Observable<ResponseBody>
}
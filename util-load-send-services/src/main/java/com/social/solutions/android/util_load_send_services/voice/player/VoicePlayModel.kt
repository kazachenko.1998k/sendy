package com.social.solutions.android.util_load_send_services.voice.player

import java.io.Serializable

data class VoicePlayModel(val userName: String, val time: Int, val fileName: String): Serializable
package com.social.solutions.android.util_load_send_services.media.load.services

import android.content.Intent
import androidx.core.app.JobIntentService
import androidx.localbroadcastmanager.content.LocalBroadcastManager

class LoadMediaService: JobIntentService() {
    companion object{
        const val LOAD_VIDEO_JOB_ID = 1001

        const val VIDEO_URL = "video_url"

        // Report work status
        const val BROADCAST_ACTION_VIDEO = "com.morozov.android.sendy.LOAD.VIDEO"
        const val EXTENDED_DATA_VIDEO_STATUS = "com.morozov.android.sendy.LOAD.VIDEO.STATUS"
    }

    override fun onHandleWork(intent: Intent) {
        val videoUrl = intent.getStringExtra(VIDEO_URL)

        when {
            videoUrl != null -> {
                // Load video into cache
            }
        }
    }

    private fun sendVideoStatusBroadcast(status: Int, videoUrl: String) {
        val localIntent = Intent(BROADCAST_ACTION_VIDEO).apply {
            // Puts the status into the Intent
            putExtra(EXTENDED_DATA_VIDEO_STATUS, status)
            putExtra(VIDEO_URL, videoUrl)
        }
        LocalBroadcastManager.getInstance(applicationContext).sendBroadcast(localIntent)
    }
}
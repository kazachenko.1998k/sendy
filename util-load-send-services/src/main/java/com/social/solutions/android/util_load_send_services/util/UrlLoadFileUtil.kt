package com.social.solutions.android.util_load_send_services.util

import android.net.Uri
import java.net.URL
import kotlin.random.Random

fun String.addSignUrl(sign: String, rid: Int = Random.nextInt(0, Int.MAX_VALUE),
                      time: Long = System.currentTimeMillis()): URL {

    val uri = Uri.parse(this).buildUpon()
        .appendQueryParameter("sign", sign)
        .appendQueryParameter("rid", rid.toString())
        .appendQueryParameter("time", time.toString())
        .build()
    return URL(uri.toString())
}
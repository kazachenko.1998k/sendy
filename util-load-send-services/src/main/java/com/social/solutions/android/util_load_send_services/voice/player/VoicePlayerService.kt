package com.social.solutions.android.util_load_send_services.voice.player

import android.content.Intent
import android.media.MediaPlayer
import android.os.Binder
import android.os.Handler
import android.os.IBinder
import android.util.Log
import androidx.core.app.JobIntentService
import androidx.core.os.HandlerCompat.postDelayed
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import java.io.IOException

class VoicePlayerService: JobIntentService() {
    companion object{
        var mIsVoicePlaying = MutableLiveData<Boolean>()
        var mIsVoicePause = MutableLiveData<Boolean>()

        const val VOICE_MODEL = "voice_model"

        const val PLAYING_FINISHED = -1
        const val START_TIMER_VALUE = 0
        private const val TIME_STEP = 100
    }

    init {
        mIsVoicePlaying.value = false
        mIsVoicePause.value = false
    }

    private val mBinder = VoicePlayerBinder()

    private var mVoicePlayModel: VoicePlayModel? = null
    private val mVoiceProgress = MutableLiveData<Int>()
    private var timeValue = START_TIMER_VALUE

    private var lastPlayedMediaPosition = 0

    private var isRunned = false

    private val updateTimeRunnable: Runnable by lazy {
        Runnable {
            isRunned = true
            if(mIsVoicePlaying.value != true) {
                isRunned = false
                return@Runnable
            }
            mVoiceProgress.postValue(timeValue)
            timeValue += TIME_STEP
            postDelayed(Handler(), updateTimeRunnable, Any(), TIME_STEP.toLong())
        }
    }

    private var player: MediaPlayer? = null

    override fun onHandleWork(intent: Intent) {
        val serializableExtra = intent.getSerializableExtra(VOICE_MODEL)
        mVoicePlayModel = if (serializableExtra is VoicePlayModel) serializableExtra else return
        startPlaying(mVoicePlayModel!!.fileName)
    }

    fun startVoice(model: VoicePlayModel) {
        if (player != null) {
            continueVoice()
            return
        }
        onHandleWork(Intent().apply {
            putExtra(VOICE_MODEL, model)
        })
    }
    
    fun pauseVoice() {
        mIsVoicePlaying.value = false
        mIsVoicePause.value = true
        player?.pause()
        lastPlayedMediaPosition = player?.currentPosition ?: return
    }

    fun continueVoice(startTime: Int? = null) {
        if (startTime != null) {
            timeValue = startTime
            player?.seekTo(startTime)
        }
        else
            player?.seekTo(lastPlayedMediaPosition)
        mIsVoicePlaying.value = true
        mIsVoicePause.value = false
        updateTimeRunnable.run()
        player?.start()
    }
    
    fun stopVoice() {
        stopPlaying()
    }

    private fun startPlaying(fileName: String) {
        player = MediaPlayer().apply {
            try {
                setDataSource(fileName)
                setOnPreparedListener {
                    mIsVoicePlaying.value = true
                    mIsVoicePause.value = false
                    if (isRunned.not())
                        updateTimeRunnable.run()
                }
                setOnCompletionListener {
                    emmitVoiceEnd()
                }
                prepare()
                start()
            } catch (e: IOException) {
                e.printStackTrace()
            }
        }
    }

    private fun emmitVoiceEnd() {
        mVoiceProgress.value = PLAYING_FINISHED
        mIsVoicePlaying.value = false
        mIsVoicePause.value = false
        lastPlayedMediaPosition = 0
        timeValue = START_TIMER_VALUE
        player?.release()
        player = null
    }

    private fun stopPlaying() {
        emmitVoiceEnd()
    }

    override fun onBind(intent: Intent): IBinder? {
        return mBinder
    }

    inner class VoicePlayerBinder: Binder() {
        fun getService(): VoicePlayerService {
            return this@VoicePlayerService
        }

        fun getVoiceModel(): VoicePlayModel? {
            return mVoicePlayModel
        }

        fun getVoiceProgress(): LiveData<Int> {
            return this@VoicePlayerService.mVoiceProgress
        }
    }

    override fun onDestroy() {
        stopPlaying()
        super.onDestroy()
    }
}
package com.social.solutions.android.util_load_send_services.media.send.receivers.video

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import com.social.solutions.android.util_load_send_services.media.load.receivers.video.LoadVideoCallback
import com.social.solutions.android.util_load_send_services.media.send.services.SendMediaService
import com.social.solutions.android.util_load_send_services.voice.send.services.SendVoiceService

class SendVideoReceiver(private val mCallback: SendVideoCallback): BroadcastReceiver() {
    override fun onReceive(context: Context?, intent: Intent?) {
        intent?:return
        if (intent.action == SendMediaService.BROADCAST_ACTION_VIDEO) {
            val status = intent.getIntExtra(SendMediaService.EXTENDED_DATA_VIDEO_STATUS, -1)
            val videoPath = intent.getStringExtra(SendMediaService.VIDEO_PATH) ?: return

            if (status != -1 ) {
                if (status == 100)
                    mCallback.onLoaded(videoPath)
                else
                    mCallback.onStatusUpdate(videoPath, status)
            }
        }
    }
}
package com.social.solutions.android.util_load_send_services.media.load.receivers.video

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.graphics.Bitmap
import com.social.solutions.android.util_load_send_services.media.load.services.LoadMediaService
import com.social.solutions.android.util_load_send_services.voice.send.services.SendVoiceService

class LoadVideoReceiver(private val mCallback: LoadVideoCallback): BroadcastReceiver() {
    override fun onReceive(context: Context?, intent: Intent?) {
        intent?:return
        if (intent.action == LoadMediaService.BROADCAST_ACTION_VIDEO) {
            val status = intent.getIntExtra(LoadMediaService.EXTENDED_DATA_VIDEO_STATUS, -1)
            val videoUrl = intent.getStringExtra(LoadMediaService.VIDEO_URL) ?: return

            if (status != -1 ) {
                if (status == 100)
                    mCallback.onLoaded(videoUrl)
                else
                    mCallback.onStatusUpdate(videoUrl, status)
            }
        }
    }
}
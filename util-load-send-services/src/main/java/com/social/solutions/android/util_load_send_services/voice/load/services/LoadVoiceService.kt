package com.social.solutions.android.util_load_send_services.voice.load.services

import android.content.Intent
import androidx.core.app.JobIntentService
import androidx.localbroadcastmanager.content.LocalBroadcastManager

class LoadVoiceService: JobIntentService() {
    companion object{
        const val LOAD_VOICE_JOB_ID = 1006
        
        const val VOICE_URL = "voice_url"
        const val VOICE_NEW_PATH = "voice_new_path"

        // Report work status
        const val BROADCAST_ACTION_VOICE = "com.morozov.android.sendy.LOAD.VOICE"
        const val EXTENDED_DATA_VOICE_STATUS = "com.morozov.android.sendy.LOAD.VOICE.STATUS"
    }

    override fun onHandleWork(intent: Intent) {
        val voiceUrl = intent.getStringExtra(VOICE_URL)

        if (voiceUrl != null) {
            // TODO: Load image from the server
            var i = 0
            var status = 0
            while (i < 10) {
                Thread.sleep(300)
                i++
                status+=10
                sendVoiceStatusBroadcast(status, voiceUrl, "mFile.mp3")
            }
        }
    }

    private fun sendVoiceStatusBroadcast(status: Int, voiceUrl: String, filePath: String) {
        val localIntent = Intent(BROADCAST_ACTION_VOICE).apply {
            // Puts the status into the Intent
            putExtra(EXTENDED_DATA_VOICE_STATUS, status)
            putExtra(VOICE_URL, voiceUrl)
            putExtra(VOICE_NEW_PATH, filePath)
        }
        LocalBroadcastManager.getInstance(applicationContext).sendBroadcast(localIntent)
    }
}
package com.social.solutions.android.util_load_send_services.media.send.receivers.video

interface SendVideoCallback {
    fun onStatusUpdate(videoPath: String, progress: Int)
    fun onLoaded(videoPath: String)
}
package com.social.solutions.android.util_load_send_services.voice.send.services

import android.content.Intent
import android.util.Log
import android.widget.Toast
import androidx.core.app.JobIntentService
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import com.social.solutions.android.util_load_send_services.boundaries.BoundariesObject
import com.social.solutions.android.util_load_send_services.retrofit.FileRetrofit
import com.social.solutions.android.util_load_send_services.retrofit.UploadAPIs
import com.social.solutions.android.util_load_send_services.retrofit.progress.ProgressRequestBody
import okhttp3.MediaType
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.MultipartBody
import java.io.File
import java.lang.IllegalArgumentException

class SendVoiceService: JobIntentService() {
    companion object{
        const val SEND_VOICE_JOB_ID = 1005
        
        const val VOICE_PATH = "voice_path"

        // Send to the server
        const val FILE_ID = "com.morozov.android.sendy.FILE.ID"
        const val USER_ID = "com.morozov.android.sendy.USER.ID"
        const val BASE_URL = "com.morozov.android.sendy.BASE.URL"
        const val CHAT_TYPE = "com.morozov.android.sendy.CHAT.TYPE"

        // Report work status
        const val BROADCAST_ACTION_VOICE = "com.morozov.android.sendy.SEND.VOICE"
        const val EXTENDED_DATA_VOICE_STATUS = "com.morozov.android.sendy.SEND.VOICE.STATUS"
    }

    override fun onHandleWork(intent: Intent) {
        val voicePath = intent.getStringExtra(VOICE_PATH)

        val chatType = intent.getIntExtra(CHAT_TYPE, -1)
        if (chatType == -1)
            throw IllegalArgumentException("Chat type is required")

        val fileId = intent.getStringExtra(FILE_ID) ?: return
        val userId = intent.getLongExtra(USER_ID, 0L)
        val baseUrl = intent.getStringExtra(BASE_URL) ?: return

        if (voicePath != null) {
            val type = "audio/mp3".toMediaTypeOrNull() ?: throw IllegalArgumentException("Wrong media type")
            uploadMediaToServerWithProgress(voicePath, fileId, userId,
                baseUrl, type, chatType)
        }
    }

    private fun uploadMediaToServerWithProgress(mediaPath: String, fileId: String,
                                                userId: Long, baseUrl: String,
                                                mediaType: MediaType, chatType: Int) {
        val uploadApi = FileRetrofit.getRetrofit(baseUrl).create(UploadAPIs::class.java)
        val file = File(mediaPath)
        val fileReqBody = object : ProgressRequestBody(file, ProgressCallback(mediaPath, fileId, chatType)) {
            override fun contentType(): MediaType? {
                return mediaType
            }
        }
        val part = MultipartBody.Part.createFormData("upload", file.name, fileReqBody)
        uploadApi.uploadFile(part, fileId, userId)
            .subscribe({
                Log.i("Sendy", it.toString())
            },{
                it.printStackTrace()
            })
    }

    private fun sendVoiceStatusBroadcast(status: Int, voiceUrl: String) {
        val localIntent = Intent(BROADCAST_ACTION_VOICE).apply {
            // Puts the status into the Intent
            putExtra(EXTENDED_DATA_VOICE_STATUS, status)
            putExtra(VOICE_PATH, voiceUrl)
        }
        LocalBroadcastManager.getInstance(applicationContext).sendBroadcast(localIntent)
    }

    private inner class ProgressCallback(private val filePath: String,
                                         private val fileLoadedId: String,
                                         private val chatType: Int): ProgressRequestBody.UploadCallbacks {
        override fun onProgressUpdate(percentage: Int) {
            sendVoiceStatusBroadcast(percentage, filePath)
        }

        override fun onError() {

        }

        override fun onFinish() {
            BoundariesObject.mLoadedCallback?.onLoaded(filePath, fileLoadedId, chatType)
            sendVoiceStatusBroadcast(100, filePath)
        }

        override fun uploadStart() {

        }
    }
}
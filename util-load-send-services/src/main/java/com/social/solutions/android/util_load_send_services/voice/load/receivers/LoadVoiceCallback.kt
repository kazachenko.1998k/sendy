package com.social.solutions.android.util_load_send_services.voice.load.receivers

interface LoadVoiceCallback {
    fun onStatusUpdate(voiceUrl: String, filePath: String, progress: Int)
    fun onLoaded(voiceUrl: String, filePath: String)
}
package com.social.solutions.android.util_load_send_services.media.send.receivers.photo

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import com.social.solutions.android.util_load_send_services.media.send.services.SendMediaService
import com.social.solutions.android.util_load_send_services.voice.send.receivers.SendVoiceCallback
import com.social.solutions.android.util_load_send_services.voice.send.services.SendVoiceService

class SendPhotoReceiver(private val mCallback: SendPhotoCallback): BroadcastReceiver() {
    override fun onReceive(context: Context?, intent: Intent?) {
        intent?:return
        if (intent.action == SendMediaService.BROADCAST_ACTION_PHOTO) {
            val status = intent.getIntExtra(SendMediaService.EXTENDED_DATA_PHOTO_STATUS, -1)
            val photoPath = intent.getStringExtra(SendMediaService.PHOTO_PATH) ?: return

            if (status != -1 ) {
                if (status == 100)
                    mCallback.onLoaded(photoPath)
                else
                    mCallback.onStatusUpdate(photoPath, status)
            }
        }
    }
}
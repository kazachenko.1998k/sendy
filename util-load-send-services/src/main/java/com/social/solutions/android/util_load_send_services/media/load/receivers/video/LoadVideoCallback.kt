package com.social.solutions.android.util_load_send_services.media.load.receivers.video

interface LoadVideoCallback {
    fun onStatusUpdate(videoUrl: String, progress: Int)
    fun onLoaded(videoUrl: String)
}
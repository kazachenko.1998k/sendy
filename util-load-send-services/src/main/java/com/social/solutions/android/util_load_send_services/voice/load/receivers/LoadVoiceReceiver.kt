package com.social.solutions.android.util_load_send_services.voice.load.receivers

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import com.social.solutions.android.util_load_send_services.voice.load.services.LoadVoiceService
import com.social.solutions.android.util_load_send_services.voice.send.services.SendVoiceService
import com.social.solutions.android.util_load_send_services.voice.send.receivers.SendVoiceCallback

class LoadVoiceReceiver(private val mCallback: LoadVoiceCallback): BroadcastReceiver() {
    override fun onReceive(context: Context?, intent: Intent?) {
        intent?:return
        if (intent.action == LoadVoiceService.BROADCAST_ACTION_VOICE) {
            val status = intent.getIntExtra(LoadVoiceService.EXTENDED_DATA_VOICE_STATUS, -1)
            val voiceUrl = intent.getStringExtra(LoadVoiceService.VOICE_URL) ?: return
            val filePath = intent.getStringExtra(LoadVoiceService.VOICE_NEW_PATH) ?: return

            if (status != -1 ) {
                if (status == 100)
                    mCallback.onLoaded(voiceUrl, filePath)
                else
                    mCallback.onStatusUpdate(voiceUrl, filePath, status)
            }
        }
    }
}
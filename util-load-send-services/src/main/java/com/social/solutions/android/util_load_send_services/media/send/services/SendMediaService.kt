package com.social.solutions.android.util_load_send_services.media.send.services

import android.content.Intent
import android.util.Log
import android.webkit.MimeTypeMap
import androidx.core.app.JobIntentService
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import com.social.solutions.android.util_load_send_services.boundaries.BoundariesObject
import com.social.solutions.android.util_load_send_services.retrofit.FileRetrofit
import com.social.solutions.android.util_load_send_services.retrofit.UploadAPIs
import com.social.solutions.android.util_load_send_services.retrofit.progress.ProgressRequestBody
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import okhttp3.MediaType
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.MultipartBody
import java.io.File


class SendMediaService : JobIntentService() {
    companion object {
        const val SEND_PHOTO_JOB_ID = 1004
        const val SEND_VIDEO_JOB_ID = 1004

        const val PHOTO_PATH = "photo_path"
        const val FILE_PATH = "file_path"
        const val VIDEO_PATH = "video_path"

        // Send to the server
        const val FILE_ID = "com.morozov.android.sendy.FILE.ID"
        const val USER_ID = "com.morozov.android.sendy.USER.ID"
        const val BASE_URL = "com.morozov.android.sendy.BASE.URL"
        const val CHAT_TYPE = "com.morozov.android.sendy.CHAT.TYPE"
        private const val notChat = -1

        // Report work status
        const val BROADCAST_ACTION_PHOTO = "com.morozov.android.sendy.SEND.PHOTO"
        const val BROADCAST_ACTION_VIDEO = "com.morozov.android.sendy.SEND.VIDEO"

        const val EXTENDED_DATA_PHOTO_STATUS = "com.morozov.android.sendy.SEND.PHOTO.STATUS"
        const val EXTENDED_DATA_VIDEO_STATUS = "com.morozov.android.sendy.SEND.VIDEO.STATUS"

        // String -- file path
        private val loadingRequests = mutableMapOf<String, Disposable>()

        fun cancelLoadingIfNotLoaded(filePath: String) {
            val disposable = loadingRequests[filePath] ?: return
            if (disposable.isDisposed.not())
                disposable.dispose()
            BoundariesObject.mLoadedCallback?.onCancel(filePath)
            loadingRequests.remove(filePath)
        }
    }

    override fun onHandleWork(intent: Intent) {
        val photoPath = intent.getStringExtra(PHOTO_PATH)
        val videoPath = intent.getStringExtra(VIDEO_PATH)
        val filePath = intent.getStringExtra(FILE_PATH)

        val chatType = intent.getIntExtra(CHAT_TYPE, -1)

        val fileId = intent.getStringExtra(FILE_ID) ?: return
        val userId = intent.getLongExtra(USER_ID, 0L)
        val baseUrl = intent.getStringExtra(BASE_URL) ?: return

        val filePathAndType: Pair<String, MediaType> = when {
            photoPath != null -> {
                val type = "image/mpeg".toMediaTypeOrNull()
                    ?: throw IllegalArgumentException("Wrong media type")
                Pair(photoPath, type)
            }
            videoPath != null -> {
                val type = "video/mp4".toMediaTypeOrNull()
                    ?: throw IllegalArgumentException("Wrong media type")
                Pair(videoPath, type)
            }
            filePath != null -> {
                val fileType = getMimeType(filePath) ?: return
                uploadMediaToServerWithProgress(
                    filePath, fileId, userId,
                    baseUrl, fileType.toMediaTypeOrNull()!!, chatType
                )
                return
            }
            else -> throw IllegalArgumentException("No PHOTO_PATH or VIDEO_PATH")
        }
        uploadMediaToServerWithProgress(
            filePathAndType.first, fileId, userId,
            baseUrl, filePathAndType.second, chatType
        )
    }

    private fun getMimeType(url: String?): String? {
        var type: String? = null
        val extension = MimeTypeMap.getFileExtensionFromUrl(url)
        if (extension != null) {
            type = MimeTypeMap.getSingleton().getMimeTypeFromExtension(extension)
        }
        return type
    }


    private fun uploadMediaToServerWithProgress(
        mediaPath: String, fileId: String,
        userId: Long, baseUrl: String,
        mediaType: MediaType, chatType: Int
    ) {
        val uploadApi = FileRetrofit.getRetrofit(baseUrl).create(UploadAPIs::class.java)
        val file = File(mediaPath)
        val fileReqBody =
            object : ProgressRequestBody(file, ProgressCallback(mediaPath, fileId, chatType)) {
                override fun contentType(): MediaType? {
                    return mediaType
                }
            }
        val part = MultipartBody.Part.createFormData("upload", file.name, fileReqBody)
        loadingRequests[mediaPath] = uploadApi.uploadFile(part, fileId, userId)
            .subscribeOn(Schedulers.io())
            .subscribe({
                Log.i("Sendy", it.toString())
            }, {
                it.printStackTrace()
            })
    }

    private fun sendPhotoStatusBroadcast(status: Int, photoPath: String) {
        val localIntent = Intent(BROADCAST_ACTION_PHOTO).apply {
            // Puts the status into the Intent
            putExtra(EXTENDED_DATA_PHOTO_STATUS, status)
            putExtra(PHOTO_PATH, photoPath)
        }
        LocalBroadcastManager.getInstance(applicationContext).sendBroadcast(localIntent)
    }

    private fun sendVideoStatusBroadcast(status: Int, videoPath: String) {
        val localIntent = Intent(BROADCAST_ACTION_VIDEO).apply {
            // Puts the status into the Intent
            putExtra(EXTENDED_DATA_VIDEO_STATUS, status)
            putExtra(VIDEO_PATH, videoPath)
        }
        LocalBroadcastManager.getInstance(applicationContext).sendBroadcast(localIntent)
    }

    private inner class ProgressCallback(
        private val filePath: String,
        private val fileLoadedId: String,
        private val chatType: Int
    ) : ProgressRequestBody.UploadCallbacks {
        override fun onProgressUpdate(percentage: Int) {
            sendPhotoStatusBroadcast(percentage, filePath)
        }

        override fun onError() {

        }

        override fun onFinish() {
            loadingRequests.remove(filePath)
            if (chatType == notChat)
                BoundariesObject.mLoadedCallback?.onLoaded(filePath, fileLoadedId)
            else
                BoundariesObject.mLoadedCallback?.onLoaded(filePath, fileLoadedId, chatType)
            sendPhotoStatusBroadcast(100, filePath)
        }

        override fun uploadStart() {

        }
    }
}

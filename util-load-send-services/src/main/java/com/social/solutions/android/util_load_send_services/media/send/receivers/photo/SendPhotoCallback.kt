package com.social.solutions.android.util_load_send_services.media.send.receivers.photo

interface SendPhotoCallback {
    fun onStatusUpdate(photoPath: String, progress: Int)
    fun onLoaded(photoPath: String)
}
package com.social.solutions.android.util_load_send_services.voice.send.receivers

interface SendVoiceCallback {
    fun onStatusUpdate(voicePath: String, progress: Int)
    fun onLoaded(voicePath: String)
}
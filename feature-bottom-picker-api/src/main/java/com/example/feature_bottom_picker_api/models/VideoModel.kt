package com.example.feature_bottom_picker_api.models

data class VideoModel(val filePath: String, val seconds: Int): MediaModel
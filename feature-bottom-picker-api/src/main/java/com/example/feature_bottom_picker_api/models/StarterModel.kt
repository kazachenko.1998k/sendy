package com.example.feature_bottom_picker_api.models

class StarterModel {
    var addComment: Boolean = true
    var isSinglePick = true
    var showMakeMediaCamera = true
    var showPhoto = true
    var showVideo = true
    var showDocuments = true
    var fullScreen = true
    var openWithAnimation = true
}
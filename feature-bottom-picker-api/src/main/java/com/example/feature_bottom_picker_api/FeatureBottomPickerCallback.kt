package com.example.feature_bottom_picker_api

import android.net.Uri
import com.example.feature_bottom_picker_api.models.MediaModel

interface FeatureBottomPickerCallback {
    fun onSelected(media: List<MediaModel>)
    fun onSelected(media: List<MediaModel>, comment: String)
    fun onExit()
    fun onFileSelected(uri: Uri)
}
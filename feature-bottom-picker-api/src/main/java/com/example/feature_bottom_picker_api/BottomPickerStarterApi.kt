package com.example.feature_bottom_picker_api

import androidx.fragment.app.FragmentManager
import com.example.feature_bottom_picker_api.models.StarterModel

interface BottomPickerStarterApi {
    fun start(
        manager: FragmentManager,
        container: Int,
        starterModel: StarterModel,
        callback: FeatureBottomPickerCallback
    )
}
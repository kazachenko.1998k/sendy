package com.example.feature_list_messages_impl.adapters

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.feature_list_messages_impl.R
import com.example.feature_list_messages_impl.adapters.AdapterWithNotInternetAndLoad.BaseViewHolder
import com.example.feature_list_messages_impl.interfaces.IListMessage
import com.social.solution.feature_list_messages_api.models.AbstractItemInListMessageUI
import com.social.solution.feature_list_messages_api.models.ItemInListMessageLoadData
import com.social.solution.feature_list_messages_api.models.ItemInListMessageNotInternet

abstract class AdapterWithNotInternetAndLoad(
    open var data: MutableList<AbstractItemInListMessageUI>
) :
    RecyclerView.Adapter<BaseViewHolder>(), IListMessage {
    var isLoading = false
    var recyclerView: RecyclerView? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BaseViewHolder {
        return when (viewType) {
            -1 -> {
                val view = LayoutInflater.from(parent.context)
                    .inflate(R.layout.item_recycler_not_internet, parent, false)
                NotInternetViewHolder(view)
            }
            else -> {
                val view = LayoutInflater.from(parent.context)
                    .inflate(R.layout.item_recycler_load_data, parent, false)
                LoadDataViewHolder(view)
            }
        }
    }

    override fun onAttachedToRecyclerView(recyclerView: RecyclerView) {
        super.onAttachedToRecyclerView(recyclerView)
        this.recyclerView = recyclerView
        recyclerView.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                super.onScrolled(recyclerView, dx, dy)
                if (!recyclerView.canScrollVertically(1)) { //1 for down
                    loadMoreData()
                }
            }
        })
    }

    @SuppressLint("SetTextI18n")
    override fun onBindViewHolder(holder: BaseViewHolder, position: Int) {
        holder.bind(position)
    }


    override fun getItemCount(): Int {
        return data.size
    }

    override fun getItemViewType(position: Int): Int {
        return when (data[position]) {
            is ItemInListMessageNotInternet -> -1
            else -> -2
        }
    }

    abstract class BaseViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        abstract fun bind(position: Int)
    }

    inner class NotInternetViewHolder(itemView: View) : BaseViewHolder(itemView) {
        override fun bind(position: Int) {
        }
    }

    inner class LoadDataViewHolder(itemView: View) : BaseViewHolder(itemView) {
        override fun bind(position: Int) {
        }
    }

    fun disconnectInternet() {
        if (data.find { it is ItemInListMessageNotInternet } == null) {
            data.add(0, ItemInListMessageNotInternet())
            notifyItemInserted(0)
            recyclerView?.post { recyclerView?.smoothScrollToPosition(0) }
        }
    }

    fun connectInternet() {
        if (data.find { it is ItemInListMessageNotInternet } != null) {
            data.removeAt(0)
            notifyItemRemoved(0)
        }
    }

    override fun loadMoreData() {
        isLoading = true
        if (data.find { it is ItemInListMessageLoadData } == null) {
            recyclerView?.post {
                data.add(ItemInListMessageLoadData())
                val lastPosition = data.lastIndex
                notifyItemInserted(lastPosition)
            }
        }
    }

    fun addNewDataFromServer(listMessages: MutableList<AbstractItemInListMessageUI>?) {
        if (recyclerView == null) return
        if (data.find { it is ItemInListMessageLoadData } != null) {
            recyclerView!!.post {
                val itemToRemove = data.lastIndex
                data.removeAt(itemToRemove)
                notifyItemRemoved(itemToRemove)
            }
            isLoading = false
        }
        if (listMessages.isNullOrEmpty()) {
            return
        }
        recyclerView!!.post {
            val beforeCount = data.size
            val afterCount = listMessages.size
            val list = mutableListOf<AbstractItemInListMessageUI>()
            list.addAll(listMessages)
            data.addAll(list)
            notifyItemRangeInserted(
                beforeCount + 1,
                beforeCount + afterCount
            )
        }
    }
}

package com.example.feature_list_messages_impl.adapters

import android.annotation.SuppressLint
import android.app.Dialog
import android.content.Context
import android.graphics.Typeface
import android.text.Spannable
import android.text.SpannableString
import android.text.style.ForegroundColorSpan
import android.text.style.StyleSpan
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import androidx.core.content.res.ResourcesCompat
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.load.resource.bitmap.BitmapTransitionOptions.withCrossFade
import com.bumptech.glide.request.RequestOptions
import com.example.feature_list_messages_impl.R
import com.example.feature_list_messages_impl.interfaces.IListMessage
import com.example.feature_list_messages_impl.interfaces.ISearch
import com.morozov.core_backend_api.SignData
import com.morozov.core_backend_api.chat.Chat.Companion.CHANNEL
import com.morozov.core_backend_api.chat.Chat.Companion.GROUP_CHAT
import com.morozov.core_backend_api.chat.Chat.Companion.PERSONAL_DIALOG
import com.morozov.core_backend_api.message.Message
import com.social.solution.feature_list_messages_api.models.AbstractItemInListMessageUI
import com.social.solution.feature_list_messages_api.models.ItemInListGroupName
import com.social.solution.feature_list_messages_api.models.ItemInListSearchUI
import kotlinx.android.synthetic.main.dialog_select_chat.*
import kotlinx.android.synthetic.main.item_recycler_search.view.*
import kotlinx.android.synthetic.main.item_recycler_search_group_stick.view.*


abstract class AdapterSearch(
    override var data: MutableList<AbstractItemInListMessageUI>,
    val message: Message?
) :
    AdapterWithNotInternetAndLoad(data), ISearch, IListMessage {

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): AdapterWithNotInternetAndLoad.BaseViewHolder {
        return when (viewType) {
            1 -> {
                val view = LayoutInflater.from(parent.context)
                    .inflate(R.layout.item_recycler_search_group_stick, parent, false)
                GroupMessageViewHolder(view)
            }
            0 -> {
                val view = LayoutInflater.from(parent.context)
                    .inflate(R.layout.item_recycler_search, parent, false)
                BaseViewHolder(view)
            }
            else -> super.onCreateViewHolder(parent, viewType)
        }
    }


    override fun getItemViewType(position: Int): Int {
        return when (data[position]) {
            is ItemInListSearchUI -> 0
            is ItemInListGroupName -> 1
            else -> super.getItemViewType(position)
        }
    }

    inner class BaseViewHolder(itemView: View) :
        AdapterWithNotInternetAndLoad.BaseViewHolder(itemView) {
        @SuppressLint("SetTextI18n")
        override fun bind(position: Int) {
            val dataItem = data[position] as ItemInListSearchUI
            if (dataItem.userMini == null) {
                clearItem()

                return
            }

            itemView.apply {
                val userMini = dataItem.userMini!!
                val requestOptions = RequestOptions().circleCrop().diskCacheStrategy(
                    DiskCacheStrategy.ALL
                )
                Glide
                    .with(itemView)
                    .asBitmap()
                    .placeholder(R.drawable.tmp_profile)
                    .transition(withCrossFade())
                    .load(SignData.server_file + userMini.avatarFileId)
                    .apply(requestOptions)
                    .into(profile_image_image_view)

                name_user_text_view.text =
                    "${userMini.firstName} ${userMini.secondName ?: ""}"
                updateTextMessage(position)
                setOnClickListener {
                    if (message != null) {
                        showDialog(
                            it.context,
                            "Отправить сообщение",
                            "Хотите отправить это сообщение ",
                            dataItem.userMini!!.firstName.toString(),
                            "Отправить",
                            dataItem
                        )
                    } else {
                        onChatClickListener(dataItem.chat)
                    }
                }
            }
        }

        private fun clearItem() {
            itemView.apply {
                profile_image_image_view.setImageDrawable(null)
                name_user_text_view.text = ""
                message_text_view.text = ""
                setOnClickListener {}
            }
        }


        @SuppressLint("SetTextI18n")
        private fun updateTextMessage(position: Int) {
            val dataItem = data[position] as ItemInListSearchUI
            val textView = itemView.message_text_view
//            when (dataItem.chat.) {
//                PERSONAL_DIALOG -> {
//                    if (dataItem.userMini.lastActive) {
//                        textView.text = ""
//                        addSelectedText(itemView.resources.getString(R.string.is_online))
//                    } else {
//                        textView.text =
//                            "${itemView.resources.getString(if (dataItem.user.male == true) R.string.he_was_online else R.string.she_was_online)} " +
//                                    dataItem.user.lastTimeOnline.convertLongToCurrentType(itemView.context)
//                    }
//                }
//                CHANNEL -> {
//                    textView.text =
//                        dataItem.user.followers.convertToShortString() + " ${itemView.resources.getString(
//                            R.string.users
//                        )}"
//                }
//                GROUP_CHAT -> {
//                    textView.text =
//                        dataItem.user.followers.convertToShortString() + " ${itemView.resources.getString(
//                            R.string.followers
//                        )}"
//                }
//            }

        }

        private fun addSelectedText(
            standardText: String,
            color: Int = R.color.colorAccent
        ) {
            val wordTwo: Spannable =
                SpannableString(standardText)
            wordTwo.setSpan(
                ForegroundColorSpan(itemView.context.resources.getColor(color)),
                0,
                wordTwo.length,
                Spannable.SPAN_EXCLUSIVE_EXCLUSIVE
            )
            itemView.message_text_view.append(wordTwo, 0, wordTwo.length)
        }
    }

    fun showDialog(
        context: Context,
        title: String,
        message: String,
        selectedMessage: String,
        actionBtnText: String,
        dataItem: ItemInListSearchUI
    ) {
        val dialog = Dialog(context)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setCancelable(true)
        dialog.setContentView(R.layout.dialog_select_chat)
        dialog.window?.setBackgroundDrawableResource(R.drawable.dialog_background)
        dialog.title_text_dialog.text = title
        dialog.text_dialog.text = message
        val typeface =
            Typeface.create(ResourcesCompat.getFont(context, R.font.roboto_medium), Typeface.BOLD)
        val wordTwo: Spannable =
            SpannableString(selectedMessage)
        wordTwo.setSpan(
            StyleSpan(typeface!!.style),
            0,
            wordTwo.length,
            Spannable.SPAN_EXCLUSIVE_EXCLUSIVE
        )
        dialog.text_dialog.append(wordTwo, 0, wordTwo.length)
        dialog.action_btn.text = actionBtnText
        dialog.action_btn.setOnClickListener {
            dialog.dismiss()
            onChatClickListener(dataItem.chat)
        }
        dialog.cancel_button.setOnClickListener { dialog.dismiss() }
        dialog.show()
    }


    inner class GroupMessageViewHolder(itemView: View) :
        AdapterWithNotInternetAndLoad.BaseViewHolder(itemView) {
        override fun bind(position: Int) {
            val dataItem = data[position] as ItemInListGroupName
            itemView.apply {
                name_group_message.text = when (dataItem.group) {
                    PERSONAL_DIALOG -> resources.getString(R.string.people)
                    CHANNEL -> resources.getString(R.string.channels)
                    GROUP_CHAT -> resources.getString(R.string.groups)
                    else -> dataItem.group.toString()
                }
            }
        }
    }

}

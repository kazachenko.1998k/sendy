package com.example.feature_list_messages_impl.adapters

import android.annotation.SuppressLint
import android.os.Handler
import android.text.Spannable
import android.text.SpannableString
import android.text.SpannableStringBuilder
import android.text.style.ForegroundColorSpan
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.widget.AppCompatTextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.DecodeFormat
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.load.resource.bitmap.BitmapTransitionOptions.withCrossFade
import com.bumptech.glide.request.RequestOptions
import com.example.feature_list_messages_impl.R
import com.example.feature_list_messages_impl.convertToVisible
import com.example.feature_list_messages_impl.interfaces.IListMessage
import com.example.util_cache.Repository
import com.morozov.core_backend_api.AnyInnerRequest
import com.morozov.core_backend_api.AnyRequest
import com.morozov.core_backend_api.SignData
import com.morozov.core_backend_api.chat.Chat
import com.morozov.core_backend_api.chat.Chat.Companion.CHANNEL
import com.morozov.core_backend_api.chat.Chat.Companion.GROUP_CHAT
import com.morozov.core_backend_api.chat.Chat.Companion.PERSONAL_DIALOG
import com.morozov.core_backend_api.chat.Chat.Companion.PRIVATE_CHANNELS
import com.morozov.core_backend_api.chat.Chat.Companion.SECRET_CHAT
import com.morozov.core_backend_api.file.FileModel
import com.morozov.core_backend_api.message.Message
import com.morozov.core_backend_api.user.requests.UserGetLastActiveRequest
import com.morozov.lib_backend.LibBackendDependency
import com.social.solution.feature_list_messages_api.models.ActiveState
import com.social.solutions.util_text_formators.convertLongToCurrentType
import com.social.solutions.util_text_formators.convertToShortString
import com.social.solutions.util_text_formators.isMuteNow
import com.social.solutions.util_text_formators.isOnline
import kotlinx.android.synthetic.main.item_recycler_message_list.view.*
import kotlinx.coroutines.*


abstract class AdapterMessage(
    var data: MutableList<Chat>
) :
    RecyclerView.Adapter<AdapterMessage.BaseViewHolder>(), IListMessage {
    var contextIsActive = true
    private val mapForUserState = sortedMapOf<Long, ActiveState>()
    private val animTime = 300L
    private val newRequestTime = 15 * 1000L
    private val timeForOffline = 5 * 1000 * 60
    val requestOptions =
        RequestOptions().diskCacheStrategy(DiskCacheStrategy.ALL).circleCrop()
            .override(200, 200).format(DecodeFormat.PREFER_RGB_565)

    abstract fun onBindLastCacheView()

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): AdapterMessage.BaseViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.item_recycler_message_list, parent, false)
        val holder = BaseViewHolder(view)
        view.setOnClickListener { onChatClickListener(data[holder.adapterPosition]) }
        Glide
            .with(view)
            .asDrawable()
            .load(R.drawable.ic_mute)
            .apply(requestOptions)
            .into(view.is_mute_image_view)

        return holder
    }

    @SuppressLint("SetTextI18n")
    override fun onBindViewHolder(holder: AdapterMessage.BaseViewHolder, position: Int) {
        holder.bind(position)
        if (position == 20) onBindLastCacheView()
    }

    override fun getItemCount(): Int {
        return data.size
    }

    override fun getItemId(position: Int): Long {
        return data[position].id
    }

    inner class BaseViewHolder(itemView: View) :
        RecyclerView.ViewHolder(itemView) {
        var onStartBindPosition = -1
        val animationHandler = Handler()
        val onlineHandler = Handler()

        @SuppressLint("SetTextI18n", "CheckResult")
        fun bind(position: Int) {
            val dataItem = data[position]
            onStartBindPosition = position
            update(dataItem, position)
        }

        private fun update(dataItem: Chat, position: Int) {
            updateOnlineStateListener(dataItem, position)
            updateUserName(dataItem)
            updateMute(dataItem)
            updateTextMessage(position)
            updateUnreadCountFromDB(position)
            updateLastMessageTime(dataItem)
            itemView.apply {
                Glide
                    .with(is_read_image_view)
                    .asDrawable()
                    .load(
                        if (dataItem.lastMessage?.owner?.uid == SignData.uid) R.drawable.ic_empty else
                            when (dataItem.lastMessage?.status) {
                                Message.WATCHED -> R.drawable.ic_read
                                Message.SENT -> R.drawable.ic_sent
                                Message.RECEIVED -> R.drawable.ic_not_read
                                else -> R.drawable.ic_empty
                            }
                    )
                    .into(is_read_image_view)
                if (dataItem.iconFileId == null) {
                    Glide
                        .with(profile_image_image_view)
                        .clear(profile_image_image_view)
                } else {
                    Glide
                        .with(profile_image_image_view)
                        .asBitmap()
                        .placeholder(R.drawable.tmp_profile)
                        .transition(withCrossFade())
                        .load(SignData.server_file + dataItem.iconFileId)
                        .thumbnail(0.2f)
                        .apply(requestOptions)
                        .into(profile_image_image_view)
                }
            }
        }

        private fun updateMute(dataItem: Chat) {
            val isMute = dataItem.muteTime.isMuteNow()
            val isMuteVisible = isMute.convertToVisible()
            val before = itemView.is_mute_image_view.visibility
            if (before != isMuteVisible) {
                itemView.is_mute_image_view.visibility = isMuteVisible
            }
        }

        private fun updateUserName(dataItem: Chat) {
            var displayName = dataItem.title
            if (displayName.isNullOrEmpty()) {
                displayName = "@" + dataItem.nick
            }
            val view = itemView.name_user_text_view
            updateText(view, displayName)
        }

        private fun updateText(view: AppCompatTextView, text: String) {
            updateText(view, StringBuilder(text))
        }

        private fun updateText(view: AppCompatTextView, text: CharSequence) {
            GlobalScope.launch(Dispatchers.Main) {
                val before = view.text
                if (before != text) {
                    view.text = text
//                    val mParams = TextViewCompat.getTextMetricsParams(view)
//                    GlobalScope.launch(Dispatchers.IO) {
//                        val pText = PrecomputedTextCompat.create(text, mParams)
//                        GlobalScope.launch(Dispatchers.Main) {
//                            TextViewCompat.setPrecomputedText(view, pText)
//                        }
//                    }
                }
            }
        }


        private fun updateLastMessageTime(dataItem: Chat) {
            var displayTime = ""
            if (dataItem.lastMessageTime != null)
                displayTime =
                    (dataItem.lastMessageTime!!).convertLongToCurrentType(itemView.context)
            updateText(itemView.date_message_text_view, displayTime)
        }

        private fun updateOnlineStateListener(dataItem: Chat, position: Int) {
            onlineHandler.removeCallbacks(onlineRunnable)
            val dialogUid = dataItem.dialogUid
            if (dialogUid != null) {
                requestOnlineState(dataItem, position)
            } else {
                dataItem.lastOnline = null
            }
            updateOnlineState(dataItem, position)
        }


        private fun requestOnlineState(dataItem: Chat, position: Int) {
            onlineRunnable.dataItem = dataItem
            onlineRunnable.position = position
            if (!mapForUserState.containsKey(dataItem.dialogUid!!)) {
                mapForUserState[dataItem.dialogUid] = ActiveState(
                    dataItem.dialogUid!!,
                    dataItem.lastOnline,
                    System.currentTimeMillis() - newRequestTime,
                    0
                )
            }
            val activeState = mapForUserState[dataItem.dialogUid]!!
            onlineRunnable.activeState = activeState
            val delay = activeState.lastRequestTime + newRequestTime - System.currentTimeMillis()
            onlineHandler.postDelayed(onlineRunnable, delay)
        }


        private fun updateOnlineState(dataItem: Chat, position: Int) {
            if (onStartBindPosition == position) {
                val time = dataItem.lastOnline
                val isOnline = time.isOnline()
                if (itemView.is_online_icon?.visibility != isOnline.convertToVisible())
                    CoroutineScope(Dispatchers.Main).launch {
                        itemView.is_online_icon?.visibility =
                            isOnline.convertToVisible()
                    }
            }
        }

        private fun updateUnreadCountFromDB(
            position: Int
        ) {
            val dataItem = data[position]
            val isMute = dataItem.muteTime.isMuteNow()
            updateUnreadCount(position, isMute)
            GlobalScope.launch(Dispatchers.IO) {
                val oldData = dataItem.unreadMessage
                val newData = Repository.chatDao.getUnreadCountMessages(dataItem.id) ?: 0
                if (oldData != newData) {
                    (data[position]).unreadMessage = newData
                    updateUnreadCount(position, isMute)
                }
            }
        }

        private fun updateUnreadCount(
            position: Int,
            isMute: Boolean
        ) {
            val dataItem = data[position]
            val countUnread = dataItem.unreadMessage
            val view = itemView.count_message_text_view ?: return
            CoroutineScope(Dispatchers.Main).launch {
                if (onStartBindPosition == position) {
                    val needVisible = (countUnread > 0)
                    view.visibility = needVisible.convertToVisible()
                    if (needVisible) {
                        updateText(view, countUnread.convertToShortString())
                        view.isEnabled = !isMute
                    }
                }
            }
        }

        private fun updateTextMessage(position: Int) {
            animationHandler.removeCallbacks(animationRunnable)
            val context = itemView.context
            val dataItem = data[position]
            val lastMessage = dataItem.lastMessage
//            when (dataItem.user.currentStateUser) {
//                NOTHING -> {
            var newText = SpannableStringBuilder("")
            if (lastMessage?.files.isNullOrEmpty()) {
                when (dataItem.type) {
                    PERSONAL_DIALOG, SECRET_CHAT ->
                        newText = if (lastMessage?.uid == SignData.uid) {
                            setSelectedTextWithMessage(
                                context.getString(R.string.your_message) + " ",
                                lastMessage.text ?: "",
                                R.color.colorTextBackground
                            )
                        } else {
                            SpannableStringBuilder(lastMessage?.text ?: "")
                        }
                    GROUP_CHAT -> {
                        if (lastMessage?.uid == SignData.uid) {
                            newText = setSelectedTextWithMessage(
                                context.getString(R.string.your_message) + " ",
                                lastMessage.text ?: "",
                                R.color.colorTextBackground
                            )
                        } else {
                            var creatorName = lastMessage?.owner?.firstName
                                ?: lastMessage?.owner?.nick
                            creatorName =
                                if (!creatorName.isNullOrEmpty()) "$creatorName: " else ""
                            newText = setSelectedTextWithMessage(
                                creatorName,
                                lastMessage?.text ?: "",
                                R.color.colorTextBackground
                            )
                        }
                    }
                    CHANNEL, PRIVATE_CHANNELS -> {
                        newText = SpannableStringBuilder(lastMessage?.text ?: "")
                    }
                }
            } else {
                val file = lastMessage!!.files!!.first()
                val nameFileType = when {
                    file.type == FileModel.FILE_TYPE_IMAGE -> "Фотография"
                    lastMessage.type == Message.VOICE -> "Голосовое сообщение"
                    file.type == FileModel.FILE_TYPE_AUDIO && lastMessage.type != Message.VOICE -> "Аудио"
                    file.type == FileModel.FILE_TYPE_GIF -> "GIF"
                    file.type == FileModel.FILE_TYPE_MP4 || file.type == FileModel.FILE_TYPE_HLS -> "Видео"
                    file.type == FileModel.FILE_TYPE_TXT -> "Текстовый документ"
                    else -> "Вложение"
                }
                when {
                    lastMessage.uid == SignData.uid -> {
                        newText = setSelectedText(
                            context.getString(R.string.your_message) + " ",
                            R.color.colorTextBackground
                        )
                    }
                    dataItem.type == GROUP_CHAT -> {
                        var creatorName = lastMessage.owner?.firstName
                            ?: lastMessage.owner?.nick
                        creatorName = if (!creatorName.isNullOrEmpty()) "$creatorName: " else ""
                        newText = setSelectedText(
                            creatorName,
                            R.color.colorTextBackground
                        )
                    }
                    else -> {
                        newText = SpannableStringBuilder("")
                    }
                }
                newText.addSelectedText(nameFileType)
            }
            updateText(itemView.message_text_view, newText)

//                }
//                SEND_PHOTO -> {
//                    when (dataItem.type) {
//                        PERSONAL_DIALOG -> {
//                            addSelectedTextWithAnim(context.getString(R.string.send_photo))
//                        }
//                        GROUP_CHAT -> {
//                            addSelectedTextWithAnim(
//                                dataItem.user.firstName + " " + context.getString(
//                                    R.string.send_photo
//                                )
//                            )
//                        }
//                        CHANNEL -> {
//                            itemView.message_text_view.append(dataItem.lastMessage.text)
//                        }
//                    }
//                }
//                SEND_VIDEO -> {
//                    when (dataItem.type) {
//                        PERSONAL_DIALOG -> {
//                            addSelectedTextWithAnim(context.getString(R.string.send_video))
//                        }
//                        GROUP_CHAT -> {
//                            addSelectedTextWithAnim(
//                                dataItem.user.firstName + " " + context.getString(
//                                    R.string.send_video
//                                )
//                            )
//                        }
//                        CHANNEL -> {
//                            itemView.message_text_view.append(dataItem.lastMessage.text)
//                        }
//                    }
//                }
//                SEND_DATA -> {
//                    when (dataItem.type) {
//                        PERSONAL_DIALOG -> {
//                            addSelectedTextWithAnim(context.getString(R.string.send_data))
//                        }
//                        GROUP_CHAT -> {
//                            addSelectedTextWithAnim(
//                                dataItem.user.firstName + " " + context.getString(
//                                    R.string.send_data
//                                )
//                            )
//                        }
//                        CHANNEL -> {
//                            itemView.message_text_view.append(dataItem.lastMessage.text)
//                        }
//                    }
//                }
//                WRITE_TEXT -> {
//                    when (dataItem.type) {
//                        PERSONAL_DIALOG -> {
//                            addSelectedTextWithAnim(context.getString(R.string.writing_message))
//                        }
//                        GROUP_CHAT -> {
//                            addSelectedTextWithAnim(
//                                dataItem.user.firstName + " " + context.getString(
//                                    R.string.writing_message
//                                )
//                            )
//                        }
//                        CHANNEL -> {
//                            itemView.message_text_view.append(dataItem.lastMessage.text)
//                        }
//                    }
//                }
//                RECORD_AUDIO -> {
//                    when (dataItem.type) {
//                        PERSONAL_DIALOG -> {
//                            addSelectedTextWithAnim(context.getString(R.string.record_message))
//                        }
//                        GROUP_CHAT -> {
//                            addSelectedTextWithAnim(
//                                dataItem.user.firstName + " " + context.getString(
//                                    R.string.record_message
//                                )
//                            )
//                        }
//                        CHANNEL -> {
//                            itemView.message_text_view.append(dataItem.lastMessage.text)
//                        }
//                    }
//                }
//                else -> {
//                    addSelectedText(
//                        context.getString(R.string.lock_account),
//                        R.color.colorTextBackground
//                    )
//                }
//            }
        }


        private fun setSelectedText(
            standardText: String,
            color: Int = R.color.colorAccent
        ): SpannableStringBuilder {
            val wordTwo: Spannable =
                SpannableString(standardText)
            wordTwo.setSpan(
                ForegroundColorSpan(itemView.context.resources.getColor(color)),
                0,
                wordTwo.length,
                Spannable.SPAN_EXCLUSIVE_EXCLUSIVE
            )
            return SpannableStringBuilder(wordTwo)
        }

        private fun SpannableStringBuilder.addSelectedText(
            standardText: String,
            color: Int = R.color.colorAccent
        ): SpannableStringBuilder {
            val wordTwo: Spannable =
                SpannableString(standardText)
            wordTwo.setSpan(
                ForegroundColorSpan(itemView.context.resources.getColor(color)),
                0,
                wordTwo.length,
                Spannable.SPAN_EXCLUSIVE_EXCLUSIVE
            )
            return this.append(wordTwo)
        }

        private fun addSelectedTextWithAnim(
            stringBuilder: StringBuilder,
            standardText: String,
            color: Int = R.color.colorAccent
        ) {
//            addSelectedText(standardText, color)//todo
            animationRunnable.count = 0
            animationRunnable.textStart = itemView.message_text_view.text
            animationHandler.postDelayed(animationRunnable, animTime)
        }

        private fun setSelectedTextWithMessage(
            standardText: String,
            messageText: String,
            color: Int = R.color.colorAccent
        ): SpannableStringBuilder {
            return setSelectedText(standardText, color).append(messageText)
        }

        private val animationRunnable = object : Runnable {
            var count = 0
            lateinit var textStart: CharSequence
            override fun run() {
                count++
                when (count) {
                    1 -> {//add two white dots for not change position
                        itemView.message_text_view.text = textStart
                        val word3: Spannable =
                            SpannableString("..")
                        word3.setSpan(
                            ForegroundColorSpan(itemView.context.resources.getColor(R.color.colorBackground)),
                            0,
                            word3.length,
                            Spannable.SPAN_EXCLUSIVE_EXCLUSIVE
                        )
                        itemView.message_text_view.append(word3)
                    }
                    2 -> {//add white and blue dots for not change position
                        val word2: Spannable =
                            SpannableString(".")
                        word2.setSpan(
                            ForegroundColorSpan(itemView.context.resources.getColor(R.color.colorAccent)),
                            0,
                            word2.length,
                            Spannable.SPAN_EXCLUSIVE_EXCLUSIVE
                        )
                        val word3: Spannable =
                            SpannableString(".")
                        word3.setSpan(
                            ForegroundColorSpan(itemView.context.resources.getColor(R.color.colorBackground)),
                            0,
                            word3.length,
                            Spannable.SPAN_EXCLUSIVE_EXCLUSIVE
                        )
                        itemView.message_text_view.text = textStart
                        itemView.message_text_view.append(word2)
                        itemView.message_text_view.append(word3)
                    }
                    3 -> {//add 2 blue dots for not change position
                        val wordTwo: Spannable =
                            SpannableString("..")
                        wordTwo.setSpan(
                            ForegroundColorSpan(itemView.context.resources.getColor(R.color.colorAccent)),
                            0,
                            wordTwo.length,
                            Spannable.SPAN_EXCLUSIVE_EXCLUSIVE
                        )
                        itemView.message_text_view.text = textStart
                        itemView.message_text_view.append(wordTwo)
                    }
                }
                animationHandler.postDelayed(this, animTime)
                if (count == 3) count = 0
            }
        }

        private val onlineRunnable = object : Runnable {
            var activeState: ActiveState? = null
            var dataItem: Chat? = null
            var position: Int? = null
            override fun run() {
                val uid = activeState?.userId ?: return
                val runnable = this
                LibBackendDependency.featureBackendApi().userApi().getLastActive(
                    AnyRequest(
                        AnyInnerRequest(UserGetLastActiveRequest(uid))
                    )
                ) { resp ->
                    CoroutineScope(Dispatchers.IO).launch {
                        val lastActive = resp.data?.last_active
                        if (lastActive != null) {
                            Repository.userDao.updateUserLastActive(uid, lastActive)
                            activeState?.lastActive = lastActive
                            dataItem?.lastOnline = lastActive
                            activeState?.lastRequestTime = System.currentTimeMillis()
                            withContext(Dispatchers.Main) {
                                dataItem ?: return@withContext
                                position ?: return@withContext
                                updateOnlineState(dataItem!!, position!!)
                            }
                            delay(newRequestTime) //15 sec
                            itemView.is_online_icon?.context ?: return@launch
                            if (position != onStartBindPosition) return@launch
                            if (!contextIsActive) return@launch
                            if ((activeState?.lastRequestTime
                                    ?: 0L) + 10 * 1000L > System.currentTimeMillis()
                            ) {
                                return@launch
                            }
                            onlineHandler.post(runnable)
                        }
                    }
                }
            }
        }
    }


}

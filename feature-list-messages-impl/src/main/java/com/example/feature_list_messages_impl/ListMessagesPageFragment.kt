package com.example.feature_list_messages_impl

import android.annotation.SuppressLint
import android.graphics.Point
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.feature_list_messages_impl.adapters.AdapterMessage
import com.example.feature_list_messages_impl.adapters.PreCachingLayoutManager
import com.example.util_cache.Repository
import com.morozov.core_backend_api.AnyInnerRequest
import com.morozov.core_backend_api.AnyRequest
import com.morozov.core_backend_api.chat.Chat
import com.morozov.core_backend_api.chat.requests.ChatRecommendChatsRequest
import com.morozov.core_backend_api.chat.requests.ChatUserChatsRequest
import com.morozov.core_backend_api.chat.responses.ChatUserChatsResponse
import com.morozov.lib_backend.LibBackendDependency
import kotlinx.android.synthetic.main.page_fragment_list_messages.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext


@Suppress("DeferredResultUnused")
class ListMessagesPageFragment(var messagesTypes: MutableList<Int>) : Fragment() {
    private var isUpdating = false
    private var adapterRecycler: AdapterMessage? = null

    @SuppressLint("CheckResult")
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        return inflater.inflate(R.layout.page_fragment_list_messages, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val layoutManager = PreCachingLayoutManager(context)
        layoutManager.orientation = LinearLayoutManager.VERTICAL
        val display = activity?.windowManager?.defaultDisplay
        val size = Point()
        display?.getSize(size)
        val height: Int = size.y * 2
        layoutManager.setExtraLayoutSpace(height)
        recycler_view?.layoutManager = layoutManager
        recycler_view?.adapter = adapterRecycler
        recycler_view?.setHasFixedSize(true)
        recycler_view?.setItemViewCacheSize(15)
        initRecycler()
    }

    override fun onResume() {
        super.onResume()
        adapterRecycler?.contextIsActive = true
    }

    override fun onPause() {
        super.onPause()
        adapterRecycler?.contextIsActive = false
    }

    fun initRecycler() {
        GlobalScope.launch(Dispatchers.Main) {
            withContext(Dispatchers.IO) {
                val request: Any = if (messagesTypes.isNullOrEmpty()) {
                    ChatRecommendChatsRequest()
                } else {
                    ChatUserChatsRequest(messagesTypes)
                }
                val api = if (request is ChatRecommendChatsRequest) {
                    LibBackendDependency.featureBackendApi().chatApi()
                } else Repository.chatDaoImpl
                api.userChats(
                    AnyRequest(
                        AnyInnerRequest(
                            request
                        )
                    )
                ) { response ->
                    if (recycler_view?.adapter == null) {
                        Log.d("SCREEN_LOAD", "recycler_view.adapter")
                        GlobalScope.launch(Dispatchers.Main) {
                            withContext(Dispatchers.IO) {
                                var result =
                                    response.data?.full_chats ?: mutableListOf()
                                if (request is ChatUserChatsRequest) {
                                    result =
                                        result.filter { it.userRole != 0 && it.userRole != null }
                                            .toMutableList()
                                }
                                val adapter = object : AdapterMessage(result) {
                                    override fun loadMoreData() {
                                    }

                                    override fun onBindLastCacheView() {
                                        val manager =
                                            recycler_view?.layoutManager as PreCachingLayoutManager?
                                        manager?.setExtraLayoutSpace(100)
                                    }

                                    override fun onChatClickListener(chat: Chat) {
                                        this@ListMessagesPageFragment.callbackOnItemChatClickListener.invoke(
                                            chat
                                        )
                                    }
                                }
                                adapter.setHasStableIds(true)
                                if (adapterRecycler == null)
                                    adapterRecycler = adapter
                            }
                            if (recycler_view?.adapter == null) {//for long execute code up
                                recycler_view?.adapter = adapterRecycler

                            } else {
                                updateRecycler(response)
                            }
                        }
                    } else {
                        updateRecycler(response)
                    }
                }
            }
        }
    }


    private fun updateRecycler(response: ChatUserChatsResponse) {
        if (isUpdating) return
        GlobalScope.launch(Dispatchers.Main) {
            isUpdating = true
            withContext(Dispatchers.IO) {
                val oldData = mutableListOf<Chat>()
                oldData.addAll(adapterRecycler!!.data)
                val newData = response.data?.full_chats ?: return@withContext
                newData.forEach { item ->
                    val oldItem =
                        oldData.find { it.id == item.id }
                    if (oldItem != null) {
                        item.unreadMessage = oldItem.unreadMessage
                        item.lastOnline = oldItem.lastOnline
                    }
                }
                Log.d("SCREEN_LOAD", "old = ${oldData.map { it.title }}")
                Log.d("SCREEN_LOAD", "new = ${newData.map { it.title }}")
                //sort old list and add new
//                newData.forEachIndexed { index, item ->
//                    val oldItem =
//                        newData.find { it is ItemInListMessageUI && it.chat.chatId == (item as ItemInListMessageUI).chat.chatId } as ItemInListMessageUI?
//                    if (oldItem != null) {
//                        //replace chat if add new message
//                        val oldIndex = newData.indexOf(oldItem)
//                        if (oldIndex !=  index) {
//                            (item as ItemInListMessageUI).fullChat = oldItem.fullChat
//                            newData.remove(oldItem)
//                            newData.add(
//                                 index, item
//                            )
////                            withContext(Dispatchers.Main) {
////                                adapterRecycler.notifyItemMoved(oldIndex, index).also {
////                                    adapterRecycler.notifyItemChanged(index)
////                                }
////                            }
//                        }
//                    } else {
//                        //add new chat if not exists
//                        newData.add(
//                           index, item
//                        )
////                        withContext(Dispatchers.Main) {
////                            adapterRecycler.notifyItemInserted(index)
////                        }
//                    }
//                }

                //remove deleted chat
//                for (index in 0 until newData.size) {
//                    if (index > newData.lastIndex) break
//                    val item = newData[index]
//                    if (item is ItemInListMessageUI) {
//                        val isExistsOldChat =
//                            result.find { it is ItemInListMessageUI && it.chat.chatId == item.chat.chatId } != null
//                        if (!isExistsOldChat) {
//                            newData.remove(item)
////                            withContext(Dispatchers.Main) {
////                                adapterRecycler.notifyItemRemoved(index)
////                            }
//                        }
//                    }
//                }
                adapterRecycler?.data = newData
            }
            adapterRecycler?.notifyDataSetChanged()
            isUpdating = false
            Log.i("UPDATE2", "FINISH")
        }
    }


    private var callbackOnItemChatClickListener: (item: Chat) -> Unit = {}

    fun setCallbackOnItemChatClickListener(callback: (item: Chat) -> Unit) {
        this.callbackOnItemChatClickListener = callback
    }

}
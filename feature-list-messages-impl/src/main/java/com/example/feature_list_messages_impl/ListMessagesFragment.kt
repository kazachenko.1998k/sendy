package com.example.feature_list_messages_impl

import android.annotation.SuppressLint
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import androidx.fragment.app.FragmentStatePagerAdapter
import androidx.viewpager.widget.ViewPager
import com.example.util_cache.Repository
import com.google.android.material.tabs.TabLayout
import com.morozov.core_backend_api.AnyInnerRequest
import com.morozov.core_backend_api.AnyRequest
import com.morozov.core_backend_api.chat.Chat
import com.morozov.core_backend_api.message.Message
import com.morozov.core_backend_api.push.PushResponse
import com.morozov.core_backend_api.update.requests.UpdateChatsRequest
import com.morozov.core_backend_api.update.requests.UpdateLastChatsRequest
import com.morozov.lib_backend.LibBackendDependency
import com.social.solution.feature_list_messages_api.FeatureListMessagesCallback
import com.social.solution.feature_list_messages_api.ListMessagesFeatureApi
import io.reactivex.disposables.Disposable
import kotlinx.android.synthetic.main.fragment_list_messages.*
import kotlinx.android.synthetic.main.item_tab_layout.view.*
import kotlinx.coroutines.*


class ListMessagesFragment : Fragment(),
    ListMessagesFeatureApi {
    var mCallback: FeatureListMessagesCallback? = null
    private val allMessagesFragment = ListMessagesPageFragment(mutableListOf(0, 1, 2, 5, 6, 7))
    private val interestingMessagesFragment = ListMessagesPageFragment(mutableListOf())
    private var countNewDataChat = 10
    private var countNewDataMessage = 100
    private var adapter: TabPagerAdapter? = null
    private var pushObserver: Disposable? = null
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        updateNewDataFromServer {
            activity?.runOnUiThread { loading_progress?.let { it.hide() } }
        }
        updateBadge()
        adapter = TabPagerAdapter(
            childFragmentManager
        )
        return inflater.inflate(
            R.layout.fragment_list_messages,
            container,
            false
        )
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
//        if (message == null) {
        initListeners()
        initViewPager()
//        } else {
//            initViewForward()
//            initListenersForward()
//        }
    }

    private fun initViewPager() {
        view_pager?.offscreenPageLimit = 1
        view_pager?.adapter = adapter
        view_pager?.currentItem = 0
        configureTabLayout()
    }

//    private fun initViewForward() {
//        val transaction = childFragmentManager.beginTransaction()
//        transaction.setCustomAnimations(
//            android.R.anim.fade_in,
//            android.R.anim.fade_out,
//            android.R.anim.fade_in,
//            android.R.anim.fade_out
//        )
//        transaction.replace(R.id.fragment_for_forward_list, allMessagesFragment)
//        transaction.commit()
//    }

    private fun configureTabLayout() {
        tab_layout_select_message_list?.setupWithViewPager(view_pager)
        for (i in 0 until tab_layout_select_message_list.tabCount) {
            val tab: TabLayout.Tab = tab_layout_select_message_list.getTabAt(i)!!
            tab.customView = adapter!!.getTabView(i)
        }
    }

    private inner class TabPagerAdapter(fm: FragmentManager) :
        FragmentPagerAdapter(fm, BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT) {
        private val tabTitles = arrayOf("Чаты", "Рекомендации")

        override fun getItem(position: Int): Fragment {
            return when (position) {
                1 -> interestingMessagesFragment
                0 -> allMessagesFragment
                else -> interestingMessagesFragment
            }
        }

        override fun getCount(): Int {
            return tabTitles.size
        }

        fun getTabView(position: Int): View? { // Given you have a custom layout in `res/layout/custom_tab.xml` with a TextView and ImageView
            val v = LayoutInflater.from(context).inflate(R.layout.item_tab_layout, null)
            v.name_tab_view.text = tabTitles[position]
            v.badge_view.visibility = false.convertToVisible()
            return v
        }

    }

//    private fun initListenersForward() {
//        search_view?.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
//            override fun onQueryTextSubmit(query: String?): Boolean {
//                return true
//            }
//
//            override fun onQueryTextChange(s: String?): Boolean {
//                if (s != null)
//                    allMessagesFragment.(currentListRoom.filter {
//                        (it.address != null && it.address!!.contains(s, true))
//                                || (it.boxNumber!!.toString().contains(s, true))
//                    })
//                return true
//            }
//        })
//        button_on_menu.setOnClickListener {
//            content_main.transitionToEnd()
//            val imm: InputMethodManager? =
//                ContextCompat.getSystemService<InputMethodManager>(
//                    context!!,
//                    InputMethodManager::class.java
//                )
//            imm?.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0)
//            search_view.requestFocus()
//        }
//        button_on_back_pressed.setOnClickListener {
//            if (content_main.targetPosition == 1f) {
//                content_main.transitionToStart()
//            } else {
//                activity?.onBackPressed()
//            }
//        }
//    }

    @SuppressLint("CheckResult")
    private fun initListeners() {
        view_pager?.addOnPageChangeListener(object : ViewPager.OnPageChangeListener {
            override fun onPageScrollStateChanged(state: Int) {
            }

            override fun onPageScrolled(
                position: Int,
                positionOffset: Float,
                positionOffsetPixels: Int
            ) {
            }

            override fun onPageSelected(position: Int) {
                mCallback?.onPageSelected(position)
            }

        })
        allMessagesFragment.setCallbackOnItemChatClickListener {
            mCallback?.onItemChatClickListener(it)

        }
        interestingMessagesFragment.setCallbackOnItemChatClickListener {
            mCallback?.onItemChatClickListener(it)
        }
        button_search.setOnClickListener {
            mCallback?.onSearchIconClick()
        }
        button_write_new_message.setOnClickListener {
            mCallback?.onCreateChatClick()
        }
        if (pushObserver == null) {
            pushObserver = LibBackendDependency.featureBackendApi().pushListener.filter {
                it.data?.type in listOf(
                    PushResponse.TYPE_DIALOG_MESSAGE,
                    PushResponse.TYPE_CHAT_ADD,
                    PushResponse.TYPE_CHAT_CREATED,
                    PushResponse.TYPE_CHAT_DELETE,
                    PushResponse.TYPE_CHAT_EDIT,
                    PushResponse.TYPE_CHAT_REMOVE,
                    PushResponse.TYPE_MESSAGE_ADD,
                    PushResponse.TYPE_MESSAGE_DELETED,
                    PushResponse.TYPE_MESSAGE_EDIT
                )
            }.subscribe({ event ->
                Log.d("UPDATE_M_and_C", event.toString())
                updateNewDataFromServer()
            }, {
                it.printStackTrace()
            })
        }


    }


    private fun updateNewDataFromServer(callback: () -> Unit = {}) {
        CoroutineScope(Dispatchers.IO).launch {
            val lastTimeMessage = Repository.chatDao.getTimeLastMessage() ?: 0
            val lastTimeChat = Repository.chatDao.getTimeLastChat() ?: 0
            if (lastTimeChat == 0L && lastTimeMessage == 0L) {
                activity?.runOnUiThread { loading_progress?.let { it.show() } }
                val request = AnyRequest(AnyInnerRequest(UpdateLastChatsRequest(20)))
                LibBackendDependency.featureBackendApi().updateApi().lastChats(request) {
                    CoroutineScope(Dispatchers.IO).launch {
                        val runnable = mutableListOf<Deferred<*>>()
                        it.data?.forEach { message ->
                            runnable.add(async { Repository.chatDao.insert(message) })
                        }
                        runnable.forEach { task -> task.await() }
                        updateAdapters()
                    }
                }
            } else {
                activity?.runOnUiThread { loading_progress?.let { it.hide() } }
            }
            val listStates = mutableListOf<Pair<StatesDownload, Boolean>>()
            downloadNewChats(lastTimeChat) {
                listStates.add(Pair(StatesDownload.NEW_CHATS, it))
                listStates.add(Pair(StatesDownload.UPDATE_CHATS, it))
                onCompleteTask(listStates, callback)
            }
            downloadNewMessages(lastTimeMessage, 0) {
                listStates.add(Pair(StatesDownload.NEW_MESSAGES, it))
                onCompleteTask(listStates, callback)
            }
            downloadNewMessages(lastTimeMessage, 1) {
                listStates.add(Pair(StatesDownload.UPDATE_MESSAGES, it))
                onCompleteTask(listStates, callback)
            }
            deleteChats(lastTimeChat) {
                listStates.add(Pair(StatesDownload.DELETE_CHATS, it))
                onCompleteTask(listStates, callback)
            }
            deleteMessages(lastTimeMessage) {
                listStates.add(Pair(StatesDownload.DELETE_MESSAGES, it))
                onCompleteTask(listStates, callback)
            }
        }
    }

    @Synchronized
    private fun onCompleteTask(
        states: List<Pair<StatesDownload, Boolean>>,
        callback: () -> Unit = {}
    ) {
        Log.d("UPDATE_M_and_C", states.toString())

        if (states.size == 6) {
            callback()
            if (dataIsNotEmpty(states)) {
                updateBadge()
                updateAdapters()
            }
        }
    }

    private fun updateBadge() {
        GlobalScope.launch(Dispatchers.IO) {
            val countUnread = Repository.chatDao.getUnreadCountChat() ?: 0
            withContext(Dispatchers.Main) {
                mCallback?.onUpdateCountUnreadChat(countUnread)
            }
        }
    }

    private fun updateAdapters() {
        allMessagesFragment.initRecycler()
        Log.d("UPDATE_M_and_C", "NOTIFY_DATA")
    }

    private fun dataIsNotEmpty(states: List<Pair<StatesDownload, Boolean>>) =
        states.find { it.second } != null


    private fun downloadNewMessages(
        time: Long,
        type: Int,
        hasData: Boolean = false,
        callback: (Boolean) -> Unit
    ) {
        requestMessages(time, type) {
            if (it.isNullOrEmpty().not()) {
                CoroutineScope(Dispatchers.IO).launch {
                    if (it!!.size >= countNewDataMessage) {
                        downloadNewMessages(
                            it.maxBy { it.timeAddMS ?: 0 }?.timeAddMS ?: time, type, true,
                            callback
                        )
                    }
                    val runnable = mutableListOf<Deferred<*>>()
                    it.forEach { message ->
                        Log.d("UPDATE_M_I", message.toString())
                        runnable.add(async {
                            Repository.messageDao.insertWithUpdate(message)
                            val owner = message.owner
                            if (owner != null) {
                                Repository.userDao.insertWithOutUpdate(owner)
                            }
                        })
                    }
                    runnable.forEach { task -> task.await() }
                    if (it.size < countNewDataMessage) {
                        callback(true)
                    }
                }
            } else {
                callback(hasData)
            }
        }
    }

    private fun deleteMessages(time: Long, hasData: Boolean = false, callback: (Boolean) -> Unit) {
        requestMessages(time, 2) {
            if (it.isNullOrEmpty().not()) {
                CoroutineScope(Dispatchers.IO).launch {
                    if (it!!.size >= countNewDataMessage) {
                        deleteMessages(
                            it.maxBy { it.timeAddMS ?: 0 }?.timeAddMS ?: time, true,
                            callback
                        )
                    }
                    val runnable = mutableListOf<Deferred<*>>()
                    it.forEach { message ->
                        Log.d("UPDATE_M_D", message.toString())
                        runnable.add(async { Repository.messageDao.delete(message) })
                    }
                    runnable.forEach { task -> task.await() }
                    if (it.size < countNewDataMessage) {
                        callback(true)
                    }
                }
            } else {
                callback(hasData)
            }
        }
    }

    private fun downloadNewChats(
        time: Long,
        hasData: Boolean = false,
        callback: (Boolean) -> Unit
    ) {
        requestChats(time, 0) {
            if (it.isNullOrEmpty().not()) {
                CoroutineScope(Dispatchers.IO).launch {
                    if (it!!.size >= countNewDataChat) {
                        downloadNewChats(
                            it.maxBy { it.editTimeMS ?: 0 }?.editTimeMS ?: time,
                            true,
                            callback
                        )
                    }
                    val runnable = mutableListOf<Deferred<*>>()
                    it.forEach { message ->
                        runnable.add(async { Repository.chatDao.insert(message) })
                    }
                    runnable.forEach { task -> task.await() }
                    if (it.size < countNewDataChat) {
                        callback(true)
                    }
                }
            } else {
                callback(hasData)
            }
        }
    }

    private fun deleteChats(time: Long, hasData: Boolean = false, callback: (Boolean) -> Unit) {
        requestChats(time, 1) {
            if (it.isNullOrEmpty().not()) {
                CoroutineScope(Dispatchers.IO).launch {
                    if (it!!.size >= countNewDataChat) {
                        deleteChats(
                            it.maxBy { it.editTimeMS ?: 0 }?.editTimeMS ?: time, true,
                            callback
                        )
                    }
                    val runnable = mutableListOf<Deferred<*>>()
                    it.forEach { message ->
                        Log.d("UPDATE_C_D", message.toString())
                        runnable.add(async { Repository.chatDao.delete(message) })
                    }
                    runnable.forEach { task -> task.await() }
                    if (it.size < countNewDataChat) {
                        callback(true)
                    }
                }
            } else {
                callback(hasData)
            }
        }
    }

    private fun requestChats(
        time: Long,
        type: Int,
        callback: (item: MutableList<Chat>?) -> Unit
    ) {
        val request = AnyRequest(AnyInnerRequest(UpdateChatsRequest(time, type, countNewDataChat)))
        LibBackendDependency.featureBackendApi().updateApi().chats(request) {
            callback(it.data)
        }
    }

    private fun requestMessages(
        time: Long,
        type: Int,
        callback: (item: MutableList<Message>?) -> Unit
    ) {
        val request =
            AnyRequest(AnyInnerRequest(UpdateChatsRequest(time, type, countNewDataMessage)))
        LibBackendDependency.featureBackendApi().updateApi().messages(request) {
            callback(it.data)
        }
    }

    enum class StatesDownload {
        NEW_CHATS,
        NEW_MESSAGES,
        UPDATE_CHATS,
        UPDATE_MESSAGES,
        DELETE_CHATS,
        DELETE_MESSAGES
    }

}
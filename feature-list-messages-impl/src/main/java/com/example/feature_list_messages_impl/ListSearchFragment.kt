package com.example.feature_list_messages_impl

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.social.solution.feature_list_messages_api.models.ItemInListMessageUI
import com.example.feature_list_messages_impl.adapters.AdapterSearch
import com.morozov.core_backend_api.chat.Chat
import kotlinx.android.synthetic.main.fragment_list_search.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext


class ListSearchFragment : Fragment() {
    private lateinit var adapterRecycler: AdapterSearch
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        return inflater.inflate(R.layout.fragment_list_search, container, false)
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initListeners()
        initRecycler()
    }

    private fun initRecycler() {
        GlobalScope.launch(Dispatchers.Main) {
            recycler_view?.adapter = withContext(Dispatchers.IO) {
                adapterRecycler = object : AdapterSearch(mutableListOf(), null) {
                    override fun loadMoreData() {
                        if (isLoading) return
                        super.loadMoreData()
                        GlobalScope.launch(Dispatchers.Main) {
//                            val data = withContext(Dispatchers.IO) {
//                            }
//                            addNewDataFromServer(data)
                        }
                    }

                    override fun onSearchTextEdit(chat: ItemInListMessageUI) {

                    }

                    override fun onChatClickListener(chat: Chat) {

                    }
                }
                adapterRecycler
            }
            recycler_view?.post {
                adapterRecycler.loadMoreData()
            }
        }
    }

    private fun initListeners() {
        button_on_back_pressed.setOnClickListener {
            activity?.onBackPressed()
        }
    }
}
package com.example.feature_list_messages_impl.utility

import android.animation.Animator
import android.animation.AnimatorListenerAdapter
import android.content.Context
import android.graphics.Point
import android.view.WindowManager
import android.view.animation.DecelerateInterpolator
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.SimpleItemAnimator


class ItemAnimatorCustom : SimpleItemAnimator() {
    override fun animateRemove(holder: RecyclerView.ViewHolder): Boolean {
        return false
    }

    private fun getHeight(context: Context): Int {
        val manager =
            context.getSystemService(Context.WINDOW_SERVICE) as WindowManager
        val size = Point()
        val display = manager.defaultDisplay
        display.getRealSize(size)
        return size.y
    }

    override fun animateAdd(viewHolder: RecyclerView.ViewHolder): Boolean {
        val height: Int = getHeight(viewHolder.itemView.context)
        viewHolder.itemView.translationY = height.toFloat()
        viewHolder.itemView.animate()
            .translationY(0F)
            .setInterpolator(DecelerateInterpolator(3.0f))
            .setDuration(1000)
            .setListener(object : AnimatorListenerAdapter() {
                override fun onAnimationEnd(animation: Animator?) {
                    super.onAnimationEnd(animation)
                    dispatchAddFinished(viewHolder)
                }
            })
            .start()
        return false
    }

    override fun animateMove(
        holder: RecyclerView.ViewHolder,
        fromX: Int,
        fromY: Int,
        toX: Int,
        toY: Int
    ): Boolean {
        return false
    }

    override fun animateChange(
        oldHolder: RecyclerView.ViewHolder,
        newHolder: RecyclerView.ViewHolder, fromLeft: Int, fromTop: Int,
        toLeft: Int, toTop: Int
    ): Boolean {
        return false
    }

    override fun runPendingAnimations() {}
    override fun endAnimation(item: RecyclerView.ViewHolder) {}
    override fun endAnimations() {}
    override fun isRunning(): Boolean {
        return false
    }
}
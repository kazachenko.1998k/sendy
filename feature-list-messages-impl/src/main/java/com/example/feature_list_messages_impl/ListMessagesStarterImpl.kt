package com.example.feature_list_messages_impl

import android.util.Log
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import com.morozov.core_backend_api.message.Message
import com.social.solution.feature_list_messages_api.FeatureListMessagesCallback
import com.social.solution.feature_list_messages_api.ListMessagesFeatureApi
import com.social.solution.feature_list_messages_api.ListMessagesStarter

class ListMessagesStarterImpl : ListMessagesStarter {
    val fragment = ListMessagesFragment()

    override fun startDefault(
        manager: FragmentManager,
        container: Int,
        addToBackStack: Boolean,
        callback: FeatureListMessagesCallback,
        message: Message?
    ): ListMessagesFeatureApi {
//        fragment.message = message
        fragment.mCallback = (callback)
        startDefault(fragment, manager, container, addToBackStack)
        return fragment
    }

    fun startSearchView(
        manager: FragmentManager,
        container: Int,
        addToBackStack: Boolean
    ) {
        startDefault(ListSearchFragment(), manager, container, addToBackStack)
    }

    private fun startDefault(
        fragment: Fragment,
        manager: FragmentManager,
        container: Int,
        addToBackStack: Boolean
    ) {
        val transaction = manager.beginTransaction()
        transaction.setCustomAnimations(
            android.R.anim.fade_in,
            android.R.anim.fade_out,
            android.R.anim.fade_in,
            android.R.anim.fade_out
        )
        transaction.replace(container, fragment)
        if (addToBackStack)
            transaction.addToBackStack(fragment.tag)
        transaction.commit()
        Log.d("TAG_CHECK", "I2")
    }

}
package com.example.feature_list_messages_impl.interfaces

import com.morozov.core_backend_api.chat.Chat

interface IListMessage {
    fun onChatClickListener(chat: Chat)
    fun loadMoreData()
}
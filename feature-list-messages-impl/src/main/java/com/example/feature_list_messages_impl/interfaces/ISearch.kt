package com.example.feature_list_messages_impl.interfaces

import com.social.solution.feature_list_messages_api.models.ItemInListMessageUI

interface ISearch {
    fun onSearchTextEdit(chat: ItemInListMessageUI)
}
package com.example.feature_list_messages_impl.interfaces

interface IPageSelected {
    fun onPageSelected(pageNumber: Int)
}
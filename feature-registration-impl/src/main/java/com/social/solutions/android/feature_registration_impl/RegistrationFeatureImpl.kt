package com.social.solutions.android.feature_registration_impl

import com.social.solutions.android.feature_registration_api.RegistrationFeatureApi
import com.social.solutions.android.feature_registration_api.RegistrationStarterApi

class RegistrationFeatureImpl(private val starterApi: RegistrationStarterApi): RegistrationFeatureApi {
    override fun starter(): RegistrationStarterApi = starterApi
}
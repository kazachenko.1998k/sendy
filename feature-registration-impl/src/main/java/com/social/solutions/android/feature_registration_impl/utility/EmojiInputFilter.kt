package com.social.solutions.android.feature_registration_impl.utility

import android.text.InputFilter
import android.text.Spanned

class EmojiInputFilter: InputFilter {
    override fun filter(source: CharSequence?, start: Int, end: Int,
                        dest: Spanned?, dstart: Int, dend: Int): CharSequence? {
        source ?: return null
        for (char in source) {
            val type: Int = Character.getType(char)
            if (type == Character.SURROGATE.toInt()) {
                return ""
            }
        }
        return null
    }
}
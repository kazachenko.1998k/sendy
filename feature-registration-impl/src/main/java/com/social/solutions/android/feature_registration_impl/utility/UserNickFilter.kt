package com.social.solutions.android.feature_registration_impl.utility

import android.text.InputFilter
import android.text.Spanned

class UserNickFilter: InputFilter {
    override fun filter(
        source: CharSequence,  start: Int,     end: Int,
        dest: Spanned?,         dstart: Int,    dend: Int
    ): CharSequence {
        for (char in source) {
            if (char.isWhitespace())
                return ""
        }
        return source
    }
}
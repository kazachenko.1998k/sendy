package com.social.solutions.android.feature_registration_impl.ui.fragments.registration.fragments

import android.net.Uri
import android.os.Bundle
import android.view.View
import com.bumptech.glide.Glide
import com.example.feature_bottom_picker_api.FeatureBottomPickerCallback
import com.example.feature_bottom_picker_api.models.MediaModel
import com.example.feature_bottom_picker_api.models.PhotoModel
import com.example.feature_bottom_picker_api.models.StarterModel
import com.social.solutions.android.feature_registration_impl.R
import com.social.solutions.android.feature_registration_impl.start.MainObject
import kotlinx.android.synthetic.main.fragment_registration.*

open class PickerRegistrationFragment: BaseRegistrationFragment(), FeatureBottomPickerCallback {

    protected var mImagePath: String? = null

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        imageAvatar.setOnClickListener {
//            context?.let { it1 -> hideKeyboard(it1) }
            val configs = StarterModel()
            configs.showVideo = false
            configs.addComment = false
            MainObject.mBottomPickerApi?.start(
                childFragmentManager,
                R.id.contentMediaPicker,
                configs,
                this
            )
        }
    }

    override fun onSelected(media: List<MediaModel>) {
        if (media.isEmpty()) return
        val photo = media.first()
        if (photo is PhotoModel) {
            mImagePath = photo.filePath
            imageAvatar.setPadding(0,0,0,0)
            Glide.with(context!!)
                .load(photo.filePath)
                .into(imageAvatar)
        }
    }

    override fun onExit() {
        childFragmentManager.popBackStack()
    }

    // Useless
    override fun onSelected(media: List<MediaModel>, comment: String) {}
    override fun onFileSelected(uri: Uri) {}
}
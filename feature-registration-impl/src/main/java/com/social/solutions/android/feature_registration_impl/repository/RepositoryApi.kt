package com.social.solutions.android.feature_registration_impl.repository

import com.social.solutions.android.feature_registration_impl.repository.api.NickNameApi
import com.social.solutions.android.feature_registration_impl.repository.api.ProfileApi

internal interface RepositoryApi: NickNameApi, ProfileApi
package com.social.solutions.android.feature_registration_impl.ui.fragments.registration

import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.fragment.app.FragmentManager
import androidx.lifecycle.Observer
import com.social.solutions.android.feature_registration_impl.R
import com.social.solutions.android.feature_registration_impl.repository.FileRepository
import com.social.solutions.android.feature_registration_impl.repository.api.NickNameApi
import com.social.solutions.android.feature_registration_impl.start.MainObject
import com.social.solutions.android.feature_registration_impl.ui.fragments.registration.fragments.PhotoRegistrationFragment
import com.social.solutions.android.feature_registration_impl.ui.fragments.registration.fragments.PickerRegistrationFragment
import com.social.solutions.android.feature_registration_impl.utility.EmojiInputFilter
import com.social.solutions.android.feature_registration_impl.utility.NickNameFilter
import com.social.solutions.android.feature_registration_impl.utility.StringInputFilter
import kotlinx.android.synthetic.main.fragment_registration.*

class RegistrationFragment: PickerRegistrationFragment(), RegistrationApi {

    companion object{
        private const val TAG = "RegistrationFragment_TAG"
        var mCorrectNick = true
        internal fun start(manager: FragmentManager, container: Int, addToBackStack: Boolean, nickNameApi: NickNameApi) {
            val fragment = RegistrationFragment()
            fragment.mNickNameApi = nickNameApi
            val transaction = manager.beginTransaction()
            transaction.replace(container, fragment)
            if (addToBackStack)
                transaction.addToBackStack(TAG)
            transaction.commit()
        }
    }

    private lateinit var mNickNameApi: NickNameApi

    private var isNetworkEnable = true

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        editNick.filters = arrayOf(EmojiInputFilter(), StringInputFilter(resources.getInteger(R.integer.max_nick_symbols)))
        editNick.addTextChangedListener(NickNameFilter(editNick, this::showNickWrongChar))
        textInputName.filters = arrayOf(EmojiInputFilter(), StringInputFilter(resources.getInteger(R.integer.max_name_symbols)))

        editNick.setEditNickListener(this, mNickNameApi, this::showNickExists)
        editNick.setEditReadyListener(this::showButtonEnabled)
        editNick.markRequired(context)

        initFinishButton()

        MainObject.mNetworkState?.observe(viewLifecycleOwner, androidx.lifecycle.Observer {
            isNetworkEnable = it
            showButtonEnabled(isNetworkEnable)
            if (isNetworkEnable.not())
                showNoInternet()
            else
                editNick.text = editNick.text
        })
    }

    private fun showNoInternet() {
        Toast.makeText(context, "Нет интернета", Toast.LENGTH_SHORT).show()
    }

    // initializer
    private fun initFinishButton() {
        buttonFinish.setOnClickListener { _ ->
            if (isNetworkEnable.not()) {
                showNoInternet()
                return@setOnClickListener
            }
            val repositoryApi = MainObject.mRepositoryApi ?: return@setOnClickListener
            repositoryApi.saveProfile(editNick.text.toString(), textInputName.text.toString())
                .observe(this, Observer { userId ->
                    if (userId != null) {
                        if(mImagePath != null) {
                            FileRepository.sendImage(context!!, viewLifecycleOwner, mImagePath!!, userId)
                                .subscribe({ avatarId ->
                                    repositoryApi.saveProfile(editNick.text.toString(), textInputName.text.toString(), avatarId)
                                        .observe(this, Observer {
                                            MainObject.mCallback?.onRegistered(userId)
                                        })
                                },{
                                    it.printStackTrace()
                                })
                        } else
                            MainObject.mCallback?.onRegistered(userId)
                    }
                    else
                        showUploadError()
                })
        }
    }

    private fun showUploadError() {
        Toast.makeText(context, "При загрузке что-то пошло не так.", Toast.LENGTH_SHORT).show()
    }

    // RegistrationApi
    override fun showNickExists(bool: Boolean) {
        if (bool) {
            textError.visibility = View.VISIBLE
            textError.text = context?.resources?.getString(R.string.reg_nick_name_already_exists) ?: "Already exists"
            showButtonEnabled(false)
        } else {
            textError.visibility = View.GONE
        }
    }

    override fun showNickWrongChar(bool: Boolean) {
        mCorrectNick = bool.not()
        if (bool) {
            textError.visibility = View.VISIBLE
            textError.text = context?.resources?.getString(R.string.reg_nick_name_wrong_characters) ?: "Wrong characters"
            showButtonEnabled(false)
        } else {
            textError.visibility = View.GONE
        }
    }

    override fun showButtonEnabled(bool: Boolean) {
        if (bool) {
            buttonFinish.isEnabled = true
            buttonFinish.backgroundTintList =
                context?.resources?.getColorStateList(R.color.colorAccent)
        } else {
            buttonFinish.isEnabled = false
            buttonFinish.backgroundTintList =
                context?.resources?.getColorStateList(R.color.reg_grey)
        }
    }
}
package com.social.solutions.android.feature_registration_impl.ui.fragments.registration.fragments

import android.app.Dialog
import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import android.widget.Toast
import androidx.activity.OnBackPressedCallback
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import com.social.solutions.android.feature_registration_impl.R
import com.social.solutions.android.feature_registration_impl.repository.api.NickNameApi
import com.social.solutions.android.feature_registration_impl.start.MainObject
import kotlinx.android.synthetic.main.dialog_cancel_registration.*
import kotlinx.android.synthetic.main.fragment_registration.*

open class BaseRegistrationFragment: Fragment() {
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? =
        inflater.inflate(R.layout.fragment_registration, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        linearTop.setOnClickListener {
            activity?.onBackPressed()
        }
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        activity?.onBackPressedDispatcher?.addCallback(this, object : OnBackPressedCallback(true) {
            override fun handleOnBackPressed() {
                showExitDialog(context)
            }
        })
    }

    private fun showExitDialog(context: Context) {
        val dialog = Dialog(context)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setCancelable(true)
        dialog.setContentView(R.layout.dialog_cancel_registration)
        dialog.window?.setBackgroundDrawableResource(R.drawable.dialog_background)
        dialog.action_btn.setOnClickListener {
            dialog.dismiss()
            MainObject.mCallback?.onDestroy()
        }
        dialog.cancel_button.setOnClickListener {
            dialog.dismiss()
        }
        dialog.show()
    }
}
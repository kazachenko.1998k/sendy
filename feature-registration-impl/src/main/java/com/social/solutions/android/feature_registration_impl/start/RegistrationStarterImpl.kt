package com.social.solutions.android.feature_registration_impl.start

import androidx.fragment.app.FragmentManager
import androidx.lifecycle.LiveData
import com.example.feature_bottom_picker_api.BottomPickerStarterApi
import com.morozov.core_backend_api.FeatureBackendApi
import com.social.solutions.android.feature_registration_api.FeatureRegistrationCallback
import com.social.solutions.android.feature_registration_api.RegistrationStarterApi
import com.social.solutions.android.feature_registration_impl.repository.RepositoryApi
import com.social.solutions.android.feature_registration_impl.repository.RepositoryImpl
import com.social.solutions.android.feature_registration_impl.ui.fragments.registration.RegistrationFragment

class RegistrationStarterImpl: RegistrationStarterApi {
    override fun start(manager: FragmentManager, container: Int,
                       addToBackStack: Boolean, callback: FeatureRegistrationCallback,
                       bottomPickerApi: BottomPickerStarterApi,
                       networkState: LiveData<Boolean>,
                       backendApi: FeatureBackendApi) {
        val repository: RepositoryApi = RepositoryImpl()
        MainObject.mBackendApi = backendApi
        MainObject.mNetworkState = networkState
        MainObject.mCallback = callback
        MainObject.mRepositoryApi = repository
        MainObject.mBottomPickerApi = bottomPickerApi

        RegistrationFragment.start(manager, container, addToBackStack, repository)
    }
}
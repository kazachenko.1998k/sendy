package com.social.solutions.android.feature_registration_impl.ui.fragments.registration

import android.annotation.SuppressLint
import android.content.Context
import android.graphics.Color
import android.text.Editable
import android.text.TextWatcher
import android.widget.EditText
import androidx.core.text.buildSpannedString
import androidx.core.text.color
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.Observer
import com.google.android.material.textfield.TextInputLayout
import com.social.solutions.android.feature_registration_impl.R
import com.social.solutions.android.feature_registration_impl.repository.api.NickNameApi

private const val NICK_MIN_LENGTH = 5

@SuppressLint("DefaultLocale")
internal fun EditText.setEditNickListener(owner: LifecycleOwner, api: NickNameApi, callback: (bool: Boolean)->Unit) {
    this.setEditListener(fun (nickName) {
        if (nickName.length >= NICK_MIN_LENGTH && RegistrationFragment.mCorrectNick) {
            api.checkNickExistsAsync(nickName.toLowerCase().trim()).observe(owner, Observer {
                callback(it)
            })
        }
    })
}

internal fun EditText.setEditReadyListener(callback: (bool: Boolean)->Unit) {
    this.setEditListener(fun (nickName) {
        callback(nickName.length >= NICK_MIN_LENGTH && RegistrationFragment.mCorrectNick)
    })
}

private fun EditText.setEditListener(runnable:(nickName: String)->Unit) {
    this.addTextChangedListener(object : TextWatcher {
        override fun afterTextChanged(p0: Editable?) {
            runnable(this@setEditListener.text.toString())
        }
        override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}
        override fun onTextChanged(c0: CharSequence?, p1: Int, p2: Int, p3: Int) {}
    })
}

fun EditText.markRequired(context: Context?) {
    val greyColor = context?.resources?.getColor(R.color.reg_grey) ?: Color.GRAY

    hint = buildSpannedString {
        color(greyColor) { append(hint) }
        color(Color.RED) { append(" *") } // Mind the space prefix.
    }
}

fun EditText.markHint(context: Context?) {
    val greyColor = context?.resources?.getColor(R.color.reg_grey) ?: Color.GRAY

    hint = buildSpannedString {
        color(greyColor) { append(hint) }
    }
}
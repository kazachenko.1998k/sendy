package com.social.solutions.android.feature_registration_impl.start

import androidx.lifecycle.LiveData
import com.example.feature_bottom_picker_api.BottomPickerStarterApi
import com.morozov.core_backend_api.FeatureBackendApi
import com.social.solutions.android.feature_registration_api.FeatureRegistrationCallback
import com.social.solutions.android.feature_registration_impl.repository.RepositoryApi

object MainObject {
    var mCallback: FeatureRegistrationCallback? = null
    internal var mRepositoryApi: RepositoryApi? = null
    var mBackendApi: FeatureBackendApi? = null
    var mNetworkState: LiveData<Boolean>? = null
    var mBottomPickerApi: BottomPickerStarterApi? = null
}
package com.social.solutions.android.feature_registration_impl.ui.fragments.registration

interface RegistrationApi {
    fun showNickExists(bool: Boolean)
    fun showNickWrongChar(bool: Boolean)
    fun showButtonEnabled(bool:Boolean)
}
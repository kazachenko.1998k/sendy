package com.social.solutions.android.feature_registration_impl.ui.fragments.registration.fragments

import android.Manifest
import android.app.Activity
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.database.Cursor
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.net.Uri
import android.os.Bundle
import android.provider.MediaStore
import android.provider.MediaStore.Images
import android.view.View
import android.widget.Toast
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import com.social.solutions.android.feature_registration_impl.ui.fragments.registration.fragments.photo.picker.dialog.PhotoPickDialog
import com.social.solutions.android.feature_registration_impl.utility.ImageFilePath
import kotlinx.android.synthetic.main.fragment_registration.*
import java.io.ByteArrayOutputStream
import java.io.FileNotFoundException

open class PhotoRegistrationFragment: BaseRegistrationFragment() {
    companion object{
        private const val PHOTO_REQUEST_CODE = 2
        private const val REQUEST_IMAGE_CAPTURE = 1
        private const val REQUEST_PERMISSIONS = 3
    }

    protected var mImagePath: String? = null

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        imageAvatar.setOnClickListener {
            if (getPermissionMedia())
                PhotoPickDialog.start(childFragmentManager, this::dispatchLoadPictureIntent, this::dispatchTakePictureIntent)
        }
    }

    override fun onActivityResult(reqCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(reqCode, resultCode, data)

        if (reqCode == PHOTO_REQUEST_CODE && resultCode == Activity.RESULT_OK) {
            try {
                val imageUri = data?.data ?: return
                mImagePath = ImageFilePath.getPath(context!!, imageUri)
                println("Jeka image uri")
                val imageStream = activity?.contentResolver?.openInputStream(imageUri)
                val selectedImage = BitmapFactory.decodeStream(imageStream)
                imageAvatar.setPadding(0,0,0,0)
                val bitmap = Bitmap.createBitmap(selectedImage)
                imageAvatar.setImageBitmap(bitmap)
                imageStream?.close()
            } catch (e: FileNotFoundException) {
                e.printStackTrace()
                Toast.makeText(context, "Something went wrong", Toast.LENGTH_LONG).show()
            }
        } else if (reqCode == REQUEST_IMAGE_CAPTURE && resultCode == Activity.RESULT_OK) {
            val imageBitmap = data?.extras?.get("data") ?: return
            imageAvatar.setPadding(0,0,0,0)
            imageAvatar.setImageBitmap(imageBitmap as Bitmap)

            val tempUri = getImageUri(
                context!!,
                imageBitmap
            ) ?: return
            mImagePath = getRealPathFromURI(tempUri)
        }
    }

    private fun dispatchTakePictureIntent() {
        Intent(MediaStore.ACTION_IMAGE_CAPTURE).also { takePictureIntent ->
            takePictureIntent.resolveActivity(context!!.packageManager)?.also {
                startActivityForResult(takePictureIntent,
                    REQUEST_IMAGE_CAPTURE
                )
            }
        }
    }

    private fun dispatchLoadPictureIntent() {
        val photoPickerIntent = Intent(Intent.ACTION_OPEN_DOCUMENT)
        photoPickerIntent.type = "image/*"
        startActivityForResult(photoPickerIntent,
            PHOTO_REQUEST_CODE
        )
    }

    open fun getImageUri(inContext: Context, inImage: Bitmap): Uri? {
        val bytes = ByteArrayOutputStream()
        inImage.compress(Bitmap.CompressFormat.JPEG, 100, bytes)
        val path =
            Images.Media.insertImage(inContext.getContentResolver(), inImage, "Title", null)
        return Uri.parse(path)
    }

    open fun getRealPathFromURI(uri: Uri): String? {
        var path = ""
        if (context!!.contentResolver != null) {
            val cursor: Cursor =
                context!!.contentResolver.query(uri, null, null, null, null) ?: return null
            cursor.moveToFirst()
            val idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA)
            path = cursor.getString(idx)
            cursor.close()
        }
        return path
    }

    // Permissions
    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>,
                                            grantResults: IntArray) {
        if (requestCode == REQUEST_PERMISSIONS &&
            grantResults[0] == PackageManager.PERMISSION_GRANTED &&
            grantResults[1] == PackageManager.PERMISSION_GRANTED) {

            PhotoPickDialog.start(childFragmentManager, this::dispatchLoadPictureIntent, this::dispatchTakePictureIntent)
        }
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
    }

    private fun getPermissionMedia(): Boolean {
        return if (checkPermissionReadExSt() && checkPermissionTakePicture()) {
            true
        } else {
            requestPermissions(
                listOf(
                    Manifest.permission.READ_EXTERNAL_STORAGE,
                    Manifest.permission.CAMERA,
                    Manifest.permission.WRITE_EXTERNAL_STORAGE
                ).toTypedArray(),
                REQUEST_PERMISSIONS
            )
            false
        }
    }

    private fun checkPermissionReadExSt(): Boolean {
        return (ContextCompat.checkSelfPermission(
            context!!,
            Manifest.permission.READ_EXTERNAL_STORAGE
        ) == PackageManager.PERMISSION_GRANTED)
    }

    private fun checkPermissionTakePicture(): Boolean {
        return (ContextCompat.checkSelfPermission(
            context!!,
            Manifest.permission.CAMERA
        ) == PackageManager.PERMISSION_GRANTED)
    }
}
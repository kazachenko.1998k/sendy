package com.social.solutions.android.feature_registration_impl.ui.fragments.registration.fragments.photo.picker.dialog

import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.FragmentManager
import com.social.solutions.android.feature_registration_impl.R
import kotlinx.android.synthetic.main.dialog_photo_pick_selection.*

class PhotoPickDialog: DialogFragment() {

    companion object{
        private const val TAG = "PhotoPickDialog_TAG"

        fun start(manager: FragmentManager, onGallery: ()->Unit, onTakeAPhoto: ()->Unit){
            val dialog = PhotoPickDialog()
            dialog.mOnGallery = onGallery
            dialog.mOnTakeAPhoto = onTakeAPhoto
            dialog.show(manager, TAG)
        }
    }

    private lateinit var mOnGallery: ()->Unit
    private lateinit var mOnTakeAPhoto: ()->Unit

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? =
        inflater.inflate(R.layout.dialog_photo_pick_selection, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        textFromGallery.setOnClickListener {
            dismiss()
            mOnGallery()
        }
        textTakeAPhoto.setOnClickListener {
            dismiss()
            mOnTakeAPhoto()
        }

        dialog?.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
    }
}
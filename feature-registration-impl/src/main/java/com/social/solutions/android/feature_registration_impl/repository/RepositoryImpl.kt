package com.social.solutions.android.feature_registration_impl.repository

import android.annotation.SuppressLint
import android.content.Context
import android.graphics.Bitmap
import android.net.Uri
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.morozov.core_backend_api.AnyInnerRequest
import com.morozov.core_backend_api.AnyRequest
import com.morozov.core_backend_api.common.request.CommonCheckFreeNickRequest
import com.morozov.core_backend_api.file.FileApi
import com.morozov.core_backend_api.file.request.FileUploadRequest
import com.morozov.core_backend_api.user.UserApi
import com.morozov.core_backend_api.user.requests.UserGetByNickRequest
import com.morozov.core_backend_api.user.requests.UserSaveProfileRequest
import com.social.solutions.android.feature_registration_impl.repository.utils.getMimeType
import com.social.solutions.android.feature_registration_impl.repository.utils.getName
import com.social.solutions.android.feature_registration_impl.repository.utils.getSize
import com.social.solutions.android.feature_registration_impl.start.MainObject
import io.reactivex.disposables.Disposable

internal class RepositoryImpl : RepositoryApi {

    @SuppressLint("DefaultLocale")
    override fun checkNickExists(nick: String): Boolean {
        return nick == ("Morgen11".toLowerCase().trim())
    }

    override fun checkNickExistsAsync(nick: String): LiveData<Boolean> {
        val userApi = MainObject.mBackendApi?.commonApi()
        val responce = MutableLiveData<Boolean>()
        val param = CommonCheckFreeNickRequest(nick)
        val sendMessage = AnyInnerRequest(param)
        val data = AnyRequest(sendMessage)
        userApi!!.checkFreeNick(data) { resultData ->
            responce.postValue(resultData.data?.status == false)
        }
        return responce
    }

    override fun saveProfile(
        nick: String,
        firstName: String,
        avatarFileId: String
    ): LiveData<Long?> {
        val userApi = MainObject.mBackendApi?.userApi()
        val responce = MutableLiveData<Long?>()
        val param = UserSaveProfileRequest()
        param.nick = nick
        param.first_name = firstName
        param.avatar_file_id = avatarFileId
        val sendMessage = AnyInnerRequest(param)
        val data = AnyRequest(sendMessage)
        userApi!!.saveProfile(data) { event ->
            responce.postValue(event.data?.uid?.toLong())
        }
        return responce
    }

    override fun saveProfile(nick: String, firstName: String): LiveData<Long?> {
        val userApi = MainObject.mBackendApi?.userApi()
        val responce = MutableLiveData<Long?>()
        val param = UserSaveProfileRequest()
        param.nick = nick
        param.first_name = firstName
        val sendMessage = AnyInnerRequest(param)
        val data = AnyRequest(sendMessage)
        userApi!!.saveProfile(data) { event ->
            responce.value = (event.data?.uid?.toLong())
        }
        return responce
    }
}
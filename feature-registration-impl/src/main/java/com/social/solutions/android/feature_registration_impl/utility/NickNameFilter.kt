package com.social.solutions.android.feature_registration_impl.utility

import android.text.Editable
import android.text.InputFilter
import android.text.Spanned
import android.text.TextWatcher
import android.widget.EditText

class NickNameFilter(private val editText: EditText,
                     private val wrongNick: (bool: Boolean) -> Unit): TextWatcher {
    override fun afterTextChanged(s: Editable?) {}
    override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}
    override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
        val filterText = editText.text?.filter { it.checkNickValid() }
        val originText = editText.text
        wrongNick(filterText.toString() != originText.toString())
    }

    private fun Char.checkNickValid(): Boolean {
        return Character.isDigit(this) || this.isEnglish() ||
                this == '_'
    }

    private fun Char.isEnglish(): Boolean {
        if ((this in 'a'..'z') || (this in 'A'..'Z')) {
            return true
        }
        return false
    }
}
package com.social.solutions.android.feature_registration_impl.repository.api

import androidx.lifecycle.LiveData

internal interface NickNameApi {
    fun checkNickExists(nick: String): Boolean
    fun checkNickExistsAsync(nick: String): LiveData<Boolean>
}
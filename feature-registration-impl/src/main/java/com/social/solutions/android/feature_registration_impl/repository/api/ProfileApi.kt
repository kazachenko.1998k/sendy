package com.social.solutions.android.feature_registration_impl.repository.api

import androidx.lifecycle.LiveData

interface ProfileApi {
    /**
     * @return if profile is saved -- user id, else null
     * */
    fun saveProfile(nick: String, firstName: String, avatarFileId: String): LiveData<Long?>
    /**
     * @return if profile is saved -- user id, else null
     * */
    fun saveProfile(nick: String, firstName: String): LiveData<Long?>
}
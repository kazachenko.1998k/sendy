package com.social.solutions.android.feature_registration_impl.repository

import android.content.Context
import android.content.Intent
import android.net.Uri
import android.util.Log
import androidx.core.app.JobIntentService
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.LiveData
import androidx.lifecycle.Observer
import com.example.util_load_send_workers.repository.DialogWorkRepository
import com.example.util_load_send_workers.repository.SendFileManager
import com.morozov.core_backend_api.AnyInnerRequest
import com.morozov.core_backend_api.AnyRequest
import com.morozov.core_backend_api.file.request.FileUploadRequest
import com.social.solutions.android.feature_registration_impl.repository.utils.getName
import com.social.solutions.android.feature_registration_impl.repository.utils.getSize
import com.social.solutions.android.feature_registration_impl.start.MainObject
import com.social.solutions.android.util_load_send_services.media.send.services.SendMediaService
import io.reactivex.Single
import okhttp3.MediaType.Companion.toMediaTypeOrNull

internal object FileRepository {
    fun sendImage(context: Context,
                  lifecycleOwner: LifecycleOwner,
                  imagePath: String,
                  userId: Long): Single<String> {
        return Single.create { emitter ->
            Log.i("FileRepository", "Start upload process")
            DialogWorkRepository.getLoadingLiveData().observe(lifecycleOwner, Observer { data ->
                if (data.filePath == imagePath)
                    emitter.onSuccess(data.fileId)
                Log.i("FileRepository", "Progress: ${data.progress}")
            })
            SendFileManager.with(context)
                .setBackendApi(MainObject.mBackendApi)
                .setUserId(userId)
                .setFilesExtended(listOf(SendFileManager.Transaction.File(imagePath)))
                .loadFiles()
        }
    }

    private fun uploadImageService(context: Context, imagePath: String, imageId: String, userId: Long, baseUrl: String) {
        val serviceIntent = Intent().apply {
            putExtra(SendMediaService.PHOTO_PATH, imagePath)
            putExtra(SendMediaService.FILE_ID, imageId)
            putExtra(SendMediaService.USER_ID, userId)
            putExtra(SendMediaService.BASE_URL, baseUrl)
        }
        JobIntentService.enqueueWork(
            context, SendMediaService::class.java,
            SendMediaService.SEND_PHOTO_JOB_ID, serviceIntent
        )
    }
}
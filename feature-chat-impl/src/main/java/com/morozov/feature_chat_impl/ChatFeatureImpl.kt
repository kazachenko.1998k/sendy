package com.morozov.feature_chat_impl

import com.morozov.feature_chat_api.ChatFeatureApi
import com.morozov.feature_chat_api.ChatStarterApi

class ChatFeatureImpl(private val starter: ChatStarterApi): ChatFeatureApi {
    override fun chatStarter(): ChatStarterApi = starter
}
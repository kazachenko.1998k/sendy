package com.morozov.feature_chat_impl.repository

import android.content.Context
import android.content.Intent
import androidx.core.app.JobIntentService
import com.morozov.core_backend_api.AnyInnerRequest
import com.morozov.core_backend_api.AnyRequest
import com.morozov.core_backend_api.file.request.FileUploadRequest
import com.morozov.feature_chat_impl.start.MainObject
import com.morozov.feature_chat_impl.utility.getName
import com.morozov.feature_chat_impl.utility.getSize
import com.social.solutions.android.util_load_send_services.media.send.services.SendMediaService
import com.social.solutions.android.util_load_send_services.voice.send.services.SendVoiceService
import io.reactivex.Single
import okhttp3.MediaType
import okhttp3.MediaType.Companion.toMediaTypeOrNull

internal object FileChatRepository {
    fun sendImage(context: Context, imagePath: String, userId: Long): Single<String> {
        val fileType = "image/mpeg".toMediaTypeOrNull() ?: throw IllegalArgumentException("Wrong media type: image/*")
        return sendMedia(context, imagePath, userId, fileType, FileChatRepository::uploadImageService)
    }

    fun sendVideo(context: Context, videoPath: String, userId: Long): Single<String> {
        val fileType = "video/mp4".toMediaTypeOrNull() ?: throw IllegalArgumentException("Wrong media type: video/*")
        return sendMedia(context, videoPath, userId, fileType, FileChatRepository::uploadVideoService)
    }

    fun sendVoice(context: Context, voicePath: String, userId: Long): Single<String> {
        val fileType = "audio/mp3".toMediaTypeOrNull() ?: throw IllegalArgumentException("Wrong media type: audio/*")
        return sendMedia(context, voicePath, userId, fileType, FileChatRepository::uploadVoiceService)
    }

    private fun sendMedia(context: Context, filePath: String,
                          userId: Long, mediaType: MediaType,
                          success: (context: Context, imagePath: String, imageId: String, userId: Long, baseUrl: String) -> Unit): Single<String> {
        return Single.create { emitter ->
            val fileApi = MainObject.mBackendApi?.fileApi() ?: throw IllegalArgumentException("mBackendApi must not be null")
            val param = FileUploadRequest(
                filePath.getName(context) ?: "empty_file",
                (filePath.getSize(context) ?: 0)/1000,
                mediaType.type
            )
            val sendMessage = AnyInnerRequest(param)
            val data = AnyRequest(sendMessage)
            fileApi.uploadFile(data) { event ->
                val dataResponse = event.data ?: return@uploadFile
                emitter.onSuccess(dataResponse.file_id)
                success(context, filePath, dataResponse.file_id, userId, dataResponse.file_upload_to)
            }
        }
    }

    private fun uploadImageService(context: Context, imagePath: String, imageId: String, userId: Long, baseUrl: String) {
        val serviceIntent = Intent().apply {
            putExtra(SendMediaService.PHOTO_PATH, imagePath)
            putExtra(SendMediaService.FILE_ID, imageId)
            putExtra(SendMediaService.USER_ID, userId)
            putExtra(SendMediaService.BASE_URL, baseUrl)
            putExtra(SendMediaService.CHAT_TYPE, 0)
        }
        JobIntentService.enqueueWork(
            context, SendMediaService::class.java,
            SendMediaService.SEND_PHOTO_JOB_ID, serviceIntent
        )
    }

    private fun uploadVideoService(context: Context, videoPath: String, imageId: String, userId: Long, baseUrl: String) {
        val serviceIntent = Intent().apply {
            putExtra(SendMediaService.VIDEO_PATH, videoPath)
            putExtra(SendMediaService.FILE_ID, imageId)
            putExtra(SendMediaService.USER_ID, userId)
            putExtra(SendMediaService.BASE_URL, baseUrl)
            putExtra(SendMediaService.CHAT_TYPE, 0)
        }
        JobIntentService.enqueueWork(
            context, SendMediaService::class.java,
            SendMediaService.SEND_VIDEO_JOB_ID, serviceIntent
        )
    }

    private fun uploadVoiceService(context: Context, voicePath: String, voiceId: String, userId: Long, baseUrl: String) {
        val serviceIntent = Intent().apply {
            putExtra(SendVoiceService.VOICE_PATH, voicePath)
            putExtra(SendVoiceService.FILE_ID, voiceId)
            putExtra(SendVoiceService.USER_ID, userId)
            putExtra(SendVoiceService.BASE_URL, baseUrl)
            putExtra(SendVoiceService.CHAT_TYPE, 5)
        }
        JobIntentService.enqueueWork(
            context, SendVoiceService::class.java,
            SendVoiceService.SEND_VOICE_JOB_ID, serviceIntent
        )
    }
}
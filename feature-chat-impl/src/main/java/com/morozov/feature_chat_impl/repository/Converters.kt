package com.morozov.feature_chat_impl.repository

import android.content.Context
import android.graphics.Bitmap
import android.util.Log
import com.morozov.core_backend_api.SignData
import com.morozov.core_backend_api.chat.Chat
import com.morozov.core_backend_api.file.FileModel
import com.morozov.core_backend_api.message.Message
import com.morozov.core_backend_api.user.User
import com.morozov.feature_chat_impl.start.MainObject
import com.morozov.feature_chat_impl.ui.fragments.chat.dialog.renderers.chat.from.me.compaign.media.and.text.FromMeMediaAndTextModel
import com.morozov.feature_chat_impl.ui.fragments.chat.dialog.renderers.chat.common.media.MediaModel
import com.morozov.feature_chat_impl.ui.fragments.chat.dialog.renderers.chat.common.media.loaded.photo.PhotoLoadedModel
import com.morozov.feature_chat_impl.ui.fragments.chat.dialog.renderers.chat.common.media.loaded.video.VideoLoadedModel
import com.morozov.feature_chat_impl.ui.fragments.chat.dialog.renderers.chat.to.me.compaign.media.and.text.ToMeMediaAndTextModel
import com.morozov.feature_chat_impl.ui.fragments.chat.dialog.utils.scaleDown
import com.morozov.feature_chat_impl.ui.fragments.chat.voice.record.renderers.VoiceState
import com.morozov.feature_chat_impl.ui.fragments.chat.voice.record.renderers.from.me.FromMeVoiceModel
import com.morozov.feature_chat_impl.ui.fragments.chat.voice.record.renderers.to.me.ToMeVoiceModel
import com.morozov.feature_chat_impl.ui.utility.ViewModelWithDate
import com.morozov.feature_chat_impl.ui.utility.ViewableMessageModel
import com.morozov.feature_chat_impl.ui.utility.message.direction.FromMeMessage
import com.morozov.feature_chat_impl.ui.utility.views.answer.message.AnswerViewConfigs
import com.morozov.feature_chat_impl.ui.utility.views.answer.message.OnAnswerViewClickListener
import com.social.solutions.android.util_load_send_services.util.addSignUrl
import com.social.solutions.android.util_media_pager.exoplayer.ExoCacheWorker
import java.lang.IllegalArgumentException
import java.net.URL
import java.util.*

fun Message.toViewModel(context: Context?, onAnswerClickListener: OnAnswerViewClickListener): ViewModelWithDate {
    return when {
        this.isMediaAndText() -> toMediaAndTextMessage(context, onAnswerClickListener)
        this.isVoice() -> toVoiceMessage()
        else -> toMediaAndTextMessage(context, onAnswerClickListener)
    }
}

fun User.getUserNameOrNick(): String {
    return if (this.firstName.isNullOrEmpty().not() && this.firstName != "Anonimus") this.firstName!! else this.nick
}

fun Message.getUserNameOrNick(): String {
    this.owner ?: return ""
    return if (this.owner!!.firstName.isNullOrEmpty().not() && this.owner!!.firstName != "Anonimus") this.owner!!.firstName!! else this.owner!!.nick
}

private fun Message.toVoiceMessage(): ViewModelWithDate {
    val calendar = Calendar.getInstance()
    calendar.timeInMillis = this.timeAdd!! * 1000
    return if (this.uid == MainObject.myId)
        when(MainObject.mChatType) {
            Chat.CHANNEL ->
                FromMeVoiceModel(this.uid!!, getUserNameOrNick(), this.id, this.files!!.toVoice(), null, 100f, this.files!!.getVoiceDuration(), 0, VoiceState.STOPPED, calendar, this.views?:0, false, status.toMessageReadStatus())
            else -> FromMeVoiceModel(this.uid!!, getUserNameOrNick(), this.id, this.files!!.toVoice(), null, 100f, this.files!!.getVoiceDuration(), 0, VoiceState.STOPPED, calendar, ViewableMessageModel.NO_VIEWABLE, false, status.toMessageReadStatus())
        }
    else {
        when(MainObject.mChatType) {
            Chat.GROUP_CHAT ->
                ToMeVoiceModel(this.uid!!, getUserNameOrNick(), this.id ?:0, getAvatar(), true, this.files!!.toVoice(), null, 100f, this.files!!.getVoiceDuration(), 0, VoiceState.STOPPED, calendar)
            Chat.CHANNEL ->
                ToMeVoiceModel(this.uid?:0, getUserNameOrNick(), this.id ?:0, null, false, this.files!!.toVoice(), null, 100f, this.files!!.getVoiceDuration(), 0, VoiceState.STOPPED, calendar, this.views ?: 0)
            else -> ToMeVoiceModel(this.uid!!, getUserNameOrNick(), this.id ?:0, null, false, this.files!!.toVoice(), null, 100f, this.files!!.getVoiceDuration(), 0, VoiceState.STOPPED, calendar)
        }
    }
}

private fun Message.toMediaAndTextMessage(context: Context?, onAnswerClickListener: OnAnswerViewClickListener): ViewModelWithDate {
    val calendar = Calendar.getInstance()
    calendar.timeInMillis = this.timeAdd!! * 1000
    var answerViewConfigs: AnswerViewConfigs? = null
    if (replyToMessage != null)
        answerViewConfigs = AnswerViewConfigs.Builder()
            .setClickListener(onAnswerClickListener)
            .setAndParseMessageItem(replyToMessage!!.toViewModel(context, onAnswerClickListener), context)
            .build()
    return if (this.uid == MainObject.myId)
        when(MainObject.mChatType) {
            Chat.CHANNEL ->
                FromMeMediaAndTextModel(this.uid!!, getUserNameOrNick(), this.id, this.files?.toMediaModel(context) ?: emptyList(), this.text ?: "", calendar, false, this.views ?: 0, answerViewConfigs, status.toMessageReadStatus())
            else -> FromMeMediaAndTextModel(this.uid!!, getUserNameOrNick(), this.id, this.files?.toMediaModel(context) ?: emptyList(), this.text ?: "", calendar, false, ViewableMessageModel.NO_VIEWABLE, answerViewConfigs, status.toMessageReadStatus())
        }
    else {
        when(MainObject.mChatType) {
            Chat.GROUP_CHAT ->
                ToMeMediaAndTextModel(this.uid!!, getUserNameOrNick(), this.id ?:0, getAvatar(), true, this.files?.toMediaModel(context) ?: emptyList(), this.text ?: "", calendar, false, ViewableMessageModel.NO_VIEWABLE, answerViewConfigs)
            Chat.CHANNEL ->
                ToMeMediaAndTextModel(this.uid ?: 0, getUserNameOrNick(), this.id ?:0, null, false, this.files?.toMediaModel(context) ?: emptyList(), this.text ?: "", calendar, false, this.views ?: 0, answerViewConfigs)
            else -> ToMeMediaAndTextModel(this.uid!!, getUserNameOrNick(), this.id ?:0, null, false, this.files?.toMediaModel(context) ?: emptyList(), this.text ?: "", calendar, false, ViewableMessageModel.NO_VIEWABLE, answerViewConfigs)
        }
    }
}

private fun List<FileModel>.toMediaModel(context: Context?): List<MediaModel> {
    return this.mapIndexed { index, fileModel ->
        val url = URL(SignData.server_file + fileModel.fileId)
        val model = when(fileModel.type) {
            FileModel.FILE_TYPE_IMAGE -> PhotoLoadedModel(
                index,
                url,
                null,
                null
            )
            FileModel.FILE_TYPE_MP4 -> {
                if (context != null)
                    ExoCacheWorker.cacheVideo(context, url.toString())
                VideoLoadedModel(
                    index,
                    url,
                    null,
                    null,
                    fileModel.length ?: 0
                )
            }
            else -> VideoLoadedModel(
                index,
                url,
                null,
                null,
                fileModel.length ?: 0
            )
        }
        val pair = ((fileModel.width ?: model.width) to (fileModel.height ?: model.height)).scaleDown()
        model.width = pair.first
        model.height = pair.second
        model
    }
}

private fun List<FileModel>.toVoice(): String {
    if (this.size != 1) throw IllegalArgumentException("Not voice")
    return (SignData.server_file + this.first().fileId).addSignUrl(SignData.sign).toString()
}

private fun List<FileModel>.getVoiceDuration(): Int {
    if (this.size != 1) throw IllegalArgumentException("Not voice")
    return this.first().length ?: 0
}

private fun Message.isMediaAndText(): Boolean =
    files?.isMediaFiles() == true

private fun List<FileModel>.isMediaFiles(): Boolean {
    for (file in this) {
        if (file.type != FileModel.FILE_TYPE_IMAGE &&
            file.type != FileModel.FILE_TYPE_MP4)
            return false
    }
    return true
}

private fun Message.isVoice(): Boolean {
//    this.files?:return false
//    for (file in this.files!!) {
//        if (file.type != FileModel.FILE_TYPE_AUDIO)
//            return false
//    }
    return this.type == 5
}

private fun Message.getAvatar(): URL {
    val url = (SignData.server_file + this.owner?.avatarFileId).addSignUrl(SignData.sign)
    Log.i("User icon", url.toString())
    return url
}

fun Int?.toMessageReadStatus(): FromMeMessage.ReadStatus {
    return when(this) {
        0 -> FromMeMessage.ReadStatus.SENT
        1 -> FromMeMessage.ReadStatus.USER_RECEIVED
        2 -> FromMeMessage.ReadStatus.USER_READ
        else -> FromMeMessage.ReadStatus.SENDING
    }
}
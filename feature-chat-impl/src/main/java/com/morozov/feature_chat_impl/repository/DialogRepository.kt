package com.morozov.feature_chat_impl.repository

import android.content.Context
import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.example.util_cache.Repository
import com.example.util_load_send_workers.repository.SendFileManager
import com.morozov.core_backend_api.AnyInnerRequest
import com.morozov.core_backend_api.AnyRequest
import com.morozov.core_backend_api.chat.Chat
import com.morozov.core_backend_api.chat.requests.ChatGetByIDRequest
import com.morozov.core_backend_api.chat.requests.ChatGetDialogIdRequest
import com.morozov.core_backend_api.chat.requests.ChatGetMessagesRequest
import com.morozov.core_backend_api.message.Message
import com.morozov.core_backend_api.message.requests.MessageDeleteRequest
import com.morozov.core_backend_api.message.requests.MessageGetRequest
import com.morozov.core_backend_api.message.requests.MessageSendRequest
import com.morozov.core_backend_api.message.requests.MessageViewRequest
import com.morozov.core_backend_api.user.User
import com.morozov.core_backend_api.user.requests.UserGetByIdsRequest
import com.morozov.feature_chat_impl.start.MainObject
import com.morozov.feature_chat_impl.ui.fragments.chat.ChatFragment
import com.morozov.feature_chat_impl.ui.fragments.chat.dialog.renderers.chat.common.media.video.VideoModel
import com.morozov.feature_chat_impl.ui.fragments.chat.dialog.renderers.chat.from.me.compaign.media.and.text.FromMeMediaAndTextModel
import com.morozov.feature_chat_impl.ui.fragments.chat.voice.record.renderers.from.me.FromMeVoiceModel
import com.morozov.feature_chat_impl.ui.utility.ViewModelWithDate
import io.reactivex.Single
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import java.lang.IllegalArgumentException

/**@return message id, if null -- message not sent
 * */
fun ViewModelWithDate.sendToBack(context: Context, replyTo: Long?): LiveData<Long?>? {
    val resultLiveData = MutableLiveData<Long?>()
    return when (this) {
        is FromMeMediaAndTextModel -> {
            if (this.media.isEmpty())
                DialogRepository.sendMessageText(MainObject.mRecipientId!!, text, replyTo)
            else {
                SendFileManager.with(context)
                    .setBackendApi(MainObject.mBackendApi)
                    .setRecipientId(MainObject.mRecipientId)
                    .setChatId(MainObject.mChatId)
                    .setUserId(MainObject.myId)
                    .setMessageSentLiveData(resultLiveData)
                    .setMessageType(0)
                    .setMessage(text)
                    .setReplyTo(replyTo)
                    .setFilesExtended(
                        media.map {
                            val length = if (it is VideoModel) it.seconds else null
                            SendFileManager.Transaction.File(it.filePath ?: throw IllegalArgumentException("File path must not be null")).createVideoIfNotNull(length)
                        }
                    )
                    .sendMessage()
                resultLiveData
            }
        }
        is FromMeVoiceModel -> {
            SendFileManager.with(context)
                .setBackendApi(MainObject.mBackendApi)
                .setRecipientId(MainObject.mRecipientId)
                .setChatId(MainObject.mChatId)
                .setUserId(MainObject.myId)
                .setMessageSentLiveData(resultLiveData)
                .setMessageType(5)
                .setVoice(fileName!!, timeInSeconds)
                .sendMessage()
            resultLiveData
        }
        else -> null
    }
}

object DialogRepository {

    const val MESSAGES_PACK_AMOUNT = 9

    fun deleteMessages(ids: List<Long>, isForAll: Boolean) {
        val messageApi = MainObject.mBackendApi?.messageApi() ?: return
        val param = MessageDeleteRequest(
            MainObject.mChatId ?: MainObject.mRecipientId,
            ids.toMutableList(),
            isForAll
        )
        val sendMessage = AnyInnerRequest(param)
        val data = AnyRequest(sendMessage)
        messageApi.delete(data) {}
    }

    fun getDialogById(uid: Long): LiveData<Chat?> {
        val chatApi = MainObject.mBackendApi?.chatApi()
        val responce = MutableLiveData<Chat?>()
        val param = ChatGetDialogIdRequest(uid)
        val sendMessage = AnyInnerRequest(param)
        val data = AnyRequest(sendMessage)
        chatApi!!.getDialogID(data) { event ->
            responce.postValue(event.data?.chat)
        }
        return responce
    }

    fun getUserById(uid: Long): LiveData<User?> {
        val userApi = MainObject.mBackendApi?.userApi()
        val responce = MutableLiveData<User?>()
        val param = UserGetByIdsRequest(mutableListOf(uid), true)
        val sendMessage = AnyInnerRequest(param)
        val data = AnyRequest(sendMessage)

        var loadedByNet = false

        userApi?.getByIdFromDB(uid) { user ->
            if (loadedByNet.not())
                responce.postValue(user)
        }

        userApi!!.getByIds(data) { event ->
            loadedByNet = true
            if (event.data != null && event.data?.users?.isNotEmpty() == true)
                responce.postValue(event.data!!.users[0])
            else
                responce.postValue(null)
        }
        return responce
    }

    fun loadDialog(chatId: Long, messageFrom: Long = 0, direction: Int = 0): LiveData<List<Message>> {
        val responce = MutableLiveData<List<Message>>()

        val chatApi = MainObject.mBackendApi?.chatApi()
        val param = ChatGetMessagesRequest(chatId)
        param.amount = MESSAGES_PACK_AMOUNT
        param.full_message = true
        param.add_message_owner = true
        param.message_id = messageFrom
        param.direction = direction
        val sendMessage = AnyInnerRequest(param)
        val data = AnyRequest(sendMessage)

        var resultFromNet = false
        Repository.chatDaoImpl.getMessages(data) { event ->
            if (resultFromNet.not() && event.data != null)
                responce.postValue(event.data!!.full_message)
        }

        chatApi!!.getMessages(data) { event ->
            if (event.result) {
                resultFromNet = true
                responce.postValue(event.data!!.full_message)
            }
        }

        return responce
    }

    fun loadMessage(chatId: Long, messageId: Long, withOwner: Boolean = false): Single<Message> {
        return Single.create{ emitter ->
            val messageApi = MainObject.mBackendApi?.messageApi()
            val param = MessageGetRequest(chatId, mutableListOf(messageId))
            param.get_owner = withOwner
            val sendMessage = AnyInnerRequest(param)
            val data = AnyRequest(sendMessage)
            messageApi!!.get(data) { event ->
                if (event.result)
                    emitter.onSuccess(event.data!!.messages!!.first())
            }
        }
    }

    /**@return chat type
     * */
    fun getChatInfo(chatId: Long): LiveData<Chat> {
        val responce = MutableLiveData<Chat>()

        val chatApi = MainObject.mBackendApi?.chatApi()
        val param = ChatGetByIDRequest(mutableListOf(chatId))
        val sendMessage = AnyInnerRequest(param)
        val data = AnyRequest(sendMessage)

        var loadedByNet = false

        chatApi?.getByIDWithDB(data) { event ->
            if (loadedByNet.not() && event.data != null)
                responce.postValue(event.data!!.chats[0])
        }

        chatApi!!.getByID(data) { event ->
            if (event.result) {
                loadedByNet = true
                responce.postValue(event.data!!.chats[0])
            }
        }

        return responce
    }

    /**@return is message sent
     * */
    internal fun sendMessageText(recipientId: Long, message: String, replyTo: Long?): LiveData<Long?> {
        val response = MutableLiveData<Long?>()
        val messageApi = MainObject.mBackendApi?.messageApi()
        val param = MessageSendRequest()
        when (MainObject.mChatType) {
            Chat.PERSONAL_DIALOG -> param.recipient_id = MainObject.mUserId
            Chat.GROUP_CHAT -> param.chat_id = MainObject.mChatId
            Chat.CHANNEL -> param.chat_id = MainObject.mChatId
        }
        param.type = 0
        param.message = message
        param.reply_to = replyTo
        val sendMessage = AnyInnerRequest(param)
        val data = AnyRequest(sendMessage)
        messageApi!!.send(data) { event ->
            response.postValue(event.data?.id)
        }
        return response
    }

    fun sendView(messageId: Long) {
        ChatFragment.mSentMessage.postValue(messageId)
        val messageApi = MainObject.mBackendApi?.messageApi()
        val param = MessageViewRequest()
        when (MainObject.mChatType) {
            Chat.PERSONAL_DIALOG -> param.chat_id = MainObject.mUserId
            Chat.GROUP_CHAT -> param.chat_id = MainObject.mChatId
            Chat.CHANNEL -> param.chat_id = MainObject.mChatId
        }
        param.message_ids = mutableListOf(messageId)
        val sendMessage = AnyInnerRequest(param)
        val data = AnyRequest(sendMessage)
        messageApi!!.view(data) {}
        CoroutineScope(Dispatchers.IO).launch {
            MainObject.mMessageDao?.readMessage(messageId)
        }
    }
}
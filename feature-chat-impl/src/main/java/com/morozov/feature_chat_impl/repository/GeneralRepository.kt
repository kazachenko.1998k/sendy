package com.morozov.feature_chat_impl.repository

import com.morozov.core_backend_api.AnyInnerRequest
import com.morozov.core_backend_api.AnyRequest
import com.morozov.core_backend_api.chat.requests.ChatClearHistoryRequest
import com.morozov.core_backend_api.chat.requests.ChatJoinRequest
import com.morozov.core_backend_api.chat.requests.ChatLeaveRequest
import com.morozov.core_backend_api.chat.requests.ChatMuteRequest
import com.morozov.feature_chat_impl.start.MainObject
import io.reactivex.Single

object GeneralRepository {
    fun enableNotificationsUser(enable: Boolean) {
        val chatApi = MainObject.mBackendApi?.chatApi() ?: return
        val chatId = MainObject.mChatId ?: return
        val time: Long = if (enable) 0 else 60*60*24*30*12*2
        val request = ChatMuteRequest(mutableListOf(chatId), time)
        val inReq = AnyInnerRequest(request)
        val req = AnyRequest(inReq)
        chatApi.mute(req) {}
    }

    fun clearChat() {
        val chatApi = MainObject.mBackendApi?.chatApi() ?: return
        val chatId = MainObject.mChatId ?: return
        val request = ChatClearHistoryRequest(chatId, false)
        val inReq = AnyInnerRequest(request)
        val req = AnyRequest(inReq)
        chatApi.clearHistory(req) {}
    }

    fun deleteChat() {

    }

    fun leaveChat() {
        val chatApi = MainObject.mBackendApi?.chatApi() ?: return
        val chatId = MainObject.mChatId ?: return
        val request = ChatLeaveRequest(chatId)
        val inReq = AnyInnerRequest(request)
        val req = AnyRequest(inReq)
        chatApi.leave(req) {}
    }

    fun joinChat(): Single<Boolean> {
        return Single.create { emitter ->
            val chatApi = MainObject.mBackendApi?.chatApi()
            val chatId = MainObject.mChatId
            if (chatId == null || chatApi == null) {
                emitter.onSuccess(false)
                return@create
            }
            val request = ChatJoinRequest(chatId)
            val inReq = AnyInnerRequest(request)
            val req = AnyRequest(inReq)
            chatApi.join(req) {
                emitter.onSuccess(true)
            }
        }
    }
}
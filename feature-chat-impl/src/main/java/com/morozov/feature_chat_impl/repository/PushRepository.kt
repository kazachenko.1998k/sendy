package com.morozov.feature_chat_impl.repository

import android.content.Context
import com.morozov.core_backend_api.push.PushResponse
import com.morozov.feature_chat_impl.start.MainObject
import com.morozov.feature_chat_impl.ui.utility.ViewModelWithDate
import com.morozov.feature_chat_impl.ui.utility.views.answer.message.OnAnswerViewClickListener
import io.reactivex.Flowable
import io.reactivex.Single

object PushRepository {
    fun subscribeMessagePush(): Flowable<PushResponse> {
        return MainObject.mBackendApi!!.pushListener.filter { response ->
            response.data?.type == PushResponse.TYPE_MESSAGE_ADD
        }
    }

    fun loadMessage(
        context: Context,
        push: PushResponse,
        listener: OnAnswerViewClickListener
    ): Single<ViewModelWithDate> {
        return Single.create { emitter ->
            DialogRepository.loadMessage(push.data!!.chat_id!!, push.data!!.msg_id!!, true)
                .subscribe({
                    emitter.onSuccess(it.toViewModel(context, listener))
                }, {
                    it.printStackTrace()
                })
        }
    }
}
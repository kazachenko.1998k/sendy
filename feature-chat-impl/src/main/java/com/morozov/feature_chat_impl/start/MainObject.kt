package com.morozov.feature_chat_impl.start

import com.example.feature_bottom_picker_api.BottomPickerStarterApi
import com.example.util_cache.message.MessageDao
import com.morozov.core_backend_api.FeatureBackendApi
import com.morozov.feature_chat_api.FeatureChatCallback

object MainObject {
    var mCallback: FeatureChatCallback? = null
    var mBackendApi: FeatureBackendApi? = null
    var mMessageDao: MessageDao? = null
    var bottomPickerStarterApi: BottomPickerStarterApi? = null
    var myId: Long? = null
    var mUserId: Long? = null
    var mChatId: Long? = null

    // Generated after start chat info
    var myUserName = ""
    var mRecipientId: Long? = null
    var mChatType: Int? = null
}
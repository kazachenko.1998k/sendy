package com.morozov.feature_chat_impl.start

import androidx.fragment.app.FragmentManager
import androidx.lifecycle.LiveData
import com.example.feature_bottom_picker_api.BottomPickerStarterApi
import com.example.util_cache.message.MessageDao
import com.morozov.core_backend_api.FeatureBackendApi
import com.morozov.core_backend_api.chat.Chat
import com.morozov.feature_chat_api.ChatStarterApi
import com.morozov.feature_chat_api.FeatureChatCallback
import com.morozov.feature_chat_impl.ui.fragments.chat.BaseChatFragment
import com.morozov.feature_chat_impl.ui.fragments.chat.ChatFragment
import com.vanniktech.emoji.EmojiManager
import com.vanniktech.emoji.ios.IosEmojiProvider

class ChatStarterImpl: ChatStarterApi {
    override fun start(myId: Long,
                       manager: FragmentManager, container: Int,
                       backendApi: FeatureBackendApi,
                       messageDao: MessageDao,
                       bottomPickerApi: BottomPickerStarterApi,
                       addToBackStack: Boolean, callback: FeatureChatCallback,
                       networkState: LiveData<Boolean>) {

        EmojiManager.install(IosEmojiProvider())
        MainObject.myId = myId
        MainObject.mCallback = callback
        MainObject.mBackendApi = backendApi
        MainObject.mMessageDao = messageDao
        MainObject.bottomPickerStarterApi = bottomPickerApi
        val fragment = ChatFragment()
        fragment.mNetworkState = networkState
        val transaction = manager.beginTransaction()
        transaction.replace(container, fragment)
        if (addToBackStack)
            transaction.addToBackStack(BaseChatFragment.TAG)
        transaction.commit()
    }

    override fun startChatWithUser(myId: Long,
                                   manager: FragmentManager, container: Int,
                                   backendApi: FeatureBackendApi,
                                   messageDao: MessageDao,
                                   bottomPickerApi: BottomPickerStarterApi,
                                   addToBackStack: Boolean,
                                   callback: FeatureChatCallback, networkState: LiveData<Boolean>, userId: Long) {

        MainObject.mUserId = userId
        MainObject.mChatId = null
        start(myId, manager, container, backendApi, messageDao, bottomPickerApi, addToBackStack, callback, networkState)
    }

    override fun startChat(myId: Long,
                           manager: FragmentManager, container: Int,
                           backendApi: FeatureBackendApi,
                           messageDao: MessageDao,
                           bottomPickerApi: BottomPickerStarterApi,
                           addToBackStack: Boolean, callback: FeatureChatCallback,
                           networkState: LiveData<Boolean>, chatId: Long) {

        MainObject.mUserId = null
        MainObject.mChatId = chatId
        start(myId, manager, container, backendApi, messageDao, bottomPickerApi, addToBackStack, callback, networkState)
    }

    override fun startChat(myId: Long,
                           manager: FragmentManager, container: Int,
                           backendApi: FeatureBackendApi,
                           messageDao: MessageDao,
                           bottomPickerApi: BottomPickerStarterApi,
                           addToBackStack: Boolean, callback: FeatureChatCallback,
                           networkState: LiveData<Boolean>, chatModel: Chat) {

        EmojiManager.install(IosEmojiProvider())
        MainObject.mUserId = null
        MainObject.mChatId = chatModel.id
        MainObject.myId = myId
        MainObject.mCallback = callback
        MainObject.mBackendApi = backendApi
        MainObject.mMessageDao = messageDao
        MainObject.bottomPickerStarterApi = bottomPickerApi
        val fragment = ChatFragment()
        fragment.mStartChatModel = chatModel
        fragment.mNetworkState = networkState
        val transaction = manager.beginTransaction()
        transaction.replace(container, fragment)
        if (addToBackStack)
            transaction.addToBackStack(BaseChatFragment.TAG)
        transaction.commit()
    }
}
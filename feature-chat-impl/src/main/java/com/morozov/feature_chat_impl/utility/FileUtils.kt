package com.morozov.feature_chat_impl.utility

import android.content.ContentResolver
import android.content.Context
import android.net.Uri
import android.webkit.MimeTypeMap
import java.io.File

fun String.getName(context: Context?): String? {
    return this.substring(this.lastIndexOf("/")+1)
}

fun Uri.getMimeType(context: Context?): String? {
    val cR: ContentResolver = context?.contentResolver ?: return null
    val mime = MimeTypeMap.getSingleton()
    return mime.getExtensionFromMimeType(cR.getType(this))
}

fun String.getSize(context: Context?): Int? {
    return (File(this).length()/1024).toString().toInt()
}
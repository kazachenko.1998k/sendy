package com.morozov.feature_chat_impl.utility

import android.annotation.SuppressLint
import android.content.Context
import android.view.View
import com.morozov.feature_chat_impl.R
import java.text.SimpleDateFormat
import java.util.*
import java.util.regex.Pattern


@SuppressLint("SimpleDateFormat")
fun String.convertDateToLong(): Long {
    val df = SimpleDateFormat("dd.MM.yyyy HH:mm")
    return df.parse(this).time
}

@SuppressLint("SimpleDateFormat")
fun Long.convertLongToCurrentType(context: Context): String {
    val thisDay = Calendar.getInstance()
    val morning = Calendar.getInstance()
    morning.add(Calendar.HOUR_OF_DAY, -morning[Calendar.HOUR_OF_DAY])
    morning.add(Calendar.MINUTE, -morning[Calendar.MINUTE])
    morning.add(Calendar.SECOND, -morning[Calendar.SECOND])
    val yesterday = Calendar.getInstance()
    yesterday.add(Calendar.DATE, -1)
    yesterday.add(Calendar.HOUR_OF_DAY, -yesterday[Calendar.HOUR_OF_DAY])
    yesterday.add(Calendar.MINUTE, -yesterday[Calendar.MINUTE])
    yesterday.add(Calendar.SECOND, -yesterday[Calendar.SECOND])
    val weekBefore = Calendar.getInstance()
    weekBefore.add(Calendar.DATE, -6)
    weekBefore.add(Calendar.HOUR_OF_DAY, -weekBefore[Calendar.HOUR_OF_DAY])
    weekBefore.add(Calendar.MINUTE, -weekBefore[Calendar.MINUTE])
    weekBefore.add(Calendar.SECOND, -weekBefore[Calendar.SECOND])
    val startYear = Calendar.getInstance()
    startYear.add(Calendar.DAY_OF_YEAR, -startYear[Calendar.DAY_OF_YEAR])
    startYear.add(Calendar.HOUR_OF_DAY, -startYear[Calendar.HOUR_OF_DAY])
    startYear.add(Calendar.MINUTE, -startYear[Calendar.MINUTE])
    startYear.add(Calendar.SECOND, -startYear[Calendar.SECOND])
    return when {
        this > morning.timeInMillis -> this.convertLongToToday(context)
        this > yesterday.timeInMillis -> this.convertLongToYesterday(context)
        this > weekBefore.timeInMillis -> this.convertLongToDayOfWeek()
        this > startYear.timeInMillis -> this.convertLongToDayOfYear()
        else -> this.convertLongToData()
    }
}

@SuppressLint("SimpleDateFormat")
fun Long.convertLongToTime(): String {
    val date = Date(this)
    val format = SimpleDateFormat("HH:mm")
    return format.format(date)
}

@SuppressLint("SimpleDateFormat")
fun Long.convertLongToDayOfWeek(): String {
    val date = Date(this)
    val format = SimpleDateFormat("EEEE")
    return format.format(date)
}

@SuppressLint("SimpleDateFormat")
fun Long.convertLongToDayOfYear(): String {
    val date = Date(this)
    val format = SimpleDateFormat("dd MMMM")
    return format.format(date)
}

@SuppressLint("SimpleDateFormat")
fun Long.convertLongToYesterday(context: Context): String {
    return context.getString(R.string.yesterday)
}

@SuppressLint("SimpleDateFormat")
fun Long.convertLongToToday(context: Context): String {
    return context.getString(R.string.today)
}

@SuppressLint("SimpleDateFormat")
fun Long.convertLongToMonth(): String {
    val date = Date(this)
    val format = SimpleDateFormat("MM.yyyy")
    return format.format(date)
}

@SuppressLint("SimpleDateFormat")
fun Long.convertLongToData(): String {
    val date = Date(this)
    val format = SimpleDateFormat("dd.MM.yyyy")
    return format.format(date)
}

fun Boolean?.converToVisible() =
    if (this == true) View.VISIBLE
    else View.GONE


@SuppressLint("SimpleDateFormat")
fun convertCalendarToData(date: Calendar): String {
    return String.format("%1\$td.%1\$tm.%1\$tY %1\$tH:%1\$tM", date)
}

fun String.emailCorrect(): Boolean {
    val regExpn = ("^(([\\w-]+\\.)+[\\w-]+|([a-zA-Z]{1}|[\\w-]{2,}))@"
            + "((([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\.([0-1]?"
            + "[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\."
            + "([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\.([0-1]?"
            + "[0-9]{1,2}|25[0-5]|2[0-4][0-9])){1}|"
            + "([a-zA-Z]+[\\w-]+\\.)+[a-zA-Z]{2,4})$")
    val pattern = Pattern.compile(regExpn, Pattern.CASE_INSENSITIVE)
    val matcher = pattern.matcher(this)
    return matcher.matches()
}
package com.morozov.feature_chat_impl.utility

import android.content.res.Resources
import android.text.Html
import android.text.Spanned
import com.morozov.feature_chat_impl.R

fun CharSequence.toBold(resources: Resources): Spanned {
    return formatHtml(formatString(resources, R.string.bold_font, this))
}
fun CharSequence.toItalic(resources: Resources): Spanned {
    return formatHtml(formatString(resources, R.string.italic_font, this))
}
fun CharSequence.toCrossed(resources: Resources): Spanned {
    return formatHtml(formatString(resources, R.string.crossed_font, this))
}
fun CharSequence.toUnderlined(resources: Resources): Spanned {
    return formatHtml(formatString(resources, R.string.underlined_font, this))
}
fun CharSequence.toMono(resources: Resources): Spanned {
    return formatHtml(formatString(resources, R.string.mono_font, this))
}

fun CharSequence.getBold(resources: Resources): String {
    return formatString(resources, R.string.bold_font, this)
}
fun CharSequence.getItalic(resources: Resources): String {
    return formatString(resources, R.string.italic_font, this)
}
fun CharSequence.getCrossed(resources: Resources): String {
    return formatString(resources, R.string.crossed_font, this)
}
fun CharSequence.getUnderlined(resources: Resources): String {
    return formatString(resources, R.string.underlined_font, this)
}
fun CharSequence.getMono(resources: Resources): String {
    return formatString(resources, R.string.mono_font, this)
}

fun formatHtml(string: String): Spanned {
    return Html.fromHtml(string)
}

private fun formatString(resources: Resources, resString: Int, pasteText: CharSequence): String {
    return String.format(resources.getString(resString), pasteText)
}
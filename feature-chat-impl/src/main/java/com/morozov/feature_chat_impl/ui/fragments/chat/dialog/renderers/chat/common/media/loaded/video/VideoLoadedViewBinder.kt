package com.morozov.feature_chat_impl.ui.fragments.chat.dialog.renderers.chat.common.media.loaded.video

import android.widget.ImageView
import com.github.vivchar.rendererrecyclerviewadapter.binder.ViewBinder
import com.github.vivchar.rendererrecyclerviewadapter.binder.ViewFinder
import com.morozov.feature_chat_impl.R
import com.morozov.feature_chat_impl.ui.fragments.chat.dialog.renderers.chat.common.media.OnMediaClicked
import com.morozov.feature_chat_impl.ui.fragments.chat.dialog.utils.loadImageScaledDown
import com.morozov.feature_chat_impl.ui.fragments.chat.dialog.utils.scaleDown
import com.social.solutions.android.util_audio_recording.formatMillis

class VideoLoadedViewBinder(private val callback: OnMediaClicked,
                            private val onLongClicked: () -> Unit): ViewBinder.Binder<VideoLoadedModel> {
    override fun bindView(model: VideoLoadedModel, finder: ViewFinder, payloads: MutableList<Any>) {
        val imageView = finder.find<ImageView>(R.id.imageMessage)
        model.editIsSingle(imageView)
        when {
            model.preview != null -> {
                imageView.setImageBitmap(model.preview!!.scaleDown())
            }
            else -> {
                imageView.loadImageScaledDown(model.url?.toString() ?: model.filePath ?: throw IllegalArgumentException(""), model)
            }
        }
        imageView.clipToOutline = true
        imageView.setOnClickListener {
            callback.onMediaClicked(model.position)
        }
        imageView.setOnLongClickListener {
            onLongClicked()
            return@setOnLongClickListener true
        }
        finder.setText(R.id.textVideoTime, (model.seconds*1000L).formatMillis())
    }
}
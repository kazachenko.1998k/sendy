package com.morozov.feature_chat_impl.ui.utility.on.message.click

import com.morozov.feature_chat_impl.ui.utility.ViewModelWithDate

interface OnMessageClickListener {
    fun onMessageClicked(item: ViewModelWithDate)
}
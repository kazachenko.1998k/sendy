package com.morozov.feature_chat_impl.ui.utility.edit.text.custom.selection

import android.view.ActionMode
import android.view.Menu
import android.view.MenuItem
import android.widget.EditText
import androidx.core.view.get
import com.morozov.feature_chat_impl.R
import com.morozov.feature_chat_impl.utility.*

fun EditText.setMyCustomSelection() {
    this.customSelectionActionModeCallback = object : ActionMode.Callback {

        val validResIdList = mutableListOf<Int>()

        override fun onCreateActionMode(mode: ActionMode?, menu: Menu?): Boolean {
            menu?:return false
            mode?:return false

            menu.clear()
            mode.menuInflater.inflate(R.menu.ns_text_menu, menu)

            for (i in 0 until menu.size()) {
                validResIdList.add(menu.getItem(i).itemId)
            }

            return true
        }

        override fun onPrepareActionMode(mode: ActionMode?, menu: Menu?): Boolean {

            menu?:return false
            mode?:return false

            val toRemoveResId = mutableListOf<Int>()

            for (i in 0 until menu.size()) {
                if (!validResIdList.contains(menu[i].itemId)) {
                    toRemoveResId.add(menu.getItem(i).itemId)
                } else {
                    formatItem(menu[i])
                }
            }

            for (resId in toRemoveResId) {
                menu.removeItem(resId)
            }

            return true
        }

        override fun onActionItemClicked(mode: ActionMode?, menuItem: MenuItem?): Boolean {
            menuItem?:return false
            mode?:return false

            this@setMyCustomSelection.setEditFormat(menuItem)

            return true
        }

        override fun onDestroyActionMode(mode: ActionMode?) {}

        private fun formatItem(menuItem: MenuItem) {
            menuItem.title = when (menuItem.itemId) {
                R.id.itemBold -> menuItem.title.toBold(resources)
                R.id.itemItalics -> menuItem.title.toItalic(resources)
                R.id.itemCrossed -> menuItem.title.toCrossed(resources)
                R.id.itemUnderlined -> menuItem.title.toUnderlined(resources)
                R.id.itemMonoFont -> menuItem.title.toMono(resources)
                else -> menuItem.title
            }
        }

        private fun EditText.setEditFormat(menuItem: MenuItem) {
            val selectionStart = this.selectionStart
            val selectionEnd = this.selectionEnd - 1

            val substring = this.text.toString().substring(IntRange(selectionStart, selectionEnd))
            val fullString = this.text.toString()

            val editedString = when (menuItem.itemId) {
                R.id.itemBold -> substring.getBold(resources)
                R.id.itemItalics -> substring.getItalic(resources)
                R.id.itemCrossed -> substring.getCrossed(resources)
                R.id.itemUnderlined -> substring.getUnderlined(resources)
                R.id.itemMonoFont -> substring.getMono(resources)
                else -> return
            }

            val replaceRange =
                fullString.replaceRange(IntRange(selectionStart, selectionEnd), editedString)

            this.setText(formatHtml(replaceRange))

            this.setSelection(selectionStart, selectionEnd+1)
        }
    }
}
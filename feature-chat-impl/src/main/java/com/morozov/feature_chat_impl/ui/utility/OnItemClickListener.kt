package com.morozov.feature_chat_impl.ui.utility

import android.view.View

interface OnItemClickListener {

    fun onItemClicked(view: View, position: Int)
}
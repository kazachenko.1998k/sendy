package com.morozov.feature_chat_impl.ui.fragments.chat.media.picker.bottom.sheet.renderers.photo

import android.content.Context
import com.morozov.feature_chat_impl.ui.fragments.chat.dialog.renderers.chat.common.media.photo.PhotoModel
import com.morozov.feature_chat_impl.ui.fragments.chat.media.picker.bottom.sheet.renderers.ViewModelSelectable

class PhotoMPModel(val filePath: String, val position: Int, selectedNumber: Int?) : ViewModelSelectable(selectedNumber) {
    fun convertToPhotoModel(context: Context, index: Int): PhotoModel {
        return PhotoModel(
            index,
            null,
            null,
            filePath,
            null
        )
    }
}
package com.morozov.feature_chat_impl.ui.utility.views.scroll.bottom

import android.content.Context
import android.util.AttributeSet
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.widget.FrameLayout
import androidx.core.view.isVisible
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.morozov.feature_chat_impl.R
import com.social.solutions.android.util_media_pager.utils.makeHideAnimToBottom
import com.social.solutions.android.util_media_pager.utils.makeShowAnimToBottom
import kotlinx.android.synthetic.main.view_scroll_to_bottom.view.*

class ScrollBottomView: FrameLayout {

    private var mNewMessagesAmount = 0

    constructor(context: Context) : this(context, null)
    constructor(context: Context, attrs: AttributeSet?) : this(context, attrs, 0)
    constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int) : super(context, attrs, defStyleAttr) {
        LayoutInflater.from(context).inflate(R.layout.view_scroll_to_bottom, this, true)
    }

    fun attachToRecycler(recyclerView: RecyclerView, click: (()->Unit) ?= null) {
        showNewMessagesAmount(0)

        recyclerView.addShowToBottomOnScrollFunc{ bool ->
            if (bool) {
                this.makeShowAnimToBottom()
            } else {
                showNewMessagesAmount(0)
                this.makeHideAnimToBottom()
            }
        }
        setOnClickListener {
            if (click != null)
                click()
            val currentItem = (recyclerView.layoutManager as LinearLayoutManager).findFirstVisibleItemPosition()
            Log.i("scrollBottomView", "Current item: $currentItem")
            if (currentItem > 20)
                recyclerView.scrollToPosition(17)
            recyclerView.smoothScrollToPosition(0)
        }
    }

    fun addNewMessage() {
        showNewMessagesAmount(mNewMessagesAmount+1)
    }

    fun showNewMessagesAmount(amount: Int) {
        Log.i("Jeka", "New amount: $amount")
        mNewMessagesAmount = amount
        setMessagesAmountVisibility(amount > 0)
        textNewMessagesCount.text = amount.toString()
    }

    private fun setMessagesAmountVisibility(bool: Boolean) {
        if (bool && textNewMessagesCount.isVisible.not()) {
            textNewMessagesCount.visibility = View.VISIBLE
        }
        else if (bool.not() && textNewMessagesCount.isVisible) {
            textNewMessagesCount.visibility = View.GONE
        }
    }
}
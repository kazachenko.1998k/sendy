package com.morozov.feature_chat_impl.ui.utility.views.answer.message

import com.morozov.feature_chat_impl.ui.utility.ViewModelWithDate

interface OnAnswerViewClickListener {
    fun onClick(item: ViewModelWithDate)
}
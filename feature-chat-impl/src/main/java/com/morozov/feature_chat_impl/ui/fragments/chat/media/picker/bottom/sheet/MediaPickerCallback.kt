package com.morozov.feature_chat_impl.ui.fragments.chat.media.picker.bottom.sheet

import android.net.Uri
import com.github.vivchar.rendererrecyclerviewadapter.ViewModel

interface MediaPickerCallback {
    fun onSelected(media: List<ViewModel>, text: String?)
    fun onFileSelected(uri: Uri)
    fun onExit()
}
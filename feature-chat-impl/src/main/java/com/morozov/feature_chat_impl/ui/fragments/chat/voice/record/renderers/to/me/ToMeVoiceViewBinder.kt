package com.morozov.feature_chat_impl.ui.fragments.chat.voice.record.renderers.to.me

import android.view.View
import android.widget.ImageView
import android.widget.ProgressBar
import android.widget.SeekBar
import android.widget.TextView
import com.github.vivchar.rendererrecyclerviewadapter.binder.ViewBinder
import com.github.vivchar.rendererrecyclerviewadapter.binder.ViewFinder
import com.mikhaellopez.circularprogressbar.CircularProgressBar
import com.morozov.feature_chat_impl.R
import com.morozov.feature_chat_impl.repository.DialogRepository
import com.morozov.feature_chat_impl.ui.fragments.chat.dialog.renderers.chat.common.voice.bindVoiceView
import com.morozov.feature_chat_impl.ui.fragments.chat.dialog.utils.OnErrorMessageStateClickListener
import com.morozov.feature_chat_impl.ui.fragments.chat.dialog.utils.OnProfileClickListener
import com.morozov.feature_chat_impl.ui.fragments.chat.dialog.utils.loadUserAvatar
import com.morozov.feature_chat_impl.ui.fragments.chat.select.messages.SelectMessageListener
import com.morozov.feature_chat_impl.ui.fragments.chat.voice.record.renderers.VoiceState
import com.morozov.feature_chat_impl.ui.fragments.chat.voice.record.utils.OnPlayVoiceCallback
import com.morozov.feature_chat_impl.ui.utility.ViewableMessageModel
import com.morozov.feature_chat_impl.ui.utility.on.message.click.OnMessageClickListener
import com.social.solutions.android.util_audio_recording.formatMillis
import java.text.SimpleDateFormat

class ToMeVoiceViewBinder(private val callback: OnPlayVoiceCallback,
                          private val onProfileClickListener: OnProfileClickListener,
                          private val selectCallback: SelectMessageListener,
                          private val onMessageClickListener: OnMessageClickListener,
                          private val onErrorMessageStateClickListener: OnErrorMessageStateClickListener
): ViewBinder.Binder<ToMeVoiceModel> {

    override fun bindView(model: ToMeVoiceModel, finder: ViewFinder, payloads: MutableList<Any>) {
        model.bindVoiceView(finder, payloads, selectCallback, onMessageClickListener, callback, onErrorMessageStateClickListener)

        val imageProfile = finder.find<ImageView>(R.id.imageProfile)
        val spaceView = finder.find<View>(R.id.spaceView)
        val margin16 = finder.find<View>(R.id.margin16)
        when {
            model.userIconUrl == null -> {
                imageProfile.visibility = View.GONE
                spaceView.visibility = View.GONE
                margin16.visibility = View.VISIBLE
            }
            model.isShow.not() -> {
                imageProfile.visibility = View.GONE
                spaceView.visibility = View.VISIBLE
                margin16.visibility = View.GONE
            }
            else -> {
                imageProfile.visibility = View.VISIBLE
                spaceView.visibility = View.VISIBLE
                margin16.visibility = View.GONE
                imageProfile.loadUserAvatar(model.userIconUrl)
                imageProfile.setOnClickListener {
                    onProfileClickListener.onClick(model.userId)
                }
            }
        }

        if (payloads.isEmpty())
            DialogRepository.sendView(model.messageId!!)
    }
}
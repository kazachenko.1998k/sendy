package com.morozov.feature_chat_impl.ui.fragments.chat.dialog.renderers.chat.common.voice

import android.view.View
import android.widget.ImageView
import android.widget.ProgressBar
import android.widget.SeekBar
import android.widget.TextView
import com.github.vivchar.rendererrecyclerviewadapter.binder.ViewFinder
import com.mikhaellopez.circularprogressbar.CircularProgressBar
import com.morozov.feature_chat_impl.R
import com.morozov.feature_chat_impl.ui.fragments.chat.dialog.renderers.chat.common.message.bindMessageView
import com.morozov.feature_chat_impl.ui.fragments.chat.dialog.renderers.chat.common.voice.CommonVoiceViewBinder.ACTION_UPDATE_PROGRESS
import com.morozov.feature_chat_impl.ui.fragments.chat.dialog.utils.OnErrorMessageStateClickListener
import com.morozov.feature_chat_impl.ui.fragments.chat.dialog.utils.SelectableModel
import com.morozov.feature_chat_impl.ui.fragments.chat.select.messages.SelectMessageListener
import com.morozov.feature_chat_impl.ui.fragments.chat.voice.record.renderers.VoiceState
import com.morozov.feature_chat_impl.ui.fragments.chat.voice.record.renderers.from.me.FromMeVoiceViewBinder
import com.morozov.feature_chat_impl.ui.fragments.chat.voice.record.utils.OnPlayVoiceCallback
import com.morozov.feature_chat_impl.ui.utility.ViewModelWithDate
import com.morozov.feature_chat_impl.ui.utility.ViewableMessageModel
import com.morozov.feature_chat_impl.ui.utility.on.message.click.OnMessageClickListener
import com.social.solutions.android.util_audio_recording.formatMillis
import java.text.SimpleDateFormat

object CommonVoiceViewBinder {
    const val ACTION_PLAY = 0
    const val ACTION_PAUSE = 1
    const val ACTION_STOP = 2
    const val ACTION_UPDATE_PROGRESS = 3
    const val ACTION_UPDATE_LOAD = 4
}

fun VoiceMessageModel.bindVoiceView(finder: ViewFinder,
                                    payloads: MutableList<Any>,
                                    selectCallback: SelectMessageListener,
                                    onMessageClickListener: OnMessageClickListener,
                                    callback: OnPlayVoiceCallback,
                                    onErrorMessageStateClickListener: OnErrorMessageStateClickListener) {
    bindMessageView(finder, payloads, selectCallback, onMessageClickListener, onErrorMessageStateClickListener)

    val imagePlay = finder.find<ImageView>(R.id.imagePlay)
    val imagePause = finder.find<ImageView>(R.id.imagePause)
    val progressBar = finder.find<SeekBar>(R.id.progressBar)
    val progressBack = finder.find<ProgressBar>(R.id.progressBack)
    val progressLoad = finder.find<CircularProgressBar>(R.id.progressLoad)

    val imageViewsNumber = finder.find<ImageView>(R.id.imageViewsNumber)
    val textViewsNumber = finder.find<TextView>(R.id.textViewsNumber)
    if (views == ViewableMessageModel.NO_VIEWABLE) {
        imageViewsNumber.visibility = View.GONE
        textViewsNumber.visibility = View.GONE
    } else {
        imageViewsNumber.visibility = View.VISIBLE
        textViewsNumber.visibility = View.VISIBLE
        textViewsNumber.text = views.toString()
    }

    when(state) {
        VoiceState.PAUSED -> {
            imagePause.visibility = View.GONE
            imagePlay.visibility = View.VISIBLE
            progressBar.isEnabled = true
        }
        VoiceState.STOPPED -> {
            imagePause.visibility = View.GONE
            imagePlay.visibility = View.VISIBLE
            progressBar.isEnabled = false
            finder.setText(R.id.textVoiceTime, (timeInSeconds * 1000L).formatMillis())
            progress = 0
            setProgress(progressBar, 0)
        }
        VoiceState.PLAYING -> {
            imagePause.visibility = View.VISIBLE
            imagePlay.visibility = View.GONE
            progressBar.isEnabled = true
        }
    }

    when {
        payloads.isEmpty() -> {
            finder.setText(R.id.timeMsg, SimpleDateFormat("HH:mm").format(date.time))

            finder.setText(R.id.textVoiceTime, ((timeInSeconds * 1000 - progress).toLong()).formatMillis())

            progressBar.max = timeInSeconds * 1000
            progressBack.max = timeInSeconds * 1000

            setProgress(progressBar, progress)

            val filePath = fileName ?: url ?: return

            imagePlay.setOnClickListener {
                if (loadProgress == 100f)
                    callback.onPlayVoice(filePath, userName, timeInSeconds * 1000)
            }
            imagePause.setOnClickListener {
                callback.onPauseVoice(filePath)
            }

            progressBar.setOnSeekBarChangeListener(object : SeekBar.OnSeekBarChangeListener {
                override fun onProgressChanged(seekBar: SeekBar?, p1: Int, p2: Boolean) {
                    seekBar?:return
                    progressBack.progress = seekBar.progress
                }

                override fun onStartTrackingTouch(p0: SeekBar?) {
                    callback.onPauseVoice(filePath)
                }

                override fun onStopTrackingTouch(seekBar: SeekBar?) {
                    seekBar?:return
                    callback.onPlayVoice(filePath, userName, timeInSeconds, seekBar.progress)
                }
            })
        }
        payloads[0] == ACTION_UPDATE_PROGRESS -> {
            finder.setText(R.id.textVoiceTime, ((timeInSeconds * 1000L - progress)).formatMillis())
            setProgress(progressBar, progress)
        }
    }

    progressLoad.progress = loadProgress
}

private fun setProgress(progressBar: SeekBar, progress: Int) {
    progressBar.progress = progress
}
package com.morozov.feature_chat_impl.ui.fragments.chat.dialog.renderers.chat.common.date

import com.morozov.feature_chat_impl.ui.utility.ViewModelWithDate
import java.util.*

class DateModel(date: Calendar): ViewModelWithDate(date)
package com.morozov.feature_chat_impl.ui.fragments.chat.user.action.bar

import com.morozov.feature_chat_impl.ui.fragments.chat.user.action.bar.api.UserProfileApi
import com.morozov.feature_chat_impl.ui.fragments.chat.user.action.bar.api.UserStateApi

interface UserActionBarApi: UserStateApi, UserProfileApi {
    fun showMute(isMute: Boolean)
}
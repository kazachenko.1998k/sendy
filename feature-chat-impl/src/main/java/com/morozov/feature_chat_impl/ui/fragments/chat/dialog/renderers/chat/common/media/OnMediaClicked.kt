package com.morozov.feature_chat_impl.ui.fragments.chat.dialog.renderers.chat.common.media

interface OnMediaClicked {
    fun onMediaClicked(position: Int)
    fun onUpdateItem(position: Int)
    fun onCancelClicked(position: Int)
}
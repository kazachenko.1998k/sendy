package com.morozov.feature_chat_impl.ui.fragments.chat.dialog.utils.cancel

import com.morozov.feature_chat_impl.ui.utility.ViewModelWithDate

interface CancelMessageCallback {
    fun onRemoveMessage(model: ViewModelWithDate)
}
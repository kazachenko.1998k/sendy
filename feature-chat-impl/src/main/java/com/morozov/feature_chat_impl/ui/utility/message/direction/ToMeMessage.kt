package com.morozov.feature_chat_impl.ui.utility.message.direction

import com.morozov.feature_chat_impl.ui.utility.message.MessageModel
import java.net.URL

interface ToMeMessage: MessageModel {

    companion object{
        const val NOTIFY_IS_SHOW = "notify_is_show"
    }

    // Null if dialog, has value if group chat
    val userIconUrl: URL?
    var isShow: Boolean
}
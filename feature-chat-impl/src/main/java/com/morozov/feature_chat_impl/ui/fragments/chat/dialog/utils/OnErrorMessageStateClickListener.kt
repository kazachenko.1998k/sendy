package com.morozov.feature_chat_impl.ui.fragments.chat.dialog.utils

import com.morozov.feature_chat_impl.ui.utility.ViewModelWithDate

interface OnErrorMessageStateClickListener {
    fun onClick(item: ViewModelWithDate)
}
package com.morozov.feature_chat_impl.ui.fragments.chat.dialog.renderers.chat.common.media.loaded.video

import android.graphics.Bitmap
import com.morozov.feature_chat_impl.ui.fragments.chat.dialog.renderers.chat.common.media.loaded.LoadedMediaModel
import com.morozov.feature_chat_impl.ui.fragments.chat.dialog.renderers.chat.common.media.video.VideoModel
import java.net.URL

class VideoLoadedModel(position: Int, url: URL?, filePath: String?, preview: Bitmap?, var seconds: Int): LoadedMediaModel(position, url, filePath, preview) {
    fun toVideo(): VideoModel {
        return VideoModel(
            position,
            url,
            filePath,
            0,
            preview,
            seconds
        ).also { it.isSingle = this.isSingle }
    }
}
package com.morozov.feature_chat_impl.ui.fragments.chat.dialog.renderers.chat.common.companion.media.and.text

import android.app.Activity
import android.graphics.drawable.ColorDrawable
import android.graphics.drawable.TransitionDrawable
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import androidx.annotation.DrawableRes
import androidx.core.view.get
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.github.vivchar.rendererrecyclerviewadapter.RendererRecyclerViewAdapter
import com.github.vivchar.rendererrecyclerviewadapter.binder.ViewBinder
import com.github.vivchar.rendererrecyclerviewadapter.binder.ViewFinder
import com.morozov.feature_chat_impl.R
import com.morozov.feature_chat_impl.start.MainObject
import com.morozov.feature_chat_impl.ui.fragments.chat.dialog.renderers.chat.common.media.OnMediaClicked
import com.morozov.feature_chat_impl.ui.fragments.chat.dialog.renderers.chat.common.media.loaded.photo.PhotoLoadedModel
import com.morozov.feature_chat_impl.ui.fragments.chat.dialog.renderers.chat.common.media.loaded.photo.PhotoLoadedViewBinder
import com.morozov.feature_chat_impl.ui.fragments.chat.dialog.renderers.chat.common.media.loaded.video.VideoLoadedModel
import com.morozov.feature_chat_impl.ui.fragments.chat.dialog.renderers.chat.common.media.loaded.video.VideoLoadedViewBinder
import com.morozov.feature_chat_impl.ui.fragments.chat.dialog.renderers.chat.common.media.photo.PhotoModel
import com.morozov.feature_chat_impl.ui.fragments.chat.dialog.renderers.chat.common.media.photo.PhotoViewBinder
import com.morozov.feature_chat_impl.ui.fragments.chat.dialog.renderers.chat.common.media.video.VideoModel
import com.morozov.feature_chat_impl.ui.fragments.chat.dialog.renderers.chat.common.media.video.VideoViewBinder
import com.morozov.feature_chat_impl.ui.fragments.chat.dialog.renderers.chat.common.message.bindMessageView
import com.morozov.feature_chat_impl.ui.fragments.chat.dialog.renderers.chat.from.me.compaign.media.and.text.FromMeMediaAndTextModel
import com.morozov.feature_chat_impl.ui.fragments.chat.dialog.renderers.chat.to.me.compaign.media.and.text.ToMeMediaAndTextModel
import com.morozov.feature_chat_impl.ui.fragments.chat.dialog.utils.OnErrorMessageStateClickListener
import com.morozov.feature_chat_impl.ui.fragments.chat.dialog.utils.cancel.CancelMessageCallback
import com.morozov.feature_chat_impl.ui.fragments.chat.select.messages.SelectMessageListener
import com.morozov.feature_chat_impl.ui.utility.ViewModelWithDate
import com.morozov.feature_chat_impl.ui.utility.ViewableMessageModel
import com.morozov.feature_chat_impl.ui.utility.initLongClick
import com.morozov.feature_chat_impl.ui.utility.on.message.click.OnMessageClickListener
import com.morozov.feature_chat_impl.ui.utility.views.answer.message.AnswerMessageView
import com.social.solutions.android.transition_media_pager.manager.MediaPagerManager
import com.social.solutions.android.util_media_pager.MediaMessageActivity
import com.social.solutions.android.util_media_pager.models.FileModel
import com.social.solutions.android.util_media_pager.models.MediaModel
import com.social.solutions.android.util_media_pager.models.MediaType
import kotlinx.coroutines.*
import java.text.SimpleDateFormat

fun MediaAndTextMessageModel.bindView(finder: ViewFinder,
                                      payloads: MutableList<Any>,
                                      activity: Activity,
                                      selectCallback: SelectMessageListener,
                                      cancelCallback: CancelMessageCallback,
                                      onMessageClickListener: OnMessageClickListener,
                                      onErrorMessageStateClickListener: OnErrorMessageStateClickListener) {
    bindMessageView(finder, payloads, selectCallback, onMessageClickListener, onErrorMessageStateClickListener)

    val backRes = if (media.isNotEmpty()) {
        this.media.first().isSingle = this.media.size == 1 && this.text.isEmpty() && this.answerViewConfigs == null
        if (this.media.first().isSingle) {
            finder.setBackgroundResource(R.id.linearInfo, R.drawable.rectangle_message_video_time)
            android.R.color.transparent
        } else {
            finder.setBackgroundResource(R.id.linearInfo, android.R.color.transparent)
            if (this is ToMeMediaAndTextModel)
                R.drawable.rectangle_chat_message_to
            else
                R.drawable.rectangle_chat_message_from
        }
    } else {
        finder.setBackgroundResource(R.id.linearInfo, android.R.color.transparent)
        if (this is ToMeMediaAndTextModel)
            R.drawable.rectangle_chat_message_to
        else
            R.drawable.rectangle_chat_message_from
    }
    finder.setBackgroundResource(R.id.linearBack, backRes)

    when {
        payloads.isEmpty() -> {
            val answerMessageView = finder.find<AnswerMessageView>(R.id.answerMessageView)

            // Init answer if exists
            if (answerViewConfigs != null) {
                answerMessageView.visibility = View.VISIBLE
                answerMessageView.setViewConfigs(answerViewConfigs!!)
                if (this is FromMeMediaAndTextModel)
                    answerMessageView.setLight()
                else
                    answerMessageView.setDark()
            } else
                answerMessageView.visibility = View.GONE

            val timeFromMeMsg = finder.find<TextView>(R.id.timeMsg)
            finder.setText(R.id.textMsg, this@bindView.text)

            val recyclerImages = finder.find<RecyclerView>(R.id.recyclerImages)
            val adapter = RendererRecyclerViewAdapter()
            adapter.enableDiffUtil()

            val callback = object :
                OnMediaClicked {
                override fun onCancelClicked(position: Int) {
                    recyclerImages.post {
                        this@bindView.media.removeAt(position)
                        if (this@bindView.media.isEmpty() && this@bindView.text.isEmpty())
                            cancelCallback.onRemoveMessage(this@bindView)
                        else
                            adapter.setItems(this@bindView.media)
                    }
                }

                override fun onMediaClicked(position: Int) {
                    val tmpList = mutableListOf<MediaModel>()
                    for (media in this@bindView.media) {
                        when (media) {
                            is PhotoLoadedModel ->
                                tmpList.add(MediaModel(MediaType.PHOTO, FileModel(media.url?.toString(), media.filePath)))
                            is VideoLoadedModel ->
                                tmpList.add(MediaModel(MediaType.VIDEO, FileModel(media.url?.toString(), media.filePath)))
                            else -> return
                        }
                    }

                    MediaPagerManager.with(timeFromMeMsg.context)
                        .setComment(this@bindView.text)
                        .setItems(tmpList)
                        .setStartPosition(position)
                        .setUserName(this@bindView.userName)
                        .setDate(date)
                        .setSharedItem(activity, recyclerImages[position], activity.applicationContext.resources.getString(R.string.shared_image_view))
                        .start()
                }

                override fun onUpdateItem(position: Int) {
                    recyclerImages.post {
                        val updatedMediaList = mutableListOf<com.morozov.feature_chat_impl.ui.fragments.chat.dialog.renderers.chat.common.media.MediaModel>()
                        for ((index, media) in this@bindView.media.withIndex()) {
                            if (index == position) {
                                when (media) {
                                    is PhotoModel -> {
                                        if (media.loadProgress == 100)
                                            updatedMediaList.add(media.toLoaded())
                                        else
                                            updatedMediaList.add(media)
                                    }
                                    is VideoModel -> {
                                        if (media.loadProgress == 100)
                                            updatedMediaList.add(media.toLoaded())
                                        else
                                            updatedMediaList.add(media)
                                    }
                                }
                            } else
                                updatedMediaList.add(media)
                        }
                        this@bindView.media = updatedMediaList
                        adapter.setItems(this@bindView.media)
                        adapter.notifyItemChanged(position)
                    }
                }
            }

            val onLongMediaClicked = {
                if (this.isSelected.not()) {
                    this.isSelected = true
                    val rootView = finder.find<View>(R.id.linearRoot)
                    rootView.setBackgroundColor(rootView.resources.getColor(R.color.on_long_click_color))
                    val imageMessageIsSelected = rootView.findViewById<ImageView>(R.id.imageMessageIsSelected)
                    imageMessageIsSelected.setImageDrawable(rootView.resources.getDrawable(R.drawable.ic_ellipse_selected_chat))
                    selectCallback.onSelected(true, this as ViewModelWithDate)
                }
            }

            adapter.registerRenderer(ViewBinder(R.layout.item_message_photo, PhotoModel::class.java,
                PhotoViewBinder(
                    callback
                )
            ))
            adapter.registerRenderer(ViewBinder(R.layout.item_message_video, VideoModel::class.java,
                VideoViewBinder(
                    callback
                )
            ))
            adapter.registerRenderer(ViewBinder(R.layout.item_message_photo_loaded, PhotoLoadedModel::class.java,
                PhotoLoadedViewBinder(
                    callback, onLongMediaClicked
                )
            ))
            adapter.registerRenderer(ViewBinder(R.layout.item_message_video_loaded, VideoLoadedModel::class.java,
                VideoLoadedViewBinder(
                    callback, onLongMediaClicked
                )
            ))
            adapter.setItems(this@bindView.media)

            recyclerImages.adapter = adapter
            recyclerImages.layoutManager = LinearLayoutManager(recyclerImages.context)
        }
        payloads[0] is Int -> {
            val adapter = finder.find<RecyclerView>(R.id.recyclerImages).adapter
            adapter?.notifyItemChanged(payloads[0] as Int, Any())
        }
    }
    val textMsg = finder.find<TextView>(R.id.textMsg)
    if (this@bindView.text.isEmpty() && this@bindView.media.isNotEmpty())
        textMsg.visibility = View.GONE
    else
        textMsg.visibility = View.VISIBLE
}
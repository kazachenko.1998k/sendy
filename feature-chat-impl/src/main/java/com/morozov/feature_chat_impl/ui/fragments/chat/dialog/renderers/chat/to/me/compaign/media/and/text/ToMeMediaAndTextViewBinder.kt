package com.morozov.feature_chat_impl.ui.fragments.chat.dialog.renderers.chat.to.me.compaign.media.and.text

import android.app.Activity
import android.view.View
import android.widget.ImageView
import androidx.annotation.ColorInt
import androidx.annotation.ColorRes
import com.github.vivchar.rendererrecyclerviewadapter.binder.ViewBinder
import com.github.vivchar.rendererrecyclerviewadapter.binder.ViewFinder
import com.morozov.feature_chat_impl.R
import com.morozov.feature_chat_impl.repository.DialogRepository
import com.morozov.feature_chat_impl.ui.fragments.chat.dialog.renderers.chat.common.companion.media.and.text.bindView
import com.morozov.feature_chat_impl.ui.fragments.chat.dialog.utils.OnErrorMessageStateClickListener
import com.morozov.feature_chat_impl.ui.fragments.chat.dialog.utils.OnProfileClickListener
import com.morozov.feature_chat_impl.ui.fragments.chat.dialog.utils.cancel.CancelMessageCallback
import com.morozov.feature_chat_impl.ui.fragments.chat.dialog.utils.loadUserAvatar
import com.morozov.feature_chat_impl.ui.fragments.chat.select.messages.SelectMessageListener
import com.morozov.feature_chat_impl.ui.utility.on.message.click.OnMessageClickListener

class ToMeMediaAndTextViewBinder(
    private val activity: Activity,
    private val onProfileClickListener: OnProfileClickListener,
    private val callback: SelectMessageListener,
    private val cancelCallback: CancelMessageCallback,
    private val onMessageClickListener: OnMessageClickListener,
    private val onErrorMessageStateClickListener: OnErrorMessageStateClickListener
): ViewBinder.Binder<ToMeMediaAndTextModel> {
    override fun bindView(model: ToMeMediaAndTextModel, finder: ViewFinder, payloads: MutableList<Any>) {
        if (payloads.isEmpty()) {
            DialogRepository.sendView(model.messageId!!)

            val imageProfile = finder.find<ImageView>(R.id.imageProfile)
            val spaceView = finder.find<View>(R.id.spaceView)
            val margin16 = finder.find<View>(R.id.margin16)
            when {
                model.userIconUrl == null -> {
                    imageProfile.visibility = View.GONE
                    spaceView.visibility = View.GONE
                    margin16.visibility = View.VISIBLE
                }
                model.isShow.not() -> {
                    imageProfile.visibility = View.GONE
                    spaceView.visibility = View.VISIBLE
                    margin16.visibility = View.GONE
                }
                else -> {
                    imageProfile.visibility = View.VISIBLE
                    spaceView.visibility = View.VISIBLE
                    margin16.visibility = View.GONE
                    imageProfile.loadUserAvatar(model.userIconUrl)
                    imageProfile.setOnClickListener {
                        onProfileClickListener.onClick(model.userId)
                    }
                }
            }
        }

        model.bindView(finder, payloads, activity, callback, cancelCallback, onMessageClickListener, onErrorMessageStateClickListener)

        @ColorInt val color = when {
            model.media.isEmpty() -> activity.resources.getColor(R.color.text_main)
            model.media.first().isSingle -> activity.resources.getColor(R.color.white)
            else -> activity.resources.getColor(R.color.text_main)
        }
        finder.setTextColor(R.id.textViewsNumber, color)
        finder.setTextColor(R.id.timeMsg, color)
    }
}
package com.morozov.feature_chat_impl.ui.fragments.chat.select.messages

import com.morozov.feature_chat_impl.ui.utility.ViewModelWithDate

interface SelectMessageListener {
    fun onSelected(isFromMe: Boolean, model: ViewModelWithDate)
    fun onNonSelected(model: ViewModelWithDate)

    fun isSelected(): Boolean
}
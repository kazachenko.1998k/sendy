package com.morozov.feature_chat_impl.ui.utility

import com.github.vivchar.rendererrecyclerviewadapter.ViewModel
import java.util.*

open class ViewModelWithDate(val date: Calendar): ViewModel{
    var isNeedMakeAnswerClickAnim = false
}
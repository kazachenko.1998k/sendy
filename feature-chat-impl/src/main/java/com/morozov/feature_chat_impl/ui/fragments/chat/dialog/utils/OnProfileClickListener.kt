package com.morozov.feature_chat_impl.ui.fragments.chat.dialog.utils

interface OnProfileClickListener {
    fun onClick(uid: Long)
}
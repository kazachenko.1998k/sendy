package com.morozov.feature_chat_impl.ui.fragments.chat.dialog.renderers.chat.common.media.loaded.photo

import android.widget.ImageView
import com.github.vivchar.rendererrecyclerviewadapter.binder.ViewBinder
import com.github.vivchar.rendererrecyclerviewadapter.binder.ViewFinder
import com.morozov.feature_chat_impl.R
import com.morozov.feature_chat_impl.ui.fragments.chat.dialog.renderers.chat.common.media.OnMediaClicked
import com.morozov.feature_chat_impl.ui.fragments.chat.dialog.utils.loadImageScaledDown
import com.morozov.feature_chat_impl.ui.fragments.chat.dialog.utils.scaleDown
import com.morozov.feature_chat_impl.ui.fragments.chat.select.messages.SelectMessageListener
import java.lang.IllegalArgumentException

class PhotoLoadedViewBinder(private val callback: OnMediaClicked,
                            private val onLongClicked: () -> Unit): ViewBinder.Binder<PhotoLoadedModel> {
    override fun bindView(model: PhotoLoadedModel, finder: ViewFinder, payloads: MutableList<Any>) {
        val imageView = finder.find<ImageView>(R.id.imageMessage)
        model.editIsSingle(imageView)
        when {
            model.preview != null -> imageView.setImageBitmap(model.preview!!.scaleDown())
            else -> {
                imageView.loadImageScaledDown(model.url?.toString() ?: model.filePath ?: throw IllegalArgumentException(""), model)
            }
        }
        imageView.clipToOutline = true
        imageView.setOnClickListener {
            callback.onMediaClicked(model.position)
        }
        imageView.setOnLongClickListener {
            onLongClicked()
            return@setOnLongClickListener true
        }
    }
}
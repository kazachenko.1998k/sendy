package com.morozov.feature_chat_impl.ui.fragments.chat.dialog.renderers.chat.common.media.video

import android.view.View
import android.widget.ImageView
import com.example.util_load_send_workers.repository.SendFileManager
import com.github.vivchar.rendererrecyclerviewadapter.binder.ViewBinder
import com.github.vivchar.rendererrecyclerviewadapter.binder.ViewFinder
import com.mikhaellopez.circularprogressbar.CircularProgressBar
import com.morozov.feature_chat_impl.R
import com.morozov.feature_chat_impl.ui.fragments.chat.dialog.renderers.chat.common.media.MediaModel
import com.morozov.feature_chat_impl.ui.fragments.chat.dialog.renderers.chat.common.media.OnMediaClicked
import com.morozov.feature_chat_impl.ui.fragments.chat.dialog.utils.loadImageScaledDown
import com.morozov.feature_chat_impl.ui.fragments.chat.dialog.utils.loadVideoScaledDown
import com.morozov.feature_chat_impl.ui.fragments.chat.dialog.utils.scaleDown
import com.social.solutions.android.util_audio_recording.formatMillis

class VideoViewBinder(private val callback: OnMediaClicked): ViewBinder.Binder<VideoModel> {
    companion object{
        private val sendedMedia = mutableSetOf<String>()
    }

    override fun bindView(model: VideoModel, finder: ViewFinder, payloads: MutableList<Any>) {
        val progressBar = finder.find<CircularProgressBar>(R.id.progressLoad)
        val imageView = finder.find<ImageView>(R.id.imageMessage)
        model.editIsSingle(imageView)
        when {
            payloads.isEmpty() -> {
                if (model.preview != null)
                    imageView.setImageBitmap(model.preview!!.scaleDown())

                finder.setText(R.id.textVideoTime, (model.seconds*1000L).formatMillis())

                finder.find<ImageView>(R.id.imageCancel).setOnClickListener {
                    if (model.filePath != null) {
                        SendFileManager.with(imageView.context)
                            .setFiles(listOf(model.filePath))
                            .cancelLoad()
//                        SendMediaService.cancelLoadingIfNotLoaded(model.filePath)
                    }
                    callback.onCancelClicked(model.position)
                }

                when {
                    model.url != null -> {
                        // TODO: Check local cache and load if exists
                        if (model.preview == null) {
                            imageView.loadVideoScaledDown(model.url.toString(), model, callback)
//                            loadVideo(imageView.context, model)
                        }
                    }
                    model.filePath != null -> {
                        if (model.preview == null) {
                            imageView.loadImageScaledDown(model.filePath, model, callback)
//                            if (model.loadProgress == null)
//                                sendVideo(imageView.context, model)
                        }
                    }
                }
                imageView.clipToOutline = true

                progressBar.progressMax = MediaModel.MEDIA_LOADED.toFloat()
                progressBar.progress = model.loadProgress?.toFloat() ?: 0f
            }
            model.loadProgress != null -> {
                progressBar.progress = model.loadProgress!!.toFloat()
                if (model.loadProgress == MediaModel.MEDIA_LOADED)
                    model.filePath?.let { sendedMedia.add(it) }
            }
        }

        if (model.loadProgress == MediaModel.MEDIA_LOADED){
            callback.onUpdateItem(model.position)
            progressBar.visibility = View.INVISIBLE
            finder.find<ImageView>(R.id.imageCancel).visibility = View.INVISIBLE
            finder.find<ImageView>(R.id.imagePlay).visibility = View.VISIBLE
        } else {
            finder.find<ImageView>(R.id.imagePlay).visibility = View.INVISIBLE
        }
    }

//    private fun sendVideo(context: Context, model: VideoModel) {
//        val disp = FileChatRepository.sendVideo(context, model.filePath!!, MainObject.myId!!).subscribe({
//            Log.i("Jeka", "Success")
//        }, {
//            it.printStackTrace()
//        })
//    }
//
//    private fun loadVideo(context: Context, model: VideoModel) {
//        val serviceIntent = Intent().apply {
//            putExtra(LoadMediaService.VIDEO_URL, model.url.toString())
//        }
//        JobIntentService.enqueueWork(
//            context, LoadMediaService::class.java,
//            LoadMediaService.LOAD_VIDEO_JOB_ID, serviceIntent
//        )
//    }
}
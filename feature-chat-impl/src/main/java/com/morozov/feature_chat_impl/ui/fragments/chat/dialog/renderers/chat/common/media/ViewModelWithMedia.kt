package com.morozov.feature_chat_impl.ui.fragments.chat.dialog.renderers.chat.common.media

import com.morozov.feature_chat_impl.ui.utility.ViewModelWithDate
import java.util.*

open class ViewModelWithMedia(var media: MutableList<MediaModel>, date: Calendar): ViewModelWithDate(date)
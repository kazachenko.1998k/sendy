package com.morozov.feature_chat_impl.ui.fragments.chat.select.messages

import android.content.ClipData
import android.content.ClipboardManager
import android.content.Context
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.core.view.isVisible
import androidx.recyclerview.widget.LinearLayoutManager
import com.morozov.feature_chat_impl.R
import com.morozov.feature_chat_impl.repository.DialogRepository
import com.morozov.feature_chat_impl.ui.fragments.chat.BaseChatFragment
import com.morozov.feature_chat_impl.ui.fragments.chat.dialog.renderers.chat.common.companion.media.and.text.MediaAndTextMessageModel
import com.morozov.feature_chat_impl.ui.fragments.chat.dialog.utils.SelectableModel
import com.morozov.feature_chat_impl.ui.fragments.chat.dialog.utils.removeItem
import com.morozov.feature_chat_impl.ui.utility.ViewModelWithDate
import com.morozov.feature_chat_impl.ui.utility.delete.showSureDeleteDialog
import com.morozov.feature_chat_impl.ui.utility.message.MessageModel
import com.morozov.feature_chat_impl.ui.utility.on.message.click.OnMessageClickListener
import com.morozov.feature_chat_impl.ui.utility.on.message.click.showOnMessageClickDialog
import com.morozov.feature_chat_impl.ui.utility.views.answer.message.AnswerViewConfigs
import com.morozov.feature_chat_impl.ui.utility.views.answer.message.OnAnswerViewClickListener
import com.social.solutions.android.util_media_pager.utils.makeHideAnimToBottom
import com.social.solutions.android.util_media_pager.utils.makeHideAnimToTop
import com.social.solutions.android.util_media_pager.utils.makeShowAnimToBottom
import com.social.solutions.android.util_media_pager.utils.makeShowAnimToTop
import kotlinx.android.synthetic.main.fragment_chat.*
import kotlinx.android.synthetic.main.item_selected_messages_footer.*
import kotlinx.android.synthetic.main.item_selected_messages_header.*
import kotlinx.android.synthetic.main.view_answer_attach_header.*

open class SelectMessagesChatFragment: BaseChatFragment(), SelectMessageListener, OnMessageClickListener {
    private var selectedCount = 0
    protected var mSelectedPositions = mutableListOf<ViewModelWithDate>()

    protected var mAnswerViewConfigs: AnswerViewConfigs? = null
    protected var mAnswerId: Long? = null

    companion object {
        var isSomethingSelected = false
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        answerMessageView.init()

        viewCloseAnswer.setOnClickListener {
            mAnswerId = null
            mAnswerViewConfigs = null
            linearAnswerAttachHeader.visibility = View.GONE
        }

        imageDelete.setOnClickListener {
            context?.showSureDeleteDialog(mSelectedPositions.size) { isForAll ->
                DialogRepository.deleteMessages(
                    mSelectedPositions.map { (it as MessageModel).messageId!! },
                    isForAll
                )
                for (modelWithDate in mSelectedPositions) {
                    mItems.removeItem(modelWithDate)
                }
                isSomethingSelected = false
                mRecyclerViewAdapter.setItems(mItems)
                mRecyclerViewAdapter.notifyItemRangeChanged(0, mItems.size, Any())
                mSelectedPositions.clear()
                animateHeaderHide()
                animateFooterHide()
            }
        }

        imageClose.setOnClickListener {
            isSomethingSelected = false
            for (modelWithDate in mSelectedPositions) {
                if (modelWithDate is SelectableModel && mItems.contains(modelWithDate)) {
                    val index = mItems.indexOf(modelWithDate)
                    (mItems[index] as SelectableModel).isSelected = false
                }
            }
            mRecyclerViewAdapter.notifyItemRangeChanged(0, mItems.size, Any())
            mSelectedPositions.clear()
            animateHeaderHide()
            animateFooterHide()
        }

        imageCopy.setOnClickListener {
            val item = mSelectedPositions.first()
            if (item is MediaAndTextMessageModel) {
                if (item.text.isEmpty())
                    return@setOnClickListener
                item.text.copyTextIntoClipboard()
            }
        }

        linearAnswer.setOnClickListener {
            val answerItem = mSelectedPositions.first()
            imageClose.callOnClick()
            makeAnswer(answerItem)
        }
    }

    override fun onSelected(isFromMe: Boolean, model: ViewModelWithDate) {
        if (isSelected().not()) {
            animateHeaderShow()
            animateFooterShow()
            isSomethingSelected = true
            mRecyclerViewAdapter.notifyItemRangeChanged(0, mItems.size, Any())
        } else if (linearAnswer.isVisible) {
            linearAnswer.visibility = View.INVISIBLE
            imageCopy.visibility = View.GONE
        }

        if (mItems.contains(model)) {
            val index = mItems.indexOf(model)
            val newItem = mItems[index]
            if (newItem is SelectableModel) {
                if ((model as SelectableModel).isSelected != newItem.isSelected) {
                    newItem.isSelected = (model as SelectableModel).isSelected
                    mRecyclerViewAdapter.setItems(mItems)
                }
            }
        }

        mSelectedPositions.add(model)

        textSelectedNumber.text = mSelectedPositions.size.toString()
    }

    override fun onNonSelected(model: ViewModelWithDate) {
        mSelectedPositions.remove(model)
        if (isSelected().not()) {
            animateHeaderHide()
            animateFooterHide()
            isSomethingSelected = false
            mRecyclerViewAdapter.notifyItemRangeChanged(0, mItems.size, Any())
        } else {
            if (mSelectedPositions.size == 1) {
                linearAnswer.visibility = View.VISIBLE
                imageCopy.visibility = View.VISIBLE
            }
            textSelectedNumber.text = mSelectedPositions.size.toString()
        }
    }

    override fun isSelected(): Boolean {
        return mSelectedPositions.isNotEmpty()
    }

    private fun animateHeaderShow() {
        selectMsgHeader.makeShowAnimToTop()
    }

    private fun animateFooterShow() {
        selectMsgFooter.makeShowAnimToBottom()
    }

    private fun animateHeaderHide() {
        selectMsgHeader.makeHideAnimToTop()
    }

    private fun animateFooterHide() {
        selectMsgFooter.makeHideAnimToBottom()
    }

    // OnMessageClickListener
    override fun onMessageClicked(item: ViewModelWithDate) {
        context!!.showOnMessageClickDialog({
            makeAnswer(item)
        },{
            if (item is MediaAndTextMessageModel) {
                if (item.text.isEmpty())
                    return@showOnMessageClickDialog
                item.text.copyTextIntoClipboard()
            }
        },{

        },{
            context!!.showSureDeleteDialog(1) { isForAll ->
                DialogRepository.deleteMessages(
                    listOf((item as MessageModel).messageId!!),
                    isForAll
                )
                mItems.remove(item)
                mRecyclerViewAdapter.setItems(mItems)
            }
        })
    }

    private fun makeAnswer(item: ViewModelWithDate) {
        mAnswerId = (item as MessageModel).messageId
        // TODO: open keyboard
        if (item is MediaAndTextMessageModel) {
            linearAnswerAttachHeader.visibility = View.VISIBLE
            val configs = AnswerViewConfigs.Builder()
                .setClickListener(onAnswerViewClickListener)
                .setAndParseMessageItem(item, context)
                .build()
            mAnswerViewConfigs = configs
            answerMessageView.setViewConfigs(configs)
        }
    }

    private fun String.copyTextIntoClipboard() {
        val clipboard = context!!.getSystemService(Context.CLIPBOARD_SERVICE) as ClipboardManager
        val clip = ClipData.newPlainText("Sendy text", this)
        clipboard.setPrimaryClip(clip)
        Toast.makeText(context, resources.getString(R.string.dialog_on_message_copied), Toast.LENGTH_SHORT).show()
    }

    protected val onAnswerViewClickListener = object : OnAnswerViewClickListener {
        override fun onClick(item: ViewModelWithDate) {
            mLastAnswerClickedItem = item
            showAnswerClickedModel()
        }
    }

    protected var mLastAnswerClickedItem: ViewModelWithDate? = null

    protected fun showAnswerClickedModel() {
        mLastAnswerClickedItem ?: return
        val index = mItems.indexOf(mLastAnswerClickedItem!!)
        val indexLastVisible = (recyclerChat.layoutManager as LinearLayoutManager).findLastCompletelyVisibleItemPosition()
        when {
            index == -1 -> {
                recyclerChat.smoothScrollToPosition(mItems.lastIndex)
            }
            indexLastVisible - index < 9 -> {
                recyclerChat.smoothScrollToPosition(index)
                makeAnswerClickAnim()
            }
            else -> {
                recyclerChat.scrollToPosition(index)
                makeAnswerClickAnim()
            }
        }
    }

    private fun makeAnswerClickAnim() {
        val itemRealPosition = mItems.indexOf(mLastAnswerClickedItem!!)
        if (itemRealPosition != -1) {
            val realItem = mItems[itemRealPosition]
            if (realItem is MediaAndTextMessageModel)
                realItem.isNeedMakeAnswerClickAnim = true
            mRecyclerViewAdapter.notifyItemChanged(itemRealPosition, Any())
        }
        mLastAnswerClickedItem = null
    }
}
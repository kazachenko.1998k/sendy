package com.morozov.feature_chat_impl.ui.fragments.chat.dialog.renderers.chat.common.message

import android.graphics.drawable.ColorDrawable
import android.graphics.drawable.TransitionDrawable
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import com.github.vivchar.rendererrecyclerviewadapter.binder.ViewFinder
import com.morozov.feature_chat_impl.R
import com.morozov.feature_chat_impl.ui.fragments.chat.dialog.utils.OnErrorMessageStateClickListener
import com.morozov.feature_chat_impl.ui.fragments.chat.dialog.utils.SelectableModel
import com.morozov.feature_chat_impl.ui.fragments.chat.select.messages.SelectMessageListener
import com.morozov.feature_chat_impl.ui.utility.ViewModelWithDate
import com.morozov.feature_chat_impl.ui.utility.ViewableMessageModel
import com.morozov.feature_chat_impl.ui.utility.initLongClick
import com.morozov.feature_chat_impl.ui.utility.message.direction.FromMeMessage
import com.morozov.feature_chat_impl.ui.utility.on.message.click.OnMessageClickListener
import kotlinx.coroutines.*
import java.text.SimpleDateFormat

fun <T> T.bindMessageView(finder: ViewFinder,
                          payloads: MutableList<Any>,
                          selectCallback: SelectMessageListener,
                          onMessageClickListener: OnMessageClickListener,
                          onErrorMessageStateClickListener: OnErrorMessageStateClickListener) where T: ViewModelWithDate,
                                                                                                    T: SelectableModel,
                                                                                                    T: ViewableMessageModel {
    // Long click
    val rootView = finder.find<View>(R.id.linearRoot)
    rootView.initLongClick(selectCallback, onMessageClickListener, this)

    // Message read status
    if (this is FromMeMessage) {
        finder.find<ImageView>(R.id.imageMessageReadStatus).setStatus(this.readStatus)
        if (this.readStatus == FromMeMessage.ReadStatus.ERROR) {
            finder.setOnClickListener(R.id.linearInfo) {
                onErrorMessageStateClickListener.onClick(this)
            }
        }
    }

    // Answer animation
    if (isNeedMakeAnswerClickAnim) {
        isNeedMakeAnswerClickAnim = false
        CoroutineScope(Dispatchers.Default).launch {
            val colorStart = rootView.resources.getColor(R.color.invisible)
            val colorEnd = rootView.resources.getColor(R.color.on_long_click_color)

            val colorDrawableStart = ColorDrawable(colorStart)
            val colorDrawableEnd = ColorDrawable(colorEnd)

            val transitionDrawable = TransitionDrawable(arrayOf(colorDrawableStart, colorDrawableEnd))

            withContext(Dispatchers.Main) {
                rootView.background = transitionDrawable
                transitionDrawable.startTransition(300)
            }
            delay(1300)
            withContext(Dispatchers.Main) {
                rootView.background = transitionDrawable
                transitionDrawable.reverseTransition(700)
            }
        }
    }

    when{
        payloads.isEmpty() -> {

            // Viewable model
            val imageViewsNumber = finder.find<ImageView>(R.id.imageViewsNumber)
            val textViewsNumber = finder.find<TextView>(R.id.textViewsNumber)
            if (this@bindMessageView.views == ViewableMessageModel.NO_VIEWABLE) {
                imageViewsNumber.visibility = View.GONE
                textViewsNumber.visibility = View.GONE
            } else {
                imageViewsNumber.visibility = View.VISIBLE
                textViewsNumber.visibility = View.VISIBLE
                textViewsNumber.text = this@bindMessageView.views.toString()
            }

            // Sent time
            finder.setText(R.id.timeMsg, SimpleDateFormat("HH:mm").format(this@bindMessageView.date.time))
        }
    }
}

private fun ImageView.setStatus(status: FromMeMessage.ReadStatus) {
    when(status) {
        FromMeMessage.ReadStatus.USER_READ -> {
            setImageResource(R.drawable.ic_read_chat)
        }
        FromMeMessage.ReadStatus.USER_RECEIVED -> {
            setImageResource(R.drawable.ic_user_received_chat)
        }
        FromMeMessage.ReadStatus.SENDING -> {
            setImageResource(R.drawable.ic_sending_chat)
        }
        FromMeMessage.ReadStatus.SENT -> {
            setImageResource(R.drawable.ic_sent_chat)
        }
        FromMeMessage.ReadStatus.ERROR -> {
            setImageResource(R.drawable.ic_error_chat)
        }
    }
}

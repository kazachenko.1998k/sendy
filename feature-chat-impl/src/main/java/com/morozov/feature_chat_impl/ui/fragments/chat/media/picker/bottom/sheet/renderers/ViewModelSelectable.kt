package com.morozov.feature_chat_impl.ui.fragments.chat.media.picker.bottom.sheet.renderers

import com.github.vivchar.rendererrecyclerviewadapter.ViewModel

open class ViewModelSelectable(var selectedNumber: Int?): ViewModel
package com.morozov.feature_chat_impl.ui.fragments.chat.dialog.renderers.chat.common.companion.media.and.text

import com.morozov.feature_chat_impl.ui.fragments.chat.dialog.renderers.chat.common.media.MediaModel
import com.morozov.feature_chat_impl.ui.fragments.chat.dialog.renderers.chat.common.media.ViewModelWithMedia
import com.morozov.feature_chat_impl.ui.fragments.chat.dialog.utils.SelectableModel
import com.morozov.feature_chat_impl.ui.utility.ViewableMessageModel
import com.morozov.feature_chat_impl.ui.utility.ViewableMessageModel.Companion.NO_VIEWABLE
import com.morozov.feature_chat_impl.ui.utility.message.MessageModel
import com.morozov.feature_chat_impl.ui.utility.views.answer.message.AnswerViewConfigs
import java.util.*

open class MediaAndTextMessageModel(override val userId: Long,
                                    override val userName: String,
                                    override var messageId: Long?,
                                    var answerViewConfigs: AnswerViewConfigs?,
                                    media: List<MediaModel>,
                                    val text: String,
                                    date: Calendar,
                                    override var isSelected: Boolean = false,
                                    override var views: Int = NO_VIEWABLE
) : ViewModelWithMedia(media.toMutableList(), date), SelectableModel, ViewableMessageModel, MessageModel {

    override fun equals(other: Any?): Boolean {
        if (other !is MediaAndTextMessageModel)
            return false
        if (messageId != null && messageId == other.messageId)
            return true
        var isMediaEquals = true
        if (media.size == other.media.size) {
            for (i in 0 until media.size) {
                if (media[i] != other.media[i]) {
                    isMediaEquals = false
                    break
                }
            }
        } else
            isMediaEquals = false
        return userId == other.userId
                && isMediaEquals
                && userName == other.userName
                && date == other.date
                && text == other.text
    }

    override fun hashCode(): Int {
        var result = userId.hashCode()
        result = 31 * result + userName.hashCode()
        result = 31 * result + text.hashCode()
        result = 31 * result + isSelected.hashCode()
        result = 31 * result + views
        return result
    }
}
package com.morozov.feature_chat_impl.ui.fragments.chat.voice.record

import android.content.*
import android.media.MediaRecorder
import android.os.Bundle
import android.os.IBinder
import android.util.Log
import android.view.View
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import com.github.vivchar.rendererrecyclerviewadapter.RendererRecyclerViewAdapter
import com.github.vivchar.rendererrecyclerviewadapter.binder.ViewBinder
import com.morozov.feature_chat_api.models.api.MediaApiModel
import com.morozov.feature_chat_api.models.api.MessageApiModel
import com.morozov.feature_chat_impl.R
import com.morozov.feature_chat_impl.start.MainObject
import com.morozov.feature_chat_impl.ui.fragments.chat.dialog.renderers.chat.common.media.MediaModel
import com.morozov.feature_chat_impl.ui.fragments.chat.dialog.renderers.chat.common.voice.CommonVoiceViewBinder
import com.morozov.feature_chat_impl.ui.fragments.chat.dialog.renderers.chat.common.voice.VoiceMessageModel
import com.morozov.feature_chat_impl.ui.fragments.chat.media.picker.ChatMediaPickerFragment
import com.morozov.feature_chat_impl.ui.fragments.chat.voice.record.renderers.VoiceState
import com.morozov.feature_chat_impl.ui.fragments.chat.voice.record.renderers.from.me.FromMeVoiceModel
import com.morozov.feature_chat_impl.ui.fragments.chat.voice.record.renderers.from.me.FromMeVoiceViewBinder
import com.morozov.feature_chat_impl.ui.fragments.chat.voice.record.renderers.to.me.ToMeVoiceModel
import com.morozov.feature_chat_impl.ui.fragments.chat.voice.record.renderers.to.me.ToMeVoiceViewBinder
import com.morozov.feature_chat_impl.ui.fragments.chat.voice.record.utils.OnPlayVoiceCallback
import com.social.solutions.android.util_audio_recording.AudioPlayerTopView
import com.social.solutions.android.util_audio_recording.AudioRecordView
import com.social.solutions.android.util_load_send_services.media.load.services.LoadMediaService
import com.social.solutions.android.util_load_send_services.voice.load.receivers.LoadVoiceCallback
import com.social.solutions.android.util_load_send_services.voice.load.receivers.LoadVoiceReceiver
import com.social.solutions.android.util_load_send_services.voice.player.VoicePlayModel
import com.social.solutions.android.util_load_send_services.voice.player.VoicePlayerService
import com.social.solutions.android.util_load_send_services.voice.send.receivers.SendVoiceCallback
import com.social.solutions.android.util_load_send_services.voice.send.receivers.SendVoiceReceiver
import com.social.solutions.android.util_load_send_services.voice.send.services.SendVoiceService
import kotlinx.android.synthetic.main.fragment_chat.*
import java.io.IOException
import java.lang.IllegalStateException
import java.util.*

open class VoiceRecordChatFragment: ChatMediaPickerFragment(), OnPlayVoiceCallback {

    companion object{
        private const val PLAY_VOICE_ID = 12
    }

    // Player service
    private val mConnection = object : ServiceConnection {
        override fun onServiceDisconnected(p0: ComponentName?) {
            mBinder = null
        }

        override fun onServiceConnected(className: ComponentName?, binder: IBinder?) {
            mBinder = binder as VoicePlayerService.VoicePlayerBinder
            mBinder?.getVoiceProgress()?.observe(this@VoiceRecordChatFragment, androidx.lifecycle.Observer { progress ->
                val voiceModel = mBinder?.getVoiceModel() ?: return@Observer

                when (progress) {
                    VoicePlayerService.START_TIMER_VALUE -> {
                        audioPlayerTop.setVoice(voiceModel.fileName, voiceModel.userName, voiceModel.time, true)
                    }
                    VoicePlayerService.PLAYING_FINISHED -> {
                        stopVoice(voiceModel.fileName)
                        audioPlayerTop.setTime(0)
                    }
                    else -> {
                        updateVoiceProgress(voiceModel.fileName, progress)
                        audioPlayerTop.setTime(progress)
                    }
                }
            })

            VoicePlayerService.mIsVoicePlaying.observe(this@VoiceRecordChatFragment, androidx.lifecycle.Observer {
                if (it)
                    audioPlayerTop.showPause()
                else if (VoicePlayerService.mIsVoicePause.value != true) {
                    audioPlayerTop.showPlay()
                }
            })
            VoicePlayerService.mIsVoicePause.observe(this@VoiceRecordChatFragment, androidx.lifecycle.Observer {
                if (it)
                    audioPlayerTop.showPlay()
            })
        }
    }
    private var mBinder: VoicePlayerService.VoicePlayerBinder? = null

    private var recorder: MediaRecorder? = null

    private var audioFileName: String = ""
    private lateinit var fileDir: String

    override fun onStart() {
        super.onStart()
        try {
            val intent = Intent(context, VoicePlayerService::class.java)
            activity?.startService(intent)
            activity?.bindService(intent, mConnection, Context.BIND_AUTO_CREATE)
        } catch (ex: IllegalStateException) {
            ex.printStackTrace()
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        registerSendReceivers()
        registerLoadReceivers()

        fileDir = activity?.externalCacheDir?.absolutePath.toString()

        mRecyclerViewAdapter.registerVoiceRenderers()

        initReordView()

        initTopAudioPlayer()
    }

    override fun onStop() {
        super.onStop()
        // Unbind voice player service
        if (mBinder != null) {
            activity?.unbindService(mConnection)
        }
        // Stop recorder
        recorder?.release()
        recorder = null
    }

    override fun onDestroy() {
        LocalBroadcastManager.getInstance(context!!).unregisterReceiver(sendVoiceReceiver)
        LocalBroadcastManager.getInstance(context!!).unregisterReceiver(loadVoiceReceiver)
        super.onDestroy()
    }

    private fun initTopAudioPlayer() {
        audioPlayerTop.callback = object : AudioPlayerTopView.AudioPlayerTopCallback {
            override fun onPause(fileName: String) {
                onPauseVoice(fileName)
            }

            override fun onPlay(fileName: String, userName: String, time: Int) {
                onPlayVoice(fileName, userName, time)
            }

            override fun onCancel() {
                stopLastVoiceIfPaused(null)
                stopLastVoiceIfPlaying(null)
            }
        }
    }

    private fun initReordView() {
        recordView.activity = activity!!
        recordView.callback = object : AudioRecordView.Callback {
            override fun onRecordStart(audio: Boolean) {
                onRecord(audio)
            }

            override fun isReady(): Boolean {
                return true
            }

            override fun onRecordEnd(time: Int) {
                onRecord(false)
                recordView
                sendMessage(FromMeVoiceModel(mUserId, MainObject.myUserName, null, null, audioFileName, 0f, time, 0, VoiceState.STOPPED, Calendar.getInstance()))
                //sendMessageCallback(listOf(VoiceApiModel(audioFileName, time)))
            }

            private fun sendMessageCallback(media: List<MediaApiModel>) {
                val recipient = MainObject.mRecipientId ?: throw IllegalArgumentException("No recipient id")
                MainObject.mCallback?.onSendMessage(recipient, MessageApiModel(media))
            }

            override fun onRecordCancel() {
                onRecord(false)
            }
        }
    }

    private fun RendererRecyclerViewAdapter.registerVoiceRenderers() {
        registerRenderer(ViewBinder(R.layout.item_from_me_voice_message, FromMeVoiceModel::class.java, FromMeVoiceViewBinder(this@VoiceRecordChatFragment, this@VoiceRecordChatFragment, this@VoiceRecordChatFragment, mErrorClickListener)))
        registerRenderer(ViewBinder(R.layout.item_to_me_voice_message, ToMeVoiceModel::class.java, ToMeVoiceViewBinder(this@VoiceRecordChatFragment, mProfileClickListener, this@VoiceRecordChatFragment, this@VoiceRecordChatFragment, mErrorClickListener)))
    }

    // Recording
    private fun onRecord(start: Boolean) = if (start) {
        startRecording()
    } else {
        stopRecording()
    }

    private fun startRecording() {
        val instance = Calendar.getInstance()
        audioFileName = "$fileDir/sendy_voice_${instance.timeInMillis}.aac"
        recorder = MediaRecorder().apply {
            setAudioSource(MediaRecorder.AudioSource.MIC)
            setOutputFormat(MediaRecorder.OutputFormat.AAC_ADTS)
            setOutputFile(audioFileName)
            setAudioEncoder(MediaRecorder.AudioEncoder.AAC)

            try {
                prepare()
                start()
            } catch (e: IOException) {
                e.printStackTrace()
            }
        }
    }

    private fun stopRecording() {
        recorder?.apply {
            stop()
            release()
        }
        recorder = null
    }

    // OnPlayVoiceCallback
    private var lastPlayedVoice: String? = null

    override fun onPlayVoice(fileName: String, userName: String, time: Int) {
        stopLastVoiceIfPlaying(fileName)
        stopLastVoiceIfPaused(fileName)
        playVoice(fileName, userName, time)
    }

    override fun onPlayVoice(fileName: String, userName: String, time: Int, startTime: Int) {
        continueVoice(fileName, startTime)
    }

    override fun onPauseVoice(fileName: String) {
        lastPlayedVoice = fileName
        pauseVoice(fileName)
    }

    private fun updateVoiceProgress(fileName: String, progress: Int) {
        val voicePosition = getVoicePosition(fileName) ?: return
        if (mItems[voicePosition] is FromMeVoiceModel)
            (mItems[voicePosition] as FromMeVoiceModel).progress = progress
        else if (mItems[voicePosition] is ToMeVoiceModel)
            (mItems[voicePosition] as ToMeVoiceModel).progress = progress
        mRecyclerViewAdapter.notifyItemChanged(voicePosition, CommonVoiceViewBinder.ACTION_UPDATE_PROGRESS)
    }

    private fun stopLastVoiceIfPlaying(fileName: String?) {
        if (VoicePlayerService.mIsVoicePlaying.value == true && lastPlayedVoice != fileName) {
            mBinder?.getService()?.stopVoice()

//            if (lastPlayedVoice != null) {
//                val oldVoicePosition = getVoicePosition(lastPlayedVoice!!) ?: return
//                (mItems[oldVoicePosition] as FromMeVoiceModel).state = VoiceState.STOPPED
//                mRecyclerViewAdapter.notifyItemChanged(oldVoicePosition, FromMeVoiceViewBinder.ACTION_STOP)
//            }
        }
    }

    private fun stopLastVoiceIfPaused(fileName: String?) {
        if (VoicePlayerService.mIsVoicePause.value == true && lastPlayedVoice != fileName) {
            mBinder?.getService()?.stopVoice()

//            if (lastPlayedVoice != null) {
//                val oldVoicePosition = getVoicePosition(lastPlayedVoice!!) ?: return
//                (mItems[oldVoicePosition] as FromMeVoiceModel).state = VoiceState.STOPPED
//                mRecyclerViewAdapter.notifyItemChanged(oldVoicePosition, FromMeVoiceViewBinder.ACTION_STOP)
//            }
        }
    }

    private fun pauseVoice(fileName: String) {
        if (VoicePlayerService.mIsVoicePlaying.value == true)
            mBinder?.getService()?.pauseVoice()
        val voicePosition = getVoicePosition(fileName) ?: return
        if (mItems[voicePosition] is FromMeVoiceModel)
            (mItems[voicePosition] as FromMeVoiceModel).state = VoiceState.PAUSED
        else if (mItems[voicePosition] is ToMeVoiceModel)
            (mItems[voicePosition] as ToMeVoiceModel).state = VoiceState.PAUSED
        mRecyclerViewAdapter.notifyItemChanged(voicePosition, CommonVoiceViewBinder.ACTION_PAUSE)
    }

    private fun stopVoice(fileName: String) {
        lastPlayedVoice = null
        val voicePosition = getVoicePosition(fileName) ?: return
        if (mItems[voicePosition] is FromMeVoiceModel)
            (mItems[voicePosition] as FromMeVoiceModel).state = VoiceState.STOPPED
        else if (mItems[voicePosition] is ToMeVoiceModel)
            (mItems[voicePosition] as ToMeVoiceModel).state = VoiceState.STOPPED
        mRecyclerViewAdapter.notifyItemChanged(voicePosition, CommonVoiceViewBinder.ACTION_STOP)
    }

    private fun playVoice(fileName: String, userName: String, time: Int) {
        val voicePosition = getVoicePosition(fileName) ?: return
        if (mItems[voicePosition] is FromMeVoiceModel)
            (mItems[voicePosition] as FromMeVoiceModel).state = VoiceState.PLAYING
        else if (mItems[voicePosition] is ToMeVoiceModel)
            (mItems[voicePosition] as ToMeVoiceModel).state = VoiceState.PLAYING
        mRecyclerViewAdapter.notifyItemChanged(voicePosition, CommonVoiceViewBinder.ACTION_PLAY)
        mBinder?.getService()?.startVoice(VoicePlayModel(userName, time, fileName))
        lastPlayedVoice = fileName
    }

    private fun continueVoice(fileName: String, startTime: Int) {
        val voicePosition = getVoicePosition(fileName) ?: return
        if (mItems[voicePosition] is FromMeVoiceModel)
            (mItems[voicePosition] as FromMeVoiceModel).state = VoiceState.PLAYING
        else if (mItems[voicePosition] is ToMeVoiceModel)
            (mItems[voicePosition] as ToMeVoiceModel).state = VoiceState.PLAYING
        mRecyclerViewAdapter.notifyItemChanged(voicePosition, CommonVoiceViewBinder.ACTION_PLAY)
        mBinder?.getService()?.continueVoice(startTime)
    }

    // Find item
    private fun getVoicePosition(fileName: String): Int? {
        val splitStr = "rid"
        val filePathRaw = fileName.split(splitStr).first()
        for ((index, mItem) in mItems.withIndex()) {
            if (mItem is VoiceMessageModel) {
                val filePathTmp = mItem.fileName ?: mItem.url!!
                val filePathTmpRaw = filePathTmp.split(splitStr).first()
                if (filePathRaw == filePathTmpRaw)
                    return index
            }
        }

        return null
    }

    //      Receivers
    private lateinit var sendVoiceReceiver: BroadcastReceiver
    private lateinit var loadVoiceReceiver: BroadcastReceiver

    private fun registerSendReceivers() {
        val statusVoiceIntentFilter = IntentFilter(SendVoiceService.BROADCAST_ACTION_VOICE)
        sendVoiceReceiver =
            SendVoiceReceiver(
                object :
                    SendVoiceCallback {
                    override fun onStatusUpdate(voicePath: String, progress: Int) {
                        sendProgressUpdate(voicePath, progress)
                    }

                    override fun onLoaded(voicePath: String) {
                        sendProgressUpdate(voicePath, MediaModel.MEDIA_LOADED)
                    }

                    private fun sendProgressUpdate(voicePath: String, progress: Int) {
                        for ((index, mItem) in mItems.withIndex()) {
                            if (mItem is FromMeVoiceModel) {
                                if (mItem.fileName == voicePath) {
                                    mItem.loadProgress = progress.toFloat()
                                    mRecyclerViewAdapter.notifyItemChanged(index, CommonVoiceViewBinder.ACTION_UPDATE_LOAD)
                                }
                            } else if (mItem is ToMeVoiceModel) {
                                if (mItem.fileName == voicePath) {
                                    mItem.loadProgress = progress.toFloat()
                                    mRecyclerViewAdapter.notifyItemChanged(index, CommonVoiceViewBinder.ACTION_UPDATE_LOAD)
                                }
                            }
                        }
                    }
                })
        LocalBroadcastManager.getInstance(context!!).registerReceiver(sendVoiceReceiver, statusVoiceIntentFilter)
    }

    private fun registerLoadReceivers() {
        val statusVoiceIntentFilter = IntentFilter(LoadMediaService.BROADCAST_ACTION_VIDEO)
        loadVoiceReceiver =
            LoadVoiceReceiver(
                object :
                    LoadVoiceCallback {
                    override fun onStatusUpdate(voiceUrl: String, filePath: String, progress: Int) {
                        sendProgressUpdate(voiceUrl, filePath, progress)
                    }

                    override fun onLoaded(voiceUrl: String, filePath: String) {
                        sendProgressUpdate(voiceUrl, filePath, MediaModel.MEDIA_LOADED)
                    }

                    private fun sendProgressUpdate(voiceUrl: String, filePath: String, progress: Int) {
                        for ((index, mItem) in mItems.withIndex()) {
                            if (mItem is FromMeVoiceModel) {
                                if (mItem.url == voiceUrl) {
                                    mItem.loadProgress = progress.toFloat()
                                    mItem.fileName = filePath
                                    mItem.url = null
                                    mRecyclerViewAdapter.notifyItemChanged(index, CommonVoiceViewBinder.ACTION_UPDATE_LOAD)
                                }
                            } else if (mItem is ToMeVoiceModel) {
                                if (mItem.url == voiceUrl) {
                                    mItem.loadProgress = progress.toFloat()
                                    mItem.fileName = filePath
                                    mItem.url = null
                                    mRecyclerViewAdapter.notifyItemChanged(index, CommonVoiceViewBinder.ACTION_UPDATE_LOAD)
                                }
                            }
                        }
                    }
                })
        LocalBroadcastManager.getInstance(context!!).registerReceiver(loadVoiceReceiver, statusVoiceIntentFilter)
    }
}
package com.morozov.feature_chat_impl.ui.utility.views.answer.message

import android.content.Context
import com.morozov.feature_chat_impl.R
import com.morozov.feature_chat_impl.ui.fragments.chat.dialog.renderers.chat.common.companion.media.and.text.MediaAndTextMessageModel
import com.morozov.feature_chat_impl.ui.fragments.chat.dialog.renderers.chat.common.media.loaded.photo.PhotoLoadedModel
import com.morozov.feature_chat_impl.ui.fragments.chat.dialog.renderers.chat.common.media.photo.PhotoModel
import com.morozov.feature_chat_impl.ui.utility.ViewModelWithDate

data class AnswerViewConfigs(val userName: String,
                             val userMessage: String,
                             val photoThumbnailPath: String?,
                             val clickListener: OnAnswerViewClickListener,
                             val messageItem: ViewModelWithDate) {

    class Builder {
        private var photoPath: String? = null
        private lateinit var name: String
        private var message = ""
        private var clickListener: OnAnswerViewClickListener? = null
        private lateinit var messageItem: ViewModelWithDate

        fun setPhotoThumbnailPath(photoPath: String, context: Context? = null): Builder {
            this.photoPath = photoPath
            if (message.isEmpty())
                message = context?.getString(R.string.answer_message_photo) ?: ""
            return this
        }

        fun setVideoThumbnailPath(videoPath: String, context: Context? = null): Builder {
            this.photoPath = videoPath
            if (message.isEmpty())
                message = context?.getString(R.string.answer_message_video) ?: ""
            return this
        }

        fun setUserName(name: String): Builder {
            this.name = name
            return this
        }

        fun setUserMessage(message: String): Builder {
            this.message = message
            return this
        }

        fun setClickListener(clickListener: OnAnswerViewClickListener): Builder {
            this.clickListener = clickListener
            return this
        }

        fun setMessageItem(item: ViewModelWithDate): Builder {
            this.messageItem = item
            return this
        }

        fun setAndParseMessageItem(item: ViewModelWithDate, context: Context?): Builder {
            setMessageItem(item)
            if (item is MediaAndTextMessageModel) {
                if (item.media.isNotEmpty()) {
                    val firstMedia = item.media.first()
                    if (firstMedia is PhotoModel || firstMedia is PhotoLoadedModel)
                        setPhotoThumbnailPath(firstMedia.filePath ?: firstMedia.url!!.toString(), context)
                    else
                        setVideoThumbnailPath(firstMedia.filePath ?: firstMedia.url!!.toString(), context)
                }
                setUserName(item.userName)
                if (item.text.isNotEmpty())
                    setUserMessage(item.text)
            }
            return this
        }

        fun build(): AnswerViewConfigs {
            return AnswerViewConfigs(
                name,
                message,
                photoPath,
                clickListener ?: DefaultAnswerClickListener(),
                messageItem
            )
        }

        private class DefaultAnswerClickListener: OnAnswerViewClickListener {
            override fun onClick(item: ViewModelWithDate) {}
        }
    }
}
package com.morozov.feature_chat_impl.ui.fragments.chat

import android.content.Context
import android.graphics.Bitmap
import android.os.Bundle
import android.text.Html
import android.util.Log
import android.view.LayoutInflater
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import android.widget.Toast
import androidx.activity.OnBackPressedCallback
import androidx.fragment.app.Fragment
import androidx.lifecycle.MutableLiveData
import com.bumptech.glide.Glide
import com.bumptech.glide.load.DecodeFormat
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.load.resource.bitmap.BitmapTransitionOptions
import com.bumptech.glide.load.resource.bitmap.BitmapTransitionOptions.withCrossFade
import com.bumptech.glide.request.RequestOptions
import com.github.vivchar.rendererrecyclerviewadapter.RendererRecyclerViewAdapter
import com.morozov.core_backend_api.chat.Chat
import com.morozov.feature_chat_impl.R
import com.morozov.feature_chat_impl.repository.GeneralRepository
import com.morozov.feature_chat_impl.start.MainObject
import com.morozov.feature_chat_impl.ui.fragments.chat.dialog.renderers.chat.common.media.loaded.photo.PhotoLoadedModel
import com.morozov.feature_chat_impl.ui.fragments.chat.dialog.renderers.chat.common.media.loaded.video.VideoLoadedModel
import com.morozov.feature_chat_impl.ui.fragments.chat.dialog.renderers.chat.common.media.photo.PhotoModel
import com.morozov.feature_chat_impl.ui.fragments.chat.dialog.renderers.chat.common.media.video.VideoModel
import com.morozov.feature_chat_impl.ui.fragments.chat.dialog.renderers.chat.from.me.compaign.media.and.text.FromMeMediaAndTextModel
import com.morozov.feature_chat_impl.ui.fragments.chat.dialog.utils.OnErrorMessageStateClickListener
import com.morozov.feature_chat_impl.ui.fragments.chat.dialog.utils.OnProfileClickListener
import com.morozov.feature_chat_impl.ui.fragments.chat.select.messages.SelectMessagesChatFragment
import com.morozov.feature_chat_impl.ui.fragments.chat.user.action.bar.*
import com.morozov.feature_chat_impl.ui.utility.OnBackPressedFragment
import com.morozov.feature_chat_impl.ui.utility.ViewModelWithDate
import com.morozov.feature_chat_impl.ui.utility.delete.showSendAgainDialog
import com.morozov.feature_chat_impl.ui.utility.message.direction.FromMeMessage
import com.morozov.feature_chat_impl.ui.utility.views.chanels.ChanelFooterSettingsCallback
import com.social.solutions.android.util_media_pager.utils.makeHideAnimToTop
import com.social.solutions.android.util_media_pager.utils.makeShowAnimToTop
import kotlinx.android.synthetic.main.dialog_menu_chat.*
import kotlinx.android.synthetic.main.fragment_chat.*
import java.net.URL
import java.util.*

open class BaseChatFragment: Fragment(), UserActionBarApi {
    companion object{
        const val TAG = "ChatFragment_TAG"

        // Mock
        var isNotificationsEnable = true
    }

    protected val mNewMessages = mutableListOf<Long>()

    protected var mChatModel: Chat? = null

    protected var mStartMessage = 0

    // Dialog recycler
    protected lateinit var mRecyclerViewAdapter: RendererRecyclerViewAdapter
    protected val mItems = mutableListOf<ViewModelWithDate>()

    private var mIsModeVisible = false

    var mUserId = 1L

    private var mSavedUrl: URL? = null
    private var mSavedName: String? = null
    private var mSavedStatus: String? = null

    protected val mProfileClickListener = object : OnProfileClickListener {
        override fun onClick(uid: Long) {
            val chatModel = Chat()
            chatModel.dialogUid = uid
            chatModel.type = Chat.PERSONAL_DIALOG
            MainObject.mCallback?.onProfileClicked(chatModel)
        }
    }

    protected val mSendAgainLiveData = MutableLiveData<ViewModelWithDate>()

    protected val mErrorClickListener = object : OnErrorMessageStateClickListener {
        override fun onClick(item: ViewModelWithDate) {
            context?.showSendAgainDialog({
                if (item is FromMeMessage)
                    item.readStatus = FromMeMessage.ReadStatus.SENDING
                if (item is FromMeMediaAndTextModel) {
                    item.media = item.media.map {
                        val newModel = when (it) {
                            is PhotoLoadedModel -> it.toPhoto()
                            is PhotoModel, is VideoModel -> it
                            else -> (it as VideoLoadedModel).toVideo()
                        }
                        newModel.loadProgress = 0
                        newModel
                    }.toMutableList()
                }
                mItems.remove(item)
                mRecyclerViewAdapter.setItems(mItems)
                mSendAgainLiveData.value = item
            },{
                mItems.remove(item)
                mRecyclerViewAdapter.setItems(mItems)
            })
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? =
        inflater.inflate(R.layout.fragment_chat, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        scrollBottomView.attachToRecycler(recyclerChat) {
            mNewMessages.clear()
            ChatFragment.mSentMessage.postValue(0)
        }

        buttonBack.setOnClickListener {
            activity?.onBackPressed()
        }

        linear_profile_info.setOnClickListener {
            mChatModel?.let { it1 ->
                if (MainObject.mChatType == Chat.PERSONAL_DIALOG)
                    mProfileClickListener.onClick(MainObject.mUserId ?: MainObject.myId?: 0L)
                else
                    MainObject.mCallback?.onProfileClicked(it1)
            }
        }

        initModeDialog()
    }

    override fun onDestroy() {
        MainObject.mCallback?.onDestroy()
        super.onDestroy()
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        activity?.onBackPressedDispatcher?.addCallback(this, object : OnBackPressedCallback(true) {
            override fun handleOnBackPressed() {
                val stackEntryCount = childFragmentManager.backStackEntryCount
                if (stackEntryCount == 0) {
                    onActivityBackPress()
                } else {
                    when (val fragment = childFragmentManager.fragments.last()) {
                        is OnBackPressedFragment -> (fragment as OnBackPressedFragment).onBackPressed()
                        else -> childFragmentManager.popBackStack()
                    }
                }
            }
        })
    }

    private fun OnBackPressedCallback.onActivityBackPress() {
        SelectMessagesChatFragment.isSomethingSelected = false
        this.isEnabled = false
        activity?.onBackPressed()
    }

    // Init
    private fun initModeDialog() {
        buttonMore.setOnClickListener {
            if (mIsModeVisible.not()) {
               showMenuChat()
            }
        }
        menuBack.setOnTouchListener { _, _ ->
            if (mIsModeVisible) {
                hideMenuChat()
                return@setOnTouchListener true
            }
            return@setOnTouchListener false
        }

        linearSearch.setOnClickListener {
            hideMenuChat()
        }
        linearEnableNotifications.setOnClickListener {
            hideMenuChat()

            if (isNotificationsEnable){
                showMute(true)
                GeneralRepository.enableNotificationsUser(false)
            } else {
                showMute(false)
                GeneralRepository.enableNotificationsUser(true)
            }
        }
        linearClearStory.setOnClickListener {
            hideMenuChat()
            // TODO: clear cache
            mItems.clear()
            mRecyclerViewAdapter.setItems(mItems)
            GeneralRepository.clearChat()
        }
        linearDeleteChat.setOnClickListener {
            hideMenuChat()
            activity?.onBackPressed()
            GeneralRepository.deleteChat()
        }
    }

    fun hideMenuChat() {
        cardMore.makeHideAnimToTop()
        mIsModeVisible = false
    }
    private fun showMenuChat() {
        cardMore.makeShowAnimToTop()
        mIsModeVisible = true
    }

    // UserActionBarApi
    fun restoreUserInfo() {
        if (mSavedName != null
            && mSavedStatus != null
            && mSavedUrl != null) {
            textUserState.setText(Html.fromHtml(mSavedStatus), TextView.BufferType.SPANNABLE)
            showUserName(mSavedName!!)
            showAvatar(mSavedUrl!!)
        }
    }

    fun initFooter() {
        chanelFooter.visibility = View.VISIBLE
        chanelFooter.mCallback = footerCallback
        constraintMessage.visibility = View.GONE
        buttonMore.visibility = View.GONE
    }

    override fun showWasOnline(isMale: Boolean, date: Calendar) {
        mSavedStatus = textUserState.showWasOnline(isMale, date)
    }

    override fun showOnline() {
        mSavedStatus = textUserState.showOnline()
    }

    override fun showChanelSubs(count: Int) {
        mSavedStatus = textUserState.showChanelSubs(count)
    }

    override fun showGroupUsers(count: Int) {
        mSavedStatus = textUserState.showGroupUsers(count)
    }

    override fun showAvatar(url: URL) {
        mSavedUrl = url
        Log.i("Jeka", "URL: $url")
        val requestOptions = RequestOptions().diskCacheStrategy(DiskCacheStrategy.ALL).circleCrop().format(DecodeFormat.PREFER_RGB_565)
        val image = imageProfile.drawable
        Glide.with(imageProfile)
            .asBitmap()
            .load(url)
            .transition(withCrossFade())
            .thumbnail(0.01f)
            .placeholder(image)
            .centerCrop()
            .apply(requestOptions)
            .into(imageProfile)
    }

    override fun showUserName(name: String) {
        mSavedName = name
        Log.i("Jeka", "Name: $name")
        textUserName?.text = name
        textUserName?.isSelected = true
    }

    override fun showMute(isMute: Boolean) {
        imageMute.visibility = if (isMute) View.VISIBLE else View.INVISIBLE
        if (isMute){
            textEnableNotif.text = resources.getString(R.string.enable_notifications)
            isNotificationsEnable = false
        } else {
            textEnableNotif.text = resources.getString(R.string.disable_notifications)
            isNotificationsEnable = true
        }
    }

    override fun showNoInternet() {
        textUserState.text = resources.getString(R.string.wait_for_network)
    }

    private val footerCallback by lazy {
        object :
            ChanelFooterSettingsCallback {
            override fun onSubscribe() {
                chanelFooter.showProgress()
                val disp = GeneralRepository.joinChat().subscribe({
                    chanelFooter.showDisableNotif()
                },{
                    it.printStackTrace()
                })
            }

            override fun onDisableNotif() {
                showMute(true)
                GeneralRepository.enableNotificationsUser(false)
                chanelFooter.showEnableNotif()
            }

            override fun onEnableNotif() {
                showMute(false)
                GeneralRepository.enableNotificationsUser(true)
                chanelFooter.showDisableNotif()
            }

            override fun onDiscussDisableNotif() {

            }

            override fun onDiscussEnableNotif() {

            }

            override fun onDiscuss() {

            }

            override fun onJoinChat() {
                chanelFooter.showProgress()
                val disp = GeneralRepository.joinChat().subscribe({
                    chanelFooter.visibility = View.GONE
                    constraintMessage.visibility = View.VISIBLE
                    buttonMore.visibility = View.VISIBLE
                },{
                    it.printStackTrace()
                })
            }
        }
    }
}
package com.morozov.feature_chat_impl.ui.fragments.chat.dialog.renderers.chat.common.media

import android.graphics.Bitmap
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import androidx.constraintlayout.widget.ConstraintLayout
import com.github.vivchar.rendererrecyclerviewadapter.ViewModel
import java.net.URL

open class MediaModel(val position: Int,
                      val url: URL?, val filePath: String?,
                      var loadProgress: Int?,
                      var preview: Bitmap?): ViewModel {
    var isSingle = true

    var width: Int = 20
    var height: Int = 20

    fun editIsSingle(image: ImageView) {
        val params = image.layoutParams
        when (params) {
            is ConstraintLayout.LayoutParams -> {
                if (isSingle)
                    params.setMargins(0,0,0,0)
                else
                    params.setMargins(2,2,2,2)
            }
            is LinearLayout.LayoutParams -> {
                if (isSingle)
                    params.setMargins(0,0,0,0)
                else
                    params.setMargins(2,2,2,2)
            }
        }
        image.layoutParams = params
    }

    companion object{
        const val MEDIA_LOADED = 100
    }

    override fun equals(other: Any?): Boolean {
        if (other !is MediaModel)
            return false
        return url == other.url
                || preview == other.preview
                || filePath == other.filePath
    }

    override fun hashCode(): Int {
        var result = position
        result = 31 * result + (url?.hashCode() ?: 0)
        result = 31 * result + (filePath?.hashCode() ?: 0)
        result = 31 * result + (loadProgress ?: 0)
        result = 31 * result + (preview?.hashCode() ?: 0)
        return result
    }
}

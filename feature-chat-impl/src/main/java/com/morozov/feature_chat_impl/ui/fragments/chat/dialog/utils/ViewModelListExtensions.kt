package com.morozov.feature_chat_impl.ui.fragments.chat.dialog.utils

import android.util.Log
import com.morozov.core_backend_api.chat.Chat
import com.morozov.feature_chat_impl.start.MainObject
import com.morozov.feature_chat_impl.ui.fragments.chat.dialog.renderers.chat.common.date.DateModel
import com.morozov.feature_chat_impl.ui.utility.ViewModelWithDate
import com.morozov.feature_chat_impl.ui.utility.ViewableMessageModel
import com.morozov.feature_chat_impl.ui.utility.message.direction.ToMeMessage
import java.text.SimpleDateFormat
import java.util.*

fun MutableList<ViewModelWithDate>.addItemLast(item: ViewModelWithDate) {
    if (isEmpty()) {
        add(item)
        add(
            DateModel(
                item.date
            )
        )
        return
    }

    if (this.contains(item)) {
        val index = this.indexOf(item)
        val atIndex = get(index)
        if(atIndex is ToMeMessage && item is ToMeMessage) {
            if (item.userId == atIndex.userId) {
                if (atIndex.userIconUrl != null && item.userIconUrl != null) {
                    item.isShow = atIndex.isShow
                }
            }
        }
        this.removeAt(index)
        this.add(index, item)
        return
    }

    val lastModel = last()

    when(lastModel) {
        is DateModel -> {
            if (compareIfDatesFromOneDay(lastModel.date, item.date))
                remove(lastModel)
        }
    }
    if (isNotEmpty()) {
        val lastModelAfterRemove = last()
        if(lastModelAfterRemove is ToMeMessage && item is ToMeMessage) {
            if (item.userId == lastModelAfterRemove.userId) {
                if (lastModelAfterRemove.userIconUrl != null && item.userIconUrl != null) {
                    item.isShow = false
                }
            }
        }
    }
    item.addViewsIfChannel()
    add(item)
    add(
        DateModel(
            item.date
        )
    )
}

fun MutableList<ViewModelWithDate>.addByTime(item: ViewModelWithDate) {
    if (isEmpty()) {
        add(0,
            DateModel(
                item.date
            )
        )
        add(0, item)
        return
    }

    if (this.contains(item)) {
        val index = this.indexOf(item)
        val atIndex = get(index)
        if(atIndex is ToMeMessage && item is ToMeMessage) {
            if (item.userId == atIndex.userId) {
                if (atIndex.userIconUrl != null && item.userIconUrl != null) {
                    item.isShow = atIndex.isShow
                }
            }
        }
        this.removeAt(index)
        this.add(index, item)
        return
    }

    var firstModel = first()
    var index = 0
    while (index < lastIndex) {
        if (get(index).date.timeInMillis < item.date.timeInMillis)
            break
        index++
        firstModel = get(index)
    }

    if (compareIfDatesFromOneDay(firstModel.date, item.date).not())
        add(index,
            DateModel(
                item.date
            )
        )
    else {
        if(firstModel is ToMeMessage && item is ToMeMessage) {
            if (item.userId == firstModel.userId) {
                if (firstModel.userIconUrl != null && item.userIconUrl != null) {
                    firstModel.isShow = false
                    item.isShow = true
                }
            }
        }
    }
    item.addViewsIfChannel()
    add(index, item)

    if (index > 0 )
        if (get(index-1) is DateModel && compareIfDatesFromOneDay(get(index-1).date, item.date))
            removeAt(index-1)
}

fun MutableList<ViewModelWithDate>.addItemFirst(item: ViewModelWithDate) {
    if (isEmpty()) {
        add(0,
            DateModel(
                item.date
            )
        )
        add(0, item)
        return
    }

    if (this.contains(item)) {
        val index = this.indexOf(item)
        val atIndex = get(index)
        if(atIndex is ToMeMessage && item is ToMeMessage) {
            if (item.userId == atIndex.userId) {
                if (atIndex.userIconUrl != null && item.userIconUrl != null) {
                    item.isShow = atIndex.isShow
                }
            }
        }
        this.removeAt(index)
        this.add(index, item)
        return
    }

    val firstModel = first()

    if (compareIfDatesFromOneDay(firstModel.date, item.date).not())
        add(0,
            DateModel(
                item.date
            )
        )
    else {
        if(firstModel is ToMeMessage && item is ToMeMessage) {
            if (item.userId == firstModel.userId) {
                if (firstModel.userIconUrl != null && item.userIconUrl != null) {
                    firstModel.isShow = false
                    item.isShow = true
                }
            }
        }
    }
    item.addViewsIfChannel()
    add(0, item)
}

private fun ViewModelWithDate.addViewsIfChannel() {
    if (MainObject.mChatType == Chat.CHANNEL)
        if (this is ViewableMessageModel)
            if (this.views == ViewableMessageModel.NO_VIEWABLE)
                this.views = 0
}

fun MutableList<ViewModelWithDate>.removeItem(item: ViewModelWithDate) {
    if (isEmpty())
        return
    val deletePosition = indexOf(item)
    val nextOfDeletePosition = deletePosition+1
    val prevOfDeletePosition = deletePosition-1

    if (nextOfDeletePosition < size) {
        val nextOfDeleteItem = get(nextOfDeletePosition)
        val prevOfDeleteItem = if (prevOfDeletePosition >= 0)
            get(prevOfDeletePosition)
        else
            null

        val bool1 = nextOfDeleteItem is DateModel
        val bool2 = if (prevOfDeleteItem != null)
            compareIfDatesFromOneDay(prevOfDeleteItem.date, item.date).not()
        else
            true

        if (bool1 && bool2)
                remove(nextOfDeleteItem)
    }
    remove(item)
}

private fun compareIfDatesFromOneDay(date1: Calendar, date2: Calendar): Boolean {
    val formater = SimpleDateFormat("ddMMyy")
    return formater.format(date1.time) == formater.format(date2.time)
}
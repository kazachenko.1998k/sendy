package com.morozov.feature_chat_impl.ui.fragments.chat.media.picker.bottom.sheet

import android.Manifest
import android.annotation.SuppressLint
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Bundle
import android.util.Log
import android.util.Range
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.Toast
import androidx.core.content.ContextCompat
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.recyclerview.widget.GridLayoutManager
import com.example.util_camera.CameraActivity
import com.example.util_camera.CameraCallback
import com.github.vivchar.rendererrecyclerviewadapter.RendererRecyclerViewAdapter
import com.github.vivchar.rendererrecyclerviewadapter.ViewModel
import com.github.vivchar.rendererrecyclerviewadapter.binder.ViewBinder
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.morozov.feature_chat_impl.R
import com.morozov.feature_chat_impl.ui.fragments.chat.media.picker.bottom.sheet.interaction.MediaInnerAdapterCallback
import com.morozov.feature_chat_impl.ui.fragments.chat.media.picker.bottom.sheet.renderers.ViewModelSelectable
import com.morozov.feature_chat_impl.ui.fragments.chat.media.picker.bottom.sheet.renderers.first.FirstMPModel
import com.morozov.feature_chat_impl.ui.fragments.chat.media.picker.bottom.sheet.renderers.first.FirstMPViewBinder
import com.morozov.feature_chat_impl.ui.fragments.chat.media.picker.bottom.sheet.renderers.photo.PhotoMPModel
import com.morozov.feature_chat_impl.ui.fragments.chat.media.picker.bottom.sheet.renderers.photo.PhotoMPViewBinder
import com.morozov.feature_chat_impl.ui.fragments.chat.media.picker.bottom.sheet.renderers.video.VideoMPModel
import com.morozov.feature_chat_impl.ui.fragments.chat.media.picker.bottom.sheet.renderers.video.VideoMPViewBinder
import com.morozov.feature_chat_impl.ui.fragments.chat.media.picker.bottom.sheet.utils.animateHide
import com.morozov.feature_chat_impl.ui.fragments.chat.media.picker.bottom.sheet.utils.animateShow
import com.morozov.feature_chat_impl.ui.fragments.chat.media.picker.bottom.sheet.utils.getMediaCursor
import com.morozov.feature_chat_impl.ui.fragments.chat.media.picker.bottom.sheet.utils.getMediaModels
import com.morozov.feature_chat_impl.ui.utility.OnBackPressedFragment
import com.morozov.feature_chat_impl.utility.hideKeyboard
import com.social.solutions.android.util_media_pager.select.media.MediaSelectingActivity
import com.social.solutions.android.util_media_pager.select.media.renderers.photo.PhotoModel
import com.social.solutions.android.util_media_pager.select.media.renderers.utils.MediaSelectCallback
import com.social.solutions.android.util_media_pager.select.media.renderers.utils.SerializableViewModel
import com.social.solutions.android.util_media_pager.select.media.renderers.video.VideoModel
import com.vanniktech.emoji.EmojiPopup
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.fragment_media_picker.*
import java.util.concurrent.TimeUnit

class RendererMediaPickerFragment: Fragment(),
    MediaInnerAdapterCallback, MediaPickerApi, OnBackPressedFragment {

    companion object{
        private val REQUIRED_PERMISSIONS = arrayOf(Manifest.permission.READ_EXTERNAL_STORAGE)
        private val MY_PERMISSON_REQUEST_CODE = 10
        private var isPermissionsGranted = false

        private const val PICK_CONTENT_REQUEST_CODE = 11

        private const val TAG = "RendererMediaPickerFragment_TAG"

        fun start(manager: FragmentManager, container: Int, callback: MediaPickerCallback): MediaPickerApi {
            val fragment = RendererMediaPickerFragment()
            fragment.mManager = manager
            fragment.mContainer = container
            fragment.mCallback = callback

            manager.beginTransaction()
                .replace(container, fragment)
                .addToBackStack(TAG)
                .commit()

            return fragment
        }
    }

    private lateinit var mManager: FragmentManager
    private var mContainer: Int? = null

    private lateinit var mCallback: MediaPickerCallback

    private lateinit var mAdapter: RendererRecyclerViewAdapter
    private val mItems = mutableListOf<ViewModel>()

    private lateinit var mSheetBehavior: BottomSheetBehavior<LinearLayout>

    private val mSpanCount = 3

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? =
        inflater.inflate(R.layout.fragment_media_picker, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        initRecycler()

        initEmoji()
        mSheetBehavior = initBottomSheet(view)

        initButtonSend(mSheetBehavior)

        viewBack.setOnClickListener {
            mSheetBehavior.state = BottomSheetBehavior.STATE_HIDDEN
        }

        buttonBackBottomSheet.setOnClickListener {
            mSheetBehavior.state = BottomSheetBehavior.STATE_HIDDEN
        }

        linearGallery.setOnClickListener {
            mSheetBehavior.state = BottomSheetBehavior.STATE_EXPANDED
        }

        linearFiles.setOnClickListener {
            val intent = Intent(Intent.ACTION_GET_CONTENT)
            intent.type = "*/*"
            startActivityForResult(intent, PICK_CONTENT_REQUEST_CODE)
        }

        checkPermission()
    }

    // Inits
    private fun initButtonSend(sheetBehavior: BottomSheetBehavior<LinearLayout>) {
        buttonSendMessage.setOnClickListener {
            val selectedData = getSelectedData()
            val mediaSign = getMediaSign()

            context?.let { it1 -> hideKeyboard(it1) }

            mCallback.onSelected(selectedData, mediaSign)
            sheetBehavior.state = BottomSheetBehavior.STATE_HIDDEN
        }
    }

    private fun initRecycler() {
        mAdapter = RendererRecyclerViewAdapter()
        mAdapter.registerRenderers()
        mAdapter.enableDiffUtil()
        mAdapter.setItems(mItems)

        recyclerMedia.adapter = mAdapter
        recyclerMedia.layoutManager = GridLayoutManager(context, mSpanCount)
    }

    private fun RendererRecyclerViewAdapter.registerRenderers() {
        registerRenderer(ViewBinder(R.layout.item_med_pick_first, FirstMPModel::class.java, FirstMPViewBinder(this@RendererMediaPickerFragment, onMakeMediaListener)))
        registerRenderer(ViewBinder(R.layout.item_med_pick_photo, PhotoMPModel::class.java, PhotoMPViewBinder(this@RendererMediaPickerFragment)))
        registerRenderer(ViewBinder(R.layout.item_med_pick_video, VideoMPModel::class.java, VideoMPViewBinder(this@RendererMediaPickerFragment)))
    }

    private fun initEmoji() {
        val emojiPopup = EmojiPopup.Builder.fromRootView(view).build(editMessage)
        imageEmoji.setOnClickListener {
            if (emojiPopup.isShowing) {
                imageEmoji.setImageDrawable(resources.getDrawable(R.drawable.ic_emoji))
                emojiPopup.dismiss()
            }
            else {
                imageEmoji.setImageDrawable(resources.getDrawable(R.drawable.ic_keyboard))
                emojiPopup.toggle()
            }
        }
    }

    private fun initBottomSheet(view: View): BottomSheetBehavior<LinearLayout> {
        val sheetBehavior = BottomSheetBehavior.from(bottomSheet)
        sheetBehavior.peekHeight = 0
        sheetBehavior.addBottomSheetCallback(object : BottomSheetBehavior.BottomSheetCallback() {
            @SuppressLint("SwitchIntDef")
            override fun onStateChanged(bottomSheet: View, newState: Int) {
                when (newState) {
                    BottomSheetBehavior.STATE_HIDDEN -> {
                        mCallback.onExit()
                    }
                    BottomSheetBehavior.STATE_EXPANDED -> {
                        isVisibleAnimate = true
                        linearBottomFilesBar.animateHide()
                    }
                    BottomSheetBehavior.STATE_COLLAPSED -> {
                        isVisibleAnimate = false
                        if (!constraintMessage.isVisible)
                            linearBottomFilesBar.animateShow()
                    }
                }
            }
            var isVisibleAnimate = true
            override fun onSlide(bottomSheet: View, slideOffset: Float) {
                if (slideOffset < 0) {
                    linearBottomFilesBar.translationY = -3.7f*(slideOffset*linearBottomFilesBar.measuredHeight)
                } else if (Range(0.8f, 0.9f).contains(slideOffset) && linearBottomFilesBar.visibility == View.GONE) {
                    if (isVisibleAnimate && !constraintMessage.isVisible){
                        linearBottomFilesBar.animateShow()
                        isVisibleAnimate = false
                    }
                }
                if (Range(0.9f, 1.0f).contains(slideOffset)) {
                    if (linearActionBar.isVisible.not())
                        linearActionBar.visibility = View.VISIBLE
                } else {
                    if (linearActionBar.isVisible)
                        linearActionBar.visibility = View.GONE
                }
            }
        })
        sheetBehavior.isHideable = true
        bottomSheet.post {
            sheetBehavior.setPeekHeight(view.measuredHeight/2, true)
            sheetBehavior.state = BottomSheetBehavior.STATE_COLLAPSED
        }
        return sheetBehavior
    }

    // Listeners

    private val onMakeMediaListener = View.OnClickListener {
        CameraActivity.start(this, object : CameraCallback {
            override fun onPictureTaken(filePath: String) {
                mCallback.onSelected(listOf(PhotoMPModel(filePath, 0, null)), null)
                mSheetBehavior.state = BottomSheetBehavior.STATE_HIDDEN
            }

            override fun onVideoTaken(filePath: String, seconds: Int) {
                mCallback.onSelected(listOf(VideoMPModel(filePath, seconds, 0, null)), null)
                mSheetBehavior.state = BottomSheetBehavior.STATE_HIDDEN
            }
        })
    }

    // Permissions
    private fun checkPermission() {
        val mContext = context ?: return
        if (ContextCompat.checkSelfPermission(mContext, REQUIRED_PERMISSIONS[0]) == PackageManager.PERMISSION_GRANTED) {
            isPermissionsGranted = true
            loadMediaData()
        } else {
            requestPermissions(REQUIRED_PERMISSIONS, MY_PERMISSON_REQUEST_CODE)
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        if (requestCode == MY_PERMISSON_REQUEST_CODE) {
            if (permissions.size == 1 &&
                permissions[0] == REQUIRED_PERMISSIONS[0] &&
                grantResults[0] == PackageManager.PERMISSION_GRANTED
            ) {
                isPermissionsGranted = true
                loadMediaData()
            } else {
                Log.d(this.javaClass.simpleName, "My Location permission denied")
            }
        } else
            super.onRequestPermissionsResult(requestCode, permissions, grantResults)
    }

    // OnBackPressedFragment
    override fun onBackPressed() {
        BottomSheetBehavior.from(bottomSheet).state = BottomSheetBehavior.STATE_HIDDEN
    }

    // Media data
    private val mPlaceholdersNumber = 10

    private fun loadMediaData() {
        if (isPermissionsGranted.not())
            return

        mItems.addPlaceholders()

        var index = 1
        val mediaCursor = getMediaCursor(context) ?:return
        val disp = mediaCursor.getMediaModels()
            .delay(430L, TimeUnit.MILLISECONDS)
            .subscribeOn(Schedulers.io())
            .observeOn(Schedulers.io())
            .subscribe({model ->
                if (index == 1)
                    mItems.removePlaceholders()

                mItems.addItemLast(model.convertToViewModel(context, index))
                index++
            }, {
                it.printStackTrace()
            })
    }

    private fun MutableList<ViewModel>.addItemFirst(item: ViewModel) {
        add(0, item)
        recyclerMedia.post {
            mAdapter.setItems(mItems)
        }
    }

    private fun MutableList<ViewModel>.addItemLast(item: ViewModel) {
        add(item)
        recyclerMedia.post {
            mAdapter.setItems(mItems)
        }
    }

    private fun MutableList<ViewModel>.removePlaceholders() {
        clear()
        addItemFirst(FirstMPModel())
        recyclerMedia.post {
            mAdapter.setItems(mItems)
        }
    }

    private fun MutableList<ViewModel>.addPlaceholders() {
        for (i in 1 until mPlaceholdersNumber) {
            addItemLast(PhotoMPModel("", i, null))
        }
    }

    // MediaInnerAdapterCallback
    private val mClickedItemsList = mutableListOf<Int>()
    private val mMaxItems = 1

    override fun onItemClicked(position: Int) {
        when {
            mClickedItemsList.contains(position) -> {
                val removedIndex = mClickedItemsList.indexOf(position)
                mClickedItemsList.remove(position)

                val clickedModel = mItems[position]
                if (clickedModel is ViewModelSelectable) {
                    clickedModel.selectedNumber = null
                    mAdapter.notifyItemChanged(position, PhotoMPViewBinder.MAKE_NON_SELECTED_ANIM)
                }
                for ((index, mItem) in mClickedItemsList.withIndex()) {
                    if (index >= removedIndex) {
                        val changeModel = mItems[mItem]
                        if (changeModel is ViewModelSelectable) {
                            changeModel.selectedNumber = index+1
                            mAdapter.notifyItemChanged(mItem, PhotoMPViewBinder.MAKE_NON_SELECTED_ANIM)
                        }
                    }
                }
                if (mClickedItemsList.isEmpty()) {
                    constraintMessage.animateHide()
                    context?.let { hideKeyboard(it) }
                    if (mSheetBehavior.state != BottomSheetBehavior.STATE_EXPANDED)
                        linearBottomFilesBar.animateShow()
                }
            }
            mClickedItemsList.size == mMaxItems -> {
                mAdapter.notifyItemChanged(position, PhotoMPViewBinder.MAKE_LIMIT_ANIM)
                Toast.makeText(context, resources.getString(R.string.selected_limit), Toast.LENGTH_SHORT).show()
                return
            }
            else -> {
                mClickedItemsList.add(position)
                val changeModel = mItems[position]
                if (changeModel is ViewModelSelectable) {
                    changeModel.selectedNumber = mClickedItemsList.size
                    mAdapter.notifyItemChanged(position, PhotoMPViewBinder.MAKE_SELECTED_ANIM)
                }
                if (mClickedItemsList.size == 1) {
                    constraintMessage.animateShow()
                    linearBottomFilesBar.animateHide()
                }
            }
        }
    }

    override fun showMedia(position: Int) {
        val tmpList = mutableListOf<SerializableViewModel>()
        for ((index, mItem) in mItems.withIndex()) {
            if (index == position)
                when(mItem) {
                    is PhotoMPModel -> tmpList.add(PhotoModel(mItem.filePath))
                    is VideoMPModel -> tmpList.add(VideoModel(mItem.filePath, mItem.seconds))
                }
        }
        if (tmpList.isEmpty()) return
        MediaSelectingActivity.start(this, tmpList, object : MediaSelectCallback {
            override fun onSelected(data: List<SerializableViewModel>, sign: String) {
                val resList = mutableListOf<ViewModel>()
                for ((index, datum) in data.withIndex()) {
                    when(datum){
                        is PhotoModel -> resList.add(PhotoMPModel(datum.filePath, index, null))
                        is VideoModel -> resList.add(VideoMPModel(datum.filePath, datum.seconds, index, null))
                    }
                }
                mCallback.onSelected(resList, if(sign.isEmpty()) null else sign)
                mSheetBehavior.state = BottomSheetBehavior.STATE_HIDDEN
            }
        })
    }

    // MediaPickerApi
    // ...

    // Get result
    private fun getSelectedData(): List<ViewModel> {
        val result = mutableListOf<ViewModel>()
        for ((index, mDatum) in mItems.withIndex()) {
            if (mClickedItemsList.contains(index))
                result.add(mDatum)
        }
        return result
    }

    private fun getMediaSign(): String? {
        val text = editMessage.text.toString()
        return if (text.isEmpty()) null else text
    }

    // Get file
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (requestCode == PICK_CONTENT_REQUEST_CODE) {
            data ?: return
            val uri = data.data as Uri
            mCallback.onFileSelected(uri)
            mSheetBehavior.state = BottomSheetBehavior.STATE_HIDDEN
        }
        super.onActivityResult(requestCode, resultCode, data)
    }
}
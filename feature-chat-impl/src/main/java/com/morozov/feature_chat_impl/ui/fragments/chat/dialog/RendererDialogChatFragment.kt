package com.morozov.feature_chat_impl.ui.fragments.chat.dialog

import android.net.Uri
import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.github.vivchar.rendererrecyclerviewadapter.RendererRecyclerViewAdapter
import com.github.vivchar.rendererrecyclerviewadapter.binder.LoadMoreViewBinder
import com.github.vivchar.rendererrecyclerviewadapter.binder.ViewBinder
import com.morozov.feature_chat_impl.R
import com.morozov.feature_chat_impl.repository.DialogRepository
import com.morozov.feature_chat_impl.repository.sendToBack
import com.morozov.feature_chat_impl.repository.toViewModel
import com.morozov.feature_chat_impl.start.MainObject
import com.morozov.feature_chat_impl.ui.fragments.chat.dialog.renderers.chat.common.companion.media.and.text.MediaAndTextMessageModel
import com.morozov.feature_chat_impl.ui.fragments.chat.dialog.renderers.chat.common.date.DateModel
import com.morozov.feature_chat_impl.ui.fragments.chat.dialog.renderers.chat.common.date.DateViewBinder
import com.morozov.feature_chat_impl.ui.fragments.chat.dialog.renderers.chat.from.me.compaign.media.and.text.FromMeMediaAndTextModel
import com.morozov.feature_chat_impl.ui.fragments.chat.dialog.renderers.chat.from.me.compaign.media.and.text.FromMeMediaAndTextViewBinder
import com.morozov.feature_chat_impl.ui.fragments.chat.dialog.renderers.chat.from.me.file.FromMeFileModel
import com.morozov.feature_chat_impl.ui.fragments.chat.dialog.renderers.chat.from.me.file.FromMeFileViewBinder
import com.morozov.feature_chat_impl.ui.fragments.chat.dialog.renderers.chat.to.me.compaign.media.and.text.ToMeMediaAndTextModel
import com.morozov.feature_chat_impl.ui.fragments.chat.dialog.renderers.chat.to.me.compaign.media.and.text.ToMeMediaAndTextViewBinder
import com.morozov.feature_chat_impl.ui.fragments.chat.dialog.utils.OnFileClickListener
import com.morozov.feature_chat_impl.ui.fragments.chat.dialog.utils.OnProfileClickListener
import com.morozov.feature_chat_impl.ui.fragments.chat.dialog.utils.addItemFirst
import com.morozov.feature_chat_impl.ui.fragments.chat.dialog.utils.addItemLast
import com.morozov.feature_chat_impl.ui.fragments.chat.dialog.utils.cancel.CancelMessageCallback
import com.morozov.feature_chat_impl.ui.fragments.chat.media.picker.bottom.sheet.utils.showFileActivity
import com.morozov.feature_chat_impl.ui.fragments.chat.select.messages.SelectMessagesChatFragment
import com.morozov.feature_chat_impl.ui.utility.ViewModelWithDate
import com.morozov.feature_chat_impl.ui.utility.edit.text.addMyTextChangeListener
import com.morozov.feature_chat_impl.ui.utility.message.MessageModel
import com.morozov.feature_chat_impl.ui.utility.message.direction.FromMeMessage
import com.vanniktech.emoji.EmojiPopup
import kotlinx.android.synthetic.main.fragment_chat.*
import kotlinx.android.synthetic.main.view_answer_attach_header.*
import java.util.*

open class RendererDialogChatFragment: SelectMessagesChatFragment(), DialogApi {

    private val mFileClickListener = object: OnFileClickListener {
        override fun onFileClicked(uri: Uri) {
            this@RendererDialogChatFragment.showFileActivity(uri)
        }
    }

    protected var lastLoadedMessageId: Long? = null
    protected var lastRequestLoadedMessageId: Long? = null
    protected var messagesTotalAmount = 0L

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val emojiPopup = EmojiPopup.Builder.fromRootView(view).build(editMessage)
        imageEmoji.setOnClickListener {
            if (emojiPopup.isShowing) {
                imageEmoji.setImageDrawable(resources.getDrawable(R.drawable.ic_emoji))
                emojiPopup.dismiss()
            } else {
                imageEmoji.setImageDrawable(resources.getDrawable(R.drawable.ic_keyboard))
                emojiPopup.toggle()
            }
        }

        mRecyclerViewAdapter = RendererRecyclerViewAdapter()
        mRecyclerViewAdapter.registerRenderers()
        mRecyclerViewAdapter.enableDiffUtil()
        mRecyclerViewAdapter.setItems(mItems)

        editMessage.addMyTextChangeListener(this::checkButtonEnable)

        buttonSendMessage.setOnClickListener {
            val message = editMessage.text.toString()
            if (message.trim().isEmpty())
                return@setOnClickListener
            editMessage.text?.clear()
            sendMessage(FromMeMediaAndTextModel(MainObject.myId!!, MainObject.myUserName, null, emptyList(), message, Calendar.getInstance()))
        }

        recyclerChat.adapter = mRecyclerViewAdapter
        val linearLayoutManager = LinearLayoutManager(context)
        linearLayoutManager.reverseLayout = true
        recyclerChat.layoutManager = linearLayoutManager

        recyclerChat.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
                super.onScrollStateChanged(recyclerView, newState)
                if (mItems.size < messagesTotalAmount &&
                    (recyclerView.layoutManager as LinearLayoutManager).findLastVisibleItemPosition() == mItems.size-1) {

                    mRecyclerViewAdapter.showLoadMore()
                    lastLoadedMessageId?.let { loadMoreDialog(it) }
                }
            }
        })

        mSendAgainLiveData.observe(viewLifecycleOwner, androidx.lifecycle.Observer {
            sendMessage(it)
        })
    }

    private fun RendererRecyclerViewAdapter.registerRenderers() {
        registerRenderer(ViewBinder(R.layout.item_date, DateModel::class.java,
            DateViewBinder()
        ))

        registerRenderer(ViewBinder(R.layout.item_from_me_media_text_message, FromMeMediaAndTextModel::class.java, FromMeMediaAndTextViewBinder(activity!!, this@RendererDialogChatFragment, cancelableCallback, this@RendererDialogChatFragment, mErrorClickListener)))
        registerRenderer(ViewBinder(R.layout.item_to_me_media_text_message, ToMeMediaAndTextModel::class.java, ToMeMediaAndTextViewBinder(activity!!, mProfileClickListener, this@RendererDialogChatFragment, cancelableCallback, this@RendererDialogChatFragment, mErrorClickListener)))

        registerRenderer(ViewBinder(R.layout.item_from_me_file_message, FromMeFileModel::class.java, FromMeFileViewBinder(mFileClickListener)))

        registerRenderer(LoadMoreViewBinder(R.layout.item_chat_load_more))
    }

    private fun loadMoreDialog(startId: Long) {
        if (startId == lastRequestLoadedMessageId)
            return
        else
            lastRequestLoadedMessageId = startId
        DialogRepository.loadDialog(MainObject.mChatId!!, startId).observe(this, androidx.lifecycle.Observer {
            if (it.isEmpty())
                return@Observer
            for (modelWithDate in it) {
                mItems.addItemLast(modelWithDate.toViewModel(context, onAnswerViewClickListener))
            }
            lastLoadedMessageId = it.last().id
            mRecyclerViewAdapter.setItems(mItems)
            showAnswerClickedModel()
        })
    }

    private val cancelableCallback by lazy {
        object : CancelMessageCallback {
            override fun onRemoveMessage(model: ViewModelWithDate) {
                mItems.remove(model)
                mRecyclerViewAdapter.setItems(mItems)
            }
        }
    }

    // DialogApi
    override fun sendMessage(message: ViewModelWithDate) {
        if (mAnswerId != null && mAnswerViewConfigs != null && message is MediaAndTextMessageModel)
            message.answerViewConfigs = mAnswerViewConfigs

        addMessageAndScroll(message)

        message.sendToBack(context!!, mAnswerId)?.observe(viewLifecycleOwner, androidx.lifecycle.Observer {
            (message as MessageModel).messageId = it

            if (message is FromMeMessage) {
                if (it == null)
                    message.readStatus = FromMeMessage.ReadStatus.ERROR
                else
                    message.readStatus = FromMeMessage.ReadStatus.SENT
            }
            if (mItems.contains(message))
                mRecyclerViewAdapter.notifyItemChanged(mItems.indexOf(message), Any())
        })

        mAnswerId = null
        mAnswerViewConfigs = null
        linearAnswerAttachHeader.visibility = View.GONE
    }

    override fun receiveMessage(message: ViewModelWithDate) {
        addMessage(message)
    }

    private fun addMessageAndScroll(item: ViewModelWithDate) {
        addMessage(item)
        recyclerChat.scrollToPosition(0)
    }

    private fun addMessage(item: ViewModelWithDate) {
        mItems.addItemFirst(item)
        mRecyclerViewAdapter.setItems(mItems)
    }

    // Message ready
    private fun checkButtonEnable(text: String?) {
        buttonSendMessage ?: return

        val isEnabled = text.isNullOrEmpty().not()
        buttonSendMessage.isEnabled = isEnabled
        val contextTmp = context ?: return
        if (isEnabled) {
            buttonSendMessage.setColorFilter(ContextCompat.getColor(contextTmp, R.color.colorAccent), android.graphics.PorterDuff.Mode.SRC_IN)
            buttonSendMessage.visibility = View.VISIBLE
            recordView.visibility = View.GONE
            imageAddFile.visibility = View.GONE
            imageMakeVoice.visibility = View.GONE
        }
        else {
            buttonSendMessage.setColorFilter(ContextCompat.getColor(contextTmp, R.color.light_grey), android.graphics.PorterDuff.Mode.SRC_IN)
            buttonSendMessage.visibility = View.GONE
            recordView.visibility = View.VISIBLE
            imageAddFile.visibility = View.VISIBLE
            imageMakeVoice.visibility = View.VISIBLE
        }
    }
}
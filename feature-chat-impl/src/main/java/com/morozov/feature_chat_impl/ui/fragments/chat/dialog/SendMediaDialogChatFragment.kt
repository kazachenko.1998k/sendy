package com.morozov.feature_chat_impl.ui.fragments.chat.dialog

import android.os.Bundle
import android.view.View
import androidx.lifecycle.Observer
import com.example.util_load_send_workers.repository.DialogWorkRepository
import com.morozov.feature_chat_impl.ui.fragments.chat.dialog.renderers.chat.common.companion.media.and.text.MediaAndTextMessageModel
import com.morozov.feature_chat_impl.ui.fragments.chat.voice.record.renderers.from.me.FromMeVoiceModel

open class SendMediaDialogChatFragment: RendererDialogChatFragment() {

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        DialogWorkRepository.getLoadingLiveData().observe(viewLifecycleOwner, Observer {
            val progress = if (it.progress < 0) it.progress * (-1) else it.progress
            sendProgressUpdate(it.filePath, progress)
        })
    }

    private fun sendProgressUpdate(filePath: String, progress: Int) {
        for ((index, mItem) in mItems.withIndex()) {
            if (mItem is MediaAndTextMessageModel) {
                for ((index2, media) in mItem.media.withIndex()) {
                    if (media.filePath == filePath) {
                        media.loadProgress = progress
                        mRecyclerViewAdapter.notifyItemChanged(index, index2)
                    }
                }
            }
            if (mItem is FromMeVoiceModel) {
                if (mItem.fileName == filePath) {
                    mItem.loadProgress = progress.toFloat()
                    mRecyclerViewAdapter.notifyItemChanged(index)
                }
            }
        }
    }
}
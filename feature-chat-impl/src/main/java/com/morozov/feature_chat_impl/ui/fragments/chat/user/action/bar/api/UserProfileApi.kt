package com.morozov.feature_chat_impl.ui.fragments.chat.user.action.bar.api

import android.graphics.Bitmap
import java.net.URL

interface UserProfileApi {
    fun showAvatar(url: URL)
    fun showUserName(name: String)
}
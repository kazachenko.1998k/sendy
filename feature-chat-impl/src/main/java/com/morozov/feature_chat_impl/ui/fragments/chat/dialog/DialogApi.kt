package com.morozov.feature_chat_impl.ui.fragments.chat.dialog

import com.morozov.feature_chat_impl.ui.utility.ViewModelWithDate

interface DialogApi {
    fun sendMessage(message: ViewModelWithDate)
    fun receiveMessage(message: ViewModelWithDate)
}
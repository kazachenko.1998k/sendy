package com.morozov.feature_chat_impl.ui.utility

interface OnBackPressedFragment {
    fun onBackPressed()
}
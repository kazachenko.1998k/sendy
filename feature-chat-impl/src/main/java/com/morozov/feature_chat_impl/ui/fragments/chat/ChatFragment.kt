package com.morozov.feature_chat_impl.ui.fragments.chat

import android.app.Dialog
import android.os.Bundle
import android.text.Html
import android.util.Log
import android.view.View
import android.view.Window
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import com.bumptech.glide.Glide
import com.morozov.core_backend_api.SignData
import com.morozov.core_backend_api.chat.Chat
import com.morozov.core_backend_api.message.Message
import com.morozov.feature_chat_api.models.callback.PhotoCallbackModel
import com.morozov.feature_chat_api.models.callback.VideoCallbackModel
import com.morozov.feature_chat_api.models.callback.VoiceCallbackModel
import com.morozov.feature_chat_impl.R
import com.morozov.feature_chat_impl.repository.*
import com.morozov.feature_chat_impl.start.MainObject
import com.morozov.feature_chat_impl.ui.fragments.chat.dialog.renderers.chat.from.me.compaign.media.and.text.FromMeMediaAndTextModel
import com.morozov.feature_chat_impl.ui.fragments.chat.dialog.renderers.chat.common.media.MediaModel
import com.morozov.feature_chat_impl.ui.fragments.chat.dialog.renderers.chat.common.media.photo.PhotoModel
import com.morozov.feature_chat_impl.ui.fragments.chat.dialog.renderers.chat.common.media.video.VideoModel
import com.morozov.feature_chat_impl.ui.fragments.chat.dialog.utils.addByTime
import com.morozov.feature_chat_impl.ui.fragments.chat.dialog.utils.addItemFirst
import com.morozov.feature_chat_impl.ui.fragments.chat.dialog.utils.addByTime
import com.morozov.feature_chat_impl.ui.fragments.chat.voice.record.VoiceRecordChatFragment
import com.morozov.feature_chat_impl.ui.fragments.chat.voice.record.renderers.VoiceState
import com.morozov.feature_chat_impl.ui.fragments.chat.voice.record.renderers.from.me.FromMeVoiceModel
import com.morozov.feature_chat_impl.ui.utility.ViewModelWithDate
import com.morozov.feature_chat_impl.ui.utility.message.MessageModel
import com.morozov.feature_chat_impl.ui.utility.message.direction.FromMeMessage
import com.morozov.feature_chat_impl.ui.utility.views.scroll.bottom.isInTheEndReverseByLinearLayoutManager
import com.social.solutions.android.util_load_send_services.util.addSignUrl
import io.reactivex.disposables.Disposable
import kotlinx.android.synthetic.main.dialog_leave_chat.*
import kotlinx.android.synthetic.main.dialog_menu_chat.*
import kotlinx.android.synthetic.main.fragment_chat.*
import java.lang.IllegalArgumentException
import java.net.URL
import java.util.*

class ChatFragment: VoiceRecordChatFragment() {

    companion object{
        internal val mSentMessage = MutableLiveData<Long>()
    }

    lateinit var mNetworkState: LiveData<Boolean>

    var mStartChatModel: Chat? = null

    lateinit var mPushMessageDisposable: Disposable

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        mNetworkState.observe(viewLifecycleOwner, Observer {
            if (it.not()) {
                showNoInternet()
            }
        })

        MainObject.mRecipientId = MainObject.mChatId ?: MainObject.mUserId ?: throw IllegalArgumentException("Recipient id must not be null!")

        initMyUserInfoAndDoAfter {
            if (mStartChatModel != null)
                loadChatUI(mStartChatModel!!)
            when {
                MainObject.mChatId != null -> {
                    initLoadingMessages(MainObject.mChatId!!)
                    initChat()
                }
                MainObject.mUserId != null -> {
                    initLoadingMessages(MainObject.mUserId!!)
                    checkIfDialogExists()
                }
            }
            initPushEvents()
        }

        mSentMessage.observe(viewLifecycleOwner, Observer {
            mNewMessages.remove(it)
            scrollBottomView.showNewMessagesAmount(mNewMessages.size)
        })
    }

    override fun onDestroy() {
        if (mPushMessageDisposable.isDisposed.not())
            mPushMessageDisposable.dispose()
        super.onDestroy()
    }

    override fun onResume() {
        super.onResume()
        restoreUserInfo()
    }

    private fun initPushEvents() {
        mPushMessageDisposable = PushRepository.subscribeMessagePush().subscribe{ response ->
            if (response.data?.chat_id == MainObject.mChatId || response.data?.chat_id == MainObject.mUserId) {
                PushRepository.loadMessage(context!!, response, onAnswerViewClickListener)
                    .subscribe({ viewModel ->
                        if (mItems.contains(viewModel).not()) {
                            if (recyclerChat.isInTheEndReverseByLinearLayoutManager())
                                recyclerChat.smoothScrollToPosition(0)
                            else {
                                mNewMessages.add((viewModel as MessageModel).messageId ?: 0)
                                scrollBottomView.addNewMessage()
                            }
                        }
                        mItems.addByTime(viewModel)
                        mRecyclerViewAdapter.setItems(mItems)
                        mRecyclerViewAdapter.notifyItemChanged(1)
                    }, { exception ->
                        exception.printStackTrace()
                    })
            }
        }
    }

    private fun initMyUserInfoAndDoAfter(doAfter: () -> Unit) {
        DialogRepository.getUserById(MainObject.myId!!).observe(viewLifecycleOwner, Observer { user ->
            if (user != null) {
                MainObject.myUserName = user.getUserNameOrNick()
                doAfter()
            } else
                MainObject.mCallback?.onDestroy()
        })
    }

    private fun initLoadingMessages(recipient: Long) {
//        val messages = MainObject.mCallback?.getLoadingMessages(recipient) ?: return
//        for (message in messages) {
//            val listMedia = mutableListOf<MediaModel>()
//            var voiceModel: FromMeVoiceModel? = null
//            for ((index, media) in message.media.withIndex()) {
//                when(media) {
//                    is PhotoCallbackModel -> listMedia.add(
//                        PhotoModel(
//                            index,
//                            null,
//                            media.loadProgress,
//                            media.filePath,
//                            null
//                        )
//                    )
//                    is VideoCallbackModel -> listMedia.add(
//                        VideoModel(
//                            index,
//                            null,
//                            media.filePath,
//                            media.loadProgress,
//                            null,
//                            media.sec
//                        )
//                    )
//                    is VoiceCallbackModel -> voiceModel = (FromMeVoiceModel(MainObject.mRecipientId!!, MainObject.myUserName, null, null, media.filePath, media.loadProgress.toFloat(), media.sec, 0, VoiceState.STOPPED, Calendar.getInstance()))
//                    else -> throw IllegalArgumentException("Wrong media model type")
//                }
//            }
//            val messageModel: ViewModelWithDate = when {
//                voiceModel != null -> voiceModel
//                message.msg != null -> {
//                    FromMeMediaAndTextModel(
//                        MainObject.myId!!,
//                        MainObject.myUserName,
//                        null,
//                        listMedia,
//                        message.msg!!,
//                        Calendar.getInstance()
//                    )
//                }
//                else -> {
//                    FromMeMediaAndTextModel(
//                        MainObject.myId!!,
//                        MainObject.myUserName,
//                        null,
//                        listMedia,
//                        "",
//                        Calendar.getInstance()
//                    )
//                }
//            }
//            mItems.addByTime(messageModel)
//        }
    }

    private fun checkIfDialogExists() {
        DialogRepository.getDialogById(MainObject.mUserId!!).observe(viewLifecycleOwner, Observer { chat ->
            if (chat == null)
                initDialogWithUser()
            else {
                MainObject.mChatId = chat.id
                loadChatUI(chat)
            }
        })
    }

    private fun initDialogWithUser() {
        Log.i("WebSoc", "initDialogWithUser")
        showMute(false)
        initUserRecipient()
    }

    private fun initUserRecipient() {
        if (MainObject.mUserId == null) return
        MainObject.mChatType = Chat.PERSONAL_DIALOG
        DialogRepository.getUserById(MainObject.mUserId!!).observe(viewLifecycleOwner, Observer { user ->
            if (user != null) {
                mChatModel = Chat()
                mChatModel?.id = user.uid
                mChatModel?.creatorUid = user.uid
                mChatModel?.type = Chat.PERSONAL_DIALOG
                showUserName(user.getUserNameOrNick())
                if (user.avatarFileId != null) {
                    showAvatar(URL(SignData.server_file + user.avatarFileId))
                }
                if (System.currentTimeMillis() - (user.lastActive * 1000) <= 60*1000 )
                    showOnline()
                else{
                    val date = Calendar.getInstance()
                    date.timeInMillis = user.lastActive * 1000
                    showWasOnline(true, date)
                }
            } else
                MainObject.mCallback?.onDestroy()
        })
    }

    private fun initChat() {
        DialogRepository.getChatInfo(MainObject.mChatId!!).observe(viewLifecycleOwner, Observer {  chat ->
            loadChatUI(chat)
        })
    }

    private fun loadChatUI(chat: Chat) {
        Log.i("WebSoc", "loadChatUI")
        mChatModel = chat
        messagesTotalAmount = chat.messageAmount ?: 0L
        MainObject.mChatType = chat.type
        when(chat.type) {
            Chat.PERSONAL_DIALOG -> {
                Log.i("WebSoc", "PERSONAL_DIALOG")
                MainObject.mUserId = chat.dialogUid
                initPersonalDialog()
            }
            Chat.SYSTEM -> {
                Log.i("WebSoc", "System chat")
                initWelcomeBot()
            }
            Chat.FAVORITES -> {
                Log.i("WebSoc", "FAVORITES")
                initWelcomeBot()
            }
            Chat.GROUP_CHAT -> {
                Log.i("WebSoc", "GROUP_CHAT")
                initGroupMenu()
                initGroupChat(chat)
            }
            Chat.CHANNEL -> {
                Log.i("WebSoc", "CHANNEL")
                initChannelMenu()
                initChannelChat(chat)
            }
        }
        showMute(chat.muteTime != null)
    }

    private fun initWelcomeBot() {
        val botAvatar = URL(SignData.server_file + mChatModel!!.iconFileId)
        val botName = mChatModel!!.title!!
        showAvatar(botAvatar)
        showUserName(botName)

        mChatModel ?: return
        DialogRepository.loadDialog(mChatModel!!.id).observe(viewLifecycleOwner, Observer {
            if (it.isEmpty())
                return@Observer
            for ((index, modelWithDate) in it.withIndex()) {
                if (modelWithDate.uid != MainObject.myId) {
                    MainObject.mUserId = modelWithDate.uid
                    mStartMessage = modelWithDate.getPositionIfNotRead(index)
                }
                mItems.addByTime(modelWithDate.toViewModel(context, onAnswerViewClickListener))
            }
            lastLoadedMessageId = it.last().id
            initUserRecipient()
            mRecyclerViewAdapter.setItems(mItems)
            if (mStartMessage != 0)
                recyclerChat.scrollToPosition(mStartMessage)
            mStartMessage = 0
        })
    }

    private fun initPersonalDialog() {
        if (mItems.isNotEmpty()) return
        MainObject.mChatId ?: return
        initUserRecipient()
        DialogRepository.loadDialog(MainObject.mChatId!!).observe(viewLifecycleOwner, Observer {
            if (it.isEmpty())
                return@Observer
            for ((index, modelWithDate) in it.withIndex()) {
                if (modelWithDate.uid != MainObject.myId) {
                    MainObject.mUserId = modelWithDate.uid
                    mStartMessage = modelWithDate.getPositionIfNotRead(index)
                }
                mItems.addByTime(modelWithDate.toViewModel(context, onAnswerViewClickListener))
            }
            lastLoadedMessageId = it.last().id
            initUserRecipient()
            mRecyclerViewAdapter.setItems(mItems)
            if (mStartMessage != 0)
                recyclerChat.scrollToPosition(mStartMessage)
            mStartMessage = 0
        })
    }

    private var groupName = ""
    private var groupAvatar: URL? = null

    private fun initGroupChat(chat: Chat) {
        if (mItems.isNotEmpty()) return
        MainObject.mChatId ?: return

        if (chat.userRole == 0 || chat.userRole == null) {
            initFooter()
            chanelFooter.showJoinChat()
        }

        groupAvatar = (URL(SignData.server_file + chat.iconFileId))
        groupName = chat.title!!
        showAvatar(groupAvatar!!)
        showUserName(groupName)

        showGroupUsers(chat.membersAmount!!)

        DialogRepository.loadDialog(MainObject.mChatId!!).observe(viewLifecycleOwner, Observer {
            if (it.isEmpty())
                return@Observer
            for ((index, modelWithDate) in it.withIndex()) {
                if (modelWithDate.uid != MainObject.myId) {
                    MainObject.mUserId = modelWithDate.uid
                    mStartMessage = modelWithDate.getPositionIfNotRead(index)
                }
                mItems.addByTime(modelWithDate.toViewModel(context, onAnswerViewClickListener))
            }
            lastLoadedMessageId = it.last().id
            mRecyclerViewAdapter.setItems(mItems)
            if (mStartMessage != 0)
                recyclerChat.scrollToPosition(mStartMessage)
            mStartMessage = 0
        })
    }

    private fun initChannelChat(chat: Chat) {
        if (mItems.isNotEmpty()) return
        MainObject.mChatId ?: return

        initFooter()
        when {
            chat.userRole == null || chat.userRole == 0 -> chanelFooter.showSubscribe()
            chat.userRole!!.checkBitTrue(2) -> {
                chanelFooter.visibility = View.GONE
                constraintMessage.visibility = View.VISIBLE
            }
            chat.muteTime != null -> chanelFooter.showEnableNotif()
            else -> chanelFooter.showDisableNotif()
        }

        showAvatar(URL(SignData.server_file + chat.iconFileId))
        showUserName(if (chat.title.isNullOrEmpty()) chat.nick?:"" else chat.title!!)

        showChanelSubs(chat.membersAmount!!)

        DialogRepository.loadDialog(MainObject.mChatId!!).observe(viewLifecycleOwner, Observer {
            if (it.isEmpty())
                return@Observer

            for ((index, modelWithDate) in it.withIndex()) {
                if (modelWithDate.uid != MainObject.myId) {
                    MainObject.mUserId = modelWithDate.uid
                    mStartMessage = modelWithDate.getPositionIfNotRead(index)
                }
                mItems.addByTime(modelWithDate.toViewModel(context, onAnswerViewClickListener))
            }
            lastLoadedMessageId = it.last().id
            mRecyclerViewAdapter.setItems(mItems)
            if (mStartMessage != 0)
                recyclerChat.scrollToPosition(mStartMessage)
            mStartMessage = 0
        })
    }

    private fun Message.getPositionIfNotRead(position: Int): Int {
        return if (status.toMessageReadStatus() != FromMeMessage.ReadStatus.USER_READ &&
                mNewMessages.contains(id).not()) {
            mNewMessages.add(id)
            position
        } else
            mStartMessage
    }

    private fun showLeaveDialog() {
        val dialog = Dialog(context!!)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setCancelable(true)
        dialog.setContentView(R.layout.dialog_leave_chat)
        dialog.window?.setBackgroundDrawableResource(R.drawable.dialog_back_chat)
        Glide.with(context!!)
            .load(groupAvatar)
            .placeholder(R.drawable.tmp_profile)
            .centerCrop()
            .into(dialog.imageAvatar)
        dialog.textSure.text = Html.fromHtml(String.format(resources.getString(R.string.leave_chat_sure), groupName))
        dialog.action_btn.setOnClickListener {
            dialog.dismiss()
            hideMenuChat()
            activity?.onBackPressed()
            GeneralRepository.leaveChat()
        }
        dialog.cancel_button.setOnClickListener {
            dialog.dismiss()
        }
        dialog.show()
    }

    private fun initGroupMenu() {
        textDeleteLeaveChat.text = resources.getString(R.string.leave_chat)
        linearDeleteChat.setOnClickListener {
            hideMenuChat()
            showLeaveDialog()
        }
    }

    private fun initChannelMenu() {
        buttonMore.visibility = View.GONE
        buttonMore.isEnabled = false
    }

    private fun Int.checkBitTrue(bit: Int): Boolean {
        val iBit = ((this shr bit) and 1)
        Log.i("Bit_res", "val $this $bit-th bit is: $iBit")
        return iBit == 1
    }
}
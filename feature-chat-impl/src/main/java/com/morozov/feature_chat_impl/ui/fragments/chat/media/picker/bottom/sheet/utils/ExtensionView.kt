package com.morozov.feature_chat_impl.ui.fragments.chat.media.picker.bottom.sheet.utils

import android.animation.Animator
import android.animation.AnimatorListenerAdapter
import android.view.View
import android.view.animation.AnimationUtils
import com.morozov.feature_chat_impl.R

fun View.animateShow() {
    this@animateShow.animate()
        .translationY(0.0f)
        .setDuration(300)
        .setListener(object : AnimatorListenerAdapter() {
            override fun onAnimationStart(animation: Animator?) {
                super.onAnimationStart(animation)
                this@animateShow.visibility = View.VISIBLE
            }
        })
}

fun View.animateHide() {
    this@animateHide.animate()
        .translationY(this@animateHide.height.toFloat())
        .setDuration(300)
        .setListener(object : AnimatorListenerAdapter() {
            override fun onAnimationEnd(animation: Animator?) {
                super.onAnimationEnd(animation)
                this@animateHide.visibility = View.GONE
            }
        })
}

fun View.animateNo() {
    val myAnim = AnimationUtils.loadAnimation(this.context, R.anim.shake_anim)
    this.startAnimation(myAnim)
}
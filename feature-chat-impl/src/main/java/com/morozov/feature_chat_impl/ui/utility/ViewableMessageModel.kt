package com.morozov.feature_chat_impl.ui.utility

interface ViewableMessageModel {
    var views: Int
    companion object{
        const val NO_VIEWABLE = -1
    }
}
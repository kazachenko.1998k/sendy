package com.morozov.feature_chat_impl.ui.fragments.chat.dialog.renderers.chat.common.media.photo

import android.graphics.Bitmap
import com.morozov.feature_chat_impl.ui.fragments.chat.dialog.renderers.chat.common.media.MediaModel
import com.morozov.feature_chat_impl.ui.fragments.chat.dialog.renderers.chat.common.media.loaded.photo.PhotoLoadedModel
import java.net.URL

class PhotoModel(position: Int, url: URL?, loadProgress: Int?, filePath: String?, preview: Bitmap?): MediaModel(position, url, filePath, loadProgress, preview) {
    fun toLoaded(): PhotoLoadedModel =
        PhotoLoadedModel(
            position,
            url,
            filePath,
            preview
        ).also { it.isSingle = this.isSingle }
}
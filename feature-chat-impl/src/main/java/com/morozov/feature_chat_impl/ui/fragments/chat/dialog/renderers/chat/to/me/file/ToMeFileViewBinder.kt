package com.morozov.feature_chat_impl.ui.fragments.chat.dialog.renderers.chat.to.me.file

import android.view.View
import android.widget.ImageView
import android.widget.TextView
import com.github.vivchar.rendererrecyclerviewadapter.binder.ViewBinder
import com.github.vivchar.rendererrecyclerviewadapter.binder.ViewFinder
import com.morozov.feature_chat_impl.R
import com.morozov.feature_chat_impl.repository.DialogRepository
import com.morozov.feature_chat_impl.ui.fragments.chat.dialog.utils.loadUserAvatar
import com.morozov.feature_chat_impl.ui.utility.ViewableMessageModel

class ToMeFileViewBinder: ViewBinder.Binder<ToMeFileModel> {
    override fun bindView(model: ToMeFileModel, finder: ViewFinder, payloads: MutableList<Any>) {
        if (payloads.isEmpty()) {
            DialogRepository.sendView(model.messageId!!)

            val imageViewsNumber = finder.find<ImageView>(R.id.imageViewsNumber)
            val textViewsNumber = finder.find<TextView>(R.id.textViewsNumber)

            if (model.views == ViewableMessageModel.NO_VIEWABLE) {
                imageViewsNumber.visibility = View.GONE
                textViewsNumber.visibility = View.GONE
            } else {
                imageViewsNumber.visibility = View.VISIBLE
                textViewsNumber.visibility = View.VISIBLE
                textViewsNumber.text = model.views.toString()
            }

            val imageProfile = finder.find<ImageView>(R.id.imageProfile)
            val spaceView = finder.find<View>(R.id.spaceView)
            when {
                model.userIconUrl == null -> {
                    imageProfile.visibility = View.GONE
                    spaceView.visibility = View.GONE
                }
                model.isShow.not() -> {
                    imageProfile.visibility = View.GONE
                    spaceView.visibility = View.VISIBLE
                }
                else -> {
                    imageProfile.visibility = View.VISIBLE
                    spaceView.visibility = View.VISIBLE
                    imageProfile.loadUserAvatar(model.userIconUrl)
                }
            }
            // TODO: Show file info
        }
    }
}
package com.morozov.feature_chat_impl.ui.fragments.chat.voice.record.renderers.from.me

import com.morozov.feature_chat_impl.ui.fragments.chat.dialog.renderers.chat.common.voice.VoiceMessageModel
import com.morozov.feature_chat_impl.ui.fragments.chat.voice.record.renderers.VoiceState
import com.morozov.feature_chat_impl.ui.utility.ViewModelWithDate
import com.morozov.feature_chat_impl.ui.utility.ViewableMessageModel
import com.morozov.feature_chat_impl.ui.utility.ViewableMessageModel.Companion.NO_VIEWABLE
import com.morozov.feature_chat_impl.ui.utility.message.MessageModel
import com.morozov.feature_chat_impl.ui.utility.message.direction.FromMeMessage
import java.util.*

class FromMeVoiceModel(override val userId: Long,
                       override val userName: String,
                       override var messageId: Long?,
                       url: String?,
                       fileName: String?,
                       loadProgress: Float,
                       timeInSeconds:Int,
                       progress: Int,
                       state: VoiceState,
                       date: Calendar,
                       override var views: Int = NO_VIEWABLE,
                       isSelected: Boolean = false,
                       override var readStatus: FromMeMessage.ReadStatus = FromMeMessage.ReadStatus.SENDING
): VoiceMessageModel(userId, userName, messageId, url, fileName, loadProgress, timeInSeconds, progress, state, date, views, isSelected), FromMeMessage
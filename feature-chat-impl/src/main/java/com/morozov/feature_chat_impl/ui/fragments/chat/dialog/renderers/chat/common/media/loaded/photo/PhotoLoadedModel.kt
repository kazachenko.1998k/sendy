package com.morozov.feature_chat_impl.ui.fragments.chat.dialog.renderers.chat.common.media.loaded.photo

import android.graphics.Bitmap
import com.morozov.feature_chat_impl.ui.fragments.chat.dialog.renderers.chat.common.media.loaded.LoadedMediaModel
import com.morozov.feature_chat_impl.ui.fragments.chat.dialog.renderers.chat.common.media.photo.PhotoModel
import java.net.URL

class PhotoLoadedModel(position: Int, url: URL?, filePath: String?, preview: Bitmap?): LoadedMediaModel(position, url, filePath, preview) {
    fun toPhoto(): PhotoModel {
        return PhotoModel(
            position,
            url,
            0,
            filePath,
            preview
        ).also { it.isSingle = this.isSingle }
    }
}
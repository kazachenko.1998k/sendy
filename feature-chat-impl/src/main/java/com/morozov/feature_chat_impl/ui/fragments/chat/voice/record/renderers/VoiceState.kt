package com.morozov.feature_chat_impl.ui.fragments.chat.voice.record.renderers

enum class VoiceState {
    STOPPED,
    PAUSED,
    PLAYING
}
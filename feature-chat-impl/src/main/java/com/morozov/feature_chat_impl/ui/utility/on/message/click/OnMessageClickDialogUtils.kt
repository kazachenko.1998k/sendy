package com.morozov.feature_chat_impl.ui.utility.on.message.click

import android.app.Dialog
import android.content.Context
import android.view.Window
import com.morozov.feature_chat_impl.R
import kotlinx.android.synthetic.main.dialog_on_message_click.*

fun Context.showOnMessageClickDialog(doAnswer: () -> Unit,
                                     doCopy: () -> Unit,
                                     doForward: () -> Unit,
                                     doDelete: () -> Unit) {
    val dialog = Dialog(this)
    dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
    dialog.setCancelable(true)
    dialog.setContentView(R.layout.dialog_on_message_click)
    dialog.window?.setBackgroundDrawableResource(R.drawable.dialog_back_chat)
    dialog.textAnswer.setOnClickListener {
        dialog.dismiss()
        doAnswer()
    }
    dialog.textCopy.setOnClickListener {
        dialog.dismiss()
        doCopy()
    }
    dialog.textForward.setOnClickListener {
        dialog.dismiss()
        doForward()
    }
    dialog.textDelete.setOnClickListener {
        dialog.dismiss()
        doDelete()
    }
    dialog.show()
}
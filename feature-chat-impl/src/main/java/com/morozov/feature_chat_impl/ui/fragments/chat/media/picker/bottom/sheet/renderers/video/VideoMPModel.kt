package com.morozov.feature_chat_impl.ui.fragments.chat.media.picker.bottom.sheet.renderers.video

import android.content.Context
import com.morozov.feature_chat_impl.ui.fragments.chat.dialog.renderers.chat.common.media.video.VideoModel
import com.morozov.feature_chat_impl.ui.fragments.chat.media.picker.bottom.sheet.renderers.ViewModelSelectable

class VideoMPModel(val filePath: String, val seconds: Int,
                   val position: Int, selectedNumber: Int?): ViewModelSelectable(selectedNumber) {
    fun convertToVideoModel(context: Context, index: Int): VideoModel {
        return VideoModel(
            index,
            null,
            filePath,
            null,
            null,
            seconds
        )
    }
}
package com.morozov.feature_chat_impl.ui.utility.views.chanels

interface ChanelFooterSettingsApi {
    fun showSubscribe()
    fun showDisableNotif()
    fun showEnableNotif()
    fun showDiscussDisableNotif()
    fun showDiscussEnableNotif()

    fun showJoinChat()
}
package com.morozov.feature_chat_impl.ui.fragments.chat.media.picker

import android.net.Uri
import android.os.Bundle
import android.view.View
import com.example.feature_bottom_picker_api.FeatureBottomPickerCallback
import com.example.feature_bottom_picker_api.models.PhotoModel
import com.example.feature_bottom_picker_api.models.StarterModel
import com.example.feature_bottom_picker_api.models.VideoModel
import com.morozov.feature_chat_api.models.api.MediaApiModel
import com.morozov.feature_chat_api.models.api.MessageApiModel
import com.morozov.feature_chat_api.models.api.PhotoApiModel
import com.morozov.feature_chat_api.models.api.VideoApiModel
import com.morozov.feature_chat_impl.R
import com.morozov.feature_chat_impl.start.MainObject
import com.morozov.feature_chat_impl.ui.fragments.chat.dialog.SendMediaDialogChatFragment
import com.morozov.feature_chat_impl.ui.fragments.chat.dialog.renderers.chat.from.me.compaign.media.and.text.FromMeMediaAndTextModel
import com.morozov.feature_chat_impl.ui.fragments.chat.dialog.renderers.chat.from.me.file.FromMeFileModel
import com.morozov.feature_chat_impl.ui.fragments.chat.dialog.renderers.chat.common.media.MediaModel
import com.morozov.feature_chat_impl.utility.hideKeyboard
import kotlinx.android.synthetic.main.fragment_chat.*
import java.util.*

open class ChatMediaPickerFragment: SendMediaDialogChatFragment(), FeatureBottomPickerCallback {

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        imageAddFile.setOnClickListener {
            context?.let { it1 -> hideKeyboard(it1) }
            val configs = StarterModel()
            configs.fullScreen = false
            configs.isSinglePick = false
            MainObject.bottomPickerStarterApi?.start(
                childFragmentManager,
                R.id.contentMediaPicker,
                configs,
                this
            )
        }
    }

    // MediaPickerCallback
    override fun onSelected(media: List<com.example.feature_bottom_picker_api.models.MediaModel>) {
//        sendMessageCallback(getResListCallback(media))
        sendMessage(FromMeMediaAndTextModel(mUserId, MainObject.myUserName, null, getResList(media), "",  Calendar.getInstance()))
    }

    override fun onSelected(media: List<com.example.feature_bottom_picker_api.models.MediaModel>, comment: String) {
//        sendMessageCallback(getResListCallback(media), comment)
        sendMessage(FromMeMediaAndTextModel(mUserId, MainObject.myUserName, null, getResList(media), comment, Calendar.getInstance()))
    }

    private fun getResList(media: List<com.example.feature_bottom_picker_api.models.MediaModel>): List<MediaModel> {
        val resList = mutableListOf<MediaModel>()
        for ((index, mediaModel) in media.withIndex()) {
            if (mediaModel is PhotoModel) {
                resList.add(
                    com.morozov.feature_chat_impl.ui.fragments.chat.dialog.renderers.chat.common.media.photo.PhotoModel(
                        index,
                        null,
                        null,
                        mediaModel.filePath,
                        null
                    )
                )
            }
            if (mediaModel is VideoModel) {
                resList.add(
                    com.morozov.feature_chat_impl.ui.fragments.chat.dialog.renderers.chat.common.media.video.VideoModel(
                        index,
                        null,
                        mediaModel.filePath,
                        null,
                        null,
                        mediaModel.seconds
                    )
                )
            }
        }
        return resList
    }

    private fun getResListCallback(media: List<com.example.feature_bottom_picker_api.models.MediaModel>): List<MediaApiModel> {
        val resList = mutableListOf<MediaApiModel>()
        for (mediaModel in media) {
            if (mediaModel is PhotoModel) {
                resList.add(PhotoApiModel(mediaModel.filePath))
            }
            if (mediaModel is VideoModel) {
                resList.add(VideoApiModel(mediaModel.filePath, mediaModel.seconds))
            }
        }
        return resList
    }

    private fun sendMessageCallback(media: List<MediaApiModel>, msg: String? = null) {
        val recipient = MainObject.mRecipientId ?: throw IllegalArgumentException("No recipient id")
        MainObject.mCallback?.onSendMessage(recipient, MessageApiModel(media, msg))
    }

    override fun onFileSelected(uri: Uri) {
        sendMessage(FromMeFileModel(mUserId, MainObject.myUserName, null, uri, Calendar.getInstance()))
    }

    override fun onExit() {
        childFragmentManager.popBackStack()
    }
}
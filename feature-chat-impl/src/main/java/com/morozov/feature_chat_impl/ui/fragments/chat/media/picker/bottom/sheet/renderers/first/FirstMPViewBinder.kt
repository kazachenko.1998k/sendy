package com.morozov.feature_chat_impl.ui.fragments.chat.media.picker.bottom.sheet.renderers.first

import android.view.View
import androidx.lifecycle.LifecycleOwner
import com.github.vivchar.rendererrecyclerviewadapter.binder.ViewBinder
import com.github.vivchar.rendererrecyclerviewadapter.binder.ViewFinder
import com.morozov.feature_chat_impl.R
import com.otaliastudios.cameraview.CameraView

class FirstMPViewBinder(private val lifecycleOwner: LifecycleOwner,
                        private val listener: View.OnClickListener): ViewBinder.Binder<FirstMPModel>{
    override fun bindView(model: FirstMPModel, finder: ViewFinder, payloads: MutableList<Any>) {
        if (payloads.isEmpty()) {
            val cameraPreview = finder.find<CameraView>(R.id.cameraPreview)
            cameraPreview.setLifecycleOwner(lifecycleOwner)
            finder.setOnClickListener(R.id.imageCamera, listener)
        }
    }
}
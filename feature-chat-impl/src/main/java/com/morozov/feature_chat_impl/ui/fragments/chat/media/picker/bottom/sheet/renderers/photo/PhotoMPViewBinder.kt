package com.morozov.feature_chat_impl.ui.fragments.chat.media.picker.bottom.sheet.renderers.photo

import android.view.View
import android.view.animation.AnimationUtils
import android.widget.ImageView
import android.widget.TextView
import com.bumptech.glide.Glide
import com.github.vivchar.rendererrecyclerviewadapter.binder.ViewBinder
import com.github.vivchar.rendererrecyclerviewadapter.binder.ViewFinder
import com.morozov.feature_chat_impl.R
import com.morozov.feature_chat_impl.ui.fragments.chat.media.picker.bottom.sheet.interaction.MediaInnerAdapterCallback
import com.morozov.feature_chat_impl.ui.fragments.chat.media.picker.bottom.sheet.utils.MyBounceInterpolator
import com.morozov.feature_chat_impl.ui.fragments.chat.media.picker.bottom.sheet.utils.animateNo

class PhotoMPViewBinder(private val mCallback: MediaInnerAdapterCallback)
    : ViewBinder.Binder<PhotoMPModel> {

    companion object{
        const val MAKE_SELECTED_ANIM = 0
        const val MAKE_NON_SELECTED_ANIM = 1
        const val MAKE_LIMIT_ANIM = 2
    }

    override fun bindView(model: PhotoMPModel, finder: ViewFinder, payloads: MutableList<Any>) {
        val textSelectedNumber = finder.find<TextView>(R.id.textSelectedNumber)
        val imageIsSelected = finder.find<ImageView>(R.id.imageIsSelected)

        if (model.selectedNumber != null)
            showSelected(textSelectedNumber, imageIsSelected, model.selectedNumber!!)
        else
            showNonSelected(textSelectedNumber, imageIsSelected)

        when {
            payloads.isEmpty() -> {
                if (model.filePath.isEmpty())
                    return

                val imagePreview = finder.find<ImageView>(R.id.imagePreview)
                val previewSize =
                    imagePreview.resources.getDimensionPixelSize(R.dimen.media_preview_size)

                imagePreview.clipToOutline = true
                Glide.with(imagePreview)
                    .load(model.filePath)
                    .override(previewSize, previewSize)
                    .centerCrop()
                    .into(imagePreview)

                imageIsSelected.setOnClickListener {
                    mCallback.onItemClicked(model.position)
                }

                imagePreview.setOnClickListener {
                    mCallback.showMedia(model.position)
                }
            }
            payloads[0] == MAKE_SELECTED_ANIM -> {
                makeBounceEffect(textSelectedNumber, imageIsSelected)
            }
            payloads[0] == MAKE_NON_SELECTED_ANIM -> {
                makeBounceEffect(textSelectedNumber, imageIsSelected)
            }
            payloads[0] == MAKE_LIMIT_ANIM -> {
                makeLimitAnim(imageIsSelected)
            }
        }
    }

    private fun showSelected(textSelectedNumber: TextView, imageIsSelected: ImageView, number: Int) {
        textSelectedNumber.visibility = View.VISIBLE
        //textSelectedNumber.text = number.toString()
        imageIsSelected.setImageDrawable(imageIsSelected.resources.getDrawable(R.drawable.ic_check_mark))
    }

    private fun showNonSelected(textSelectedNumber: TextView, imageIsSelected: ImageView) {
        textSelectedNumber.visibility = View.INVISIBLE
        textSelectedNumber.text = ""
        imageIsSelected.setImageDrawable(imageIsSelected.resources.getDrawable(R.drawable.ic_non_selected_media_item))
    }

    private fun makeBounceEffect(textSelectedNumber: TextView, imageIsSelected: ImageView) {
        val myAnim = AnimationUtils.loadAnimation(imageIsSelected.context, R.anim.bounce_anim)
        myAnim.interpolator =
            MyBounceInterpolator(
                0.2,
                10.0
            )
        imageIsSelected.startAnimation(myAnim)
        textSelectedNumber.startAnimation(myAnim)
    }

    private fun makeLimitAnim(imageIsSelected: ImageView) {
        imageIsSelected.animateNo()
    }
}
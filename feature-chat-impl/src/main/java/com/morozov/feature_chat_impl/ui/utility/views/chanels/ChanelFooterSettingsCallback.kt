package com.morozov.feature_chat_impl.ui.utility.views.chanels

interface ChanelFooterSettingsCallback {
    fun onSubscribe()
    fun onDisableNotif()
    fun onEnableNotif()
    fun onDiscussDisableNotif()
    fun onDiscussEnableNotif()
    fun onDiscuss()

    fun onJoinChat()
}
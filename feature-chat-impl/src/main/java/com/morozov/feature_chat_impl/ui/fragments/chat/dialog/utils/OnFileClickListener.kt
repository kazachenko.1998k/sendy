package com.morozov.feature_chat_impl.ui.fragments.chat.dialog.utils

import android.net.Uri

interface OnFileClickListener {
    fun onFileClicked(uri: Uri)
}
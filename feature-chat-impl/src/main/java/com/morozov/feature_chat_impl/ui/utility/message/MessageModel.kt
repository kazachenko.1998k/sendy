package com.morozov.feature_chat_impl.ui.utility.message

interface MessageModel {
    val userId: Long
    val userName: String
    var messageId: Long?
}
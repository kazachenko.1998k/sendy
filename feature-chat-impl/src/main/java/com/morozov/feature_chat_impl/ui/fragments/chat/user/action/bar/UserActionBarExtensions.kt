package com.morozov.feature_chat_impl.ui.fragments.chat.user.action.bar

import android.text.Html
import android.widget.TextView
import com.morozov.feature_chat_impl.R
import java.text.SimpleDateFormat
import java.util.*

fun TextView.showWasOnline(isMale: Boolean, date: Calendar): String {
    val wasOnline = if (isMale)
        resources.getString(R.string.was_online_male)
    else
        resources.getString(R.string.was_online_female)

    val wasOnlineTime = SimpleDateFormat("HH:mm").format(date.time)

    text = String.format(wasOnline, wasOnlineTime)
    return text.toString()
}

fun TextView.showOnline(): String {
    setText(Html.fromHtml(resources.getString(R.string.user_online)), TextView.BufferType.SPANNABLE)
    return resources.getString(R.string.user_online)
}

fun TextView.showChanelSubs(count: Int): String {
    val usersText = resources.getQuantityString(R.plurals.channel_plurals, count)
    text = String.format(usersText, count)
    return text.toString()
}

fun TextView.showGroupUsers(count: Int): String {
    val usersText = resources.getQuantityString(R.plurals.group_plurals, count)
    text = String.format(usersText, count)
    return text.toString()
}

package com.morozov.feature_chat_impl.ui.fragments.chat.dialog.renderers.chat.common.voice

import com.morozov.feature_chat_impl.ui.fragments.chat.dialog.utils.SelectableModel
import com.morozov.feature_chat_impl.ui.fragments.chat.voice.record.renderers.VoiceState
import com.morozov.feature_chat_impl.ui.fragments.chat.voice.record.renderers.from.me.FromMeVoiceModel
import com.morozov.feature_chat_impl.ui.utility.ViewModelWithDate
import com.morozov.feature_chat_impl.ui.utility.ViewableMessageModel
import com.morozov.feature_chat_impl.ui.utility.message.MessageModel
import com.morozov.feature_chat_impl.ui.utility.message.direction.FromMeMessage
import java.util.*

open class VoiceMessageModel(
    override val userId: Long,
    override val userName: String,
    override var messageId: Long?,
    var url: String?,
    var fileName: String?,
    var loadProgress: Float,
    val timeInSeconds:Int,
    var progress: Int,
    var state: VoiceState,
    date: Calendar,
    override var views: Int = ViewableMessageModel.NO_VIEWABLE,
    override var isSelected: Boolean = false
): ViewModelWithDate(date), ViewableMessageModel, MessageModel, SelectableModel {

    override fun equals(other: Any?): Boolean {
        if (other !is VoiceMessageModel)
            return false
        if (messageId != null && messageId == other.messageId)
            return true
        return userId == other.userId
                && userName == other.userName
                && (url == other.url || fileName == other.fileName)
                && timeInSeconds == other.timeInSeconds
                && date == other.date
    }

    override fun hashCode(): Int {
        var result = userId.hashCode()
        result = 31 * result + userName.hashCode()
        result = 31 * result + (url?.hashCode() ?: 0)
        result = 31 * result + (fileName?.hashCode() ?: 0)
        result = 31 * result + loadProgress.hashCode()
        result = 31 * result + timeInSeconds
        result = 31 * result + progress
        result = 31 * result + state.hashCode()
        result = 31 * result + views
        return result
    }
}
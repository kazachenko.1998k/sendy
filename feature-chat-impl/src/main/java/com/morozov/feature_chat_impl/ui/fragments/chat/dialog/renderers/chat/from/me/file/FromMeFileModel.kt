package com.morozov.feature_chat_impl.ui.fragments.chat.dialog.renderers.chat.from.me.file

import android.net.Uri
import com.morozov.feature_chat_impl.ui.utility.ViewModelWithDate
import com.morozov.feature_chat_impl.ui.utility.ViewableMessageModel
import com.morozov.feature_chat_impl.ui.utility.ViewableMessageModel.Companion.NO_VIEWABLE
import com.morozov.feature_chat_impl.ui.utility.message.direction.FromMeMessage
import java.util.*

class FromMeFileModel(override val userId: Long,
                      override val userName: String,
                      override var messageId: Long?,
                      val uri: Uri, date: Calendar,
                      override var views: Int = NO_VIEWABLE,
                      override var readStatus: FromMeMessage.ReadStatus = FromMeMessage.ReadStatus.SENDING
) : ViewModelWithDate(date), ViewableMessageModel, FromMeMessage {
    override fun equals(other: Any?): Boolean {
        if (other !is FromMeFileModel)
            return false
        return userId == other.userId
                && date == other.date
                && uri == other.uri
                && userName == other.userName
    }

    override fun hashCode(): Int {
        var result = userId.hashCode()
        result = 31 * result + userName.hashCode()
        result = 31 * result + uri.hashCode()
        result = 31 * result + views
        return result
    }
}
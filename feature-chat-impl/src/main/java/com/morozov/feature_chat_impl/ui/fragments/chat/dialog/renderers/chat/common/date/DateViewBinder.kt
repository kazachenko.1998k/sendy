package com.morozov.feature_chat_impl.ui.fragments.chat.dialog.renderers.chat.common.date

import android.widget.TextView
import com.github.vivchar.rendererrecyclerviewadapter.binder.ViewBinder
import com.github.vivchar.rendererrecyclerviewadapter.binder.ViewFinder
import com.morozov.feature_chat_impl.R
import com.morozov.feature_chat_impl.utility.convertLongToCurrentType

class DateViewBinder: ViewBinder.Binder<DateModel> {
    override fun bindView(model: DateModel, finder: ViewFinder, payloads: MutableList<Any>) {
        val textDate = finder.find<TextView>(R.id.textDate)
        textDate.text = model.date.timeInMillis.convertLongToCurrentType(textDate.context)
    }
}
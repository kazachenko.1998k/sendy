package com.morozov.feature_chat_impl.ui.fragments.chat.dialog.utils

import android.graphics.Bitmap
import android.graphics.drawable.Drawable
import android.media.MediaMetadataRetriever
import android.util.Log
import android.widget.ImageView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.load.resource.bitmap.BitmapTransitionOptions.withCrossFade
import com.bumptech.glide.request.RequestOptions
import com.bumptech.glide.request.target.CustomTarget
import com.bumptech.glide.request.transition.Transition
import com.morozov.feature_chat_impl.R
import com.morozov.feature_chat_impl.ui.fragments.chat.dialog.renderers.chat.common.media.MediaModel
import com.morozov.feature_chat_impl.ui.fragments.chat.dialog.renderers.chat.common.media.OnMediaClicked
import com.morozov.feature_chat_impl.ui.fragments.chat.dialog.renderers.chat.common.media.video.VideoModel
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import java.net.URL
import kotlin.math.roundToInt

fun Bitmap.scaleDown(): Bitmap? {
    val maxImageSize = 550f
    val filter = true
    val ratio = (maxImageSize / width).coerceAtMost(maxImageSize / height)
    val width = (ratio * width).roundToInt()
    val height = (ratio * height).roundToInt()
    return Bitmap.createScaledBitmap(
        this, width,
        height, filter
    )
}

fun Pair<Int, Int>.scaleDown(): Pair<Int, Int> {
    val maxImageSize = 550f
    val ratio = (maxImageSize / first).coerceAtMost(maxImageSize / second)
    val width = (ratio * first).roundToInt()
    val height = (ratio * second).roundToInt()
    return width to height
}

fun ImageView.loadImageScaledDown(url: String,
                                  model: MediaModel,
                                  callback: OnMediaClicked? = null): Disposable {
    if (callback == null) {
        this.layoutParams?.width = model.width
        this.layoutParams?.height = model.height
    }
    return Observable.create<Bitmap> {
        val trans = Glide.with(context)
            .asBitmap()
            .load(url)
            .thumbnail(0.1f)
            .transition(withCrossFade())
        if (callback == null)
            trans.override(model.width, model.height)
        trans.into(object : CustomTarget<Bitmap>() {
                override fun onLoadCleared(placeholder: Drawable?) {}
                override fun onResourceReady(resource: Bitmap, transition: Transition<in Bitmap>?) {
                    it.onNext(resource)
                }
            })
    }
        .observeOn(AndroidSchedulers.mainThread())
        .subscribeOn(Schedulers.io())
        .subscribe({ resource ->
            model.preview = resource
            if (callback == null)
                this.setImageBitmap(resource)
            else
                callback.onUpdateItem(model.position)
        }, {
            it.printStackTrace()
        })
}

fun ImageView.loadVideoScaledDown(url: String, model: VideoModel, callback: OnMediaClicked): Disposable {
    Log.d("MY_CUSTOM_TAG", url)
    return Observable.create<Pair<Bitmap, Int>> {
        val trans = Glide.with(context)
            .asBitmap()
            .load(url)
            .transition(withCrossFade())
            .thumbnail(0.1f)
            .override(model.width, model.height)
            .into(object : CustomTarget<Bitmap>() {
                override fun onLoadCleared(placeholder: Drawable?) {}
                override fun onResourceReady(resource: Bitmap, transition: Transition<in Bitmap>?) {
                    val retriever = MediaMetadataRetriever()
                    retriever.setDataSource(url, hashMapOf())
                    val time = retriever.extractMetadata(MediaMetadataRetriever.METADATA_KEY_DURATION)
                    it.onNext(Pair(resource, time.toInt()/1000))
                }
            })
    }
        .observeOn(AndroidSchedulers.mainThread())
        .subscribeOn(Schedulers.io())
        .subscribe({ result ->
            model.seconds = result.second
            model.preview = result.first
            callback.onUpdateItem(model.position)
        }, {
            it.printStackTrace()
        })
}

fun ImageView.loadUserAvatar(url: URL) {
    val requestOptions = RequestOptions().diskCacheStrategy(DiskCacheStrategy.ALL).circleCrop()
    Glide.with(this)
        .load(url)
        .transition(com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions.withCrossFade())
        .placeholder(R.drawable.tmp_profile)
        .override(resources.getDimensionPixelSize(R.dimen.avatar_size),
                  resources.getDimensionPixelSize(R.dimen.avatar_size))
        .centerCrop()
        .apply(requestOptions)
        .into(this)
}
package com.morozov.feature_chat_impl.ui.fragments.chat.dialog.renderers.chat.common.media.loaded

import android.graphics.Bitmap
import com.morozov.feature_chat_impl.ui.fragments.chat.dialog.renderers.chat.common.media.MediaModel
import java.net.URL

open class LoadedMediaModel(position: Int, url: URL?, filePath: String?, preview: Bitmap?): MediaModel(position, url, filePath, null, preview)
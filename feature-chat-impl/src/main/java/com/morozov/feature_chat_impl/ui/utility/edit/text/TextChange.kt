package com.morozov.feature_chat_impl.ui.utility.edit.text

import android.text.Editable
import android.text.TextWatcher
import android.widget.EditText

fun EditText.addMyTextChangeListener(callback:(text:String?)->Unit) {
    this.addTextChangedListener(object : TextWatcher {
        override fun afterTextChanged(p0: Editable?) {}
        override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}
        override fun onTextChanged(string: CharSequence?, p1: Int, p2: Int, p3: Int) {
            if(string == null)
                callback(null)
            else
                callback(string.toString())
        }
    })
}
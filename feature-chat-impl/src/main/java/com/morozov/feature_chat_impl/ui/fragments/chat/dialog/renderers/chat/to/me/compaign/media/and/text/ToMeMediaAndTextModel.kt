package com.morozov.feature_chat_impl.ui.fragments.chat.dialog.renderers.chat.to.me.compaign.media.and.text

import com.morozov.feature_chat_impl.ui.fragments.chat.dialog.renderers.chat.common.companion.media.and.text.MediaAndTextMessageModel
import com.morozov.feature_chat_impl.ui.fragments.chat.dialog.renderers.chat.common.media.MediaModel
import com.morozov.feature_chat_impl.ui.utility.ViewableMessageModel.Companion.NO_VIEWABLE
import com.morozov.feature_chat_impl.ui.utility.message.direction.ToMeMessage
import com.morozov.feature_chat_impl.ui.utility.views.answer.message.AnswerViewConfigs
import java.net.URL
import java.util.*

class ToMeMediaAndTextModel(override val userId: Long,
                            override val userName: String,
                            override var messageId: Long?,
                            override val userIconUrl: URL? = null,
                            override var isShow: Boolean = true,
                            media: List<MediaModel>,
                            text: String,
                            date: Calendar,
                            override var isSelected: Boolean = false,
                            override var views: Int = NO_VIEWABLE,
                            answerViewConfigs: AnswerViewConfigs? = null
) : MediaAndTextMessageModel(userId, userName, messageId, answerViewConfigs, media, text, date, isSelected, views), ToMeMessage
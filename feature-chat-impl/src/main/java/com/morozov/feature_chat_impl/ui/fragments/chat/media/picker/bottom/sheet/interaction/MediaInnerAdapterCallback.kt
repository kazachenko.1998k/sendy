package com.morozov.feature_chat_impl.ui.fragments.chat.media.picker.bottom.sheet.interaction

interface MediaInnerAdapterCallback {
    fun onItemClicked(position: Int)
    fun showMedia(position: Int)
}
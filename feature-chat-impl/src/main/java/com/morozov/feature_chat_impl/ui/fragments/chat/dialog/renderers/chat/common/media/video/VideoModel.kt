package com.morozov.feature_chat_impl.ui.fragments.chat.dialog.renderers.chat.common.media.video

import android.graphics.Bitmap
import com.morozov.feature_chat_impl.ui.fragments.chat.dialog.renderers.chat.common.media.MediaModel
import com.morozov.feature_chat_impl.ui.fragments.chat.dialog.renderers.chat.common.media.loaded.video.VideoLoadedModel
import java.net.URL

class VideoModel(position: Int, url: URL?, filePath: String?, loadProgress: Int?, preview: Bitmap?, var seconds: Int): MediaModel(position, url, filePath, loadProgress, preview) {
    fun toLoaded(): VideoLoadedModel =
        VideoLoadedModel(
            position,
            url,
            filePath,
            preview,
            seconds
        ).also { it.isSingle = this.isSingle }
}
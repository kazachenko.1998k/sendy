package com.morozov.feature_chat_impl.ui.fragments.chat.dialog.renderers.chat.from.me.file

import android.util.Log
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import com.github.vivchar.rendererrecyclerviewadapter.binder.ViewBinder
import com.github.vivchar.rendererrecyclerviewadapter.binder.ViewFinder
import com.morozov.feature_chat_impl.R
import com.morozov.feature_chat_impl.ui.fragments.chat.dialog.utils.OnFileClickListener
import com.morozov.feature_chat_impl.ui.fragments.chat.media.picker.bottom.sheet.utils.getMimeType
import com.morozov.feature_chat_impl.ui.fragments.chat.media.picker.bottom.sheet.utils.getName
import com.morozov.feature_chat_impl.ui.fragments.chat.media.picker.bottom.sheet.utils.getSize
import com.morozov.feature_chat_impl.ui.utility.ViewableMessageModel
import java.text.SimpleDateFormat

class FromMeFileViewBinder(private val listener: OnFileClickListener): ViewBinder.Binder<FromMeFileModel> {
    override fun bindView(model: FromMeFileModel, finder: ViewFinder, payloads: MutableList<Any>) {
        val textDocName = finder.find<TextView>(R.id.textDocName)
        val textDocInfo = finder.find<TextView>(R.id.textDocInfo)
        if (payloads.isEmpty()) {
            val imageViewsNumber = finder.find<ImageView>(R.id.imageViewsNumber)
            val textViewsNumber = finder.find<TextView>(R.id.textViewsNumber)

            if (model.views == ViewableMessageModel.NO_VIEWABLE) {
                imageViewsNumber.visibility = View.GONE
                textViewsNumber.visibility = View.GONE
            } else {
                imageViewsNumber.visibility = View.VISIBLE
                textViewsNumber.visibility = View.VISIBLE
                textViewsNumber.text = model.views.toString()
            }

            Log.i("Document", "Uri: ${model.uri}")
            textDocName.text = model.uri.getName(textDocName.context)
            val size = model.uri.getSize(textDocName.context)
            val mime = model.uri.getMimeType(textDocName.context)
            textDocInfo.text = "$size • $mime"

            finder.setText(R.id.timeMsg, SimpleDateFormat("HH:mm").format(model.date.time))

            finder.setOnClickListener(R.id.linearRootDoc) { listener.onFileClicked(model.uri) }
        }
    }
}
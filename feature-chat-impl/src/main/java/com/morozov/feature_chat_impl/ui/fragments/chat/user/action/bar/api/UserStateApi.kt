package com.morozov.feature_chat_impl.ui.fragments.chat.user.action.bar.api

import java.util.*

interface UserStateApi {
    fun showWasOnline(isMale: Boolean, date: Calendar)
    fun showOnline()
    fun showChanelSubs(count: Int)
    fun showGroupUsers(count: Int)

    fun showNoInternet()
}
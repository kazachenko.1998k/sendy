package com.morozov.feature_chat_impl.ui.fragments.chat.dialog.utils

interface SelectableModel {
    var isSelected: Boolean
}
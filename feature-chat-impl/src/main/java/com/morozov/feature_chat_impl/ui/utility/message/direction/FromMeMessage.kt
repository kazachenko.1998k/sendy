package com.morozov.feature_chat_impl.ui.utility.message.direction

import com.morozov.feature_chat_impl.ui.utility.message.MessageModel

interface FromMeMessage: MessageModel {
    var readStatus: ReadStatus

    enum class ReadStatus {
        SENDING,
        SENT,
        USER_RECEIVED,
        USER_READ,
        ERROR
    }
}
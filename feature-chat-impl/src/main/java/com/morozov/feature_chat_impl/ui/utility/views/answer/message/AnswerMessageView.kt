package com.morozov.feature_chat_impl.ui.utility.views.answer.message

import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import android.view.View
import android.widget.FrameLayout
import com.bumptech.glide.Glide
import com.morozov.feature_chat_impl.R
import kotlinx.android.synthetic.main.view_answer_message.view.*

class AnswerMessageView: FrameLayout {

    private lateinit var mConfigs: AnswerViewConfigs

    constructor(context: Context) : this(context, null)
    constructor(context: Context, attrs: AttributeSet?) : this(context, attrs, 0)
    constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int) : super(context, attrs, defStyleAttr) {
        LayoutInflater.from(context).inflate(R.layout.view_answer_message, this, true)
    }

    fun init() {
        textUserName.text = ""
    }

    fun setLight() {
        viewAccentLine.setBackgroundColor(resources.getColor(R.color.lightAccent))
        textUserName.setTextColor(resources.getColor(R.color.lightAccent))
        textUserMessage.setTextColor(resources.getColor(R.color.text_white))
    }

    fun setDark() {
        viewAccentLine.setBackgroundColor(resources.getColor(R.color.colorAccent))
        textUserName.setTextColor(resources.getColor(R.color.colorAccent))
        textUserMessage.setTextColor(resources.getColor(R.color.text_main))
    }

    fun setViewConfigs(configs: AnswerViewConfigs) {
        mConfigs = configs
        applyConfigs()
    }

    private fun applyConfigs() {
        val configs = mConfigs
        constraintRoot.setOnClickListener {
            configs.clickListener.onClick(configs.messageItem)
        }
        if (configs.photoThumbnailPath != null) {
            imageUserMessage.visibility = View.VISIBLE
            loadMediaThumbnail(configs.photoThumbnailPath)
        } else {
            imageUserMessage.visibility = View.GONE
        }
        textUserName.text = configs.userName
        textUserMessage.text = configs.userMessage
    }

    private fun loadMediaThumbnail(mediaPath: String) {
        Glide.with(imageUserMessage)
            .load(mediaPath)
            .centerCrop()
            .override(24, 24)
            .into(imageUserMessage)
    }
}
package com.morozov.feature_chat_impl.ui.utility.views.chanels

import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import android.view.View
import android.widget.FrameLayout
import com.morozov.feature_chat_impl.R
import kotlinx.android.synthetic.main.view_chanels_footer.view.*

class ChanelFooterSettingsView: FrameLayout,
    ChanelFooterSettingsApi {

    lateinit var mCallback: ChanelFooterSettingsCallback

    constructor(context: Context) : this(context, null)
    constructor(context: Context, attrs: AttributeSet?) : this(context, attrs, 0)
    constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int) : super(context, attrs, defStyleAttr) {
        LayoutInflater.from(context).inflate(R.layout.view_chanels_footer, this, true)

        textSubscribe.setOnClickListener {
            mCallback.onSubscribe()
        }
        textDisableNotif.setOnClickListener {
            mCallback.onDisableNotif()
        }
        textEnableNotif.setOnClickListener {
            mCallback.onEnableNotif()
        }
        textDiscussEnableNotif.setOnClickListener {
            mCallback.onDiscussEnableNotif()
        }
        textDiscussDisableNotif.setOnClickListener {
            mCallback.onDiscussDisableNotif()
        }
        textDiscuss.setOnClickListener {
            mCallback.onDiscuss()
        }
        textJoinChat.setOnClickListener {
            mCallback.onJoinChat()
        }
    }

    fun showProgress() {
        hideAll()
        progressBar.visibility = View.VISIBLE
    }

    override fun showSubscribe() {
        hideAll()
        textSubscribe.visibility = View.VISIBLE
    }

    override fun showDisableNotif() {
        hideAll()
        textDisableNotif.visibility = View.VISIBLE
    }

    override fun showEnableNotif() {
        hideAll()
        textEnableNotif.visibility = View.VISIBLE
    }

    override fun showDiscussDisableNotif() {
        hideAll()
        textDiscussDisableNotif.visibility = View.VISIBLE
        textDiscuss.visibility = View.VISIBLE
    }

    override fun showDiscussEnableNotif() {
        hideAll()
        textDiscussEnableNotif.visibility = View.VISIBLE
        textDiscuss.visibility = View.VISIBLE
    }

    private fun hideAll() {
        textJoinChat.visibility = View.GONE
        textSubscribe.visibility = View.GONE
        textDisableNotif.visibility = View.GONE
        textEnableNotif.visibility = View.GONE
        textDiscussEnableNotif.visibility = View.GONE
        textDiscussDisableNotif.visibility = View.GONE
        textDiscuss.visibility = View.GONE
        progressBar.visibility = View.GONE
    }

    override fun showJoinChat() {
        hideAll()
        textJoinChat.visibility = View.VISIBLE
    }
}
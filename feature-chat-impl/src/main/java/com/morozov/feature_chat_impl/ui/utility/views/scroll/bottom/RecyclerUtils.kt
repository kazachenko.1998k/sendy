package com.morozov.feature_chat_impl.ui.utility.views.scroll.bottom

import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView

/** @param toStart Run with true when recycler is not in the bottom, else with false.
 * */
fun RecyclerView.addShowToBottomOnScrollFunc(toStart: (b: Boolean) -> Unit) {
    addOnScrollListener(object: RecyclerView.OnScrollListener() {
        override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
            toStart(recyclerView.isInTheEndReverseByLinearLayoutManager().not())
        }
    })
}

fun RecyclerView.isInTheEndReverseByLinearLayoutManager(): Boolean {
    return (layoutManager as LinearLayoutManager).isInTheEndReverse()
}

fun LinearLayoutManager.isInTheEndReverse(): Boolean {
    val position = 1
//    if (findFirstCompletelyVisibleItemPosition() > position)
//        return findFirstVisibleItemPosition() == position
    return findFirstVisibleItemPosition() < position
}
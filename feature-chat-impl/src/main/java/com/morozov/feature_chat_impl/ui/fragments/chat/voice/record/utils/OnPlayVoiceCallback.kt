package com.morozov.feature_chat_impl.ui.fragments.chat.voice.record.utils

interface OnPlayVoiceCallback {
    fun onPlayVoice(fileName: String, userName: String, time: Int)
    fun onPlayVoice(fileName: String, userName: String, time: Int, startTime: Int)
    fun onPauseVoice(fileName: String)
}
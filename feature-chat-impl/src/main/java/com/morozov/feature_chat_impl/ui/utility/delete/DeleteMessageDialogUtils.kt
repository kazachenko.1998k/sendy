package com.morozov.feature_chat_impl.ui.utility.delete

import android.app.Dialog
import android.content.Context
import android.view.Window
import com.morozov.feature_chat_impl.R
import kotlinx.android.synthetic.main.dialog_delete_message.*
import kotlinx.android.synthetic.main.dialog_send_again.*

fun Context.showSureDeleteDialog(messagesAmount: Int, doDelete: (isForAll: Boolean) -> Unit) {
    val dialog = Dialog(this)
    dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
    dialog.setCancelable(true)
    dialog.setContentView(R.layout.dialog_delete_message)
    dialog.window?.setBackgroundDrawableResource(R.drawable.dialog_back_chat)
    val deleteHeaterText = resources.getQuantityString(R.plurals.delete_message_ask_plurals, messagesAmount)
    dialog.textDeleteHeader.text = String.format(deleteHeaterText, messagesAmount)
    dialog.action_btn.setOnClickListener {
        dialog.dismiss()
        doDelete(dialog.checkDeleteForAll.isChecked)
    }
    dialog.cancel_button.setOnClickListener {
        dialog.dismiss()
    }
    dialog.show()
}

fun Context.showSendAgainDialog(doSend: () -> Unit,
                                doDelete: () -> Unit) {
    val dialog = Dialog(this)
    dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
    dialog.setCancelable(true)
    dialog.setContentView(R.layout.dialog_send_again)
    dialog.window?.setBackgroundDrawableResource(R.drawable.dialog_back_chat)
    dialog.textSendAgain.setOnClickListener {
        dialog.dismiss()
        doSend()
    }
    dialog.textDelete.setOnClickListener {
        dialog.dismiss()
        doDelete()
    }
    dialog.show()
}
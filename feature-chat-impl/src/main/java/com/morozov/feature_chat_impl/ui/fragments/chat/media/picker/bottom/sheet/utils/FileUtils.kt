package com.morozov.feature_chat_impl.ui.fragments.chat.media.picker.bottom.sheet.utils

import android.content.ContentResolver
import android.content.Context
import android.content.Intent
import android.database.Cursor
import android.net.Uri
import android.provider.OpenableColumns
import android.webkit.MimeTypeMap
import androidx.fragment.app.Fragment


fun Uri.getName(context: Context?): String? {
    context ?: return null
    var fileName: String? = null
    val cursor = context.contentResolver
        .query(this, null, null, null, null, null)
    cursor.use { cursor ->
        if (cursor != null && cursor.moveToFirst()) { // get file name
            fileName = cursor.getString(
                cursor.getColumnIndex(OpenableColumns.DISPLAY_NAME)
            )
        }
    }
    return fileName
}

fun Uri.getMimeType(context: Context?): String? {
    val cR: ContentResolver = context?.contentResolver ?: return null
    val mime = MimeTypeMap.getSingleton()
    return mime.getExtensionFromMimeType(cR.getType(this))
}

fun Uri.getSize(context: Context?): String? {
    context ?: return null
    var fileSize: String? = null
    val cursor: Cursor? = context.contentResolver
        .query(this, null, null, null, null, null)
    cursor.use { cursor ->
        if (cursor != null && cursor.moveToFirst()) { // get file size
            val sizeIndex: Int = cursor.getColumnIndex(OpenableColumns.SIZE)
            if (!cursor.isNull(sizeIndex)) {
                fileSize = cursor.getString(sizeIndex)
            }
        }
    }
    return "$fileSize КБ"
}

fun Fragment.showFileActivity(uri: Uri) {
    val intent = Intent(Intent.ACTION_VIEW)
    intent.data = uri
    intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
    startActivity(intent)
}

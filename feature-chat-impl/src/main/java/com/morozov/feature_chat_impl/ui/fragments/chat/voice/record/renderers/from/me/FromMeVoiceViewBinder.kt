package com.morozov.feature_chat_impl.ui.fragments.chat.voice.record.renderers.from.me

import android.view.View
import android.widget.ImageView
import android.widget.ProgressBar
import android.widget.SeekBar
import android.widget.TextView
import com.github.vivchar.rendererrecyclerviewadapter.binder.ViewBinder
import com.github.vivchar.rendererrecyclerviewadapter.binder.ViewFinder
import com.mikhaellopez.circularprogressbar.CircularProgressBar
import com.morozov.feature_chat_impl.R
import com.morozov.feature_chat_impl.ui.fragments.chat.dialog.renderers.chat.common.voice.bindVoiceView
import com.morozov.feature_chat_impl.ui.fragments.chat.dialog.utils.OnErrorMessageStateClickListener
import com.morozov.feature_chat_impl.ui.fragments.chat.dialog.utils.cancel.CancelMessageCallback
import com.morozov.feature_chat_impl.ui.fragments.chat.select.messages.SelectMessageListener
import com.morozov.feature_chat_impl.ui.fragments.chat.voice.record.renderers.VoiceState
import com.morozov.feature_chat_impl.ui.fragments.chat.voice.record.utils.OnPlayVoiceCallback
import com.morozov.feature_chat_impl.ui.utility.ViewableMessageModel
import com.morozov.feature_chat_impl.ui.utility.on.message.click.OnMessageClickListener
import com.social.solutions.android.util_audio_recording.formatMillis
import java.text.SimpleDateFormat

class FromMeVoiceViewBinder(private val callback: OnPlayVoiceCallback,
                            private val selectCallback: SelectMessageListener,
                            private val onMessageClickListener: OnMessageClickListener,
                            private val onErrorMessageStateClickListener: OnErrorMessageStateClickListener
): ViewBinder.Binder<FromMeVoiceModel> {

    override fun bindView(model: FromMeVoiceModel, finder: ViewFinder, payloads: MutableList<Any>) {
        model.bindVoiceView(finder, payloads, selectCallback, onMessageClickListener, callback, onErrorMessageStateClickListener)
    }
}
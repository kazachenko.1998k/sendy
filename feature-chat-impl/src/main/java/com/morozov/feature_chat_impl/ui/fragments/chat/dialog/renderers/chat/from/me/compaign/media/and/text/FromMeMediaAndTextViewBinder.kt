package com.morozov.feature_chat_impl.ui.fragments.chat.dialog.renderers.chat.from.me.compaign.media.and.text

import android.app.Activity
import com.github.vivchar.rendererrecyclerviewadapter.binder.ViewBinder
import com.github.vivchar.rendererrecyclerviewadapter.binder.ViewFinder
import com.morozov.feature_chat_impl.ui.fragments.chat.dialog.renderers.chat.common.companion.media.and.text.bindView
import com.morozov.feature_chat_impl.ui.fragments.chat.dialog.utils.OnErrorMessageStateClickListener
import com.morozov.feature_chat_impl.ui.fragments.chat.dialog.utils.cancel.CancelMessageCallback
import com.morozov.feature_chat_impl.ui.fragments.chat.select.messages.SelectMessageListener
import com.morozov.feature_chat_impl.ui.utility.on.message.click.OnMessageClickListener

class FromMeMediaAndTextViewBinder(
    private val activity: Activity,
    private val callback: SelectMessageListener,
    private val cancelCallback: CancelMessageCallback,
    private val onMessageClickListener: OnMessageClickListener,
    private val onErrorMessageStateClickListener: OnErrorMessageStateClickListener
): ViewBinder.Binder<FromMeMediaAndTextModel> {
    override fun bindView(model: FromMeMediaAndTextModel, finder: ViewFinder, payloads: MutableList<Any>) {
        model.bindView(finder, payloads, activity, callback, cancelCallback, onMessageClickListener, onErrorMessageStateClickListener)
    }
}
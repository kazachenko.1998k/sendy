package com.morozov.feature_chat_impl.ui.utility

import android.view.View
import android.widget.ImageView
import com.morozov.feature_chat_impl.R
import com.morozov.feature_chat_impl.ui.fragments.chat.dialog.utils.SelectableModel
import com.morozov.feature_chat_impl.ui.fragments.chat.select.messages.SelectMessageListener
import com.morozov.feature_chat_impl.ui.fragments.chat.select.messages.SelectMessagesChatFragment
import com.morozov.feature_chat_impl.ui.utility.message.MessageModel
import com.morozov.feature_chat_impl.ui.utility.on.message.click.OnMessageClickListener

fun View.initLongClick(callback: SelectMessageListener,
                       onMessageClickListener: OnMessageClickListener,
                       model: SelectableModel) {
    if (model !is MessageModel)
        throw IllegalStateException("Long click is just for message model")
    if ((model as MessageModel).messageId != null) {
        this.setOnLongClickListener {
            selectMessage(this, callback, model)
            return@setOnLongClickListener true
        }
        this.setOnClickListener {
            if (callback.isSelected())
                selectMessage(this, callback, model)
            else
                onMessageClickListener.onMessageClicked(model as ViewModelWithDate)
        }
    }
    selectMessageFirst(this, model.isSelected)
}

private fun selectMessage(rootView: View, callback: SelectMessageListener, model: SelectableModel) {
    val imageMessageIsSelected = rootView.findViewById<ImageView>(R.id.imageMessageIsSelected)

    if (model.isSelected.not()) {
        model.isSelected = true
        rootView.setBackgroundColor(rootView.resources.getColor(R.color.on_long_click_color))
        imageMessageIsSelected.setImageDrawable(rootView.resources.getDrawable(R.drawable.ic_ellipse_selected_chat))
        callback.onSelected(true, model as ViewModelWithDate)
    } else {
        model.isSelected = false
        rootView.setBackgroundColor(rootView.resources.getColor(R.color.invisible))
        imageMessageIsSelected.setImageDrawable(rootView.resources.getDrawable(R.drawable.ic_ellipse_nonselected_chat))
        callback.onNonSelected(model as ViewModelWithDate)
    }
}

private fun selectMessageFirst(rootView: View, isSelected: Boolean) {
    val imageMessageIsSelected = rootView.findViewById<ImageView>(R.id.imageMessageIsSelected)

    if (SelectMessagesChatFragment.isSomethingSelected)
        imageMessageIsSelected.visibility = View.VISIBLE
    else
        imageMessageIsSelected.visibility = View.GONE

    if (isSelected) {
        rootView.setBackgroundColor(rootView.resources.getColor(R.color.on_long_click_color))
        imageMessageIsSelected.setImageDrawable(rootView.resources.getDrawable(R.drawable.ic_ellipse_selected_chat))
    }
    else {
        rootView.setBackgroundColor(rootView.resources.getColor(R.color.invisible))
        imageMessageIsSelected.setImageDrawable(rootView.resources.getDrawable(R.drawable.ic_ellipse_nonselected_chat))
    }
}
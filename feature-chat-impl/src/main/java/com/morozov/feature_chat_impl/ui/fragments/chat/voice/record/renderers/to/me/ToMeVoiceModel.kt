package com.morozov.feature_chat_impl.ui.fragments.chat.voice.record.renderers.to.me

import com.morozov.feature_chat_impl.ui.fragments.chat.dialog.renderers.chat.common.voice.VoiceMessageModel
import com.morozov.feature_chat_impl.ui.fragments.chat.voice.record.renderers.VoiceState
import com.morozov.feature_chat_impl.ui.utility.ViewModelWithDate
import com.morozov.feature_chat_impl.ui.utility.ViewableMessageModel
import com.morozov.feature_chat_impl.ui.utility.message.MessageModel
import com.morozov.feature_chat_impl.ui.utility.message.direction.ToMeMessage
import java.net.URL
import java.util.*

class ToMeVoiceModel(override val userId: Long,
                     override val userName: String,
                     override var messageId: Long?,
                     override val userIconUrl: URL?,
                     override var isShow: Boolean,
                     url: String?,
                     fileName: String?,
                     loadProgress: Float,
                     timeInSeconds:Int,
                     progress: Int,
                     state: VoiceState,
                     date: Calendar,
                     override var views: Int = ViewableMessageModel.NO_VIEWABLE,
                     isSelected: Boolean = false
): VoiceMessageModel(userId, userName, messageId, url, fileName, loadProgress, timeInSeconds, progress, state, date, views, isSelected), ToMeMessage
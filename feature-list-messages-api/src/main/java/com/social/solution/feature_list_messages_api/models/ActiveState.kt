package com.social.solution.feature_list_messages_api.models

data class ActiveState(
    var userId: Long = 0,
    var lastActive: Long?,
    var lastRequestTime: Long = System.currentTimeMillis(),
    var stateNow: Int
)
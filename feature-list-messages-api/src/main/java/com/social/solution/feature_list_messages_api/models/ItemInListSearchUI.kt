package com.social.solution.feature_list_messages_api.models

import com.morozov.core_backend_api.chat.Chat
import com.morozov.core_backend_api.user.User


class ItemInListSearchUI(
    val chat: Chat,
    var userMini: User?
) : AbstractItemInListMessageUI()


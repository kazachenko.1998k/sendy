package com.social.solution.feature_list_messages_api

import androidx.fragment.app.FragmentManager
import androidx.lifecycle.LiveData
import com.morozov.core_backend_api.message.Message

interface ListMessagesStarter {
    fun startDefault(manager: FragmentManager, container: Int,
                     addToBackStack: Boolean, callback: FeatureListMessagesCallback,
                     message: Message? = null
    ):ListMessagesFeatureApi

}
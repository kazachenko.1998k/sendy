package com.social.solution.feature_list_messages_api.models

import com.morozov.core_backend_api.chat.Chat
import com.morozov.core_backend_api.chat.UserChat
import io.reactivex.Flowable
import io.reactivex.disposables.Disposable


data class ItemInListMessageUI(
    var fullChat: Chat
) : AbstractItemInListMessageUI()


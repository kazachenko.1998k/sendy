package com.social.solution.feature_list_messages_api.models

import com.morozov.core_backend_api.chat.Chat

class ItemInListGroupName(@Chat.Companion.Type val group: Int) : AbstractItemInListMessageUI()

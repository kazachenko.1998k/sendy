package com.social.solution.feature_list_messages_api

import com.morozov.core_backend_api.chat.Chat
import com.morozov.core_backend_api.message.Message


interface FeatureListMessagesCallback {
    fun onSearchIconClick()
    fun onCreateChatClick()
    fun onPageSelected(pageNumber: Int)
    fun onUpdateCountUnreadChat(count: Int)
    fun onItemChatClickListener(item: Chat, message: Message? = null)
}
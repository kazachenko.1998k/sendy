package com.example.feature_list_profile_api.models

class ItemNotificationUI(
    val name: String = "Уведомления",
    var state: Boolean = true
) : AbstractItemUI()


package com.example.feature_list_profile_api.models

import com.morozov.core_backend_api.user.UserMini

class ItemUserUI(
    val user: UserMini
) : AbstractItemUI()

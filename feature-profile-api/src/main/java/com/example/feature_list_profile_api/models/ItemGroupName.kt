package com.example.feature_list_profile_api.models

class ItemGroupName(val group: Int, val text: String = "") : AbstractItemUI()

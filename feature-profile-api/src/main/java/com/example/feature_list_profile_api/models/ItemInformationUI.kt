package com.example.feature_list_profile_api.models

class ItemInformationUI(
    val name: String = "Информация",
    val details: String = ""
) : AbstractItemUI()


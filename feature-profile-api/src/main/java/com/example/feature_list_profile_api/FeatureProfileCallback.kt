package com.example.feature_list_profile_api

import android.widget.ImageView
import com.morozov.core_backend_api.chat.Chat
import com.morozov.core_backend_api.message.Message
import com.morozov.core_backend_api.user.User
import com.morozov.core_backend_api.user.UserMini


interface FeatureProfileCallback {
    fun onSearchIconClick(chat: Chat)
    fun onAddUserClick(chat: Chat)
    fun onAddAdminClick(chat: Chat)
    fun onUserSubscribeClick(userMini: UserMini)
    fun onUserClickListener(userMini: UserMini)
    fun onUserSubscribersClickListener(user: User)
    fun onChatClickListener(userId: Long)
    fun onFaqClickListener(user: User)
    fun onExitClickListener(chat: Chat)
    fun onAvatarShowClickListener(user: User?)
    fun onAvatarChangeClickListener(function: (url: String?) -> Unit)
    fun onEditNickClickListener(user: User)
    fun onEditNameClickListener(user: User)
    fun onEditDescriptionClickListener(user: User)
    fun onEditChatClickListener(chat: Chat)
    fun onDeleteChatClickListener(chat: Chat)
    fun onEditGroupSubscribersClickListener(chat: Chat, filter: Int)
    fun onEditPermissionsClickListener(chat: Chat, user: UserMini? = null)
    fun onEditTypeChatClickListener(chat: Chat, callback: (chat: Chat) -> Unit)
    fun onSettingClickListener(user: User)
    fun onPhotoClickListener(
        url: String,
        imageView: ImageView?,
        withVideo: Boolean,
        function: (url: String?) -> Unit
    )

    fun share(post: Message)
    fun openPost(post: Message)
    fun openComment(post: Message)
    fun openProfile(user: UserMini)
    fun createPost(resultCallback: (Boolean) -> Unit)
}
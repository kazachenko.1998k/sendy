package com.example.feature_list_profile_api

import androidx.fragment.app.FragmentManager
import androidx.lifecycle.LiveData
import com.example.util_cache.AppDatabase
import com.morozov.core_backend_api.FeatureBackendApi
import com.morozov.core_backend_api.chat.Chat
import com.morozov.core_backend_api.user.User
import com.morozov.core_backend_api.user.UserMini
import com.social.solution.feature_feed_api.FeatureFeedStarter

interface ProfileStarter {
    fun startProfile(
        manager: FragmentManager,
        container: Int,
        addToBackStack: Boolean,
        callback: FeatureProfileCallback?,
        chat: Chat,
        featureFeedStarter: FeatureFeedStarter,
        backendApi: FeatureBackendApi,
        networkState: LiveData<Boolean>,
        localDB: AppDatabase
    )

    fun startSubscribersFragment(
        manager: FragmentManager,
        container: Int,
        addToBackStack: Boolean,
        callback: FeatureProfileCallback?,
        user: User
    )

    fun startEditSubscribersGroupFragment(
        fragmentManager: FragmentManager,
        mainContainer: Int,
        callback: FeatureProfileCallback?,
        chat: Chat,
        membersFilter: Int
    )

    fun startEditPermissionsFragment(
        fragmentManager: FragmentManager,
        mainContainer: Int,
        chat: Chat,
        user: UserMini? = null
    )

    fun startEditProfileFragment(
        fragmentManager: FragmentManager,
        mainContainer: Int,
        callback: FeatureProfileCallback?,
        user: User,
        field: String
    )

    fun startEditProfileFragment(
        fragmentManager: FragmentManager,
        mainContainer: Int,
        callback: FeatureProfileCallback?,
        chat: Chat
    )
}
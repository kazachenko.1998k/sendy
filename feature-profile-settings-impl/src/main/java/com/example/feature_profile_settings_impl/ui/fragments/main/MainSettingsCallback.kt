package com.example.feature_profile_settings_impl.ui.fragments.main

interface MainSettingsCallback {
    fun onChat()
    fun onFeed()
}
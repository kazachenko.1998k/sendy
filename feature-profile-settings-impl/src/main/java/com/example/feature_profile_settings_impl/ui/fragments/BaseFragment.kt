package com.example.feature_profile_settings_impl.ui.fragments

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.activity.OnBackPressedCallback
import androidx.fragment.app.Fragment
import com.example.feature_profile_settings_impl.R
import com.example.feature_profile_settings_impl.start.MainObject
import com.example.feature_profile_settings_impl.ui.fragments.chat.ChatSettingsFragment
import com.example.feature_profile_settings_impl.ui.fragments.feed.FeedSettingsFragment
import com.example.feature_profile_settings_impl.ui.fragments.main.MainSettingsCallback
import com.example.feature_profile_settings_impl.ui.fragments.main.MainSettingsFragment

class BaseFragment: Fragment() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? =
        inflater.inflate(R.layout.fragment_base_pr_set, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        startMainSettings()
    }

    override fun onDestroy() {
        MainObject.mCallback?.onDestroy()
        super.onDestroy()
    }

    private fun startMainSettings() {
        MainSettingsFragment.start(childFragmentManager, R.id.contentProfileSettings, false,
            object : MainSettingsCallback {
                override fun onChat() {
                    startChatSettings()
                }

                override fun onFeed() {
                    startFeedSettings()
                }
            })
    }

    private fun startChatSettings() {
        ChatSettingsFragment.start(childFragmentManager, R.id.contentProfileSettings, true)
    }

    private fun startFeedSettings() {
        FeedSettingsFragment.start(childFragmentManager, R.id.contentProfileSettings, true)
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        activity?.onBackPressedDispatcher?.addCallback(this, object : OnBackPressedCallback(true) {
            override fun handleOnBackPressed() {
                if (childFragmentManager.backStackEntryCount == 0) {
                    onActivityBackPress()
                } else {
                    childFragmentManager.popBackStack()
                }
            }
        })
    }

    private fun OnBackPressedCallback.onActivityBackPress() {
        this.isEnabled = false
        activity?.onBackPressed()
    }
}
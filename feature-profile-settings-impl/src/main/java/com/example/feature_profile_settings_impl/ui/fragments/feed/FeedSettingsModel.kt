package com.example.feature_profile_settings_impl.ui.fragments.feed

data class FeedSettingsModel(
    var comment: Boolean,
    var isPublic: Boolean
)
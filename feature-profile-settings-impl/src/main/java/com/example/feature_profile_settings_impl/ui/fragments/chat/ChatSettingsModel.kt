package com.example.feature_profile_settings_impl.ui.fragments.chat

data class ChatSettingsModel(
    var online: Boolean,
    var phoneNumber: Boolean,
    var name: Boolean
)
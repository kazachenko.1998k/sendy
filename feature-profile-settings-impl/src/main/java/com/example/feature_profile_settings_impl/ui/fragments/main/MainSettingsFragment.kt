package com.example.feature_profile_settings_impl.ui.fragments.main

import android.app.Dialog
import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import com.example.feature_profile_settings_impl.R
import com.example.feature_profile_settings_impl.repository.ProfileSettingsRepository
import com.example.feature_profile_settings_impl.start.MainObject
import kotlinx.android.synthetic.main.dialog_exit_acc_pr_set.*
import kotlinx.android.synthetic.main.fragment_main_pr_set.*
import kotlinx.android.synthetic.main.header_pr_set.*

class MainSettingsFragment: Fragment() {

    companion object{
        fun start(manager: FragmentManager, container: Int, addToBackStack: Boolean, callback: MainSettingsCallback) {
            val transaction = manager.beginTransaction()
            val fragment = MainSettingsFragment()
            fragment.mCallback = callback
            transaction.replace(container, fragment)
            if (addToBackStack)
                transaction.addToBackStack(MainSettingsFragment::class.java.simpleName)
            transaction.commit()
        }
    }

    lateinit var mCallback: MainSettingsCallback

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? =
        inflater.inflate(R.layout.fragment_main_pr_set, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        imageArrowBack.setOnClickListener {
            activity?.onBackPressed()
        }

        textHeader.text = resources.getString(R.string.settings_pr_set)

        linearDeleteAcc.setOnClickListener {
            context?.let { it1 -> showDeleteDialog(it1) }
        }

        linearExit.setOnClickListener {
            context?.let { it1 -> showExitDialog(it1) }
        }

        linearChat.setOnClickListener {
            mCallback.onChat()
        }

        linearFeed.setOnClickListener {
            mCallback.onFeed()
        }
    }

    private fun showExitDialog(context: Context) {
        val dialog = Dialog(context)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setCancelable(true)
        dialog.setContentView(R.layout.dialog_exit_acc_pr_set)
        dialog.window?.setBackgroundDrawableResource(R.drawable.dialog_back_pr_set)
        dialog.action_btn.setOnClickListener {
            ProfileSettingsRepository.logOut()
            dialog.dismiss()
            MainObject.mCallback?.onLoggedOut()
        }
        dialog.cancel_button.setOnClickListener {
            dialog.dismiss()
        }
        dialog.show()
    }

    private fun showDeleteDialog(context: Context) {
        val dialog = Dialog(context)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setCancelable(true)
        dialog.setContentView(R.layout.dialog_delete_acc_pr_set)
        dialog.window?.setBackgroundDrawableResource(R.drawable.dialog_back_pr_set)
        dialog.action_btn.setOnClickListener {
            dialog.dismiss()
            ProfileSettingsRepository.deleteAcc()
            MainObject.mCallback?.onLoggedOut()
            Toast.makeText(context, R.string.toast_delete_acc_accepted_pr_set, Toast.LENGTH_LONG).show()
        }
        dialog.cancel_button.setOnClickListener {
            dialog.dismiss()
        }
        dialog.show()
    }
}
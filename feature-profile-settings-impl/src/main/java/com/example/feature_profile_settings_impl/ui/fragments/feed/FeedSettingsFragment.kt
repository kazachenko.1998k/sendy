package com.example.feature_profile_settings_impl.ui.fragments.feed

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.lifecycle.Observer
import com.example.feature_profile_settings_impl.R
import com.example.feature_profile_settings_impl.repository.ProfileSettingsRepository
import com.example.feature_profile_settings_impl.start.MainObject
import kotlinx.android.synthetic.main.fragment_feed_pr_set.*
import kotlinx.android.synthetic.main.header_pr_set.*
import java.lang.ref.SoftReference

class FeedSettingsFragment: Fragment() {

    companion object{
        fun start(manager: FragmentManager, container: Int, addToBackStack: Boolean) {
            val transaction = manager.beginTransaction()
            val fragment = FeedSettingsFragment()
            transaction.replace(container, fragment)
            if (addToBackStack)
                transaction.addToBackStack(FeedSettingsFragment::class.java.simpleName)
            transaction.commit()
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? =
        inflater.inflate(R.layout.fragment_feed_pr_set, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        ProfileSettingsRepository.getFeedSettings().observe(this, Observer {
            initFeedSettings(it)
        })

        imageArrowBack.setOnClickListener {
            activity?.onBackPressed()
        }

        textHeader.text = resources.getString(R.string.feed_pr_set)
    }

    private fun initFeedSettings(feedModel: FeedSettingsModel) {
        switchOffComment.isChecked = feedModel.comment
        radioPublic.isChecked = feedModel.isPublic
        radioPrivate.isChecked = feedModel.isPublic.not()
    }

    override fun onDestroyView() {
        MainObject.feedSettings = SoftReference(
            FeedSettingsModel(
                switchOffComment.isChecked,
                radioPublic.isChecked
            )
        )

        ProfileSettingsRepository.setComment(switchOffComment.isChecked)
        ProfileSettingsRepository.setFeedPublic(radioPublic.isChecked)

        super.onDestroyView()
    }
}
package com.example.feature_profile_settings_impl.ui.fragments.chat

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.CompoundButton
import android.widget.RadioGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.lifecycle.Observer
import com.example.feature_profile_settings_impl.R
import com.example.feature_profile_settings_impl.repository.ProfileSettingsRepository
import com.example.feature_profile_settings_impl.start.MainObject
import com.example.feature_profile_settings_impl.ui.fragments.feed.FeedSettingsFragment
import kotlinx.android.synthetic.main.fragment_chat_pr_set.*
import kotlinx.android.synthetic.main.header_pr_set.*
import java.lang.ref.SoftReference

class ChatSettingsFragment: Fragment() {

    companion object{
        fun start(manager: FragmentManager, container: Int, addToBackStack: Boolean) {
            val transaction = manager.beginTransaction()
            val fragment = ChatSettingsFragment()
            transaction.replace(container, fragment)
            if (addToBackStack)
                transaction.addToBackStack(ChatSettingsFragment::class.java.simpleName)
            transaction.commit()
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? =
        inflater.inflate(R.layout.fragment_chat_pr_set, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        ProfileSettingsRepository.getChatSettings().observe(this, Observer {
            initChatSettings(it)
        })

        imageArrowBack.setOnClickListener {
            activity?.onBackPressed()
        }

        textHeader.text = resources.getString(R.string.chat_pr_set)
    }

    private fun initChatSettings(chatModel: ChatSettingsModel) {
        switchOffOnline.isChecked = chatModel.online.not()
        switchOffPhoneNumber.isChecked = chatModel.phoneNumber.not()
        switchOffName.isChecked = chatModel.name.not()
    }

    override fun onDestroyView() {
        MainObject.chatSettings = SoftReference(
            ChatSettingsModel(
                switchOffOnline.isChecked.not(),
                switchOffPhoneNumber.isChecked.not(),
                switchOffName.isChecked.not()
            ))

        ProfileSettingsRepository.setOnline(switchOffOnline.isChecked.not())
        ProfileSettingsRepository.setPhone(switchOffPhoneNumber.isChecked.not())
        ProfileSettingsRepository.setName(switchOffName.isChecked.not())

        super.onDestroyView()
    }
}
package com.example.feature_profile_settings_impl.repository

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.example.feature_profile_settings_impl.start.MainObject
import com.example.feature_profile_settings_impl.ui.fragments.chat.ChatSettingsModel
import com.example.feature_profile_settings_impl.ui.fragments.feed.FeedSettingsModel
import com.morozov.core_backend_api.AnyInnerRequest
import com.morozov.core_backend_api.AnyRequest
import com.morozov.core_backend_api.chat.Chat
import com.morozov.core_backend_api.chat.requests.ChatGetDialogIdRequest
import com.morozov.core_backend_api.preference.BasePrefs
import com.morozov.core_backend_api.preference.FeedPrefs
import com.morozov.core_backend_api.user.requests.UserSavePrefrenceRequest
import com.morozov.core_backend_api.userPreference.UserPreference

object ProfileSettingsRepository {

    fun deleteAcc() {
        MainObject.mBackendApi?.userApi()?.delete{}
    }

    fun logOut() {

    }

    fun getChatSettings(): LiveData<ChatSettingsModel> {
        val result = MutableLiveData<ChatSettingsModel>()
        val chatCached = MainObject.chatSettings?.get()
        if (chatCached != null)
            result.postValue(chatCached)
        val userApi = MainObject.mBackendApi?.userApi()
        if (userApi != null) {
            val param = UserSavePrefrenceRequest(mutableListOf())
            val sendMessage = AnyInnerRequest(param)
            val data = AnyRequest(sendMessage)
            userApi.savePreference(data) { event ->
                val charResModel = ChatSettingsModel(online = true, phoneNumber = true, name = true)
                val eventData = event.data
                if (eventData != null) {
                    for (userPreference in eventData.preference) {
                        when(userPreference.code) {
                            BasePrefs.BASE_HIDE_LAST_ACTIVE.name -> charResModel.online =
                                userPreference.value == 0
                            BasePrefs.BASE_HIDE_PHONE.name -> charResModel.phoneNumber =
                                userPreference.value == 0
                            BasePrefs.BASE_HIDE_NAME.name -> charResModel.name =
                                userPreference.value == 0
                        }
                    }
                    result.postValue(charResModel)
                }
            }
        }
        return result
    }

    fun getFeedSettings(): LiveData<FeedSettingsModel> {
        val result = MutableLiveData<FeedSettingsModel>()
        val feedCached = MainObject.feedSettings?.get()
        if (feedCached != null)
            result.postValue(feedCached)
        val userApi = MainObject.mBackendApi?.userApi()
        if (userApi != null) {
            val param = UserSavePrefrenceRequest(mutableListOf())
            val sendMessage = AnyInnerRequest(param)
            val data = AnyRequest(sendMessage)
            userApi.savePreference(data) { event ->
                val feedResModel = FeedSettingsModel(comment = true, isPublic = true)
                val eventData = event.data
                if (eventData != null) {
                    for (userPreference in eventData.preference) {
                        when(userPreference.code) {
                            FeedPrefs.FEED_ALLOW_COMMENTS.name -> feedResModel.comment =
                                userPreference.value == 0
                            FeedPrefs.FEED_PRIVATE.name -> feedResModel.isPublic =
                                userPreference.value == 0
                        }
                    }
                    result.postValue(feedResModel)
                }
            }
        }
        return result
    }

    fun setOnline(enable: Boolean) {
        setUserPref(BasePrefs.BASE_HIDE_LAST_ACTIVE.name, if (enable) 0 else 2)
    }

    fun setPhone(enable: Boolean) {
        setUserPref(BasePrefs.BASE_HIDE_PHONE.name, if (enable) 0 else 2)
    }

    fun setName(enable: Boolean) {
        setUserPref(BasePrefs.BASE_HIDE_NAME.name, if (enable) 0 else 2)
    }

    fun setComment(enable: Boolean) {
        setUserPref(FeedPrefs.FEED_ALLOW_COMMENTS.name, if (enable) 0 else 2)
    }

    fun setFeedPublic(enable: Boolean) {
        setUserPref(FeedPrefs.FEED_PRIVATE.name, if (enable) 0 else 2)
    }

    private fun setUserPref(preferenceCone: String, value: Int) {
        val userApi = MainObject.mBackendApi?.userApi()
        if (userApi != null) {
            val preference = UserPreference(
                preferenceCone,
                value
            )
            val param = UserSavePrefrenceRequest(mutableListOf(preference))
            val sendMessage = AnyInnerRequest(param)
            val data = AnyRequest(sendMessage)
            userApi.savePreference(data) { responce ->
            }
        }
    }
}
package com.example.feature_profile_settings_impl.start

import androidx.fragment.app.FragmentManager
import com.example.feature_profile_settings_api.FeatureProfileSettingsCallback
import com.example.feature_profile_settings_api.ProfileSettingsStarterApi
import com.example.feature_profile_settings_impl.ui.fragments.BaseFragment
import com.morozov.core_backend_api.FeatureBackendApi

class ProfileSettingsStarterImpl: ProfileSettingsStarterApi {
    override fun start(
        manager: FragmentManager,
        container: Int,
        backendApi: FeatureBackendApi,
        addToBackStack: Boolean,
        callback: FeatureProfileSettingsCallback
    ) {
        MainObject.mBackendApi = backendApi
        MainObject.mCallback = callback
        val fragment = BaseFragment()
        val transaction = manager.beginTransaction()
        transaction.replace(container, fragment)
        if (addToBackStack)
            transaction.addToBackStack(BaseFragment::class.java.simpleName)
        transaction.commit()
    }
}
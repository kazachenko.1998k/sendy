package com.example.feature_profile_settings_impl.start

import com.example.feature_profile_settings_api.FeatureProfileSettingsCallback
import com.example.feature_profile_settings_impl.ui.fragments.chat.ChatSettingsModel
import com.example.feature_profile_settings_impl.ui.fragments.feed.FeedSettingsModel
import com.morozov.core_backend_api.FeatureBackendApi
import java.lang.ref.SoftReference

object MainObject {
    var mBackendApi: FeatureBackendApi? = null
    var mCallback: FeatureProfileSettingsCallback? = null

    internal var chatSettings: SoftReference<ChatSettingsModel>? = null
    internal var feedSettings: SoftReference<FeedSettingsModel>? = null
}
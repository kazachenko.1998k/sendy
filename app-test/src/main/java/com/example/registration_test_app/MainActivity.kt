package com.example.registration_test_app

import android.annotation.SuppressLint
import android.content.Context
import android.os.Build
import android.os.Bundle
import android.provider.Settings
import android.util.Base64.DEFAULT
import android.util.Base64.encodeToString
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.MutableLiveData
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.morozov.core_backend_api.FeatureBackendApi
import com.morozov.core_backend_api.authorization.modelsRequest.AuthReq
import com.morozov.core_backend_api.authorization.modelsRequest.InitRequest
import com.morozov.lib_backend.LibBackendDependency
import com.social.solution.feature_auth_api.FeatureAuthorizationCallback
import com.social.solution.lib_auth.LibAuthDependency
import com.social.solutions.android.feature_registration_api.FeatureRegistrationCallback
import com.social.solutions.android.feature_registration_impl.RegistrationFeatureImpl
import com.social.solutions.android.feature_registration_impl.start.RegistrationStarterImpl
import java.lang.reflect.Type
import java.security.MessageDigest
import java.security.NoSuchAlgorithmException

class MainActivity : AppCompatActivity() {

    private lateinit var featureBackendApi: FeatureBackendApi
    private lateinit var mSign: String

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        initConnection()
    }

    private fun startAuth() {
        val featureAuthApi = LibAuthDependency.featureAuthApi(applicationContext, featureBackendApi)
        featureAuthApi.authorizationStarter()
            .start(
                this.supportFragmentManager,
                R.id.contentMain,
                true,
                AuthCallback(),
                MutableLiveData(),
                featureBackendApi,
                mSign
            )
    }

    private fun startRegistration() {
        RegistrationFeatureImpl(RegistrationStarterImpl()).starter().start(
            supportFragmentManager,
            R.id.contentMain,
            true,
            object : FeatureRegistrationCallback {
                override fun onRegistered() {
                    Toast.makeText(applicationContext, "Registered!", Toast.LENGTH_SHORT).show()
                }

                override fun onDestroy() {
                    clearBackStack()
                    startAuth()
                }
            },
            featureBackendApi,
            mSign
        )
    }

    inner class AuthCallback : FeatureAuthorizationCallback {
        override fun onSignedAnonym(userUID: Int) {
            val sharedPreferences =
                getSharedPreferences(getString(R.string.auth), Context.MODE_PRIVATE)
            sharedPreferences.edit()
                .putInt(getString(R.string.user_uid), userUID).apply()
            Toast.makeText(applicationContext, "Signed anonym!", Toast.LENGTH_SHORT).show()
        }

        override fun onAuthorized(isRegister: Boolean) {
            if (isRegister) {
                Toast.makeText(applicationContext, "Success", Toast.LENGTH_SHORT).show()
            } else {
                startRegistration()
            }
        }
    }

    @SuppressLint("HardwareIds")
    private fun initConnection() {
        val deviceInfo = Settings.Secure.getString(
            applicationContext.contentResolver,
            Settings.Secure.ANDROID_ID
        )
        val deviceModel = Build.BRAND + "_"+ Build.MODEL
        val sessionInfo = (deviceInfo + 10 + deviceModel).md5()

        val gson = Gson()
        val sharedPreferences = getSharedPreferences(getString(R.string.auth), Context.MODE_PRIVATE)
        val uid = sharedPreferences.getInt(getString(R.string.user_uid), 0)
        val authData = AuthReq(
            user_id = uid,
            device_hash = deviceInfo,
            device_model = deviceModel,
            user_session = sessionInfo
        )
        val personType: Type = object : TypeToken<AuthReq?>() {}.type
        val json = gson.toJson(authData, personType)
        val authString: String = encodeToString(json.toByteArray(), DEFAULT)
        val newUrl =
            "wss://dev2.sendy.systems/?auth=$authString&debug=57xbmudsIsWE7W7mLp406&app_key_id=10"
        featureBackendApi = LibBackendDependency.featureBackendApi(newUrl, this)

        val authApi = featureBackendApi.authApi()
        if (uid != 0) {
//            authApi.signAnonim(data) { event ->
//                sharedPreferences.edit().putInt(getString(R.string.user_uid), event.data!!.user.uid).apply()
//                println(event.data!!.toString())
//            }
            mSign = sharedPreferences.getString(getString(R.string.sign), null)
                ?: throw IllegalArgumentException("UID exists but no SIGN in shared preferences")
            startAuth()
        } else {
            authApi.init(sessionInfo, InitRequest(deviceInfo)) {
                mSign = it.data!!.sign
                sharedPreferences.edit()
                    .putString(getString(R.string.sign), it.data!!.sign).apply()
                startAuth()
            }
        }
    }

    fun String.md5(): String {
        val MD5 = "MD5"
        try {
            // Create MD5 Hash
            val digest = MessageDigest
                .getInstance(MD5)
            digest.update(this.toByteArray())
            val messageDigest = digest.digest()

            // Create Hex String
            val hexString = StringBuilder()
            for (aMessageDigest in messageDigest) {
                var h = Integer.toHexString(0xFF and aMessageDigest.toInt())
                while (h.length < 2) h = "0$h"
                hexString.append(h)
            }
            return hexString.toString()
        } catch (e: NoSuchAlgorithmException) {
            e.printStackTrace()
        }
        return ""
    }

    private fun clearBackStack() {
        for (i in supportFragmentManager.backStackEntryCount downTo 0) {
            supportFragmentManager.popBackStack()
        }
    }
}

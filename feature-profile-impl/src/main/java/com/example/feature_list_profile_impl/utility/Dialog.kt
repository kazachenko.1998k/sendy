package com.example.feature_list_profile_impl.utility

import android.app.Dialog
import android.content.Context
import android.graphics.Typeface
import android.text.Spannable
import android.text.SpannableString
import android.text.style.StyleSpan
import android.view.Window
import androidx.core.content.res.ResourcesCompat
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.bitmap.BitmapTransitionOptions
import com.bumptech.glide.request.RequestOptions
import com.example.feature_list_profile_impl.R
import kotlinx.android.synthetic.main.dialog_action_with_subscriber.*
import kotlinx.android.synthetic.main.dialog_exit_group.*

object Dialog {
    fun showDialog(
        context: Context,
        imageUrl: String?,
        title: String,
        message: String,
        selectedMessage: String,
        actionBtnText: String,
        callback: () -> Unit
    ) {
        val dialog = Dialog(context)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setCancelable(true)
        dialog.setContentView(R.layout.dialog_exit_group)
        dialog.window?.setBackgroundDrawableResource(R.drawable.dialog_background)
        dialog.title_text_dialog?.text = title
        Glide
            .with(dialog.title_text_dialog)
            .asBitmap()
            .transition(BitmapTransitionOptions.withCrossFade())
            .load(imageUrl)
            .apply(RequestOptions().circleCrop())
            .into(dialog.profile_icon)
        dialog.text_dialog?.text = message
        val typeface =
            Typeface.create(ResourcesCompat.getFont(context, R.font.roboto_medium), Typeface.BOLD)
        val wordTwo: Spannable =
            SpannableString(selectedMessage)
        wordTwo.setSpan(
            StyleSpan(typeface!!.style),
            0,
            wordTwo.length,
            Spannable.SPAN_EXCLUSIVE_EXCLUSIVE
        )
        dialog.text_dialog?.append(wordTwo, 0, wordTwo.length)
        dialog.action_btn?.text = actionBtnText
        dialog.action_btn?.setOnClickListener {
            callback()
            dialog.dismiss()
        }
        dialog.cancel_button?.setOnClickListener { dialog.dismiss() }
        dialog.show()
    }

    fun showDialog(
        layout: Int,
        context: Context,
        firstBtn: String,
        secondBtn: String? = null,
        callback: (blueBtn: Boolean) -> Unit
    ) {
        val dialog = Dialog(context)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setCancelable(true)
        dialog.setContentView(layout)
        dialog.window?.setBackgroundDrawableResource(R.drawable.dialog_background)
        dialog.first_tab?.text = firstBtn
        dialog.second_tab?.text = secondBtn
        dialog.first_tab?.setOnClickListener {
            callback(true)
            dialog.dismiss()
        }
        dialog.second_tab?.setOnClickListener {
            callback(false)
            dialog.dismiss()
        }
        dialog.show()
    }

}
package com.example.feature_list_profile_impl.utility

import android.annotation.SuppressLint
import android.widget.EditText
import com.morozov.core_backend_api.AnyInnerRequest
import com.morozov.core_backend_api.AnyRequest
import com.morozov.core_backend_api.common.request.CommonCheckFreeNickRequest
import com.morozov.lib_backend.LibBackendDependency

const val NICK_MIN_LENGTH = 5

@SuppressLint("DefaultLocale")
internal fun EditText.onEditNickListener(
    callback: (bool: Boolean) -> Unit
) {
    if (this.length() >= NICK_MIN_LENGTH) {
        LibBackendDependency.featureBackendApi().commonApi().checkFreeNick(
            AnyRequest(
                AnyInnerRequest(
                    CommonCheckFreeNickRequest(
                        this.text.toString().toLowerCase().trim()
                    )
                )
            )
        ) {
            callback(it.data?.status == true)
        }
    }
}
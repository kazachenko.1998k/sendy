package com.example.feature_list_profile_impl.utility

import android.animation.ObjectAnimator
import android.text.Spannable
import android.text.SpannableString
import android.text.style.ForegroundColorSpan
import android.widget.TextView
import com.example.feature_list_profile_impl.R

object TextCollapseAnimator {
    fun cycleTextViewExpansion(tv: TextView, collapseTextView: TextView) {
        val collapsedMaxLines = 5
        val text = if (tv.maxLines == collapsedMaxLines) "Свернуть" else "Показать полностью..."
        val animation = ObjectAnimator.ofInt(
            tv, "maxLines",
            if (tv.maxLines == collapsedMaxLines) tv.lineCount else collapsedMaxLines
        )
        collapseTextView.text = text
        animation.setDuration(200).start()
    }

    fun addSelectedText(
        view: TextView,
        standardText: String,
        color: Int = R.color.colorAccent
    ) {
        val wordTwo: Spannable =
            SpannableString(standardText)
        wordTwo.setSpan(
            ForegroundColorSpan(view.context.resources.getColor(color)),
            0,
            wordTwo.length,
            Spannable.SPAN_EXCLUSIVE_EXCLUSIVE
        )
        view.append(wordTwo, 0, wordTwo.length)
    }

}
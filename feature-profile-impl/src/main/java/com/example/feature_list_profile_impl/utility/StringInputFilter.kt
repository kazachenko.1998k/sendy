package com.example.feature_list_profile_impl.utility

import android.text.InputFilter
import android.text.Spanned

class StringInputFilter(private val maxLength: Int): InputFilter {
    override fun filter(source: CharSequence?, start: Int, end: Int,
                        dest: Spanned?, dstart: Int, dend: Int): CharSequence? {
        dest ?: return null
        return if (dest.length < maxLength) null else ""
    }
}
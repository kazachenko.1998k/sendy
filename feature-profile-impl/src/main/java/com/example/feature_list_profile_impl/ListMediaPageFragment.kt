package com.example.feature_list_profile_impl

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.GridLayoutManager
import com.example.feature_list_profile_api.FeatureProfileCallback
import com.example.feature_list_profile_api.models.AbstractItemUI
import com.example.feature_list_profile_api.models.ItemInPhotoGalleryUI
import com.example.feature_list_profile_impl.adapters.AdapterPhotoGallery
import com.example.feature_list_profile_impl.views.ItemDecorationAlbumColumns
import com.morozov.core_backend_api.AnyInnerRequest
import com.morozov.core_backend_api.AnyRequest
import com.morozov.core_backend_api.chat.requests.ChatGetMessagesRequest
import com.morozov.core_backend_api.user.User
import com.morozov.core_backend_api.user.UserMini
import com.morozov.lib_backend.LibBackendDependency
import kotlinx.android.synthetic.main.page_fragment_photo_gallery.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext


class ListMediaPageFragment(val user: User) : Fragment() {
    var callback: FeatureProfileCallback? = null
    lateinit var adapterRecycler: AdapterPhotoGallery
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        return inflater.inflate(R.layout.page_fragment_photo_gallery, container, false)
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initRecycler()
        Log.d("UPDATEMP", "" + view)
    }

    private fun initRecycler() {
        var lastId = 0L
        val countForDownload = 50
        GlobalScope.launch(Dispatchers.Main) {
            recycler_view?.layoutManager = GridLayoutManager(
                context,
                resources.getInteger(R.integer.photo_list_preview_columns)
            )
            recycler_view?.addItemDecoration(
                ItemDecorationAlbumColumns(
                    resources.getDimensionPixelSize(R.dimen.photos_list_spacing),
                    resources.getInteger(R.integer.photo_list_preview_columns)
                )
            )
            recycler_view?.adapter = withContext(Dispatchers.IO) {
                adapterRecycler = object :
                    AdapterPhotoGallery(mutableListOf(), user, this@ListMediaPageFragment) {

                    override fun loadMoreData() {
                        if (user.galleryChatId == null) return
                        if (isLoading) return
                        super.loadMoreData()
                        LibBackendDependency.featureBackendApi().chatApi().getMessages(
                            AnyRequest(
                                AnyInnerRequest(
                                    ChatGetMessagesRequest(
                                        user.galleryChatId!!,
                                        full_message = true,
                                        amount = countForDownload,
                                        message_id = lastId
                                    )
                                )
                            )
                        ) {
                            GlobalScope.launch(Dispatchers.Main) {
                                val downloadList = it.data?.full_message
                                val result = withContext(Dispatchers.IO) {
                                    val data = mutableListOf<AbstractItemUI>()
                                    lastId =
                                        if (!downloadList.isNullOrEmpty()) downloadList.last().id else 0
                                    downloadList?.forEach {
                                        it.files?.forEach { file ->
                                            data.add(ItemInPhotoGalleryUI(file))
                                        }
                                    }
                                    data
                                }
                                Log.d("UPDATEMP", "" + lastId)
                                addNewDataFromServer(result)
                                if (result.isEmpty()) {
                                    isLoading = true
                                }
                            }
                        }
                    }

                    override fun onUserClickListener(user: UserMini) {
                    }

                    override fun onPhotoClickListener(
                        url: String,
                        imageView: ImageView?,
                        withVideo: Boolean,
                        function: (url: String?) -> Unit
                    ) {
                        callback?.onPhotoClickListener(url, imageView, withVideo, function)
                    }
                }
                adapterRecycler
            }
            recycler_view?.post {
                adapterRecycler.loadMoreData()
            }
        }
    }

}
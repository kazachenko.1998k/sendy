package com.example.feature_list_profile_impl.editFragments

import android.annotation.SuppressLint
import android.app.Activity
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.load.resource.bitmap.BitmapTransitionOptions
import com.bumptech.glide.load.resource.bitmap.BitmapTransitionOptions.withCrossFade
import com.bumptech.glide.request.RequestOptions
import com.example.feature_list_profile_api.FeatureProfileCallback
import com.example.feature_list_profile_impl.FileRepository
import com.example.feature_list_profile_impl.R
import com.example.feature_list_profile_impl.utility.Dialog.showDialog
import com.morozov.core_backend_api.AnyInnerRequest
import com.morozov.core_backend_api.AnyRequest
import com.morozov.core_backend_api.SignData
import com.morozov.core_backend_api.chat.Chat
import com.morozov.core_backend_api.chat.Chat.Companion.CHANNEL
import com.morozov.core_backend_api.chat.Chat.Companion.GROUP_CHAT
import com.morozov.core_backend_api.chat.Chat.Companion.PRIVATE_CHANNELS
import com.morozov.core_backend_api.chat.requests.ChatDeleteRequest
import com.morozov.core_backend_api.chat.requests.ChatEditRequest
import com.morozov.core_backend_api.chat.requests.ChatGetMembersAmountRequest
import com.morozov.core_backend_api.chat.requests.ChatGetMembersRequest.Companion.FILTER_ADMIN
import com.morozov.core_backend_api.chat.requests.ChatGetMembersRequest.Companion.FILTER_ALL
import com.morozov.core_backend_api.chat.requests.ChatGetMembersRequest.Companion.FILTER_BLACK_LIST
import com.morozov.lib_backend.LibBackendDependency
import com.social.solutions.util_text_formators.convertToShortString
import com.social.solutions.util_text_formators.convertToVisible
import kotlinx.android.synthetic.main.fragment_profile_edit_group_channel.*
import kotlinx.android.synthetic.main.item_recycler_btn_with_count.view.*


class FragmentEditGroupOrChannel(var chat: Chat) : Fragment() {
    var callback: FeatureProfileCallback? = null
    lateinit var titleName: String
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        return inflater.inflate(R.layout.fragment_profile_edit_group_channel, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initView()
        initListeners()
        updateCount()
    }


    @SuppressLint("SetTextI18n")
    private fun initView() {
        val requestOptions = RequestOptions().circleCrop().diskCacheStrategy(DiskCacheStrategy.ALL)
        Glide
            .with(profile_image_image_view)
            .asBitmap()
            .placeholder(R.drawable.tmp_profile)
            .transition(BitmapTransitionOptions.withCrossFade())
            .load(SignData.server_file + chat.iconFileId)
            .apply(requestOptions)
            .thumbnail(0.2f)
            .into(profile_image_image_view)
        input_name.setText(chat.title)
        description_name.setText(chat.description)
        error_text.visibility = View.INVISIBLE
        updateTypeGroup()
        type_group_name.text = when (chat.type) {
            Chat.CHANNEL, Chat.PRIVATE_CHANNELS -> "Тип канала"
            Chat.GROUP_CHAT -> "Тип группы"
            else -> "Данная группа закрыта"
        }

        val accessLevel = chat.getAccessLevels()
        exit_layout.visibility =
            (accessLevel.contains(Chat.Companion.AccessLevel.DELETE_CHAT)).convertToVisible()
        when (chat.type) {
            Chat.CHANNEL, Chat.PRIVATE_CHANNELS -> name_user_text_view.text = "Удалить канал"
            Chat.GROUP_CHAT -> name_user_text_view.text = "Удалить и покинуть группу"
            else -> exit_layout.visibility = View.GONE
        }
        if (accessLevel.contains(Chat.Companion.AccessLevel.SET_ADMIN)) {
            setUserPermission(
                admin_tab_count,
                R.drawable.ic_administrator,
                R.string.admins
            ) {
                callback?.onEditGroupSubscribersClickListener(chat, FILTER_ADMIN)
            }
        }
        if (accessLevel.contains(Chat.Companion.AccessLevel.ADD_USERS)) {
            setUserPermission(
                users_tab_count,
                R.drawable.profile_ic_subscribers,
                R.string.user_subscribers
            ) {
                callback?.onEditGroupSubscribersClickListener(chat, FILTER_ALL)
            }
        }
        if (accessLevel.contains(Chat.Companion.AccessLevel.BLOCK_UNBLOCK_USER)) {
            setUserPermission(
                black_list_tab_count,
                R.drawable.ic_blacklist,
                R.string.black_list
            ) {
                callback?.onEditGroupSubscribersClickListener(chat, FILTER_BLACK_LIST)
            }
        }
        if (chat.type == GROUP_CHAT && accessLevel.contains(Chat.Companion.AccessLevel.EDIT_PROFILE)) {
            setUserPermission(
                permission_tab_count,
                R.drawable.ic_permision,
                R.string.permissions,
                "${chat.getDefaultAccessLevels()
                    .filter { Chat.validDataPermissionForGroupEdit.contains(it) }.size}/" +
                        "${Chat.validDataPermissionForGroupEdit.size}"
            ) {
                callback?.onEditPermissionsClickListener(chat)
            }
        }
        titleName = title.text.toString()
    }


    private fun updateCount() {
        LibBackendDependency.featureBackendApi().chatApi().getMembersAmount(
            AnyRequest(
                AnyInnerRequest(ChatGetMembersAmountRequest(chat.id))
            )
        ) {
            val amount = it.data?.members_amount ?: return@getMembersAmount
            setCountUserPermission(admin_tab_count, amount.admin)
            setCountUserPermission(users_tab_count, amount.all)
            setCountUserPermission(black_list_tab_count, amount.block)
        }
    }


    private fun updateTypeGroup() {
        Log.d("TEST_UPDATE_GROUP", "privateEND = " + chat.private)
        type_group_text.text = when (chat.private) {
            Chat.PUBLIC_CHAT -> resources.getString(R.string.public_type)
            else -> resources.getString(R.string.private_type)
        }
    }

    private fun initListeners() {
        SignData.connectionState.observe(viewLifecycleOwner, Observer {
            updateBtnEnabled()
        })
        input_name.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {}

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}

            @SuppressLint("SetTextI18n")
            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                error_text.visibility =
                    (input_name.text!!.isNullOrEmpty()).convertToVisible()
                updateBtnEnabled()
            }
        })
        profile_image_image_view.setOnClickListener {
            callback?.onAvatarChangeClickListener { url ->
                if (url != null)
                    changeAvatar(url)
            }
        }
        button_on_back_pressed.setOnClickListener { activity?.onBackPressed() }
        type_group_layout.setOnClickListener {
            callback?.onEditTypeChatClickListener(chat) {
                Log.d("TEST_UPDATE_GROUP", "privateEnd = " + chat.private)
                chat = it
                type_group_text?.post {
                    updateTypeGroup()
                }
            }
        }
        exit_layout.setOnClickListener { view ->
            var title = "Покинуть группу"
            var message = "Вы точно хотите покинуть группу "
            var actionBtn = "Удалить"
            if (chat.type in listOf(CHANNEL, PRIVATE_CHANNELS)) {
                title = "Покинуть канал"
                message = "Вы точно хотите покинуть "
                actionBtn = "Покинуть"
            }
            showDialog(
                context!!,
                chat.iconFileId,
                title,
                message,
                chat.title + "?",
                actionBtn
            ) {
                LibBackendDependency.featureBackendApi().chatApi().delete(
                    AnyRequest(
                        AnyInnerRequest(ChatDeleteRequest(chat.id))
                    )
                ) {
                    if (it.result) {
                        callback?.onDeleteChatClickListener(chat)
                    } else {
                        Toast
                            .makeText(
                                view.context,
                                "Что-то пошло не так: " + it.error?.msg,
                                Toast.LENGTH_SHORT
                            )
                            .show()
                    }
                }
            }

        }

        button_save.setOnClickListener { view ->
            LibBackendDependency.featureBackendApi().chatApi().edit(
                AnyRequest(
                    AnyInnerRequest(
                        ChatEditRequest(
                            chat.id,
                            input_name.text.toString(),
                            description_name.text.toString(),
                            chat.iconFileId,
                            chat.parentChatId
                        )
                    )
                )
            ) {
                if (it.result) {
                    (view.context as Activity).onBackPressed()
                } else {
                    Toast
                        .makeText(view.context, "Что-то пошло не так", Toast.LENGTH_SHORT)
                        .show()
                }
            }
        }
    }


    private fun setUserPermission(
        view: View,
        icon: Int,
        text: Int,
        count: String = "0",
        onClickListener: () -> Unit
    ) {
        view.visibility = View.VISIBLE
        view.icon_btn.setImageDrawable(resources.getDrawable(icon))
        view.text_btn.text = (resources.getString(text))
        view.count_btn.text = count
        view.setOnClickListener { onClickListener() }
    }

    private fun setCountUserPermission(
        view: View,
        count: Int
    ) {
        view.count_btn.text = count.convertToShortString()
    }


    @SuppressLint("CheckResult")
    private fun changeAvatar(url: String) {
        FileRepository.sendFile(
            this,
            mutableListOf(url),
            null
        ) {
            chat.iconFileId = it.fileId
            LibBackendDependency.featureBackendApi().chatApi().edit(
                AnyRequest(
                    AnyInnerRequest(
                        ChatEditRequest(
                            chat.id,
                            chat.title,
                            chat.description,
                            chat.iconFileId,
                            chat.parentChatId
                        )
                    )
                )
            ) {
                val requestOptions =
                    RequestOptions().circleCrop().diskCacheStrategy(DiskCacheStrategy.ALL)
                Glide
                    .with(profile_image_image_view)
                    .asBitmap()
                    .transition(withCrossFade())
                    .load(url)
                    .apply(requestOptions)
                    .into(profile_image_image_view)
            }
        }
    }


    private fun updateBtnEnabled() {
        button_save.isEnabled =
            SignData.connectionState.value == true && error_text.visibility != View.VISIBLE
    }

}


package com.example.feature_list_profile_impl.editFragments

import android.annotation.SuppressLint
import android.app.Activity
import android.os.Bundle
import android.text.Editable
import android.text.InputFilter
import android.text.InputFilter.LengthFilter
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import android.widget.EditText
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import com.example.feature_list_profile_api.FeatureProfileCallback
import com.example.feature_list_profile_impl.R
import com.example.feature_list_profile_impl.utility.EmojiInputFilter
import com.example.feature_list_profile_impl.utility.NICK_MIN_LENGTH
import com.example.feature_list_profile_impl.utility.onEditNickListener
import com.morozov.core_backend_api.AnyInnerRequest
import com.morozov.core_backend_api.AnyRequest
import com.morozov.core_backend_api.SignData
import com.morozov.core_backend_api.user.User
import com.morozov.core_backend_api.user.UserMini
import com.morozov.core_backend_api.user.requests.UserSaveProfileRequest
import com.morozov.lib_backend.LibBackendDependency
import com.social.solutions.util_text_formators.checkNickValid
import com.social.solutions.util_text_formators.convertToVisible
import kotlinx.android.synthetic.main.fragment_edit_profile.*


class FragmentEditProfile(val user: User, val field: String) : Fragment() {
    var callback: FeatureProfileCallback? = null
    lateinit var title: String
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        return inflater.inflate(R.layout.fragment_edit_profile, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initView()
        initListeners()
    }


    @SuppressLint("SetTextI18n")
    private fun initView() {

        error_text.visibility = View.INVISIBLE
        when (field) {
            "nick" -> {
                toolbar.title = "Никнейм"
                input_name.isSingleLine = true
                input_name.setText(user.nick)
                input_name.hint = "Введите никнейм, который вам нравится"
                button_save.text = "Сменить"
                input_name.filters = arrayOf(LengthFilter(32), EmojiInputFilter())
            }
            "firstName" -> {
                toolbar.title = "Изменить имя"
                input_name.isSingleLine = true
                input_name.setText(user.firstName)
                input_name.hint = "Введите ваше имя"
                button_save.text = "Сменить имя"
                input_name.filters = arrayOf(LengthFilter(32), EmojiInputFilter())
            }
            "shortBio" -> {
                toolbar.title = "О себе"
                input_name.isSingleLine = false
                input_name.setText(user.shortBio)
                input_name.hint = "Напишите пару слов о себе"
                button_save.text = "Обновить"
                input_name.filters = arrayOf<InputFilter>(LengthFilter(300))
                input_name.addTextChangedListener(object : TextWatcher {
                    override fun afterTextChanged(s: Editable?) {}

                    override fun beforeTextChanged(
                        s: CharSequence?,
                        start: Int,
                        count: Int,
                        after: Int
                    ) {
                    }

                    @SuppressLint("SetTextI18n")
                    override fun onTextChanged(
                        s: CharSequence?,
                        start: Int,
                        before: Int,
                        count: Int
                    ) {
                        val text = (input_name as EditText).text.toString()
                        val editTextRowCount = input_name.lineCount
                        if (editTextRowCount > 15) {
                            val newText = text.substring(0, text.length - 1)
                            input_name.setText("")
                            input_name.append(newText)
                        }
                    }
                })
            }
            else -> {
                throw IllegalArgumentException("Not correct field: $field. Expected: nick, firstName, shortBio")
            }
        }
        title = toolbar.title.toString()
    }

    var isWaiting = false
    private fun initListeners() {
        SignData.connectionState.observe(viewLifecycleOwner, Observer {
            updateBtnEnabled()
        })
        input_name.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {

            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {

            }

            @SuppressLint("SetTextI18n")
            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                if (field == "nick") {
                    if (field == "nick") {
                        val filterText = input_name.text?.filter { it.checkNickValid() }
                        val originText = input_name.text
                        if (filterText.toString() != originText.toString()) {
                            input_name.setText(filterText)
                            input_name.setSelection(input_name.text?.length ?: 0);
                            return
                        }
                    }
                    error_text.text = "Минимальная длина $NICK_MIN_LENGTH символов"
                    error_text.visibility =
                        (input_name.text == null || input_name.text!!.length < NICK_MIN_LENGTH).convertToVisible()
                }
                if (field == "nick") {
                    isWaiting = true
                    input_name.onEditNickListener {
                        error_text.text = "Никнейм уже занят"
                        error_text.visibility =
                            (!(it || input_name.text.toString() == user.nick)).convertToVisible()
                        isWaiting = false
                        updateBtnEnabled()
                    }
                }
                updateBtnEnabled()
            }
        })
        toolbar.setNavigationOnClickListener {
            hideKeyboard()
            activity?.onBackPressed()
        }
        button_save.setOnClickListener {
            if (error_text.visibility != View.VISIBLE && !isWaiting) {
                when (field) {
                    "nick" -> {
                        user.nick = input_name.text.toString()
                    }
                    "firstName" -> {
                        user.firstName = input_name.text.toString()
                    }
                    "shortBio" -> {
                        user.shortBio = input_name.text.toString()
                    }
                }
                LibBackendDependency.featureBackendApi().userApi().saveProfile(
                    AnyRequest(
                        AnyInnerRequest(
                            UserSaveProfileRequest(
                                lang_id = user.langId,
                                country_id = user.countryId,
                                city_id = user.cityId,
                                avatar_file_id = user.avatarFileId,
                                background_file_id = user.backgroundFileId,
                                nick = user.nick,
                                first_name = user.firstName,
                                second_name = user.secondName,
                                short_bio = user.shortBio
                            )
                        )
                    )
                ) { newUser ->
                    if (newUser.data != null && newUser.result) {
                        hideKeyboard()
                        callback?.onUserClickListener(UserMini(user.uid))
                    } else {
                        Toast
                            .makeText(
                                it.context,
                                "Что-то пошло не так: ${newUser.error}",
                                Toast.LENGTH_SHORT
                            )
                            .show()
                    }
                }
            } else {
                Toast
                    .makeText(it.context, "Идет проверка на занятость ника", Toast.LENGTH_SHORT)
                    .show()
            }
        }
    }

    fun hideKeyboard() {
        val imm: InputMethodManager =
            activity?.getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
        //Find the currently focused view, so we can grab the correct window token from it.
        var view = activity?.currentFocus
        //If no view currently has focus, create a new one, just so we can grab a window token from it
        if (view == null) {
            view = View(activity)
        }
        imm.hideSoftInputFromWindow(view.windowToken, 0)
    }

    private fun updateBtnEnabled() {
        button_save?.isEnabled =
            (SignData.connectionState.value == true && error_text.visibility != View.VISIBLE)
    }

}


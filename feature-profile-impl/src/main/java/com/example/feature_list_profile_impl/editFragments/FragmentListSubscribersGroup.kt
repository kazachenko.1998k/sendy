package com.example.feature_list_profile_impl.editFragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.example.feature_list_profile_api.FeatureProfileCallback
import com.example.feature_list_profile_api.ProfileFeatureApi
import com.example.feature_list_profile_api.models.AbstractItemUI
import com.example.feature_list_profile_api.models.ItemUserUI
import com.example.feature_list_profile_impl.R
import com.example.feature_list_profile_impl.adapters.AdapterGroupSubscribers
import com.morozov.core_backend_api.AnyInnerRequest
import com.morozov.core_backend_api.AnyRequest
import com.morozov.core_backend_api.chat.Chat
import com.morozov.core_backend_api.chat.requests.ChatGetMembersRequest
import com.morozov.core_backend_api.chat.requests.ChatGetMembersRequest.Companion.FILTER_ADMIN
import com.morozov.core_backend_api.chat.requests.ChatGetMembersRequest.Companion.FILTER_ALL
import com.morozov.core_backend_api.chat.requests.ChatGetMembersRequest.Companion.FILTER_BLACK_LIST
import com.morozov.core_backend_api.user.UserMini
import com.morozov.lib_backend.LibBackendDependency
import com.social.solutions.util_text_formators.convertToVisible
import kotlinx.android.synthetic.main.fragment_profile_subscrubers_group.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext


class FragmentListSubscribersGroup(var chat: Chat, val membersFilter: Int = FILTER_ALL) :
    Fragment(),
    ProfileFeatureApi {
    private lateinit var adapterRecycler: AdapterGroupSubscribers
    override var callback: FeatureProfileCallback? = null
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        return inflater.inflate(R.layout.fragment_profile_subscrubers_group, container, false)
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initToolbar()
        initRecycler()
    }

    private fun initToolbar() {
        title?.text = when (membersFilter) {
            FILTER_ALL -> "Подписчики"
            FILTER_BLACK_LIST -> "Чёрный список"
            FILTER_ADMIN -> "Администраторы"
            else -> "Администраторы"
        }
        button_search?.visibility = (membersFilter == FILTER_ALL).convertToVisible()
        button_on_back_pressed?.setOnClickListener {
            activity?.onBackPressed()
        }
        button_search?.setOnClickListener {
            callback?.onSearchIconClick(chat)
        }
    }

    private fun initRecycler() {
        GlobalScope.launch(Dispatchers.Main) {
            val amount = 100
            var currentPage = 0
            recycler_view?.adapter = withContext(Dispatchers.IO) {
                adapterRecycler =
                    object : AdapterGroupSubscribers(mutableListOf(), chat, membersFilter) {
                        override fun loadMoreData() {
                            if (isLoading) return
                            super.loadMoreData()
                            LibBackendDependency.featureBackendApi().chatApi().getMembers(
                                AnyRequest(
                                    AnyInnerRequest(
                                        ChatGetMembersRequest(
                                            chat.id,
                                            amount,
                                            currentPage,
                                            members_filter = membersFilter
                                        )
                                    )
                                )
                            ) {
                                GlobalScope.launch(Dispatchers.Main) {
                                    withContext(Dispatchers.IO) {
                                        val users = it.data?.users
                                        val data = mutableListOf<AbstractItemUI>()
                                        if (!users.isNullOrEmpty()) {
                                            currentPage++
                                            users.forEach {
                                                data.add(ItemUserUI(it))
                                            }
                                        }
                                        addNewDataFromServer(data)
                                        if (users.isNullOrEmpty() || users.size < amount) {
                                            isLoading = true
                                        }
                                    }
                                }
                            }
                        }

                        override fun onUserClickListener(user: UserMini) {
                            callback?.onUserClickListener(user)
                        }

                        override fun onAddUserClick(chat: Chat) {
                            callback?.onAddUserClick(chat)
                        }

                        override fun onAddAdminClick(chat: Chat) {
                            callback?.onAddAdminClick(chat)
                        }

                        override fun onChangeAccessLevel(chat: Chat, userMini: UserMini) {
                            callback?.onEditPermissionsClickListener(chat, userMini)
                        }
                    }
                adapterRecycler
            }
            if (membersFilter == FILTER_ALL) {
                LibBackendDependency.featureBackendApi().chatApi().getMembers(
                    AnyRequest(
                        AnyInnerRequest(
                            ChatGetMembersRequest(
                                chat.id,
                                500,
                                0,
                                members_filter = FILTER_ADMIN
                            )
                        )
                    )
                ) {
                    GlobalScope.launch(Dispatchers.Main) {
                        withContext(Dispatchers.IO) {
                            val users = it.data?.users ?: return@withContext
                            adapterRecycler.listAdmins = users
                            withContext(Dispatchers.Main) {
                                adapterRecycler.notifyDataSetChanged()
                            }
                        }
                    }
                }
            }
            recycler_view?.post {
                adapterRecycler.loadMoreData()
            }
        }
    }
}
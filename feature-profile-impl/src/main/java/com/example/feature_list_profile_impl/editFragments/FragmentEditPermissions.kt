package com.example.feature_list_profile_impl.editFragments

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.load.resource.bitmap.BitmapTransitionOptions.withCrossFade
import com.bumptech.glide.request.RequestOptions
import com.example.feature_list_profile_impl.R
import com.example.feature_list_profile_impl.adapters.AdapterPermissions
import com.example.feature_list_profile_impl.utility.TextCollapseAnimator.addSelectedText
import com.morozov.core_backend_api.AnyInnerRequest
import com.morozov.core_backend_api.AnyRequest
import com.morozov.core_backend_api.SignData
import com.morozov.core_backend_api.chat.Chat
import com.morozov.core_backend_api.chat.requests.ChatGetUserRoleRequest
import com.morozov.core_backend_api.chat.requests.ChatSetAdminRolesRequest
import com.morozov.core_backend_api.chat.requests.ChatSetGroupRolesRequest
import com.morozov.core_backend_api.errors.Error
import com.morozov.core_backend_api.user.UserMini
import com.morozov.lib_backend.LibBackendDependency
import com.social.solutions.util_text_formators.convertLongToCurrentType
import com.social.solutions.util_text_formators.convertToVisible
import com.social.solutions.util_text_formators.isOnline
import kotlinx.android.synthetic.main.fragment_profile_permissions.*
import kotlinx.android.synthetic.main.item_recycler_features.view.*
import kotlinx.android.synthetic.main.item_recycler_subscribers.view.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch


class FragmentEditPermissions(val chat: Chat, val user: UserMini? = null) : Fragment() {
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        return inflater.inflate(R.layout.fragment_profile_permissions, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initView()
        initListeners()
        initRecycler()
    }

    @SuppressLint("SetTextI18n")
    private fun initView() {
        if (user != null) {
            title.text = resources.getString(R.string.admins_permissions)
            profile_header?.visibility = true.convertToVisible()
            features_header?.name_group_message?.text =
                resources.getString(R.string.admins_features)
            val requestOptions =
                RequestOptions().circleCrop().diskCacheStrategy(DiskCacheStrategy.ALL)
            Glide
                .with(profile_header.profile_image_image_view)
                .asBitmap()
                .placeholder(R.drawable.tmp_profile)
                .transition(withCrossFade())
                .load(SignData.server_file + user.avatarFileId)
                .apply(requestOptions)
                .thumbnail(0.2f)
                .into(profile_header.profile_image_image_view)
            var displayName = user.firstName
            if (user.firstName.isNullOrEmpty()) {
                displayName = "@" + user.nick
            }
            profile_header.name_user_text_view?.text = displayName
            if (user.lastActive.isOnline()) {
                profile_header.nick_text_view?.text = ""
                addSelectedText(
                    profile_header.nick_text_view,
                    profile_header.nick_text_view.resources.getString(R.string.is_online)
                )
            } else {
                profile_header.nick_text_view?.text =
                    "${profile_header.nick_text_view.resources.getString(R.string.it_was_online)} " +
                            user.lastActive.convertLongToCurrentType(profile_header.nick_text_view.context)
            }
        } else {
            title.text = resources.getString(R.string.permissions)
            profile_header?.visibility = false.convertToVisible()
            features_header?.name_group_message?.text =
                resources.getString(R.string.subscribe_features)
        }
    }

    private fun initRecycler() {
        val fullList = Chat.Companion.AccessLevel.values()
        val accessLevel = if (user == null) {
            chat.getDefaultAccessLevels()
        } else {
            GlobalScope.launch(Dispatchers.IO) {
                LibBackendDependency.featureBackendApi().chatApi().getUserRole(
                    AnyRequest(
                        AnyInnerRequest(ChatGetUserRoleRequest(chat.id, user.uid))
                    )
                ) { resp ->
                    val userRole = resp.data?.user_role
                    val wasAdmin = userRole != chat.defaultUserRole
                    val changerUserRole = chat.getAccessLevels()
                    val accessLevelCurrentUser =
                        if (wasAdmin) Chat.getAccessLevels(userRole) else fullList.filter {
                            it != Chat.Companion.AccessLevel.DELETE_CHAT
                                    && changerUserRole.contains(it)
                        }
                            .toMutableList()
                    val resultList =
                        fullList.map { Pair(it, accessLevelCurrentUser.contains(it)) }
                            .filter {
                                !Chat.defaultAdminRoles.contains(it.first)
                                        && it.first != Chat.Companion.AccessLevel.EMPTY
                            }
                            .toMutableList()
                    GlobalScope.launch(Dispatchers.Main) {
                        updateRecycler(resultList)
                    }
                }
            }
            mutableListOf()
        }
        var resultList = fullList.map { Pair(it, accessLevel.contains(it)) }.toMutableList()
        resultList.remove(resultList.find { it.first == Chat.Companion.AccessLevel.EMPTY })
        resultList = if (user == null) {
            resultList.filter { Chat.validDataPermissionForGroupEdit.contains(it.first) }
                .toMutableList()
        } else {
            resultList.filter { !Chat.defaultAdminRoles.contains(it.first) }
                .toMutableList()
        }
        val adapterPermissions = AdapterPermissions(resultList, chat.getAccessLevels())
        adapterPermissions.setHasStableIds(true)
        val dividerItemDecoration =
            DividerItemDecoration(recycler_view?.context, LinearLayoutManager.VERTICAL)
        dividerItemDecoration.setDrawable(resources.getDrawable(R.drawable.divider))
        recycler_view?.addItemDecoration(dividerItemDecoration)
        recycler_view?.adapter = adapterPermissions
    }

    private fun updateRecycler(data: MutableList<Pair<Chat.Companion.AccessLevel, Boolean>>) {
        val adapterPermissions = (recycler_view?.adapter as AdapterPermissions?) ?: return
        adapterPermissions.data = data
        adapterPermissions.notifyDataSetChanged()
    }


    private fun initListeners() {
        button_on_back_pressed.setOnClickListener { activity?.onBackPressed() }
        button_ok.setOnClickListener { _ ->
            val adapter = recycler_view?.adapter as AdapterPermissions? ?: return@setOnClickListener
            if (user == null) {
                chat.defaultUserRole = Chat.setAccessLevels(adapter.getCurrentAccessLevel())
                LibBackendDependency.featureBackendApi().chatApi().setGroupRoles(
                    AnyRequest(
                        AnyInnerRequest(ChatSetGroupRolesRequest(chat.id, chat.defaultUserRole!!))
                    )
                ) {
                    if (it.result) {
                        activity?.onBackPressed()
                    } else {
                        onError(it.error)
                    }
                }
            } else {
                val adminRole = Chat.defaultAdminRoles
                var newRole = adapter.getCurrentAccessLevel()
                newRole.addAll(adminRole)
                newRole = newRole.toSet().toMutableList()
                val userRole = Chat.setAccessLevels(newRole)
                LibBackendDependency.featureBackendApi().chatApi().setAdminRoles(
                    AnyRequest(
                        AnyInnerRequest(ChatSetAdminRolesRequest(chat.id, user.uid, userRole))
                    )
                ) {
                    if (it.result) {
                        activity?.onBackPressed()
                    } else {
                        onError(it.error)
                    }
                }
            }
        }
    }

    private fun onError(error: Error?) {
        Toast
            .makeText(context, "Что-то пошло не так: " + error?.msg, Toast.LENGTH_SHORT)
            .show()
    }

}


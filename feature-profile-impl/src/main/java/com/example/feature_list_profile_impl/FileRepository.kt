package com.example.feature_list_profile_impl

import androidx.fragment.app.Fragment
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import com.example.util_load_send_workers.repository.DialogWorkRepository
import com.example.util_load_send_workers.repository.DialogWorkRepository.getLoadingLiveData
import com.example.util_load_send_workers.repository.SendFileManager
import com.morozov.core_backend_api.SignData
import com.morozov.lib_backend.LibBackendDependency


object FileRepository {
    fun sendFile(
        fragment: Fragment,
        media: MutableList<String>,
        chatId: Long?,
        callback: (DialogWorkRepository.SendingData) -> Unit
    ) {
        println("RESULT -$fragment")
        val result = MutableLiveData<Long?>()
        val transaction = SendFileManager.with(fragment.context!!)
            .setBackendApi(LibBackendDependency.featureBackendApi())
            .setChatId(chatId)
            .setUserId(SignData.uid)
            .setMessageSentLiveData(result)
            .setMessageType(0)
            .setFiles(media)
        if (chatId != null)
            transaction.sendMessage()
        else
            transaction.loadFiles()
        getLoadingLiveData().observe(fragment.viewLifecycleOwner, Observer {
            if (media.contains(it.filePath) && it.progress == 100) callback(it)
        })
    }
}
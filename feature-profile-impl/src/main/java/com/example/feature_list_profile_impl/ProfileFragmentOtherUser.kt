package com.example.feature_list_profile_impl

import android.annotation.SuppressLint
import android.content.ClipData
import android.content.ClipboardManager
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.Animation
import android.view.animation.Transformation
import android.widget.LinearLayout
import android.widget.PopupWindow
import android.widget.Toast
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentStatePagerAdapter
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.load.resource.bitmap.BitmapTransitionOptions.withCrossFade
import com.bumptech.glide.request.RequestOptions
import com.example.feature_list_profile_api.FeatureProfileCallback
import com.example.feature_list_profile_api.ProfileFeatureApi
import com.example.feature_list_profile_impl.utility.TextCollapseAnimator.addSelectedText
import com.example.feature_list_profile_impl.utility.TextCollapseAnimator.cycleTextViewExpansion
import com.example.util_cache.Repository
import com.google.android.material.snackbar.Snackbar
import com.morozov.core_backend_api.AnyInnerRequest
import com.morozov.core_backend_api.AnyRequest
import com.morozov.core_backend_api.SignData
import com.morozov.core_backend_api.message.Message
import com.morozov.core_backend_api.phoneContact.PhoneContact
import com.morozov.core_backend_api.user.ContactMiniDB
import com.morozov.core_backend_api.user.User
import com.morozov.core_backend_api.user.UserMini
import com.morozov.core_backend_api.user.requests.*
import com.morozov.lib_backend.LibBackendDependency
import com.social.solution.feature_feed_api.FeatureFeedCallback
import com.social.solution.feature_feed_api.FeatureFeedStarter
import com.social.solution.feature_feed_api.models.FeedConfig
import com.social.solution.feature_feed_api.models.FeedItemPostUI
import com.social.solution.feature_feed_api.models.FeedTypeFeed
import com.social.solutions.util_text_formators.convertLongToCurrentType
import com.social.solutions.util_text_formators.convertToVisible
import com.social.solutions.util_text_formators.isOnline
import kotlinx.android.synthetic.main.fragment_information_user.view.*
import kotlinx.android.synthetic.main.fragment_profile_user_other.*
import kotlinx.android.synthetic.main.fragment_profile_user_other.view.*
import kotlinx.android.synthetic.main.menu_profile_me.view.*
import kotlinx.coroutines.*


open class ProfileFragmentOtherUser(val userId: Long, val starterImpl: ProfileStarterImpl) : Fragment(), ProfileFeatureApi {
    private var adapter: TabPagerAdapter? = null
    override var callback: FeatureProfileCallback? = null
    open var user: User? = null
    private val DURACTION = 1000L
    var isContact: Boolean? = null
    var onlineListenerIsInit = false
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        return inflater.inflate(R.layout.fragment_profile_user_other, container, false)
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initListeners()
    }

    override fun onResume() {
        super.onResume()
        loadData {
            initView()
            initOnlineListener()
            if (user?.feedChatId != null) {
                initViewPager()
            }
        }
    }

    private fun loadData(function: () -> Unit) {
        CoroutineScope(Dispatchers.IO).launch {
            isContact = Repository.userDao.getSingleContactMiniById(userId) != null
        }
        LibBackendDependency.featureBackendApi().userApi().getByIds(
            AnyRequest(
                AnyInnerRequest(
                    UserGetByIdsRequest(
                        mutableListOf(userId), true
                    )
                )
            )
        ) {
            val data = it.data
            user = if (data == null || data.users.isNullOrEmpty()) {
                return@getByIds
            } else {
                it.data!!.users.first()
            }
            function()
        }
    }

    @SuppressLint("SetTextI18n")
    private fun initView() {
        val requestOptions = RequestOptions().circleCrop().diskCacheStrategy(DiskCacheStrategy.ALL)
        Glide
            .with(profile_image_image_view)
            .asBitmap()
            .placeholder(R.drawable.tmp_profile)
            .transition(withCrossFade())
            .load(SignData.server_file + user!!.avatarFileId)
            .apply(requestOptions)
            .into(profile_image_image_view)
        profile_image_image_view?.setOnClickListener {
            callback?.onAvatarShowClickListener(user)
        }
        var displayName = user!!.firstName
        if (user!!.firstName.isNullOrEmpty()) {
            displayName = "@" + user!!.nick
        }
        name_user_text_view?.text = displayName
        updateOnlineState()

        toolbar?.full_user_info?.phone_number?.visibility =
            (!user!!.phone.isNullOrEmpty()).convertToVisible()
        toolbar?.full_user_info?.phone_number_description?.visibility =
            (!user!!.phone.isNullOrEmpty()).convertToVisible()
        toolbar?.full_user_info?.user_info?.visibility =
            (!user!!.shortBio.isNullOrEmpty()).convertToVisible()
        toolbar?.full_user_info?.user_info_expand?.visibility =
            (!user!!.shortBio.isNullOrEmpty()).convertToVisible()
        toolbar?.full_user_info?.user_info_description?.visibility =
            (!user!!.shortBio.isNullOrEmpty()).convertToVisible()

        toolbar?.full_user_info?.phone_number?.text = user!!.phone
        toolbar?.full_user_info?.user_info?.text = user!!.shortBio
        toolbar?.full_user_info?.user_info?.post {
            toolbar?.full_user_info?.user_info_expand?.visibility =
                (toolbar.full_user_info.user_info.lineCount >
                        toolbar.full_user_info.user_info.maxLines)
                    .convertToVisible()
        }
        toolbar?.full_user_info?.user_nickname?.text = "@" + user!!.nick
        toolbar?.full_user_info?.user_info?.visibility =
            false.convertToVisible()
        toolbar?.full_user_info?.user_info_expand?.visibility =
            false.convertToVisible()
        toolbar?.full_user_info?.user_info_description?.visibility =
            false.convertToVisible()
        updateSubscribe()
        updateMute()
        name_user_text_view?.isSelected = true
    }

    @SuppressLint("SetTextI18n")
    private fun updateOnlineState() {
        if (user!!.lastActive.isOnline()) {
            was_online_text_view?.text = ""
            addSelectedText(
                was_online_text_view,
                was_online_text_view.resources.getString(R.string.is_online)
            )
        } else {
            was_online_text_view?.text =
                "${was_online_text_view.resources.getString(R.string.it_was_online)} " +
                        user!!.lastActive.convertLongToCurrentType(was_online_text_view.context)
        }
    }

    private fun updateSubscribe() {
        toolbar.subscribe.visibility = (!user!!.isSubscribe).convertToVisible()
        toolbar.divider2.visibility = (!user!!.isSubscribe).convertToVisible()
    }

    private fun updateMute() {
        is_mute_image_view?.visibility =
            false.convertToVisible()//TODO user.isMute.convertToVisible()
    }

    private fun initOnlineListener() {
        if (onlineListenerIsInit) return
        onlineListenerIsInit = true
        requestOnlineState()
    }

    private fun requestOnlineState() {
        LibBackendDependency.featureBackendApi().userApi().getLastActive(
            AnyRequest(
                AnyInnerRequest(UserGetLastActiveRequest(user!!.uid))
            )
        ) { resp ->
            CoroutineScope(Dispatchers.IO).launch {
                val lastActive = resp.data?.last_active
                if (lastActive != null) {
                    Repository.userDao.updateUserLastActive(user!!.uid, lastActive)
                    user!!.lastActive = lastActive
                    withContext(Dispatchers.Main) {
                        updateOnlineState()
                    }
                    delay(15 * 1000) //15 sec
                    val contextView = was_online_text_view?.context
                    if (contextView == null) {
                        onlineListenerIsInit = false
                        return@launch
                    }
                    requestOnlineState()
                }
            }
        }
    }


    open fun initListeners() {
        button_on_back_pressed?.setOnClickListener { activity?.onBackPressed() }
        button_on_menu?.setOnClickListener { showPopupMenu(it) }
        button_on_chat?.setOnClickListener { callback?.onChatClickListener(userId) }
        toolbar?.subscribe?.setOnClickListener {
            subscribe()
        }
        toolbar?.full_user_info?.phone_number?.setOnClickListener {
            val clipboard: ClipboardManager? =
                ContextCompat.getSystemService(
                    it.context,
                    ClipboardManager::class.java
                )
            val clip = ClipData.newPlainText("share link", user?.phone)
            clipboard?.setPrimaryClip(clip)
            Snackbar.make(it, R.string.phone_copyd, Snackbar.LENGTH_SHORT).show()
        }
        toolbar?.full_user_info?.user_nickname?.setOnClickListener {
            val clipboard: ClipboardManager? =
                ContextCompat.getSystemService(
                    it.context,
                    ClipboardManager::class.java
                )
            val clip = ClipData.newPlainText("share link", user?.nick)
            clipboard?.setPrimaryClip(clip)
            Snackbar.make(it, R.string.nick_copyd, Snackbar.LENGTH_SHORT).show()
        }
        toolbar?.full_user_info?.user_info_expand?.setOnClickListener {
            cycleTextViewExpansion(
                toolbar.full_user_info.user_info,
                toolbar.full_user_info.user_info_expand
            )
        }
    }

    private fun subscribe() {
        user!!.isSubscribe = !user!!.isSubscribe
        LibBackendDependency.featureBackendApi().userApi().subscribe(
            AnyRequest(
                AnyInnerRequest(
                    UserSubscribeRequest(
                        userId,
                        user!!.isSubscribe
                    )
                )
            )
        )
        if (user!!.isSubscribe) {
            collapse(toolbar.subscribe) {
                updateSubscribe()
            }
        } else {
            expand(toolbar.subscribe) {
                updateSubscribe()
            }
        }
    }

    private fun expand(v: View, callback: () -> Unit) {
        val matchParentMeasureSpec = View.MeasureSpec.makeMeasureSpec(
            (v.parent as View).width,
            View.MeasureSpec.EXACTLY
        )
        val wrapContentMeasureSpec = View.MeasureSpec.makeMeasureSpec(
            0,
            View.MeasureSpec.UNSPECIFIED
        )
        v.measure(matchParentMeasureSpec, wrapContentMeasureSpec)
        val targetHeight = v.measuredHeight

        // Older versions of android (pre API 21) cancel animations for views with a height of 0.
        v.layoutParams.height = 1

        v.visibility = View.VISIBLE
        val a: Animation = object : Animation() {
            override fun applyTransformation(
                interpolatedTime: Float,
                t: Transformation?
            ) {
                v.layoutParams.height =
                    if (interpolatedTime == 1f) LinearLayout.LayoutParams.WRAP_CONTENT else (targetHeight * interpolatedTime).toInt()
                v.requestLayout()
            }

            override fun willChangeBounds(): Boolean {
                return true
            }

        }
        a.setAnimationListener(object : Animation.AnimationListener {
            override fun onAnimationRepeat(animation: Animation?) {

            }

            override fun onAnimationEnd(animation: Animation?) {
                callback()
            }

            override fun onAnimationStart(animation: Animation?) {
            }

        })
        // Expansion speed of 1dp/ms
        a.duration = DURACTION
        v.startAnimation(a)
    }

    private fun collapse(v: View, callback: () -> Unit) {
        val initialHeight = v.measuredHeight
        val a: Animation = object : Animation() {
            override fun applyTransformation(
                interpolatedTime: Float,
                t: Transformation
            ) {
                if (interpolatedTime == 1f) {
                    v.visibility = View.GONE
                } else {
                    v.layoutParams.height =
                        initialHeight - (initialHeight * interpolatedTime).toInt()
                    v.requestLayout()
                }
            }

            override fun willChangeBounds(): Boolean {
                return true
            }
        }
        a.setAnimationListener(object : Animation.AnimationListener {
            override fun onAnimationRepeat(animation: Animation?) {

            }

            override fun onAnimationEnd(animation: Animation?) {
                callback()
            }

            override fun onAnimationStart(animation: Animation?) {
            }

        })
        // Collapse speed of 1dp/ms
        a.duration = DURACTION
        v.startAnimation(a)
    }


    private fun showPopupMenu(v: View) {
        val popupView: View =
            layoutInflater.inflate(R.layout.menu_profile_me, null)

        val popupWindow = PopupWindow(
            popupView,
            ViewGroup.LayoutParams.WRAP_CONTENT,
            ViewGroup.LayoutParams.WRAP_CONTENT
        )
        popupView.first_tab?.apply {
            //            visibility = View.GONE
            first_image?.setImageDrawable(
                resources.getDrawable(if (!user!!.isBlocked) R.drawable.ic_mute_on else R.drawable.ic_mute_off)
            )
            first_text?.text =
                getString(if (!user!!.isBlocked) R.string.mute else R.string.unmute)
            setOnClickListener {
                user!!.isBlocked = !user!!.isBlocked
//                LibBackendDependency.featureBackendApi().userApi()
//                    .block(AnyRequest(AnyInnerRequest(UserBlockRequest(userId, user!!.isBlocked))))
                Toast.makeText(v.context, "Не готово на бэке", Toast.LENGTH_SHORT).show()
                popupWindow.dismiss()
            }
        }
        popupView.second_tab?.apply {
            second_image?.setImageDrawable(
                resources.getDrawable(R.drawable.ic_copy_link)
            )
            second_text?.text =
                getString(R.string.copy_link)
            setOnClickListener {
                popupWindow.dismiss()
                val clipboard: ClipboardManager? =
                    ContextCompat.getSystemService(
                        context,
                        ClipboardManager::class.java
                    )
                val clip = ClipData.newPlainText("share link", user?.nick)
                clipboard?.setPrimaryClip(clip)
                Snackbar.make(v, R.string.link_copyd, Snackbar.LENGTH_SHORT).show()
            }
        }
        popupView.third_tab?.apply {
            third_image?.setImageDrawable(
                resources.getDrawable(if (user?.isSubscribe == true) R.drawable.ic_delete_contact else R.drawable.ic_add_contact)
            )
            third_text?.text =
                getString(if (user?.isSubscribe == true) R.string.unsubscribe else R.string.subscribe)
            setOnClickListener {
                subscribe()
                popupWindow.dismiss()
            }
        }
        popupView.fourth_tab?.apply {
            fourth_text?.text = "Данные обновляются"
            isContact?.let {
                fourth_image?.setImageDrawable(
                    resources.getDrawable(if (it) R.drawable.ic_delete_from_contact else R.drawable.ic_add_in_contact)
                )
                fourth_text?.text =
                    getString(if (it) R.string.delete_from_contact else R.string.add_in_contact)
                setOnClickListener {
                    isContact = !isContact!!
                    updateIsContact()
                    popupWindow.dismiss()
                }
            }
        }
        popupWindow.isOutsideTouchable = true
        popupWindow.isFocusable = true
        popupWindow.showAsDropDown(v, 0, -v.height)
    }

    private fun updateIsContact() {
        user?.let {
            val contact = ContactMiniDB(
                uid = it.uid,
                avatarFileId = it.avatarFileId,
                nick = it.nick,
                firstName = it.firstName,
                secondName = it.secondName,
                phone = it.phone ?: "",
                isDeleted = it.isDeleted,
                isConfirmed = it.isConfirmed,
                lastActive = it.lastActive,
                isSubscribe = it.isSubscribe
            )
            if (isContact!!) {
                LibBackendDependency.featureBackendApi().userApi().addContact(
                    AnyRequest(
                        AnyInnerRequest(
                            UserAddContactRequest(
                                PhoneContact(
                                    it.phone ?: "",
                                    it.firstName ?: "",
                                    it.secondName ?: ""
                                )
                            )
                        )
                    )
                ) {
                    CoroutineScope(Dispatchers.IO).launch {
                        Repository.userDao.insert(
                            contact
                        )
                    }
                }
            } else {
                LibBackendDependency.featureBackendApi().userApi().deleteContact(
                    AnyRequest(
                        AnyInnerRequest(
                            UserDeleteContactRequest(
                                it.uid
                            )
                        )
                    )
                ) {
                    CoroutineScope(Dispatchers.IO).launch {
                        Repository.userDao.delete(contact)
                    }
                }
            }
        }
    }


    private fun initViewPager() {
        view_pager?.offscreenPageLimit = 1
        configureTabLayout()
    }

    private fun configureTabLayout() {
        adapter =
            TabPagerAdapter(childFragmentManager)
        view_pager?.adapter = adapter
        view_pager?.currentItem = 0
        tab_layout_select_message_list?.setupWithViewPager(view_pager)
    }

    private inner class TabPagerAdapter(fm: FragmentManager) :
        FragmentStatePagerAdapter(fm) {
        var config: FeedConfig? = null
        private var feedPageFragment: Fragment? = null

        init {
            config = FeedConfig(false, 10, FeedTypeFeed.USER, user!!.feedChatId!!, false)
            feedPageFragment = MainObject.mFeatureFeedStarter.startUserFeed(
                MainObject.mNetworkState,
                MainObject.mBackendApi,
                MainObject.mLocalDB,
                FeedCallback(MainObject.mFeatureFeedStarter),
                config!!
            )
            Log.d("FEED_MODULE", "User Config-1 = " + config!!.isMyProfile)
        }


        private val mediaPageFragment = ListMediaPageFragment(user!!).also {
            callback?.let { it1 ->
                it.callback = it1
            }
        }
        private val tabTitles = arrayOf("Записи", "Медиа")

        override fun getItem(position: Int): Fragment {
            return when (position) {
                0 -> feedPageFragment!!
                else -> mediaPageFragment
            }
        }

        override fun getCount(): Int {
            return tabTitles.size
        }

        override fun getPageTitle(position: Int): CharSequence? {
            return tabTitles[position]
        }

    }

    inner class FeedCallback(private val starter: FeatureFeedStarter) : FeatureFeedCallback {
        override fun share(post: Message) {
            callback?.share(post)
        }

        override fun openPost(post: Message) {
            callback?.openPost(post)
        }

        override fun openComment(post: Message) {
            callback?.openComment(post)
        }

        override fun openProfile(user: UserMini) {
            callback?.openProfile(user)
        }

        override fun createPost(resultCallback: (Boolean) -> Unit) {
            callback?.createPost(resultCallback)
        }

        override fun openNotification() {}
        override fun removeNotify(postUI: FeedItemPostUI) {
            starter.notifyRemove(postUI)
        }

    }

}
package com.example.feature_list_profile_impl

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.load.resource.bitmap.BitmapTransitionOptions
import com.bumptech.glide.request.RequestOptions
import com.example.feature_list_profile_api.FeatureProfileCallback
import com.example.feature_list_profile_api.ProfileFeatureApi
import com.example.feature_list_profile_api.models.AbstractItemUI
import com.example.feature_list_profile_api.models.ItemUserUI
import com.example.feature_list_profile_impl.adapters.AdapterProfile
import com.example.feature_list_profile_impl.utility.Dialog.showDialog
import com.example.util_cache.Repository
import com.morozov.core_backend_api.AnyInnerRequest
import com.morozov.core_backend_api.AnyRequest
import com.morozov.core_backend_api.SignData
import com.morozov.core_backend_api.chat.Chat
import com.morozov.core_backend_api.chat.requests.ChatGetByIDRequest
import com.morozov.core_backend_api.chat.requests.ChatGetMembersRequest
import com.morozov.core_backend_api.user.User
import com.morozov.core_backend_api.user.UserMini
import com.morozov.lib_backend.LibBackendDependency
import com.social.solutions.util_text_formators.convertToShortString
import com.social.solutions.util_text_formators.convertToVisible
import com.social.solutions.util_text_formators.isMuteNow
import kotlinx.android.synthetic.main.fragment_profile_group.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext


open class ProfileFragmentGroup(open val chatId: Long) : Fragment(), ProfileFeatureApi {
    lateinit var adapterRecycler: AdapterProfile
    open var chat: Chat? = null
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        return inflater.inflate(R.layout.fragment_profile_group, container, false)
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initListeners()
        loadData {
            initView()
        }
    }

    private fun loadData(function: () -> Unit) {
        LibBackendDependency.featureBackendApi().chatApi().getByIDWithDB(
            AnyRequest(
                AnyInnerRequest(
                    ChatGetByIDRequest(
                        mutableListOf(chatId)
                    )
                )
            )
        ) {
            val data = it.data
            chat = if (data == null || data.chats.isNullOrEmpty()) {
                return@getByIDWithDB
            } else {
                it.data!!.chats.first()
            }
            function()
        }
    }

    @SuppressLint("SetTextI18n")
    private fun initView() {
        val requestOptions = RequestOptions().circleCrop().diskCacheStrategy(DiskCacheStrategy.ALL)
        Glide
            .with(profile_image_image_view)
            .asBitmap()
            .placeholder(R.drawable.tmp_profile)
            .transition(BitmapTransitionOptions.withCrossFade())
            .load(SignData.server_file + chat!!.iconFileId)
            .apply(requestOptions)
            .into(profile_image_image_view)
        profile_image_image_view?.setOnClickListener {
            callback?.onAvatarShowClickListener(
                User(
                    avatarFileId = chat!!.iconFileId,
                    firstName = chat!!.title
                )
            )
        }
        name_user_text_view?.text = chat!!.title
        val countUsers = chat!!.membersAmount ?: 0
        val caseWord = if (countUsers < 1000 && countUsers % 100 !in 10..20) {
            when (countUsers % 10) {
                1 -> "участник"
                in (2..4) -> "участника"
                else -> "участников"
            }
        } else {
            "участников"
        }
        was_online_text_view?.text = countUsers.convertToShortString() + " $caseWord"
        is_mute_image_view?.visibility = chat!!.muteTime.isMuteNow().convertToVisible()
        button_on_edit_group?.visibility = (chat!!.getAccessLevels()
            .contains(Chat.Companion.AccessLevel.EDIT_PROFILE)).convertToVisible()
        //for scroll line
        name_user_text_view?.isSelected = true
        initRecycler()
    }


    open fun initListeners() {
        button_on_back_pressed?.setOnClickListener { activity?.onBackPressed() }
        button_on_edit_group?.setOnClickListener {
            chat?.let { it1 ->
                callback?.onEditChatClickListener(
                    it1
                )
            }
        }
    }


    open fun initRecycler() {
        var currentPage = 0
        val countForDownload = 100
        GlobalScope.launch(Dispatchers.Main) {
            recycler_view?.adapter = withContext(Dispatchers.IO) {
                adapterRecycler = object : AdapterProfile(chat!!, mutableListOf()) {
                    override fun loadMoreData() {
                        if (isLoading) return
                        if (!chat.getAccessLevels()
                                .contains(Chat.Companion.AccessLevel.VIEW_SUBSCRIBERS_USERS)
                        ) {
                            isLoading = true
                            return
                        }
                        super.loadMoreData()
                        LibBackendDependency.featureBackendApi().chatApi().getMembers(
                            AnyRequest(
                                AnyInnerRequest(
                                    ChatGetMembersRequest(
                                        chatId,
                                        countForDownload,
                                        currentPage
                                    )
                                )
                            )
                        ) {
                            GlobalScope.launch(Dispatchers.Main) {
                                withContext(Dispatchers.IO) {

                                    val users = it.data?.users
                                    val data = mutableListOf<AbstractItemUI>()
                                    if (!users.isNullOrEmpty()) {
                                        currentPage++
                                        users.forEach {
                                            data.add(ItemUserUI(it))
                                        }
                                    }
                                    data.sortByDescending {
                                        var sortVal = (it as ItemUserUI).user.lastActive
                                        if (sortVal < 0) sortVal = it.user.uid
                                        sortVal
                                    }
                                    addNewDataFromServer(data)
                                    if (users.isNullOrEmpty() || users.size < countForDownload) {
                                        isLoading = true
                                    }
                                }
                            }
                        }
                    }

                    override fun onUserClickListener(user: UserMini) {
                        callback?.onUserClickListener(user)

                    }

                    override fun onAddUserClick(chat: Chat) {
                        callback?.onAddUserClick(chat)
                    }

                    override fun onExitClickListener(chat: Chat) {
                        showDialog(
                            context!!,
                            chat.iconFileId,
                            "Покинуть группу",
                            "Вы точно хотите покинуть группу ",
                            chat.title + "?",
                            "Покинуть"
                        ) {
                            callback?.onDeleteChatClickListener(chat)
                        }
                    }
                }
                adapterRecycler.setHasStableIds(false)
                adapterRecycler
            }
            recycler_view?.post {
                adapterRecycler.loadMoreData()
            }
        }
    }

    override var callback: FeatureProfileCallback? = null

}
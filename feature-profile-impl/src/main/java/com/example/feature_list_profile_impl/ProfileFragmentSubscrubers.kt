package com.example.feature_list_profile_impl

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentStatePagerAdapter
import androidx.viewpager.widget.ViewPager
import com.example.feature_list_profile_api.FeatureProfileCallback
import com.example.feature_list_profile_api.ProfileFeatureApi
import com.google.android.material.tabs.TabLayout
import com.morozov.core_backend_api.user.User
import kotlinx.android.synthetic.main.fragment_profile_subscrubers.*


class ProfileFragmentSubscrubers(val user: User) : Fragment(), ProfileFeatureApi {
    override var callback: FeatureProfileCallback? = null
    private val listMySubscribers = ListSubscribersPageFragment(user, true)
    private val listSubscribers = ListSubscribersPageFragment(user, false)
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        return inflater.inflate(R.layout.fragment_profile_subscrubers, container, false)
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initViewPager()
        initListeners()
    }

    private fun initViewPager() {
        view_pager?.offscreenPageLimit = 1
        configureTabLayout()
    }

    private fun configureTabLayout() {
        val adapter = TabPagerAdapter(
            childFragmentManager
        )
        view_pager?.adapter = adapter
        view_pager?.currentItem = 0
        tab_layout_select_message_list?.setupWithViewPager(view_pager)
    }

    private inner class TabPagerAdapter(fm: FragmentManager) :
        FragmentStatePagerAdapter(fm, BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT) {
        private val tabTitles = arrayOf("Подписки", "Подписчики")

        override fun getItem(position: Int): Fragment {
            return when (position) {
                0 -> listSubscribers
                else -> listMySubscribers
            }
        }

        override fun getCount(): Int {
            return tabTitles.size
        }

        override fun getPageTitle(position: Int): CharSequence? {
            return tabTitles[position]
        }
    }

    fun toggleRefreshing(enabled: Boolean) {
        refresh_layout?.isEnabled = enabled
    }

    private fun initListeners() {
        view_pager?.addOnPageChangeListener(object : ViewPager.OnPageChangeListener {
            override fun onPageScrollStateChanged(state: Int) {
                toggleRefreshing(state == ViewPager.SCROLL_STATE_IDLE)
            }

            override fun onPageScrolled(
                position: Int,
                positionOffset: Float,
                positionOffsetPixels: Int
            ) {
            }

            override fun onPageSelected(position: Int) {
            }
        }
        )

        callback?.let { it1 ->
            listSubscribers.callback = it1
            listMySubscribers.callback = it1
        }
        button_on_back_pressed?.setOnClickListener {
            activity?.onBackPressed()
        }
        refresh_layout?.setOnRefreshListener {
            listMySubscribers.initRecycler()
            listSubscribers.initRecycler()
            refresh_layout?.isRefreshing = false
        }
    }
}
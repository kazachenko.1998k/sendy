package com.example.feature_list_profile_impl

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.feature_list_profile_api.FeatureProfileCallback
import com.example.feature_list_profile_api.ProfileFeatureApi
import com.example.feature_list_profile_api.models.AbstractItemUI
import com.example.feature_list_profile_api.models.ItemUserUI
import com.example.feature_list_profile_impl.adapters.AdapterSubscribers
import com.morozov.core_backend_api.AnyInnerRequest
import com.morozov.core_backend_api.AnyRequest
import com.morozov.core_backend_api.user.User
import com.morozov.core_backend_api.user.UserMini
import com.morozov.core_backend_api.user.requests.UserGetSubscribersRequest
import com.morozov.lib_backend.LibBackendDependency
import kotlinx.android.synthetic.main.page_fragment_list_messages.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext


class ListSubscribersPageFragment(var user: User, var isMySubscribers: Boolean) : Fragment(),
    ProfileFeatureApi {
    private lateinit var adapterRecycler: AdapterSubscribers
    override var callback: FeatureProfileCallback? = null
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        return inflater.inflate(R.layout.page_fragment_photo_gallery, container, false)
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initRecycler()
    }

    fun initRecycler() {
        GlobalScope.launch(Dispatchers.Main) {
            val amount = 100
            var currentPage = 0
            recycler_view?.layoutManager =
                withContext(Dispatchers.IO) {
                    LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
                }
            recycler_view?.adapter = withContext(Dispatchers.IO) {
                adapterRecycler = object : AdapterSubscribers(mutableListOf()) {
                    override fun loadMoreData() {
                        if (isLoading) return
                        super.loadMoreData()
                        LibBackendDependency.featureBackendApi().userApi().getSubscribersMini(
                            AnyRequest(
                                AnyInnerRequest(
                                    UserGetSubscribersRequest(
                                        amount,
                                        null,
                                        currentPage,
                                        isMySubscribers,
                                        false
                                    )
                                )
                            )
                        ) {
                            GlobalScope.launch(Dispatchers.Main) {
                                withContext(Dispatchers.IO) {

                                    val users = it.data?.users
                                    val data = mutableListOf<AbstractItemUI>()
                                    if (!users.isNullOrEmpty()) {
                                        currentPage++
                                        users.forEach {
                                            data.add(ItemUserUI(it))
                                        }
                                    }
                                    data.sortByDescending {
                                        var sortVal = (it as ItemUserUI).user.lastActive
                                        if (sortVal < 0) sortVal = it.user.uid
                                        sortVal
                                    }
                                    addNewDataFromServer(data)
                                    if (users.isNullOrEmpty() || users.size < amount) {
                                        isLoading = true
                                    }
                                }
                            }
                        }
                    }

                    override fun onUserClickListener(user: UserMini) {
                        callback?.onUserClickListener(user)
                    }

                    override fun onAddUserClick(user: UserMini) {
                        callback?.onUserSubscribeClick(user)
                    }

                    override fun onExitClickListener() {
                    }
                }
                adapterRecycler
            }
            recycler_view?.post {
                adapterRecycler.loadMoreData()
            }
        }
    }
}
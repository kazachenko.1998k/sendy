package com.example.feature_list_profile_impl

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.feature_list_profile_api.ProfileFeatureApi
import com.example.feature_list_profile_impl.adapters.AdapterProfile
import com.example.feature_list_profile_impl.utility.Dialog.showDialog
import com.morozov.core_backend_api.chat.Chat
import com.morozov.core_backend_api.user.UserMini
import kotlinx.android.synthetic.main.fragment_profile_group.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext


class ProfileFragmentChannel(override val chatId: Long) : ProfileFragmentGroup(chatId),
    ProfileFeatureApi {
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        return inflater.inflate(R.layout.fragment_profile_channel, container, false)
    }


    override fun initListeners() {
        button_on_back_pressed?.setOnClickListener { activity?.onBackPressed() }
        button_on_edit_group?.setOnClickListener {
            chat?.let { it1 ->
                callback?.onEditChatClickListener(
                    it1
                )
            }
        }

    }


    override fun initRecycler() {
        GlobalScope.launch(Dispatchers.Main) {
            recycler_view?.adapter = withContext(Dispatchers.IO) {
                adapterRecycler = object : AdapterProfile(chat!!, mutableListOf()) {
                    override fun loadMoreData() {
                    }

                    override fun onUserClickListener(user: UserMini) {
                        callback?.onUserClickListener(user)
                    }

                    override fun onAddUserClick(chat: Chat) {
                        callback?.onAddUserClick(chat)
                    }

                    override fun onExitClickListener(chat: Chat) {
                        showDialog(
                            context!!,
                            chat.iconFileId,
                            "Покинуть канал",
                            "Вы точно хотите покинуть ",
                            chat.title + "?",
                            "Покинуть"
                        ) {
                            callback?.onDeleteChatClickListener(chat)
                        }
                    }
                }
                adapterRecycler.setHasStableIds(false)
                adapterRecycler
            }
            recycler_view?.post {
                adapterRecycler.loadMoreData()
            }
        }
    }


}
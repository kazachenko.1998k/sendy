package com.example.feature_list_profile_impl

import android.annotation.SuppressLint
import android.app.Dialog
import android.content.Context
import android.os.Bundle
import android.os.Parcelable
import android.text.Spannable
import android.text.SpannableString
import android.text.style.ForegroundColorSpan
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import android.widget.PopupWindow
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentStatePagerAdapter
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.load.resource.bitmap.BitmapTransitionOptions.withCrossFade
import com.bumptech.glide.request.RequestOptions
import com.example.feature_list_profile_api.FeatureProfileCallback
import com.example.feature_list_profile_api.ProfileFeatureApi
import com.example.feature_list_profile_impl.utility.TextCollapseAnimator.cycleTextViewExpansion
import com.morozov.core_backend_api.AnyInnerRequest
import com.morozov.core_backend_api.AnyRequest
import com.morozov.core_backend_api.SignData
import com.morozov.core_backend_api.message.Message
import com.morozov.core_backend_api.user.User
import com.morozov.core_backend_api.user.UserMini
import com.morozov.core_backend_api.user.requests.UserGetByIdsRequest
import com.morozov.core_backend_api.user.requests.UserSaveProfileRequest
import com.morozov.lib_backend.LibBackendDependency
import com.social.solution.feature_feed_api.FeatureFeedCallback
import com.social.solution.feature_feed_api.FeatureFeedStarter
import com.social.solution.feature_feed_api.models.FeedConfig
import com.social.solution.feature_feed_api.models.FeedItemPostUI
import com.social.solution.feature_feed_api.models.FeedTypeFeed
import com.social.solutions.util_text_formators.convertLongToCurrentType
import com.social.solutions.util_text_formators.convertToVisible
import com.social.solutions.util_text_formators.isOnline
import kotlinx.android.synthetic.main.dialog_selector_event_on_photo.*
import kotlinx.android.synthetic.main.fragment_information_user.view.*
import kotlinx.android.synthetic.main.fragment_profile_user_me.*
import kotlinx.android.synthetic.main.fragment_profile_user_me.view.*
import kotlinx.android.synthetic.main.menu_profile.view.first_tab
import kotlinx.android.synthetic.main.menu_profile.view.second_tab
import kotlinx.android.synthetic.main.menu_profile_me.view.*


open class ProfileFragmentMe(val userId: Long, val starterImpl: ProfileStarterImpl) : Fragment(),
    ProfileFeatureApi {
    override var callback: FeatureProfileCallback? = null
    open var user: User? = null
    private var adapter: TabPagerAdapter? = null
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        return inflater.inflate(R.layout.fragment_profile_user_me, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initListeners()
        loadData {
            initView()
            user?.feedChatId ?: return@loadData
            user?.galleryChatId ?: return@loadData
            initViewPager()
        }
    }

    private fun loadData(function: () -> Unit) {
        LibBackendDependency.featureBackendApi().userApi().getByIds(
            AnyRequest(
                AnyInnerRequest(
                    UserGetByIdsRequest(
                        mutableListOf(userId), true
                    )
                )
            )
        ) {
            val data = it.data ?: return@getByIds
            user = if (data.users.isNullOrEmpty()) {
                null
            } else {
                data.users.first()
            }
            user ?: return@getByIds
            function()
        }
    }

    @SuppressLint("SetTextI18n")
    private fun initView() {
        val requestOptions = RequestOptions().circleCrop().diskCacheStrategy(DiskCacheStrategy.ALL)
        if (profile_image_image_view != null) {
            Glide
                .with(context!!)
                .asBitmap()
                .placeholder(R.drawable.tmp_profile)
                .transition(withCrossFade())
                .load(SignData.server_file + user!!.avatarFileId)
                .apply(requestOptions)
                .into(profile_image_image_view)
            var displayName = user!!.firstName
            if (user!!.firstName.isNullOrEmpty()) {
                displayName = "@" + user!!.nick
            }
            name_user_text_view?.text = displayName
            if (user!!.lastActive.isOnline()) {
                was_online_text_view?.text = ""
                addSelectedText(
                    was_online_text_view,
                    was_online_text_view.resources.getString(R.string.is_online)
                )
            } else {
                was_online_text_view?.text =
                    "${was_online_text_view.resources.getString(R.string.it_was_online)} " +
                            user!!.lastActive.convertLongToCurrentType(was_online_text_view.context)
            }
            is_mute_image_view.visibility =
                false.convertToVisible() //TODO user!!.convertToVisible()
            toolbar?.full_user_info?.title_description?.text = "Аккаунт"
            toolbar?.full_user_info?.phone_number?.text = user!!.phone

            toolbar?.full_user_info?.user_info?.text = user!!.shortBio
            toolbar?.full_user_info?.user_info?.post {
                toolbar?.full_user_info?.user_info_expand?.visibility =
                    (toolbar.full_user_info.user_info.lineCount >
                            toolbar.full_user_info.user_info.maxLines)
                        .convertToVisible()
                toolbar?.full_user_info?.user_info_expand?.visibility =
                    false.convertToVisible()
            }
            toolbar?.full_user_info?.user_nickname?.text = "@" + user!!.nick
            name_user_text_view?.isSelected = true

            toolbar?.full_user_info?.user_info?.visibility =
                false.convertToVisible()
            toolbar?.full_user_info?.user_info_expand?.visibility =
                false.convertToVisible()
            toolbar?.full_user_info?.user_info_description?.visibility =
                false.convertToVisible()

        }
    }

    private fun addSelectedText(
        view: TextView,
        standardText: String,
        color: Int = R.color.colorAccent
    ) {
        val wordTwo: Spannable =
            SpannableString(standardText)
        wordTwo.setSpan(
            ForegroundColorSpan(view.context.resources.getColor(color)),
            0,
            wordTwo.length,
            Spannable.SPAN_EXCLUSIVE_EXCLUSIVE
        )
        view.append(wordTwo, 0, wordTwo.length)
    }

    open fun initListeners() {
        button_faq?.setOnClickListener { user?.let { it1 -> callback?.onFaqClickListener(it1) } }
        button_on_menu?.setOnClickListener { showPopupMenu(it) }
        profile_image_image_view?.setOnClickListener { showDialogAvatar(it.context) }
        toolbar?.full_user_info?.user_info?.setOnClickListener {
            user?.let { it1 ->
                callback?.onEditDescriptionClickListener(
                    it1
                )
            }
        }
        toolbar?.full_user_info?.user_info_description?.setOnClickListener {
            user?.let { it1 ->
                callback?.onEditDescriptionClickListener(
                    it1
                )
            }
        }
        toolbar?.full_user_info?.user_info_expand?.setOnClickListener {
            cycleTextViewExpansion(
                toolbar.full_user_info.user_info,
                toolbar.full_user_info.user_info_expand
            )
        }
        toolbar?.full_user_info?.user_nickname?.setOnClickListener {
            user?.let { it1 ->
                callback?.onEditNickClickListener(
                    it1
                )
            }
        }
        toolbar?.full_user_info?.user_nickname_description?.setOnClickListener {
            user?.let { it1 ->
                callback?.onEditNickClickListener(
                    it1
                )
            }
        }
    }


    private fun showPopupMenu(v: View) {
        val popupView: View =
            layoutInflater.inflate(R.layout.menu_profile_me, null)

        val popupWindow = PopupWindow(
            popupView,
            ViewGroup.LayoutParams.WRAP_CONTENT,
            ViewGroup.LayoutParams.WRAP_CONTENT
        )
        popupView.first_tab?.apply {
            setOnClickListener {
                popupWindow.dismiss()
                user?.let { it1 -> callback?.onUserSubscribersClickListener(it1) }
            }
        }
        popupView.second_tab?.apply {
            setOnClickListener {
                user?.let { it1 -> callback?.onEditNameClickListener(it1) }
                popupWindow.dismiss()
            }
        }
        popupView.third_tab?.apply {
            visibility = View.GONE
        }
        popupView.fourth_tab?.setOnClickListener {
            user?.let { it1 -> callback?.onSettingClickListener(it1) }
            popupWindow.dismiss()
        }
        popupWindow.isOutsideTouchable = true
        popupWindow.isFocusable = true
        popupWindow.showAsDropDown(v, 0, -v.height)
    }


    fun showDialogAvatar(
        context: Context
    ) {
        val dialog = Dialog(context)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setCancelable(true)
        dialog.setContentView(R.layout.dialog_selector_event_on_photo)
        dialog.window?.setBackgroundDrawableResource(R.drawable.dialog_background)
        dialog.create_photo?.setOnClickListener {
            callback?.onAvatarChangeClickListener { url ->
                if (url != null) {
                    changeAvatar(url)
                }
            }
            dialog.dismiss()
        }
        dialog.show_photo?.setOnClickListener {
            dialog.dismiss()
            callback?.onAvatarShowClickListener(user)
        }
        dialog.show()
    }


    @SuppressLint("CheckResult")
    private fun changeAvatar(url: String) {
        FileRepository.sendFile(
            this,
            mutableListOf(url),
            user!!.galleryChatId
        ) {
            starterImpl.mediaPageFragment?.adapterRecycler?.callBackAddNewPhotoInGallery(url)
            LibBackendDependency.featureBackendApi().userApi().saveProfile(
                AnyRequest(
                    AnyInnerRequest(
                        UserSaveProfileRequest(
                            lang_id = user!!.langId,
                            country_id = user!!.countryId,
                            city_id = user!!.cityId,
                            avatar_file_id = it.fileId,
                            background_file_id = user!!.backgroundFileId,
                            nick = user!!.nick,
                            first_name = user!!.firstName,
                            second_name = user!!.secondName,
                            short_bio = user!!.shortBio
                        )
                    )
                )
            ) {
                val requestOptions =
                    RequestOptions().circleCrop().diskCacheStrategy(DiskCacheStrategy.ALL)
                Glide
                    .with(profile_image_image_view)
                    .asBitmap()
                    .transition(withCrossFade())
                    .load(url)
                    .apply(requestOptions)
                    .into(profile_image_image_view)
            }
        }
    }

    private fun initViewPager() {
        view_pager?.offscreenPageLimit = 1
        configureTabLayout()
    }

    private fun configureTabLayout() {
        Log.d("FEED_MODULE", "ME profile configureTabLayout")
        Log.d("FEED_MODULE", "ME profile init TabAdapter")
        if (starterImpl.config == null) {
            starterImpl.config =
                FeedConfig(true, 10, FeedTypeFeed.USER, user!!.feedChatId!!, true)
            Log.d("FEED_MODULE", "ME profile create config")
        }
        starterImpl.feedPageFragment = MainObject.mFeatureFeedStarter.startUserFeed(
            MainObject.mNetworkState,
            MainObject.mBackendApi,
            MainObject.mLocalDB,
            FeedCallback(MainObject.mFeatureFeedStarter),
            starterImpl.config!!
        )
        starterImpl.mediaPageFragment = ListMediaPageFragment(user!!).also {
            callback?.let { it1 ->
                it.callback = it1
            }
        }
        view_pager?.offscreenPageLimit = 2
        Log.d("FEED_MODULE", "ME profile start UserFeed")
        Log.d("FEED_MODULE", "viewPager = " + view_pager)
        Log.d("FEED_MODULE", "tab_layout_select_message_list = " + tab_layout_select_message_list)
        if (adapter != null) {
//            childFragmentManager.beginTransaction().remove(starterImpl.feedPageFragment!!)
        } else {
            Log.d("FEED_MODULE", "NEW ADAPTER!!!!!!!!!!")
            adapter = TabPagerAdapter(childFragmentManager)
        }
        view_pager?.adapter = adapter
        view_pager?.currentItem = 0
        tab_layout_select_message_list?.removeAllTabs()
        tab_layout_select_message_list?.setupWithViewPager(view_pager)
    }


    private inner class TabPagerAdapter(private val fm: FragmentManager) :
        FragmentStatePagerAdapter(fm) {

//        override fun saveState(): Parcelable? {
//            return Bundle()
//        }

        private val tabTitles = arrayOf("Записи", "Медиа")

        override fun getItem(position: Int): Fragment {

            return when (position) {
                0 -> {
                    Log.d("FEED_MODULE", "init = ")
                    starterImpl.feedPageFragment!!
                }
                else -> starterImpl.mediaPageFragment!!
            }
        }

        override fun restoreState(state: Parcelable?, loader: ClassLoader?) {

        }

        override fun getCount(): Int {
            return tabTitles.size
        }

        override fun getPageTitle(position: Int): CharSequence? {
            return tabTitles[position]
        }

    }

    inner class FeedCallback(private val starter: FeatureFeedStarter) : FeatureFeedCallback {
        override fun share(post: Message) {
            callback?.share(post)
        }

        override fun openPost(post: Message) {
            callback?.openPost(post)
        }

        override fun openComment(post: Message) {
            callback?.openComment(post)
        }

        override fun openProfile(user: UserMini) {
            callback?.openProfile(user)
        }

        override fun createPost(resultCallback: (Boolean) -> Unit) {
            callback?.createPost(resultCallback)
        }

        override fun openNotification() {}
        override fun removeNotify(postUI: FeedItemPostUI) {
            starter.notifyRemove(postUI)
        }

    }

}
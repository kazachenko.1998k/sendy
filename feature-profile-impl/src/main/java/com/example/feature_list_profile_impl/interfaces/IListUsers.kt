package com.example.feature_list_profile_impl.interfaces

import com.morozov.core_backend_api.user.UserMini

interface IListUsers {
    fun onUserClickListener(user: UserMini)
    fun loadMoreData()
}
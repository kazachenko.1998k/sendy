package com.example.feature_list_profile_impl

import androidx.lifecycle.LiveData
import com.example.util_cache.AppDatabase
import com.morozov.core_backend_api.FeatureBackendApi
import com.social.solution.feature_feed_api.FeatureFeedApi
import com.social.solution.feature_feed_api.FeatureFeedStarter

object MainObject {
    lateinit var mFeatureFeedStarter : FeatureFeedStarter
    lateinit var mNetworkState : LiveData<Boolean>
    lateinit var mBackendApi : FeatureBackendApi
    lateinit var mLocalDB: AppDatabase

}
package com.example.feature_list_profile_impl.adapters

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.load.resource.bitmap.BitmapTransitionOptions.withCrossFade
import com.bumptech.glide.request.RequestOptions
import com.example.feature_list_profile_api.models.AbstractItemUI
import com.example.feature_list_profile_api.models.ItemUserUI
import com.example.feature_list_profile_impl.R
import com.morozov.core_backend_api.SignData
import com.morozov.core_backend_api.user.UserMini
import com.social.solutions.util_text_formators.convertToVisible
import kotlinx.android.synthetic.main.item_recycler_subscribers.view.*


abstract class AdapterSubscribers(
    final override val data: MutableList<AbstractItemUI>
) :
    AdapterWithLoad(data) {
    abstract fun onExitClickListener()
    abstract fun onAddUserClick(user: UserMini)

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): AdapterWithLoad.BaseViewHolder {
        return when (viewType) {
            1 -> {
                val view = LayoutInflater.from(parent.context)
                    .inflate(R.layout.item_recycler_subscribers, parent, false)
                BaseViewHolder(view)
            }
            else -> super.onCreateViewHolder(parent, viewType)
        }
    }

    override fun getItemViewType(position: Int): Int {
        return when (data[position]) {
            is ItemUserUI -> 1
            else -> super.getItemViewType(position)
        }
    }

    inner class BaseViewHolder(itemView: View) :
        AdapterWithLoad.BaseViewHolder(itemView) {
        @SuppressLint("SetTextI18n")
        override fun bind(position: Int) {
            val dataItem = data[position] as ItemUserUI
            itemView.apply {
                val requestOptions = RequestOptions().circleCrop().diskCacheStrategy(
                    DiskCacheStrategy.ALL
                )
                Glide
                    .with(itemView)
                    .asBitmap()
                    .placeholder(R.drawable.tmp_profile)
                    .transition(withCrossFade())
                    .load(SignData.server_file + dataItem.user.avatarFileId)
                    .apply(requestOptions)
                    .into(profile_image_image_view)
                var displayName = dataItem.user.firstName
                if (dataItem.user.firstName.isNullOrEmpty()) {
                    displayName = "@" + dataItem.user.nick
                }
                name_user_text_view.text = displayName
                nick_text_view.text = dataItem.user.nick
                add_user_btn.visibility = (!dataItem.user.isSubscribe).convertToVisible()
                add_user_btn.setOnClickListener {
                    dataItem.user.isSubscribe = true
                    add_user_btn.animate().scaleY(0f).scaleY(0f).alpha(0f).withEndAction {
                        add_user_btn.visibility = false.convertToVisible()
                        add_user_btn.scaleX = 1f
                        add_user_btn.scaleY = 1f
                        add_user_btn.alpha = 1f
                    }.start()
                    onAddUserClick(dataItem.user)
                }
                setOnClickListener {
                    onUserClickListener(dataItem.user)
                }
            }
        }
    }


}

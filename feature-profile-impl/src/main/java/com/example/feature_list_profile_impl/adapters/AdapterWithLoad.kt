package com.example.feature_list_profile_impl.adapters

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.feature_list_profile_api.models.*
import com.example.feature_list_profile_impl.R
import com.example.feature_list_profile_impl.adapters.AdapterWithLoad.BaseViewHolder
import com.example.feature_list_profile_impl.interfaces.IListUsers
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch

abstract class AdapterWithLoad(
    open val data: MutableList<AbstractItemUI>
) :
    RecyclerView.Adapter<BaseViewHolder>(), IListUsers {
    var isLoading = false
    var recyclerView: RecyclerView? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BaseViewHolder {

        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.item_recycler_load_data, parent, false)
        return LoadDataViewHolder(view)
    }

    override fun getItemId(position: Int): Long {
        val dataItem = data[position]

        return when (dataItem) {
            is ItemInformationUI -> -1
            is ItemNotificationUI -> -2
            is ItemLinkFriendUI -> -3
            is ItemAddFriendUI -> -4
            is ItemGroupName -> -5
            is ItemUserUI -> dataItem.user.uid
            is ItemExitChannelUI -> -6
            else -> dataItem.hashCode().toLong()
        }
    }

    override fun onAttachedToRecyclerView(recyclerView: RecyclerView) {
        super.onAttachedToRecyclerView(recyclerView)
        this.recyclerView = recyclerView
        recyclerView.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                super.onScrolled(recyclerView, dx, dy)
                if (!recyclerView.canScrollVertically(1)) { //1 for down
                    loadMoreData()
                }
            }
        })
    }


    @SuppressLint("SetTextI18n")
    override fun onBindViewHolder(holder: BaseViewHolder, position: Int) {
        holder.bind(position)
    }


    override fun getItemCount(): Int {
        return data.size
    }

    override fun getItemViewType(position: Int): Int {
        return -2
    }

    abstract class BaseViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        abstract fun bind(position: Int)
    }

    inner class LoadDataViewHolder(itemView: View) : BaseViewHolder(itemView) {
        override fun bind(position: Int) {
        }
    }

    override fun loadMoreData() {
        isLoading = true
        if (data.find { it is ItemLoadData } == null) {
            data.add(ItemLoadData())
            recyclerView?.post {
                notifyDataSetChanged()
            }
        }
    }

    fun addNewDataFromServer(
        listMessages: MutableList<AbstractItemUI>?
    ) {
        if (recyclerView == null) return
        if (data.find { it is ItemLoadData } != null) {
            val itemToRemove = data.lastIndex
            if (itemToRemove >= 0) {
                data.removeAt(itemToRemove)
           }
            isLoading = false
        }
        if (listMessages.isNullOrEmpty()) {
            GlobalScope.launch(Dispatchers.Main) { notifyDataSetChanged() }
            return
        }
        data.addAll(listMessages)
        GlobalScope.launch(Dispatchers.Main) { notifyDataSetChanged() }
    }


}

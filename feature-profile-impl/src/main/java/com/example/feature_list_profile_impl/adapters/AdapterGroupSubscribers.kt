package com.example.feature_list_profile_impl.adapters

import android.annotation.SuppressLint
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.load.resource.bitmap.BitmapTransitionOptions.withCrossFade
import com.bumptech.glide.request.RequestOptions
import com.example.feature_list_profile_api.models.*
import com.example.feature_list_profile_impl.R
import com.example.feature_list_profile_impl.utility.Dialog.showDialog
import com.morozov.core_backend_api.AnyInnerRequest
import com.morozov.core_backend_api.AnyRequest
import com.morozov.core_backend_api.SignData
import com.morozov.core_backend_api.chat.Chat
import com.morozov.core_backend_api.chat.requests.ChatBlockUserRequest
import com.morozov.core_backend_api.chat.requests.ChatDeleteAdminRequest
import com.morozov.core_backend_api.chat.requests.ChatGetMembersRequest.Companion.FILTER_ADMIN
import com.morozov.core_backend_api.chat.requests.ChatGetMembersRequest.Companion.FILTER_ALL
import com.morozov.core_backend_api.chat.requests.ChatGetMembersRequest.Companion.FILTER_BLACK_LIST
import com.morozov.core_backend_api.errors.Error
import com.morozov.core_backend_api.user.UserMini
import com.morozov.lib_backend.LibBackendDependency
import com.social.solutions.util_text_formators.convertToVisible
import kotlinx.android.synthetic.main.item_recycler_contacts_in_this_group.view.*
import kotlinx.android.synthetic.main.item_recycler_subscribers.view.add_user_btn
import kotlinx.android.synthetic.main.item_recycler_subscribers.view.name_user_text_view
import kotlinx.android.synthetic.main.item_recycler_subscribers.view.nick_text_view
import kotlinx.android.synthetic.main.item_recycler_subscribers.view.profile_image_image_view
import kotlinx.android.synthetic.main.item_recycler_subscribers_group.view.*
import kotlinx.android.synthetic.main.item_recycler_this_list_see_only_admin.view.*


abstract class AdapterGroupSubscribers(
    final override val data: MutableList<AbstractItemUI>,
    val chat: Chat,
    val filter: Int,
    var listAdmins: MutableList<UserMini> = mutableListOf()
) :
    AdapterWithLoad(data) {
    abstract fun onAddUserClick(chat: Chat)
    abstract fun onAddAdminClick(chat: Chat)
    abstract fun onChangeAccessLevel(chat: Chat, userMini: UserMini)

    init {
        when (filter) {
            FILTER_ALL -> {
                data.add(ItemAddFriendUI())
                data.add(ItemContactsInThisGroupUI())
                data.add(ItemThisListViewOnlyAdminUI())
            }
            FILTER_ADMIN -> {
                data.add(ItemAddAdminUI())
                data.add(ItemThisListViewOnlyAdminUI("Назначайте помощников для управления каналом"))
            }
            FILTER_BLACK_LIST -> {
                data.add(ItemThisListViewOnlyAdminUI("Пользователь, добавленный в чёрный список, больше не сможет присоединиться к каналу по ссылке"))
            }
        }
    }

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): AdapterWithLoad.BaseViewHolder {
        return when (viewType) {
            1 -> {
                val view = LayoutInflater.from(parent.context)
                    .inflate(R.layout.item_recycler_subscribers_group, parent, false)
                BaseViewHolder(view)
            }
            2 -> {
                val view = LayoutInflater.from(parent.context)
                    .inflate(R.layout.item_recycler_add_friend, parent, false)
                AddFriendViewHolder(view)
            }
            3 -> {
                val view = LayoutInflater.from(parent.context)
                    .inflate(R.layout.item_recycler_contacts_in_this_group, parent, false)
                TextViewHolder(view)
            }
            4 -> {
                val view = LayoutInflater.from(parent.context)
                    .inflate(R.layout.item_recycler_this_list_see_only_admin, parent, false)
                TextViewHolder(view)
            }
            5 -> {
                val view = LayoutInflater.from(parent.context)
                    .inflate(R.layout.item_recycler_add_admin, parent, false)
                AddAdminViewHolder(view)
            }
            else -> super.onCreateViewHolder(parent, viewType)
        }
    }

    override fun getItemViewType(position: Int): Int {
        return when (data[position]) {
            is ItemUserUI -> 1
            is ItemAddFriendUI -> 2
            is ItemContactsInThisGroupUI -> 3
            is ItemThisListViewOnlyAdminUI -> 4
            is ItemAddAdminUI -> 5
            else -> super.getItemViewType(position)
        }
    }

    inner class BaseViewHolder(itemView: View) :
        AdapterWithLoad.BaseViewHolder(itemView) {
        @SuppressLint("SetTextI18n")
        override fun bind(position: Int) {
            val dataItem = data[position] as ItemUserUI
            itemView.apply {
                val requestOptions = RequestOptions().circleCrop().diskCacheStrategy(
                    DiskCacheStrategy.ALL
                )
                if (dataItem.user.avatarFileId != null) {
                    Glide
                        .with(itemView)
                        .asBitmap()
                        .placeholder(R.drawable.tmp_profile)
                        .transition(withCrossFade())
                        .load(SignData.server_file + dataItem.user.avatarFileId)
                        .apply(requestOptions)
                        .into(profile_image_image_view)
                } else {
                    Glide
                        .with(itemView)
                        .asBitmap()
                        .transition(withCrossFade())
                        .load(R.drawable.tmp_profile)
                        .apply(requestOptions)
                        .into(profile_image_image_view)
                }
                var displayName = dataItem.user.firstName
                if (dataItem.user.firstName.isNullOrEmpty()) {
                    displayName = "@" + dataItem.user.nick
                }
                name_user_text_view.text = displayName
                nick_text_view.text = dataItem.user.nick
                val itIsCreator = dataItem.user.uid == chat.creatorUid
                val itIsAdmin = (itIsCreator || (filter == FILTER_ALL && listAdmins.find { it.uid == dataItem.user.uid } != null))
                add_user_btn.visibility = itIsAdmin.not().convertToVisible()
                is_owner_text_view?.visibility = itIsAdmin.convertToVisible()
                is_owner_text_view?.text = if (itIsCreator) {
                    "владелец"
                } else {
                    "админ"
                }
                add_user_btn.setOnClickListener {
                    val firstBtn = when (filter) {
                        FILTER_ALL -> this.context.getString(R.string.set_admin)
                        FILTER_ADMIN -> context.getString(R.string.change_admin_rule)
                        else -> context.getString(R.string.remove_from_black_list)
                    }
                    val secondBtn = when (filter) {
                        FILTER_ALL -> this.context.getString(R.string.lock)
                        FILTER_ADMIN -> context.getString(R.string.remove_from_admin_list)
                        else -> null
                    }
                    val layout = when (filter) {
                        FILTER_ALL, FILTER_ADMIN -> R.layout.dialog_action_with_subscriber
                        else -> R.layout.dialog_action_with_subscriber_bl
                    }
                    showDialog(
                        layout,
                        this.context,
                        firstBtn,
                        secondBtn
                    ) { isFirstChoose ->
                        if (isFirstChoose) {
                            when (filter) {
                                FILTER_ALL, FILTER_ADMIN -> onChangeAccessLevel(chat, dataItem.user)
                                else -> {
                                    LibBackendDependency.featureBackendApi().chatApi().blockUser(
                                        AnyRequest(
                                            AnyInnerRequest(
                                                ChatBlockUserRequest(
                                                    chat.id,
                                                    dataItem.user.uid,
                                                    false
                                                )
                                            )
                                        )
                                    ) {
                                        if (it.result) {
                                            val newPos = data.indexOf(dataItem)
                                            if (data.remove(dataItem)) {
                                                notifyItemRemoved(newPos)
                                            } else {
                                                onError(context, it.error)
                                            }
                                        } else {
                                            onError(context, it.error)
                                        }
                                    }
                                }
                            }
                        } else {
                            when (filter) {
                                FILTER_ALL -> {
                                    LibBackendDependency.featureBackendApi().chatApi().blockUser(
                                        AnyRequest(
                                            AnyInnerRequest(
                                                ChatBlockUserRequest(
                                                    chat.id,
                                                    dataItem.user.uid,
                                                    true
                                                )
                                            )
                                        )
                                    ) {
                                        if (it.result) {
                                            val newPos = data.indexOf(dataItem)
                                            if (data.remove(dataItem)) {
                                                notifyItemRemoved(newPos)
                                            } else {
                                                onError(context, it.error)
                                            }
                                        } else {
                                            onError(context, it.error)
                                        }
                                    }
                                }
                                FILTER_ADMIN -> {
                                    LibBackendDependency.featureBackendApi().chatApi().deleteAdmin(
                                        AnyRequest(
                                            AnyInnerRequest(
                                                ChatDeleteAdminRequest(
                                                    chat.id,
                                                    dataItem.user.uid
                                                )
                                            )
                                        )
                                    ) {
                                        if (it.result) {
                                            val newPos = data.indexOf(dataItem)
                                            if (data.remove(dataItem)) {
                                                notifyItemRemoved(newPos)
                                            } else {
                                                onError(context, it.error)
                                            }
                                        } else {
                                            onError(context, it.error)
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                setOnClickListener {
                    onUserClickListener(dataItem.user)
                }
            }
        }
    }

    fun onError(context: Context, error: Error?) {
        Toast
            .makeText(context, "Что-то пошло не так: " + error?.msg, Toast.LENGTH_SHORT)
            .show()
    }

    inner class AddFriendViewHolder(itemView: View) :
        AdapterWithLoad.BaseViewHolder(itemView) {
        override fun bind(position: Int) {
            itemView.setOnClickListener {
                onAddUserClick(chat)
            }
        }
    }

    inner class AddAdminViewHolder(itemView: View) :
        AdapterWithLoad.BaseViewHolder(itemView) {
        override fun bind(position: Int) {
            itemView.setOnClickListener {
                onAddAdminClick(chat)
            }
        }
    }

    inner class TextViewHolder(itemView: View) :
        AdapterWithLoad.BaseViewHolder(itemView) {
        override fun bind(position: Int) {
            val dataItem = data[position]
            when (dataItem) {
                is ItemContactsInThisGroupUI -> {
                    if (dataItem.text != null) {
                        itemView.name_group_message.text = dataItem.text
                    }
                    if (dataItem.colorIsAccent) {
                        itemView.name_group_message.setTextColor(itemView.resources.getColor(R.color.colorAccent))
                    }
                }
                is ItemThisListViewOnlyAdminUI -> {
                    if (dataItem.text != null) {
                        itemView.little_text_message.text = dataItem.text
                    }
                }
            }
        }
    }
}

package com.example.feature_list_profile_impl.adapters

import android.annotation.SuppressLint
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.fragment.app.Fragment
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.load.resource.bitmap.BitmapTransitionOptions.withCrossFade
import com.bumptech.glide.request.RequestOptions
import com.example.feature_list_profile_api.models.AbstractItemUI
import com.example.feature_list_profile_api.models.ItemInPhotoGalleryForNewUI
import com.example.feature_list_profile_api.models.ItemInPhotoGalleryUI
import com.example.feature_list_profile_impl.FileRepository
import com.example.feature_list_profile_impl.R
import com.morozov.core_backend_api.SignData
import com.morozov.core_backend_api.file.FileModel
import com.morozov.core_backend_api.user.User
import kotlinx.android.synthetic.main.item_recycler_photo_gallery.view.*


abstract class AdapterPhotoGallery(
    final override val data: MutableList<AbstractItemUI>,
    val user: User,
    val fragment: Fragment
) :
    AdapterWithLoad(data) {

    init {
        if (user.uid == SignData.uid) {
            data.add(0, ItemInPhotoGalleryForNewUI())
        }
    }

    abstract fun onPhotoClickListener(
        url: String,
        imageView: ImageView?,
        withVideo: Boolean,
        function: (url: String?) -> Unit
    )

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): AdapterWithLoad.BaseViewHolder {
        return when (viewType) {
            0 -> {
                val view = LayoutInflater.from(parent.context)
                    .inflate(R.layout.item_recycler_photo_gallery, parent, false)
                BaseViewHolder(view)
            }
            1 -> {
                val view = LayoutInflater.from(parent.context)
                    .inflate(R.layout.item_recycler_for_new_photo_gallery, parent, false)
                NewPhotoGalleryViewHolder(view)
            }
            else -> super.onCreateViewHolder(parent, viewType)
        }
    }

    override fun getItemViewType(position: Int): Int {
        return when (data[position]) {
            is ItemInPhotoGalleryUI -> 0
            is ItemInPhotoGalleryForNewUI -> 1
            else -> super.getItemViewType(position)
        }
    }

    inner class BaseViewHolder(itemView: View) :
        AdapterWithLoad.BaseViewHolder(itemView) {
        override fun bind(position: Int) {
            val dataItem = data[position] as ItemInPhotoGalleryUI
            itemView.apply {
                val requestOptions = RequestOptions().diskCacheStrategy(DiskCacheStrategy.ALL)
                val path = if (dataItem.file.type != -1) SignData.server_file else ""
                Glide
                    .with(itemView)
                    .asBitmap()
                    .transition(withCrossFade())
                    .load(path + dataItem.file.fileId)
                    .thumbnail(0.25f)
                    .apply(requestOptions)
                    .into(image)
                image?.setOnClickListener {
                    if (dataItem.file.fileId != null) {
                        onPhotoClickListener(
                            dataItem.file.fileId!!,
                            image,
                            (dataItem.file.type in 3..5)
                        ) {}
                    }
                }
            }
        }
    }

    inner class NewPhotoGalleryViewHolder(itemView: View) :
        AdapterWithLoad.BaseViewHolder(itemView) {
        override fun bind(position: Int) {
            itemView.apply {
                image?.setOnClickListener {
                    onPhotoClickListener("", image, true) {
                        if (it != null)
                            addNewPhotoInGallery(it)
                    }
                }
            }
        }
    }

    @SuppressLint("CheckResult")
    fun addNewPhotoInGallery(url: String) {
        FileRepository.sendFile(
            this.fragment,
            mutableListOf(url),
            user.galleryChatId ?: -1L
        ) { id ->
            callBackAddNewPhotoInGallery(url)
        }
    }

    fun callBackAddNewPhotoInGallery(url: String) {
        data.add(1, ItemInPhotoGalleryUI(FileModel(url, -1)))
        recyclerView?.adapter?.notifyItemInserted(1)
    }
}

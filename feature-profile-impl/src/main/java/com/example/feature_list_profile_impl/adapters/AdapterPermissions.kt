package com.example.feature_list_profile_impl.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.feature_list_profile_impl.R
import com.morozov.core_backend_api.chat.Chat
import com.morozov.core_backend_api.chat.Chat.Companion.AccessLevel.*
import kotlinx.android.synthetic.main.item_recycler_permission.view.*


class AdapterPermissions(
    var data: MutableList<Pair<Chat.Companion.AccessLevel, Boolean>>,
    val changerUserRole: MutableList<Chat.Companion.AccessLevel>
) :
    RecyclerView.Adapter<AdapterPermissions.BaseViewHolder>() {

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): BaseViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.item_recycler_permission, parent, false)
        return BaseViewHolder(view)
    }

    override fun getItemCount(): Int {
        return data.size
    }

    override fun getItemId(position: Int): Long {
        return data[position].first.level.toLong()
    }

    override fun onBindViewHolder(holder: BaseViewHolder, position: Int) {
        holder.bind(position)
    }

    inner class BaseViewHolder(itemView: View) :
        RecyclerView.ViewHolder(itemView) {

        fun bind(position: Int) {
            val dataItem = data[position]
            itemView.apply {
                switch_permission.text = when (dataItem.first) {
                    READ_MESSAGE -> context.resources.getString(R.string.read_message_1)
                    SEND_MESSAGE -> context.resources.getString(R.string.send_message_2)
                    SEND_MEDIA -> context.resources.getString(R.string.send_media_3)
                    FORWARD_MESSAGE -> context.resources.getString(R.string.forward_message_4)
                    PREVIEW_LINK -> context.resources.getString(R.string.preview_link_5)
                    ADD_USERS -> context.resources.getString(R.string.add_users_6)
                    PINNED_MESSAGE -> context.resources.getString(R.string.pinned_message_7)
                    EDIT_PROFILE -> context.resources.getString(R.string.edit_profile_8)
                    EDIT_OTHER_MESSAGE -> context.resources.getString(R.string.edit_other_message_9)
                    DELETE_OTHER_MESSAGE -> context.resources.getString(R.string.delete_other_message_10)
                    SET_ADMIN -> context.resources.getString(R.string.set_admin_11)
                    EDIT_PRIVACY -> context.resources.getString(R.string.edit_privacy_12)
                    BLOCK_UNBLOCK_USER -> context.resources.getString(R.string.block_unblock_user_13)
                    VIEW_SUBSCRIBERS_USERS -> context.resources.getString(R.string.view_subscribers_users_14)
                    DELETE_CHAT -> context.resources.getString(R.string.delete_chat_15)
                    ADMIN_CHAT -> context.resources.getString(R.string.admin_chat_16)
                    EMPTY -> ""
                }
                switch_permission.isEnabled = changerUserRole.contains(dataItem.first)
                switch_permission.setOnCheckedChangeListener { _, _ -> }
                switch_permission.isChecked = dataItem.second
                switch_permission.setOnCheckedChangeListener { _, isChecked ->
                    data[position] = Pair(dataItem.first, isChecked)
                }
            }
        }
    }

    fun getCurrentAccessLevel(): MutableList<Chat.Companion.AccessLevel> {
        val result = mutableListOf<Chat.Companion.AccessLevel>()
        data.forEach {
            if (it.second) {
                result.add(it.first)
            }
        }
        return result
    }
}

package com.example.feature_list_profile_impl.adapters

import android.annotation.SuppressLint
import android.content.ClipData
import android.content.ClipboardManager
import android.content.Intent
import android.text.Spannable
import android.text.SpannableString
import android.text.style.ForegroundColorSpan
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.core.content.ContextCompat.getSystemService
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.load.resource.bitmap.BitmapTransitionOptions.withCrossFade
import com.bumptech.glide.request.RequestOptions
import com.example.feature_list_profile_api.models.*
import com.example.feature_list_profile_impl.R
import com.example.feature_list_profile_impl.utility.TextCollapseAnimator.cycleTextViewExpansion
import com.google.android.material.snackbar.Snackbar
import com.morozov.core_backend_api.AnyInnerRequest
import com.morozov.core_backend_api.AnyRequest
import com.morozov.core_backend_api.SignData
import com.morozov.core_backend_api.chat.Chat
import com.morozov.core_backend_api.chat.requests.ChatMuteRequest
import com.morozov.core_backend_api.user.UserMini
import com.morozov.lib_backend.LibBackendDependency
import com.social.solutions.util_text_formators.*
import kotlinx.android.synthetic.main.fragment_information_user.view.title_description
import kotlinx.android.synthetic.main.fragment_information_user.view.user_info_expand
import kotlinx.android.synthetic.main.item_recycler_information.view.*
import kotlinx.android.synthetic.main.item_recycler_notification.view.*
import kotlinx.android.synthetic.main.item_recycler_search.view.*
import kotlinx.android.synthetic.main.item_recycler_search_group_stick.view.*


abstract class AdapterProfile(
    val chat: Chat,
    final override val data: MutableList<AbstractItemUI>
) :
    AdapterWithLoad(data) {
    abstract fun onExitClickListener(chat: Chat)
    abstract fun onAddUserClick(chat: Chat)

    init {
        val userRole = chat.getAccessLevels()
        when (chat.type) {
            Chat.GROUP_CHAT -> {
                if (chat.description.isNullOrEmpty().not())
                    data.add(ItemInformationUI("Информация", chat.description.toString()))
                data.add(ItemNotificationUI(state = chat.muteTime.isMuteNow()))
                if (chat.inviteHash != null) data.add(ItemLinkFriendUI(chat.inviteHash!!))
                if (userRole.contains(Chat.Companion.AccessLevel.ADD_USERS)) data.add(
                    ItemAddFriendUI()
                )
                if (chat.creatorUid != SignData.uid) data.add(ItemExitChannelUI("Покинуть группу"))
                val countUsers = chat.membersAmount ?: 0
                val caseWord = if (countUsers < 1000 && countUsers % 100 !in 10..20) {
                    when (countUsers % 10) {
                        1 -> "участник"
                        in (2..4) -> "участника"
                        else -> "участников"
                    }
                } else {
                    "участников"
                }
                val countText = countUsers.convertToShortString() + " $caseWord"
                data.add(ItemGroupName(-1, countText))
            }
            Chat.CHANNEL -> {
                if (chat.description.isNullOrEmpty().not())
                    data.add(ItemInformationUI("Описание", chat.description.toString()))
                data.add(ItemNotificationUI(state = chat.muteTime.isMuteNow()))
                if (chat.inviteHash != null) data.add(ItemLinkFriendUI(chat.inviteHash!!))
                if (chat.creatorUid != SignData.uid) data.add(ItemExitChannelUI("Покинуть канал"))
            }
            else -> {
            }
        }
    }

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): AdapterWithLoad.BaseViewHolder {
        return when (viewType) {
            0 -> {
                val view = LayoutInflater.from(parent.context)
                    .inflate(R.layout.item_recycler_information, parent, false)
                InformationViewHolder(view)
            }
            1 -> {
                val view = LayoutInflater.from(parent.context)
                    .inflate(R.layout.item_recycler_notification, parent, false)
                NotificationViewHolder(view)
            }
            2 -> {
                val view = LayoutInflater.from(parent.context)
                    .inflate(R.layout.item_recycler_friend_link, parent, false)
                LinkFriendViewHolder(view)
            }
            3 -> {
                val view = LayoutInflater.from(parent.context)
                    .inflate(R.layout.item_recycler_add_friend, parent, false)
                AddFriendViewHolder(view)
            }
            4 -> {
                val view = LayoutInflater.from(parent.context)
                    .inflate(R.layout.item_recycler_search_group_stick, parent, false)
                GroupMessageViewHolder(view)
            }
            5 -> {
                val view = LayoutInflater.from(parent.context)
                    .inflate(R.layout.item_recycler_search, parent, false)
                val vh = BaseViewHolder(view)
                view.setOnClickListener {
                    val item = data[vh.adapterPosition]
                    if (item is ItemUserUI) {
                        onUserClickListener(item.user)
                    }
                }
                vh
            }
            6 -> {
                val view = LayoutInflater.from(parent.context)
                    .inflate(R.layout.item_recycler_exit_channel, parent, false)
                ExitChannelViewHolder(view)
            }
            7 -> {
                val view = LayoutInflater.from(parent.context)
                    .inflate(R.layout.item_recycler_test, parent, false)
                TestViewHolder(view)
            }
            else -> super.onCreateViewHolder(parent, viewType)
        }
    }


    override fun getItemViewType(position: Int): Int {
        return when (data[position]) {
            is ItemInformationUI -> 0
            is ItemNotificationUI -> 1
            is ItemLinkFriendUI -> 2
            is ItemAddFriendUI -> 3
            is ItemGroupName -> 4
            is ItemUserUI -> 5
            is ItemExitChannelUI -> 6
            else -> super.getItemViewType(position)
        }
    }

    val requestOptions = RequestOptions().circleCrop().diskCacheStrategy(
        DiskCacheStrategy.ALL
    )

    inner class BaseViewHolder(itemView: View) :
        AdapterWithLoad.BaseViewHolder(itemView) {
        @SuppressLint("SetTextI18n")
        override fun bind(position: Int) {
            val dataItem = data[position] as ItemUserUI
            itemView.apply {
                Glide
                    .with(itemView)
                    .asBitmap()
                    .placeholder(R.drawable.tmp_profile)
                    .transition(withCrossFade())
                    .load(SignData.server_file + dataItem.user.avatarFileId)
                    .apply(requestOptions)
                    .into(profile_image_image_view)
                var displayName = dataItem.user.firstName
                if (dataItem.user.firstName.isNullOrEmpty()) {
                    displayName = "@" + dataItem.user.nick
                }
                name_user_text_view?.text = displayName
                owner_text_view?.visibility =
                    (dataItem.user.uid == chat.creatorUid).convertToVisible()
                updateTextMessage(message_text_view, position)
            }
        }

        @SuppressLint("SetTextI18n")
        private fun updateTextMessage(textView: TextView?, position: Int) {
            val dataItem = data[position] as ItemUserUI
            if (dataItem.user.lastActive.isOnline()) {
                textView?.text = ""
                addSelectedText(itemView.resources.getString(R.string.is_online))
            } else {
                textView?.text =
                    "${itemView.resources.getString(R.string.it_was_online)} " +
                            dataItem.user.lastActive.convertLongToCurrentType(itemView.context)
            }
        }

        private fun addSelectedText(
            standardText: String,
            color: Int = R.color.colorAccent
        ) {
            val wordTwo: Spannable =
                SpannableString(standardText)
            wordTwo.setSpan(
                ForegroundColorSpan(itemView.context.resources.getColor(color)),
                0,
                wordTwo.length,
                Spannable.SPAN_EXCLUSIVE_EXCLUSIVE
            )
            itemView.message_text_view.append(wordTwo, 0, wordTwo.length)
        }
    }

    inner class GroupMessageViewHolder(itemView: View) :
        AdapterWithLoad.BaseViewHolder(itemView) {
        override fun bind(position: Int) {
            val dataItem = data[position] as ItemGroupName
            itemView.apply {
                name_group_message.text = when (dataItem.group) {
                    Chat.PERSONAL_CHAT -> resources.getString(R.string.people)
                    Chat.CHANNEL -> resources.getString(R.string.channels)
                    Chat.GROUP_CHAT -> resources.getString(R.string.groups)
                    else -> dataItem.text
                }
            }
        }
    }

    inner class InformationViewHolder(itemView: View) :
        AdapterWithLoad.BaseViewHolder(itemView) {
        override fun bind(position: Int) {
            val item = data[position] as ItemInformationUI
            itemView.title_description.text = item.name
            itemView.description.text = item.details
            itemView.description.post {
                itemView.user_info_expand.visibility =
                    (itemView.description.lineCount >
                            itemView.description.maxLines)
                        .convertToVisible()
            }
            itemView.user_info_expand.setOnClickListener {
                cycleTextViewExpansion(
                    itemView.description,
                    itemView.user_info_expand
                )
            }
        }
    }

    inner class NotificationViewHolder(itemView: View) :
        AdapterWithLoad.BaseViewHolder(itemView) {
        override fun bind(position: Int) {
            val item = data[position] as ItemNotificationUI
            itemView.switch_notification.isChecked = !item.state
            itemView.switch_notification.setOnCheckedChangeListener { compoundButton, isChecked ->
                item.state = !isChecked
                var timeForMute = 0L
                if (!isChecked) timeForMute = Long.MAX_VALUE
                LibBackendDependency.featureBackendApi().chatApi().mute(
                    AnyRequest(
                        AnyInnerRequest(
                            ChatMuteRequest(
                                mutableListOf(chat.id), timeForMute
                            )
                        )
                    )
                )
            }
        }
    }

    inner class ExitChannelViewHolder(itemView: View) :
        AdapterWithLoad.BaseViewHolder(itemView) {
        override fun bind(position: Int) {
            val item = data[position] as ItemExitChannelUI
            if (item.text != null) {
                itemView.name_user_text_view.text = item.text
            }
            itemView.setOnClickListener { onExitClickListener(chat) }
        }
    }

    inner class TestViewHolder(itemView: View) :
        AdapterWithLoad.BaseViewHolder(itemView) {
        override fun bind(position: Int) {
//            val item = data[position] as ItemExitChannelUI
//            if (item.text != null) {
//                itemView.name_user_text_view.text = item.text
//            }
//            itemView.setOnClickListener { onExitClickListener(chat) }
        }
    }


    inner class LinkFriendViewHolder(itemView: View) :
        AdapterWithLoad.BaseViewHolder(itemView) {
        override fun bind(position: Int) {
            val item = data[position] as ItemLinkFriendUI
            itemView.setOnClickListener {
                val sendIntent: Intent = Intent().apply {
                    action = Intent.ACTION_SEND
                    putExtra(Intent.EXTRA_TEXT, item.link)
                    type = "text/plain"
                }

                val shareIntent = Intent.createChooser(sendIntent, null)
                it.context.startActivity(shareIntent)
            }
            itemView.setOnLongClickListener {
                val clipboard: ClipboardManager? =
                    getSystemService<ClipboardManager>(it.context, ClipboardManager::class.java)
                val clip = ClipData.newPlainText("share link", item.link)
                clipboard?.setPrimaryClip(clip)
                Snackbar.make(it, R.string.link_copyd, Snackbar.LENGTH_SHORT).show()
                true
            }
        }
    }

    inner class AddFriendViewHolder(itemView: View) :
        AdapterWithLoad.BaseViewHolder(itemView) {
        override fun bind(position: Int) {
            itemView.setOnClickListener {
                onAddUserClick(chat)
            }
        }
    }


}

package com.example.feature_list_profile_impl


import android.util.Log
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.lifecycle.LiveData
import com.example.feature_list_profile_api.FeatureProfileCallback
import com.example.feature_list_profile_api.ProfileFeatureApi
import com.example.feature_list_profile_api.ProfileStarter
import com.example.feature_list_profile_impl.MainObject.mBackendApi
import com.example.feature_list_profile_impl.MainObject.mFeatureFeedStarter
import com.example.feature_list_profile_impl.MainObject.mLocalDB
import com.example.feature_list_profile_impl.MainObject.mNetworkState
import com.example.feature_list_profile_impl.editFragments.FragmentEditGroupOrChannel
import com.example.feature_list_profile_impl.editFragments.FragmentEditPermissions
import com.example.feature_list_profile_impl.editFragments.FragmentEditProfile
import com.example.feature_list_profile_impl.editFragments.FragmentListSubscribersGroup
import com.example.util_cache.AppDatabase
import com.morozov.core_backend_api.FeatureBackendApi
import com.morozov.core_backend_api.SignData
import com.morozov.core_backend_api.chat.Chat
import com.morozov.core_backend_api.user.User
import com.morozov.core_backend_api.user.UserMini
import com.social.solution.feature_feed_api.FeatureFeedStarter
import com.social.solution.feature_feed_api.models.FeedConfig
import com.social.solution.feature_feed_impl.MainObject.TAG

class ProfileStarterImpl : ProfileStarter {
    private val profileMe by lazy {
        Log.d("FEED_MODULE", "CREATE_PROFILE_ME")
        ProfileFragmentMe(SignData.uid, this) }
    var config: FeedConfig? = null
    var feedPageFragment: Fragment? = null
    var mediaPageFragment: ListMediaPageFragment? = null
    override fun startProfile(
        manager: FragmentManager,
        container: Int,
        addToBackStack: Boolean,
        callback: FeatureProfileCallback?,
        chat: Chat,
        featureFeedStarter: FeatureFeedStarter,
        backendApi: FeatureBackendApi,
        networkState: LiveData<Boolean>,
        localDB: AppDatabase
    ) {
        mNetworkState = networkState
        mFeatureFeedStarter = featureFeedStarter
        mBackendApi = backendApi
        mLocalDB = localDB
        val fragment: ProfileFeatureApi =
            when (chat.type) {
                Chat.CHANNEL, Chat.PRIVATE_CHANNELS -> ProfileFragmentChannel(chat.id)
                Chat.PERSONAL_DIALOG, Chat.SECRET_CHAT -> {
                    if (chat.dialogUid == null || chat.dialogUid == SignData.uid) {
                        profileMe
                    } else {
                        ProfileFragmentOtherUser(chat.dialogUid!!, this)
                    }
                }
                Chat.GROUP_CHAT -> ProfileFragmentGroup(chat.id)
                else -> {
                    return
                }
            }
        fragment.callback = callback
        startDefault(fragment as Fragment, manager, container, addToBackStack)

    }

    override fun startSubscribersFragment(
        manager: FragmentManager,
        container: Int,
        addToBackStack: Boolean,
        callback: FeatureProfileCallback?,
        user: User
    ) {
        val fragment = ProfileFragmentSubscrubers(user)
        fragment.callback = callback
        startDefault(fragment as Fragment, manager, container, addToBackStack)
    }

    override fun startEditProfileFragment(
        fragmentManager: FragmentManager,
        mainContainer: Int,
        callback: FeatureProfileCallback?,
        user: User,
        field: String
    ) {
        val fragment = FragmentEditProfile(user, field)
        fragment.callback = callback
        startDefault(fragment, fragmentManager, mainContainer, true)
    }

    override fun startEditProfileFragment(
        fragmentManager: FragmentManager,
        mainContainer: Int,
        callback: FeatureProfileCallback?,
        chat: Chat
    ) {
        val fragment = FragmentEditGroupOrChannel(chat)
        fragment.callback = callback
        startDefault(fragment, fragmentManager, mainContainer, true)
    }

    override fun startEditSubscribersGroupFragment(
        fragmentManager: FragmentManager,
        mainContainer: Int,
        callback: FeatureProfileCallback?,
        chat: Chat,
        membersFilter: Int
    ) {
        val fragment = FragmentListSubscribersGroup(chat, membersFilter)
        fragment.callback = callback
        startDefault(fragment, fragmentManager, mainContainer, true)
    }

    override fun startEditPermissionsFragment(
        fragmentManager: FragmentManager,
        mainContainer: Int,
        chat: Chat,
        user: UserMini?
    ) {
        val fragment = FragmentEditPermissions(chat, user)
        startDefault(fragment, fragmentManager, mainContainer, true)
    }

    private fun startDefault(
        fragment: Fragment,
        manager: FragmentManager,
        container: Int,
        addToBackStack: Boolean
    ) {
        val transaction = manager.beginTransaction()
        transaction.setCustomAnimations(
            android.R.anim.fade_in,
            android.R.anim.fade_out,
            android.R.anim.fade_in,
            android.R.anim.fade_out
        )
        transaction.replace(container, fragment)
        if (addToBackStack)
            transaction.addToBackStack(fragment.tag)
        transaction.commitAllowingStateLoss()
    }
}
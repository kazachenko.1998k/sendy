
package com.social.solution.feature_post_api

import androidx.lifecycle.MutableLiveData

interface FeaturePostApi {
    fun postStarter(): FeaturePostStarter
}
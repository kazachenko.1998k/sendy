package com.social.solution.feature_post_api.models

data class PostOwnerUI(
    val id: Long,
    val firstName: String,
    val lastName: String,
    val login: String,
    val avatar: String,
    var isSubscribe: Boolean
)
package com.social.solution.feature_post_api.models

data class PostSizeUI (var width: Int, var height: Int)
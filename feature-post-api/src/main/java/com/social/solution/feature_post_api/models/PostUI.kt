package com.social.solution.feature_post_api.models

import com.morozov.core_backend_api.file.FileModel
import com.morozov.core_backend_api.user.UserMini

data class PostUI(
    val id: Long?,
    val ownerUI: UserMini?,
    val date: Long?,
    val post: MutableList<FileModel>?,
    val descriptionPost: String?,
    var isLiked: Boolean?,
    var countLike: Int?,
    var countComment: Long?
)

package com.social.solution.feature_post_api

import androidx.fragment.app.FragmentManager
import androidx.lifecycle.MutableLiveData
import com.morozov.core_backend_api.FeatureBackendApi
import com.morozov.core_backend_api.feed.FeedApi
import com.morozov.core_backend_api.message.Message
import com.morozov.core_backend_api.message.MessageApi

interface FeaturePostStarter {

    fun start(
        manager: FragmentManager,
        container: Int,
        bottomNavContainer: Int,
        addToBackStack: Boolean,
        callback: FeaturePostCallback,
        backendApi: FeatureBackendApi,
        post: Message,
        networkState: MutableLiveData<Boolean>
    )
}

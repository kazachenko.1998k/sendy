package com.social.solution.feature_post_api

import com.morozov.core_backend_api.user.UserMini


interface FeaturePostCallback {
    fun openProfile(user: UserMini)
}
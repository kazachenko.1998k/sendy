package com.social.solution.feature_post_api.models

import com.morozov.core_backend_api.file.FileModel
import com.morozov.core_backend_api.user.UserMini

class PostCommentUI(
    val idComment: Long?,
    val owner: UserMini?,
    var date: Long?,
    var files: MutableList<FileModel>?,
    var text: String?,
    var isLike: Boolean?,
    var countLike: Int?,
    var repliesAmount: Long?,
    var replies: MutableList<PostCommentUI>?,
    var isExpanded: Boolean = false,
    var isDeleted: Boolean?,
    var newMessage: Boolean = false
)
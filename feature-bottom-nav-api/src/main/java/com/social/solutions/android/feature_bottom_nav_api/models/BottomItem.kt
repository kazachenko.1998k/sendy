package com.social.solutions.android.feature_bottom_nav_api.models

import android.graphics.drawable.Drawable

data class BottomItem(val name: String, val icon: Drawable, val newEventCount: Int? = null)
package com.social.solutions.android.feature_bottom_nav_api

import com.social.solutions.android.feature_bottom_nav_api.api.CustomizeApi
import com.social.solutions.android.feature_bottom_nav_api.api.DataApi
import com.social.solutions.android.feature_bottom_nav_api.api.EventsApi

interface BottomNavFeatureApi: CustomizeApi, EventsApi, DataApi
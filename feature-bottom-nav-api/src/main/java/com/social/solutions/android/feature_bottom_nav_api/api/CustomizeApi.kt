package com.social.solutions.android.feature_bottom_nav_api.api

import com.social.solutions.android.feature_bottom_nav_api.models.BottomItem

interface CustomizeApi {
    fun addBottomIcon(icon: BottomItem)
    fun replaceIcon(icon: BottomItem)
    fun removeIcon(icon: BottomItem)
}
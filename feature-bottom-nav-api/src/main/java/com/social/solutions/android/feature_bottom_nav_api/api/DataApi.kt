package com.social.solutions.android.feature_bottom_nav_api.api

import com.social.solutions.android.feature_bottom_nav_api.models.BottomItem

interface DataApi {
    fun getAllIcons(): List<BottomItem>
}
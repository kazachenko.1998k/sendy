package com.social.solutions.android.feature_bottom_nav_api

import androidx.fragment.app.FragmentManager
import com.social.solutions.android.feature_bottom_nav_api.models.BottomItem

interface BottomNavStarter {
    fun startDefault(manager: FragmentManager, container: Int,
                     addToBackStack: Boolean, callback: FeatureBottomNavCallback): BottomNavFeatureApi

    fun startCustom(icons: List<BottomItem>, manager: FragmentManager, container: Int,
                    addToBackStack: Boolean, callback: FeatureBottomNavCallback): BottomNavFeatureApi
}
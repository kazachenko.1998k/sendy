package com.social.solutions.android.feature_bottom_nav_api

import com.social.solutions.android.feature_bottom_nav_api.models.BottomItem

interface FeatureBottomNavCallback {
    fun onItemClicked(item: BottomItem)
    fun onReady()
}
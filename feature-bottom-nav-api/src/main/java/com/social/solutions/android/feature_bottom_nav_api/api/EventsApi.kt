package com.social.solutions.android.feature_bottom_nav_api.api

import com.social.solutions.android.feature_bottom_nav_api.models.BottomItem

interface EventsApi {
    fun newEvent(icon: BottomItem)
    fun setSelected(icon: BottomItem)
}
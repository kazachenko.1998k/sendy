package com.social.solutions.android.util_audio_recording

import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import android.view.View
import android.widget.FrameLayout
import androidx.core.view.isVisible
import kotlinx.android.synthetic.main.view_audio_player_top.view.*

class AudioPlayerTopView: FrameLayout {

    lateinit var callback: AudioPlayerTopCallback

    private var lastUserName: String = ""
    private var lastFileName: String = ""
    private var lastVoiceDuration: Long = 0

    constructor(context: Context) : this(context, null)
    constructor(context: Context, attrs: AttributeSet?) : this(context, attrs, 0)
    constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int) : super(context, attrs, defStyleAttr) {
        LayoutInflater.from(context).inflate(R.layout.view_audio_player_top, this, true)

        imagePlay.setOnClickListener {
            callback.onPlay(lastFileName, lastUserName, lastVoiceDuration.toInt())
        }
        imagePause.setOnClickListener {
            callback.onPause(lastFileName)
        }
        imageClose.setOnClickListener {
            doFinishAnimation()
            callback.onCancel()
        }
    }

    fun showPlay() {
        imagePlay.visibility = View.VISIBLE
        imagePause.visibility = View.GONE
    }

    fun showPause() {
        imagePlay.visibility = View.GONE
        imagePause.visibility = View.VISIBLE
    }

    private fun doStartAnimation() {
        if (audioPlayerRoot.isVisible.not())
            audioPlayerRoot.animateShow()
    }

    private fun doFinishAnimation() {
        if (audioPlayerRoot.isVisible)
            audioPlayerRoot.animateHide()
    }

    fun setVoice(fileName: String, userName: String, voiceDuration: Int, withAnimation: Boolean) {
        lastUserName = userName
        lastFileName = fileName
        lastVoiceDuration = voiceDuration.toLong()
        textName.text = userName
        textVoiceTime.text = lastVoiceDuration.formatMillis()
        progressBack.max = voiceDuration

        if (withAnimation)
            doStartAnimation()
    }

    fun setTime(timeProgress: Int) {
        textVoiceTime.text = ((lastVoiceDuration - timeProgress)).formatMillis()
        progressBack.progress = timeProgress
    }

    interface AudioPlayerTopCallback {
        fun onPause(fileName: String)
        fun onPlay(fileName: String, userName: String, time: Int)
        fun onCancel()
    }
}
package com.example.feature_bottom_picker_impl.start

import com.example.feature_bottom_picker_api.FeatureBottomPickerCallback
import com.example.feature_bottom_picker_api.models.StarterModel

object MainObject {
    var mCallback: FeatureBottomPickerCallback? = null
    var mStartConfigs: StarterModel? = null
}
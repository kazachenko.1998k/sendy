package com.example.feature_bottom_picker_impl.start

import androidx.fragment.app.FragmentManager
import com.example.feature_bottom_picker_api.BottomPickerStarterApi
import com.example.feature_bottom_picker_api.FeatureBottomPickerCallback
import com.example.feature_bottom_picker_api.models.StarterModel
import com.example.feature_bottom_picker_impl.ui.fragments.BottomPickerFragment

class BottomPickerStarterImpl: BottomPickerStarterApi {

    companion object{
        const val TAG = "BottomPickerFragment_TAG"
    }

    override fun start(manager: FragmentManager, container: Int,
                       starterModel: StarterModel, callback: FeatureBottomPickerCallback) {
        MainObject.mCallback = callback
        MainObject.mStartConfigs = starterModel
        val fragment = BottomPickerFragment()
        manager.beginTransaction()
            .add(container, fragment)
            .addToBackStack(TAG)
            .commit()
    }
}
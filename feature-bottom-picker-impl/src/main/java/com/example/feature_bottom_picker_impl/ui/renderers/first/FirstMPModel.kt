package com.example.feature_bottom_picker_impl.ui.renderers.first

import com.github.vivchar.rendererrecyclerviewadapter.ViewModel

class FirstMPModel: ViewModel {
    override fun equals(other: Any?): Boolean {
        return other is FirstMPModel
    }

    override fun hashCode(): Int {
        return javaClass.hashCode()
    }
}
package com.example.feature_bottom_picker_impl.ui.fragments.utils

import android.Manifest
import android.content.Context
import android.content.pm.PackageManager
import android.util.Log
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import java.lang.Exception

fun Fragment.checkMyPermissions(andRequest: Boolean, func: () -> Unit) {
    try {
        if (Permissions.checkPermission(context!!))
            func()
        else if (andRequest)
            requestPermissions(Permissions.REQUIRED_PERMISSIONS, Permissions.MY_PERMISSON_REQUEST_CODE)
    } catch (e: Exception) {
        e.printStackTrace()
    }
}

object Permissions {
    const val MY_PERMISSON_REQUEST_CODE = 10
    val REQUIRED_PERMISSIONS by lazy { arrayOf(Manifest.permission.READ_EXTERNAL_STORAGE) }

    var isPermissionsGranted = false

    fun checkPermission(context: Context): Boolean {
        if (ContextCompat.checkSelfPermission(context, REQUIRED_PERMISSIONS[0])
            == PackageManager.PERMISSION_GRANTED) {
            isPermissionsGranted = true
        }
        return isPermissionsGranted
    }

    fun processRequestPermissionsResult(requestCode: Int, permissions: Array<out String>,
                                        grantResults: IntArray, func: () -> Unit) {
        if (requestCode == MY_PERMISSON_REQUEST_CODE) {
            if (permissions.size == 1 &&
                permissions[0] == REQUIRED_PERMISSIONS[0] &&
                grantResults[0] == PackageManager.PERMISSION_GRANTED
            ) {
                isPermissionsGranted = true
                func()
            } else {
                Log.d(this.javaClass.simpleName, "My Location permission denied")
            }
        }
    }
}
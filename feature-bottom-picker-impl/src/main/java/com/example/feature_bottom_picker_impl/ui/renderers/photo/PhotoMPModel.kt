package com.example.feature_bottom_picker_impl.ui.renderers.photo

import android.content.Context
import android.net.Uri
import com.example.feature_bottom_picker_impl.ui.renderers.Selectable
import com.github.vivchar.rendererrecyclerviewadapter.ViewModel
import com.example.feature_bottom_picker_api.models.PhotoModel
import com.example.feature_bottom_picker_impl.ui.utility.ImageFilePath

data class PhotoMPModel(val filePath: String, val position: Int): ViewModel, Selectable {
    override var selectedNumber: Int? = null
    fun convertToPhoto(context: Context): PhotoModel = PhotoModel(filePath)

    override fun equals(other: Any?): Boolean {
        if (other !is PhotoMPModel)
            return false
        return filePath == other.filePath
    }

    override fun hashCode(): Int {
        var result = filePath.hashCode()
        result = 31 * result + position
        result = 31 * result + (selectedNumber ?: 0)
        return result
    }
}
package com.example.feature_bottom_picker_impl.ui.fragments.utils

import android.view.View
import android.widget.EditText
import android.widget.ImageView
import com.example.feature_bottom_picker_impl.R
import com.vanniktech.emoji.EmojiPopup

object EmojiUtils {
    fun initEmoji(view: View, editMessage: EditText, imageEmoji: ImageView) {
        val emojiPopup = EmojiPopup.Builder.fromRootView(view).build(editMessage)
        imageEmoji.setOnClickListener {
            if (emojiPopup.isShowing) {
                imageEmoji.setImageDrawable(view.context.resources.getDrawable(R.drawable.ic_emoji))
                emojiPopup.dismiss()
            }
            else {
                imageEmoji.setImageDrawable(view.context.resources.getDrawable(R.drawable.ic_keyboard))
                emojiPopup.toggle()
            }
        }
    }
}
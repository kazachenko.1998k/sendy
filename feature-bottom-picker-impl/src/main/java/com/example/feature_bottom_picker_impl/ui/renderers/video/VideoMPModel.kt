package com.example.feature_bottom_picker_impl.ui.renderers.video

import android.content.Context
import android.net.Uri
import com.example.feature_bottom_picker_impl.ui.renderers.Selectable
import com.github.vivchar.rendererrecyclerviewadapter.ViewModel
import com.example.feature_bottom_picker_api.models.VideoModel
import com.example.feature_bottom_picker_impl.ui.utility.ImageFilePath

data class VideoMPModel(val filePath: String, val seconds: Int, val position: Int): ViewModel, Selectable {
    override var selectedNumber: Int? = null
    fun convertToVideo(context: Context): VideoModel = VideoModel(filePath, seconds)

    override fun equals(other: Any?): Boolean {
        if (other !is VideoMPModel)
            return false
        return filePath == other.filePath
                && seconds == other.seconds
    }

    override fun hashCode(): Int {
        var result = filePath.hashCode()
        result = 31 * result + seconds
        result = 31 * result + position
        result = 31 * result + (selectedNumber ?: 0)
        return result
    }
}
package com.example.feature_bottom_picker_impl.ui.fragments.utils

import androidx.recyclerview.widget.RecyclerView
import com.example.feature_bottom_picker_impl.start.MainObject
import com.example.feature_bottom_picker_impl.ui.renderers.first.FirstMPModel
import com.example.feature_bottom_picker_impl.ui.renderers.photo.PhotoMPModel
import com.example.feature_bottom_picker_impl.ui.renderers.video.VideoMPModel
import com.github.vivchar.rendererrecyclerviewadapter.RendererRecyclerViewAdapter
import com.github.vivchar.rendererrecyclerviewadapter.ViewModel

private val mPlaceholdersNumber = 20
private var mIsPlaceholdersInside = false

fun MutableList<ViewModel>.addItemFirst(item: ViewModel, recyclerMedia: RecyclerView, mAdapter: RendererRecyclerViewAdapter) {
    if (updateIfContains(item).not())
        add(0, item)
    recyclerMedia.post {
        mAdapter.setItems(this)
    }
}

fun MutableList<ViewModel>.addItemLast(item: ViewModel, recyclerMedia: RecyclerView, mAdapter: RendererRecyclerViewAdapter) {
    if (updateIfContains(item).not()) {
        if (item is VideoMPModel && MainObject.mStartConfigs?.showVideo == true)
            add(item)
        else if (item is PhotoMPModel && MainObject.mStartConfigs?.showPhoto == true)
            add(item)
    }
    recyclerMedia.postDelayed({
        mAdapter.setItems(this)
    }, 100)
}

fun MutableList<ViewModel>.addItemByIndex(index: Int, item: ViewModel, recyclerMedia: RecyclerView, mAdapter: RendererRecyclerViewAdapter) {
    if (updateIfContains(item).not()) {
        if (item is VideoMPModel && MainObject.mStartConfigs?.showVideo == true)
            add(index, item)
        else if (item is PhotoMPModel && MainObject.mStartConfigs?.showPhoto == true)
            add(index, item)
    }
    recyclerMedia.postDelayed({
        mAdapter.setItems(this)
    }, 100)
}

private fun MutableList<ViewModel>.updateIfContains(item: ViewModel): Boolean {
    if (this.contains(item)) {
        val index = this.indexOf(item)
        this.removeAt(index)
        this.add(index, item)
        return true
    }
    return false
}

fun MutableList<ViewModel>.removePlaceholders(recyclerMedia: RecyclerView, mAdapter: RendererRecyclerViewAdapter) {
    if (this.first() is FirstMPModel)
        return
    clear()
    if (MainObject.mStartConfigs?.showMakeMediaCamera == true)
        addItemFirst(FirstMPModel(), recyclerMedia, mAdapter)
    else {
        recyclerMedia.post {
            mAdapter.setItems(this)
        }
    }
}

fun MutableList<ViewModel>.addPlaceholders(recyclerMedia: RecyclerView, mAdapter: RendererRecyclerViewAdapter) {
    for (i in 1 until mPlaceholdersNumber) {
        addItemLast(PhotoMPModel("", i), recyclerMedia, mAdapter)
    }
}
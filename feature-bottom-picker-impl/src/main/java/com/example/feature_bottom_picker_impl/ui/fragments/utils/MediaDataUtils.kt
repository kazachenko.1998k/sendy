package com.example.feature_bottom_picker_impl.ui.fragments.utils

import android.content.Context
import androidx.loader.app.LoaderManager
import com.example.feature_bottom_picker_impl.start.MainObject
import com.social.solutions.rxmedialoader.RxMediaLoader
import com.social.solutions.rxmedialoader.entity.Media
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import java.util.concurrent.TimeUnit

object MediaDataUtils {
    fun loadMedia(context: Context, manager: LoaderManager, onNextMedia: (medias: List<MediaModel>) -> Unit): Disposable? {
        val single = when {
            MainObject.mStartConfigs?.showPhoto == true &&
                    MainObject.mStartConfigs?.showVideo == false -> {
                RxMediaLoader.photos(context, manager)
            }
            MainObject.mStartConfigs?.showVideo == true &&
                    MainObject.mStartConfigs?.showPhoto == false -> {
                RxMediaLoader.videos(context, manager)
            }
            else -> RxMediaLoader.medias(context, manager)
        }
        var index = 1
        return single
        .subscribe ({
            Single.create<List<Media>> { emitter ->
                val mediaList = mutableListOf<Media>()
                it.forEach { album ->
                    mediaList.addAll(album.medias)
                }
                mediaList.sortByDescending {  media ->
                    media.date
                }
                emitter.onSuccess(mediaList)
            }
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe({ mediaList ->
                    onNextMedia(mediaList.map { it.toModel() })
            }, {
                it.printStackTrace()
            })
            },{
            it.printStackTrace()
        })
    }

    private fun Media.toModel(): MediaModel {
        return MediaModel(
                when (type) {
                        Media.Type.Photo -> MediaType.PHOTO
                        Media.Type.Video -> MediaType.VIDEO
                        null -> throw IllegalArgumentException("Wrong media type")
                    },
                this.uri.toString()
        )
    }
}
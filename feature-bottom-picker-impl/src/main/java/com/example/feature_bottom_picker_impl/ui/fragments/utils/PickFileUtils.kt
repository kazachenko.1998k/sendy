package com.example.feature_bottom_picker_impl.ui.fragments.utils

import android.content.Intent
import android.net.Uri
import androidx.fragment.app.Fragment
import com.example.feature_bottom_picker_impl.ui.fragments.utils.PickFileUtils.PICK_CONTENT_REQUEST_CODE
import com.google.android.material.bottomsheet.BottomSheetBehavior

fun Fragment.startPickFileActivity() {
    val intent = Intent(Intent.ACTION_GET_CONTENT)
    intent.type = "*/*"
    startActivityForResult(intent, PICK_CONTENT_REQUEST_CODE)
}

object PickFileUtils {
    const val PICK_CONTENT_REQUEST_CODE = 123

    fun processActivityResult(requestCode: Int, resultCode: Int, data: Intent?): Uri? {
        return if (requestCode == PICK_CONTENT_REQUEST_CODE && data != null)
            data.data as Uri
        else
            null
    }
}
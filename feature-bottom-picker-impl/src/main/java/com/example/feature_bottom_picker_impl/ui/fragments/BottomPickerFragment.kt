package com.example.feature_bottom_picker_impl.ui.fragments

import android.content.Context
import kotlinx.android.synthetic.main.fragment_bottom_picker_feature.*
import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.util.Log
import android.util.Range
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.Toast
import androidx.activity.OnBackPressedCallback
import androidx.annotation.RequiresApi
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.loader.app.LoaderManager
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.feature_bottom_picker_api.models.MediaModel
import com.example.feature_bottom_picker_impl.R
import com.example.feature_bottom_picker_impl.start.MainObject
import com.example.feature_bottom_picker_impl.ui.fragments.utils.*
import com.example.feature_bottom_picker_impl.ui.interaction.MediaInnerAdapterCallback
import com.example.feature_bottom_picker_impl.ui.renderers.Selectable
import com.example.feature_bottom_picker_impl.ui.renderers.first.FirstMPModel
import com.example.feature_bottom_picker_impl.ui.renderers.first.FirstMPViewBinder
import com.example.feature_bottom_picker_impl.ui.renderers.photo.PhotoMPModel
import com.example.feature_bottom_picker_impl.ui.renderers.photo.PhotoMPViewBinder
import com.example.feature_bottom_picker_impl.ui.renderers.video.VideoMPModel
import com.example.feature_bottom_picker_impl.ui.renderers.video.VideoMPViewBinder
import com.example.feature_bottom_picker_impl.ui.utility.animateHide
import com.example.feature_bottom_picker_impl.ui.utility.animateShow
import com.example.util_camera.CameraActivity
import com.example.util_camera.CameraCallback
import com.github.vivchar.rendererrecyclerviewadapter.RendererRecyclerViewAdapter
import com.github.vivchar.rendererrecyclerviewadapter.ViewModel
import com.github.vivchar.rendererrecyclerviewadapter.binder.ViewBinder
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.social.solutions.android.transition_media_pager.manager.MediaPagerManager
import com.social.solutions.android.util_media_pager.models.FileModel
import com.social.solutions.android.util_media_pager.models.MediaType
import java.lang.Exception
import java.lang.ref.SoftReference

class BottomPickerFragment: Fragment(), MediaInnerAdapterCallback {

    companion object{
        private const val ITEMS_BOTTOM_TO_LOAD = 1
        private const val MEDIA_PACK_AMOUNT = 70

        private var mCachedItems = SoftReference<List<ViewModel>>(mutableListOf(FirstMPModel()))
    }

    // Renderer recycler view
    private lateinit var mAdapter: RendererRecyclerViewAdapter
    private val mSpanCount = 3
    private var mAllItems: List<com.example.feature_bottom_picker_impl.ui.fragments.utils.MediaModel>? = null
    private val mItems = mutableListOf<ViewModel>()

    private var lastMediaLoaded = -1

    // Bottom sheet
    private lateinit var mSheetBehavior: BottomSheetBehavior<LinearLayout>
    private var isVisibleAnimate = true

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? =
        inflater.inflate(R.layout.fragment_bottom_picker_feature, container, false)

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        Permissions.processRequestPermissionsResult(requestCode, permissions, grantResults, this::loadMediaData)
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val cachedItems = mCachedItems.get()
        if (cachedItems != null && mItems.isEmpty()) {
            mItems.addAll(cachedItems.map { it.apply {
                    if (this is PhotoMPModel) {
                        this.selectedNumber = null
                    } else if (this is VideoMPModel) {
                        this.selectedNumber = null
                    }
                }
            })
        }

        initRecycler()

        EmojiUtils.initEmoji(view, editMessage, imageEmoji)

        mSheetBehavior = initBottomSheet(view)

        initOtherClickListeners()

        initButtonSend()
    }

    override fun onDestroy() {
        super.onDestroy()
        mCachedItems = if (mItems.size > MEDIA_PACK_AMOUNT)
            SoftReference(mItems.subList(0, MEDIA_PACK_AMOUNT))
        else
            SoftReference(mItems)
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        activity?.onBackPressedDispatcher?.addCallback(this, object : OnBackPressedCallback(true) {
            override fun handleOnBackPressed() {
                mSheetBehavior.state = BottomSheetBehavior.STATE_HIDDEN
            }
        })
    }

    /**     Recycler adapter
     * */

    private fun initRecycler() {
        mAdapter = RendererRecyclerViewAdapter()
        mAdapter.registerRenderers()
        mAdapter.enableDiffUtil()
        mAdapter.setItems(mItems)

        recyclerMedia?.adapter = mAdapter
        recyclerMedia?.layoutManager = GridLayoutManager(context, mSpanCount)

        recyclerMedia?.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
                super.onScrollStateChanged(recyclerView, newState)
                if (mAllItems == null)
                    return
                if (mItems.size < mAllItems!!.size &&
                    (recyclerView.layoutManager as GridLayoutManager).findLastVisibleItemPosition() == mItems.size-ITEMS_BOTTOM_TO_LOAD) {

                    loadNextPack()
                }
            }
        })
    }

    private fun RendererRecyclerViewAdapter.registerRenderers() {
        registerRenderer(ViewBinder(R.layout.item_med_pick_first, FirstMPModel::class.java, FirstMPViewBinder(this@BottomPickerFragment, onMakeMediaListener)))
        registerRenderer(ViewBinder(R.layout.item_med_pick_photo, PhotoMPModel::class.java, PhotoMPViewBinder(this@BottomPickerFragment)))
        registerRenderer(ViewBinder(R.layout.item_med_pick_video, VideoMPModel::class.java, VideoMPViewBinder(this@BottomPickerFragment)))
    }

    private val onMakeMediaListener = View.OnClickListener {
        if (mItems.size <= 1)
            return@OnClickListener
        CameraActivity.start(this, object : CameraCallback {
            override fun onPictureTaken(filePath: String) {
                startNewMediaAcceptActivity(null, mutableListOf(PhotoMPModel(filePath, 0)))
            }

            override fun onVideoTaken(filePath: String, seconds: Int) {
                startNewMediaAcceptActivity(null, mutableListOf(VideoMPModel(filePath, seconds, 0)))
            }
        }, MainObject.mStartConfigs!!.showPhoto, MainObject.mStartConfigs!!.showVideo)
    }

    /**     Other
     * */

    private fun initButtonSend() {
        buttonSendMessage?.setOnClickListener(sendClickListener)
        buttonAttach?.setOnClickListener(sendClickListener)
    }

    private val sendClickListener by lazy {
        object: View.OnClickListener {
            private var isSent = false
            override fun onClick(v: View?) {
                if (isSent)
                    return
                isSent = true
                val selectedData = getSelectedData()
                val mediaSign = getMediaSign()

                context?.let { it1 -> hideKeyboard(it1) }

                if (mediaSign != null)
                    MainObject.mCallback?.onSelected(selectedData, mediaSign)
                else
                    MainObject.mCallback?.onSelected(selectedData)

                makeCloseBottomSheet()
            }
        }
    }

    private fun initOtherClickListeners() {
        viewBack?.setOnClickListener {
            makeCloseBottomSheet()
        }

        buttonBackBottomSheet?.setOnClickListener {
            makeCloseBottomSheet()
        }

        linearGallery?.setOnClickListener {
            mSheetBehavior.state = BottomSheetBehavior.STATE_EXPANDED
        }

        linearFiles?.setOnClickListener {
            startPickFileActivity()
        }
    }

    /**     Bottom sheet
     * */
    private fun initBottomSheet(view: View): BottomSheetBehavior<LinearLayout> {
        val sheetBehavior = BottomSheetBehavior.from(bottomSheet)
        sheetBehavior.initMyBehavior(
            this::processStateChanged,
            this::processSlideChanged
        )
        sheetBehavior.initStartConfigs(view)
        return sheetBehavior
    }

    private fun BottomSheetBehavior<LinearLayout>.initStartConfigs(view: View) {
        val configs = MainObject.mStartConfigs ?: throw IllegalArgumentException("Start configs must not be null")
        if(configs.fullScreen) {
            this.initFullScreen(view)
            showActionBar()
        } else
            this.initHalfScreen(view)
        if (MainObject.mStartConfigs?.showDocuments != true)
            linearBottomFilesBar?.visibility = View.GONE
    }

    private fun processStateChanged(newState: Int) {
        BottomSheet.processStateChanged(
            newState,
            this::stateHidden,
            this::stateExpanded,
            this::stateCollapsed
        )
    }

    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    private fun processSlideChanged(slideOffset: Float) {
        if (slideOffset < 0 && MainObject.mStartConfigs?.showDocuments == true) {
            viewBack.alpha = 1f + slideOffset
            linearBottomFilesBar?.translationY = -3.7f*(slideOffset*linearBottomFilesBar.measuredHeight)
        } else if (Range(0.8f, 0.9f).contains(slideOffset) && linearBottomFilesBar.visibility == View.GONE) {
            if (isVisibleAnimate)
                stateCollapsed()
        }
        if (MainObject.mStartConfigs?.fullScreen != true) {
            if (Range(0.9f, 1.0f).contains(slideOffset))
                showActionBar()
            else
                hideActionBar()
        }
    }

    private fun showActionBar() {
        if (linearActionBar?.isVisible?.not() == true)
            linearActionBar?.visibility = View.VISIBLE
    }

    private fun hideActionBar() {
        if (linearActionBar?.isVisible == true)
            linearActionBar?.visibility = View.GONE
    }

    private fun stateHidden() {
        MainObject.mCallback?.onExit()
    }

    private fun stateExpanded() {
        isVisibleAnimate = true
        linearBottomFilesBar?.animateHide()
    }

    private fun stateCollapsed() {
        isVisibleAnimate = false
        if (constraintMessage?.isVisible != true && MainObject.mStartConfigs?.showDocuments == true)
            linearBottomFilesBar?.animateShow()
    }

    /**     Media data
     * */
    private fun checkPermissionAndLoadMediaData() {
        checkMyPermissions(true, this::loadMediaData)
    }

    private fun loadMediaData() {
        if (Permissions.isPermissionsGranted.not())
            return
        if (mItems.isEmpty())
            mItems.addPlaceholders(recyclerMedia, mAdapter)
        MediaDataUtils.loadMedia(context!!, LoaderManager.getInstance(this)) {
            mAllItems = it
            loadNextPack()
        }
    }

    private fun loadNextPack() {
        if (mAllItems == null)
            return
        for (i in 0 until MEDIA_PACK_AMOUNT) {
            if (lastMediaLoaded >= mAllItems!!.size)
                return
            lastMediaLoaded++
            onNextMedia(lastMediaLoaded+1, mAllItems!![lastMediaLoaded])
        }
    }

    private fun onNextMedia(index: Int, model: com.example.feature_bottom_picker_impl.ui.fragments.utils.MediaModel) {
        if (index == 1)
            mItems.removePlaceholders(recyclerMedia, mAdapter)
        mItems.addItemByIndex(index, model.convertToViewModel(context, mItems.size), recyclerMedia, mAdapter)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        val uri = PickFileUtils.processActivityResult(requestCode, resultCode, data)
        if (uri != null) {
            MainObject.mCallback?.onFileSelected(uri)
            mSheetBehavior.state = BottomSheetBehavior.STATE_HIDDEN
        }
        super.onActivityResult(requestCode, resultCode, data)
    }

    /**     Helper
     * */

    private var isSended = false

    private fun makeCloseBottomSheet() {
        try {
            if (MainObject.mStartConfigs?.fullScreen != true)
                mSheetBehavior.state = BottomSheetBehavior.STATE_HIDDEN
            else
                MainObject.mCallback?.onExit()
        } catch (ex: IllegalStateException) {
            isSended = true
            ex.printStackTrace()
        }
    }

    override fun onResume() {
        try {
            if (isSended)
                MainObject.mCallback?.onExit()
            else {
                bottomSheet.postDelayed({
                    checkPermissionAndLoadMediaData()
                }, 200)
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
        super.onResume()
    }

    private val mClickedItemsList = mutableListOf<Int>()

    private fun getSelectedData(): List<MediaModel> {
        val result = mutableListOf<MediaModel>()
        for ((index, mDatum) in mItems.withIndex()) {
            if (mClickedItemsList.contains(index)) {
                if (mDatum is PhotoMPModel)
                    result.add(mDatum.convertToPhoto(context!!))
                else if (mDatum is VideoMPModel)
                    result.add(mDatum.convertToVideo(context!!))
            }
        }
        return result
    }

    private fun getMediaSign(): String? {
        val text = editMessage?.text.toString() ?: ""
        return if (text.isEmpty()) null else text
    }

    /**     MediaInnerAdapterCallback
     * */

    override fun onItemClicked(item: ViewModel) {
        val position = mItems.indexOf(item)
        when {
            mClickedItemsList.contains(position) -> {
                val removedIndex = mClickedItemsList.indexOf(position)
                mClickedItemsList.remove(position)

                val clickedModel = mItems[position]
                if (clickedModel is Selectable) {
                    clickedModel.selectedNumber = null
                    mAdapter.notifyItemChanged(position, PhotoMPViewBinder.MAKE_NON_SELECTED_ANIM)
                }
                for ((index, mItem) in mClickedItemsList.withIndex()) {
                    if (index >= removedIndex) {
                        val changeModel = mItems[mItem]
                        if (changeModel is Selectable) {
                            changeModel.selectedNumber = index+1
                            mAdapter.notifyItemChanged(mItem, PhotoMPViewBinder.MAKE_NON_SELECTED_ANIM)
                        }
                    }
                }
                if (mClickedItemsList.isEmpty()) {
                    if (MainObject.mStartConfigs?.addComment == true)
                        constraintMessage?.animateHide()
                    else if (MainObject.mStartConfigs?.showDocuments != true)
                        linearAttachMedia?.animateHide()
                    context?.let { hideKeyboard(it) }
                    if (mSheetBehavior.state != BottomSheetBehavior.STATE_EXPANDED && MainObject.mStartConfigs?.showDocuments == true)
                        linearBottomFilesBar?.animateShow()
                }
            }
            mClickedItemsList.size == Constants.MAX_ITEMS_SELECT -> {
                mAdapter.notifyItemChanged(position, PhotoMPViewBinder.MAKE_LIMIT_ANIM)
                Toast.makeText(context, resources.getString(R.string.selected_limit_med_pick), Toast.LENGTH_SHORT).show()
                return
            }
            else -> {
                mClickedItemsList.add(position)
                val changeModel = mItems[position]
                if (changeModel is Selectable) {
                    changeModel.selectedNumber = mClickedItemsList.size
                    mAdapter.notifyItemChanged(position, PhotoMPViewBinder.MAKE_SELECTED_ANIM)
                }
                if (mClickedItemsList.size == 1) {
                    if (MainObject.mStartConfigs?.addComment == true)
                        constraintMessage?.animateShow()
                    else if (MainObject.mStartConfigs?.showDocuments != true)
                        linearAttachMedia?.animateShow()
                    linearBottomFilesBar?.animateHide()
                }
            }
        }
    }

    override fun showMedia(view: View, item: ViewModel) {
        if (MainObject.mStartConfigs?.addComment == true)
            startNewMediaSelectActivity(view, listOf(item))
        else
            startNewMediaAcceptActivity(view, listOf(item))
    }

    private fun startNewMediaSelectActivity(view: View?, tmpList: List<ViewModel>) {
        MediaPagerManager.with(context!!)
            .setUserName("")
            .setItems(tmpList.toPagerMediaList())
            .setMultiPickMode { sign ->
                if(sign.isEmpty())
                    MainObject.mCallback?.onSelected(tmpList.toMediaList())
                else
                    MainObject.mCallback?.onSelected(tmpList.toMediaList(), sign)
                makeCloseBottomSheet()
            }
            .setSharedItem(activity, view, resources.getString(R.string.shared_image_view))
            .start()
    }

    private fun startNewMediaAcceptActivity(view: View?, tmpList: List<ViewModel>) {
        MediaPagerManager.with(context!!)
            .setUserName("")
            .setItems(tmpList.toPagerMediaList())
            .setSinglePickMode {
                MainObject.mCallback?.onSelected(tmpList.toMediaList())
                makeCloseBottomSheet()
            }
            .setSharedItem(activity, view, resources.getString(R.string.shared_image_view))
            .start()
    }

    private fun List<ViewModel>.toMediaList(): List<MediaModel> {
        val resList = mutableListOf<MediaModel>()
        for (datum in this) {
            when(datum){
                is PhotoMPModel -> resList.add(datum.convertToPhoto(context!!))
                is VideoMPModel -> resList.add(datum.convertToVideo(context!!))
            }
        }
        return resList
    }

    private fun List<ViewModel>.toPagerMediaList(): List<com.social.solutions.android.util_media_pager.models.MediaModel> {
        val resList = mutableListOf<com.social.solutions.android.util_media_pager.models.MediaModel>()
        for (datum in this) {
            when(datum){
                is PhotoMPModel -> resList.add(com.social.solutions.android.util_media_pager.models.MediaModel(MediaType.PHOTO, FileModel(null, datum.filePath)))
                is VideoMPModel -> resList.add(com.social.solutions.android.util_media_pager.models.MediaModel(MediaType.VIDEO, FileModel(null, datum.filePath)))
            }
        }
        return resList
    }
}
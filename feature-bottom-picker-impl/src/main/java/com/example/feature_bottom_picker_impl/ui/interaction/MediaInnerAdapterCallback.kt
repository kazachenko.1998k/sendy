package com.example.feature_bottom_picker_impl.ui.interaction

import android.view.View
import com.github.vivchar.rendererrecyclerviewadapter.ViewModel

interface MediaInnerAdapterCallback {
    fun onItemClicked(item: ViewModel)
    fun showMedia(view: View, item: ViewModel)
}
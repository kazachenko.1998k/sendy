package com.example.feature_bottom_picker_impl.ui.renderers.first

import android.os.SystemClock
import android.view.View
import androidx.lifecycle.LifecycleOwner
import com.example.feature_bottom_picker_impl.R
import com.github.vivchar.rendererrecyclerviewadapter.binder.ViewBinder
import com.github.vivchar.rendererrecyclerviewadapter.binder.ViewFinder
import com.otaliastudios.cameraview.CameraView

class FirstMPViewBinder(private val lifecycleOwner: LifecycleOwner,
                        private val listener: View.OnClickListener): ViewBinder.Binder<FirstMPModel>{

    companion object{
        var lastClickTime: Long = 0

        fun click(): Boolean {
            if (SystemClock.elapsedRealtime() - lastClickTime < 1000)
                return false
            lastClickTime = SystemClock.elapsedRealtime()
            return true
        }
    }

    override fun bindView(model: FirstMPModel, finder: ViewFinder, payloads: MutableList<Any>) {
        if (payloads.isEmpty()) {
            val cameraPreview = finder.find<CameraView>(R.id.cameraPreview)
            cameraPreview.setLifecycleOwner(lifecycleOwner)
            finder.setOnClickListener(R.id.imageCamera) {
                if (click())
                    listener.onClick(it)
            }
        }
    }
}
package com.example.feature_bottom_picker_impl.ui.fragments.utils

import android.view.View
import android.widget.LinearLayout
import com.google.android.material.bottomsheet.BottomSheetBehavior

fun BottomSheetBehavior<LinearLayout>.initHalfScreen(view: View) {
    peekHeight = 0
    isHideable = true
    view.post {
        setPeekHeight(view.measuredHeight/2, true)
        state = BottomSheetBehavior.STATE_COLLAPSED
    }
}

fun BottomSheetBehavior<LinearLayout>.initFullScreen(view: View) {
    view.post {
        state = BottomSheetBehavior.STATE_EXPANDED
        setPeekHeight(view.measuredHeight, false)
        isHideable = false
    }
}

fun BottomSheetBehavior<LinearLayout>.initMyBehavior(stateListener: (state: Int) -> Unit,
                                                     slideListener: (slideOffset: Float) -> Unit) {
    addBottomSheetCallback(object : BottomSheetBehavior.BottomSheetCallback() {
        override fun onStateChanged(bottomSheet: View, newState: Int) {
            stateListener(newState)
        }

        override fun onSlide(bottomSheet: View, slideOffset: Float) {
            slideListener(slideOffset)
        }
    })
}

object BottomSheet {
    fun processStateChanged(newState: Int,
                            stateHidden: () -> Unit,
                            stateExpanded: () -> Unit,
                            stateCollapsed: () -> Unit) {
        when (newState) {
            BottomSheetBehavior.STATE_HIDDEN -> {
                stateHidden()
            }
            BottomSheetBehavior.STATE_EXPANDED -> {
                stateExpanded()
            }
            BottomSheetBehavior.STATE_COLLAPSED -> {
                stateCollapsed()
            }
        }
    }
}
package com.example.feature_bottom_picker_impl.ui.renderers

interface Selectable {
    var selectedNumber: Int?
}
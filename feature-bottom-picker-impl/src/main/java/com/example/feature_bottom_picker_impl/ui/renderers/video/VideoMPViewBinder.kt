package com.example.feature_bottom_picker_impl.ui.renderers.video

import android.annotation.SuppressLint
import android.os.Build
import android.view.View
import android.view.animation.AnimationUtils
import android.widget.ImageView
import android.widget.TextView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.DecodeFormat
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.request.RequestOptions
import com.example.feature_bottom_picker_impl.R
import com.example.feature_bottom_picker_impl.start.MainObject
import com.example.feature_bottom_picker_impl.ui.interaction.MediaInnerAdapterCallback
import com.example.feature_bottom_picker_impl.ui.renderers.first.FirstMPViewBinder
import com.example.feature_bottom_picker_impl.ui.utility.MyBounceInterpolator
import com.example.feature_bottom_picker_impl.ui.utility.animateNo
import com.example.feature_bottom_picker_impl.ui.utility.formatMillis
import com.github.vivchar.rendererrecyclerviewadapter.binder.ViewBinder
import com.github.vivchar.rendererrecyclerviewadapter.binder.ViewFinder

class VideoMPViewBinder(private val mCallback: MediaInnerAdapterCallback)
    : ViewBinder.Binder<VideoMPModel> {

    companion object{
        const val MAKE_SELECTED_ANIM = 0
        const val MAKE_NON_SELECTED_ANIM = 1
        const val MAKE_LIMIT_ANIM = 2
    }

    @SuppressLint("SetTextI18n")
    override fun bindView(model: VideoMPModel, finder: ViewFinder, payloads: MutableList<Any>) {
        val textSelectedNumber = finder.find<TextView>(R.id.textSelectedNumber)
        val imageIsSelected = finder.find<ImageView>(R.id.imageIsSelected)
        val imagePreview = finder.find<ImageView>(R.id.imagePreview)

        if (model.selectedNumber != null)
            showSelected(textSelectedNumber, imageIsSelected, model.selectedNumber!!)
        else
            showNonSelected(textSelectedNumber, imageIsSelected)

//        if (MainObject.mStartConfigs?.isSinglePick == true)
            imageIsSelected.visibility = View.INVISIBLE

//        if (MainObject.mStartConfigs?.isSinglePick != true) {
//            imageIsSelected.setOnClickListener {
//                mCallback.onItemClicked(model)
//            }
//        }

        imagePreview.setOnClickListener {
            if (FirstMPViewBinder.click())
                mCallback.showMedia(imagePreview, model)
        }

        when {
            payloads.isEmpty() -> {
                val textVideoTime = finder.find<TextView>(R.id.textVideoTime)

                val previewSize =
                    imagePreview.resources.getDimensionPixelSize(R.dimen.media_preview_size)

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    imagePreview.clipToOutline = true
                }
                val requestOptions = RequestOptions().diskCacheStrategy(DiskCacheStrategy.ALL).circleCrop().format(
                    DecodeFormat.PREFER_RGB_565)
                Glide.with(imagePreview)
                    .load(model.filePath)
                    .override(previewSize, previewSize)
                    .thumbnail(0.01f)
                    .apply(requestOptions)
                    .centerCrop()
                    .into(imagePreview)

                textVideoTime.text = (model.seconds*1000L).formatMillis()
            }
            payloads[0] == MAKE_SELECTED_ANIM -> {
                makeBounceEffect(textSelectedNumber, imageIsSelected)
            }
            payloads[0] == MAKE_NON_SELECTED_ANIM -> {
                makeBounceEffect(textSelectedNumber, imageIsSelected)
            }
            payloads[0] == MAKE_LIMIT_ANIM -> {
                makeLimitAnim(imageIsSelected)
            }
        }
    }

    private fun showSelected(textSelectedNumber: TextView, imageIsSelected: ImageView, number: Int) {
        textSelectedNumber.visibility = View.VISIBLE
//        textSelectedNumber.text = number.toString()
        imageIsSelected.setImageDrawable(imageIsSelected.resources.getDrawable(R.drawable.ic_check_mark))
    }

    private fun showNonSelected(textSelectedNumber: TextView, imageIsSelected: ImageView) {
        textSelectedNumber.visibility = View.INVISIBLE
        textSelectedNumber.text = ""
        imageIsSelected.setImageDrawable(imageIsSelected.resources.getDrawable(R.drawable.ic_non_selected_media_item))
    }

    private fun makeBounceEffect(textSelectedNumber: TextView, imageIsSelected: ImageView) {
        val myAnim = AnimationUtils.loadAnimation(imageIsSelected.context, R.anim.bounce_anim)
        myAnim.interpolator =
            MyBounceInterpolator(
                0.2,
                10.0
            )
        imageIsSelected.startAnimation(myAnim)
        textSelectedNumber.startAnimation(myAnim)
    }

    private fun makeLimitAnim(imageIsSelected: ImageView) {
        imageIsSelected.animateNo()
    }
}
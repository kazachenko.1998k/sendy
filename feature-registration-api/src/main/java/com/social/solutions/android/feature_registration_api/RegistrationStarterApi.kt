package com.social.solutions.android.feature_registration_api

import androidx.fragment.app.FragmentManager
import androidx.lifecycle.LiveData
import com.example.feature_bottom_picker_api.BottomPickerStarterApi
import com.morozov.core_backend_api.FeatureBackendApi

interface RegistrationStarterApi {
    fun start(manager: FragmentManager, container: Int, addToBackStack: Boolean,
              callback: FeatureRegistrationCallback,
              bottomPickerApi: BottomPickerStarterApi,
              networkState: LiveData<Boolean>, backendApi: FeatureBackendApi)
}
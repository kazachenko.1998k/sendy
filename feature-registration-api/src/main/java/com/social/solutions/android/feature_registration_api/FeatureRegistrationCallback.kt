package com.social.solutions.android.feature_registration_api

interface FeatureRegistrationCallback {
    fun onRegistered(uid: Long)
    fun onDestroy()
}
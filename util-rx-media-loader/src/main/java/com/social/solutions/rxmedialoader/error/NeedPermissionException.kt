package com.social.solutions.rxmedialoader.error

class NeedPermissionException(message: String?): RuntimeException(message)
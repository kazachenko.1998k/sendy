package com.social.solutions.rxmedialoader

import android.content.Context
import androidx.loader.app.LoaderManager
import com.social.solutions.rxmedialoader.entity.Album
import com.social.solutions.rxmedialoader.entity.Folder
import com.social.solutions.rxmedialoader.error.NeedPermissionException
import com.social.solutions.rxmedialoader.loader.FolderLoader
import com.social.solutions.rxmedialoader.loader.PhotoLoader
import com.social.solutions.rxmedialoader.loader.VideoLoader
import com.social.solutions.rxmedialoader.util.PermissionUtil
import io.reactivex.Single
import io.reactivex.functions.Function
import java.util.*

object RxMediaLoader {
    fun medias(context: Context, loaderManager: LoaderManager): Single<List<Album>> {
        return folders(context, loaderManager)
            .map(toAlbums())
            .flatMap(photosFunc(context, loaderManager))
            .flatMap(videosFunc(context, loaderManager))
    }

    fun medias(context: Context, loaderManager: LoaderManager, folder: Folder): Single<Album> {
        return Single.just<List<Folder>>(listOf(folder))
            .map(toAlbums())
            .flatMap(photosFunc(context, loaderManager))
            .flatMap(videosFunc(context, loaderManager))
            .map { albums -> albums[0] }
    }

    fun photos(context: Context, loaderManager: LoaderManager): Single<List<Album>> {
        return folders(context, loaderManager)
            .map(toAlbums())
            .flatMap(photosFunc(context, loaderManager))
    }

    fun photos(context: Context, loaderManager: LoaderManager, folder: Folder): Single<Album> {
        return Single.just<List<Folder>>(listOf(folder))
            .map(toAlbums())
            .flatMap(photosFunc(context, loaderManager))
            .map { albums -> albums[0] }
    }

    fun videos(
        context: Context, loaderManager: LoaderManager
    ): Single<List<Album>> {
        return folders(context, loaderManager)
            .map(toAlbums())
            .flatMap(
                videosFunc(
                    context,
                    loaderManager
                )
            )
    }

    fun videos(context: Context, loaderManager: LoaderManager, folder: Folder): Single<Album> {
        return Single.just<List<Folder>>(listOf(folder))
            .map(toAlbums())
            .flatMap(videosFunc(context, loaderManager))
            .map { albums -> albums[0] }
    }

    private fun folders(context: Context, loaderManager: LoaderManager): Single<List<Folder>> {
        return if (!PermissionUtil.hasReadExternalStoragePermission(context)) {
            Single.error(
                NeedPermissionException(
                    "This operation needs android.permission.READ_EXTERNAL_STORAGE"
                )
            )
        } else Single.create { emitter ->
            FolderLoader(
                context,
                loaderManager,
                object: FolderLoader.Callback {
                    override fun onFolderLoaded(folders: List<Folder>) {
                        emitter.onSuccess(folders)
                    }
                }
            )
        }
    }

    private fun toAlbums(): Function<List<Folder>, List<Album>> {
        return Function { folders ->
            val albums: MutableList<Album> = ArrayList()
            for (folder in folders) {
                albums.add(Album(folder))
            }
            albums
        }
    }

    private fun photosFunc(context: Context,
                           loaderManager: LoaderManager): Function<List<Album>, Single<List<Album>>> {
        return Function { albums ->
            Single.create { emitter ->
                PhotoLoader(
                    context,
                    loaderManager,
                    albums,
                    object: PhotoLoader.Callback {
                        override fun onPhotoLoaded(albums: List<Album>) {
                            emitter.onSuccess(albums)
                        }
                    })
            }
        }
    }

    private fun videosFunc(context: Context,
                           loaderManager: LoaderManager): Function<List<Album>, Single<List<Album>>> {
        return Function { albums ->
            Single.create { emitter ->
                VideoLoader(
                    context,
                    loaderManager,
                    albums,
                    object: VideoLoader.Callback {
                        override fun onVideoLoaded(albums: List<Album>) {
                            emitter.onSuccess(albums)
                        }
                    })
            }
        }
    }
}
package com.social.solutions.rxmedialoader.entity

import android.os.Parcel
import android.os.Parcelable

class Folder : Parcelable {
    var id: String? = null
    var name: String? = null

    constructor()

    constructor(parcel: Parcel) {
        id = parcel.readString()
        name = parcel.readString()
    }

    override fun describeContents(): Int = 0

    override fun writeToParcel(dest: Parcel, flags: Int) {
        dest.writeString(id)
        dest.writeString(name)
    }

    companion object {
        @JvmField val CREATOR: Parcelable.Creator<Folder> =
            object: Parcelable.Creator<Folder> {
                override fun createFromParcel(source: Parcel): Folder? = Folder(source)
                override fun newArray(size: Int): Array<Folder?> = arrayOfNulls(size)
            }
    }
}
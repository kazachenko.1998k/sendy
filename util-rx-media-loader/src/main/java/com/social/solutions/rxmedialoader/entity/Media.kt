package com.social.solutions.rxmedialoader.entity

import android.net.Uri
import android.os.Parcel
import android.os.Parcelable

class Media : Parcelable {
    enum class Type { Photo, Video }

    var id: Long = 0
    var date: Long = 0
    var type: Type? = null
    var uri: Uri? = null

    constructor()

    constructor(parcel: Parcel) {
        id = parcel.readLong()
        date = parcel.readLong()
        type = parcel.readSerializable() as Type
        uri = parcel.readParcelable(Uri::class.java.classLoader)
    }

    override fun describeContents(): Int = 0

    override fun writeToParcel(dest: Parcel, flags: Int) {
        dest.writeLong(id)
        dest.writeLong(date)
        dest.writeSerializable(type)
        dest.writeParcelable(uri, flags)
    }

    companion object {
        @JvmField val CREATOR: Parcelable.Creator<Media> =
            object: Parcelable.Creator<Media> {
                override fun createFromParcel(source: Parcel): Media? = Media(source)
                override fun newArray(size: Int): Array<Media?> = arrayOfNulls(size)
            }
    }
}
package com.social.solutions.rxmedialoader.loader

import android.content.Context
import android.database.Cursor
import android.os.Bundle
import android.provider.MediaStore
import androidx.loader.app.LoaderManager
import androidx.loader.content.CursorLoader
import androidx.loader.content.Loader
import com.social.solutions.rxmedialoader.entity.Folder
import com.social.solutions.rxmedialoader.util.valueOf
import java.util.*

class FolderLoader(private val context: Context,
                   loaderManager: LoaderManager,
                   private val callback: Callback
) : LoaderManager.LoaderCallbacks<Cursor?> {

    init {
        loaderManager.restartLoader<Cursor>(0, null, this)
    }

    interface Callback {
        fun onFolderLoaded(folders: List<Folder>)
    }

    override fun onCreateLoader(id: Int, args: Bundle?): Loader<Cursor?> = InternalFolderLoader(context)

    override fun onLoaderReset(loader: Loader<Cursor?>) {}

    override fun onLoadFinished(loader: Loader<Cursor?>, cursor: Cursor?) {
        if (cursor == null) {
            callback.onFolderLoaded(ArrayList())
            return
        }
        val count = cursor.count
        val folders: MutableList<Folder> = ArrayList(count)
        if (cursor.moveToFirst()) {
            do {
                folders.add(cursor.valueOf())
            } while (cursor.moveToNext())
        }
        callback.onFolderLoaded(folders)
    }

    private class InternalFolderLoader(context: Context) :CursorLoader(context, MediaStore.Images.Media.EXTERNAL_CONTENT_URI, PROJECTION, null, null, null) {
        companion object {
            private val PROJECTION = arrayOf(
                MediaStore.Images.Media.BUCKET_ID,
                MediaStore.Images.Media.BUCKET_DISPLAY_NAME,
                MediaStore.Images.Media._ID
            )
//            private const val BUCKET_GROUP_BY = "1) GROUP BY (1"
//            private const val BUCKET_ORDER_BY = "MAX(datetaken) DESC"
        }
    }
}
package com.social.solutions.rxmedialoader.util

import android.database.Cursor
import android.provider.MediaStore
import com.social.solutions.rxmedialoader.entity.Folder

fun Cursor.valueOf(): Folder {
    val folder = Folder()
    folder.id = getString(getColumnIndex(MediaStore.Images.Media.BUCKET_ID))
    folder.name = getString(getColumnIndex(MediaStore.Images.Media.BUCKET_DISPLAY_NAME))
    return folder
}
package com.social.solutions.rxmedialoader.entity

import android.os.Parcel
import android.os.Parcelable
import java.lang.IllegalArgumentException
import java.util.*

class Album : Parcelable {
    var folder: Folder
    var cover: Media? = null
    var medias: MutableList<Media> = ArrayList()

    constructor(folder: Folder) {
        this.folder = folder
    }

    constructor(parcel: Parcel) {
        folder = parcel.readParcelable(Folder::class.java.classLoader) ?: throw IllegalArgumentException("Folder must not be null")
        cover = parcel.readParcelable(Media::class.java.classLoader)
        medias = parcel.createTypedArrayList(Media.CREATOR) ?: throw IllegalArgumentException("Medias must not be null")
    }

    override fun describeContents(): Int = 0

    override fun writeToParcel(dest: Parcel, flags: Int) {
        dest.writeParcelable(folder, flags)
        dest.writeParcelable(cover, flags)
        dest.writeTypedList(medias)
    }

    companion object {
        @JvmField val CREATOR: Parcelable.Creator<Album> =
            object: Parcelable.Creator<Album> {
                override fun createFromParcel(source: Parcel): Album = Album(source)
                override fun newArray(size: Int): Array<Album?> = arrayOfNulls(size)
            }
    }
}
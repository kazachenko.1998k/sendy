package com.social.solutions.rxmedialoader.loader

import android.content.Context
import android.database.Cursor
import android.net.Uri
import android.os.Bundle
import android.provider.MediaStore
import androidx.loader.app.LoaderManager
import androidx.loader.content.CursorLoader
import androidx.loader.content.Loader
import com.social.solutions.rxmedialoader.entity.Album
import com.social.solutions.rxmedialoader.util.video

class VideoLoader(private val context: Context,
                  loaderManager: LoaderManager,
                  private val albums: List<Album>,
                  private val callback: Callback
) : LoaderManager.LoaderCallbacks<Cursor> {

    init {
        loaderManager.restartLoader(0, null, this)
    }

    interface Callback {
        fun onVideoLoaded(albums: List<Album>)
    }

    override fun onCreateLoader(id: Int, args: Bundle?): Loader<Cursor> =
        InternalVideoLoader.newInstance(context, albums)

    override fun onLoaderReset(loader: Loader<Cursor>) {}

    override fun onLoadFinished(loader: Loader<Cursor>, cursor: Cursor) {
        callback.onVideoLoaded(albums)
    }

    private class InternalVideoLoader private constructor(
        context: Context,
        private val albums: List<Album>,
        uri: Uri,
        projection: Array<String>,
        selection: String?,
        selectionArgs: Array<String>?,
        sortOrder: String
    ) : CursorLoader(
        context,
        uri,
        projection,
        selection,
        selectionArgs,
        sortOrder
    ) {
        override fun loadInBackground(): Cursor? {
            val cursor = super.loadInBackground() ?: return null
            if (cursor.moveToFirst()) {
                do {
                    val media = cursor.video()
                    val id = cursor.getString(cursor.getColumnIndex(MediaStore.Video.Media.BUCKET_ID))
                    val album = getAlbum(id)
                    if (album != null) {
                        if (album.medias.isEmpty())
                            album.cover = media
                        album.medias.add(media)
                    }
                } while (cursor.moveToNext())
            }
            return cursor
        }

        private fun getAlbum(id: String): Album? {
            for (album in albums) {
                if (album.folder.id == id)
                    return album
            }
            return null
        }

        companion object {
            private val PROJECTION = arrayOf(
                MediaStore.Video.Media._ID,
                MediaStore.Video.Media.BUCKET_ID,
                MediaStore.Video.Media.DATE_TAKEN
            )

            private const val ORDER_BY = MediaStore.Video.Media.DATE_TAKEN + " DESC"

            fun newInstance(context: Context, albums: List<Album>): CursorLoader {
                return InternalVideoLoader(
                    context,
                    albums,
                    MediaStore.Video.Media.EXTERNAL_CONTENT_URI,
                    PROJECTION,
                    null,
                    null,
                    ORDER_BY
                )
            }
        }
    }
}
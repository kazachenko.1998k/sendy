package com.social.solutions.rxmedialoader.util

import android.content.ContentUris
import android.database.Cursor
import android.net.Uri
import android.provider.MediaStore
import com.social.solutions.rxmedialoader.entity.Media

fun Cursor.photo(): Media {
    val media = Media()
    media.id = getLong(getColumnIndex(MediaStore.Images.Media._ID))
    media.date = getLong(getColumnIndex(MediaStore.Images.Media.DATE_TAKEN))
    media.type = Media.Type.Photo
    media.uri = uri(media.id, media.type!!)
    return media
}

fun Cursor.video(): Media {
    val media = Media()
    media.id = getLong(getColumnIndex(MediaStore.Video.Media._ID))
    media.date = getLong(getColumnIndex(MediaStore.Images.Media.DATE_TAKEN))
    media.type = Media.Type.Video
    media.uri = uri(media.id, media.type!!)
    return media
}

private fun uri(id: Long, type: Media.Type): Uri =
    if (type == Media.Type.Photo)
        ContentUris.withAppendedId(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, id)
    else
        ContentUris.withAppendedId(MediaStore.Video.Media.EXTERNAL_CONTENT_URI, id)
package com.example.util_camera

interface CameraCallback {
    fun onPictureTaken(filePath: String)
    fun onVideoTaken(filePath: String, seconds: Int)
}
package com.example.util_camera

import android.content.Context
import android.content.ContextWrapper
import android.content.Intent
import android.media.MediaPlayer
import android.net.Uri
import android.os.Bundle
import android.os.Environment
import android.view.MotionEvent
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import com.otaliastudios.cameraview.CameraListener
import com.otaliastudios.cameraview.PictureResult
import com.otaliastudios.cameraview.VideoResult
import com.otaliastudios.cameraview.controls.Mode
import com.otaliastudios.cameraview.controls.PictureFormat
import kotlinx.android.synthetic.main.activity_camera.*
import java.io.File
import java.io.FileOutputStream
import java.net.URLEncoder
import java.util.*
import kotlin.math.ceil


class CameraActivity: AppCompatActivity() {

    companion object{
        private const val TAG = "CameraActivity_TAG"

        fun start(fragment: Fragment,callback: CameraCallback , makeJustPhoto: Boolean = false, makeJustVideo: Boolean = false) {
            this.mCallback = callback
            this.makeJustPhoto = makeJustPhoto
            this.makeJustVideo = makeJustVideo
            val intent = Intent(fragment.context, CameraActivity::class.java)
            intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
            fragment.startActivity(intent)
        }

        private lateinit var mCallback: CameraCallback
        private var makeJustPhoto = false
        private var makeJustVideo = false
    }

    private var isRecordButtonLongPressed = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_camera)

        cameraView.pictureFormat = PictureFormat.JPEG

        cameraView.setLifecycleOwner(this)

        cameraView.addCameraListener(object : CameraListener() {
            override fun onPictureTaken(result: PictureResult) {
                val file = getPhotoFile()
                result.toFile(file) {
                    it?:return@toFile
                    mCallback.onPictureTaken(it.path)
                    finish()
                }
            }

            override fun onVideoTaken(result: VideoResult) {
                val time = ceil(MediaPlayer.create(applicationContext, Uri.parse(result.file.path)).duration.toDouble()/1000).toInt()
                mCallback.onVideoTaken(result.file.path, time)
                finish()
            }
        })

        when {
            makeJustPhoto && makeJustVideo.not()  -> {
                textMediaTip.text = resources.getString(R.string.photo_tip)
                initMakePhoto()
            }
            makeJustVideo && makeJustPhoto.not() -> {
                textMediaTip.text = resources.getString(R.string.video_tip)
                initMakeVideo()
            }
            else -> {
                initMakePhoto()
                initMakeVideo()
            }
        }
    }

    private fun initMakePhoto() {
        imageMakeMedia.setOnClickListener {
            cameraView.mode = Mode.PICTURE
            cameraView.takePicture()
        }
    }

    private fun initMakeVideo() {
        imageMakeMedia.setOnLongClickListener(recordHoldListener)
        imageMakeMedia.setOnTouchListener(recordTouchListener)
    }

    private fun getPhotoFile(): File {
        val cw = ContextWrapper(applicationContext)
        val directory = cw.getDir("imageDir", Context.MODE_PRIVATE)
        val generator = Random()
        var n = 10000
        n = generator.nextInt(n)
        return File(directory, "Image-$n.jpg")
    }

    private fun getVideoFile(): File {
        val cw = ContextWrapper(applicationContext)
        val directory = cw.getDir("videoDir", Context.MODE_PRIVATE)
        val generator = Random()
        var n = 10000
        n = generator.nextInt(n)
        return File(directory, "Video-$n.mp4")
    }

    private val recordHoldListener = View.OnLongClickListener {
        cameraView.mode = Mode.VIDEO
        cameraView.takeVideo(getVideoFile())
        textMediaTip.visibility = View.INVISIBLE
        isRecordButtonLongPressed = true
        return@OnLongClickListener true
    }

    private val recordTouchListener = View.OnTouchListener { v, event ->
        v.onTouchEvent(event)
        if (event.action == MotionEvent.ACTION_UP) {
            if (isRecordButtonLongPressed) {
                cameraView.stopVideo()
                textMediaTip.visibility = View.VISIBLE
                isRecordButtonLongPressed = false
            }
        }
        return@OnTouchListener false
    }
}
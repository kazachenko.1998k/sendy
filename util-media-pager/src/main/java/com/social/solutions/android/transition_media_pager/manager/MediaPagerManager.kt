package com.social.solutions.android.transition_media_pager.manager

import android.app.Activity
import android.app.ActivityOptions
import android.content.Context
import android.content.Intent
import android.view.View
import com.social.solutions.android.transition_media_pager.ui.MediaPagerActivity
import com.social.solutions.android.util_media_pager.R
import com.social.solutions.android.util_media_pager.models.MediaModel
import java.lang.IllegalArgumentException
import java.util.*

class MediaPagerManager {

    companion object{
        fun with(context: Context): Builder = Builder(context)
    }

    /**     Required:
     *   @property mUserName
     *   @property mItems
     * */
    class Builder(private val context: Context) {
        private var mUserName: String? = null
        private var mItems: List<MediaModel>? = null
        private var mStartPosition = 0
        private var mDate = Calendar.getInstance()
        private var mSinglePickedCallback: (() -> Unit)? = null
        private var mSendCallback: ((message: String) -> Unit)? = null
        private var mTransitionSharedItemName: String? = null
        private var mBaseActivity: Activity? = null
        private var mTransitionView: View? = null
        private var mComment: String? = null

        fun setComment(comment: String): Builder {
            mComment = comment
            return this
        }

        /**  Use when want transition animation.
         *   @param name: use {R.string.shared_image_view} by default
         *   @param transitionView view that will be start transition point
         * */
        fun setSharedItem(baseActivity: Activity?, transitionView: View?, name: String?): Builder {
//            mBaseActivity = baseActivity
//            mTransitionSharedItemName = name
//            mTransitionView = transitionView
            return this
        }

        /**  @param userName: set null if would like to hide name
         * */
        fun setUserName(userName: String?): Builder {
            mUserName = userName ?: ""
            return this
        }

        fun setItems(items: List<MediaModel>): Builder {
            if (items.isEmpty())
                throw IllegalArgumentException("Media items must not be empty")
            mItems = items
            return this
        }

        fun setStartPosition(startPosition: Int): Builder {
            if (startPosition < 0)
                throw IllegalArgumentException("Start position must be great than zero")
            mStartPosition = startPosition
            return this
        }

        fun setDate(date: Calendar): Builder {
            mDate = date
            return this
        }

        /**  Use when want just show media list. This method is required
         * */
        fun setViewingMode(): Builder {
            mSendCallback = null
            mSinglePickedCallback = null
            return this
        }

        /**  Use when want accept or cancel single media item.
         *   @param pickedCallback call when user clicked accept button
         * */
        fun setSinglePickMode(pickedCallback: () -> Unit): Builder {
            mSendCallback = null
            mSinglePickedCallback = pickedCallback
            return this
        }

        /**  Use when want pick media pack with comment
         * @param sendCallback call when user clicked send button
         * */
        fun setMultiPickMode(sendCallback: (message: String) -> Unit): Builder {
            mSinglePickedCallback = null
            mSendCallback = sendCallback
            return this
        }

        /**  Starting pager activity
         * */
        fun start() {
            val mediaPagerConfigs = MediaPagerConfigs(
                mUserName!!,
                mItems!!,
                mStartPosition,
                mDate,
                mComment,
                mSinglePickedCallback,
                mSendCallback
            )

            val intent = Intent(context, MediaPagerActivity::class.java)
            MediaPagerActivity.mStartConfigs = mediaPagerConfigs
            intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK

            if (mBaseActivity != null && mTransitionView != null) {
                val transition = ActivityOptions.makeSceneTransitionAnimation(
                    mBaseActivity,
                    mTransitionView,
                    mTransitionSharedItemName
                )
                context.startActivity(intent, transition.toBundle())
            } else
                context.startActivity(intent)
        }
    }
}
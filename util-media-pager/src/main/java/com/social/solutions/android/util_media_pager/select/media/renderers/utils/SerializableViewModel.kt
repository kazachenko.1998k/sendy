package com.social.solutions.android.util_media_pager.select.media.renderers.utils

import com.github.vivchar.rendererrecyclerviewadapter.ViewModel
import java.io.Serializable

interface SerializableViewModel: ViewModel, Serializable
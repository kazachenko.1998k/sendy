package com.social.solutions.android.util_media_pager.select.media.renderers.photo

import android.graphics.Bitmap
import android.graphics.drawable.Drawable
import android.view.View
import android.widget.ImageView
import com.bumptech.glide.Glide
import com.bumptech.glide.request.target.CustomTarget
import com.bumptech.glide.request.transition.Transition
import com.github.vivchar.rendererrecyclerviewadapter.binder.ViewBinder
import com.github.vivchar.rendererrecyclerviewadapter.binder.ViewFinder
import com.social.solutions.android.util_media_pager.R
import com.social.solutions.android.util_media_pager.utils.HeaderFooterVisibilityCallback
import kotlinx.android.synthetic.main.item_media_select_photo.view.*

class PhotoViewBinder(private val mCallback: HeaderFooterVisibilityCallback): ViewBinder.Binder<PhotoModel> {
    override fun bindView(model: PhotoModel, finder: ViewFinder, payloads: MutableList<Any>) {
        val imagePhoto = finder.find<ImageView>(R.id.imagePhoto)
        if (payloads.isEmpty()) {
//            if (model.bitmap != null)
//                imagePhoto.setImageBitmap(model.bitmap)
//            else
                Glide.with(imagePhoto.context)
                    .asBitmap()
                    .load(model.filePath)
                    .into(object : CustomTarget<Bitmap>() {
                        override fun onLoadCleared(placeholder: Drawable?) {}
                        override fun onResourceReady(resource: Bitmap, transition: Transition<in Bitmap>?) {
                            finder.setVisibility(R.id.progress, View.INVISIBLE)
                            model.bitmap = resource
                            imagePhoto.setImageBitmap(resource)
                        }
                    })
            imagePhoto.setOnClickListener {
                if (mCallback.isVisible()) {
                    mCallback.onHideHeader()
                    mCallback.onHideFooter(true)
                } else {
                    mCallback.onShowHeader()
                    mCallback.onShowFooter()
                }
            }
        }
    }
}
package com.social.solutions.android.util_media_pager.exoplayer

import android.content.Context
import android.net.Uri
import android.util.Log
import androidx.core.net.toFile
import com.google.android.exoplayer2.C
import com.google.android.exoplayer2.ExoPlayerFactory
import com.google.android.exoplayer2.SimpleExoPlayer
import com.google.android.exoplayer2.offline.FilteringManifestParser
import com.google.android.exoplayer2.offline.StreamKey
import com.google.android.exoplayer2.source.ExtractorMediaSource
import com.google.android.exoplayer2.source.MediaSource
import com.google.android.exoplayer2.source.ProgressiveMediaSource
import com.google.android.exoplayer2.source.dash.DashMediaSource
import com.google.android.exoplayer2.source.dash.manifest.DashManifestParser
import com.google.android.exoplayer2.source.hls.HlsMediaSource
import com.google.android.exoplayer2.source.hls.playlist.DefaultHlsPlaylistParserFactory
import com.google.android.exoplayer2.source.smoothstreaming.SsMediaSource
import com.google.android.exoplayer2.source.smoothstreaming.manifest.SsManifestParser
import com.google.android.exoplayer2.ui.PlayerView
import com.google.android.exoplayer2.upstream.DataSource
import com.google.android.exoplayer2.upstream.DefaultDataSourceFactory
import com.google.android.exoplayer2.upstream.DefaultHttpDataSourceFactory
import com.google.android.exoplayer2.upstream.cache.CacheDataSource
import com.google.android.exoplayer2.upstream.cache.CacheDataSourceFactory
import com.google.android.exoplayer2.util.Util
import com.social.solutions.android.util_media_pager.R
import java.io.File

fun PlayerView.setMyExoPlayer(context: Context, uri: Uri): SimpleExoPlayer {
    player = MyExoplayer.getPlayer(context, uri)
    return MyExoplayer.mPlayer!!
}

object MyExoplayer {
    internal var mPlayer: SimpleExoPlayer? = null
    // Video uri and played time
    private var lastPlayedPair: Pair<Uri, Long>? = null

    internal fun getPlayer(context: Context, uri: Uri): SimpleExoPlayer {
        if (mPlayer == null) {
            mPlayer = ExoPlayerFactory.newSimpleInstance(context)
            preparePlayer(context, uri)
        }
        if (lastPlayedPair?.first == uri)
            mPlayer!!.seekTo(lastPlayedPair!!.second)
        else
            lastPlayedPair = Pair(uri, 0L)
        return mPlayer!!
    }

    private fun releasePlayer() {
        mPlayer?.release()
        mPlayer = null
    }

    fun releaseAndSavePlayer() {
        if (lastPlayedPair != null)
            lastPlayedPair = Pair(lastPlayedPair!!.first, mPlayer?.currentPosition ?: 0L)
        releasePlayer()
    }

    fun releaseFullPlayer() {
        if (mPlayer != null) {
            lastPlayedPair = null
            releasePlayer()
        }
    }

    private fun preparePlayer(context: Context, uri: Uri) {
        if (mPlayer == null)
            throw java.lang.IllegalStateException("mPlayer must not be null")
        val dataSourceFactory: DataSource.Factory = DefaultDataSourceFactory(context, Util.getUserAgent(context, "Sendy"))
        val cacheDataSourceFactory = CacheDataSourceFactory(
            ExoCacheObject.mSimpleCache,
            DefaultHttpDataSourceFactory(context.let {
                Util.getUserAgent(
                    it, context.resources.getString(
                        R.string.app_name
                    )
                )
            }),
            CacheDataSource.FLAG_IGNORE_CACHE_ON_ERROR
        )
        val videoSource: MediaSource = buildMediaSource(uri, if (File(uri.toString()).exists()) {
            Log.i("Jeka", "dataSourceFactory: $uri")
            dataSourceFactory
        } else {
            Log.i("Jeka", "cacheDataSourceFactory: $uri")
            cacheDataSourceFactory
        })
        mPlayer!!.prepare(videoSource)
    }

    private fun buildMediaSource(uri: Uri, dataSourceFactory: DataSource.Factory): MediaSource {
        return ProgressiveMediaSource.Factory(dataSourceFactory).createMediaSource(uri)
    }
}
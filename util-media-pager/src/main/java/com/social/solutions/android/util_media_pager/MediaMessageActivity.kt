package com.social.solutions.android.util_media_pager

import android.Manifest
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.os.Bundle
import android.view.MotionEvent
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.core.view.isVisible
import androidx.viewpager.widget.ViewPager
import com.social.solutions.android.util_media_pager.adapter.MediaPagerAdapter
import com.social.solutions.android.util_media_pager.models.MediaModel
import com.social.solutions.android.util_media_pager.models.MediaType
import com.social.solutions.android.util_media_pager.utils.*
import kotlinx.android.synthetic.main.activity_media_message_mp.*
import kotlinx.android.synthetic.main.dialog_media_more_mp.*
import kotlinx.android.synthetic.main.item_media_footer_mp.*
import kotlinx.android.synthetic.main.item_media_header_mp.*
import org.apache.commons.io.FilenameUtils
import java.lang.ref.WeakReference

@Deprecated(
    message = "This class is not supported anymore.",
    replaceWith = ReplaceWith(
        "MediaPagerManager.with(context)",
        "com.social.solutions.android.transition_media_pager.manager.MediaPagerManager"
    ),
    level = DeprecationLevel.WARNING
)
class MediaMessageActivity : AppCompatActivity(), HeaderFooterVisibilityCallback {

    companion object {
        var headerFooterVisibilityCallback = WeakReference<HeaderFooterVisibilityCallback>(null)

        private const val BUNDLE_EXTRA = "bundle_extra"
        private const val START_POSITION_EXTRA = "start_position_extra"
        private const val ITEMS_LIST_EXTRA = "items_list_extra"
        private const val NAME_HEADER_EXTRA = "name_extra"
        private const val DATE_HEADER_EXTRA = "date_extra"

        private const val REQUEST_PERMISSIONS = 12

        fun start(
            context: Context,
            items: List<MediaModel>,
            startPosition: Int = 0,
            name: String = "",
            date: String = ""
        ) {
            val intent = Intent(context, MediaMessageActivity::class.java)
            intent.putExtra(BUNDLE_EXTRA, createBundle(items, startPosition, name, date))
            intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
            context.startActivity(intent)
        }

        private fun createBundle(
            items: List<MediaModel>,
            startPosition: Int,
            name: String,
            date: String
        ): Bundle {
            return createBundle(Bundle(), items, startPosition, name, date)

        }

        private fun createBundle(
            destination: Bundle,
            items: List<MediaModel>,
            startPosition: Int,
            name: String,
            date: String
        ): Bundle {
            destination.putInt(START_POSITION_EXTRA, startPosition)
            destination.putString(NAME_HEADER_EXTRA, name)
            destination.putString(DATE_HEADER_EXTRA, date)
            destination.putSerializable(ITEMS_LIST_EXTRA, items.toTypedArray())
            return destination
        }
    }

    private var name = ""
    private var date = ""
    private lateinit var mItems: List<MediaModel>
    private var mStartPosition = 0

    private lateinit var mAdapter: MediaPagerAdapter

    private var mIsModeVisible = false

    override fun onCreate(savedInstanceState: Bundle?) {
        headerFooterVisibilityCallback = WeakReference(this)

        if (savedInstanceState != null) {
            parseBundle(savedInstanceState)
        } else {
            if (intent.hasExtra(BUNDLE_EXTRA))
                parseBundle(intent.getBundleExtra(BUNDLE_EXTRA)!!)
            else
                throw IllegalArgumentException("No bundle to start activity!")
        }
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_media_message_mp)

        initViewPager()
        initTopBar()
        initTopDialog()

        pagerMedia.currentItem = mStartPosition
        setSelectedPage(mStartPosition + 1)
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        createBundle(outState, mItems, mStartPosition, name, date)
    }

    override fun dispatchTouchEvent(ev: MotionEvent?): Boolean {
        if (mIsModeVisible) {
            cardMore.makeHideAnimToTop()
            mIsModeVisible = false
        }
        return super.dispatchTouchEvent(ev)
    }

    // Bundle parser
    private fun parseBundle(bundle: Bundle) {
        if (bundle.getSerializable(ITEMS_LIST_EXTRA) == null)
            throw IllegalArgumentException("No bundle argument")
        mStartPosition = bundle.getInt(START_POSITION_EXTRA)
        mItems = (bundle.getSerializable(ITEMS_LIST_EXTRA) as Array<MediaModel>).toList()
        name = bundle.getString(NAME_HEADER_EXTRA, "")
        date = bundle.getString(DATE_HEADER_EXTRA, "")
    }

    // Initializers
    private fun initTopDialog() {
        linearSave.setOnClickListener {
            if (getPermissionMedia())
                saveCurrentMedia()
        }
        linearShare.setOnClickListener {
            val bitmap = mAdapter.getImage(pagerMedia.currentItem) ?: return@setOnClickListener
            applicationContext.shareImage(bitmap)
        }
    }

    private fun saveCurrentMedia() {
        val bitmap = mAdapter.getImage(pagerMedia.currentItem) ?: return
        val mediaUrl = mItems[pagerMedia.currentItem].data.url ?: return
        val mediaName = FilenameUtils.getBaseName(mediaUrl)
//        SaveToGalleryUtils.insertImage(contentResolver, bitmap, mediaName)
        applicationContext.insertImage(bitmap, mediaName)
        Toast.makeText(applicationContext, R.string.toast_image_saved, Toast.LENGTH_SHORT).show()
    }

    private fun initTopBar() {
        imageBack.setOnClickListener {
            onBackPressed()
        }
        imageMore.setOnClickListener {
            cardMore.makeShowAnimToTop()
            mIsModeVisible = true
        }

        textUserName.text = name
        textDate.text = date
        textDate.visibility = if (date == "") View.GONE else View.VISIBLE
    }

    private fun initViewPager() {
        mAdapter = MediaPagerAdapter(supportFragmentManager)
        mAdapter.mData = mItems

        pagerMedia.adapter = mAdapter

        pagerMedia.addOnPageChangeListener(object : ViewPager.OnPageChangeListener {
            override fun onPageScrollStateChanged(state: Int) {}
            override fun onPageScrolled(position: Int,
                                        positionOffset: Float,
                                        positionOffsetPixels: Int) {
                if (mItems[position].mediaType == MediaType.VIDEO)
                    imageMore.visibility = View.GONE
                else
                    imageMore.visibility = View.VISIBLE
            }

            override fun onPageSelected(position: Int) {
                setSelectedPage(position + 1)
            }
        })
    }

    // Animations click
    // HeaderFooterVisibilityCallback
    override fun onShowHeader() {
        viewHead.makeShowAnimToTop()
    }

    override fun onHideHeader() {
        viewHead.makeHideAnimToTop()
    }

    override fun onShowFooter() {
        //viewFooter.makeShowAnimToBottom()
    }

    override fun onHideFooter(isSmooth: Boolean) {
//        if (isSmooth)
//            viewFooter.makeHideAnimToBottom()
//        else
//            viewFooter.visibility = View.GONE
    }

    override fun isVisible(): Boolean {
        return viewHead.isVisible
    }

    override fun hasFooter(): Boolean {
        return true
    }

    // Helper funcs
    private fun setSelectedPage(page: Int) {
        textPosition.text = String.format(
            resources.getString(R.string.media_position),
            page.toString(),
            mAdapter.count.toString()
        )
    }

    // Permission
    override fun onRequestPermissionsResult(requestCode: Int,
                                            permissions: Array<out String>,
                                            grantResults: IntArray) {
        if (requestCode == REQUEST_PERMISSIONS &&
            grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            saveCurrentMedia()
        } else
            super.onRequestPermissionsResult(requestCode, permissions, grantResults)
    }

    private fun getPermissionMedia(): Boolean {
        return if (checkPermissionWriteExSt()) {
            true
        } else {
            ActivityCompat.requestPermissions(
                this,
                listOf(Manifest.permission.WRITE_EXTERNAL_STORAGE).toTypedArray(),
                REQUEST_PERMISSIONS
            )
            false
        }
    }

    private fun checkPermissionWriteExSt(): Boolean {
        return (ContextCompat.checkSelfPermission(
            applicationContext,
            Manifest.permission.WRITE_EXTERNAL_STORAGE
        ) == PackageManager.PERMISSION_GRANTED)
    }
}
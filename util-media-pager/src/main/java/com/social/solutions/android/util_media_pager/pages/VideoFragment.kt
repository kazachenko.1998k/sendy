package com.social.solutions.android.util_media_pager.pages

import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import com.google.android.exoplayer2.Player
import com.social.solutions.android.util_media_pager.MediaMessageActivity
import com.social.solutions.android.util_media_pager.R
import com.social.solutions.android.util_media_pager.exoplayer.MyExoplayer
import com.social.solutions.android.util_media_pager.exoplayer.setMyExoPlayer
import com.social.solutions.android.util_media_pager.models.FileModel
import com.social.solutions.android.util_media_pager.utils.HeaderFooterVisibilityCallback
import kotlinx.android.synthetic.main.item_media_video.*

class VideoFragment: Fragment() {

    companion object{
        private const val FILE_DATA = "file_data_video"

        fun create(fileData: FileModel): Fragment {
            val videoFragment = VideoFragment()
            videoFragment.mFileData = fileData
            return videoFragment
        }
    }

    private var videoEnded = false

    lateinit var mCallback: HeaderFooterVisibilityCallback

    private lateinit var mFileData: FileModel

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? =
        inflater.inflate(R.layout.item_media_video, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        if (savedInstanceState != null) {
            mFileData = savedInstanceState.getSerializable(FILE_DATA) as FileModel
        }

        super.onViewCreated(view, savedInstanceState)

        mCallback = MediaMessageActivity.headerFooterVisibilityCallback.get() ?: throw IllegalArgumentException("headerFooterVisibilityCallback must be initialized")

        playerView.transitionName = resources.getString(R.string.shared_image_view)
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        outState.putSerializable(FILE_DATA, mFileData)
    }

    override fun onDestroy() {
        MyExoplayer.releaseFullPlayer()
        super.onDestroy()
    }

    override fun onPause() {
        if (videoEnded)
            MyExoplayer.releaseFullPlayer()
        else
            MyExoplayer.releaseAndSavePlayer()
        super.onPause()
    }

    override fun onResume() {
        mCallback = MediaMessageActivity.headerFooterVisibilityCallback.get() ?: throw IllegalArgumentException("headerFooterVisibilityCallback must be initialized")
        super.onResume()

        val loadName = when {
            mFileData.filePath != null -> {
                mFileData.filePath
            }
            mFileData.url != null -> {
                mFileData.url
            }
            else -> return
        }

        val player = playerView.setMyExoPlayer(context!!, Uri.parse(loadName))
        player.playWhenReady = true

        if (mCallback.hasFooter().not() || mCallback.useFooter()) {
            playerView.useController = false

            customController.player = player
            customController.show()

            if (mCallback.useFooter()) {
                customController.setBackgroundColor(resources.getColor(R.color.media_header_bottom_mp))
            }

            customController.addVisibilityListener {
                if (it == View.VISIBLE)
                    mCallback.onShowHeader()
                else
                    mCallback.onHideHeader()
                if (mCallback.useFooter()) {
                    if (it == View.VISIBLE)
                        mCallback.onShowFooter()
                    else
                        mCallback.onHideFooter(false)
                }
            }

            playerView.videoSurfaceView?.setOnClickListener {
                if (customController.isVisible)
                    customController.hide()
                else
                    customController.show()
            }
        } else {
            customController.visibility = View.GONE

            playerView.setControllerVisibilityListener {
                if (it != View.VISIBLE) {
                    mCallback.onShowHeader()
                    mCallback.onShowFooter()
                } else {
                    mCallback.onHideHeader()
                    mCallback.onHideFooter(true)
                }
            }

            playerView.hideController()
        }

        player.addListener(object : Player.EventListener {
            override fun onPlayerStateChanged(playWhenReady: Boolean, playbackState: Int) {
                if (playbackState == Player.STATE_BUFFERING)
                    exoProgressLoad.visibility = View.VISIBLE
                else
                    exoProgressLoad.visibility = View.GONE
                videoEnded = playbackState == Player.STATE_ENDED
            }
        })
    }
}
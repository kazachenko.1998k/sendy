package com.social.solutions.android.util_media_pager.exoplayer

import android.content.Context
import com.google.android.exoplayer2.database.ExoDatabaseProvider
import com.google.android.exoplayer2.upstream.cache.LeastRecentlyUsedCacheEvictor
import com.google.android.exoplayer2.upstream.cache.SimpleCache

object ExoCacheObject {
    internal lateinit var mSimpleCache: SimpleCache
    private lateinit var mLRUCacheEvictor: LeastRecentlyUsedCacheEvictor
    private lateinit var mExoDatabaseProvider: ExoDatabaseProvider
    private const val mExoPlayerCacheSize: Long = 90 * 1024 * 1024

    fun initialize(context: Context) {
        mLRUCacheEvictor = LeastRecentlyUsedCacheEvictor(mExoPlayerCacheSize)
        mExoDatabaseProvider = ExoDatabaseProvider(context)
        mSimpleCache = SimpleCache(context.cacheDir, mLRUCacheEvictor, mExoDatabaseProvider)
    }
}
package com.social.solutions.android.transition_media_pager.ui.common

import android.view.View
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.item_media_header_mp.*

internal fun AppCompatActivity.initHeader(name: String?, date: String?) {
    textUserName.text = name ?: ""
    textDate.text = date ?: ""
    textDate.visibility = if (date.isNullOrEmpty()) View.GONE else View.VISIBLE
    textUserName.visibility = if (name.isNullOrEmpty()) View.GONE else View.VISIBLE
}
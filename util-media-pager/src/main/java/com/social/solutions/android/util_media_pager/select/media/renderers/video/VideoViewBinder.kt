package com.social.solutions.android.util_media_pager.select.media.renderers.video

import android.net.Uri
import android.view.View
import android.widget.ProgressBar
import com.github.vivchar.rendererrecyclerviewadapter.binder.ViewBinder
import com.github.vivchar.rendererrecyclerviewadapter.binder.ViewFinder
import com.google.android.exoplayer2.Player
import com.google.android.exoplayer2.ui.PlayerView
import com.social.solutions.android.util_media_pager.R
import com.social.solutions.android.util_media_pager.exoplayer.MyExoplayer
import com.social.solutions.android.util_media_pager.exoplayer.setMyExoPlayer
import com.social.solutions.android.util_media_pager.utils.HeaderFooterVisibilityCallback

class VideoViewBinder(private val mCallback: HeaderFooterVisibilityCallback): ViewBinder.Binder<VideoModel> {
    override fun bindView(model: VideoModel, finder: ViewFinder, payloads: MutableList<Any>) {
        val playerView = finder.find<PlayerView>(R.id.playerView)
        val exoProgressLoad = finder.find<ProgressBar>(R.id.exoProgressLoad)
        if (payloads.isEmpty()) {
            val player = playerView.setMyExoPlayer(playerView.context, Uri.parse(model.filePath))
            player.addListener(object: Player.EventListener {
                override fun onPlayerStateChanged(playWhenReady: Boolean, playbackState: Int) {
                    if (playbackState == Player.STATE_BUFFERING)
                        exoProgressLoad.visibility = View.VISIBLE
                    else
                        exoProgressLoad.visibility = View.GONE
                }
            })
            playerView.setControllerVisibilityListener {
                if (it != View.VISIBLE) {
                    mCallback.onShowHeader()
                    mCallback.onShowFooter()
                } else {
                    mCallback.onHideHeader()
                    mCallback.onHideFooter(true)
                }
            }
            playerView.hideController()
        }
    }
}
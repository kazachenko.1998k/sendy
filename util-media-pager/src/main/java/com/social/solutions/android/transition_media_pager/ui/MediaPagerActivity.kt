package com.social.solutions.android.transition_media_pager.ui

import android.Manifest
import android.annotation.SuppressLint
import android.content.pm.ActivityInfo
import android.content.pm.PackageManager
import android.os.Bundle
import android.text.method.ScrollingMovementMethod
import android.view.MotionEvent
import android.view.View
import android.widget.LinearLayout
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.core.view.isVisible
import androidx.viewpager.widget.ViewPager
import com.social.solutions.android.transition_media_pager.manager.MediaPagerConfigs
import com.social.solutions.android.transition_media_pager.ui.multi.pick.initAsMultiPick
import com.social.solutions.android.transition_media_pager.ui.single.pick.initAsSinglePick
import com.social.solutions.android.transition_media_pager.ui.viewing.initAsViewing
import com.social.solutions.android.util_media_pager.MediaMessageActivity
import com.social.solutions.android.util_media_pager.R
import com.social.solutions.android.util_media_pager.adapter.MediaPagerAdapter
import com.social.solutions.android.util_media_pager.models.MediaType
import com.social.solutions.android.util_media_pager.pages.OnMediaLoadedCallback
import com.social.solutions.android.util_media_pager.utils.*
import kotlinx.android.synthetic.main.dialog_media_more_mp.*
import org.apache.commons.io.FilenameUtils
import java.lang.IllegalArgumentException
import java.lang.ref.WeakReference

internal class MediaPagerActivity: AppCompatActivity(), HeaderFooterVisibilityCallback {

    companion object{
        internal var mStartConfigs: MediaPagerConfigs? = null

        internal var mediaLoadedCallback = WeakReference<OnMediaLoadedCallback>(null)

        private const val REQUEST_PERMISSIONS = 12
    }

    private lateinit var mConfigs: MediaPagerConfigs

    private var mHeader: View? = null
    private var mFooter: View? = null
    private var mMore: View? = null
    private lateinit var mViewPager: ViewPager
    private lateinit var mAdapter: MediaPagerAdapter

    private var shareCurrentMedia = false

    override fun onCreate(savedInstanceState: Bundle?) {
        MediaMessageActivity.headerFooterVisibilityCallback = WeakReference(this)
        super.onCreate(savedInstanceState)
        mConfigs = mStartConfigs ?: return
        mStartConfigs = null

        mAdapter = when{
            mConfigs.sendCallback != null -> this.initAsMultiPick(mConfigs)
            mConfigs.singleMediaPickedCallback != null -> this.initAsSinglePick(mConfigs)
            else -> this.initAsViewing(mConfigs)
        }
        mediaLoadedCallback = WeakReference(mAdapter)

        mHeader = findViewById(R.id.viewHead)
        mFooter = findViewById(R.id.viewFooter)
        mMore = findViewById(R.id.cardMore)
        mViewPager = findViewById(R.id.pagerMedia)

        // More dialog
        if (mMore != null) {
            linearSave.setOnClickListener {
                if (getPermissionMedia())
                    saveCurrentMedia()
            }
            linearShare.setOnClickListener {
                shareCurrentMedia = true
                if (getPermissionMedia()) {
                    val bitmap = mAdapter.getImage(mViewPager.currentItem) ?: return@setOnClickListener
                    applicationContext.shareImage(bitmap)
                    shareCurrentMedia = false
                }
            }
        }

        if (mConfigs.comment.isNullOrEmpty().not()) {
            val textComment = findViewById<TextView>(R.id.textComment)
            val linearFooter = findViewById<LinearLayout>(R.id.linearFooter)
            if (mFooter != null && textComment != null) {
                if (mConfigs.items.first().mediaType == MediaType.VIDEO)
                    linearFooter.visibility = View.INVISIBLE
                textComment.movementMethod = ScrollingMovementMethod()
                mFooter!!.visibility = View.VISIBLE
                textComment.visibility = View.VISIBLE
                textComment.text = mConfigs.comment
            }
        }
    }

    @SuppressLint("SourceLockedOrientationActivity")
    override fun onBackPressed() {
        requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_PORTRAIT
        super.onBackPressed()
    }

    override fun onResume() {
        findViewById<ViewPager>(R.id.pagerMedia)?.transitionName = ""
        super.onResume()
    }

    override fun onSaveInstanceState(outState: Bundle) {
        mStartConfigs = mConfigs
        super.onSaveInstanceState(outState)
    }

    /**     More dialog
     * */
    private fun saveCurrentMedia() {
        val bitmap = mAdapter.getImage(mViewPager.currentItem) ?: return
        val mediaUrl = mConfigs.items[mViewPager.currentItem].data.url ?: return
        val mediaName = FilenameUtils.getBaseName(mediaUrl)
        applicationContext.insertImage(bitmap, mediaName)
        Toast.makeText(applicationContext, R.string.toast_image_saved, Toast.LENGTH_SHORT).show()
    }

    override fun dispatchTouchEvent(ev: MotionEvent?): Boolean {
        if (mMore?.isVisible == true) {
            mMore!!.makeHideAnimToTop()
        }
        return super.dispatchTouchEvent(ev)
    }

    /**     HeaderFooterVisibilityCallback
     * */
    override fun onShowHeader() {
        mHeader?.makeShowAnimToTop()
    }

    override fun onHideHeader() {
        mHeader?.makeHideAnimToTop()
    }

    override fun isVisible(): Boolean {
        return mHeader?.isVisible == true
    }

    override fun onShowFooter() {
        mFooter?.makeShowAnimToBottom()
    }

    override fun onHideFooter(isSmooth: Boolean) {
        if (isSmooth)
            mFooter?.makeHideAnimToBottom()
        else
            mFooter?.visibility = View.GONE
    }

    override fun hasFooter(): Boolean {
        return mConfigs.sendCallback != null
    }

    override fun useFooter(): Boolean {
        return mConfigs.sendCallback == null
                && mConfigs.singleMediaPickedCallback == null
                && mConfigs.comment.isNullOrEmpty().not()
    }

    // Permission
    override fun onRequestPermissionsResult(requestCode: Int,
                                            permissions: Array<out String>,
                                            grantResults: IntArray) {
        if (requestCode == REQUEST_PERMISSIONS &&
            grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            if (shareCurrentMedia) {
                val bitmap = mAdapter.getImage(mViewPager.currentItem) ?: return
                applicationContext.shareImage(bitmap)
                shareCurrentMedia = false
            } else
                saveCurrentMedia()
        } else
            super.onRequestPermissionsResult(requestCode, permissions, grantResults)
    }

    private fun getPermissionMedia(): Boolean {
        return if (checkPermissionWriteExSt()) {
            true
        } else {
            ActivityCompat.requestPermissions(
                this,
                listOf(Manifest.permission.WRITE_EXTERNAL_STORAGE).toTypedArray(),
                REQUEST_PERMISSIONS
            )
            false
        }
    }

    private fun checkPermissionWriteExSt(): Boolean {
        return (ContextCompat.checkSelfPermission(
            applicationContext,
            Manifest.permission.WRITE_EXTERNAL_STORAGE
        ) == PackageManager.PERMISSION_GRANTED)
    }
}
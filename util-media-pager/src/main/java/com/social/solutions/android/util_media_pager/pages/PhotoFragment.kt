package com.social.solutions.android.util_media_pager.pages

import android.graphics.Bitmap
import android.graphics.drawable.Drawable
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.bumptech.glide.Glide
import com.bumptech.glide.request.target.CustomTarget
import com.bumptech.glide.request.transition.Transition
import com.davemorrissey.labs.subscaleview.ImageSource
import com.social.solutions.android.transition_media_pager.ui.MediaPagerActivity
import com.social.solutions.android.util_media_pager.MediaMessageActivity
import com.social.solutions.android.util_media_pager.R
import com.social.solutions.android.util_media_pager.models.FileModel
import com.social.solutions.android.util_media_pager.utils.HeaderFooterVisibilityCallback
import kotlinx.android.synthetic.main.item_media_photo.*
import java.lang.IllegalArgumentException

class PhotoFragment: Fragment() {

    companion object{
        private const val FILE_DATA = "file_data_photo"

        fun create(position: Int, fileData: FileModel): Fragment {
            val photoFragment = PhotoFragment()
            photoFragment.mPosition = position
            photoFragment.mFileData = fileData
            return photoFragment
        }
    }

    private var mPosition: Int = 0

    private lateinit var mOnMediaLoadedCallback: OnMediaLoadedCallback

    lateinit var mFileData: FileModel
    lateinit var mCallback: HeaderFooterVisibilityCallback

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? =
        inflater.inflate(R.layout.item_media_photo, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        if (savedInstanceState != null) {
            mFileData = savedInstanceState.getSerializable(FILE_DATA) as FileModel
        }
        mOnMediaLoadedCallback = MediaPagerActivity.mediaLoadedCallback.get() ?: throw IllegalArgumentException("OnMediaLoadedCallback must not be null")
        super.onViewCreated(view, savedInstanceState)

        mCallback = MediaMessageActivity.headerFooterVisibilityCallback.get() ?: throw IllegalArgumentException("headerFooterVisibilityCallback must be initialized")

        imagePhoto.transitionName = resources.getString(R.string.shared_image_view)

        imagePhoto.setOnClickListener {
            if (mCallback.isVisible()) {
                mCallback.onHideHeader()
                mCallback.onHideFooter(true)
            } else {
                mCallback.onShowHeader()
                mCallback.onShowFooter()
            }
        }

        val loadName = when {
            mFileData.filePath != null -> {
                mFileData.filePath
            }
            mFileData.url != null -> {
                mFileData.url
            }
            else -> return
        }

        Glide.with(view)
            .asBitmap()
            .load(loadName)
            .into(object : CustomTarget<Bitmap>() {
                override fun onLoadCleared(placeholder: Drawable?) {}
                override fun onResourceReady(resource: Bitmap, transition: Transition<in Bitmap>?) {
                    mOnMediaLoadedCallback.onBitmapReady(mPosition, resource)
                    progress.visibility = View.INVISIBLE
                    imagePhoto.setImage(ImageSource.bitmap(resource))
                }
            })
    }

    override fun onResume() {
        mCallback = MediaMessageActivity.headerFooterVisibilityCallback.get() ?: throw IllegalArgumentException("headerFooterVisibilityCallback must be initialized")
        super.onResume()
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        outState.putSerializable(FILE_DATA, mFileData)
    }
}
package com.social.solutions.android.transition_media_pager.ui.single.pick

import androidx.appcompat.app.AppCompatActivity
import com.social.solutions.android.transition_media_pager.manager.MediaPagerConfigs
import com.social.solutions.android.transition_media_pager.ui.common.initMedia
import com.social.solutions.android.util_media_pager.R
import com.social.solutions.android.util_media_pager.adapter.MediaPagerAdapter
import com.social.solutions.android.util_media_pager.models.MediaType
import kotlinx.android.synthetic.main.activity_multi_pick_media_pager.*
import kotlinx.android.synthetic.main.create_groupitem_media_header.*

internal fun AppCompatActivity.initAsSinglePick(configs: MediaPagerConfigs): MediaPagerAdapter {
    setContentView(R.layout.activity_single_pick_media_pager)

    title_select_photo?.setText(if (configs.items.first().mediaType == MediaType.PHOTO) R.string.select_photo else R.string.select_video)

    val disableButtons = {
        image_close.isEnabled = false
        image_accept.isEnabled = false
    }

    image_close.setOnClickListener {
        disableButtons()
        onBackPressed()
    }

    image_accept.setOnClickListener {
        disableButtons()
        configs.singleMediaPickedCallback?.let { run -> run() }
        finish()
    }

    return pagerMedia.initMedia(supportFragmentManager, configs.items)
}
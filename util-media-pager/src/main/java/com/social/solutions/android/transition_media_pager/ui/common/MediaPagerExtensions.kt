package com.social.solutions.android.transition_media_pager.ui.common

import android.view.View
import androidx.fragment.app.FragmentManager
import androidx.viewpager.widget.ViewPager
import com.social.solutions.android.util_media_pager.adapter.MediaPagerAdapter
import com.social.solutions.android.util_media_pager.models.MediaModel

internal fun ViewPager.initMedia(manager: FragmentManager, items: List<MediaModel>): MediaPagerAdapter {
    val mAdapter = MediaPagerAdapter(manager)
    mAdapter.mData = items
    this.adapter = mAdapter
    return mAdapter
}

internal fun ViewPager.setPageSelectedCallback(selected: (page: Int) -> Unit) {
    addOnPageChangeListener(object : ViewPager.OnPageChangeListener {
        override fun onPageScrollStateChanged(state: Int) {}
        override fun onPageSelected(position: Int) {}
        override fun onPageScrolled(position: Int,
                                    positionOffset: Float,
                                    positionOffsetPixels: Int) {
            selected(position)
        }
    })
}
package com.social.solutions.android.util_media_pager.models

import java.io.Serializable

data class MediaModel(val mediaType: MediaType, val data: FileModel): Serializable
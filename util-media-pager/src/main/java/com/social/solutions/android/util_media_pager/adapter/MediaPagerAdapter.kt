package com.social.solutions.android.util_media_pager.adapter

import android.graphics.Bitmap
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentStatePagerAdapter
import com.social.solutions.android.util_media_pager.models.FileModel
import com.social.solutions.android.util_media_pager.models.MediaModel
import com.social.solutions.android.util_media_pager.models.MediaType
import com.social.solutions.android.util_media_pager.pages.OnMediaLoadedCallback
import com.social.solutions.android.util_media_pager.pages.PhotoFragment
import com.social.solutions.android.util_media_pager.pages.VideoFragment
import com.social.solutions.android.util_media_pager.utils.HeaderFooterVisibilityCallback

class MediaPagerAdapter(manager: FragmentManager)
    : FragmentStatePagerAdapter(manager, BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT),
      OnMediaLoadedCallback {

    lateinit var mData: List<MediaModel>
    private val mLoadedImages = mutableMapOf<Int, Bitmap>()

    fun getImage(position: Int): Bitmap? = mLoadedImages[position]

    override fun getItem(position: Int): Fragment {
        return when(mData[position].mediaType) {
            MediaType.PHOTO -> PhotoFragment.create(position, mData[position].data)
            MediaType.VIDEO -> VideoFragment.create(mData[position].data)
        }
    }

    override fun getCount(): Int = mData.size

    // OnMediaLoadedCallback
    override fun onBitmapReady(position: Int, bitmap: Bitmap) {
        mLoadedImages[position] = bitmap
    }
}
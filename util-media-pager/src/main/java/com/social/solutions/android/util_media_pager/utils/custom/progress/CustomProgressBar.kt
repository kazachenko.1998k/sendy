package com.social.solutions.android.util_media_pager.utils.custom.progress

import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import android.widget.FrameLayout
import android.widget.SeekBar
import com.google.android.exoplayer2.ui.TimeBar
import com.social.solutions.android.util_media_pager.R
import kotlinx.android.synthetic.main.custom_progress_bar_view.view.*

class CustomProgressBar: FrameLayout, TimeBar {

    private val scrubListeners = mutableListOf<TimeBar.OnScrubListener>()

    constructor(context: Context) : this(context, null)
    constructor(context: Context, attrs: AttributeSet?) : this(context, attrs, 0)
    constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int) : super(context, attrs, defStyleAttr) {
        LayoutInflater.from(context).inflate(R.layout.custom_progress_bar_view, this)
        progressView.setOnSeekBarChangeListener(object : SeekBar.OnSeekBarChangeListener {
            override fun onProgressChanged(seekBar: SeekBar?, progress: Int, fromUser: Boolean) {
                viewBackground.max = progressView.max
                viewBackground.progress = progressView.progress
                for (listener in scrubListeners) {
                    listener.onScrubMove(this@CustomProgressBar, progress.toLong())
                }
            }
            override fun onStartTrackingTouch(seekBar: SeekBar?) {
                for (listener in scrubListeners) {
                    listener.onScrubStart(this@CustomProgressBar, progressView.progress.toLong())
                }
            }
            override fun onStopTrackingTouch(seekBar: SeekBar?) {
                for (listener in scrubListeners) {
                    listener.onScrubStop(this@CustomProgressBar, progressView.progress.toLong(), false)
                }
            }
        })
    }

    override fun addListener(listener: TimeBar.OnScrubListener) {
        scrubListeners.add(listener)
    }

    override fun setDuration(duration: Long) {
        progressView.max = duration.toInt()
    }

    override fun removeListener(listener: TimeBar.OnScrubListener) {
        scrubListeners.remove(listener)
    }

    override fun setPosition(position: Long) {
        progressView.progress = position.toInt()
    }

    override fun setAdGroupTimesMs(adGroupTimesMs: LongArray?, playedAdGroups: BooleanArray?, adGroupCount: Int) {}

    override fun setKeyCountIncrement(count: Int) {}

    override fun setBufferedPosition(bufferedPosition: Long) {}

    override fun setKeyTimeIncrement(time: Long) {}

    override fun getPreferredUpdateDelay(): Long = 100L
}
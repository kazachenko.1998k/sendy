package com.social.solutions.android.util_media_pager.models

import android.graphics.Bitmap
import java.io.Serializable

data class FileModel(val url: String?, val filePath: String?): Serializable
package com.social.solutions.android.util_media_pager.models

data class MessageModel(val userId: Long, val startPosition: Int,
                        val senderName: String, val sendDate: String,
                        val media: List<MediaModel>)
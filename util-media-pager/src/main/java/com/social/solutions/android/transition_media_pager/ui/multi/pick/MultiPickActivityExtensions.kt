package com.social.solutions.android.transition_media_pager.ui.multi.pick

import androidx.appcompat.app.AppCompatActivity
import com.social.solutions.android.transition_media_pager.manager.MediaPagerConfigs
import com.social.solutions.android.transition_media_pager.ui.common.initMedia
import com.social.solutions.android.util_media_pager.R
import com.social.solutions.android.util_media_pager.adapter.MediaPagerAdapter
import com.vanniktech.emoji.EmojiPopup
import kotlinx.android.synthetic.main.activity_multi_pick_media_pager.*
import kotlinx.android.synthetic.main.item_media_select_footer_mp.*
import kotlinx.android.synthetic.main.item_media_select_header_mp.*

internal fun AppCompatActivity.initAsMultiPick(configs: MediaPagerConfigs): MediaPagerAdapter {
    setContentView(R.layout.activity_multi_pick_media_pager)

    val disableButtons = {
        imageBack.isEnabled = false
        buttonSendMessage.isEnabled = false
    }

    imageBack.setOnClickListener {
        disableButtons()
        onBackPressed()
    }

    val emojiPopup = EmojiPopup.Builder.fromRootView(frameRoot).build(editMessage)
    imageEmoji.setOnClickListener {
        if (emojiPopup.isShowing) {
            imageEmoji.setImageDrawable(resources.getDrawable(R.drawable.ic_emoji))
            emojiPopup.dismiss()
        } else {
            imageEmoji.setImageDrawable(resources.getDrawable(R.drawable.ic_keyboard))
            emojiPopup.toggle()
        }
    }

    buttonSendMessage.setOnClickListener {
        disableButtons()
        configs.sendCallback?.let { run -> run(editMessage.text.toString()) }
        finish()
    }

    return pagerMedia.initMedia(supportFragmentManager, configs.items)
}
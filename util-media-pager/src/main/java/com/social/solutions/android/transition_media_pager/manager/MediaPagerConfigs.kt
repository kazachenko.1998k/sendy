package com.social.solutions.android.transition_media_pager.manager

import com.social.solutions.android.util_media_pager.models.MediaModel
import java.util.*

internal data class MediaPagerConfigs(val userName: String,
                                      val items: List<MediaModel>,
                                      val startPosition: Int,
                                      val date: Calendar,
                                      val comment: String? = null,
                                      val singleMediaPickedCallback: (() -> Unit)?,
                                      val sendCallback: ((message: String) -> Unit)?)
package com.social.solutions.android.util_media_pager.select.media

import android.content.Intent
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import android.widget.EditText
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import com.github.vivchar.rendererrecyclerviewadapter.RendererRecyclerViewAdapter
import com.github.vivchar.rendererrecyclerviewadapter.binder.ViewBinder
import com.social.solutions.android.util_media_pager.R
import com.social.solutions.android.util_media_pager.exoplayer.MyExoplayer
import com.social.solutions.android.util_media_pager.select.media.renderers.photo.PhotoModel
import com.social.solutions.android.util_media_pager.select.media.renderers.photo.PhotoViewBinder
import com.social.solutions.android.util_media_pager.select.media.renderers.utils.MediaSelectCallback
import com.social.solutions.android.util_media_pager.select.media.renderers.utils.SerializableViewModel
import com.social.solutions.android.util_media_pager.select.media.renderers.video.VideoModel
import com.social.solutions.android.util_media_pager.select.media.renderers.video.VideoViewBinder
import com.social.solutions.android.util_media_pager.utils.*
import com.vanniktech.emoji.EmojiPopup
import kotlinx.android.synthetic.main.activity_media_select_mp.*
import kotlinx.android.synthetic.main.item_media_select_footer_mp.*
import kotlinx.android.synthetic.main.item_media_select_header_mp.*

@Deprecated(
    message = "This class is not supported anymore.",
    replaceWith = ReplaceWith(
        "MediaPagerManager.with(context)",
        "com.social.solutions.android.transition_media_pager.manager.MediaPagerManager"
    ),
    level = DeprecationLevel.WARNING
)
class MediaSelectingActivity: AppCompatActivity(), HeaderFooterVisibilityCallback {

    companion object{
        const val BUNDLE_EXTRA = "bundle_extra"
        const val ITEMS_LIST_EXTRA = "items_list_extra"
        const val SIGN_EXTRA = "sign_extra"

        const val REQUEST_CODE = 12

        private var mCallback: MediaSelectCallback? = null

        fun start(fragment: Fragment, items: List<SerializableViewModel>, callback: MediaSelectCallback) {
            mCallback = callback
            val intent = Intent(fragment.context, MediaSelectingActivity::class.java)
            intent.putExtra(BUNDLE_EXTRA, createBundle(items))
            intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
            fragment.startActivity(intent)
        }

        private fun createBundle(items: List<SerializableViewModel>): Bundle {
            return createBundle(Bundle(), items)
        }

        private fun createBundle(destination: Bundle, items: List<SerializableViewModel>, sign: String? = null): Bundle {
            destination.putSerializable(ITEMS_LIST_EXTRA, items.toTypedArray())
            if (sign != null)
                destination.putString(SIGN_EXTRA, sign)
            return destination
        }
    }

    // Media recycler
    private lateinit var mRecyclerViewAdapter: RendererRecyclerViewAdapter
    private lateinit var mItems: List<SerializableViewModel>

    override fun onCreate(savedInstanceState: Bundle?) {
        if (savedInstanceState != null) {
            parseBundle(savedInstanceState)
        } else {
            if (intent.hasExtra(BUNDLE_EXTRA))
                parseBundle(intent.getBundleExtra(BUNDLE_EXTRA)!!)
            else
                throw java.lang.IllegalArgumentException("No bundle to start activity!")
        }

        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_media_select_mp)

        mRecyclerViewAdapter = RendererRecyclerViewAdapter()
        mRecyclerViewAdapter.registerRenderers()
        mRecyclerViewAdapter.enableDiffUtil()
        mRecyclerViewAdapter.setItems(mItems)

        recyclerMedia.adapter = mRecyclerViewAdapter

        imageBack.setOnClickListener {
            onBackPressed()
        }

        val emojiPopup = EmojiPopup.Builder.fromRootView(frameRoot).build(editMessage)
        imageEmoji.setOnClickListener {
            if (emojiPopup.isShowing) {
                imageEmoji.setImageDrawable(resources.getDrawable(R.drawable.ic_emoji))
                emojiPopup.dismiss()
            } else {
                imageEmoji.setImageDrawable(resources.getDrawable(R.drawable.ic_keyboard))
                emojiPopup.toggle()
            }
        }

        buttonSendMessage.setOnClickListener {
            mCallback?.onSelected(mItems, editMessage.text.toString())
            finish()
        }
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        createBundle(outState, mItems)
        MyExoplayer.releaseAndSavePlayer()
    }

    override fun onDestroy() {
        MyExoplayer.releaseFullPlayer()
        super.onDestroy()
    }

    // Renderers
    private fun RendererRecyclerViewAdapter.registerRenderers() {
        registerRenderer(ViewBinder(R.layout.item_media_select_photo, PhotoModel::class.java, PhotoViewBinder(this@MediaSelectingActivity)))
        registerRenderer(ViewBinder(R.layout.item_media_select_video, VideoModel::class.java, VideoViewBinder(this@MediaSelectingActivity)))
    }

    // Bundle parser
    private fun parseBundle(bundle: Bundle) {
        if (bundle.getSerializable(ITEMS_LIST_EXTRA) == null)
            throw IllegalArgumentException("No bundle argument")
        mItems = (bundle.getSerializable(ITEMS_LIST_EXTRA) as Array<SerializableViewModel>).toList()
    }

    // Animations click
    // HeaderFooterVisibilityCallback
    override fun onShowHeader() {
        viewHead.makeShowAnimToTop()
    }

    override fun onHideHeader() {
        viewHead.makeHideAnimToTop()
    }

    override fun onShowFooter() {
        viewFooter.makeShowAnimToBottom()
    }

    override fun onHideFooter(isSmooth: Boolean) {
        if (isSmooth)
            viewFooter.makeHideAnimToBottom()
        else
            viewFooter.visibility = View.GONE
    }

    override fun isVisible(): Boolean {
        return viewHead.isVisible
    }

    override fun hasFooter(): Boolean {
        return false
    }

    // Button send
    private fun checkButtonEnable(text: String?) {
        val isEnabled = text.isNullOrEmpty().not()
        buttonSendMessage.isEnabled = isEnabled
        val contextTmp = applicationContext
        if (isEnabled) {
            buttonSendMessage.setColorFilter(ContextCompat.getColor(contextTmp, R.color.button_enable), android.graphics.PorterDuff.Mode.SRC_IN)
        }
        else {
            buttonSendMessage.setColorFilter(ContextCompat.getColor(contextTmp, R.color.button_disable), android.graphics.PorterDuff.Mode.SRC_IN)
        }
    }

    // Extensions
    fun EditText.addMyTextChangeListener(callback:(text:String?)->Unit) {
        this.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(p0: Editable?) {}
            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}
            override fun onTextChanged(string: CharSequence?, p1: Int, p2: Int, p3: Int) {
                if(string == null)
                    callback(null)
                else
                    callback(string.toString())
            }
        })
    }
}
package com.social.solutions.android.util_media_pager.models

import java.io.Serializable

enum class MediaType: Serializable {
    VIDEO,
    PHOTO
}
package com.social.solutions.android.util_media_pager.select.media.renderers.utils

import java.io.Serializable

interface MediaSelectCallback: Serializable {
    fun onSelected(data: List<SerializableViewModel>, sign: String)
}
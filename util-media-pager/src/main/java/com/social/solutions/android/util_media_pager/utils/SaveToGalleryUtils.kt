package com.social.solutions.android.util_media_pager.utils

import android.R.attr
import android.content.ContentResolver
import android.content.ContentValues
import android.content.Context
import android.content.Intent
import android.graphics.Bitmap
import android.net.Uri
import android.os.Environment
import android.provider.MediaStore.Images
import android.util.Log
import java.io.File
import java.io.FileOutputStream
import java.io.IOException
import java.text.SimpleDateFormat
import java.util.*

fun Context.insertImage(source: Bitmap,
                        title: String): Uri? {
    val timeStamp: String = SimpleDateFormat("ddMMyyyy_HHmmss").format(Date())
    val fileName = "sendy$timeStamp.JPG"

    val exDir = "${getExternalFilesDir(Environment.DIRECTORY_PICTURES)}/${SaveToGalleryUtils.mAppFolderName}"

    val direct = File(exDir)

    if (!direct.exists()) {
        val wallpaperDirectory = File(exDir)
        wallpaperDirectory.mkdirs()
    }

    val file = File(File(exDir), fileName)
    if (file.exists())
        file.delete()

    try {
        val out = FileOutputStream(file)
        source.compress(Bitmap.CompressFormat.JPEG, 20, out)
        out.flush()
        out.close()
    } catch (e: Exception) {
        e.printStackTrace()
    }

    val values = ContentValues()
    values.put(Images.Media.TITLE, title)
    values.put(Images.Media.DATE_TAKEN, System.currentTimeMillis())
    values.put(Images.ImageColumns.BUCKET_ID, file.toString().toLowerCase(Locale.US).hashCode())
    values.put(Images.ImageColumns.BUCKET_DISPLAY_NAME, file.name.toLowerCase(Locale.US))
    values.put("_data", file.absolutePath)
    return contentResolver.insert(Images.Media.EXTERNAL_CONTENT_URI, values)
}

fun Context.shareImage(bitmap: Bitmap) {
    val i = Intent(Intent.ACTION_SEND)
    i.type = "image/*"
    i.putExtra(Intent.EXTRA_STREAM, insertImage(bitmap, "Share"))
    val chIntent = Intent.createChooser(i, "Share Image")
    chIntent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
    startActivity(chIntent)
}

object SaveToGalleryUtils {

    const val mAppFolderName = "Sendy"

    fun insertImage(contentResolver: ContentResolver,
                    source: Bitmap,
                    title: String) {
        val values = ContentValues()
        values.put(Images.Media.TITLE, title)
        values.put(Images.Media.DISPLAY_NAME, title)
        values.put(Images.Media.DESCRIPTION, attr.description)
        values.put(Images.Media.MIME_TYPE, "image/jpeg")
        values.put(Images.Media.DATE_ADDED, System.currentTimeMillis())
        values.put(Images.Media.DATE_TAKEN, System.currentTimeMillis())

        val uri = contentResolver.insert(Images.Media.EXTERNAL_CONTENT_URI, values)
        if (uri != null) {
            contentResolver.openOutputStream(uri).use { outStream ->
                source.compress(Bitmap.CompressFormat.JPEG, 50, outStream)
            }
        } else {
            Log.e("SaveToGalleryUtils", "Returned from content resolver URI is null")
        }
    }
}
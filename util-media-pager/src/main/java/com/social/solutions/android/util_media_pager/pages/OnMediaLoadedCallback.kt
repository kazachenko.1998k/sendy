package com.social.solutions.android.util_media_pager.pages

import android.graphics.Bitmap
import java.io.Serializable

interface OnMediaLoadedCallback: Serializable {
    fun onBitmapReady(position: Int, bitmap: Bitmap)
}
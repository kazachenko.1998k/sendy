package com.social.solutions.android.util_media_pager.exoplayer

import android.content.Context
import android.net.Uri
import android.util.Log
import androidx.work.*
import com.google.android.exoplayer2.upstream.DataSource
import com.google.android.exoplayer2.upstream.DataSpec
import com.google.android.exoplayer2.upstream.DefaultDataSourceFactory
import com.google.android.exoplayer2.upstream.cache.CacheKeyFactory
import com.google.android.exoplayer2.upstream.cache.CacheUtil
import com.google.android.exoplayer2.util.Util
import com.social.solutions.android.util_media_pager.R

class ExoCacheWorker(context: Context, parameters: WorkerParameters) : CoroutineWorker(context, parameters) {

    companion object{
        private const val VIDEO_URL = "com.morozov.EXO.VIDEO.CACHE.URL"

        fun cacheVideo(context: Context, url: String) {
            val request = OneTimeWorkRequestBuilder<ExoCacheWorker>()
                .addTag("ExoCacheWorker")
                .setInputData(workDataOf(VIDEO_URL to url))
                .build()
            WorkManager.getInstance(context).enqueue(request)
        }
    }

    override suspend fun doWork(): Result {
        val uri = Uri.parse(inputData.getString(VIDEO_URL))
        val dataSpec = DataSpec(uri)
        val defaultCacheKeyFactory = CacheUtil.DEFAULT_CACHE_KEY_FACTORY
        val progressListener = CacheUtil.ProgressListener { requestLength, bytesCached, _ ->
            val downloadPercentage: Double = (bytesCached * 100.0 / requestLength)
            Log.i("ExoCacheWorker", "Url: $uri; Progress: $downloadPercentage")
        }
        val dataSource: DataSource =
            DefaultDataSourceFactory(applicationContext,
                Util.getUserAgent(applicationContext, applicationContext.resources.getString(R.string.app_name))).createDataSource()
        cacheVideo(dataSpec, defaultCacheKeyFactory, dataSource, progressListener)
        return Result.success()
    }

    private fun cacheVideo(dataSpec: DataSpec,
                           defaultCacheKeyFactory: CacheKeyFactory?,
                           dataSource: DataSource,
                           progressListener: CacheUtil.ProgressListener) {
        CacheUtil.cache(
            dataSpec,
            ExoCacheObject.mSimpleCache,
            defaultCacheKeyFactory,
            dataSource,
            progressListener,
            null
        )
    }
}
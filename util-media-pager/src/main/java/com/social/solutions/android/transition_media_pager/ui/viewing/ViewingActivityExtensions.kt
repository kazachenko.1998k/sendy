package com.social.solutions.android.transition_media_pager.ui.viewing

import android.view.View
import androidx.appcompat.app.AppCompatActivity
import com.social.solutions.android.transition_media_pager.manager.MediaPagerConfigs
import com.social.solutions.android.transition_media_pager.ui.common.initHeader
import com.social.solutions.android.transition_media_pager.ui.common.initMedia
import com.social.solutions.android.transition_media_pager.ui.common.setPageSelectedCallback
import com.social.solutions.android.util_media_pager.R
import com.social.solutions.android.util_media_pager.adapter.MediaPagerAdapter
import com.social.solutions.android.util_media_pager.models.MediaType
import com.social.solutions.android.util_media_pager.utils.makeShowAnimToTop
import kotlinx.android.synthetic.main.activity_multi_pick_media_pager.*
import kotlinx.android.synthetic.main.dialog_media_more_mp.*
import kotlinx.android.synthetic.main.item_media_header_mp.*
import java.text.SimpleDateFormat

internal fun AppCompatActivity.initAsViewing(configs: MediaPagerConfigs): MediaPagerAdapter {
    setContentView(R.layout.activity_viewing_media_pager)

    imageBack.setOnClickListener {
        onBackPressed()
    }

    initHeader(configs.userName, SimpleDateFormat("dd.MM.yyyy HH:mm").format(configs.date.time))

    pagerMedia.setPageSelectedCallback { position ->
        if (configs.items[position].mediaType == MediaType.VIDEO)
            imageMore.visibility = View.GONE
        else
            imageMore.visibility = View.VISIBLE
    }

    imageMore.setOnClickListener {
        cardMore.makeShowAnimToTop()
    }

    return pagerMedia.initMedia(supportFragmentManager, configs.items)
}
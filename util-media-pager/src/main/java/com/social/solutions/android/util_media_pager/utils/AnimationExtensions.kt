package com.social.solutions.android.util_media_pager.utils

import android.animation.Animator
import android.animation.AnimatorListenerAdapter
import android.view.View
import android.view.WindowManager
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import android.view.animation.Transformation
import androidx.core.view.isVisible
import com.social.solutions.android.util_media_pager.R
import kotlin.math.roundToInt

fun View.makeShowAnimToTop() {
    if (isVisible) return
    val slideDown: Animation = AnimationUtils.loadAnimation(context, R.anim.slide_down_mp)
    slideDown.setAnimationListener(object : Animation.AnimationListener {
        override fun onAnimationRepeat(p0: Animation?) {}
        override fun onAnimationEnd(p0: Animation?) {}
        override fun onAnimationStart(p0: Animation?) {
            this@makeShowAnimToTop.visibility = View.VISIBLE
        }
    })
    startAnimation(slideDown)
}

fun View.makeHideAnimToTop() {
    if (isVisible.not()) return
    val slideUp: Animation = AnimationUtils.loadAnimation(context, R.anim.slide_up_mp)
    slideUp.setAnimationListener(object : Animation.AnimationListener {
        override fun onAnimationRepeat(p0: Animation?) {}
        override fun onAnimationEnd(p0: Animation?) {
            this@makeHideAnimToTop.visibility = View.GONE
        }
        override fun onAnimationStart(p0: Animation?) {}
    })
    startAnimation(slideUp)
}

fun View.makeShowAnimToBottom() {
    if (isVisible) return
    this@makeShowAnimToBottom.animate()
        .translationY(0.0f)
        .setDuration(300)
        .setListener(object : AnimatorListenerAdapter() {
            override fun onAnimationStart(animation: Animator?) {
                super.onAnimationStart(animation)
                this@makeShowAnimToBottom.visibility = View.VISIBLE
            }
        })
}

fun View.makeHideAnimToBottom() {
    if (isVisible.not()) return
    this@makeHideAnimToBottom.animate()
        .translationY(this@makeHideAnimToBottom.height.toFloat())
        .setDuration(300)
        .setListener(object : AnimatorListenerAdapter() {
            override fun onAnimationEnd(animation: Animator?) {
                super.onAnimationEnd(animation)
                this@makeHideAnimToBottom.visibility = View.GONE
            }
        })
}

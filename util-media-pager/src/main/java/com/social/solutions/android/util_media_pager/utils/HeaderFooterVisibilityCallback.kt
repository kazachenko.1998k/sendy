package com.social.solutions.android.util_media_pager.utils

import java.io.Serializable

interface HeaderFooterVisibilityCallback: Serializable {
    fun onShowHeader()
    fun onHideHeader()
    fun onShowFooter()
    fun onHideFooter(isSmooth: Boolean)

    fun hasFooter(): Boolean
    fun useFooter(): Boolean = false

    fun isVisible(): Boolean
}
package com.social.solution.feature_create_community_api

import androidx.fragment.app.FragmentManager
import androidx.lifecycle.MutableLiveData
import com.example.util_cache.chat.ChatDao
import com.morozov.core_backend_api.FeatureBackendApi
import com.morozov.core_backend_api.chat.Chat
import com.morozov.core_backend_api.chat.ChatApi
import com.morozov.core_backend_api.common.CommonApi
import com.social.solution.feature_create_community_api.models.CreateCommunityAbstractItemRecycler

interface FeatureCreateGroupStarter {

    fun startCreateGroup(
        manager: FragmentManager,
        parentContainer: Int,
        addToBackStack: Boolean,
        backendApi: FeatureBackendApi,
        callback: FeatureCreateGroupCallback,
        chatDao: ChatDao,
        uid: Long,
        networkState: MutableLiveData<Boolean>
    )
    fun startCreateChannel(
        manager: FragmentManager,
        parentContainer: Int,
        addToBackStack: Boolean,
        backendApi: FeatureBackendApi,
        callback: FeatureCreateChannelCallback,
        chatDao: ChatDao,
        uid: Long,
        networkState: MutableLiveData<Boolean>
    )
    fun startSettingGroup(
        manager: FragmentManager,
        parentContainer: Int,
        addToBackStack: Boolean,
        chat: Chat,
        backendApi: FeatureBackendApi,
        backendCommonApi: CommonApi,
        callback: FeatureSettingGroupCallback
    )

    fun startSettingChannel(
        manager: FragmentManager,
        parentContainer: Int,
        addToBackStack: Boolean,
        chat: Chat,
        backendApi: FeatureBackendApi,
        backendCommonApi: CommonApi,
        callback: FeatureSettingChannelCallback
    )

    fun selectUsersGroup(selectedUserForCreatingChat: MutableList<CreateCommunityAbstractItemRecycler>)

    fun selectUsersChannel(selectedUserForCreatingChat: MutableList<CreateCommunityAbstractItemRecycler>)
}

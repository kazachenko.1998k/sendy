package com.social.solution.feature_create_community_api

import com.morozov.core_backend_api.chat.Chat

interface FeatureCreateGroupCallback {

    fun addUser()
    fun openChat(chat: Chat)
    fun getPermissionGroupCamera():Boolean
    fun onAvatarChangeClickListener(function: (url: String?) -> Unit)

}
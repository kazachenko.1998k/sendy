package com.social.solution.feature_create_community_api

import com.morozov.core_backend_api.chat.Chat

interface FeatureSettingGroupCallback {

    fun openGroup(chatNew: Chat)
    fun shareLink(text: String)
}
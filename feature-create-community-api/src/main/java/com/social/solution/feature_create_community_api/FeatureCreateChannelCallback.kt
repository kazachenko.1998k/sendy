package com.social.solution.feature_create_community_api

import com.morozov.core_backend_api.chat.Chat

interface FeatureCreateChannelCallback {
    fun addUser()
    fun openChat(chat: Chat)
    fun getPermissionChannelCamera():Boolean
    fun onAvatarChangeClickListener(function: (url: String?) -> Unit)
}
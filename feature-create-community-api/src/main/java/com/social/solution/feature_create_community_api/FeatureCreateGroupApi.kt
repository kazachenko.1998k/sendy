package com.social.solution.feature_create_community_api

interface FeatureCreateGroupApi {
    fun createGroupStarter(): FeatureCreateGroupStarter
}
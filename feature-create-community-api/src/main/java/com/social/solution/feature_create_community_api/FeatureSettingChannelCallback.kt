package com.social.solution.feature_create_community_api

import com.morozov.core_backend_api.chat.Chat

interface FeatureSettingChannelCallback {

    fun openChannel(chat: Chat)
    fun shareLink(text: String)

}
package com.social.solution.feature_create_community_api.models

data class CreateCommunityLocalUserUI(
    val id: Long, val firstName: String?, val lastName: String?, val login: String,
    val dateLastVisit: Long,
    val avatar: String?, var isSubscribe: Boolean
) : CreateCommunityAbstractItemRecycler()
package com.social.solution.feature_feed_impl.ui.view_models

import android.annotation.SuppressLint
import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.morozov.core_backend_api.AnyInnerRequest
import com.morozov.core_backend_api.AnyRequest
import com.morozov.core_backend_api.feed.FeedApi
import com.morozov.core_backend_api.feed.requests.FeedGetFeedRequest
import com.morozov.core_backend_api.message.Message
import com.morozov.core_backend_api.message.MessageApi
import com.morozov.core_backend_api.message.requests.MessageDeleteRequest
import com.morozov.core_backend_api.message.requests.MessageLikeRequest
import com.morozov.core_backend_api.message.responses.MessageLikeResponse
import com.social.solution.feature_feed_api.models.FeedItemPostUI
import com.social.solution.feature_feed_impl.MainObject
import com.social.solution.feature_feed_impl.MainObject.TAG
import com.social.solution.feature_feed_impl.utils.convertFrom
import io.reactivex.Single

class FeedBestViewModel : ViewModel() {


    private val messagesBest = MutableLiveData<MutableSet<Message>>()
    private val lastBestPost = MutableLiveData<Message>()
    private val isSeeCache = MutableLiveData<Boolean>()

    @SuppressLint("CheckResult")
    fun loadBest(feedApi: FeedApi?, lastId: Long?): Single<MutableList<FeedItemPostUI>?>? {
        val lid = if (lastId == null) null else lastBestPost.value?.id
        return Single.create { single ->
            val feedMessagesRequest = FeedGetFeedRequest(
                false,
                lid,
                20,
                null,
                null,
                full_message = true,
                add_message_owner = true,
                time_period = null,
                hashtags = null,
                min_params = null,
                max_params = null,
                sort_default = 0,
                sort_direction = false,
                sort_custom_feed = ""
            )
            val anyInnerRequest = AnyInnerRequest(feedMessagesRequest)
            val anyRequest = AnyRequest(anyInnerRequest)
            feedApi?.getFeedMessages(anyRequest) { resp ->
                if (resp.result) {
                    if (resp.data != null) {
                        if (!resp.data!!.full_message.isNullOrEmpty()) {
                            if (messagesBest.value.isNullOrEmpty()) {
                                messagesBest.value = resp.data!!.full_message.toMutableSet()
                            } else {
                                messagesBest.value!!.addAll(resp.data!!.full_message.toMutableSet())
                            }
                            lastBestPost.value = resp.data!!.full_message.last()
                            val result =
                                resp.data!!.full_message.map { convertFrom(it) }.toMutableList()
                            single.onSuccess(result)
                        } else {
                            single.onSuccess(mutableListOf())
                        }
                    } else {
                        single.onError(Exception("DATA IS NULL"))
                    }
                } else {
                    single.onError(Exception(resp.error?.msg))
                }
            }
        }
    }

    @SuppressLint("CheckResult")
    fun like(messageApi: MessageApi, post: FeedItemPostUI): Single<MessageLikeResponse.Data?>? {
        return Single.create { single ->
            if (post.id != null) {
                val message = getMessageById(post.id)
                if (message != null) {
                    val feedMessagesRequest = MessageLikeRequest(message.chatId, message.id!!)
                    val anyInnerRequest = AnyInnerRequest(feedMessagesRequest)
                    val anyRequest = AnyRequest(anyInnerRequest)
                    messageApi.like(anyRequest) { resp ->
                        if (resp.result) {
                            if (resp.data != null) {
                                single.onSuccess(resp.data!!)
                                refreshLike(
                                    post.id!!,
                                    resp.data!!.likes!!,
                                    resp.data!!.user_like!!
                                )
                            } else {
                                single.onError(Exception("DATA IS NULL"))
                            }
                        } else {
                            single.onError(Exception(resp.error?.msg))
                        }
                    }
                }
            }
        }
    }

    fun getMessageById(id: Long?): Message? {
        if (id == null) return null
        return if (messagesBest.value != null) {
            messagesBest.value!!.find { it.id == id }
        } else {
            null
        }
    }

    fun getMessages(): MutableSet<Message>? {
        return messagesBest.value
    }

    fun refreshComment(id: Long, countComment: Long) {
        messagesBest.value?.find { it.id == id }?.repliesAmount = countComment
    }

    fun refreshLike(id: Long, likes: Int, userLike: Boolean) {
        val message = messagesBest.value?.find { it.id == id }
        message?.likes = likes
        message?.userLike = userLike
    }

    fun clearLastMessageId() {
        lastBestPost.value = null
    }

    fun removePost(postUI: FeedItemPostUI): Single<Any> {
        return Single.create { single ->
            if (postUI.id != null) {
                val message = getMessageById(postUI.id)
                if (message != null) {
                    val feedMessagesRequest =
                        MessageDeleteRequest(message.chatId, mutableListOf(message.id), true)
                    val anyInnerRequest = AnyInnerRequest(feedMessagesRequest)
                    val anyRequest = AnyRequest(anyInnerRequest)
                    MainObject.mBackendApi.messageApi().delete(anyRequest) {
                        if (it.result) {
                            removeByID(postUI.id!!)
                            single.onSuccess(it)
                        } else {
                            Log.e(TAG, "Error message: " + it.error?.msg)
                            single.onError(Exception("Result is false"))
                        }
                    }
                }
            }
        }
    }

    fun removeByID(id: Long) {
        messagesBest.value = messagesBest.value?.filter { it.id != id }?.toMutableSet()
    }

    fun addNewPost(message: Message) {
        if (messagesBest.value != null) {
            val newSet = mutableSetOf(message)
            newSet.addAll(messagesBest.value!!)
            messagesBest.value = newSet
        }
        else
            messagesBest.value = mutableSetOf(message)
    }

    fun clearMessages() {
        messagesBest.value = null
    }
}

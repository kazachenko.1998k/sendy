package com.social.solution.feature_feed_impl.ui.fragments

import android.annotation.SuppressLint
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.os.Bundle
import android.util.Log
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.RecyclerView
import com.morozov.core_backend_api.message.responses.MessageLikeResponse
import com.morozov.core_backend_api.user.UserMini
import com.social.solution.feature_feed_api.FeatureFeedCallback
import com.social.solution.feature_feed_api.models.FeedItem
import com.social.solution.feature_feed_api.models.FeedItemPlaceholder
import com.social.solution.feature_feed_api.models.FeedItemPostUI
import com.social.solution.feature_feed_impl.MainObject
import com.social.solution.feature_feed_impl.MainObject.TAG
import com.social.solution.feature_feed_impl.MainObject.mNetworkState
import com.social.solution.feature_feed_impl.R
import com.social.solution.feature_feed_impl.ui.adapters.FeedAdapter
import com.social.solution.feature_feed_impl.ui.view_models.FeedSubscribersViewModel
import com.social.solution.feature_feed_impl.utils.convertFrom
import com.social.solution.feature_feed_impl.utils.dpToPx
import com.social.solutions.android.transition_media_pager.manager.MediaPagerManager
import com.social.solutions.android.util_media_pager.models.FileModel
import com.social.solutions.android.util_media_pager.models.MediaModel
import com.social.solutions.android.util_media_pager.models.MediaType
import io.reactivex.Single
import kotlinx.android.synthetic.main.feed_layout.*
import java.util.*

open class FeedSubscribersFragment : Fragment() {

    private var loading = false
    private var isRegistered = false
    private lateinit var feedAdapter: FeedAdapter
    lateinit var mFeedSubscribersViewModel: FeedSubscribersViewModel
    private lateinit var callback: FeatureFeedCallback
    private val mReceiver = NotifyBroadcastReceiver()

    companion object {
        const val NAME = "FEED_SUBSCRIBERS"
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        Log.d("FEED_MODULE", "Subscription Feed onCreateView")
        setHasOptionsMenu(true)
        mFeedSubscribersViewModel =
            ViewModelProviders.of(activity!!).get(FeedSubscribersViewModel::class.java)
        return inflater.inflate(R.layout.feed_layout, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        Log.d("FEED_MODULE", "Subscription Feed onViewCreated")
        initSwipeRefresh()
        initFeedAdapter(feed_recycler_view_post)
        loadNewPost(true)
        val filter = IntentFilter("REMOVE")
        filter.addAction("LIKE")
        filter.addAction("COMMENT")
        filter.addAction("CREATE_POST")
        try {
            if (!isRegistered){
                context!!.registerReceiver(mReceiver, filter)
                isRegistered = true
            }
        } catch (e: Exception) {
        }
    }

    private fun initSwipeRefresh() {
        swipe_container?.setOnRefreshListener {
            if (mNetworkState.value != null && !mNetworkState.value!!) {
                showErrorInternetConnection()
                return@setOnRefreshListener
            }
            mFeedSubscribersViewModel.loadSubscribers(MainObject.mBackendApi.feedApi(), null)
                ?.subscribe({ refreshPosts ->
                    refreshNewData(refreshPosts)
                    swipe_container?.isRefreshing = false
                }, { throwable ->
                    Log.e(TAG, throwable.printStackTrace().toString())
                    swipe_container?.isRefreshing = false
                })

        }
    }

    private fun showErrorInternetConnection() {
        swipe_container?.isRefreshing = false
        val toast = Toast.makeText(
            context,
            "Нет соединения. Проверьте подключение к интернету",
            Toast.LENGTH_SHORT
        )
        toast.setGravity(
            Gravity.BOTTOM,
            0,
            dpToPx(80f, resources)
        )
        toast.show()
    }

    private fun refreshNewData(refreshPosts: MutableList<FeedItemPostUI>?) {
        feedAdapter.clearData()
        if (refreshPosts.isNullOrEmpty() && mFeedSubscribersViewModel.getMessages().isNullOrEmpty()) {
            feedAdapter.addData(mutableListOf(FeedItemPlaceholder()))
        } else {
            feedAdapter.addData(refreshPosts as MutableList<FeedItem>)
        }
    }

    private fun initFeedAdapter(recycler: RecyclerView?) {

        val initList = mutableListOf<FeedItem>()
        feedAdapter = FeedAdapter(
            context!!,
            initList,
            FeedAdapterCallback()
        )
        recycler?.adapter = feedAdapter
    }

    private fun initListenerConnection() {
        mNetworkState.observe(viewLifecycleOwner, Observer {
            if (it) {
                feedAdapter.hideProgress()
            }
        })
    }

    @SuppressLint("CheckResult")
    private fun loadNewPost(viewCache: Boolean) {
        val messages = mFeedSubscribersViewModel.getMessages()
        if (mFeedSubscribersViewModel.getViewCacheStrategy() && viewCache && messages != null) {
            Log.d("FEED_MODULE", "Subscription strategy1")
            if (messages.isNullOrEmpty()) {
                feedAdapter.addData(mutableListOf(FeedItemPlaceholder()))
            } else {
                feedAdapter.addData(messages.map { convertFrom(it) }.toMutableList() as MutableList<FeedItem>)
            }
            initListenerConnection()
        } else {
            Log.d("FEED_MODULE", "Subscription strategy2")
            if (!loading) {
                loading = true
                feedAdapter.showProgress()
                mFeedSubscribersViewModel.loadSubscribers(MainObject.mBackendApi.feedApi(), 0)
                    ?.subscribe({ newPost ->
                        Log.d(TAG, "NEW POSTS DOUBLE = " + newPost)
                        loading = false
                        feedAdapter.hideProgress()
                        initListenerConnection()
                        if (newPost.isNullOrEmpty() && messages.isNullOrEmpty()) {
                            feedAdapter.addData(mutableListOf(FeedItemPlaceholder()))
                        } else {
                            feedAdapter.addData(newPost as MutableList<FeedItem>)
                        }
                    }, { throwable ->
                        Log.e(TAG, throwable.printStackTrace().toString())
                    })
            }
        }
    }

    fun refreshData() {
        mFeedSubscribersViewModel.clearLastMessageId()
        mFeedSubscribersViewModel.clearMessages()
        feedAdapter.clearData()
    }

    fun removePost(postUI: FeedItemPostUI) {
        mFeedSubscribersViewModel.removeByID(postUI.id!!)
        feedAdapter.removeNotifyByID(postUI)
    }

    fun setCallback(callback: FeatureFeedCallback) {
        this.callback = callback
    }

    inner class FeedAdapterCallback : FeedAdapter.FeedCallbackInterface {
        override fun showImage(url: String, name: String, date: Calendar) {
            val tmpList = mutableListOf<MediaModel>()
            tmpList.add(
                MediaModel(
                    MediaType.PHOTO,
                    FileModel(url, null)
                )
            )
            MediaPagerManager.with(context!!).setUserName(name).setDate(date).setItems(tmpList)
                .start()
        }

        override fun showVideo(url: String, position: Long, name: String, date: Calendar) {
            val tmpList = mutableListOf<MediaModel>()
            tmpList.add(
                MediaModel(
                    MediaType.VIDEO,
                    FileModel(url, null)
                )
            )
            MediaPagerManager.with(context!!).setUserName(name).setDate(date).setItems(tmpList)
                .setStartPosition(position.toInt()).start()
        }

        override fun createPost() {
            callback.createPost {
                mFeedSubscribersViewModel.setViewCacheStrategy(!it)
            }
        }

        override fun openPost(post: FeedItemPostUI) {
            val message = mFeedSubscribersViewModel.getMessageById(post.id)
            if (message != null) {
                callback.openPost(message)
            }
        }

        override fun openComment(post: FeedItemPostUI) {
            val message = mFeedSubscribersViewModel.getMessageById(post.id)
            if (message != null) {
                callback.openPost(message)
            }
        }

        override fun openProfile(user: UserMini) {
            callback.openProfile(user)
        }

        override fun likePost(post: FeedItemPostUI): Single<MessageLikeResponse.Data?>? {
            return mFeedSubscribersViewModel.like(MainObject.mBackendApi.messageApi(), post)
        }

        override fun loadMore() {
            feed_recycler_view_post?.post {
                loadNewPost(false)
            }
        }

        override fun removePost(postUI: FeedItemPostUI): Single<Any> {
            return mFeedSubscribersViewModel.removePost(postUI)
        }

        override fun removeNotifyCallback(postUI: FeedItemPostUI) {

        }
    }

    inner class NotifyBroadcastReceiver : BroadcastReceiver() {

        override fun onReceive(context: Context, intent: Intent) {
            val id = intent.extras!!.getLong("id")
            Log.d("FEED_MODULE", "RECEIVE data = " + id)
            when (intent.action) {
                "REMOVE" -> {
                    mFeedSubscribersViewModel.removeByID(id)
                    feedAdapter.removeById(id)
                }
                "LIKE" -> {
                    val like = intent.extras!!.getBoolean("like")
                    val countLike = intent.extras!!.getInt("countLike")
                    val countComment = intent.extras!!.getLong("countComment")
                    feedAdapter.likeById(id, like, countLike, countComment)
                    mFeedSubscribersViewModel.refreshLike(id, countLike, like)
                }
                "COMMENT" -> {
                    val countComment = intent.extras!!.getLong("countComment")
                    feedAdapter.refreshComment(id, countComment)
                    mFeedSubscribersViewModel.refreshComment(id, countComment)
                }
                "CREATE_POST" -> {
//                    val message = MessageConverter().toMessage(intent.extras!!.getString("message"))
//                    feedAdapter.addNewPost(0,convertFrom(message!!))
//                    mFeedSubscribersViewModel.addNewPost(message)
                    Log.d(TAG, "NEW POSTS DOUBLE")
                    refreshData()
                    loadNewPost(false)
                }
            }
            Log.d("FEED_MODULE", "RECEIVE = " + intent.action)
        }
    }
}
package com.social.solution.feature_feed_impl.ui.view_models

import android.annotation.SuppressLint
import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.morozov.core_backend_api.AnyInnerRequest
import com.morozov.core_backend_api.AnyRequest
import com.morozov.core_backend_api.chat.ChatApi
import com.morozov.core_backend_api.chat.requests.ChatGetMessagesRequest
import com.morozov.core_backend_api.message.Message
import com.morozov.core_backend_api.message.MessageApi
import com.morozov.core_backend_api.message.requests.MessageDeleteRequest
import com.morozov.core_backend_api.message.requests.MessageLikeRequest
import com.morozov.core_backend_api.message.responses.MessageLikeResponse
import com.social.solution.feature_feed_api.models.FeedConfig
import com.social.solution.feature_feed_api.models.FeedItemPostUI
import com.social.solution.feature_feed_impl.MainObject
import com.social.solution.feature_feed_impl.MainObject.TAG
import com.social.solution.feature_feed_impl.utils.convertFrom
import io.reactivex.Single

class FeedUserViewModel : ViewModel() {

    private val messagesMyUser = MutableLiveData<MutableSet<Message>>()
    private val messagesOtherUser = MutableLiveData<MutableSet<Message>>()
    private val lastMyUserPost = MutableLiveData<Message>()
    private val lastOtherUserPost = MutableLiveData<Message>()
    private lateinit var config: FeedConfig

    fun loadUser(userChatId: Long, chatApi: ChatApi?): Single<MutableList<FeedItemPostUI>?>? {
        if (config.isMyProfile && messagesMyUser.value == null){
            messagesMyUser.value = mutableSetOf()
        }
        if (!config.isMyProfile && messagesOtherUser.value == null){
            messagesOtherUser.value = mutableSetOf()
        }
        return Single.create { single ->
            val lastPost = if (config.isMyProfile) lastMyUserPost else lastOtherUserPost
            val messages = if (config.isMyProfile) messagesMyUser else messagesOtherUser
            val lastId = if (lastPost.value?.id != null) lastPost.value?.id!! else 0
            val feedMessagesRequest = ChatGetMessagesRequest(
                userChatId,
                lastId,
                0,
                null,
                null,
                full_message = true,
                add_message_owner = true
            )
            val anyInnerRequest = AnyInnerRequest(feedMessagesRequest)
            val anyRequest = AnyRequest(anyInnerRequest)
            chatApi?.getMessages(anyRequest) { resp ->
                if (resp.result) {
                    if (resp.data != null) {
                        if (!resp.data?.full_message.isNullOrEmpty()) {
                            if (messages.value.isNullOrEmpty()) {
                                messages.value = resp.data!!.full_message.toMutableSet()
                                Log.d(TAG, "MESSAGES1 =  = " + messagesMyUser.value)

                            } else {
                                messages.value!!.addAll(resp.data!!.full_message.toMutableSet())
                                Log.d(TAG, "MESSAGES2 =  = " + messagesMyUser.value)
                            }
                            lastPost.value = resp.data!!.full_message.last()
                            single.onSuccess(resp.data!!.full_message.map { convertFrom(it) }.toMutableList())
                        } else {
                            single.onSuccess(mutableListOf())
                        }
                    } else {
                        single.onError(Exception("DATA IS NULL"))
                    }
                } else {
                    single.onError(Exception(resp.error?.msg))
                }
            }
        }
    }

    @SuppressLint("CheckResult")
    fun like(messageApi: MessageApi, post: FeedItemPostUI): Single<MessageLikeResponse.Data?>? {
        return Single.create { single ->
            if (post.id != null) {
                val message = getMessageById(post.id)
                if (message != null) {
                    val feedMessagesRequest = MessageLikeRequest(message.chatId, message.id!!)
                    val anyInnerRequest = AnyInnerRequest(feedMessagesRequest)
                    val anyRequest = AnyRequest(anyInnerRequest)
                    messageApi.like(anyRequest) { resp ->
                        if (resp.result) {
                            if (resp.data != null) {
                                single.onSuccess(resp.data!!)
                                refreshLike(
                                    post.id!!,
                                    resp.data!!.likes!!,
                                    resp.data!!.user_like!!
                                )
                            } else {
                                single.onError(Exception("DATA IS NULL"))
                            }
                        } else {
                            single.onError(Exception(resp.error?.msg))
                        }
                    }
                }
            }
        }
    }

    fun getMessageById(id: Long?): Message? {
        val messages = if (config.isMyProfile) messagesMyUser else messagesOtherUser

        if (id == null) return null
        return if (messages.value != null) {
            messages.value!!.find { it.id == id }
        } else {
            null
        }
    }

    fun getMessages(): MutableSet<Message>? {
        val messages = if (config.isMyProfile) messagesMyUser else messagesOtherUser
        return messages.value
    }

    fun refreshComment(id: Long, countComment: Long) {
        val messages = if (config.isMyProfile) messagesMyUser else messagesOtherUser
        messages.value?.find { it.id == id }?.repliesAmount = countComment
    }

    fun refreshLike(id: Long, likes: Int, userLike: Boolean) {
        val messages = if (config.isMyProfile) messagesMyUser else messagesOtherUser
        val message = messages.value?.find { it.id == id }
        message?.likes = likes
        message?.userLike = userLike
    }

    fun clearLastMessageId() {
        val lastPost = if (config.isMyProfile) lastMyUserPost else lastOtherUserPost
        lastPost.value = null
    }

    fun removePost(postUI: FeedItemPostUI): Single<Any> {
        return Single.create { single ->
            if (postUI.id != null) {
                val message = getMessageById(postUI.id)
                if (message != null) {
                    val feedMessagesRequest =
                        MessageDeleteRequest(message.chatId, mutableListOf(message.id), true)
                    val anyInnerRequest = AnyInnerRequest(feedMessagesRequest)
                    val anyRequest = AnyRequest(anyInnerRequest)
                    MainObject.mBackendApi.messageApi().delete(anyRequest) {
                        if (it.result) {
                            removeByID(postUI.id!!)
                            single.onSuccess(it)
                        } else {
                            Log.e(MainObject.TAG, "Error message: " + it.error?.msg)
                            single.onError(Exception("Result is false"))
                        }
                    }
                }
            }
        }
    }

    fun removeByID(id: Long) {
        messagesMyUser.value = messagesMyUser.value?.filter { it.id != id }?.toMutableSet()
    }

    fun clearData() {
        lastOtherUserPost.value = null
        messagesOtherUser.value = null
    }

    fun setConfig(config: FeedConfig) {
        this.config = config
    }


    fun addNewPost(message: Message?) {
        if (messagesMyUser.value != null) {
            val newSet = mutableSetOf(message!!)
            newSet.addAll(messagesMyUser.value!!)
            messagesMyUser.value = newSet
        }
        else
            messagesMyUser.value = mutableSetOf(message!!)
    }
}

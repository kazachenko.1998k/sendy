package com.social.solution.feature_feed_impl

import android.content.Context
import com.morozov.core_backend_api.FeatureBackendApi
import com.social.solution.feature_feed_api.FeatureFeedApi
import com.social.solution.feature_feed_api.FeatureFeedStarter

class FeedFeatureImpl(
    private val starter: FeatureFeedStarter,
    private val context: Context,
    private val api: FeatureBackendApi
) : FeatureFeedApi {

    override fun feedStarter(): FeatureFeedStarter = starter

}
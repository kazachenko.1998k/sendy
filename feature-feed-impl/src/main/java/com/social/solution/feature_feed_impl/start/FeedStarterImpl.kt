package com.social.solution.feature_feed_impl.start

import android.util.Log
import androidx.fragment.app.FragmentManager
import androidx.lifecycle.LiveData
import com.example.util_cache.AppDatabase
import com.morozov.core_backend_api.FeatureBackendApi
import com.social.solution.feature_feed_api.FeatureFeedCallback
import com.social.solution.feature_feed_api.FeatureFeedStarter
import com.social.solution.feature_feed_api.models.FeedConfig
import com.social.solution.feature_feed_api.models.FeedItemPostUI
import com.social.solution.feature_feed_impl.MainObject
import com.social.solution.feature_feed_impl.MainObject.TAG
import com.social.solution.feature_feed_impl.ui.fragments.CommonFragment
import com.social.solution.feature_feed_impl.ui.fragments.FeedBestFragment
import com.social.solution.feature_feed_impl.ui.fragments.FeedSubscribersFragment
import com.social.solution.feature_feed_impl.ui.fragments.FeedUserFragment

class FeedStarterImpl : FeatureFeedStarter {

    private var bestFeedFragment: FeedBestFragment = FeedBestFragment()
    private var userFeedFragment: FeedUserFragment = FeedUserFragment()
    private var subscriptionFeedFragment: FeedSubscribersFragment = FeedSubscribersFragment()
    private var commonFeedFragment: CommonFragment =
        CommonFragment(bestFeedFragment, subscriptionFeedFragment)

    override fun startCommonFeed(
        manager: FragmentManager,
        container: Int,
        addToBackStack: Boolean,
        networkState: LiveData<Boolean>,
        backendApi: FeatureBackendApi,
        localDB: AppDatabase,
        callback: FeatureFeedCallback
    ) {
        commonFeedFragment.setCallback(callback)
        bestFeedFragment.setCallback(callback)
        subscriptionFeedFragment.setCallback(callback)
        MainObject.mBackendApi = backendApi
        MainObject.mLocalDB = localDB
        MainObject.mNetworkState = networkState
        Log.d(TAG, "COMMON REPLACE")
        manager.beginTransaction()
            .replace(container, commonFeedFragment)
            .commit()
    }

    override fun startUserFeed(
        networkState: LiveData<Boolean>,
        backendApi: FeatureBackendApi,
        localDB: AppDatabase,
        callback: FeatureFeedCallback,
        config: FeedConfig
    ): FeedUserFragment {
        Log.d("FEED_MODULE", "Profile start feed")
        Log.d(MainObject.TAG, "User Config0 = " + config.isMyProfile)
        userFeedFragment.setCallback(callback)
        userFeedFragment.setConfig(config)
        MainObject.mBackendApi = backendApi
        MainObject.mLocalDB = localDB
        MainObject.mNetworkState = networkState
        return userFeedFragment
    }

    override fun notifyRemove(postUI: FeedItemPostUI) {
        bestFeedFragment.removePost(postUI)
        subscriptionFeedFragment.removePost(postUI)
        userFeedFragment.removePost(postUI)
    }


}
package com.social.solution.feature_feed_impl.utils

import android.app.Activity
import android.content.Context
import android.content.res.Resources
import android.util.DisplayMetrics
import android.util.TypedValue
import android.view.WindowManager
import android.view.inputmethod.InputMethodManager
import kotlin.math.roundToInt

fun hideKeyboard(context: Context?) {
    try {
        (context as Activity).window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN)
        if (context.currentFocus != null && context.currentFocus!!.windowToken != null) {
            (context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager).hideSoftInputFromWindow(
                context.currentFocus!!.windowToken,
                0
            )
        }
    } catch (e: Exception) {
        e.printStackTrace()
    }
}

fun pxToDp(px: Int, context: Context): Int {
    val displayMetrics: DisplayMetrics = context.resources.displayMetrics
    return (px / (displayMetrics.xdpi / DisplayMetrics.DENSITY_DEFAULT)).roundToInt()
}

fun dpToPx(dip: Float, resources: Resources): Int {
    return TypedValue.applyDimension(
        TypedValue.COMPLEX_UNIT_DIP,
        dip,
        resources.displayMetrics
    ).toInt()
}

fun durationToString(value: Long): String {
    if (value < 0) return "00:00"
    val minutes = value / 60
    var minutesString = minutes.toString()
    if (minutes < 10) minutesString = "0$minutesString"
    val seconds = value % 60
    var secondsString = seconds.toString()
    if (minutes < 10) secondsString = "0$secondsString"
    return "$minutesString:$secondsString"
}

fun sizeFile(size: Int?): String {
    return when (size) {
        null -> "0 КБ"
        in 0..100 -> "0 КБ"
        in 100..1000000 -> "${size / 1000}.${size % 1000} КБ"
        in 1000000..1000000000 -> "${size / 1000000}.${size / 1000 % 1000} МБ"
        else -> "999.999 МБ"
    }
}
package com.social.solution.feature_feed_impl.ui.view_models

import android.annotation.SuppressLint
import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.morozov.core_backend_api.AnyInnerRequest
import com.morozov.core_backend_api.AnyRequest
import com.morozov.core_backend_api.feed.FeedApi
import com.morozov.core_backend_api.feed.requests.FeedGetFeedRequest
import com.morozov.core_backend_api.message.Message
import com.morozov.core_backend_api.message.MessageApi
import com.morozov.core_backend_api.message.requests.MessageDeleteRequest
import com.morozov.core_backend_api.message.requests.MessageLikeRequest
import com.morozov.core_backend_api.message.responses.MessageLikeResponse
import com.social.solution.feature_feed_api.models.FeedItemPostUI
import com.social.solution.feature_feed_impl.MainObject
import com.social.solution.feature_feed_impl.MainObject.TAG
import com.social.solution.feature_feed_impl.utils.convertFrom
import io.reactivex.Single

class FeedSubscribersViewModel : ViewModel() {

    private val messagesSubscribers = MutableLiveData<MutableSet<Message>>()
    private val lastSubscriberPost = MutableLiveData<Message>()
    private val isSeeCache = MutableLiveData<Boolean>()

    init {
        Log.d("TAG_CHECK", "Feed Init VM")
    }

    override fun onCleared() {
        super.onCleared()
        Log.d("TAG_CHECK", "Feed Cleared VM")
    }

    @SuppressLint("CheckResult")
    fun loadSubscribers(feedApi: FeedApi?, lastId: Long?): Single<MutableList<FeedItemPostUI>?>? {
        val lid = if (lastId == null) null else lastSubscriberPost.value?.id
        Log.d(TAG, "LAST_POST_ID_SUBSCRIBERS_FEED = $lid")
        return Single.create { single ->
            val feedMessagesRequest = FeedGetFeedRequest(
                true,
                lid,
                20,
                null,
                null,
                true,
                true,
                null,
                null,
                null,
                null,
                0,
                false,
                ""
            )
            val anyInnerRequest = AnyInnerRequest(feedMessagesRequest)
            val anyRequest = AnyRequest(anyInnerRequest)
            feedApi?.getFeedMessages(anyRequest) { resp ->
                val i = resp.data!!.full_message.find { it.text == "еще проверка" }
                Log.d(TAG, "ABC = " + i)
                if (resp.result) {
                    if (resp.data != null) {
                        if (!resp.data!!.full_message.isNullOrEmpty()) {
                            if (messagesSubscribers.value.isNullOrEmpty()) {
                                messagesSubscribers.value = resp.data!!.full_message.toMutableSet()
                            } else {
                                messagesSubscribers.value!!.addAll(resp.data!!.full_message.toMutableSet())
                            }
                            lastSubscriberPost.value = resp.data!!.full_message.last()
                            val result =
                                resp.data!!.full_message.map { convertFrom(it) }.toMutableList()
                            single.onSuccess(result)
                        } else {
                            single.onSuccess(mutableListOf())
                        }
                    } else {
                        single.onError(Exception("DATA IS NULL"))
                    }
                } else {
                    single.onError(Exception(resp.error?.msg))
                }
            }
        }
    }

    @SuppressLint("CheckResult")
    fun like(messageApi: MessageApi, post: FeedItemPostUI): Single<MessageLikeResponse.Data?>? {
        return Single.create { single ->
            if (post.id != null) {
                val message = getMessageById(post.id)
                if (message != null) {
                    val feedMessagesRequest = MessageLikeRequest(message.chatId, message.id!!)
                    val anyInnerRequest = AnyInnerRequest(feedMessagesRequest)
                    val anyRequest = AnyRequest(anyInnerRequest)
                    messageApi.like(anyRequest) { resp ->
                        if (resp.result) {
                            if (resp.data != null) {
                                single.onSuccess(resp.data!!)
                                refreshLike(
                                    post.id!!,
                                    resp.data!!.likes!!,
                                    resp.data!!.user_like!!
                                )
                            } else {
                                single.onError(Exception("DATA IS NULL"))
                            }
                        } else {
                            single.onError(Exception(resp.error?.msg))
                        }
                    }
                }
            }
        }
    }

    fun getMessageById(id: Long?): Message? {
        if (id == null) return null
        return if (messagesSubscribers.value != null) {
            messagesSubscribers.value!!.find { it.id == id }
        } else {
            null
        }
    }

    fun getMessages(): MutableSet<Message>? {
        return messagesSubscribers.value
    }

    fun refreshComment(id: Long, countComment: Long) {
        messagesSubscribers.value?.find { it.id == id }?.repliesAmount = countComment
    }

    fun refreshLike(id: Long, likes: Int, userLike: Boolean) {
        val message = messagesSubscribers.value?.find { it.id == id }
        message?.likes = likes
        message?.userLike = userLike
    }

    fun clearLastMessageId() {
        lastSubscriberPost.value = null
    }


    fun setViewCacheStrategy(seeCache: Boolean) {
        isSeeCache.value = seeCache
    }

    fun getViewCacheStrategy(): Boolean {
        return if (isSeeCache.value == null) true
        else isSeeCache.value!!
    }

    fun clearMessages() {
        messagesSubscribers.value = null
    }

    fun removePost(postUI: FeedItemPostUI): Single<Any> {
        return Single.create { single ->
            if (postUI.id != null) {
                val message = getMessageById(postUI.id)
                if (message != null) {
                    val feedMessagesRequest =
                        MessageDeleteRequest(message.chatId, mutableListOf(message.id), true)
                    val anyInnerRequest = AnyInnerRequest(feedMessagesRequest)
                    val anyRequest = AnyRequest(anyInnerRequest)
                    MainObject.mBackendApi.messageApi().delete(anyRequest) {
                        if (it.result) {
                            removeByID(postUI.id!!)
                            single.onSuccess(it)
                        } else {
                            Log.e(TAG, "Error message: " + it.error?.msg)
                            single.onError(Exception("Result is false"))
                        }
                    }
                }
            }
        }
    }

    fun removeByID(id: Long) {
        messagesSubscribers.value =
            messagesSubscribers.value?.filter { it.id != id }?.toMutableSet()
    }

    fun addNewPost(message: Message) {
        if (messagesSubscribers.value != null) {
            val newSet = mutableSetOf(message)
            newSet.addAll(messagesSubscribers.value!!)
            messagesSubscribers.value = newSet
        } else
            messagesSubscribers.value = mutableSetOf(message)
    }
}

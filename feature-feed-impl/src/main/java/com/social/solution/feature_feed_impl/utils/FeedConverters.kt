package com.social.solution.feature_feed_impl.utils

import com.morozov.core_backend_api.message.Message
import com.social.solution.feature_feed_api.models.FeedItemPostUI

fun convertFrom(post: Message): FeedItemPostUI {
    return FeedItemPostUI(
        post.id,
        post.uid,
        post.owner,
        post.timeAdd,
        post.files,
        post.text,
        post.userLike,
        post.likes,
        post.repliesAmount
    )
}



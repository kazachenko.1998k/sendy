package com.social.solution.feature_feed_impl.models

import com.social.solutions.android.util_media_pager.select.media.renderers.utils.SerializableViewModel

data class VideoModel(val filePath: String, val seconds: Int): SerializableViewModel
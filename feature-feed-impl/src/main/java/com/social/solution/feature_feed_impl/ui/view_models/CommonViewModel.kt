package com.social.solution.feature_feed_impl.ui.view_models

import android.annotation.SuppressLint
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.morozov.core_backend_api.AnyInnerRequest
import com.morozov.core_backend_api.AnyRequest
import com.morozov.core_backend_api.feed.FeedApi
import com.morozov.core_backend_api.feed.requests.FeedGetFeedRequest
import com.morozov.core_backend_api.message.Message
import com.morozov.core_backend_api.message.MessageApi
import com.morozov.core_backend_api.message.requests.MessageLikeRequest
import com.morozov.core_backend_api.message.responses.MessageLikeResponse
import com.social.solution.feature_feed_api.models.FeedItemPostUI
import com.social.solution.feature_feed_impl.utils.convertFrom
import io.reactivex.Single

class CommonViewModel : ViewModel() {

    private val resultCallback = MutableLiveData<Boolean>()


}

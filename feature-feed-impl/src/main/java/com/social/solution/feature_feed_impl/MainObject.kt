package com.social.solution.feature_feed_impl

import androidx.lifecycle.LiveData
import com.example.util_cache.AppDatabase
import com.morozov.core_backend_api.FeatureBackendApi
import com.social.solution.feature_feed_api.FeatureFeedCallback
import com.social.solution.feature_feed_api.models.FeedConfig


object MainObject {

    lateinit var mNetworkState: LiveData<Boolean>
    lateinit var mLocalDB: AppDatabase
    lateinit var mBackendApi: FeatureBackendApi
    var TAG = "FEED_MODULE"
}
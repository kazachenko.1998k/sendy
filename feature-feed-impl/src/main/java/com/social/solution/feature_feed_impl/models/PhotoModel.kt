package com.social.solution.feature_feed_impl.models

import android.graphics.Bitmap
import com.social.solutions.android.util_media_pager.select.media.renderers.utils.SerializableViewModel

data class PhotoModel(val filePath: String, var bitmap: Bitmap? = null): SerializableViewModel
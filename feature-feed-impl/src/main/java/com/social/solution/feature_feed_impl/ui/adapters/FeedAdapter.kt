package com.social.solution.feature_feed_impl.ui.adapters

import android.animation.ObjectAnimator
import android.annotation.SuppressLint
import android.app.Dialog
import android.content.Context
import android.content.Intent
import android.graphics.drawable.BitmapDrawable
import android.net.Uri
import android.util.Log
import android.view.LayoutInflater.from
import android.view.View
import android.view.ViewGroup
import android.view.Window
import android.widget.PopupWindow
import android.widget.Toast
import androidx.core.content.ContextCompat.startActivity
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.request.RequestOptions
import com.morozov.core_backend_api.SignData
import com.morozov.core_backend_api.SignData.uid
import com.morozov.core_backend_api.file.FileModel
import com.morozov.core_backend_api.message.responses.MessageLikeResponse
import com.morozov.core_backend_api.user.UserMini
import com.social.solution.feature_feed_api.models.*
import com.social.solution.feature_feed_impl.MainObject.TAG
import com.social.solution.feature_feed_impl.R
import com.social.solution.feature_feed_impl.utils.formatMillis
import com.social.solution.feature_feed_impl.utils.getCalendar
import com.social.solution.feature_feed_impl.utils.getStringTimeAgoByLong
import com.social.solution.feature_feed_impl.utils.sizeFile
import com.social.solutions.android.transition_media_pager.manager.MediaPagerManager
import com.social.solutions.android.util_media_pager.models.MediaModel
import com.social.solutions.android.util_media_pager.models.MediaType
import io.reactivex.Single
import kotlinx.android.synthetic.main.feed_card_post.view.*
import kotlinx.android.synthetic.main.feed_dialog_approve_remove.*
import kotlinx.android.synthetic.main.feed_menu_post_me.view.*
import kotlinx.android.synthetic.main.feed_menu_post_other.view.*
import java.util.*


class FeedAdapter(
    private val context: Context,
    private var data: MutableList<FeedItem>,
    private val feedCallbackInterface: FeedCallbackInterface
) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    init {
        Log.d("TAG_CHECK", "Feed init adapter")
    }

    override fun onAttachedToRecyclerView(recyclerView: RecyclerView) {
        super.onAttachedToRecyclerView(recyclerView)
        Log.d("TAG_CHECK", "Feed attach adapter to recycler")
    }

    companion object {
        const val ROUND_CORNER = 12
        const val DURATION_SHOW_CONTENT_MS = 1000
        const val POST_TYPE = 0
        const val CREATE_POST_TYPE = 1
        const val LOAD_MORE_TYPE = 2
        const val PLACEHOLDER_TYPE_PROFILE = 3
        const val PLACEHOLDER_TYPE_USER = 4
        const val PLACEHOLDER_TYPE = 5
        const val CLICK_TIME_INTERVAL = 500
    }

    private var mLastClickTime = System.currentTimeMillis()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return when (viewType) {
            POST_TYPE -> {
                val holder = PostViewHolder(
                    from(parent.context).inflate(
                        R.layout.feed_card_post,
                        parent,
                        false
                    )
                )
                holder
            }
            CREATE_POST_TYPE -> CreatePostViewHolder(
                from(parent.context).inflate(
                    R.layout.feed_card_create_post,
                    parent,
                    false
                )
            )
            PLACEHOLDER_TYPE_PROFILE -> PlaceholderProfileViewHolder(
                from(parent.context).inflate(
                    R.layout.feed_card_placeholder_profile,
                    parent,
                    false
                )
            )
            PLACEHOLDER_TYPE_USER -> PlaceholderUserViewHolder(
                from(parent.context).inflate(
                    R.layout.feed_card_placeholder_user,
                    parent,
                    false
                )
            )
            PLACEHOLDER_TYPE -> PlaceholderFeedViewHolder(
                from(parent.context).inflate(
                    R.layout.feed_card_placeholder,
                    parent,
                    false
                )
            )
            else -> LoadMoreFeedViewHolder(
                from(parent.context).inflate(
                    R.layout.feed_card_recycler_load_data,
                    parent,
                    false
                )
            )
        }
    }

    override fun getItemViewType(position: Int): Int {
        return when (data[position]) {
            is FeedItemPostUI -> POST_TYPE
            is FeedItemCreatePost -> CREATE_POST_TYPE
            is FeedItemPlaceholderUser -> PLACEHOLDER_TYPE_USER
            is FeedItemPlaceholderProfile -> PLACEHOLDER_TYPE_PROFILE
            is FeedItemPostLoadData -> LOAD_MORE_TYPE
            else -> PLACEHOLDER_TYPE
        }
    }

    override fun getItemCount() = data.size


    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        if (position == data.size - 1 && holder !is LoadMoreFeedViewHolder && holder !is PlaceholderFeedViewHolder
            && holder !is PlaceholderProfileViewHolder && holder !is PlaceholderUserViewHolder && holder !is CreatePostViewHolder
        ) feedCallbackInterface.loadMore()
        when (holder) {
            is PostViewHolder -> holder.bind(position)
            is CreatePostViewHolder -> holder.bind(position)
            is PlaceholderFeedViewHolder -> holder.bind(position)
            is PlaceholderProfileViewHolder -> holder.bind(position)
            is PlaceholderUserViewHolder -> holder.bind(position)
            is LoadMoreFeedViewHolder -> holder.bind(position)
        }
    }

    fun clearData() {
        data.clear()
        notifyDataSetChanged()
    }

    fun addData(refreshPosts: MutableList<FeedItem>?) {
        if (refreshPosts != null) {
            val oldSize = data.size
            data.addAll(refreshPosts)
            notifyItemRangeInserted(oldSize + 1, refreshPosts.size)
        }
    }

    fun showProgress() {
        if (data.find { it is FeedItemPostLoadData } == null) {
            data.add(FeedItemPostLoadData())
            notifyItemInserted(data.size - 1)
        }
    }

    fun hideProgress() {
        val removeElement = data.find { it is FeedItemPostLoadData }
        if (removeElement != null) {
            data.remove(removeElement)
            notifyItemRangeRemoved(data.size, 1)
        }
    }

    fun removeNotifyByID(post: FeedItemPostUI) {
        val index = data.indexOf(post)
        data.remove(post)
        notifyItemRemoved(index)
    }

    fun removeById(id: Long) {
        val findItem = data.find { it is FeedItemPostUI && it.id == id }
        if (findItem != null) {
            val index = data.indexOf(findItem)
            data.remove(findItem)
            notifyItemRemoved(index)
        }
    }

    fun likeById(
        id: Long,
        like: Boolean,
        countLike: Int,
        countComment: Long
    ) {
        val findItem = data.find { it is FeedItemPostUI && it.id == id }
        if (findItem != null) {
            val post = (findItem as FeedItemPostUI)
            val index = data.indexOf(findItem)
            post.isLiked = like
            post.countLike = countLike
            post.countComment = countComment
            notifyItemChanged(index)
        }
    }

    fun refreshComment(id: Long, countComment: Long) {
        val findItem = data.find { it is FeedItemPostUI && it.id == id }
        if (findItem != null) {
            val index = data.indexOf(findItem)
            val post = (findItem as FeedItemPostUI)
            post.countComment = countComment
            notifyItemChanged(index)
        }
    }

    fun addNewPost(position: Int, convertFrom: FeedItemPostUI) {
        data.add(position, convertFrom)
    }


    inner class PostViewHolder(view: View) : RecyclerView.ViewHolder(view) {

        private fun refreshLikeState(context: Context, isLike: Boolean?) {
            if (isLike == null) return
            val likeImage = itemView.like_image
            val countLikeText = itemView.count_like
            if (likeImage != null && countLikeText != null)
                if (isLike) {
                    likeImage.setImageResource(R.drawable.feed_ic_like_active)
                    countLikeText.setTextColor(context.resources.getColor(R.color.colorSecondary))
                } else {
                    likeImage.setImageResource(R.drawable.feed_ic_like_noactive)
                    countLikeText.setTextColor(context.resources.getColor(R.color.santas_gray))
                }
        }

        @SuppressLint("CheckResult")
        fun bind(position: Int) {

            val post = data[position] as FeedItemPostUI
            itemView.setOnClickListener {}
            itemView.setOnClickListener {
                val now = System.currentTimeMillis()
                if (now - mLastClickTime > CLICK_TIME_INTERVAL) {
                    feedCallbackInterface.openPost(post)
                    mLastClickTime = now
                }
            }
            setupHeaderPost(post)
            setupText(post)
            setupPostContent(post)
            setupActionBarPost(post)
        }

        @SuppressLint("CheckResult")
        private fun setupActionBarPost(post: FeedItemPostUI) {
            val countLikeView = itemView.count_like
            val likeFrameView = itemView.like_frame
            val countCommentView = itemView.count_comment
            val commentFrameView = itemView.comment_frame
            if (post.countLike != null && post.countLike != 0) {
                countLikeView.visibility = View.VISIBLE
                countLikeView.text = post.countLike.toString()
            } else {
                countLikeView.visibility = View.INVISIBLE
            }
            if (post.countComment != null && post.countComment != 0L) {
                countCommentView.visibility = View.VISIBLE
                countCommentView.text = post.countComment.toString()
            } else {
                countCommentView.visibility = View.INVISIBLE
            }
            refreshLikeState(context, post.isLiked)
            likeFrameView.setOnClickListener {
                val now = System.currentTimeMillis()
                if (now - mLastClickTime > CLICK_TIME_INTERVAL) {
                    feedCallbackInterface.likePost(post)?.subscribe({ result ->
                        if (result?.likes != null) {
                            post.countLike = result.likes
                            countLikeView.text = result.likes.toString()
                            if (result.likes != 0) {
                                countLikeView.visibility = View.VISIBLE
                            } else {
                                countLikeView.visibility = View.INVISIBLE
                            }
                            post.isLiked = result.user_like
                            refreshLikeState(context, result.user_like)
                            val intent = Intent("LIKE")
                            intent.putExtra("id", post.id)
                            intent.putExtra("like", post.isLiked)
                            intent.putExtra("countLike", post.countLike)
                            intent.putExtra("countComment", post.countComment)
                            context.sendBroadcast(intent)
                        }
                    }, { throwable ->
                        Log.e(TAG, "Like response throwable")
                        throwable.printStackTrace()
                    })
                    mLastClickTime = now
                }
            }

            commentFrameView.setOnClickListener {
                feedCallbackInterface.openComment(post)
            }
        }

        private fun setupHeaderPost(post: FeedItemPostUI) {
            val iconImageView = itemView.icon_image
            val nameOwner = itemView.name_user
            val dateText = itemView.date_post
            dateText.text = getStringTimeAgoByLong(post.date)
            itemView.action_menu.setOnClickListener {
                if (uid != 0L) {
                    if (post.uid == uid) {
                        showPopupMe(it, post)
                    } else {
                        showPopupOther(it, post)
                    }
                }
            }
            if (post.ownerUI?.firstName != null) {
                nameOwner.text = post.ownerUI!!.firstName
                itemView.profile_contour.setOnClickListener {
                    feedCallbackInterface.openProfile(post.ownerUI!!)
                }
            } else {
                if (post.ownerUI?.nick != null) {
                    nameOwner.text = ("@" + post.ownerUI?.nick)
                } else {
                    nameOwner.text = null
                }
            }
            if (post.ownerUI?.avatarFileId != null) {
                val requestOptions =
                    RequestOptions().diskCacheStrategy(DiskCacheStrategy.ALL).circleCrop()
                Glide
                    .with(itemView)
                    .asBitmap()
                    .placeholder(R.drawable.feed_ic_placeholder_avatar)
                    .load(SignData.server_file + post.ownerUI!!.avatarFileId)
                    .apply(requestOptions)
                    .into(iconImageView)
            } else {
                Glide
                    .with(itemView)
                    .asBitmap()
                    .load(R.drawable.feed_ic_placeholder_avatar)
                    .into(iconImageView)
            }
        }

        @SuppressLint("ClickableViewAccessibility")
        private fun setupPostContent(post: FeedItemPostUI) {
            val videoView = itemView.video_layout
            val imageView = itemView.post_image
            val textOnVideo = itemView.text_video_time
            val fileLayout = itemView.file_layout
            imageView.visibility = View.GONE
            videoView.visibility = View.GONE
            fileLayout.visibility = View.GONE
            val postFile = post.post?.firstOrNull() ?: return
            val linkFile = postFile.fileId ?: return

            when (postFile.type) {
                FileModel.FILE_TYPE_IMAGE, FileModel.FILE_TYPE_GIF -> {
                    fileLayout.visibility = View.GONE
                    imageView.visibility = View.VISIBLE
                    videoView.visibility = View.GONE
                    val requestOptions =
                        RequestOptions().diskCacheStrategy(DiskCacheStrategy.ALL)
                    Glide
                        .with(itemView)
                        .asBitmap()
                        .placeholder(R.drawable.feed_place_holder)
                        .load(SignData.server_file + linkFile)
                        .apply(requestOptions)
                        .into(itemView.post_image)
                    imageView.setOnClickListener {
                        val now = System.currentTimeMillis()
                        if (now - mLastClickTime > CLICK_TIME_INTERVAL) {
                            val name = post.ownerUI?.firstName ?: ""
                            val date = getCalendar(post.date)
                            feedCallbackInterface.showImage(
                                SignData.server_file + linkFile,
                                name,
                                date
                            )
                            mLastClickTime = now
                        }
                    }
                }
                FileModel.FILE_TYPE_MP4 -> {
                    imageView.visibility = View.GONE
                    videoView.visibility = View.VISIBLE
                    fileLayout.visibility = View.GONE
                    textOnVideo.text = (post.post!!.first().length!! * 1000L).formatMillis()
                    Glide.with(context).asBitmap()
                        .diskCacheStrategy(DiskCacheStrategy.ALL)
                        .load(SignData.server_file + linkFile)
                        .into(itemView.video_choose)
                    videoView.setOnClickListener {
                        val tmpList = mutableListOf<MediaModel>()
                        tmpList.add(
                            MediaModel(
                                MediaType.VIDEO,
                                com.social.solutions.android.util_media_pager.models.FileModel(
                                    SignData.server_file + linkFile,
                                    null
                                )
                            )
                        )
                        val name = if (post.ownerUI?.firstName != null) {
                            post.ownerUI!!.firstName!!
                        } else {
                            if (post.ownerUI?.nick != null) {
                                ("@" + post.ownerUI?.nick)
                            } else {
                                "Неизвестно"
                            }
                        }
                        MediaPagerManager.with(context).setUserName(name).setItems(tmpList).start()
                    }
                }
                FileModel.FILE_TYPE_ANOTHER, FileModel.FILE_TYPE_TXT, FileModel.FILE_TYPE_ZIP, FileModel.FILE_TYPE_AUDIO, FileModel.FILE_TYPE_EXE -> {
                    imageView.visibility = View.GONE
                    videoView.visibility = View.GONE
                    fileLayout.visibility = View.VISIBLE
                    itemView.feed_file_name.text = post.post?.first()?.title
                    itemView.feed_file_weight_text.text = sizeFile(post.post?.first()?.size)
                    itemView.file_layout.setOnClickListener {
                        val i = Intent(Intent.ACTION_VIEW)
                        i.data = Uri.parse(SignData.server_file + linkFile)
                        startActivity(context, i, null)
                    }
                }
                else -> {
                    imageView.visibility = View.GONE
                    videoView.visibility = View.GONE
                    fileLayout.visibility = View.GONE
                }
            }
        }

        private fun setupText(post: FeedItemPostUI) {
            if (post.descriptionPost == null) return
            val descriptionText = itemView.text_description_post
            val expandText = itemView.expand_text
            descriptionText.text = post.descriptionPost
            descriptionText.post {
                if (descriptionText.lineCount > 7) {
                    expandText.visibility = View.VISIBLE
                    descriptionText.maxLines = 7
                    expandText.setOnClickListener {
                        expandText.visibility = View.GONE
                        val animation: ObjectAnimator =
                            ObjectAnimator.ofInt(
                                descriptionText,
                                "maxLines",
                                descriptionText.lineCount
                            )
                        animation.setDuration(50).start()
                    }
                } else {
                    expandText.visibility = View.GONE
                    descriptionText.maxLines = 7
                }
            }
        }

        private fun showPopupMe(v: View, post: FeedItemPostUI) {
            val popupView: View =
                from(context).inflate(R.layout.feed_menu_post_me, null)

            val popupWindow = PopupWindow(
                popupView,
                ViewGroup.LayoutParams.WRAP_CONTENT,
                ViewGroup.LayoutParams.WRAP_CONTENT
            )

            popupView.third_tab?.apply {
                setOnClickListener {
                    popupWindow.dismiss()
                    sharePost(post)
                }
            }
            popupView.fourth_tab?.setOnClickListener {
                showDialogRemove(it.context, post)
                popupWindow.dismiss()
            }
            popupWindow.isOutsideTouchable = true
            popupWindow.isFocusable = true
            popupWindow.showAsDropDown(v, 0, -v.height)
        }

        private fun sharePost(post: FeedItemPostUI) {
            val txtIntent = Intent(Intent.ACTION_SEND)
            txtIntent.type = "text/plain"
            txtIntent.putExtra(Intent.EXTRA_SUBJECT, "Ссылка на пост")
            txtIntent.putExtra(Intent.EXTRA_TEXT, SignData.server_file + "/post/${post.id}")
            startActivity(
                context,
                Intent.createChooser(
                    txtIntent,
                    "Поделиться"
                ), null
            )
        }

        private fun showPopupOther(v: View, post: FeedItemPostUI) {
            val popupView: View =
                from(context).inflate(R.layout.feed_menu_post_other, null)


            val popupWindow = PopupWindow(
                popupView,
                ViewGroup.LayoutParams.WRAP_CONTENT,
                ViewGroup.LayoutParams.WRAP_CONTENT
            )
            popupView.first_tab?.setOnClickListener {
                popupWindow.dismiss()
                sharePost(post)
            }
//        popupView.second_tab?.setOnClickListener {
//            popupWindow.dismiss()
//        }
//            popupView.third_tab?.setOnClickListener {
//                popupWindow.dismiss()
//            }
            popupWindow.setBackgroundDrawable(BitmapDrawable())
            popupWindow.isOutsideTouchable = true
            popupWindow.showAsDropDown(v, 0, -v.height)
        }

        private fun showDialogRemove(
            context: Context,
            post: FeedItemPostUI
        ) {
            val dialog = Dialog(context)
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
            dialog.setCancelable(true)
            dialog.setContentView(R.layout.feed_dialog_approve_remove)
            dialog.window?.setBackgroundDrawableResource(R.drawable.feed_dialog_background)
            dialog.action_btn?.setOnClickListener {
                feedCallbackInterface.removePost(post).subscribe({
                    val intent = Intent("REMOVE")
                    intent.putExtra("id", post.id)
                    context.sendBroadcast(intent)
                    val index = data.indexOf(post)
                    data.remove(post)
                    notifyItemRemoved(index)
                }, {
                    Toast.makeText(context, "Не удалось удалить пост", Toast.LENGTH_SHORT).show()
                })
                dialog.dismiss()
            }
            dialog.cancel_button?.setOnClickListener {
                dialog.dismiss()
            }
            dialog.show()
        }
    }


    inner class CreatePostViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        fun bind(position: Int) {
            itemView.setOnClickListener {
                feedCallbackInterface.createPost()
            }
        }
    }

    inner class PlaceholderProfileViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        fun bind(position: Int) {}
    }

    inner class PlaceholderUserViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        fun bind(position: Int) {}
    }

    inner class PlaceholderFeedViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        fun bind(position: Int) {}
    }

    inner class LoadMoreFeedViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        fun bind(position: Int) {}
    }

    interface FeedCallbackInterface {
        fun showImage(url: String, name: String, date: Calendar)
        fun showVideo(url: String, position: Long, name: String, date: Calendar)
        fun createPost()
        fun openPost(post: FeedItemPostUI)
        fun openComment(post: FeedItemPostUI)
        fun openProfile(user: UserMini)
        fun likePost(post: FeedItemPostUI): Single<MessageLikeResponse.Data?>?
        fun loadMore()
        fun removePost(postUI: FeedItemPostUI): Single<Any>
        fun removeNotifyCallback(postUI: FeedItemPostUI)
    }
}


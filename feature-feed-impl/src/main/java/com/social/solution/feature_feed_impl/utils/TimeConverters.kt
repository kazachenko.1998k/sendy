package com.social.solution.feature_feed_impl.utils

import java.text.SimpleDateFormat
import java.util.*


private const val SECOND = 1000
private const val MINUTE = SECOND * 60
private const val HOUR = MINUTE * 60
private const val DAY = HOUR * 24

private val months =
    listOf("янв", "фев", "мар", "апр", "мая", "июн", "июл", "авг", "сен", "окт", "ноя", "дек")

fun getStringTimeAgo(date: String): String {
    val sdf = SimpleDateFormat("dd.MM.yyyy HH:mm:ss")
    val timeLong = sdf.parse(date)?.time
    return if (timeLong != null)
        getStringTimeAgoByLong(timeLong)
    else date
}

fun getStringTimeAgoByLong(timeLong: Long?): String {
    if (timeLong == null) return "01.01.2077"
    val timeMS = timeLong*1000L
    val timeSDF = SimpleDateFormat("HH:mm")
    val calendarPost = Calendar.getInstance()
    calendarPost.timeInMillis = timeMS
    val calendarCurrent = Calendar.getInstance()
    calendarCurrent.timeInMillis = System.currentTimeMillis()
    val monthIndex = calendarPost.get(Calendar.MONTH)
    val dayIndex = calendarPost.get(Calendar.DAY_OF_MONTH)
    val yearIndex = calendarPost.get(Calendar.YEAR)
    val hourIndex = calendarPost.get(Calendar.HOUR)
    val minuteIndex = calendarPost.get(Calendar.MINUTE)

    val currentMonthIndex = calendarCurrent.get(Calendar.MONTH)
    val currentDayIndex = calendarCurrent.get(Calendar.DAY_OF_MONTH)
    val currentYearIndex = calendarCurrent.get(Calendar.YEAR)
    val currentHourIndex = calendarCurrent.get(Calendar.HOUR)
    val currentMinuteIndex = calendarCurrent.get(Calendar.MINUTE)

    val dDay = currentDayIndex - dayIndex
    val dateLong = Date(timeMS)

    return if (dDay == 0 && currentMonthIndex == monthIndex && yearIndex == currentYearIndex) {
        "Сегодня, ${timeSDF.format(dateLong)}"
    } else {
        if (dDay == 1 && currentMonthIndex == monthIndex && yearIndex == currentYearIndex)
            "Вчера, ${timeSDF.format(dateLong)}"
        else "$dayIndex ${months[monthIndex]} в ${timeSDF.format(dateLong)}"
    }
}

fun getCalendar(timeLong: Long?): Calendar {
    return if (timeLong == null) {
        val calendar = Calendar.getInstance()
        calendar.timeInMillis = System.currentTimeMillis() + 1000L * 60L * 60L * 24L * 31L * 12L * 50L
        calendar
    } else {
        val calendarPost = Calendar.getInstance()
        val timeMS = timeLong * 1000
        calendarPost.timeInMillis = timeMS
        calendarPost
    }
}

fun Long.formatMillis(): String {
    val formatBuilder = StringBuilder()
    val formatter = Formatter(formatBuilder, Locale.getDefault())
    getStringForTime(formatBuilder, formatter, this)
    return formatBuilder.toString()
}

/**
 * Returns the specified millisecond time formatted as a string.
 *
 * @param builder The builder that `formatter` will write to.
 * @param formatter The formatter.
 * @param timeMs The time to format as a string, in milliseconds.
 * @return The time formatted as a string.
 */
fun getStringForTime(builder: StringBuilder, formatter: Formatter, timeMs: Long): String {
    val totalSeconds = (timeMs + 500) / 1000
    val seconds = totalSeconds % 60
    val minutes = totalSeconds / 60 % 60
    val hours = totalSeconds / 3600
    builder.setLength(0)
    return if (hours > 0)
        formatter.format("%d:%02d:%02d", hours, minutes, seconds).toString()
    else
        formatter.format("%02d:%02d", minutes, seconds).toString()
}
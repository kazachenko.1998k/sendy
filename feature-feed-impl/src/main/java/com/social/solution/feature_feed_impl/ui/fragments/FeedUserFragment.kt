package com.social.solution.feature_feed_impl.ui.fragments

import android.annotation.SuppressLint
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.RecyclerView
import com.morozov.core_backend_api.chat.converter.MessageConverter
import com.morozov.core_backend_api.message.responses.MessageLikeResponse
import com.morozov.core_backend_api.user.UserMini
import com.social.solution.feature_feed_api.FeatureFeedCallback
import com.social.solution.feature_feed_api.models.*
import com.social.solution.feature_feed_impl.MainObject
import com.social.solution.feature_feed_impl.MainObject.TAG
import com.social.solution.feature_feed_impl.R
import com.social.solution.feature_feed_impl.ui.adapters.FeedAdapter
import com.social.solution.feature_feed_impl.ui.view_models.FeedUserViewModel
import com.social.solution.feature_feed_impl.utils.convertFrom
import com.social.solutions.android.transition_media_pager.manager.MediaPagerManager
import com.social.solutions.android.util_media_pager.models.FileModel
import com.social.solutions.android.util_media_pager.models.MediaModel
import com.social.solutions.android.util_media_pager.models.MediaType
import io.reactivex.Single
import kotlinx.android.synthetic.main.feed_user_layout.*
import java.util.*


open class FeedUserFragment : Fragment() {

    private var isRegistered = false
    private lateinit var config: FeedConfig
    private lateinit var callback: FeatureFeedCallback
    private var feedAdapter: FeedAdapter? = null
    private var mFeedUserViewModel: FeedUserViewModel? = null
    private val mReceiver = NotifyBroadcastReceiver()

    companion object {
        const val NAME = "FEED_USER"
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        setHasOptionsMenu(true)
        Log.d("FEED_MODULE", "ON_CR")
        if (mFeedUserViewModel == null) {
            Log.d("FEED_MODULE", "CREATE_FEED_USER_VIEW_MODEL")
            mFeedUserViewModel = ViewModelProviders.of(this).get(FeedUserViewModel::class.java)
        }
        return inflater.inflate(R.layout.feed_user_layout, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        mFeedUserViewModel!!.setConfig(config)
        Log.d(TAG, "User feed onViewCreated")
        Log.d(TAG, "User Config1 = " + config.isMyProfile)

        if (!config.isMyProfile) {
            Log.d(TAG, "User feed cleared data")
            mFeedUserViewModel!!.clearData()
        }
        val listCachePosts =
            mFeedUserViewModel!!.getMessages()?.map { convertFrom(it) }?.toMutableList()
        if (listCachePosts == null) {
            feedAdapter = null
        } else {
            feedAdapter?.clearData()
            if (config.showCreatePostButton) {
                feedAdapter?.addData(mutableListOf(FeedItemCreatePost()))
            }
            feedAdapter?.addData(listCachePosts as MutableList<FeedItem>)
        }
        initFeedAdapter(feed_recycler_view_post_user)
        val filter = IntentFilter("REMOVE")
        filter.addAction("LIKE")
        filter.addAction("COMMENT")
        filter.addAction("CREATE_POST")
        try {
            if (!isRegistered){
                context!!.registerReceiver(mReceiver, filter)
                isRegistered = true
            }
        } catch (e: Exception) {
        }
    }

    private fun initFeedAdapter(recycler: RecyclerView?) {
        if (feedAdapter == null) {
            val initList = mutableListOf<FeedItem>()
            if (config.showCreatePostButton) {
                initList.add(FeedItemCreatePost())
            }
            feedAdapter = FeedAdapter(
                context!!,
                initList,
                FeedAdapterCallback()
            )
            loadNewPost(true)
        }
        recycler?.adapter = feedAdapter

    }

    private fun initListenerConnection() {
        MainObject.mNetworkState.observe(viewLifecycleOwner, Observer {
            if (it) {
                feedAdapter!!.hideProgress()
            }
        })
    }

    @SuppressLint("CheckResult")
    private fun loadNewPost(viewCache: Boolean) {
        val messages = mFeedUserViewModel?.getMessages()
        Log.d("FEED_MODULE", "User strategy = " + messages)
        if (viewCache && messages != null) {
            Log.d("FEED_MODULE", "User strategy1")
            if (messages.isNullOrEmpty()) {
                feedAdapter!!.addData(mutableListOf(FeedItemPlaceholder()))
            } else {
                feedAdapter!!.addData(messages.map { convertFrom(it) }
                    .toMutableList() as MutableList<FeedItem>)
            }
            initListenerConnection()
        } else {
            Log.d("FEED_MODULE", "User strategy2")
            feedAdapter!!.showProgress()
            mFeedUserViewModel!!.loadUser(config.feedId, MainObject.mBackendApi.chatApi())
                ?.subscribe({ newPost ->
                    feedAdapter!!.hideProgress()
                    initListenerConnection()
                    addPostToFeed(newPost as MutableList<FeedItem>)
                }, { throwable ->
                    Log.e(TAG, throwable.printStackTrace().toString())
                })
        }
    }

    private fun addPostToFeed(itemFeed: MutableList<FeedItem>?) {

        if (itemFeed.isNullOrEmpty() && mFeedUserViewModel!!.getMessages().isNullOrEmpty()) {
            if (config.isMyProfile) {
                feedAdapter!!.addData(mutableListOf(FeedItemPlaceholderProfile()))
            } else {
                feedAdapter!!.addData(mutableListOf(FeedItemPlaceholderUser()))
            }
        } else {
            feedAdapter!!.addData(itemFeed)
        }
    }

    fun removePost(postUI: FeedItemPostUI) {
        mFeedUserViewModel!!.removeByID(postUI.id!!)
        feedAdapter!!.removeNotifyByID(postUI)
    }

    fun setCallback(callback: FeatureFeedCallback) {
        this.callback = callback
    }

    fun setConfig(config: FeedConfig) {
        Log.d(TAG, "User Config1 = " + config.isMyProfile)
        this.config = config
    }

    inner class FeedAdapterCallback : FeedAdapter.FeedCallbackInterface {
        override fun showImage(url: String, name: String, date: Calendar) {
            val tmpList = mutableListOf<MediaModel>()
            tmpList.add(
                MediaModel(
                    MediaType.PHOTO,
                    FileModel(url, null)
                )
            )
            MediaPagerManager.with(context!!).setUserName(name).setDate(date).setItems(tmpList)
                .start()
        }

        override fun showVideo(url: String, position: Long, name: String, date: Calendar) {
            val tmpList = mutableListOf<MediaModel>()
            tmpList.add(
                MediaModel(
                    MediaType.VIDEO,
                    FileModel(url, null)
                )
            )
            MediaPagerManager.with(context!!).setUserName(name).setDate(date).setItems(tmpList)
                .setStartPosition(position.toInt()).start()
        }

        override fun createPost() {
            callback.createPost() {
            }
        }

        override fun openPost(post: FeedItemPostUI) {
            val message = mFeedUserViewModel!!.getMessageById(post.id)
            if (message != null) {
                callback.openPost(message)
            }
        }

        override fun openComment(post: FeedItemPostUI) {

            val message = mFeedUserViewModel?.getMessageById(post.id)
            if (message != null) {
                callback.openPost(message)
            }
        }

        override fun openProfile(user: UserMini) {
            callback.openProfile(user)
        }

        override fun likePost(post: FeedItemPostUI): Single<MessageLikeResponse.Data?>? {

            return mFeedUserViewModel!!.like(MainObject.mBackendApi.messageApi(), post)
        }

        override fun loadMore() {
            feed_recycler_view_post_user?.post {
                loadNewPost(false)
            }
        }

        override fun removePost(postUI: FeedItemPostUI): Single<Any> {
            return mFeedUserViewModel!!.removePost(postUI)
        }

        override fun removeNotifyCallback(postUI: FeedItemPostUI) {
            callback.removeNotify(postUI)
        }
    }

    inner class NotifyBroadcastReceiver : BroadcastReceiver() {

        override fun onReceive(context: Context, intent: Intent) {
            val id = intent.extras!!.getLong("id")
            Log.d("FEED_MODULE", "RECEIVE data = " + id)
            when (intent.action) {
                "REMOVE" -> {
                    mFeedUserViewModel?.removeByID(id)
                    feedAdapter?.removeById(id)
                }
                "LIKE" -> {
                    val like = intent.extras!!.getBoolean("like")
                    val countLike = intent.extras!!.getInt("countLike")
                    val countComment = intent.extras!!.getLong("countComment")
                    feedAdapter?.likeById(id, like, countLike, countComment)
                    mFeedUserViewModel?.refreshLike(id, countLike, like)
                }
                "COMMENT" -> {
                    val countComment = intent.extras!!.getLong("countComment")
                    feedAdapter?.refreshComment(id, countComment)
                    mFeedUserViewModel?.refreshComment(id, countComment)
                }
                "CREATE_POST" -> {
                    val message = MessageConverter().toMessage(intent.extras!!.getString("message"))
                    feedAdapter?.addNewPost(1, convertFrom(message!!))
                    mFeedUserViewModel?.addNewPost(message)
                }
            }
            Log.d("FEED_MODULE", "RECEIVE = " + intent.action)
        }
    }
}
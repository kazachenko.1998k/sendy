package com.social.solution.feature_feed_impl.ui.fragments

import android.annotation.SuppressLint
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.os.Bundle
import android.util.Log
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.RecyclerView
import com.morozov.core_backend_api.chat.converter.MessageConverter
import com.morozov.core_backend_api.message.responses.MessageLikeResponse
import com.morozov.core_backend_api.user.UserMini
import com.social.solution.feature_feed_api.FeatureFeedCallback
import com.social.solution.feature_feed_api.models.FeedItem
import com.social.solution.feature_feed_api.models.FeedItemPlaceholder
import com.social.solution.feature_feed_api.models.FeedItemPostUI
import com.social.solution.feature_feed_impl.MainObject
import com.social.solution.feature_feed_impl.MainObject.TAG
import com.social.solution.feature_feed_impl.MainObject.mNetworkState
import com.social.solution.feature_feed_impl.R
import com.social.solution.feature_feed_impl.ui.adapters.FeedAdapter
import com.social.solution.feature_feed_impl.ui.view_models.FeedBestViewModel
import com.social.solution.feature_feed_impl.utils.convertFrom
import com.social.solution.feature_feed_impl.utils.dpToPx
import com.social.solutions.android.transition_media_pager.manager.MediaPagerManager
import com.social.solutions.android.util_media_pager.models.FileModel
import com.social.solutions.android.util_media_pager.models.MediaModel
import com.social.solutions.android.util_media_pager.models.MediaType
import io.reactivex.Single
import kotlinx.android.synthetic.main.feed_layout.*
import java.util.*

open class FeedBestFragment : Fragment() {

    private var isRegistered = false
    private lateinit var feedAdapter: FeedAdapter
    lateinit var mFeedBestViewModel: FeedBestViewModel
    private lateinit var callback: FeatureFeedCallback
    private val mReceiver = NotifyBroadcastReceiver()

    companion object {
        const val NAME = "FEED_BEST"
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        setHasOptionsMenu(true)
        mFeedBestViewModel = ViewModelProviders.of(activity!!).get(FeedBestViewModel::class.java)
        return inflater.inflate(R.layout.feed_layout, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initSwipeRefresh()
        initFeedAdapter(feed_recycler_view_post)
        loadNewPost(true)
        val filter = IntentFilter("REMOVE")
        filter.addAction("LIKE")
        filter.addAction("COMMENT")
        filter.addAction("CREATE_POST")
        try {
            if (!isRegistered){
                context!!.registerReceiver(mReceiver, filter)
                isRegistered = true
            }
        } catch (e: Exception) {
        }
    }

    private fun initListenerConnection() {
        mNetworkState.observe(viewLifecycleOwner, Observer {
            if (it) {
                feedAdapter.hideProgress()
            }
        })
    }


    private fun initSwipeRefresh() {
        swipe_container?.setOnRefreshListener {
            if (mNetworkState.value != null && !mNetworkState.value!!) {
                showErrorInternetConnection()
                return@setOnRefreshListener
            }
            mFeedBestViewModel.loadBest(MainObject.mBackendApi.feedApi(), null)
                ?.subscribe({ refreshPosts ->
                    refreshNewData(refreshPosts)
                    swipe_container?.isRefreshing = false
                }, { throwable ->
                    Log.e(TAG, throwable.printStackTrace().toString())
                    swipe_container?.isRefreshing = false
                })
        }
    }

    private fun showErrorInternetConnection() {
        swipe_container?.isRefreshing = false
        val toast = Toast.makeText(
            context,
            "Нет соединения. Проверьте подключение к интернету",
            Toast.LENGTH_SHORT
        )
        toast.setGravity(
            Gravity.BOTTOM,
            0,
            dpToPx(80f, resources)
        )
        toast.show()
    }

    private fun refreshNewData(refreshPosts: MutableList<FeedItemPostUI>?) {
        feedAdapter.clearData()
        if (refreshPosts.isNullOrEmpty() && mFeedBestViewModel.getMessages().isNullOrEmpty()) {
            feedAdapter.addData(mutableListOf(FeedItemPlaceholder()))
        } else {
            feedAdapter.addData(refreshPosts as MutableList<FeedItem>)
        }
    }

    private fun initFeedAdapter(recycler: RecyclerView?) {
        val initList = mutableListOf<FeedItem>()
        feedAdapter = FeedAdapter(
            context!!,
            initList,
            FeedAdapterCallback()
        )
        recycler?.adapter = feedAdapter
    }

    @SuppressLint("CheckResult")
    private fun loadNewPost(viewCache: Boolean) {
        val messages = mFeedBestViewModel.getMessages()
        if (viewCache && messages != null) {
            if (messages.isNullOrEmpty()) {
                feedAdapter.addData(mutableListOf(FeedItemPlaceholder()))
            } else {
                feedAdapter.addData(messages.map { convertFrom(it) }.toMutableList() as MutableList<FeedItem>)
            }
            initListenerConnection()
        } else {
            feedAdapter.showProgress()
            mFeedBestViewModel.loadBest(MainObject.mBackendApi.feedApi(), 0)
                ?.subscribe({ newPost ->
                    feedAdapter.hideProgress()
                    initListenerConnection()
                    if (newPost.isNullOrEmpty() && mFeedBestViewModel.getMessages().isNullOrEmpty()) {
                        feedAdapter.addData(mutableListOf(FeedItemPlaceholder()))
                    } else {
                        feedAdapter.addData(newPost as MutableList<FeedItem>)
                    }
                }, { throwable ->
                    Log.e(TAG, throwable.printStackTrace().toString())
                })
        }
    }

    fun refreshData() {
        mFeedBestViewModel.clearLastMessageId()
        mFeedBestViewModel.clearMessages()
        feedAdapter.clearData()
    }

    fun removePost(postUI: FeedItemPostUI) {
        mFeedBestViewModel.removeByID(postUI.id!!)
        feedAdapter.removeNotifyByID(postUI)
    }

    fun setCallback(callback: FeatureFeedCallback) {
        this.callback = callback
    }

    inner class FeedAdapterCallback : FeedAdapter.FeedCallbackInterface {
        override fun showImage(url: String, name: String, date: Calendar) {
            val tmpList = mutableListOf<MediaModel>()
            tmpList.add(
                MediaModel(
                    MediaType.PHOTO,
                    FileModel(url, null)
                )
            )
            MediaPagerManager.with(context!!).setUserName(name).setDate(date).setItems(tmpList)
                .start()
        }

        override fun showVideo(url: String, position: Long, name: String, date: Calendar) {
            val tmpList = mutableListOf<MediaModel>()
            tmpList.add(
                MediaModel(
                    MediaType.VIDEO,
                    FileModel(url, null)
                )
            )
            MediaPagerManager.with(context!!).setUserName(name).setDate(date).setItems(tmpList)
                .setStartPosition(position.toInt()).start()
        }

        override fun createPost() {
            callback.createPost() {

            }
        }

        override fun openPost(post: FeedItemPostUI) {
            val message = mFeedBestViewModel.getMessageById(post.id)
            if (message != null) {
                callback.openPost(message)
            }
        }

        override fun openComment(post: FeedItemPostUI) {
            val message = mFeedBestViewModel.getMessageById(post.id)
            if (message != null) {
                callback.openPost(message)
            }
        }

        override fun openProfile(user: UserMini) {
            callback.openProfile(user)
        }

        override fun likePost(post: FeedItemPostUI): Single<MessageLikeResponse.Data?>? {
            return mFeedBestViewModel.like(MainObject.mBackendApi.messageApi(), post)
        }

        override fun loadMore() {
            feed_recycler_view_post?.post {
                loadNewPost(false)
            }
        }

        override fun removePost(postUI: FeedItemPostUI): Single<Any> {
            return mFeedBestViewModel.removePost(postUI)
        }

        override fun removeNotifyCallback(postUI: FeedItemPostUI) {

        }
    }

    inner class NotifyBroadcastReceiver : BroadcastReceiver() {

        override fun onReceive(context: Context, intent: Intent) {
            val id = intent.extras!!.getLong("id")
            Log.d("FEED_MODULE", "RECEIVE data = " + id)
            when (intent.action) {
                "REMOVE" -> {
                    mFeedBestViewModel.removeByID(id)
                    feedAdapter.removeById(id)
                }
                "LIKE" -> {
                    val like = intent.extras!!.getBoolean("like")
                    val countLike = intent.extras!!.getInt("countLike")
                    val countComment = intent.extras!!.getLong("countComment")
                    feedAdapter.likeById(id, like, countLike, countComment)
                    mFeedBestViewModel.refreshLike(id, countLike, like)
                }
                "COMMENT" -> {
                    val countComment = intent.extras!!.getLong("countComment")
                    feedAdapter.refreshComment(id, countComment)
                    mFeedBestViewModel.refreshComment(id, countComment)
                }
                "CREATE_POST" -> {
//                    val message = MessageConverter().toMessage(intent.extras!!.getString("message"))
//                    feedAdapter.addNewPost(0,convertFrom(message!!))
//                    mFeedBestViewModel.addNewPost(message)
                    refreshData()
                    loadNewPost(false)
                }
            }
            Log.d("FEED_MODULE", "RECEIVE = " + intent.action)
        }
    }
}
package com.social.solution.feature_feed_impl.ui.fragments


import android.annotation.SuppressLint
import android.os.Bundle
import android.os.Handler
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import androidx.lifecycle.LiveData
import androidx.lifecycle.Observer
import com.google.android.material.tabs.TabLayout
import com.social.solution.feature_feed_api.FeatureFeedCallback
import com.social.solution.feature_feed_impl.MainObject
import com.social.solution.feature_feed_impl.R
import com.social.solution.feature_feed_impl.ui.adapters.FeedAdapter
import kotlinx.android.synthetic.main.feed_common_fragment.*
import kotlinx.android.synthetic.main.feed_item_tab_layout.view.*


class CommonFragment(
    private val bestFeedFragment: FeedBestFragment,
    private var subscriptionFeedFragment: FeedSubscribersFragment
) : Fragment() {

    private lateinit var callback: FeatureFeedCallback
    companion object {
        const val NAME = "COMMON_FRAGMENT"
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        setHasOptionsMenu(true)
        return inflater.inflate(R.layout.feed_common_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        this.initToolbarMenu()
        this.initViewPager()
    }

    private fun initToolbarMenu() {
        action_add_note?.setOnClickListener {
            callback.createPost {
//                subscriptionFeedFragment.refreshData()
                Handler().postDelayed({
                    feed_tab_layout?.getTabAt(0)?.select()
                }, 200)
            }
        }
        button_search?.setOnClickListener {
            callback.openNotification()
        }
    }

    private fun initViewPager() {
        Log.d("FEED_MODULE", "Common Feed configureTabLayout")
        feed_view_pager?.offscreenPageLimit = 2
        val adapter = TabPagerAdapter(childFragmentManager)
        feed_view_pager?.adapter = adapter
        feed_view_pager?.currentItem = 0
        feed_tab_layout?.setupWithViewPager(feed_view_pager)
        for (i in 0 until feed_tab_layout.tabCount) {
            val tab: TabLayout.Tab = feed_tab_layout.getTabAt(i)!!
            tab.customView = adapter.getTabView(i)
        }
    }

    fun setCallback(callback: FeatureFeedCallback) {
        this.callback = callback
    }

    @SuppressLint("WrongConstant")
    private inner class TabPagerAdapter(fm: FragmentManager) :
        FragmentPagerAdapter(fm) {
        private val tabTitles = arrayOf("Подписки", "Интересное")

        override fun getItem(position: Int): Fragment {
            return when (position) {
                0 -> subscriptionFeedFragment
                else -> bestFeedFragment
            }
        }

        override fun getCount(): Int {
            return tabTitles.size
        }

        fun getTabView(position: Int): View? {
            val v = LayoutInflater.from(context).inflate(R.layout.feed_item_tab_layout, null)
            v.name_tab_view?.text = tabTitles[position]
            return v
        }
    }
}
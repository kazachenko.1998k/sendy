package com.social.solution.feature_feed_impl.ui.adapters

import android.view.LayoutInflater.from
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.google.android.flexbox.FlexboxLayoutManager
import com.social.solution.feature_feed_api.models.FeedItemPostUI
import com.social.solution.feature_feed_impl.R
import kotlinx.android.synthetic.main.feed_card_post_item.view.*


class FeedPostAdapter(
    private var postCollection: FeedItemPostUI,
    private val widthRecycler: Int
) :
    RecyclerView.Adapter<FeedPostAdapter.PostCollectionViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PostCollectionViewHolder {
        val view = from(parent.context).inflate(R.layout.feed_card_post_item, parent, false)
        return PostCollectionViewHolder(view)
    }

    override fun getItemCount() = postCollection.post!!.size

    override fun onBindViewHolder(holder: PostCollectionViewHolder, position: Int) {
        holder.onBind(position, postCollection)
    }

    class PostCollectionViewHolder(view: View) : RecyclerView.ViewHolder(view) {

        fun onBind(
            position: Int,
            postCollection: FeedItemPostUI
        ) {
            if (postCollection.post != null) {
                val imageLink = postCollection.post!![position].sourceUrl
                val postImage = itemView.post_collection_image
                Glide
                    .with(postImage.context)
                    .asDrawable()
                    .placeholder(R.drawable.feed_place_holder)
                    .load(imageLink)
                    .into(postImage)
                postImage.setImageResource(R.drawable.feed_ic_placeholder)
                val lp = postImage.layoutParams
                if (lp is FlexboxLayoutManager.LayoutParams) {
                    lp.flexGrow = 2f
                }
            }
        }
    }
}
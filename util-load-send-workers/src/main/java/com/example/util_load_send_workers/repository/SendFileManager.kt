package com.example.util_load_send_workers.repository

import android.content.Context
import androidx.lifecycle.MutableLiveData
import com.example.util_load_send_workers.send.models.UploadFileModel
import com.morozov.core_backend_api.FeatureBackendApi
import java.lang.IllegalArgumentException

object SendFileManager {

    fun with(context: Context): Transaction = Transaction(context)

    /**                     Required for message:
        recipientId or chatId
        message type
        files list
        user id
                            Required for cancel:
        files list
                            Required for send files:
        files list
     * */
    class Transaction(private val context: Context) {
        private var backendApi: FeatureBackendApi? = null
        private var recipientId: Long? = null
        private var chatId: Long? = null
        private var replyTo: Long? = null
        private var messageType: Int? = null
        private var message: String? = null
        private var userId: Long? = null
        private var filesList: List<File>? = null
        private var messageSentLiveData: MutableLiveData<Long?>? = null

        /** messageSentLiveData Long -- message id
         * */
        fun setMessageSentLiveData(messageSentLiveData: MutableLiveData<Long?>): Transaction {
            this.messageSentLiveData = messageSentLiveData
            return this
        }

        fun setBackendApi(backendApi: FeatureBackendApi?): Transaction {
            this.backendApi = backendApi
            return this
        }

        fun setRecipientId(recipientId: Long?): Transaction {
            if (recipientId != null)
                this.chatId = null
            this.recipientId = recipientId
            return this
        }

        fun setChatId(chatId: Long?): Transaction {
            if (chatId != null)
                this.recipientId = null
            this.chatId = chatId
            return this
        }

        fun setReplyTo(replyTo: Long?): Transaction {
            this.replyTo = replyTo
            return this
        }

        fun setMessageType(messageType: Int): Transaction {
            this.messageType = messageType
            return this
        }

        fun setMessage(message: String): Transaction {
            if (message.isNotEmpty())
                this.message = message
            return this
        }

        fun setUserId(userId: Long?): Transaction {
            this.userId = userId
            return this
        }

        @Deprecated(
            "No more support",
            ReplaceWith("setFilesExtended(filesList: List<File>)"),
            DeprecationLevel.WARNING)
        fun setFiles(filesList: List<String>): Transaction {
            this.filesList = filesList.map { File(it) }
            return this
        }

        fun setVoice(filePath: String, length: Int): Transaction {
            filesList = listOf(File(filePath).createVoice(length))
            return this
        }

        fun setFilesExtended(filesList: List<File>): Transaction {
            this.filesList = filesList
            return this
        }

        private fun getUploadModels(): List<UploadFileModel> {
            return filesList!!.map {  fileModel ->
                UploadFileModel(
                    fileModel.filePath,
                    "",
                    userId!!,
                    "",
                    fileModel.title,
                    fileModel.ext,
                    fileModel.size,
                    false,
                    fileModel.length
                )
            }
        }

        private fun checkRequiredForMessageNotNull() {
            var requiredString: String? = null
            when {
                recipientId == null && chatId == null ->
                    requiredString = "RecipientId or chatId"
                messageType == null ->
                    requiredString = "Message type"
                filesList == null ->
                    requiredString = "Files list"
                userId == null ->
                    requiredString = "UserId"
            }
            if (requiredString != null)
                throw IllegalArgumentException("$requiredString is required!")
        }

        private fun checkFilesNotNull() {
            var requiredString: String? = null
            if (filesList == null)
                requiredString = "Files list"
            if (requiredString != null)
                throw IllegalArgumentException("$requiredString is required!")
        }

        fun cancelLoad() {
            checkFilesNotNull()

            filesList!!.forEach {
                FilesWorkRepository.cancelUpload(context, it.filePath)
            }
        }

        fun loadFiles() {
            checkFilesNotNull()

            backendApi!!.fileApi().sendListFilesAndBody(
                context,
                getUploadModels()
            )
        }

        fun sendMessage() {
            checkRequiredForMessageNotNull()

            backendApi!!.sendReplyMessageWithFiles(
                context,
                recipientId,
                chatId,
                replyTo,
                messageType!!,
                message,
                getUploadModels(),
                messageSentLiveData
            )
        }

        class File(val filePath: String,
                   val title: String? = null,
                   val ext: String? = null,
                   val size: Int? = null,
                   var length: Int? = null) {
            fun createVoice(length: Int): File {
                this.length = length
                return this
            }
            fun createVideo(length: Int): File {
                this.length = length
                return this
            }
            fun createVideoIfNotNull(length: Int?): File {
                this.length = length
                return this
            }
        }
    }
}
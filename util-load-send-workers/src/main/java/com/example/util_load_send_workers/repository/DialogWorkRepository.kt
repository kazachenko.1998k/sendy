package com.example.util_load_send_workers.repository

import android.content.Context
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.example.util_load_send_workers.send.models.UploadFileModel
import com.morozov.core_backend_api.AnyInnerRequest
import com.morozov.core_backend_api.AnyRequest
import com.morozov.core_backend_api.FeatureBackendApi
import com.morozov.core_backend_api.message.MessageApi
import com.morozov.core_backend_api.message.requests.MessageSendRequest
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch

fun FeatureBackendApi.sendMessageWithFiles(context: Context,
                                           recipientId: Long?,
                                           chatId: Long?,
                                           messageType: Int?,
                                           message: String?,
                                           files: List<UploadFileModel>,
                                           messageSentLiveData: MutableLiveData<Long?>? = null) {
    DialogWorkRepository.sendFilesAndMessageAfter(
        context,
        this,
        DialogWorkRepository.MessageWorkModel(
            recipientId,
            chatId,
            null,
            messageType,
            message,
            files.toMutableList(),
            messageSentLiveData
        )
    )
}

fun FeatureBackendApi.sendReplyMessageWithFiles(context: Context,
                                                recipientId: Long?,
                                                chatId: Long?,
                                                replyTo: Long?,
                                                messageType: Int?,
                                                message: String?,
                                                files: List<UploadFileModel>,
                                                messageSentLiveData: MutableLiveData<Long?>? = null) {
    DialogWorkRepository.sendFilesAndMessageAfter(
        context,
        this,
        DialogWorkRepository.MessageWorkModel(
            recipientId,
            chatId,
            replyTo,
            messageType,
            message,
            files.toMutableList(),
            messageSentLiveData
        )
    )
}

object DialogWorkRepository {

    private var mBackendApi: FeatureBackendApi? = null

    private val mMessageWorkStack = mutableListOf<MessageWorkModel>()

    internal val mSendingLiveData = MutableLiveData<SendingData>()

    internal const val LOAD_FAILED = -100

    fun getLoadingLiveData(): LiveData<SendingData> = mSendingLiveData
    fun getSendingMessages(): List<MessageWorkModel> = mMessageWorkStack

    init {
        mSendingLiveData.observeForever { loadData ->
            val backendApi = mBackendApi ?: return@observeForever
            when (loadData.progress) {
                LOAD_FAILED -> setError(loadData.filePath)
                100 -> setLoadedAndSend(backendApi.messageApi(), loadData.filePath)
                FilesWorkRepository.CANCELED_STATUS -> setCancelFileAndSendOrDelete(backendApi.messageApi(), loadData.filePath)
            }
        }
    }

    fun sendFilesAndMessageAfter(context: Context,
                                 backendApi: FeatureBackendApi,
                                 messageModel: MessageWorkModel) {
        mMessageWorkStack.add(messageModel)
        mBackendApi = backendApi
        backendApi.fileApi().sendListFilesAndBody(context, messageModel.files)
    }

    private fun setError(filePath: String) {
        val msgStackTmp = mutableListOf<MessageWorkModel>()
        val deleteStackTmp = mutableListOf<MessageWorkModel>()
        msgStackTmp.addAll(mMessageWorkStack)

        for (model in msgStackTmp) {
            val index = model.indexIfContains(filePath)
            if (index != null) {
                model.messageSentLiveData?.value = null
                deleteStackTmp.add(model)
            }
        }

        mMessageWorkStack.removeAll(deleteStackTmp)
    }

    private fun setLoadedAndSend(messageApi: MessageApi,
                                 filePath: String) {
        val msgStackTmp = mutableListOf<MessageWorkModel>()
        val deleteStackTmp = mutableListOf<MessageWorkModel>()

        msgStackTmp.addAll(mMessageWorkStack)

        for (model in msgStackTmp) {
            val index = model.indexIfContains(filePath)
            if (index != null) {
                model.files[index].isLoaded = true
                if (model.isAllFilesSent()) {
                    sendMessageWithMedia(messageApi, model)
                    deleteStackTmp.add(model)
                }
            }
        }

        mMessageWorkStack.removeAll(deleteStackTmp)
    }

    private fun setCancelFileAndSendOrDelete(messageApi: MessageApi,
                                             filePath: String) {
        val msgStackTmp = mutableListOf<MessageWorkModel>()
        val deleteStackTmp = mutableListOf<MessageWorkModel>()

        msgStackTmp.addAll(mMessageWorkStack)

        for (model in msgStackTmp) {
            val index = model.indexIfContains(filePath)
            if (index != null) {
                model.files.removeAt(index)
                when {
                    model.files.isEmpty() && model.message.isNullOrEmpty().not() -> {
                        sendMessageWithMedia(messageApi, model)
                        deleteStackTmp.add(model)
                    }
                    model.files.isEmpty() -> {
                        deleteStackTmp.add(model)
                    }
                    model.isAllFilesSent() -> {
                        sendMessageWithMedia(messageApi, model)
                        deleteStackTmp.add(model)
                    }
                }
            }
        }

        mMessageWorkStack.removeAll(deleteStackTmp)
    }

    private fun sendMessageWithMedia(messageApi: MessageApi,
                                     messageModel: MessageWorkModel) {
        val param = MessageSendRequest()
        param.recipient_id = messageModel.recipientId
        param.chat_id = messageModel.chatId
        param.message = messageModel.message
        param.file_ids = messageModel.files.map { it.fileId }.toMutableList()
        param.type = messageModel.messageType
        param.reply_to = messageModel.replyTo
        val sendMessage = AnyInnerRequest(param)
        val data = AnyRequest(sendMessage)
        messageApi.send(data) {
            messageModel.messageSentLiveData?.postValue(it.data?.id)
        }
    }

    data class MessageWorkModel(val recipientId: Long?,
                                val chatId: Long?,
                                val replyTo: Long?,
                                val messageType: Int?,
                                val message: String?,
                                val files: MutableList<UploadFileModel>,
                                val messageSentLiveData: MutableLiveData<Long?>? = null) {
        fun indexIfContains(filePath: String): Int? {
            for ((index, file) in files.withIndex()) {
                if (file.filePath == filePath)
                    return index
            }
            return null
        }

        fun isAllFilesSent(): Boolean {
            var result = true
            for (file in files) {
                if (file.isLoaded.not())
                    result = false
            }
            return result
        }
    }

    data class SendingData(val filePath: String,
                           val fileId: String,
                           val progress: Int)
}
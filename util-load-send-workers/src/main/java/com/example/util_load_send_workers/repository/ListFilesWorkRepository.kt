package com.example.util_load_send_workers.repository

import android.content.Context
import androidx.lifecycle.LifecycleOwner
import com.example.util_load_send_workers.send.models.UploadFileModel
import com.morozov.core_backend_api.file.FileApi

fun FileApi.sendListFilesAndBody(context: Context,
                                 files: List<UploadFileModel>) {
    files.forEach { file ->
        sendAndUploadBody(context, file)
    }
}
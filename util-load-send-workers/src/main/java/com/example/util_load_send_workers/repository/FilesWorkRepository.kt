package com.example.util_load_send_workers.repository

import android.content.Context
import android.util.Log
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.Observer
import androidx.work.*
import com.example.util_load_send_workers.send.UploadWorker
import com.example.util_load_send_workers.send.models.UploadFileModel
import com.example.util_load_send_workers.utils.Constants.PROGRESS
import com.example.util_load_send_workers.utils.getMimeType
import com.example.util_load_send_workers.utils.getName
import com.example.util_load_send_workers.utils.getSize
import com.morozov.core_backend_api.AnyInnerRequest
import com.morozov.core_backend_api.AnyRequest
import com.morozov.core_backend_api.file.FileApi
import com.morozov.core_backend_api.file.request.FileUploadRequest
import io.reactivex.Single
import io.reactivex.disposables.Disposable
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import java.util.*

fun FileApi.sendAndUploadBody(context: Context,
                              file: UploadFileModel) {
    FilesWorkRepository.addNewUpload(
        FilesWorkRepository.UploadingRequestModel(
            file.filePath,
            file.fileId,
            sendFile(file.filePath, file.title, file.ext, file.size, file.length)
                .subscribe({
                    file.fileId = it.first
                    file.baseUrl = it.second
                    FilesWorkRepository.sendBody(
                        context,
                        file
                    )
                }, {
                    it.printStackTrace()
                }),
            null
        )
    )
}

/**     Single: Pair first file id, second base url
 * */
private fun FileApi.sendFile(filePath: String,
                             title: String? = null,
                             ext: String? = null,
                             size: Int? = null,
                             length: Int? = null): Single<Pair<String, String>> {
    return Single.create { emitter ->
        val param = FileUploadRequest(
            filePath.getName(),
            size ?: filePath.getSize() /1000,
            ext ?: filePath.getMimeType().toMediaTypeOrNull()?.subtype ?: throw IllegalArgumentException("Media type must not be null")
        )
        param.title = title
        param.duration = length
        val sendMessage = AnyInnerRequest(param)
        val data = AnyRequest(sendMessage)
        this.uploadFile(data) { event ->
            val dataResponse = event.data ?: return@uploadFile
            emitter.onSuccess(dataResponse.file_id to dataResponse.file_upload_to)
        }
    }
}

object FilesWorkRepository {

    const val CANCELED_STATUS = -1

    private val mUploadingStack = mutableListOf<UploadingRequestModel>()

    fun addNewUpload(model: UploadingRequestModel) {
        mUploadingStack.add(model)
    }

    fun cancelUpload(context: Context, filePath: String) {
        val uploadStackTmp = mutableListOf<UploadingRequestModel>()
        val deleteStackTmp = mutableListOf<UploadingRequestModel>()

        uploadStackTmp.addAll(mUploadingStack)

        for (model in uploadStackTmp) {
            if (filePath == model.filePath) {
                if (model.disposable.isDisposed.not())
                    model.disposable.dispose()
                else
                    model.workId?.let { WorkManager.getInstance(context).cancelWorkById(it) }

                deleteStackTmp.add(model)

                DialogWorkRepository.mSendingLiveData.value = DialogWorkRepository.SendingData(filePath, model.fileId, CANCELED_STATUS)
            }
        }

        mUploadingStack.removeAll(deleteStackTmp)
    }

    fun sendBody(context: Context,
                 file: UploadFileModel) {
        val mConstraints = Constraints.Builder()
            .setRequiredNetworkType(NetworkType.CONNECTED)
            .build()
        val request = OneTimeWorkRequestBuilder<UploadWorker>()
            .setConstraints(mConstraints)
            .setInputData(file.toData())
            .build()
        for (requestModel in mUploadingStack) {
            if (requestModel.filePath == file.filePath)
                requestModel.workId = request.id
        }
        WorkManager.getInstance(context).cancelAllWorkByTag("ExoCacheWorker")
        WorkManager.getInstance(context).enqueue(request)
    }

    data class UploadingRequestModel(val filePath: String,
                                     val fileId: String,
                                     val disposable: Disposable,
                                     var workId: UUID?)
}
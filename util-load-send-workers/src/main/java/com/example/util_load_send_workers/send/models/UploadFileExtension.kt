package com.example.util_load_send_workers.send.models

import androidx.work.Data
import com.example.util_load_send_workers.send.models.UploadFileModel.Companion.BASE_URL
import com.example.util_load_send_workers.send.models.UploadFileModel.Companion.FILE_ID
import com.example.util_load_send_workers.send.models.UploadFileModel.Companion.FILE_PATH
import com.example.util_load_send_workers.send.models.UploadFileModel.Companion.USER_ID

fun Data.toUploadFileModel(): UploadFileModel {
    return UploadFileModel(
        getString(FILE_PATH) ?: error("File path must not be null"),
        getString(FILE_ID) ?: error("File id must not be null"),
        getLong(USER_ID, 0),
        getString(BASE_URL) ?: error("Base url must not be null")
    )
}
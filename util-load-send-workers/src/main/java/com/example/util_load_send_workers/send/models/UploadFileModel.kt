package com.example.util_load_send_workers.send.models

import androidx.work.Data
import androidx.work.workDataOf

data class UploadFileModel(val filePath: String,
                           var fileId: String,
                           val userId: Long,
                           var baseUrl: String,
                           var title: String? = null,
                           var ext: String? = null,
                           var size: Int? = null,
                           var isLoaded: Boolean = false,
                           var length: Int? = null) {

    companion object{
        internal const val FILE_PATH = "file_path"
        internal const val FILE_ID = "com.morozov.android.sendy.FILE.ID"
        internal const val USER_ID = "com.morozov.android.sendy.USER.ID"
        internal const val BASE_URL = "com.morozov.android.sendy.BASE.URL"
    }

    fun toData(): Data {
        return workDataOf(
            FILE_PATH to filePath,
            FILE_ID to fileId,
            USER_ID to userId,
            BASE_URL to baseUrl
        )
    }
}
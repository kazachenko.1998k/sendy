package com.example.util_load_send_workers.send

import android.content.Context
import android.util.Log
import androidx.work.CoroutineWorker
import androidx.work.WorkerParameters
import com.example.util_load_send_workers.repository.DialogWorkRepository
import com.example.util_load_send_workers.retrofit.FileRetrofit
import com.example.util_load_send_workers.retrofit.UploadAPIs
import com.example.util_load_send_workers.retrofit.progress.ProgressRequestBody
import com.example.util_load_send_workers.send.models.UploadFileModel
import com.example.util_load_send_workers.send.models.toUploadFileModel
import com.example.util_load_send_workers.utils.getMimeType
import kotlinx.coroutines.*
import okhttp3.MediaType
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.MultipartBody
import okhttp3.ResponseBody
import retrofit2.Response
import java.io.File
import java.lang.Exception

internal class UploadWorker(context: Context, parameters: WorkerParameters)
    : CoroutineWorker(context, parameters) {

    override suspend fun doWork(): Result {
        val inputDataParsed = inputData.toUploadFileModel()
        return try {
            val response = uploadMediaToServerWithProgressAsync(inputDataParsed).await()
            Log.i("UploadWorker", "Responce: ${response.isSuccessful}")
            Log.i("UploadWorker", "Responce code: ${response.code()}")
            if (response.isSuccessful)
                Result.success()
            else
                Result.failure()
        } catch (e: Exception) {
            CoroutineScope(Dispatchers.Main).launch {
                DialogWorkRepository.mSendingLiveData.value = DialogWorkRepository.SendingData(inputDataParsed.filePath, inputDataParsed.fileId, DialogWorkRepository.LOAD_FAILED)
            }
            e.printStackTrace()
            Result.failure()
        }
    }

    private suspend fun uploadMediaToServerWithProgressAsync(uploadFileModel: UploadFileModel): Deferred<Response<ResponseBody>> {
        val uploadApi = FileRetrofit.getRetrofit(uploadFileModel.baseUrl).create(UploadAPIs::class.java)
        val file = File(uploadFileModel.filePath)
        val fileReqBody =
            object : ProgressRequestBody(applicationContext, file, ProgressCallback(uploadFileModel.filePath, uploadFileModel.fileId)) {
                override fun contentType(): MediaType? {
                    return uploadFileModel.filePath.getMimeType().toMediaTypeOrNull()
                }
            }
        val part = MultipartBody.Part.createFormData("upload", file.name, fileReqBody)
        return withContext(Dispatchers.IO) {
            async {
                uploadApi.uploadFileAsync(part, uploadFileModel.fileId, uploadFileModel.userId)
            }
        }
    }

    private inner class ProgressCallback(private val filePath: String,
                                         private val fileId: String): ProgressRequestBody.UploadCallbacks {
        override fun onProgressUpdate(percentage: Int) {
            DialogWorkRepository.mSendingLiveData.value = DialogWorkRepository.SendingData(filePath, fileId, percentage)
        }

        override fun onError() {
            DialogWorkRepository.mSendingLiveData.value = DialogWorkRepository.SendingData(filePath, fileId, DialogWorkRepository.LOAD_FAILED)
        }

        override fun onFinish() {
            DialogWorkRepository.mSendingLiveData.value = DialogWorkRepository.SendingData(filePath, fileId, 100)
        }

        override fun uploadStart() {
        }
    }
}
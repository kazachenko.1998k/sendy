package com.example.util_load_send_workers.retrofit

import okhttp3.MultipartBody
import okhttp3.ResponseBody
import retrofit2.Response
import retrofit2.http.Header
import retrofit2.http.Multipart
import retrofit2.http.POST
import retrofit2.http.Part

internal interface UploadAPIs {
    @Multipart
    @POST("api/v1/uploadFile/")
    suspend fun uploadFileAsync(
        @Part file: MultipartBody.Part,
        @Header("X-Content-Id") contentId: String,
        @Header("X-User-Id") userId: Long
    ): Response<ResponseBody>
}
package com.example.util_load_send_workers.retrofit.progress

import android.content.Context
import android.os.Handler
import android.os.Looper
import com.example.util_load_send_workers.utils.getStreamByteFromImage
import okhttp3.RequestBody
import okio.BufferedSink
import java.io.File
import java.io.FileInputStream
import java.io.IOException

abstract class ProgressRequestBody(
    context: Context,
    private var mFile: File,
    private val mListener: UploadCallbacks
) : RequestBody() {

    init {
        mFile = mFile.getStreamByteFromImage(context) ?: mFile
    }

    interface UploadCallbacks {
        fun onProgressUpdate(percentage: Int)
        fun onError()
        fun onFinish()
        fun uploadStart()
    }

    @Throws(IOException::class)
    override fun contentLength(): Long {
        return mFile.length()
    }

    @Throws(IOException::class)
    override fun writeTo(sink: BufferedSink) {
        val fileLength = mFile.length()
        val buffer = ByteArray(DEFAULT_BUFFER_SIZE)
        val inputStream = FileInputStream(mFile)
        var uploaded: Long = 0
        inputStream.use { inputStrm ->
            var read: Int
            val handler = Handler(Looper.getMainLooper())
            while (inputStrm.read(buffer).also { read = it } != -1) {
                if (read != 0) {
                    uploaded += read.toLong()
                    sink.write(buffer, 0, read)
                    handler.post(
                        ProgressUpdater(
                            uploaded,
                            fileLength
                        )
                    )
                }
            }
        }
    }

    private inner class ProgressUpdater(private val mUploaded: Long, private val mTotal: Long) :
        Runnable {
        override fun run() {
            try {
                val progress = (100 * mUploaded / mTotal).toInt()
                if (progress == 100) mListener.onFinish() else mListener.onProgressUpdate(progress)
            } catch (e: ArithmeticException) {
                mListener.onError()
                e.printStackTrace()
            }
        }

    }

    companion object {
        private const val DEFAULT_BUFFER_SIZE = 2048
    }

    init {
        mListener.uploadStart()
    }
}
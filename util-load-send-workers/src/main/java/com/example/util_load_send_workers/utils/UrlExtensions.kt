package com.example.util_load_send_workers.utils

import android.webkit.MimeTypeMap
import java.io.File
import java.lang.IllegalArgumentException

fun String.getMimeType(): String {
    var type: String? = null
    val extension = MimeTypeMap.getFileExtensionFromUrl(this)
    if (extension != null) {
        type = MimeTypeMap.getSingleton().getMimeTypeFromExtension(extension)
    }
    return type ?: throw IllegalArgumentException("Wrong url, no mime type")
}

fun String.getName(): String {
    return this.substring(this.lastIndexOf("/")+1)
}

fun String.getSize(): Int {
    return (File(this).length()/1024).toString().toInt()
}

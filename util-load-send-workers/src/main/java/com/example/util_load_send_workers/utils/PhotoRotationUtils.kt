package com.example.util_load_send_workers.utils

import android.content.Context
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.Matrix
import android.media.ExifInterface
import java.io.ByteArrayOutputStream
import java.io.File
import java.io.FileOutputStream
import java.io.IOException

fun File.getStreamByteFromImage(context: Context): File? {
    var photoBitmap = BitmapFactory.decodeFile(this.path) ?: return null
    val newFile = File.createTempFile("prefix", "extension", context.cacheDir)
    val stream = FileOutputStream(newFile)
    val imageRotation = this.getImageRotation()
    if (imageRotation != 0f) photoBitmap = getBitmapRotatedByDegree(photoBitmap, imageRotation)
    photoBitmap.compress(Bitmap.CompressFormat.JPEG, 70, stream)
    return newFile
}

private fun File.getImageRotation(): Float {
    var exif: ExifInterface? = null
    var exifRotation = 0
    try {
        exif = ExifInterface(this.path)
        exifRotation =
            exif.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_NORMAL)
    } catch (e: IOException) {
        e.printStackTrace()
    }
    return if (exif == null) 0f else exifToDegrees(exifRotation)
}

private fun exifToDegrees(rotation: Int): Float {
    if (rotation == ExifInterface.ORIENTATION_ROTATE_90) return 90f else if (rotation == ExifInterface.ORIENTATION_ROTATE_180) return 180f else if (rotation == ExifInterface.ORIENTATION_ROTATE_270) return 270f
    return 0f
}

private fun getBitmapRotatedByDegree(bitmap: Bitmap, rotationDegree: Float): Bitmap {
    val matrix = Matrix()
    matrix.preRotate(rotationDegree)
    return Bitmap.createBitmap(bitmap, 0, 0, bitmap.width, bitmap.height, matrix, true)
}
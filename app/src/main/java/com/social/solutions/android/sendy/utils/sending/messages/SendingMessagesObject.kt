package com.social.solutions.android.sendy.utils.sending.messages

import com.morozov.core_backend_api.AnyInnerRequest
import com.morozov.core_backend_api.AnyRequest
import com.morozov.core_backend_api.message.MessageApi
import com.morozov.core_backend_api.message.requests.MessageSendRequest
import com.morozov.feature_chat_api.models.api.MessageApiModel
import com.morozov.feature_chat_api.models.api.PhotoApiModel
import com.morozov.feature_chat_api.models.api.VideoApiModel
import com.morozov.feature_chat_api.models.api.VoiceApiModel
import com.morozov.feature_chat_api.models.callback.MessageCallbackModel
import com.morozov.feature_chat_api.models.callback.PhotoCallbackModel
import com.morozov.feature_chat_api.models.callback.VideoCallbackModel
import com.morozov.feature_chat_api.models.callback.VoiceCallbackModel

object SendingMessagesObject {
    /**     First item: Long -- recipient id
     * */
    private val mSendingMessages by lazy { mutableListOf<Pair<Long, MessageCallbackModel>>() }

    fun getByRecipientId(recipientId: Long): List<MessageCallbackModel> {
        return mSendingMessages.mapNotNull {
            if (it.first == recipientId)
                it.second
            else
                null
        }
    }

    fun addMessage(recipientId: Long, message: MessageApiModel) {
        mSendingMessages.add(
            Pair(
                recipientId,
                MessageCallbackModel(
                    message.media.mapNotNull {
                        when(it) {
                            is PhotoApiModel -> PhotoCallbackModel(it.filePath, 0)
                            is VideoApiModel -> VideoCallbackModel(it.filePath, it.sec, 0)
                            is VoiceApiModel -> VoiceCallbackModel(it.filePath, it.sec, 0)
                            else -> null
                        }
                    }.toMutableList(),
                    message.msg
                )
            )
        )
    }

    fun setLoaded(fileLoadedId: String, filePath: String, messageApi: MessageApi, chatType: Int) {
        for (pair in mSendingMessages) {
            if (setLoaded(
                    fileLoadedId,
                    filePath,
                    pair.second
                )
            ) {
                if (checkIsAllLoaded(
                        pair.second
                    )
                ) {
                    sendMessage(
                        pair.first,
                        pair.second,
                        messageApi,
                        chatType
                    )
                    mSendingMessages.remove(pair)
                }
                break
            }
        }
    }

    fun cancelLoading(filePath: String) {
        for (pair in mSendingMessages) {
            cancelLoading(
                filePath,
                pair
            )
        }
    }

    private fun cancelLoading(filePath: String, model: Pair<Long, MessageCallbackModel>) {
        for (media in model.second.media) {
            if (media.filePath == filePath) {
                model.second.media.remove(media)
                break
            }
        }
        if (model.second.media.isEmpty())
            mSendingMessages.remove(model)
    }

    private fun setLoaded(fileLoadedId: String, filePath: String, model: MessageCallbackModel): Boolean {
        var result = false
        for (media in model.media) {
            if (media.filePath == filePath) {
                media.loadProgress = 100
                media.loadedMediaId = fileLoadedId
                result = true
                break
            }
        }
        return result
    }

    private fun checkIsAllLoaded(model: MessageCallbackModel): Boolean {
        var result = true
        for (media in model.media) {
            if (media.loadProgress != 100) {
                result = false
                break
            }
        }
        return result
    }

    private fun sendMessage(recipientId: Long, model: MessageCallbackModel, messageApi: MessageApi, chatType: Int) {
        val param = MessageSendRequest()
        param.chat_id = recipientId
        param.message = model.msg
        param.file_ids = model.media.map { it.loadedMediaId!! }.toMutableList()
        param.type = chatType
        val sendMessage = AnyInnerRequest(param)
        val data = AnyRequest(sendMessage)
        messageApi.send(data) {
        }
    }
}
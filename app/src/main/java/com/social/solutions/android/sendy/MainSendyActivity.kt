package com.social.solutions.android.sendy

import android.Manifest
import android.annotation.SuppressLint
import android.content.*
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.provider.Settings
import android.util.Base64
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.fragment.app.FragmentManager
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import androidx.navigation.fragment.NavHostFragment
import com.example.feature_bottom_picker_api.BottomPickerStarterApi
import com.example.feature_bottom_picker_api.FeatureBottomPickerCallback
import com.example.feature_bottom_picker_api.models.PhotoModel
import com.example.feature_bottom_picker_api.models.StarterModel
import com.example.feature_bottom_picker_api.models.VideoModel
import com.example.feature_bottom_picker_impl.start.BottomPickerStarterImpl
import com.example.feature_bottom_picker_impl.ui.fragments.BottomPickerFragment
import com.example.feature_list_messages_impl.ListMessagesStarterImpl
import com.example.feature_list_profile_impl.ProfileStarterImpl
import com.example.feature_list_profile_impl.editFragments.FragmentEditGroupOrChannel
import com.example.feature_list_profile_impl.editFragments.FragmentEditProfile
import com.example.feature_list_profile_impl.editFragments.FragmentListSubscribersGroup
import com.example.feature_profile_settings_api.FeatureProfileSettingsCallback
import com.example.feature_profile_settings_api.ProfileSettingsStarterApi
import com.example.feature_profile_settings_impl.start.ProfileSettingsStarterImpl
import com.example.feature_profile_settings_impl.ui.fragments.BaseFragment
import com.example.util_cache.AppDatabase
import com.example.util_cache.Repository
import com.github.vivchar.rendererrecyclerviewadapter.BuildConfig
import com.google.android.material.bottomnavigation.BottomNavigationView
import com.google.firebase.iid.FirebaseInstanceId
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.morozov.core_backend_api.AnyInnerRequest
import com.morozov.core_backend_api.AnyRequest
import com.morozov.core_backend_api.FeatureBackendApi
import com.morozov.core_backend_api.SignData
import com.morozov.core_backend_api.SignData.updateSign
import com.morozov.core_backend_api.auth.AuthApiREST
import com.morozov.core_backend_api.auth.modelsRequest.AuthCloseSessionsRequest
import com.morozov.core_backend_api.auth.modelsRequest.AuthInitRequest
import com.morozov.core_backend_api.auth.modelsRequest.AuthReq
import com.morozov.core_backend_api.auth.modelsRequest.AuthSetTokenRequest
import com.morozov.core_backend_api.auth.modelsResponse.InitResponse
import com.morozov.core_backend_api.chat.Chat
import com.morozov.core_backend_api.chat.Chat.Companion.CLOSE_CHAT
import com.morozov.core_backend_api.message.Message
import com.morozov.core_backend_api.user.User
import com.morozov.core_backend_api.user.UserMini
import com.morozov.core_backend_impl.network.NetworkModule
import com.morozov.feature_chat_api.ChatFeatureApi
import com.morozov.feature_chat_api.FeatureChatCallback
import com.morozov.feature_chat_api.models.api.MessageApiModel
import com.morozov.feature_chat_api.models.callback.MessageCallbackModel
import com.morozov.feature_chat_impl.ChatFeatureImpl
import com.morozov.feature_chat_impl.start.ChatStarterImpl
import com.morozov.feature_chat_impl.ui.fragments.chat.ChatFragment
import com.morozov.lib_backend.LibBackendDependency
import com.social.solution.feature_auth_api.AuthorizationFeatureApi
import com.social.solution.feature_auth_api.FeatureAuthorizationCallback
import com.social.solution.feature_create_chat.LibCreateChatDependency
import com.social.solution.feature_create_chat.ui.fragments.AddContactFragment
import com.social.solution.feature_create_chat_api.FeatureAddContactCallback
import com.social.solution.feature_create_chat_api.FeatureCreateChatApi
import com.social.solution.feature_create_chat_api.FeatureCreateChatCallback
import com.social.solution.feature_create_community_api.FeatureCreateChannelCallback
import com.social.solution.feature_create_community_api.FeatureCreateGroupApi
import com.social.solution.feature_create_community_api.FeatureCreateGroupCallback
import com.social.solution.feature_create_community_api.FeatureSettingGroupCallback
import com.social.solution.feature_create_community_impl.LibCreateCommunityDependency
import com.social.solution.feature_create_community_impl.ui.fragments.channel.CreateChannelFragment
import com.social.solution.feature_create_community_impl.ui.fragments.channel.CreateChannelSettingsFragment
import com.social.solution.feature_create_community_impl.ui.fragments.group.CreateGroupFragment
import com.social.solution.feature_create_community_impl.ui.fragments.group.CreateGroupSettingsFragment
import com.social.solution.feature_create_post_api.FeatureCreatePostApi
import com.social.solution.feature_create_post_api.FeatureCreatePostCallback
import com.social.solution.feature_create_post_impl.LibCreatePostDependency
import com.social.solution.feature_create_post_impl.ui.fragment.CreatePostFragment
import com.social.solution.feature_feed_api.FeatureFeedApi
import com.social.solution.feature_feed_api.FeatureFeedCallback
import com.social.solution.feature_feed_api.FeatureFeedStarter
import com.social.solution.feature_feed_api.models.FeedItemPostUI
import com.social.solution.feature_feed_impl.LibFeedDependency
import com.social.solution.feature_list_messages_api.FeatureListMessagesCallback
import com.social.solution.feature_list_messages_api.ListMessagesStarter
import com.social.solution.feature_post_api.FeaturePostCallback
import com.social.solution.feature_post_impl.LibPostDependency
import com.social.solution.feature_post_impl.ui.BottomBarWriteFragment
import com.social.solution.feature_post_impl.ui.PostFragment
import com.social.solution.feature_search_api.FeatureSearchApi
import com.social.solution.feature_search_api.FeatureSearchCallback
import com.social.solution.feature_search_api.models.*
import com.social.solution.feature_search_impl.LibSearchDependency
import com.social.solution.feature_search_impl.ui.fragments.SearchFragment
import com.social.solution.lib_auth.LibAuthDependency
import com.social.solutions.android.feature_registration_api.FeatureRegistrationCallback
import com.social.solutions.android.feature_registration_api.RegistrationFeatureApi
import com.social.solutions.android.feature_registration_impl.RegistrationFeatureImpl
import com.social.solutions.android.feature_registration_impl.start.RegistrationStarterImpl
import com.social.solutions.android.feature_registration_impl.ui.fragments.registration.RegistrationFragment
import com.social.solutions.android.sendy.callbacks.profile.ProfileCallback
import com.social.solutions.android.sendy.converters.convertFrom
import com.social.solutions.android.sendy.push.PushController
import com.social.solutions.android.sendy.utils.connection.initConnectionStateView
import com.social.solutions.android.sendy.utils.sending.messages.SendingMessagesObject
import com.social.solutions.android.util_load_send_services.boundaries.BoundariesObject
import com.social.solutions.android.util_load_send_services.boundaries.OnLoadedCallback
import com.social.solutions.util_network_check.network.NetworkChangeReceiver
import com.social.solutions.util_network_check.network.OnNetworkStateCallback
import com.tinder.scarlet.Lifecycle
import com.tinder.scarlet.ShutdownReason
import com.tinder.scarlet.lifecycle.LifecycleRegistry
import com.vanniktech.emoji.EmojiManager
import com.vanniktech.emoji.ios.IosEmojiProvider
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import java.io.File
import java.lang.reflect.Type
import kotlin.concurrent.thread

class MainSendyActivity : AppCompatActivity() {

    companion object {
        const val REQUEST_CONTACT = 1000
        const val REQUEST_CAMERA_GROUP = 10011
        const val REQUEST_CAMERA_CHANNEL = 10012
        private const val USER_UID_EMPTY = 0L
        private var connectionLifecycleRegistry = LifecycleRegistry(0L)
    }

    private lateinit var featureBackendApi: FeatureBackendApi
    private var liveDataBackendApi = MutableLiveData<FeatureBackendApi?>()

    var dataBase: AppDatabase = AppDatabase.getAppDataBase(this)
    private val pushController = PushController(this, supportFragmentManager)

    override fun onCreate(savedInstanceState: Bundle?) {
        setTheme(R.style.AppTheme)
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        LibBackendDependency.lifecycleRegistry = connectionLifecycleRegistry
        liveDataBackendApi = LibBackendDependency.liveDataBackEndApi
        EmojiManager.install(IosEmojiProvider())

        viewConnectionState.initConnectionStateView(this, mNetworkState, SignData.connectionState)

        initListenerOnChangeFragment()

        addNetworkReceiver()

        initLoadServiceCallback()

        initConnection()

    }

    override fun onDestroy() {
        removeNetworkReceiver()
        super.onDestroy()
    }

    var connect = false
    var scanning = false
    override fun onResume() {
        super.onResume()
        thread {
            if (!scanning && SignData.server_file != "" && mNetworkState.value == true) {
                scanning = true
                connect = false
                var countReq = 0L
                while (!connect && countReq < 5L) {
                    LibBackendDependency.featureBackendApi().infoApi().time {
                        scanning = false
                        connect = true
                    }
                    countReq++
                    Thread.sleep(500 * countReq)
                }
            }
        }
    }

    /**     Network state
     * */

    private val mNetworkState = MutableLiveData<Boolean>()

    // Receivers
    private var networkReceiver: BroadcastReceiver? = null

    private fun addNetworkReceiver() {
        networkReceiver = NetworkChangeReceiver.create(listOf(object : OnNetworkStateCallback {
            override fun onInternetConnected() {
                mNetworkState.postValue(true)
            }

            override fun onInternetDisconnected() {
                mNetworkState.postValue(false)
            }
        }))
        val networkFilter = IntentFilter().apply {
            addAction("android.net.conn.CONNECTIVITY_CHANGE")
        }

        registerReceiver(networkReceiver, networkFilter)
    }

    private fun removeNetworkReceiver() {
        unregisterReceiver(networkReceiver)
    }

    /**     Authorization and registration epic
     * */

    private val featureAuthApi: AuthorizationFeatureApi by lazy {
        LibAuthDependency.featureAuthApi()
    }

    private val featureRegistrationApi: RegistrationFeatureApi by lazy {
        RegistrationFeatureImpl(RegistrationStarterImpl())
    }

    private fun startAuthFeature() {
        featureAuthApi.authorizationStarter().start(
            supportFragmentManager,
            R.id.main_container,
            true,
            authCallback,
            mNetworkState,
            liveDataBackendApi
        )
    }

    private fun startRegistrationFeature() {
        supportFragmentManager.clearBackStack()
        val sharedPreferences = getMySharedPreferences()
        sharedPreferences
            .edit()
            .remove(getString(R.string.user_uid))
            .apply()
        featureRegistrationApi.starter().start(
            supportFragmentManager,
            R.id.main_container,
            true,
            registrationCallback,
            featureBottomPickerApi,
            mNetworkState,
            featureBackendApi
        )
    }

    private val authCallback: FeatureAuthorizationCallback by lazy {
        object : FeatureAuthorizationCallback {
            override fun onSignedAnonym(userUID: Long) {
                setUidSP(userUID)
                setUid(userUID)
                updateLibBackendDependency()
                startFirstEpic()
            }


            override fun onAuthorized(userUID: Long, isNeedNick: Boolean) {
                setUidSP(userUID)
                setUid(userUID)
                updateLibBackendDependency()
                if (isNeedNick) {
                    startRegistrationFeature()
                } else {
                    startFirstEpic()
                }
            }

            override fun onExit() {
                // Close session
            }
        }
    }

    private val registrationCallback: FeatureRegistrationCallback by lazy {
        object : FeatureRegistrationCallback {
            override fun onRegistered(uid: Long) {
                setUidSP(uid)
                setUid(uid)
                updateLibBackendDependency()
                startFirstEpic()
            }

            override fun onDestroy() {
                logOutAndCloseConnection()
            }
        }
    }

    /**
     *      Chat epic
     * */

    private val featureChatListStarter: ListMessagesStarter by lazy {
        ListMessagesStarterImpl()
    }

    private val featureChatApi: ChatFeatureApi by lazy {
        ChatFeatureImpl(ChatStarterImpl())
    }

    private val featureSearchApi: FeatureSearchApi by lazy {
        LibSearchDependency.featureSearchApi(applicationContext, featureBackendApi)
    }

    private val featureCreateChatApi: FeatureCreateChatApi by lazy {
        LibCreateChatDependency.featureCreateChatApi(applicationContext, featureBackendApi)
    }

    val featureCreateCommunityApi: FeatureCreateGroupApi by lazy {
        LibCreateCommunityDependency.featureCreateGroupApi(applicationContext, featureBackendApi)
    }

    val featureBottomPickerApi: BottomPickerStarterApi by lazy {
        BottomPickerStarterImpl()
    }

    fun startListChatsFeature(message: Message? = null) {
//        Log.d("TAG_CHECK", "I1")
//        supportFragmentManager.clearBackStack()
        featureChatListStarter.startDefault(
            supportFragmentManager,
            R.id.main_container,
            false,
            listChatCallback,
            message
        )
    }

    private fun startChatFeature(chatId: Long) {
//        contentBottomNav.visibility = View.GONE
        featureChatApi.chatStarter().startChat(
            getUidSP(),
            supportFragmentManager,
            R.id.main_container,
            featureBackendApi,
            dataBase.messageDao(),
            featureBottomPickerApi,
            true,
            chatFeatureCallback,
            mNetworkState,
            chatId
        )
    }

    private fun startChatFeature(chatModel: Chat) {
//        contentBottomNav.visibility = View.GONE
        featureChatApi.chatStarter().startChat(
            getUidSP(),
            supportFragmentManager,
            R.id.main_container,
            featureBackendApi,
            dataBase.messageDao(),
            featureBottomPickerApi,
            true,
            chatFeatureCallback,
            mNetworkState,
            chatModel
        )
    }

    fun startChatWithUserFeature(uid: Long) {
//        contentBottomNav.visibility = View.GONE
        featureChatApi.chatStarter().startChatWithUser(
            getUidSP(),
            supportFragmentManager,
            R.id.main_container,
            featureBackendApi,
            dataBase.messageDao(),
            featureBottomPickerApi,
            true,
            chatFeatureCallback,
            mNetworkState,
            uid
        )
    }

    fun startSearchFeature(
        config: ConfigSearch,
        callback: FeatureSearchCallback
    ) {
        featureSearchApi.searchStarter().start(
            supportFragmentManager,
            R.id.main_container,
            true,
            config,
            callback,
            dataBase.userDao(),
            dataBase.chatDao(),
            featureBackendApi,
            mNetworkState
        )
    }

    private fun startCreateChatFeature() {
        featureCreateChatApi.createChatStarter().start(
            supportFragmentManager,
            R.id.main_container,
            true,
            featureCreateChatCallback,
            featureBackendApi.userApi(),
            dataBase.userDao(),
            mNetworkState
        )
    }

    private fun startCreateChannelFeature() {
        featureCreateCommunityApi.createGroupStarter().startCreateChannel(
            supportFragmentManager,
            R.id.main_container,
            false,
            featureBackendApi,
            featureCreateChanelCallback,
            dataBase.chatDao(),
            getUid(),
            mNetworkState
        )
    }

    private fun startCreateGroupFeature() {
        featureCreateCommunityApi.createGroupStarter().startCreateGroup(
            supportFragmentManager,
            R.id.main_container,
            false,
            featureBackendApi,
            featureCreateGroupCallback,
            dataBase.chatDao(),
            getUid(),
            mNetworkState
        )
    }

    private fun startCreateContactFeature() {
        featureCreateChatApi.createChatStarter().startAddContact(
            supportFragmentManager,
            R.id.main_container,
            true,
            featureCreateContactCallback,
            featureBackendApi.userApi(),
            dataBase.userDao(),
            mNetworkState
        )
    }

    private val featureCreateContactCallback by lazy {
        object : FeatureAddContactCallback {
            override fun openChatP2P(user: UserMini) {
                supportFragmentManager.popBackStack()
                supportFragmentManager.popBackStack()
                startChatWithUserFeature(user.uid)
            }
        }
    }

    private val featureCreateChanelCallback by lazy {
        object : FeatureCreateChannelCallback {

            override fun onAvatarChangeClickListener(
                function: (url: String?) -> Unit
            ) {
                val configs = StarterModel()
                configs.fullScreen = true
                configs.showDocuments = false
                configs.addComment = false
                configs.showVideo = false
                configs.isSinglePick = true
                featureBottomPickerApi.start(
                    supportFragmentManager,
                    R.id.main_container,
                    configs,
                    initProfileBottomPickerCallback(function)
                )
            }

            override fun addUser() {
                val config = ConfigSearch(
                    "Добавить участников",
                    "Создать",
                    false,
                    maxCountSearch = true,
                    chat = null,
                    targets = listOf(SearchTarget.LOCAL_USER),
                    multiSelection = true,
                    searchView = false,
                    inGroupSearch = false,
                    filteredMe = true
                )
                startSearchFeature(
                    config,
                    featureSearchCreateChannelCallback
                )
            }

            override fun openChat(chat: Chat) {
                supportFragmentManager.clearBackStack()
                startChatFeature(chat)
            }

            override fun getPermissionChannelCamera(): Boolean {
                return this@MainSendyActivity.getPermissionCamera(REQUEST_CAMERA_CHANNEL)
            }
        }
    }

    private val featureCreateGroupCallback by lazy {
        object : FeatureCreateGroupCallback {

            override fun onAvatarChangeClickListener(
                function: (url: String?) -> Unit
            ) {
                val configs = StarterModel()
                configs.fullScreen = true
                configs.showDocuments = false
                configs.addComment = false
                configs.showVideo = false
                configs.isSinglePick = true
                featureBottomPickerApi.start(
                    supportFragmentManager,
                    R.id.main_container,
                    configs,
                    initProfileBottomPickerCallback(function)
                )
            }

            override fun addUser() {
                val config = ConfigSearch(
                    "Добавить участников",
                    "Создать",
                    false,
                    maxCountSearch = true,
                    chat = null,
                    targets = listOf(SearchTarget.LOCAL_USER),
                    multiSelection = true,
                    searchView = false,
                    inGroupSearch = false,
                    filteredMe = true
                )
                startSearchFeature(
                    config,
                    featureSearchCreateGroupCallback
                )
            }

            override fun openChat(chat: Chat) {
                supportFragmentManager.clearBackStack()
                startChatFeature(chat)
            }

            override fun getPermissionGroupCamera(): Boolean {
                return this@MainSendyActivity.getPermissionCamera(REQUEST_CAMERA_GROUP)
            }
        }
    }

    private val featureSettingGroupCallback by lazy {
        object : FeatureSettingGroupCallback {
            override fun openGroup(chatNew: Chat) {
                Toast.makeText(baseContext, "Open group Profile", Toast.LENGTH_SHORT).show()
            }

            override fun shareLink(text: String) {
                val sendIntent = Intent()
                sendIntent.action = Intent.ACTION_SEND
                sendIntent.putExtra(Intent.EXTRA_TEXT, "Ссылка на группу: $text")
                sendIntent.type = "text/plain"
                startActivity(sendIntent)
            }
        }
    }

    private val featureCreateChatCallback by lazy {
        object : FeatureCreateChatCallback {
            override fun openChatP2P(user: UserMini) {
                startChatWithUserFeature(user.uid.toLong())
            }

            override fun createChannel() {
                startCreateChannelFeature()
            }

            override fun createGroup() {
                startCreateGroupFeature()
            }

            override fun shareApp() {
                openShareApp()
            }

            override fun createContact() {
                startCreateContactFeature()
            }

            override fun searchChats() {
                val config = ConfigSearch(
                    null,
                    null,
                    false,
                    maxCountSearch = true,
                    chat = null,
                    targets = listOf(SearchTarget.GLOBAL_USER, SearchTarget.LOCAL_USER),
                    multiSelection = false,
                    searchView = true,
                    inGroupSearch = false,
                    filteredMe = true
                )
                startSearchFeature(
                    config,
                    featureSearchCreateChatCallback
                )
            }

            override fun getPermissionContact(): Boolean {
                return this@MainSendyActivity.getPermissionContact()
            }

            override fun checkMyPhone(cleanNumber: String): Boolean {
                //todo: check phone
                return false
            }
        }
    }

    private val featureSearchCreateGroupCallback by lazy {
        object : FeatureSearchCallback {
            override fun multiSelectUser(selectedUserList: List<SelectingItem>) {
                val selectedUserForCreatingGroup = selectedUserList.map {
                    if (it is SearchLocalUserUI) convertFrom(it)
                    else convertFrom(it as SearchGlobalUserUI)
                }.toMutableList()
                featureCreateCommunityApi.createGroupStarter()
                    .selectUsersGroup(selectedUserForCreatingGroup)
            }

            override fun selectLocalUser(searchUserUIList: SearchLocalUserUI) {
                startProfileFeature(
                    Chat(
                        searchUserUIList.id,
                        Chat.PERSONAL_DIALOG,
                        Chat.PRIVATE_CHAT,
                        searchUserUIList.id,
                        searchUserUIList.id
                    )
                )
                Toast.makeText(baseContext, "Select local user", Toast.LENGTH_SHORT).show()
            }

            override fun selectGlobalUser(searchUserUIList: SearchGlobalUserUI) {
                startProfileFeature(
                    Chat(
                        searchUserUIList.id,
                        Chat.PERSONAL_DIALOG,
                        Chat.PUBLIC_CHAT,
                        searchUserUIList.id,
                        searchUserUIList.id
                    )
                )
                Toast.makeText(baseContext, "Select global user", Toast.LENGTH_SHORT).show()
            }

            override fun selectLocalChannel(searchUserUIList: SearchLocalChannelUI) {
                startProfileFeature(
                    Chat(
                        searchUserUIList.id,
                        Chat.CHANNEL,
                        Chat.PRIVATE_CHANNELS,
                        searchUserUIList.id
                    )
                )
                Toast.makeText(baseContext, "Select local channel", Toast.LENGTH_SHORT).show()
            }

            override fun selectGlobalChannel(searchUserUIList: SearchGlobalChannelUI) {
                startProfileFeature(
                    Chat(
                        searchUserUIList.id,
                        Chat.CHANNEL,
                        Chat.PUBLIC_CHAT,
                        searchUserUIList.id
                    )
                )
                Toast.makeText(baseContext, "Select global channel", Toast.LENGTH_SHORT).show()
            }

            override fun selectLocalGroup(searchUserUIList: SearchLocalGroupUI) {
                startProfileFeature(
                    Chat(
                        searchUserUIList.id,
                        Chat.GROUP_CHAT,
                        Chat.PRIVATE_CHANNELS,
                        searchUserUIList.id
                    )
                )
                Toast.makeText(baseContext, "Select local group", Toast.LENGTH_SHORT).show()
            }

            override fun selectGlobalGroup(searchUserUIList: SearchGlobalGroupUI) {
                startProfileFeature(
                    Chat(
                        searchUserUIList.id,
                        Chat.GROUP_CHAT,
                        Chat.PUBLIC_CHAT,
                        searchUserUIList.id
                    )
                )
                Toast.makeText(baseContext, "Select global group", Toast.LENGTH_SHORT).show()
            }

            override fun selectUserMessage(searchUserUIList: SearchUserMessageUI) {
                Toast.makeText(baseContext, "Select message user", Toast.LENGTH_SHORT).show()
            }

            override fun selectChannelMessage(searchUserUIList: SearchChannelMessageUI) {
                Toast.makeText(baseContext, "Select message channel", Toast.LENGTH_SHORT).show()
            }

            override fun selectGroupMessage(searchUserUIList: SearchGroupMessageUI) {
                Toast.makeText(baseContext, "Select message group", Toast.LENGTH_SHORT).show()
            }

            override fun addParticipant() {
                Toast.makeText(baseContext, "Add participant", Toast.LENGTH_SHORT).show()
            }
        }
    }

    private val featureSearchCreateChatCallback by lazy {
        object : FeatureSearchCallback {
            override fun multiSelectUser(selectedUserList: List<SelectingItem>) {
                Toast.makeText(baseContext, "Select users", Toast.LENGTH_SHORT).show()
            }

            override fun selectLocalUser(searchUserUIList: SearchLocalUserUI) {
                startChatWithUserFeature(searchUserUIList.id)
            }

            override fun selectGlobalUser(searchUserUIList: SearchGlobalUserUI) {
                startChatWithUserFeature(searchUserUIList.id)
            }

            override fun selectLocalChannel(searchUserUIList: SearchLocalChannelUI) {
                Toast.makeText(baseContext, "Select local channel", Toast.LENGTH_SHORT).show()
            }

            override fun selectGlobalChannel(searchUserUIList: SearchGlobalChannelUI) {
                Toast.makeText(baseContext, "Select global channel", Toast.LENGTH_SHORT).show()
            }

            override fun selectLocalGroup(searchUserUIList: SearchLocalGroupUI) {
                Toast.makeText(baseContext, "Select local group", Toast.LENGTH_SHORT).show()
            }

            override fun selectGlobalGroup(searchUserUIList: SearchGlobalGroupUI) {
                Toast.makeText(baseContext, "Select global group", Toast.LENGTH_SHORT).show()
            }

            override fun selectUserMessage(searchUserUIList: SearchUserMessageUI) {
                Toast.makeText(baseContext, "Select message user", Toast.LENGTH_SHORT).show()
            }

            override fun selectChannelMessage(searchUserUIList: SearchChannelMessageUI) {
                Toast.makeText(baseContext, "Select message channel", Toast.LENGTH_SHORT).show()
            }

            override fun selectGroupMessage(searchUserUIList: SearchGroupMessageUI) {
                Toast.makeText(baseContext, "Select message group", Toast.LENGTH_SHORT).show()
            }

            override fun addParticipant() {
                Toast.makeText(baseContext, "Add participant", Toast.LENGTH_SHORT).show()
            }
        }
    }

    private val featureSearchCreateChannelCallback by lazy {
        object : FeatureSearchCallback {
            override fun multiSelectUser(selectedUserList: List<SelectingItem>) {
                val selectedUserForCreatingChannel = selectedUserList.map {
                    if (it is SearchLocalUserUI) convertFrom(it)
                    else convertFrom(it as SearchGlobalUserUI)
                }.toMutableList()
                featureCreateCommunityApi.createGroupStarter()
                    .selectUsersChannel(selectedUserForCreatingChannel)
            }

            override fun selectLocalUser(searchUserUIList: SearchLocalUserUI) {
                Toast.makeText(baseContext, "Select local user", Toast.LENGTH_SHORT).show()
            }

            override fun selectGlobalUser(searchUserUIList: SearchGlobalUserUI) {
                Toast.makeText(baseContext, "Select global user", Toast.LENGTH_SHORT).show()
            }

            override fun selectLocalChannel(searchUserUIList: SearchLocalChannelUI) {
                Toast.makeText(baseContext, "Select local channel", Toast.LENGTH_SHORT).show()
            }

            override fun selectGlobalChannel(searchUserUIList: SearchGlobalChannelUI) {
                Toast.makeText(baseContext, "Select global channel", Toast.LENGTH_SHORT).show()
            }

            override fun selectLocalGroup(searchUserUIList: SearchLocalGroupUI) {
                Toast.makeText(baseContext, "Select local group", Toast.LENGTH_SHORT).show()
            }

            override fun selectGlobalGroup(searchUserUIList: SearchGlobalGroupUI) {
                Toast.makeText(baseContext, "Select global group", Toast.LENGTH_SHORT).show()
            }

            override fun selectUserMessage(searchUserUIList: SearchUserMessageUI) {
                Toast.makeText(baseContext, "Select message user", Toast.LENGTH_SHORT).show()
            }

            override fun selectChannelMessage(searchUserUIList: SearchChannelMessageUI) {
                Toast.makeText(baseContext, "Select message channel", Toast.LENGTH_SHORT).show()
            }

            override fun selectGroupMessage(searchUserUIList: SearchGroupMessageUI) {
                Toast.makeText(baseContext, "Select message group", Toast.LENGTH_SHORT).show()
            }

            override fun addParticipant() {
                Toast.makeText(baseContext, "Add participant", Toast.LENGTH_SHORT).show()
            }
        }
    }

    val featureSearchListChatCallback by lazy {
        object : FeatureSearchCallback {
            override fun multiSelectUser(selectedUserList: List<SelectingItem>) {
                Toast.makeText(baseContext, "Select users", Toast.LENGTH_SHORT).show()
            }

            override fun selectLocalUser(searchUserUIList: SearchLocalUserUI) {
                startChatWithUserFeature(searchUserUIList.id)
            }

            override fun selectGlobalUser(searchUserUIList: SearchGlobalUserUI) {
                startChatWithUserFeature(searchUserUIList.id)
            }

            override fun selectLocalChannel(searchUserUIList: SearchLocalChannelUI) {
                Toast.makeText(baseContext, "Select local channel", Toast.LENGTH_SHORT).show()
            }

            override fun selectGlobalChannel(searchUserUIList: SearchGlobalChannelUI) {
                Toast.makeText(baseContext, "Select global channel", Toast.LENGTH_SHORT).show()
            }

            override fun selectLocalGroup(searchUserUIList: SearchLocalGroupUI) {
                Toast.makeText(baseContext, "Select local group", Toast.LENGTH_SHORT).show()
            }

            override fun selectGlobalGroup(searchUserUIList: SearchGlobalGroupUI) {
                Toast.makeText(baseContext, "Select global group", Toast.LENGTH_SHORT).show()
            }

            override fun selectUserMessage(searchUserUIList: SearchUserMessageUI) {
                Toast.makeText(baseContext, "Select message user", Toast.LENGTH_SHORT).show()
            }

            override fun selectChannelMessage(searchUserUIList: SearchChannelMessageUI) {
                Toast.makeText(baseContext, "Select message channel", Toast.LENGTH_SHORT).show()
            }

            override fun selectGroupMessage(searchUserUIList: SearchGroupMessageUI) {
                Toast.makeText(baseContext, "Select message group", Toast.LENGTH_SHORT).show()
            }

            override fun addParticipant() {
                Toast.makeText(baseContext, "Add participant", Toast.LENGTH_SHORT).show()
            }
        }
    }


    private val chatFeatureCallback by lazy {
        object : FeatureChatCallback {
            override fun onDestroy() {
//                contentBottomNav.visibility = View.VISIBLE
            }

            override fun onProfileClicked(chat: Chat) {
                if (mNetworkState.value == true)
                    startProfileFeature(chat)
            }

            override fun onSendMessage(recipientId: Long, message: MessageApiModel) {
                SendingMessagesObject.addMessage(recipientId, message)
            }

            override fun getLoadingMessages(recipientId: Long): List<MessageCallbackModel> {
                return SendingMessagesObject.getByRecipientId(recipientId)
            }
        }
    }

    private fun initLoadServiceCallback() {
        BoundariesObject.mLoadedCallback = loadServiceCallback
    }

    private val loadServiceCallback by lazy {
        object : OnLoadedCallback {
            override fun onCancel(filePath: String) {
                SendingMessagesObject.cancelLoading(filePath)
            }

            override fun onLoaded(filePath: String, fileLoadedId: String) {
                // Just loaded file, not chat
            }

            override fun onLoaded(filePath: String, fileLoadedId: String, chatType: Int) {
                SendingMessagesObject.setLoaded(
                    fileLoadedId,
                    filePath,
                    featureBackendApi.messageApi(),
                    chatType
                )
            }
        }
    }

    private val listChatCallback by lazy {
        object : FeatureListMessagesCallback {
            override fun onSearchIconClick() {
                val config = ConfigSearch(
                    null,
                    null,
                    false,
                    maxCountSearch = false,
                    chat = null,
                    targets = listOf(
                        SearchTarget.LOCAL_USER,
                        SearchTarget.LOCAL_CHANNEL,
                        SearchTarget.LOCAL_GROUP,
                        SearchTarget.MESSAGE,
                        SearchTarget.GLOBAL_USER,
                        SearchTarget.GLOBAL_CHANNEL,
                        SearchTarget.GLOBAL_GROUP
                    ),
                    multiSelection = false,
                    searchView = true,
                    inGroupSearch = false,
                    filteredMe = true
                )
                startSearchFeature(
                    config,
                    featureSearchListChatCallback
                )
            }

            override fun onCreateChatClick() {
                startCreateChatFeature()
            }

            override fun onPageSelected(pageNumber: Int) {

            }

            override fun onUpdateCountUnreadChat(count: Int) {
                val badge = contentBottomNav.getOrCreateBadge(R.id.page_chat)
                if (count > 0) {
                    badge.number = count
                    badge.backgroundColor = resources.getColor(R.color.colorSecondary)
                    badge.isVisible = true
                } else {
                    contentBottomNav.removeBadge(R.id.page_chat)
                }
            }

            override fun onItemChatClickListener(item: Chat, message: Message?) {
//                startListChatsFeature(item.lastMessage)
                startChatFeature(item)
            }


        }
    }

    /**
     *      App Permissions
     * */

    private fun getPermissionContact(): Boolean {
        return if (checkPermissionContact()) {
            true
        } else {
            ActivityCompat.requestPermissions(
                this,
                listOf(
                    Manifest.permission.READ_CONTACTS
                ).toTypedArray(),
                REQUEST_CONTACT
            )
            false
        }
    }

    private fun getPermissionCamera(key: Int): Boolean {
        return if (checkPermissionCamera()) {
            true
        } else {
            ActivityCompat.requestPermissions(
                this,
                listOf(
                    Manifest.permission.CAMERA
                ).toTypedArray(),
                key
            )
            false
        }
    }

    private fun checkPermissionContact(): Boolean {
        return (ContextCompat.checkSelfPermission(
            this,
            Manifest.permission.READ_CONTACTS
        ) == PackageManager.PERMISSION_GRANTED)
    }

    private fun checkPermissionCamera(): Boolean {
        return (ContextCompat.checkSelfPermission(
            this,
            Manifest.permission.CAMERA
        ) == PackageManager.PERMISSION_GRANTED)
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        when (requestCode) {
            REQUEST_CONTACT -> {
                featureCreateChatApi.createChatStarter()
                    .permissionContact(grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED)
            }
        }
    }


    /**
     *      Profile epic
     * */

    private val profileStarterApi: ProfileStarterImpl by lazy {
        ProfileStarterImpl()
    }

    private val profileSettingsStarterApi: ProfileSettingsStarterApi by lazy {
        ProfileSettingsStarterImpl()
    }

    fun startProfileFeature(chat: Chat) {
        contentBottomNav.visibility = View.VISIBLE
        profileStarterApi.startProfile(
            supportFragmentManager,
            R.id.main_container,
            true,
            featureProfileCallback,
            chat,
            feedFeatureStarter,
            featureBackendApi,
            mNetworkState,
            dataBase
        )
    }

    fun startProfileSettingsFeature() {
//        contentBottomNav.visibility = View.GONE
        profileSettingsStarterApi.start(
            supportFragmentManager,
            R.id.main_container,
            featureBackendApi,
            true,
            featureProfileSettingsCallback
        )
    }

    fun startProfileSubscribersFeature(user: User) {
        profileStarterApi.startSubscribersFragment(
            supportFragmentManager,
            R.id.main_container,
            true,
            featureProfileCallback,
            user
        )
    }


    fun startEditProfileFeature(field: String, user: User) {
        profileStarterApi.startEditProfileFragment(
            supportFragmentManager,
            R.id.main_container,
            featureProfileCallback,
            user,
            field
        )
    }

    fun startEditProfileFeature(chat: Chat) {
        profileStarterApi.startEditProfileFragment(
            supportFragmentManager,
            R.id.main_container,
            featureProfileCallback,
            chat
        )
    }

    fun startEditGroupSubscribersFeature(chat: Chat, filter: Int) {
        profileStarterApi.startEditSubscribersGroupFragment(
            supportFragmentManager,
            R.id.main_container,
            featureProfileCallback,
            chat,
            filter
        )
    }

    fun startEditPermissionsFeature(chat: Chat, user: UserMini? = null) {
        profileStarterApi.startEditPermissionsFragment(
            supportFragmentManager,
            R.id.main_container,
            chat,
            user
        )
    }

    private val featureProfileSettingsCallback by lazy {
        object : FeatureProfileSettingsCallback {
            override fun onLoggedOut() {
                logOutAndCloseConnection()
            }

            override fun onDestroy() {
//                contentBottomNav.visibility = View.VISIBLE
            }
        }
    }

    fun initProfileBottomPickerCallback(
        function: (url: String?) -> Unit
    ): FeatureBottomPickerCallback {
        val callback = featureProfileBottomPickerCallback
        callback.function = function
        return callback
    }

    private val featureProfileBottomPickerCallback by lazy {
        object : FeatureBottomPickerCallback {
            lateinit var function: (url: String?) -> Unit

            @SuppressLint("CheckResult")
            override fun onSelected(media: List<com.example.feature_bottom_picker_api.models.MediaModel>) {
                if (media.isNotEmpty()) {
                    if (media.first() is PhotoModel) {
                        val photo = media.first() as PhotoModel
                        val file = File(photo.filePath)
                        if (file.canRead()) {
                            function(photo.filePath)
                        }
                    } else {
                        val videoModel = media.first() as VideoModel
                        val file = File(videoModel.filePath)
                        if (file.canRead()) {
                            function(videoModel.filePath)
                        }
                    }
                }
            }

            override fun onSelected(
                media: List<com.example.feature_bottom_picker_api.models.MediaModel>,
                comment: String
            ) {
            }

            override fun onExit() {
                supportFragmentManager.popBackStack()
            }

            override fun onFileSelected(uri: Uri) {
            }
        }
    }

    private val featureProfileCallback by lazy {
        ProfileCallback(this)
    }

    /**
     *      Feed epic
     * */


    private val createPostFeatureApi: FeatureCreatePostApi by lazy {
        LibCreatePostDependency.featureCreatePostApi(applicationContext, featureBackendApi)
    }

    fun startCreatePost(resultCallback: (Boolean) -> Unit) {
        return createPostFeatureApi.createPostStarter().start(
            supportFragmentManager,
            R.id.main_container,
            true,
            CreatePostCallback(),
            dataBase.userDao(),
            featureBackendApi,
            mNetworkState,
            resultCallback
        )
    }

    private val feedFeatureApi: FeatureFeedApi by lazy {
        LibFeedDependency.featureFeedApi(applicationContext, featureBackendApi)
    }

    private val feedFeatureStarter: FeatureFeedStarter by lazy {
        feedFeatureApi.feedStarter()
    }

    private fun startFeedFeature() {
        feedFeatureStarter.startCommonFeed(
            supportFragmentManager,
            R.id.main_container,
            true,
            mNetworkState,
            featureBackendApi,
            dataBase,
            FeedCallback(feedFeatureStarter)
        )
    }

    fun startPostFragment(post: Message) {
        val featurePostApi =
            LibPostDependency.featurePostApi(applicationContext, featureBackendApi)
        featurePostApi.postStarter().start(
            supportFragmentManager,
            R.id.main_container,
            R.id.contentBottomNav,
            false,
            PostCallback(),
            featureBackendApi,
            post,
            mNetworkState
        )
    }

    inner class CreatePostCallback : FeatureCreatePostCallback {
        override fun chooseFromGallery(function: (String?) -> Unit) {
            val configs = StarterModel()
            configs.fullScreen = true
            configs.showDocuments = false
            configs.addComment = false
            configs.showVideo = true
            configs.isSinglePick = true
            featureBottomPickerApi.start(
                supportFragmentManager,
                R.id.main_container,
                configs,
                initProfileBottomPickerCallback(function)
            )
        }

        override fun openPost(message: Message) {
            startPostFragment(message)
        }
    }

    inner class FeedCallback(private val starter: FeatureFeedStarter) : FeatureFeedCallback {
        override fun share(post: Message) {
            val sendIntent = Intent()
            sendIntent.action = Intent.ACTION_SEND
            sendIntent.putExtra(
                Intent.EXTRA_TEXT,
                "sendy.ru/${post}"
            )
            sendIntent.type = "text/plain"
            startActivity(sendIntent)
        }

        override fun openPost(post: Message) {
            startPostFragment(post)
        }

        override fun openComment(post: Message) {
            startPostFragment(post)
        }

        override fun openProfile(user: UserMini) {
            startProfileFeature(
                Chat(
                    user.uid,
                    Chat.PERSONAL_DIALOG,
                    CLOSE_CHAT,
                    user.uid,
                    user.uid
                )
            )
        }

        override fun createPost(resultCallback: (Boolean) -> Unit) {
            startCreatePost(resultCallback)
        }

        override fun openNotification() {}
        override fun removeNotify(postUI: FeedItemPostUI) {
            starter.notifyRemove(postUI)
        }

    }


    inner class PostCallback : FeaturePostCallback {
        override fun openProfile(user: UserMini) {
            startProfileFeature(
                Chat(
                    user.uid,
                    Chat.PERSONAL_DIALOG,
                    CLOSE_CHAT,
                    user.uid,
                    user.uid
                )
            )
        }
    }

    /**
     *      Bottom navigation
     * */

    private fun startDefaultBottomNav() {
        contentBottomNav.visibility = View.VISIBLE
        contentBottomNav.setOnNavigationItemSelectedListener(bottomNavCallback)
        contentBottomNav.selectedItemId = R.id.page_chat
    }

    private val bottomNavCallback by lazy {
        BottomNavigationView.OnNavigationItemSelectedListener {
            when (it.itemId) {
                R.id.page_feed -> {
                    startFeedFeature()
                    return@OnNavigationItemSelectedListener true
                }
                R.id.page_chat -> {
                    startListChatsFeature()
                    return@OnNavigationItemSelectedListener true
                }
                R.id.page_profile -> {
                    startProfileFeature(getMockUserUi())
                    return@OnNavigationItemSelectedListener true
                }
            }
            return@OnNavigationItemSelectedListener false
        }
    }

    private fun getMockUserUi(): Chat = Chat(
        getUidSP(),
        Chat.PERSONAL_DIALOG,
        Chat.PRIVATE_CHAT,
        getUidSP()
    )

    /**
     * Init Connection
     * */

    @SuppressLint("HardwareIds")
    private fun initConnection() {
        val deviceInfo = Settings.Secure.getString(
            applicationContext.contentResolver,
            Settings.Secure.ANDROID_ID
        )
        val uid = getUidSP()
        val serverFile = getServerFileSP()
        val session = getSessionSP()
        val sign = getSignSP()

        connectionLifecycleRegistry.onNext(Lifecycle.State.Started)
        Log.i("WebSocket", "uid $uid serverFile $serverFile session $session ")
        if (uid != USER_UID_EMPTY && serverFile != null && session != null) {
            setSession(session)
            setSign(sign.toString())
            setUid(uid)
            setServerFile(serverFile)
            initBackEndApi()
            startFirstEpic()
        } else {
            initRequest(AuthInitRequest(deviceInfo)) {
                setSession(it.data!!.session)
                setSessionSP(it.data!!.session)
                setSign(it.data!!.sign)
                setSignSP(it.data!!.sign)
                setServerFile(it.data!!.server_file)
                setServerFileSP(it.data!!.server_file)
                initBackEndApi()
                FirebaseInstanceId.getInstance().instanceId
                    .addOnCompleteListener { task ->
                        if (!task.isSuccessful) {
                            Log.w(
                                "FirebaseInstanceId",
                                "getInstanceId failed",
                                task.exception
                            )
                        } else {
                            // Get new Instance ID token
                            val token: String = task.result?.token ?: ""
                            Log.d(
                                "FirebaseInstanceId",
                                "Got the token: $token"
                            )
                            setTokenSP(token)
                            updateTokenInFB()
                        }
                    }
            }
            startAuthFeature()
        }
    }

    @SuppressLint("CheckResult")
    private fun initRequest(
        request: AuthInitRequest?,
        callback: (response: InitResponse) -> Unit
    ) {
        var observer: Observer<Boolean>? = null
        observer = Observer { hasConnection ->
            if (hasConnection) {
                val authApiREST = NetworkModule.retrofit().create(AuthApiREST::class.java)
                authApiREST.init(request).subscribeOn(Schedulers.io())
                    .subscribe({ callback(it) }, {
                        Log.e("Retrofit", it.message.toString())
                        initRequest(request, callback)
                    })
                mNetworkState.removeObserver(observer!!)
            }
        }
        mNetworkState.observe(this, observer)
    }

    @SuppressLint("HardwareIds")
    private fun initBackEndApi() {
        val deviceInfo = Settings.Secure.getString(
            applicationContext.contentResolver,
            Settings.Secure.ANDROID_ID
        )
        val deviceModel = Build.BRAND + "_" + Build.MODEL
        val uid = getUidSP()
        val session = getSessionSP() ?: return
        val authData = AuthReq(
            user_id = uid,
            device_hash = deviceInfo,
            device_model = deviceModel,
            user_session = session
        )
        val gson = Gson()
        val personType: Type = object : TypeToken<AuthReq?>() {}.type
        val json = gson.toJson(authData, personType)
        val authString: String = Base64.encodeToString(json.toByteArray(), Base64.DEFAULT)
        val DEBUG = true
        val newUrl =
            "wss://dev2.sendy.systems/?auth=$authString${if (DEBUG) "&debug=57xbmudsIsWE7W7mLp406" else ""}&app_key_id=" + SignData.applicationId
        featureBackendApi = LibBackendDependency.featureBackendApi(newUrl, this) {
            Log.e("WebSocket", it.toString())
            it.onNext(Lifecycle.State.Stopped.WithReason(ShutdownReason.GRACEFUL))
            if (it != connectionLifecycleRegistry) {
                return@featureBackendApi
            }
            runOnUiThread {
                var observer: Observer<Boolean>? = null
                observer = Observer { hasConnection ->
                    if (hasConnection) {
                        connectionLifecycleRegistry.onNext(Lifecycle.State.Started)
                        mNetworkState.removeObserver(observer!!)
                    }
                }
                mNetworkState.observe(this, observer)
            }
        }
        pushController.init()
    }

    /**     Helper functions
     * */

    private fun openShareApp() {
        val sendIntent = Intent()
        sendIntent.action = Intent.ACTION_SEND
        sendIntent.putExtra(
            Intent.EXTRA_TEXT,
            "Я пользуюсь Sendy. Заходи: https://play.google.com/store/apps/details?id=" + BuildConfig.APPLICATION_ID
        )
        sendIntent.type = "text/plain"
        startActivity(sendIntent)
    }

    private fun startFirstEpic() {
        LibBackendDependency.vFeatureBackendImpl?.let {
            supportFragmentManager.clearOldFragments()
            startDefaultBottomNav()
        }
    }

    private fun setSign(sign: String) {
        SignData.sign = sign
    }

    private fun setSignSP(sign: String) {
        val sharedPreferences = getMySharedPreferences()
        sharedPreferences.edit().putString(getString(R.string.sign), sign).apply()
    }

    private fun setSession(sign: String) {
        SignData.session = sign
    }

    private fun setSessionSP(sign: String) {
        val sharedPreferences = getMySharedPreferences()
        sharedPreferences.edit().putString(getString(R.string.session), sign).apply()
    }

    private fun updateTokenInFB() {
        val sign = getSign()
        if (sign.isEmpty()) return
        val token = getTokenSP() ?: return
        try {
            val req = AnyRequest(AnyInnerRequest(AuthSetTokenRequest(token)))
            req.updateSign()
            LibBackendDependency.featureBackendApi().authApi()
                .setToken(req)
        } catch (ignored: IllegalStateException) {
        }
    }

    private fun getTokenSP(): String? {
        val sharedPreferences = getMySharedPreferences()
        return sharedPreferences.getString(getString(R.string.token), null)
    }

    private fun setTokenSP(token: String) {
        val sharedPreferences = getMySharedPreferences()
        sharedPreferences.edit().putString(getString(R.string.token), token).apply()
    }

    private fun setServerFile(sign: String) {
        SignData.server_file = sign
    }

    private fun setServerFileSP(sign: String) {
        val sharedPreferences = getMySharedPreferences()
        sharedPreferences.edit().putString(getString(R.string.server_file), sign).apply()
    }

    private fun getSign(): String = SignData.sign

    private fun getSignSP(): String? {
        val sharedPreferences = getMySharedPreferences()
        return sharedPreferences.getString(getString(R.string.sign), null)
    }

    private fun getSession(): String = SignData.session

    private fun getSessionSP(): String? {
        val sharedPreferences = getMySharedPreferences()
        return sharedPreferences.getString(getString(R.string.session), null)
    }

    private fun getServerFile(): String = SignData.server_file

    private fun getServerFileSP(): String? {
        val sharedPreferences = getMySharedPreferences()
        return sharedPreferences.getString(getString(R.string.server_file), null)
    }

    private fun setUidSP(uid: Long) {
        val sharedPreferences = getMySharedPreferences()
        sharedPreferences.edit().putLong(getString(R.string.user_uid), uid).apply()
    }

    private fun setUid(uid: Long) {
        SignData.uid = uid
    }

    private fun getUidSP(): Long {
        val sharedPreferences = getMySharedPreferences()
        return sharedPreferences.getLong(getString(R.string.user_uid), USER_UID_EMPTY)
    }

    private fun getUid() = SignData.uid

    private fun clearSignData() {
        val sharedPreferences = getMySharedPreferences()
        sharedPreferences
            .edit()
            .remove(getString(R.string.sign))
            .remove(getString(R.string.user_uid))
            .apply()
        CoroutineScope(Dispatchers.IO).launch {
            Repository.clearDB()
        }
    }

    private fun getMySharedPreferences(): SharedPreferences =
        getSharedPreferences(getString(R.string.auth), Context.MODE_PRIVATE)

    private fun FragmentManager.clearBackStack() {
        for (i in 0..backStackEntryCount)
            popBackStack()
    }

    private fun FragmentManager.clearOldFragments() {
        this.clearBackStack()
        val trans = this.beginTransaction()
        for (i in this.fragments) {
            if (i is NavHostFragment) {
                i.childFragmentManager.clearOldFragments()
            }
            trans.remove(i)
        }
        trans.commit()
        this.fragments.clear()
    }

    private fun initListenerOnChangeFragment() {
        supportFragmentManager.addOnBackStackChangedListener {
            val lastView = supportFragmentManager.fragments.last() ?: null
            Log.d(
                "STACK_ADD",
                lastView?.javaClass.toString()
            )
            if (lastView != null) {
                when (lastView) {
                    is RegistrationFragment,
                    is ChatFragment,
                    is FragmentEditProfile,
                    is BottomPickerFragment,
                    is BaseFragment,
                    is SearchFragment,
                    is CreateGroupFragment,
                    is CreateChannelFragment,
                    is AddContactFragment,
                    is CreateGroupSettingsFragment,
                    is CreateChannelSettingsFragment,
                    is FragmentEditGroupOrChannel,
                    is PostFragment,
                    is CreatePostFragment,
                    is BottomBarWriteFragment,
                    is FragmentListSubscribersGroup
                    -> {
                        hideBottomBar()
                    }
                    else -> {
                        showBottomBar()
                    }
                }
            }
        }
    }

    private fun hideBottomBar() {
        if (contentBottomNav.alpha != 0f || contentBottomNav.visibility != View.INVISIBLE) {
            contentBottomNav.animate().alpha(0f)
                .withEndAction { contentBottomNav.visibility = View.INVISIBLE }
                .start()
        }
    }

    private fun showBottomBar() {
        if (contentBottomNav.alpha != 1f || contentBottomNav.visibility != View.VISIBLE) {
            contentBottomNav.animate().alpha(1f)
                .withStartAction { contentBottomNav.visibility = View.VISIBLE }
                .start()
        }
    }

    private fun logOutAndCloseConnection() {
        closeConnection()
        clearSignData()
        reconnection()
    }

    private fun closeConnection() {
        val request = AuthCloseSessionsRequest(SignData.session)
        val inReq = AnyInnerRequest(request)
        val req = AnyRequest(inReq)
        featureBackendApi.authApi().closeSessions(req) {}
    }

    private fun reconnection() {
        clearLibBackendDependency()
        finish()
        startActivity(intent)
    }


    private fun updateLibBackendDependency() {
        clearLibBackendDependency()
        initNewLibBackendDependency()
    }

    private fun initNewLibBackendDependency() {
        initBackEndApi()
        connectionLifecycleRegistry.onNext(Lifecycle.State.Started)
    }

    private fun clearLibBackendDependency() {
        connectionLifecycleRegistry.onNext(Lifecycle.State.Stopped.AndAborted)
        connectionLifecycleRegistry = LifecycleRegistry(0L)
        LibBackendDependency.lifecycleRegistry = connectionLifecycleRegistry
        LibBackendDependency.clearFeatureBackend()
    }

}
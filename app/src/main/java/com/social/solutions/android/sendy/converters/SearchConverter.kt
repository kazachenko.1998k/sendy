package com.social.solutions.android.sendy.converters

import com.social.solution.feature_create_community_api.models.CreateCommunityGlobalUserUI
import com.social.solution.feature_create_community_api.models.CreateCommunityLocalUserUI
import com.social.solution.feature_search_api.models.SearchGlobalUserUI
import com.social.solution.feature_search_api.models.SearchLocalUserUI


fun convertFrom(user: SearchLocalUserUI): CreateCommunityLocalUserUI {
    return CreateCommunityLocalUserUI(
        user.id,
        user.firstName,
        user.lastName,
        user.login,
        user.dateLastVisit,
        user.avatar,
        user.isSubscribe
    )
}

fun convertFrom(user: SearchGlobalUserUI): CreateCommunityGlobalUserUI {
    return CreateCommunityGlobalUserUI(
        user.id,
        user.firstName,
        user.lastName,
        user.login,
        user.dateLastVisit,
        user.avatar,
        user.isSubscribe
    )
}
package com.social.solutions.android.sendy.utils.connection

import android.util.Log
import android.view.View
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.LiveData
import androidx.lifecycle.Observer

fun View.initConnectionStateView(lifecycleOwner: LifecycleOwner,
                                 net: LiveData<Boolean>,
                                 back: LiveData<Boolean>) {
    val observer = Observer<Boolean> {
        this.visibility = if (back.value == false || net.value == false) View.VISIBLE else View.GONE
    }
    net.observe(lifecycleOwner, observer)
    back.observe(lifecycleOwner, observer)
}
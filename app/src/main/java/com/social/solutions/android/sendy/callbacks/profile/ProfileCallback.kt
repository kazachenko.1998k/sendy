package com.social.solutions.android.sendy.callbacks.profile

import android.content.Intent
import android.util.Log
import android.widget.ImageView
import android.widget.Toast
import com.example.feature_bottom_picker_api.models.StarterModel
import com.example.feature_list_profile_api.FeatureProfileCallback
import com.example.util_cache.Repository
import com.morozov.core_backend_api.AnyInnerRequest
import com.morozov.core_backend_api.AnyRequest
import com.morozov.core_backend_api.SignData
import com.morozov.core_backend_api.chat.Chat
import com.morozov.core_backend_api.chat.requests.ChatAddMembersRequest
import com.morozov.core_backend_api.chat.requests.ChatLeaveRequest
import com.morozov.core_backend_api.message.Message
import com.morozov.core_backend_api.user.User
import com.morozov.core_backend_api.user.UserMini
import com.morozov.core_backend_api.user.requests.UserSubscribeRequest
import com.morozov.lib_backend.LibBackendDependency
import com.social.solution.feature_create_community_api.FeatureSettingChannelCallback
import com.social.solution.feature_create_community_api.FeatureSettingGroupCallback
import com.social.solution.feature_search_api.FeatureSearchWithChatCallback
import com.social.solution.feature_search_api.models.*
import com.social.solutions.android.sendy.MainSendyActivity
import com.social.solutions.android.sendy.R
import com.social.solutions.android.transition_media_pager.manager.MediaPagerManager
import com.social.solutions.android.util_media_pager.models.FileModel
import com.social.solutions.android.util_media_pager.models.MediaModel
import com.social.solutions.android.util_media_pager.models.MediaType
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class ProfileCallback(val activity: MainSendyActivity) : FeatureProfileCallback {

    val featureSearchAddUsersToGroupOrChannelCallback by lazy {
        object : FeatureSearchWithChatCallback {
            override var chat: Chat? = null

            override fun multiSelectUser(selectedUserList: List<SelectingItem>) {
                val selectedUserForCreatingGroup = selectedUserList.map {
                    if (it is SearchLocalUserUI) it.id
                    else (it as SearchGlobalUserUI).id
                }.toMutableList()
                CoroutineScope(Dispatchers.IO).launch {
                    LibBackendDependency.featureBackendApi().chatApi().addMembers(
                        AnyRequest(
                            AnyInnerRequest(
                                ChatAddMembersRequest(
                                    chat!!.id,
                                    selectedUserForCreatingGroup
                                )
                            )
                        )
                    ) {
                        activity.onBackPressed()
                    }
                }
            }

            override fun selectLocalUser(searchUserUIList: SearchLocalUserUI) {}
            override fun selectGlobalUser(searchUserUIList: SearchGlobalUserUI) {}
            override fun selectLocalChannel(searchUserUIList: SearchLocalChannelUI) {}
            override fun selectGlobalChannel(searchUserUIList: SearchGlobalChannelUI) {}
            override fun selectLocalGroup(searchUserUIList: SearchLocalGroupUI) {}
            override fun selectGlobalGroup(searchUserUIList: SearchGlobalGroupUI) {}
            override fun selectUserMessage(searchUserUIList: SearchUserMessageUI) {}
            override fun selectChannelMessage(searchUserUIList: SearchChannelMessageUI) {}
            override fun selectGroupMessage(searchUserUIList: SearchGroupMessageUI) {}
            override fun addParticipant() {}
        }
    }
    val featureSearchAddAdminToGroupOrChannelCallback by lazy {
        object : FeatureSearchWithChatCallback {
            override var chat: Chat? = null


            override fun multiSelectUser(selectedUserList: List<SelectingItem>) {}

            override fun selectLocalUser(searchUserUIList: SearchLocalUserUI) {
                val userMini = UserMini(
                    uid = searchUserUIList.id,
                    avatarFileId = searchUserUIList.avatar,
                    nick = searchUserUIList.login,
                    firstName = searchUserUIList.firstName,
                    secondName = searchUserUIList.lastName,
                    lastActive = searchUserUIList.dateLastVisit,
                    isSubscribe = searchUserUIList.isSubscribe
                )
                onEditPermissionsClickListener(chat!!, userMini)
            }

            override fun selectGlobalUser(searchUserUIList: SearchGlobalUserUI) {
                val userMini = UserMini(
                    uid = searchUserUIList.id,
                    avatarFileId = searchUserUIList.avatar,
                    nick = searchUserUIList.login,
                    firstName = searchUserUIList.firstName,
                    secondName = searchUserUIList.lastName,
                    lastActive = searchUserUIList.dateLastVisit,
                    isSubscribe = searchUserUIList.isSubscribe
                )
                onEditPermissionsClickListener(chat!!, userMini)
            }

            override fun selectLocalChannel(searchUserUIList: SearchLocalChannelUI) {}
            override fun selectGlobalChannel(searchUserUIList: SearchGlobalChannelUI) {}
            override fun selectLocalGroup(searchUserUIList: SearchLocalGroupUI) {}
            override fun selectGlobalGroup(searchUserUIList: SearchGlobalGroupUI) {}
            override fun selectUserMessage(searchUserUIList: SearchUserMessageUI) {}
            override fun selectChannelMessage(searchUserUIList: SearchChannelMessageUI) {}
            override fun selectGroupMessage(searchUserUIList: SearchGroupMessageUI) {}
            override fun addParticipant() {}
        }
    }

    override fun onSearchIconClick(chat: Chat) {
        val config = ConfigSearch(
            null,
            null,
            false,
            true,
            chat,
            listOf(
                SearchTarget.LOCAL_USER
            ),
            false,
            true,
            true,
            false
        )
        activity.startSearchFeature(
            config,
            activity.featureSearchListChatCallback
        )
    }

    override fun onAddUserClick(chat: Chat) {
        featureSearchAddUsersToGroupOrChannelCallback.chat = chat
        val config = ConfigSearch(
            "Добавить участника",
            "Добавить",
            false,
            true,
            chat,
            listOf(
                SearchTarget.LOCAL_USER, SearchTarget.GLOBAL_USER
            ),
            true,
            true,
            false,
            true
        )
        activity.startSearchFeature(
            config,
            featureSearchAddUsersToGroupOrChannelCallback
        )
    }

    override fun onAddAdminClick(chat: Chat) {
        featureSearchAddAdminToGroupOrChannelCallback.chat = chat
        val config = ConfigSearch(
            "Добавить администратора",
            "Назначить",
            false,
            true,
            chat,
            listOf(
                SearchTarget.LOCAL_USER, SearchTarget.GLOBAL_USER
            ),
            false,
            true,
            true,
            true
        )
        activity.startSearchFeature(
            config,
            featureSearchAddAdminToGroupOrChannelCallback
        )
    }

    override fun onUserSubscribeClick(userMini: UserMini) {
        LibBackendDependency.featureBackendApi().userApi().subscribe(
            AnyRequest(
                AnyInnerRequest(
                    UserSubscribeRequest(
                        userMini.uid,
                        true
                    )
                )
            )
        )
    }

    override fun onUserClickListener(userMini: UserMini) {
        activity.startProfileFeature(
            Chat(
                userMini.uid,
                Chat.PERSONAL_DIALOG,
                dialogUid = userMini.uid
            )
        )
    }

    override fun onUserSubscribersClickListener(user: User) {
        activity.startProfileSubscribersFeature(user)
    }

    override fun onChatClickListener(userId: Long) {
        activity.startChatWithUserFeature(userId)
    }

    override fun onFaqClickListener(user: User) {
        Toast.makeText(activity.baseContext, "onFaqClickListener", Toast.LENGTH_SHORT).show()
    }

    override fun onExitClickListener(chat: Chat) {
        LibBackendDependency.featureBackendApi().chatApi()
            .leave(AnyRequest(AnyInnerRequest(ChatLeaveRequest(chat.id))))
    }

    override fun onAvatarShowClickListener(user: User?) {
        MediaPagerManager
            .with(activity)
            .setItems(
                listOf(
                    MediaModel(
                        MediaType.PHOTO,
                        FileModel(SignData.server_file + user?.avatarFileId, null)
                    )
                )
            )
            .setUserName(user?.firstName ?: "")
            .start()
    }

    override fun onAvatarChangeClickListener(
        function: (url: String?) -> Unit
    ) {
        val configs = StarterModel()
        configs.fullScreen = true
        configs.showDocuments = false
        configs.addComment = false
        configs.showVideo = false
        configs.isSinglePick = true
        activity.featureBottomPickerApi.start(
            activity.supportFragmentManager,
            R.id.main_container,
            configs,
            activity.initProfileBottomPickerCallback(function)
        )
    }

    override fun onEditNickClickListener(user: User) {
        activity.startEditProfileFeature("nick", user)
    }

    override fun onEditNameClickListener(user: User) {
        activity.startEditProfileFeature("firstName", user)
    }

    override fun onEditDescriptionClickListener(user: User) {
        activity.startEditProfileFeature("shortBio", user)
    }

    override fun onEditChatClickListener(chat: Chat) {
        activity.startEditProfileFeature(chat)
    }

    override fun onDeleteChatClickListener(chat: Chat) {
        CoroutineScope(Dispatchers.IO).launch {
            Repository.chatDao.delete(chat)
            withContext(Dispatchers.Main) {
                activity.startListChatsFeature()
            }
        }
    }

    override fun onEditGroupSubscribersClickListener(
        chat: Chat,
        filter: Int
    ) {
        activity.startEditGroupSubscribersFeature(chat, filter)
    }

    override fun onEditPermissionsClickListener(chat: Chat, user: UserMini?) {
        activity.startEditPermissionsFeature(chat, user)
    }

    override fun onEditTypeChatClickListener(
        chat: Chat,
        callback: (chat: Chat) -> Unit
    ) {
        when (chat.type) {
            Chat.CHANNEL, Chat.PRIVATE_CHANNELS -> {
                activity.featureCreateCommunityApi.createGroupStarter().startSettingChannel(
                    activity.supportFragmentManager,
                    R.id.main_container,
                    true,
                    chat,
                    LibBackendDependency.featureBackendApi(),
                    LibBackendDependency.featureBackendApi().commonApi(),
                    object : FeatureSettingChannelCallback {
                        override fun openChannel(chat: Chat) {
                            Log.d("SETTINGS", chat.toString())
                            callback(chat)
                        }

                        override fun shareLink(text: String) {
                            Log.d("SETTINGS", "shareLink")
                        }
                    })
            }
            Chat.GROUP_CHAT -> activity.featureCreateCommunityApi.createGroupStarter()
                .startSettingGroup(
                    activity.supportFragmentManager,
                    R.id.main_container,
                    true,
                    chat,
                    LibBackendDependency.featureBackendApi(),
                    LibBackendDependency.featureBackendApi().commonApi(),
                    object : FeatureSettingGroupCallback {
                        override fun openGroup(chatNew: Chat) {
                            Log.d("SETTINGS", chat.toString())
                            callback(chatNew)
                        }

                        override fun shareLink(text: String) {
                            Log.d("SETTINGS", "shareLink")
                        }
                    })
            else -> return
        }
    }

    override fun onSettingClickListener(user: User) {
        activity.startProfileSettingsFeature()
    }

    override fun onPhotoClickListener(
        url: String,
        imageView: ImageView?,
        withVideo: Boolean,
        function: (url: String?) -> Unit
    ) {
        if (url == "") {
            val configs = StarterModel()
            configs.fullScreen = true
            configs.showDocuments = false
            configs.addComment = false
            configs.showVideo = withVideo
            configs.isSinglePick = true
            activity.featureBottomPickerApi.start(
                activity.supportFragmentManager,
                R.id.main_container,
                configs,
                activity.initProfileBottomPickerCallback(function)
            )
        } else {
            MediaPagerManager
                .with(activity)
                .setItems(
                    listOf(
                        MediaModel(
                            if (withVideo) MediaType.VIDEO else MediaType.PHOTO,
                            FileModel(SignData.server_file + url, null)
                        )
                    )
                )
                .setUserName("")
                .start()
        }
    }

    override fun share(post: Message) {
        val sendIntent = Intent()
        sendIntent.action = Intent.ACTION_SEND
        sendIntent.putExtra(
            Intent.EXTRA_TEXT,
            "sendy.ru/${post}"
        )
        sendIntent.type = "text/plain"
        activity.startActivity(sendIntent)
    }

    override fun openPost(post: Message) {

        activity.startPostFragment(post)
    }

    override fun openComment(post: Message) {
        activity.startPostFragment(post)
    }

    override fun openProfile(user: UserMini) {
        activity.startProfileFeature(
            Chat(
                user.uid,
                Chat.PERSONAL_DIALOG,
                Chat.CLOSE_CHAT,
                user.uid,
                user.uid
            )
        )
    }

    override fun createPost(resultCallback: (Boolean) -> Unit) {
        activity.startCreatePost(resultCallback)
    }

}
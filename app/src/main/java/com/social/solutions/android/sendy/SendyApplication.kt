package com.social.solutions.android.sendy

import android.app.Application
import android.app.NotificationChannel
import android.app.NotificationManager
import android.os.Build
import com.example.util_cache.Repository
import com.social.solutions.android.util_media_pager.exoplayer.ExoCacheObject
import com.social.solutions.android.util_notification_push.Constants
import com.vanniktech.emoji.EmojiManager
import com.vanniktech.emoji.ios.IosEmojiProvider
import io.reactivex.plugins.RxJavaPlugins


class SendyApplication : Application() {
    val CHANNEL_1_ID = "channel_id"

    override fun onCreate() {
        Constants.classMainActivity = MainSendyActivity::class.java
        super.onCreate()
        Repository.initRepository(this)
        EmojiManager.install(IosEmojiProvider())
        RxJavaPlugins.setErrorHandler { throwable: Throwable? -> }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val channel1 = NotificationChannel(
                CHANNEL_1_ID,
                "channel_name",
                NotificationManager.IMPORTANCE_HIGH
            )
            channel1.description = "This is Channel 2"
            val channel = NotificationChannel(
                "channel_id", "channel_name", NotificationManager.IMPORTANCE_DEFAULT
            )
            channel.description = "channel description"
            channel.setShowBadge(true)
            channel.canShowBadge()
            channel.enableLights(true)
            channel.enableVibration(true)
            channel.vibrationPattern = longArrayOf(100, 200, 300, 400, 500)
            val manager = getSystemService(
                NotificationManager::class.java
            )
            manager?.createNotificationChannel(channel1)
        }

        ExoCacheObject.initialize(applicationContext)
    }
}
package com.social.solutions.android.sendy.push

import android.annotation.SuppressLint
import android.content.Context
import androidx.core.app.NotificationManagerCompat
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import com.example.feature_list_messages_impl.ListMessagesFragment
import com.morozov.feature_chat_impl.start.MainObject
import com.morozov.feature_chat_impl.ui.fragments.chat.ChatFragment
import com.morozov.lib_backend.LibBackendDependency
import com.social.solutions.android.util_notification_push.Constants
import com.social.solutions.android.util_notification_push.NotificationPush

class PushController(private val context: Context, private val fragmentManager: FragmentManager) {

    private var canShow: Boolean = false
    private var lastView: Fragment? = null

    init {
        initFragmentListener()
    }

    fun init() {
        initPushListener()
    }

    private fun clearPushHistory() {
        val sp = context.getSharedPreferences(Constants.spKey, Context.MODE_PRIVATE)
        sp.edit().remove(Constants.spKey).apply()
        val notificationManager = NotificationManagerCompat.from(context)
        notificationManager.cancel(Constants.notifyId)
    }

    @SuppressLint("CheckResult")
    private fun initPushListener() {
        LibBackendDependency.featureBackendApi().pushListener.subscribe({ event ->
            if (lastView != null && lastView is ChatFragment && event.data?.chat_id != null && MainObject.mChatId == event.data!!.chat_id!!) {
                return@subscribe
            }
            //TODO need logic for this
//            if (canShow) {
            NotificationPush(context).sendNotification(event)
//            }
        }, {
            it.printStackTrace()
        })

    }

    private fun initFragmentListener() {
        fragmentManager.addOnBackStackChangedListener {
            lastView = fragmentManager.fragments.last()
            if (lastView != null) {
                canShow = when (lastView) {
                    is ListMessagesFragment
                    -> {
                        clearPushHistory()
                        false
                    }
                    else -> {
                        true
                    }
                }
            }
        }
    }
}
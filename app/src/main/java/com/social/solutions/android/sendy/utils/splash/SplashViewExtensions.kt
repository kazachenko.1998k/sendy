package com.social.solutions.android.sendy.utils.splash

import android.view.View
import android.view.animation.*

object SplashViewExtensions {
    var mIsReadyLoadFirstScreen = false
}

fun View.addSplashAnimation(time: Long) {
    val fadeIn = AlphaAnimation(0f, 1f)
    fadeIn.interpolator = DecelerateInterpolator() //add this
    fadeIn.duration = time

    val fadeOut = AlphaAnimation(1f, 0f)
    fadeOut.interpolator = AccelerateInterpolator() //and this
    fadeOut.startOffset = 1000
    fadeOut.duration = time

    val animation = AnimationSet(false) //change to false
    animation.addAnimation(fadeIn)
//    animation.addAnimation(fadeOut)
    this.animation = animation
}

fun View?.makeSplashAnimation(doAfterSplash: () -> Unit) {
    this?.animation?.setAnimationListener(object : Animation.AnimationListener {
        override fun onAnimationEnd(animation: Animation?) {
            doAfterSplash()
        }
        override fun onAnimationRepeat(animation: Animation?) {}
        override fun onAnimationStart(animation: Animation?) {}
    })
    this?.animation?.start()
}
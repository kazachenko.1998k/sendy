package com.social.solution.feature_create_post_api

import androidx.fragment.app.FragmentManager
import androidx.lifecycle.MutableLiveData
import com.example.util_cache.user.UserDao
import com.morozov.core_backend_api.FeatureBackendApi

interface FeatureCreatePostStarter {

    fun start(
        manager: FragmentManager,
        parentContainer: Int,
        addToBackStack: Boolean,
        callback: FeatureCreatePostCallback,
        userDao: UserDao,
        backendApi: FeatureBackendApi,
        mNetworkState: MutableLiveData<Boolean>,
        resultCallback: (Boolean) -> Unit
    )

}

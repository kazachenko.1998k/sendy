package com.social.solution.feature_create_post_api

interface FeatureCreatePostApi {
    fun createPostStarter(): FeatureCreatePostStarter
}
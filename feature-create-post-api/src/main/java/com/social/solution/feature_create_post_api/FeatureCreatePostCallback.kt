package com.social.solution.feature_create_post_api

import com.morozov.core_backend_api.message.Message


interface FeatureCreatePostCallback {
    fun chooseFromGallery(function: (String?) -> Unit)
    fun openPost(message: Message)
}
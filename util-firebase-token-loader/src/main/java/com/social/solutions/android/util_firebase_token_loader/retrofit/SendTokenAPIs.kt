package com.social.solutions.android.util_firebase_token_loader.retrofit

import com.morozov.core_backend_api.AnyRequest
import com.morozov.core_backend_api.EmptyResponse
import io.reactivex.Single
import retrofit2.http.*

internal interface SendTokenAPIs {
    @POST("http/v1.0/auth")
    fun setToken(
        @Body request: AnyRequest?
    ): Single<EmptyResponse>
}
package com.social.solutions.android.util_firebase_token_loader.send

import android.content.Context
import android.content.SharedPreferences
import android.util.Log
import com.google.firebase.messaging.FirebaseMessagingService
import com.google.firebase.messaging.RemoteMessage
import com.morozov.core_backend_api.AnyInnerRequest
import com.morozov.core_backend_api.AnyRequest
import com.morozov.core_backend_api.SignData.updateSign
import com.morozov.core_backend_api.auth.modelsRequest.AuthSetTokenRequest
import com.social.solutions.android.util_firebase_token_loader.R
import com.social.solutions.android.util_firebase_token_loader.retrofit.FileRetrofit
import com.social.solutions.android.util_firebase_token_loader.retrofit.SendTokenAPIs
import com.social.solutions.android.util_notification_push.NotificationPush


internal class MyFirebaseMessagingService
    : FirebaseMessagingService() {


    override fun onNewToken(token: String) {
        super.onNewToken(token)
        Log.e("Firebase", "New token: $token")
        setTokenSP(token)
        val req = AnyRequest(
            AnyInnerRequest(AuthSetTokenRequest(token), "auth.setToken")
        )
        req.updateSign()
        FileRetrofit.retrofit().create(SendTokenAPIs::class.java)
            .setToken(req)
    }

    override fun onMessageReceived(remoteMessage: RemoteMessage) {
        super.onMessageReceived(remoteMessage)
        // TODO(developer): Handle FCM messages here.
        // If the application is in the foreground handle both data and notification messages here.
        // Also if you intend on generating your own notifications as a result of a received FCM
        // message, here is where that should be initiated. See sendNotification method below.
        Log.i("Firebase", remoteMessage.notification.toString())
        val notification = remoteMessage.notification ?: return
        val data = remoteMessage.data
        NotificationPush(applicationContext).sendNotification(notification, data)
    }

    private fun getSignSP(): String? {
        val sharedPreferences = getMySharedPreferences()
        return sharedPreferences.getString(getString(R.string.sign), null)
    }

    private fun getTokenSP(): String? {
        val sharedPreferences = getMySharedPreferences()
        return sharedPreferences.getString(getString(R.string.token), null)
    }

    private fun setTokenSP(token: String) {
        val sharedPreferences = getMySharedPreferences()
        sharedPreferences.edit().putString(getString(R.string.token), token).apply()
    }

    private fun getMySharedPreferences(): SharedPreferences =
        getSharedPreferences(getString(R.string.auth), Context.MODE_PRIVATE)

}
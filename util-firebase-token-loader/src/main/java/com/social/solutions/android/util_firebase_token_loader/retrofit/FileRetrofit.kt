package com.social.solutions.android.util_firebase_token_loader.retrofit

import com.fasterxml.jackson.databind.DeserializationFeature
import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.databind.SerializationFeature
import com.jakewharton.retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.jackson.JacksonConverterFactory
import java.security.cert.CertificateException
import javax.net.ssl.HostnameVerifier
import javax.net.ssl.SSLContext
import javax.net.ssl.TrustManager
import javax.net.ssl.X509TrustManager

internal object FileRetrofit {

    private const val endPointHttps = "https://dev2.sendy.systems/"

    private fun objectMapper(): ObjectMapper {
        val mapper = ObjectMapper()

        mapper.configure(SerializationFeature.FAIL_ON_EMPTY_BEANS, false)
        mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false)

        return mapper
    }

    private fun convertFactory(mapper: ObjectMapper): retrofit2.Converter.Factory {
        return JacksonConverterFactory.create(mapper)
    }

    private fun okHttp(): OkHttpClient {
        try {
            val trustAllCerts = arrayOf<TrustManager>(object : X509TrustManager {
                @Throws(CertificateException::class)
                override fun checkClientTrusted(
                    chain: Array<java.security.cert.X509Certificate>,
                    authType: String
                ) {
                }

                @Throws(CertificateException::class)
                override fun checkServerTrusted(
                    chain: Array<java.security.cert.X509Certificate>,
                    authType: String
                ) {
                }

                override fun getAcceptedIssuers(): Array<java.security.cert.X509Certificate> {
                    return arrayOf()
                }
            })
            val sslContext = SSLContext.getInstance("SSL")
            sslContext.init(null, trustAllCerts, java.security.SecureRandom())
            val sslSocketFactory = sslContext.socketFactory

            val builder = OkHttpClient.Builder().apply {
                sslSocketFactory(sslSocketFactory, trustAllCerts[0] as X509TrustManager)
                hostnameVerifier(HostnameVerifier { _, _ -> true })
            }
            return builder.build()
        } catch (e: Exception) {
            throw RuntimeException(e)
        }
    }
    fun retrofit(): Retrofit {
        return Retrofit.Builder()
            .baseUrl(endPointHttps)
            .client(okHttp())
            .addConverterFactory(convertFactory(objectMapper()))
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .build()
    }
}
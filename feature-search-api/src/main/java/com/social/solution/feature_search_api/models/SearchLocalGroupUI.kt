package com.social.solution.feature_search_api.models

data class SearchLocalGroupUI(
    val id: Long, val nameGroup: String?,
    val lastMessage: String,
    val avatar: String?, var isSubscribe: Boolean, val countParticipant: Int?
) : SelectingItem
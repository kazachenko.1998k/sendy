package com.social.solution.feature_search_api.models

data class SearchTypeLabel(val searchTarget: SearchTarget) : RecyclerItem
package com.social.solution.feature_search_api.models

data class SearchUserMessageUI(
    val id: String, val firstName: String, val lastName: String, val textMessage: String,
    val dateMessage: Long, val ownerMessage: Boolean,
    val avatar: String, var isSubscribe: Boolean
) : SelectingItem
package com.social.solution.feature_search_api.models

data class SearchChannelMessageUI(
    val id: String, val nameChannel: String, val textMessage: String,
    val dateMessage: Long, val ownerMessage: Boolean,
    val avatar: String, var isSubscribe: Boolean
) : SelectingItem
package com.social.solution.feature_search_api

import com.morozov.core_backend_api.chat.Chat


interface FeatureSearchWithChatCallback: FeatureSearchCallback {
    var chat: Chat?
}
package com.social.solution.feature_search_api.models

data class SearchGlobalChannelUI(
    val id: Long, val nameChannel: String?,
    val lastMessage: String,
    val avatar: String?, var isSubscribe: Boolean, val countSubscribers: Int?
) : SelectingItem
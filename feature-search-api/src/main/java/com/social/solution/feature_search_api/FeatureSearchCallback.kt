package com.social.solution.feature_search_api

import com.social.solution.feature_search_api.models.*

interface FeatureSearchCallback {

    fun multiSelectUser(selectedUserList: List<SelectingItem>)
    fun selectLocalUser(searchUserUIList: SearchLocalUserUI)
    fun selectGlobalUser(searchUserUIList: SearchGlobalUserUI)
    fun selectLocalChannel(searchUserUIList: SearchLocalChannelUI)
    fun selectGlobalChannel(searchUserUIList: SearchGlobalChannelUI)
    fun selectLocalGroup(searchUserUIList: SearchLocalGroupUI)
    fun selectGlobalGroup(searchUserUIList: SearchGlobalGroupUI)
    fun selectUserMessage(searchUserUIList: SearchUserMessageUI)
    fun selectChannelMessage(searchUserUIList: SearchChannelMessageUI)
    fun selectGroupMessage(searchUserUIList: SearchGroupMessageUI)
    fun addParticipant()
}
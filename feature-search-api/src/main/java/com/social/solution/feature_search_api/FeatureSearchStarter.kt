package com.social.solution.feature_search_api

import androidx.fragment.app.FragmentManager
import androidx.lifecycle.MutableLiveData
import com.example.util_cache.chat.ChatDao
import com.example.util_cache.user.UserDao
import com.morozov.core_backend_api.FeatureBackendApi
import com.morozov.core_backend_api.chat.Chat
import com.social.solution.feature_search_api.models.ConfigSearch
import com.social.solution.feature_search_api.models.SearchTarget

interface FeatureSearchStarter {

    fun start(
        manager: FragmentManager,
        parentContainer: Int,
        addToBackStack: Boolean,
        config: ConfigSearch,
        callback: FeatureSearchCallback,
        userDao: UserDao,
        chatDao: ChatDao,
        backendApi: FeatureBackendApi,
        networkState: MutableLiveData<Boolean>
    )
}

package com.social.solution.feature_search_api.models

data class SearchGroupMessageUI(
    val id: String, val nameGroup: String, val textMessage: String,
    val dateMessage: Long, val ownerMessage: Boolean,
    val avatar: String, var isSubscribe: Boolean
) : SelectingItem
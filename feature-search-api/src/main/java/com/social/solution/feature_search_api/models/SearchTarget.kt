package com.social.solution.feature_search_api.models

enum class SearchTarget {
    LOCAL_USER,
    LOCAL_CHANNEL,
    LOCAL_GROUP,
    GLOBAL_USER,
    GLOBAL_CHANNEL,
    GLOBAL_GROUP,
    MESSAGE
}
package com.social.solution.feature_search_api

interface FeatureSearchApi {
    fun searchStarter(): FeatureSearchStarter
}
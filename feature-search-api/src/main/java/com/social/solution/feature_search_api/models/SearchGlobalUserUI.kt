package com.social.solution.feature_search_api.models

data class SearchGlobalUserUI(
    val id: Long, val firstName: String?, val lastName: String?, val login: String,
    val dateLastVisit: Long,
    val avatar: String?, var isSubscribe: Boolean, var isSelected: Boolean
) : SelectingItem
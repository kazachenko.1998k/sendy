package com.social.solution.feature_search_api.models

import com.morozov.core_backend_api.chat.Chat

data class ConfigSearch(
    val title: String?,
    val textButton: String?,
    val addParticipant: Boolean,
    val maxCountSearch: Boolean,
    val chat: Chat?,
    val targets: List<SearchTarget>,
    val multiSelection: Boolean,
    val searchView: Boolean,
    val inGroupSearch: Boolean,
    val filteredMe: Boolean
)
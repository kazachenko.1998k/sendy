package com.morozov.feature_chat_api

import androidx.fragment.app.FragmentManager
import androidx.lifecycle.LiveData
import com.example.feature_bottom_picker_api.BottomPickerStarterApi
import com.example.util_cache.message.MessageDao
import com.morozov.core_backend_api.FeatureBackendApi
import com.morozov.core_backend_api.chat.Chat

interface ChatStarterApi {
    fun start(myId: Long,
              manager: FragmentManager, container: Int,
              backendApi: FeatureBackendApi,
              messageDao: MessageDao,
              bottomPickerApi: BottomPickerStarterApi,
              addToBackStack: Boolean, callback: FeatureChatCallback,
              networkState: LiveData<Boolean>)

    fun startChatWithUser(myId: Long,
                          manager: FragmentManager, container: Int,
                          backendApi: FeatureBackendApi,
                          messageDao: MessageDao,
                          bottomPickerApi: BottomPickerStarterApi,
                          addToBackStack: Boolean, callback: FeatureChatCallback,
                          networkState: LiveData<Boolean>,
                          userId: Long)

    fun startChat(myId: Long,
                  manager: FragmentManager, container: Int,
                  backendApi: FeatureBackendApi,
                  messageDao: MessageDao,
                  bottomPickerApi: BottomPickerStarterApi,
                  addToBackStack: Boolean, callback: FeatureChatCallback,
                  networkState: LiveData<Boolean>,
                  chatId: Long)

    fun startChat(myId: Long,
                  manager: FragmentManager, container: Int,
                  backendApi: FeatureBackendApi,
                  messageDao: MessageDao,
                  bottomPickerApi: BottomPickerStarterApi,
                  addToBackStack: Boolean, callback: FeatureChatCallback,
                  networkState: LiveData<Boolean>,
                  chatModel: Chat)
}
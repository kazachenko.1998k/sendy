package com.morozov.feature_chat_api

interface ChatFeatureApi {
    fun chatStarter(): ChatStarterApi
}
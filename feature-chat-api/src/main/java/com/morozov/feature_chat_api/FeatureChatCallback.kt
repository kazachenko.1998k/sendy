package com.morozov.feature_chat_api

import com.morozov.core_backend_api.chat.Chat
import com.morozov.feature_chat_api.models.api.MediaApiModel
import com.morozov.feature_chat_api.models.api.MessageApiModel
import com.morozov.feature_chat_api.models.callback.MessageCallbackModel

interface FeatureChatCallback {
    fun onDestroy()
    fun onProfileClicked(chat: Chat)
    fun onSendMessage(recipientId: Long, message: MessageApiModel)
    fun getLoadingMessages(recipientId: Long): List<MessageCallbackModel>
}
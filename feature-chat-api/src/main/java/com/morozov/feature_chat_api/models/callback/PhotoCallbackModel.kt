package com.morozov.feature_chat_api.models.callback

data class PhotoCallbackModel(override val filePath: String,
                              override var loadProgress: Int = 0,
                              override var loadedMediaId: String? = null) : MediaCallbackModel
package com.morozov.feature_chat_api.models.callback

data class MessageCallbackModel(val media: MutableList<MediaCallbackModel>, val msg: String? = null)
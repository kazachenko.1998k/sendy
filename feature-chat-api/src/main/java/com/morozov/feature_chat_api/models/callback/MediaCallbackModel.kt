package com.morozov.feature_chat_api.models.callback

interface MediaCallbackModel {
    val filePath: String
    var loadProgress: Int
    var loadedMediaId: String?
}
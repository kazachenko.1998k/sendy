package com.morozov.feature_chat_api.models.api

interface MediaApiModel {
    val filePath: String
}
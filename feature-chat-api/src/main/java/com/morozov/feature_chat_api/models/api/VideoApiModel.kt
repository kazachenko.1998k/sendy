package com.morozov.feature_chat_api.models.api

import com.morozov.feature_chat_api.models.api.MediaApiModel

data class VideoApiModel(override val filePath: String, val sec: Int):
    MediaApiModel
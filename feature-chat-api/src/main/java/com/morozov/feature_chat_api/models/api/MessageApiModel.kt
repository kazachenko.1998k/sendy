package com.morozov.feature_chat_api.models.api

data class MessageApiModel(val media: List<MediaApiModel>, val msg: String? = null)
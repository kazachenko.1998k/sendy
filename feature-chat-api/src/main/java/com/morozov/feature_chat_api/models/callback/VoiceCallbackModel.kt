package com.morozov.feature_chat_api.models.callback

data class VoiceCallbackModel(override val filePath: String, val sec: Int,
                              override var loadProgress: Int = 0,
                              override var loadedMediaId: String? = null) : MediaCallbackModel
package com.morozov.feature_chat_api.models.api

import com.morozov.feature_chat_api.models.api.MediaApiModel

data class PhotoApiModel(override val filePath: String):
    MediaApiModel
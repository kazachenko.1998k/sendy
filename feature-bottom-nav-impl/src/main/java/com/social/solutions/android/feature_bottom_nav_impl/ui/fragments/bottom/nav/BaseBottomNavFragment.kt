package com.social.solutions.android.feature_bottom_nav_impl.ui.fragments.bottom.nav

import android.content.res.ColorStateList
import android.graphics.drawable.Drawable
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.core.content.ContextCompat
import androidx.core.widget.ImageViewCompat
import androidx.fragment.app.Fragment
import com.social.solutions.android.feature_bottom_nav_api.FeatureBottomNavCallback
import com.social.solutions.android.feature_bottom_nav_api.api.DataApi
import com.social.solutions.android.feature_bottom_nav_api.models.BottomItem
import com.social.solutions.android.feature_bottom_nav_impl.R
import com.social.solutions.android.feature_bottom_nav_impl.ui.fragments.bottom.nav.models.BottomUiItem

open class BaseBottomNavFragment: Fragment(), DataApi {

    private val itemsList = mutableListOf<BottomUiItem>()
    protected var mCallback: FeatureBottomNavCallback? = null
    private var lastClickedView: ImageView? = null

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? =
        inflater.inflate(R.layout.fragment_bottom_nav, container, false)

    fun setCallback(callback: FeatureBottomNavCallback) {
        mCallback = callback
    }

    protected fun getDrawable(id: Int): Drawable {
        return resources.getDrawable(id)
    }

    // List of items controlls
    protected fun addBottomItem(itemView: View, imageView: ImageView, item: BottomItem) {
        itemsList.add(BottomUiItem(itemView, imageView, item))
        itemView.setOnClickListener {
            clickEffectOnItem(item)
            mCallback?.onItemClicked(item)
        }
    }

    protected fun removeBottomItem(itemView: View): BottomUiItem? {
        for (uiItem in itemsList) {
            if (uiItem.view == itemView) {
                itemsList.remove(uiItem)
                return uiItem
            }
        }
        return null
    }

    protected fun removeBottomItem(name: String): BottomUiItem? {
        for (uiItem in itemsList) {
            if (uiItem.item.name == name) {
                itemsList.remove(uiItem)
                return uiItem
            }
        }
        return null
    }

    protected fun getBottomItem(name: String): BottomUiItem? {
        for (uiItem in itemsList) {
            if (uiItem.item.name == name) {
                return uiItem
            }
        }
        return null
    }

    protected fun getBottomItem(position: Int): BottomUiItem? {
        if (position <= itemsList.size)
            return itemsList[position]
        return null
    }

    // View helper funcs
    protected fun clickEffectOnItem(item: BottomItem) {
        val bottomUiItem = getBottomItem(item.name)?:return
        for (icon in itemsList) {
            if (icon.item.name == bottomUiItem.item.name) {
                setSelectedItem(icon.image)
            } else {
                setUnselectedItem(icon.image)
            }
        }
    }

    private fun setSelectedItem(view: ImageView) {
        ImageViewCompat.setImageTintList(view, ColorStateList.valueOf(ContextCompat.getColor(context!!, R.color.selected_item)))
    }

    private fun setUnselectedItem(view: ImageView) {
        ImageViewCompat.setImageTintList(view, ColorStateList.valueOf(ContextCompat.getColor(context!!, R.color.unselected_item)))
    }

    // DataApi impl
    override fun getAllIcons(): List<BottomItem> {
        val result = mutableListOf<BottomItem>()
        for (uiItem in itemsList) {
            result.add(uiItem.item)
        }
        return result
    }
}
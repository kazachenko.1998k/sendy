package com.social.solutions.android.feature_bottom_nav_impl.ui.fragments.bottom.nav

import android.os.Bundle
import android.view.View
import com.social.solutions.android.feature_bottom_nav_api.BottomNavFeatureApi
import com.social.solutions.android.feature_bottom_nav_api.models.BottomItem
import com.social.solutions.android.feature_bottom_nav_impl.R
import com.social.solutions.android.feature_bottom_nav_impl.ui.fragments.bottom.nav.api.CustomizeBottomNavFragment

class BottomNavFragment: CustomizeBottomNavFragment(), BottomNavFeatureApi {
    var itemCustomList: List<BottomItem>? = null
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        if (itemCustomList == null)
            initDefaultBottomNav()
        else
            initCustomBottomNav()

        mCallback?.onReady()
    }

    private fun initDefaultBottomNav() {
        addBottomIcon(BottomItem("Feed", getDrawable(R.drawable.ic_item_feed)))
        addBottomIcon(BottomItem("Chat", getDrawable(R.drawable.ic_item_chat)))
        addBottomIcon(BottomItem("Events", getDrawable(R.drawable.ic_item_events)))
        addBottomIcon(BottomItem("Profile", getDrawable(R.drawable.ic_item_profile)))
    }

    private fun initCustomBottomNav() {
        for (bottomItem in itemCustomList!!) {
            addBottomIcon(bottomItem)
        }
    }
}
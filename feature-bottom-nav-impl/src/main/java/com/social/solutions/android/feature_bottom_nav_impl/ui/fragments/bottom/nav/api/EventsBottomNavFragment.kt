package com.social.solutions.android.feature_bottom_nav_impl.ui.fragments.bottom.nav.api

import android.view.View
import com.social.solutions.android.feature_bottom_nav_api.api.EventsApi
import com.social.solutions.android.feature_bottom_nav_api.models.BottomItem
import com.social.solutions.android.feature_bottom_nav_impl.ui.fragments.bottom.nav.BaseBottomNavFragment
import kotlinx.android.synthetic.main.item_bottom_nav.view.*

open class EventsBottomNavFragment: BaseBottomNavFragment(), EventsApi {
    override fun newEvent(icon: BottomItem) {
        val bottomItem = getBottomItem(icon.name)?:return
        bottomItem.view.imageItemBottomNav.setImageDrawable(icon.icon)
        if (icon.newEventCount != null) {
            bottomItem.view.textItemBottomNav.visibility = View.VISIBLE
            bottomItem.view.textItemBottomNav.text = icon.newEventCount.toString()
        } else {
            bottomItem.view.textItemBottomNav.visibility = View.INVISIBLE
        }
    }

    override fun setSelected(icon: BottomItem) {
        clickEffectOnItem(icon)
    }
}
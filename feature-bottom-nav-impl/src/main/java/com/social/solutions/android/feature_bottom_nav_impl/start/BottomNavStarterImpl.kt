package com.social.solutions.android.feature_bottom_nav_impl.start

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import com.social.solutions.android.feature_bottom_nav_api.BottomNavFeatureApi
import com.social.solutions.android.feature_bottom_nav_api.BottomNavStarter
import com.social.solutions.android.feature_bottom_nav_api.FeatureBottomNavCallback
import com.social.solutions.android.feature_bottom_nav_api.models.BottomItem
import com.social.solutions.android.feature_bottom_nav_impl.ui.fragments.bottom.nav.BottomNavFragment

class BottomNavStarterImpl: BottomNavStarter {
    override fun startDefault(manager: FragmentManager, container: Int, addToBackStack: Boolean,
                              callback: FeatureBottomNavCallback): BottomNavFeatureApi {
        val fragment = BottomNavFragment()
        fragment.setCallback(callback)
        startDefault(fragment, manager, container, addToBackStack)
        return fragment
    }

    override fun startCustom(icons: List<BottomItem>, manager: FragmentManager, container: Int, addToBackStack: Boolean,
                             callback: FeatureBottomNavCallback): BottomNavFeatureApi {
        val fragment = BottomNavFragment()
        fragment.setCallback(callback)
        fragment.itemCustomList = icons
        startDefault(fragment, manager, container, addToBackStack)
        return fragment
    }

    private fun startDefault(fragment: Fragment, manager: FragmentManager, container: Int, addToBackStack: Boolean) {
        val transaction = manager.beginTransaction()
        transaction.replace(container, fragment)
        if (addToBackStack)
            transaction.addToBackStack(fragment.tag)
        transaction.commit()
    }
}
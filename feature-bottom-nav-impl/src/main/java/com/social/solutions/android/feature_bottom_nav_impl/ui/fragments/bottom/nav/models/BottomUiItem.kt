package com.social.solutions.android.feature_bottom_nav_impl.ui.fragments.bottom.nav.models

import android.view.View
import android.widget.ImageView
import com.social.solutions.android.feature_bottom_nav_api.models.BottomItem

data class BottomUiItem(val view: View, val image: ImageView, val item: BottomItem)
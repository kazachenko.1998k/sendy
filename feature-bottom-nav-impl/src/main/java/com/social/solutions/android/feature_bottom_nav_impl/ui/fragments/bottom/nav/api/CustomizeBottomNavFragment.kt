package com.social.solutions.android.feature_bottom_nav_impl.ui.fragments.bottom.nav.api

import android.view.LayoutInflater
import android.view.View
import com.social.solutions.android.feature_bottom_nav_api.api.CustomizeApi
import com.social.solutions.android.feature_bottom_nav_api.models.BottomItem
import com.social.solutions.android.feature_bottom_nav_impl.R
import kotlinx.android.synthetic.main.fragment_bottom_nav.*
import kotlinx.android.synthetic.main.item_bottom_nav.view.*

open class CustomizeBottomNavFragment: EventsBottomNavFragment(), CustomizeApi {
    override fun addBottomIcon(icon: BottomItem) {
        val itemView =
            LayoutInflater.from(context).inflate(R.layout.item_bottom_nav, linearBottomNav, false)
        itemView.imageItemBottomNav.setImageDrawable(icon.icon)
        if (icon.newEventCount != null)
            itemView.textItemBottomNav.text = icon.newEventCount.toString()
        else
            itemView.textItemBottomNav.visibility = View.INVISIBLE
        addBottomItem(itemView, itemView.imageItemBottomNav, icon)
        linearBottomNav.addView(itemView)
    }

    override fun replaceIcon(icon: BottomItem) {
        val bottomItem = getBottomItem(icon.name)?:return
        bottomItem.view.imageItemBottomNav.setImageDrawable(icon.icon)
    }

    override fun removeIcon(icon: BottomItem) {
        val uiItem = removeBottomItem(icon.name)?:return
        linearBottomNav.removeView(uiItem.view)
    }
}
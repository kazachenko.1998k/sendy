package com.social.solution.feature_create_chat.ui.picker.adapter.holders

import android.view.View
import com.social.solution.feature_create_chat.ui.picker.CountryPickerFragment
import com.social.solution.feature_create_chat.ui.picker.adapter.holders.BaseCountryViewHolder
import com.social.solution.feature_create_chat.ui.picker.adapter.models.CountryItemModel
import kotlinx.android.synthetic.main.add_contact_item_country.view.*

class CountryViewHolder(itemView: View): BaseCountryViewHolder(itemView) {
    override fun populate(countryItemModel: CountryItemModel, listener: View.OnClickListener) {
        val countryModel = countryItemModel.countryModel ?: return
        itemView.imageFlag.setImageResource(countryModel.fragId)
        itemView.textCountryName.text = countryModel.countryName
        itemView.setOnClickListener {
            CountryPickerFragment.lastCountryName = countryModel.countryNameCode
            listener.onClick(it)
        }
    }
}
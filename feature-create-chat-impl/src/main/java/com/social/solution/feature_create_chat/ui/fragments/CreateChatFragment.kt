package com.social.solution.feature_create_chat.ui.fragments


import android.annotation.SuppressLint
import android.content.Context
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.activity.OnBackPressedCallback
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import com.example.util_cache.user.saveOrUpdateContactMini
import com.morozov.core_backend_api.user.ContactMiniDB
import com.social.solution.feature_create_chat.MainObject
import com.social.solution.feature_create_chat.MainObject.TAG
import com.social.solution.feature_create_chat.MainObject.mUserApi
import com.social.solution.feature_create_chat.R
import com.social.solution.feature_create_chat.models.CreateChatOwnerUI
import com.social.solution.feature_create_chat.ui.adapters.CreateChatAdapter
import com.social.solution.feature_create_chat.utils.convertContactDBToOwnerUserUI
import com.social.solution.feature_create_chat.utils.convertContactServerToOwnerUserUI
import com.social.solution.feature_create_chat.utils.convertOwnerUserUIToContact
import com.social.solution.feature_create_chat.utils.convertOwnerUserUIToContactDB
import kotlinx.android.synthetic.main.create_chat_fragment.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class CreateChatFragment : Fragment() {

    private var localContacts: List<ContactMiniDB>? = null
    private lateinit var adapter: CreateChatAdapter
    private lateinit var mCreateViewModel: CreateChatViewModel

    companion object {
        const val NAME = "CREATE_CHAT_FRAGMENT"
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        val callback: OnBackPressedCallback = object : OnBackPressedCallback(true) {
            override fun handleOnBackPressed() {
                fragmentManager?.popBackStack()
            }
        }
        requireActivity().onBackPressedDispatcher.addCallback(
            this,
            callback
        )
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        setHasOptionsMenu(true)
        mCreateViewModel = ViewModelProviders.of(this).get(CreateChatViewModel::class.java)
        return inflater.inflate(R.layout.create_chat_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initListeners()
        GlobalScope.launch(Dispatchers.Main) {
            setupAdapter()

            localContacts = loadContactFromDB()
            updateContacts(localContacts?.mapNotNull { convertContactDBToOwnerUserUI(it) })
            Log.d(TAG, "In local DB = " + localContacts)

            loadUserList()

            val accessContact = MainObject.mCallback?.getPermissionContact()
            if (accessContact != null && accessContact) {
                getContactBook()
            }
        }
    }

    private fun initListeners() {
        button_share?.setOnClickListener {
            MainObject.mCallback?.shareApp()
        }
        button_add_channel?.setOnClickListener {
            MainObject.mCallback?.createChannel()
        }
        button_add_group?.setOnClickListener {
            MainObject.mCallback?.createGroup()
        }
        button_add_contact?.setOnClickListener {
            MainObject.mCallback?.createContact()
        }
        create_chat_action_back?.setOnClickListener { fragmentManager?.popBackStack() }
        create_chat_action_search?.setOnClickListener { MainObject.mCallback?.searchChats() }
    }

    private suspend fun setupAdapter() {
        withContext(Dispatchers.IO) {
            if (context != null)
                adapter = CreateChatAdapter(context!!, mutableListOf(), UserChatCallback())
        }
        create_chat_users_recycler?.adapter = adapter
    }

    fun permissionContact(access: Boolean) {
        if (access) {
            getContactBook()
        }
    }

    private suspend fun loadContactFromDB(): List<ContactMiniDB>? {
        return MainObject.mUserDao?.getAllContactsMini()
    }

    @SuppressLint("CheckResult")
    private fun loadUserList() {
        mCreateViewModel.loadContacts(mUserApi)?.subscribe({ contacts ->
            // ищем всех кого нет в локальной бд контактов
            val updateList = contacts.mapNotNull { convertContactServerToOwnerUserUI(it) }
            Log.d(TAG, "Contacts from server (filtered by new) = $updateList")
            // добавляем на экран недостающих юзеров
            updateContacts(contacts.mapNotNull { convertContactServerToOwnerUserUI(it) })
            // пополняем локальную базу
            insertContactServerUser(updateList.map { convertOwnerUserUIToContactDB(it) })
        }, { t ->
            Log.e(TAG, "EXCEPTION GET_CONTACTS")
            t.printStackTrace()
        })
    }

    private fun getContactBook() {
        GlobalScope.launch(Dispatchers.Main) {
            withContext(Dispatchers.IO) {
                if (context != null)
                    mCreateViewModel.syncContacts(context!!, mUserApi)
                        ?.subscribe({ contacts ->
                            // ищем всех кого нет в локальной бд
                            val contactsUI =
                                contacts.mapNotNull { convertContactServerToOwnerUserUI(it) }
                            Log.d(TAG, "Import contacts = $contactsUI")
                            //выводим на экран
                            updateContacts(contactsUI)
                            // инсертим в локальную базу
                            insertContactBookUser(contactsUI.map { convertOwnerUserUIToContactDB(it) })
                        }, { t ->
                            Log.e(TAG, "EXCEPTION IMPORT CONTACT")
                            t.printStackTrace()
                        })
            }
        }
    }

    private fun insertContactBookUser(contactMiniDB: List<ContactMiniDB>) {
        GlobalScope.launch(Dispatchers.IO) {
            contactMiniDB.forEach { contactDB ->
                MainObject.mUserDao?.saveOrUpdateContactMini(contactDB)?.subscribe({
                    Log.d(TAG, "Contacts from my contacts book INSERT/UPDATE in DB = $contactDB")
                }, { t -> t.printStackTrace() })
            }
        }
    }

    private fun insertContactServerUser(contactMiniDB: List<ContactMiniDB>) {
        GlobalScope.launch(Dispatchers.IO) {
            contactMiniDB.forEach { contactDB ->
                MainObject.mUserDao?.saveOrUpdateContactMini(contactDB)?.subscribe({
                    Log.d(TAG, "New contact received from server INSERTED in DB = $contactDB")
                }, { t -> t.printStackTrace() })
            }
        }
    }

    private fun updateContacts(contacts: List<CreateChatOwnerUI>?) {
        if (!contacts.isNullOrEmpty()) {
            if (adapter.data.isEmpty()) {
                adapter.data.addAll(contacts)
            } else {
                contacts.forEach { newContact ->
                    val duplicate = adapter.data.find { findElement -> findElement.id == newContact.id }
                    if (duplicate != null) duplicate.override(newContact)
                    else adapter.data.add(newContact)
                }
            }
            adapter.data.sortByDescending { it.dateLastVisit }
            adapter.notifyDataSetChanged()
        }
    }


    inner class UserChatCallback : CreateChatAdapter.ChatUserInterface {
        override fun selectUser(user: CreateChatOwnerUI) {
            val userMini = convertOwnerUserUIToContact(user).userMini
            if (userMini != null) {
                MainObject.mCallback?.openChatP2P(userMini)
            } else {
                Log.e(TAG, "Converted UI model to UserMini return null userMini")
            }
        }
    }
}

package com.social.solution.feature_create_chat.models

data class CreateChatOwnerUI(
    val id: Long,
    var firstName: String?,
    var lastName: String?,
    var login: String?,
    var phone: String,
    var dateLastVisit: Long,
    var avatar: String?,
    var isSubscribe: Boolean
){
    fun override(newContact: CreateChatOwnerUI) {
        this.firstName = newContact.firstName
        this.lastName = newContact.lastName
        this.login = newContact.login
        this.phone = newContact.phone
        this.dateLastVisit = newContact.dateLastVisit
        this.avatar = newContact.avatar
        this.isSubscribe = newContact.isSubscribe
    }
}
package com.social.solution.feature_create_chat.ui.adapters

import android.content.Context
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.load.resource.bitmap.BitmapTransitionOptions
import com.bumptech.glide.request.RequestOptions
import com.morozov.core_backend_api.SignData
import com.social.solution.feature_create_chat.MainObject.TAG
import com.social.solution.feature_create_chat.R
import com.social.solution.feature_create_chat.models.CreateChatOwnerUI
import com.social.solution.feature_create_chat.utils.getStringTimeAgoByLong
import com.social.solutions.android.util_load_send_services.util.addSignUrl
import kotlinx.android.synthetic.main.create_chat_card_user.view.*

class CreateChatAdapter(
    private val context: Context,
    var data: MutableList<CreateChatOwnerUI>,
    private val chatUserCallback: ChatUserInterface
) :
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    companion object {
        const val HEADER_USERS_TYPE = 1
        const val USER_TYPE = 2

        const val THRESHOLD_STATUS = 60 * 5 // delta 5 minutes for status online
    }

    override fun getItemViewType(position: Int): Int {
        return if (position == 0) HEADER_USERS_TYPE else USER_TYPE
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return when (viewType) {

            HEADER_USERS_TYPE -> HeaderViewHolder(
                LayoutInflater.from(parent.context).inflate(
                    R.layout.create_chat_card_header,
                    parent,
                    false
                )
            )
            else -> UserViewHolder(
                LayoutInflater.from(parent.context).inflate(
                    R.layout.create_chat_card_user,
                    parent,
                    false
                )
            )
        }
    }

    override fun getItemCount() = data.size + 1

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        when (holder.itemViewType) {
            HEADER_USERS_TYPE -> (holder as HeaderViewHolder)
            else -> (holder as UserViewHolder).onBind(position - 1)
        }
    }

    inner class UserViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        fun onBind(position: Int) {
            val user = data[position]
            val statusView = itemView.create_chat_status_user
            Glide.with(context).asBitmap()
                .placeholder(R.drawable.create_chat_ic_avatar_placeholder)
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .load(SignData.server_file + user.avatar).apply(RequestOptions().circleCrop())
                .into(itemView.create_chat_user_icon)
            if (!user.firstName.isNullOrEmpty())
                itemView.create_chat_name_user.text = user.firstName
            else
                itemView.create_chat_name_user.text = user.login
            if (System.currentTimeMillis()/1000 - user.dateLastVisit < THRESHOLD_STATUS) {
                statusView.text = "В сети"
                statusView.setTextColor(ContextCompat.getColor(context, R.color.colorAccent))
            } else {
                val date = getStringTimeAgoByLong(user.dateLastVisit * 1000L)
                statusView.text = ("Был в сети $date")
                statusView.setTextColor(ContextCompat.getColor(context, R.color.santas_gray))
            }
            itemView.setOnClickListener {
                chatUserCallback.selectUser(user)
            }
        }
    }

    inner class HeaderViewHolder(view: View) : RecyclerView.ViewHolder(view)

    interface ChatUserInterface {
        fun selectUser(user: CreateChatOwnerUI)
    }
}
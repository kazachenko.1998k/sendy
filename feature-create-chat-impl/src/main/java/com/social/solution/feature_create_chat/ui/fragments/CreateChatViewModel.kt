package com.social.solution.feature_create_chat.ui.fragments

import android.content.ContentResolver
import android.content.Context
import android.provider.ContactsContract
import android.util.Log
import android.view.Gravity
import android.widget.Toast
import androidx.lifecycle.ViewModel
import com.morozov.core_backend_api.AnyInnerRequest
import com.morozov.core_backend_api.AnyRequest
import com.morozov.core_backend_api.contact.Contact
import com.morozov.core_backend_api.phoneContact.PhoneContact
import com.morozov.core_backend_api.user.UserApi
import com.morozov.core_backend_api.user.requests.UserAddContactRequest
import com.morozov.core_backend_api.user.requests.UserImportContactsRequest
import com.social.solution.feature_create_chat.MainObject
import com.social.solution.feature_create_chat.MainObject.TAG
import com.social.solution.feature_create_chat.utils.checkNumberPhone
import com.social.solution.feature_create_chat.utils.cleanNumber
import com.social.solution.feature_create_chat.utils.comparePhone
import com.social.solution.feature_create_chat.utils.dpToPx
import io.reactivex.Single
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext


class CreateChatViewModel : ViewModel() {

    fun loadContacts(
        userApi: UserApi?
    ): Single<List<Contact>>? {
        return Single.create { single ->
            userApi?.getContacts {
                if (it.result) {
                    if (it.data != null)
                        single.onSuccess(it.data!!.contacts)
                    else
                        single.onError(Exception("DATA IS NULL"))
                } else {
                    single.onError(Exception(it.error?.msg))
                }
            }
        }
    }

    fun syncContacts(context: Context, userApi: UserApi?): Single<MutableList<Contact>>? {
        val list = getLocalContacts(context)
        val importContactRequest = UserImportContactsRequest(list.toMutableList())
        val anyInnerRequest = AnyInnerRequest(importContactRequest)
        val anyRequest = AnyRequest(anyInnerRequest)
        return Single.create { single ->
            if (list.isNotEmpty()) {
                userApi?.importContacts(anyRequest) {
                    if (it.result) {
                        if (it.data != null) {
                            val contacts = it.data!!.contacts
                            val result = contacts.map { contact ->
                                val myContact = list.find { search ->
                                    comparePhone(
                                        contact.phone,
                                        search.phone
                                    )
                                }
                                if (myContact != null) {
                                    contact.userMini?.firstName = myContact.firstName
                                }
                                contact
                            }.toMutableList()
                            single.onSuccess(result)
                        } else
                            single.onError(Exception("Import contact return data == null"))
                    } else {
                        single.onError(Exception(it.error?.msg))
                    }
                }
            } else {
                single.onError(Exception("EmptyContactList"))
            }
        }
    }

    inner class AddSelfException : java.lang.Exception()

    fun addContact(phone: String, name: String, userApi: UserApi?): Single<Contact> {
        val phoneContact = PhoneContact()
        phoneContact.phone = phone
        phoneContact.firstName = name
        Log.d(TAG, "Add contact by phone: $phone and name: $name")
        val addContactRequest = UserAddContactRequest(phoneContact)
        val anyInnerRequest = AnyInnerRequest(addContactRequest)
        val anyRequest = AnyRequest(anyInnerRequest)
        return Single.create { single ->
            userApi?.addContact(anyRequest) {
                if (it.result) {
                    if (it.data != null) {
                        if (it.data!!.contact != null)
                            single.onSuccess(it.data!!.contact!!)
                        else
                            single.onError(Exception("Not found contact"))

                    } else {
                        if (it.error != null && it.error!!.code == 401){
                            single.onError(AddSelfException())
                        }else{
                            single.onError(Exception("Data in add contact request is null"))
                        }
                    }
                } else {
                    if (it.error != null && it.error!!.code == 401){
                        single.onError(AddSelfException())
                    }else{
                        single.onError(Exception(it.error?.msg))
                    }
                }
            }
        }
    }

    private fun getLocalContacts(context: Context): List<PhoneContact> {
        val cr: ContentResolver = context.contentResolver
        val contactList = mutableListOf<PhoneContact>()
        val cur = cr.query(
            ContactsContract.Contacts.CONTENT_URI,
            null, null, null, null
        )
        if (cur != null && cur.count > 0) {
            while (cur.moveToNext()) {
                try {
                    val id: String = cur.getString(
                        cur.getColumnIndex(ContactsContract.Contacts._ID)
                    )
                    val name: String = cur.getString(
                        cur.getColumnIndex(
                            ContactsContract.Contacts.DISPLAY_NAME
                        )
                    )
                    if (cur.getInt(
                            cur.getColumnIndex(
                                ContactsContract.Contacts.HAS_PHONE_NUMBER
                            )
                        ) > 0
                    ) {
                        val pCur = cr.query(
                            ContactsContract.CommonDataKinds.Phone.CONTENT_URI,
                            null,
                            ContactsContract.CommonDataKinds.Phone.CONTACT_ID + " = ?",
                            arrayOf(id),
                            null
                        )
                        if (pCur != null) {
                            while (pCur.moveToNext()) {
                                val phoneNo: String = pCur.getString(
                                    pCur.getColumnIndex(
                                        ContactsContract.CommonDataKinds.Phone.NUMBER
                                    )
                                )
                                val phoneContact = PhoneContact()
                                phoneContact.phone = phoneNo
                                phoneContact.firstName = name
                                contactList.add(phoneContact)
                            }
                            pCur.close()
                        }
                    }
                } catch (ex: Exception) {
                }
            }
        }
        cur?.close()
        Log.d(TAG, "My Phone book: $contactList")
        val uniqueList =
            contactList.filter { it.phone.length > 8 }.toSet()
                .toList()
        Log.d(TAG, "My Phone book filtered by number: $uniqueList")
        return uniqueList
    }

    suspend fun checkPhoneNumberInLocalDB(number: String): Boolean {
        return withContext(Dispatchers.IO){
        val newCleanNumber = cleanNumber(number)
        val contacts = MainObject.mUserDao?.getAllContactsMini()
        //todo: add full number check and other method in project
        val findNumber = contacts?.find {
            it.phone.contains(
                newCleanNumber.subSequence(
                    4,
                    newCleanNumber.length
                )
            )
        } != null
            findNumber
        }
    }

}

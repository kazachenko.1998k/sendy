package com.social.solution.feature_create_chat.utils

import android.util.Log
import com.morozov.core_backend_api.contact.Contact
import com.morozov.core_backend_api.user.ContactMiniDB
import com.morozov.core_backend_api.user.UserMini
import com.social.solution.feature_create_chat.MainObject
import com.social.solution.feature_create_chat.models.CreateChatOwnerUI

internal fun convertContactServerToOwnerUserUI(contact: Contact): CreateChatOwnerUI? {
    val userMini = contact.userMini
    return if (userMini == null) userMini
    else {
        val create = CreateChatOwnerUI(
            userMini.uid,
            userMini.firstName,
            userMini.secondName,
            userMini.nick,
            contact.phone,
            userMini.lastActive,
            userMini.avatarFileId,
            userMini.isSubscribe
        )
        create
    }
}

internal fun convertOwnerUserUIToContact(createChatOwnerUI: CreateChatOwnerUI): Contact {
    val login = createChatOwnerUI.login ?: " "
    val userMini = UserMini(
        createChatOwnerUI.id,
        createChatOwnerUI.avatar,
        login,
        createChatOwnerUI.firstName,
        createChatOwnerUI.lastName,
        isDeleted = false,
        isConfirmed = false,
        lastActive = createChatOwnerUI.dateLastVisit,
        isSubscribe = createChatOwnerUI.isSubscribe
    )
    return Contact(createChatOwnerUI.phone, userMini)
}

internal fun convertOwnerUserUIToContactDB(createChatOwnerUI: CreateChatOwnerUI): ContactMiniDB {
    val login = createChatOwnerUI.login ?: " "

    return ContactMiniDB(
        createChatOwnerUI.id,
        createChatOwnerUI.avatar,
        login,
        createChatOwnerUI.firstName,
        createChatOwnerUI.lastName,
        isDeleted = false,
        isConfirmed = false,
        lastActive = createChatOwnerUI.dateLastVisit,
        isSubscribe = createChatOwnerUI.isSubscribe
    )
}

internal fun convertContactDBToOwnerUserUI(contact: ContactMiniDB): CreateChatOwnerUI? {
    return CreateChatOwnerUI(
        contact.uid,
        contact.firstName,
        contact.secondName,
        contact.nick,
        contact.phone,
        contact.lastActive,
        contact.avatarFileId,
        contact.isSubscribe
    )
}
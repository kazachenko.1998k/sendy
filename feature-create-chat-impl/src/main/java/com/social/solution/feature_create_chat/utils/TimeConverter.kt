package com.social.solution.feature_create_chat.utils

import android.util.Log
import com.social.solution.feature_create_chat.MainObject.TAG
import java.text.SimpleDateFormat
import java.util.*

private const val SECOND = 1000
private const val MINUTE = SECOND * 60
private const val HOUR = MINUTE * 60
private const val DAY = HOUR * 24

private val months =
    listOf("янв", "фев", "мар", "апр", "мая", "июн", "июл", "авг", "сен", "окт", "ноя", "дек")

fun getStringTimeAgo(date: String): String {
    val sdf = SimpleDateFormat("dd.MM.yyyy HH:mm:ss")
    val timeLong = sdf.parse(date)?.time
    return if (timeLong != null)
        getStringTimeAgoByLong(timeLong)
    else date
}

fun getStringTimeAgoByLong(timeLong: Long): String {
    val timeSDF = SimpleDateFormat("HH:mm")
    val calendarPost = Calendar.getInstance()
    calendarPost.timeInMillis = timeLong
    val calendarCurrent = Calendar.getInstance()
    calendarCurrent.timeInMillis = System.currentTimeMillis()
    val monthIndex = calendarPost.get(Calendar.MONTH)
    val dayIndex = calendarPost.get(Calendar.DAY_OF_MONTH)
    val yearIndex = calendarPost.get(Calendar.YEAR)
    val hourIndex = calendarPost.get(Calendar.HOUR)
    val minuteIndex = calendarPost.get(Calendar.MINUTE)

    val currentMonthIndex = calendarCurrent.get(Calendar.MONTH)
    val currentDayIndex = calendarCurrent.get(Calendar.DAY_OF_MONTH)
    val currentYearIndex = calendarCurrent.get(Calendar.YEAR)
    val currentHourIndex = calendarCurrent.get(Calendar.HOUR)
    val currentMinuteIndex = calendarCurrent.get(Calendar.MINUTE)

    val dDay = currentDayIndex - dayIndex
    val dateLong = Date(timeLong)
    return if (dDay == 0 && currentMonthIndex == monthIndex && yearIndex == currentYearIndex) {
        "Сегодня, ${timeSDF.format(dateLong)}"
    } else {
        if (dDay == 1 && currentMonthIndex == monthIndex && yearIndex == currentYearIndex)
            "Вчера, ${timeSDF.format(dateLong)}"
        else "$dayIndex ${months[monthIndex]} в ${timeSDF.format(dateLong)}"
    }
}
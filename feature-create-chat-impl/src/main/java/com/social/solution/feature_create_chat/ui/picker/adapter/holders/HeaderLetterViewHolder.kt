package com.social.solution.feature_create_chat.ui.picker.adapter.holders

import android.view.View
import com.social.solution.feature_create_chat.ui.picker.adapter.holders.BaseCountryViewHolder
import com.social.solution.feature_create_chat.ui.picker.adapter.models.CountryItemModel
import kotlinx.android.synthetic.main.add_contact_item_header_letter.view.*

class HeaderLetterViewHolder(itemView: View): BaseCountryViewHolder(itemView) {
    override fun populate(countryItemModel: CountryItemModel, listener: View.OnClickListener) {
        itemView.textLetter.text = countryItemModel.firstLetter.toString()
    }
}
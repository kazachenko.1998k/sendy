package com.social.solution.feature_create_chat.utils

import java.util.regex.Matcher
import java.util.regex.Pattern

val listCharsNumber = listOf('0','1','2','3','4','5','6','7','8','9')
fun checkNumberPhone(number: String): Boolean {

    val p: Pattern = Pattern.compile("(\\s*)?(\\+)?([- _():=+]?\\d[- _():=+]?){10,14}(\\s*)?", Pattern.CASE_INSENSITIVE)
    val m: Matcher = p.matcher(number)
    while (m.find()) {
        return false
    }
    return true
}

fun comparePhone(phone1: String, phone2: String): Boolean {
    return (phone1.substring(phone1.length-8, phone1.length) == phone2.substring(phone2.length-8, phone2.length))
}

fun cleanNumber(number: String): String {
    return number.filter { it1 -> listCharsNumber.contains(it1) }
}
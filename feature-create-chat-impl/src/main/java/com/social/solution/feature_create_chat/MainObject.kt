package com.social.solution.feature_create_chat

import androidx.lifecycle.MutableLiveData
import com.example.util_cache.user.UserDao
import com.morozov.core_backend_api.feedMemus.FeedMemusApi
import com.morozov.core_backend_api.user.UserApi
import com.social.solution.feature_create_chat_api.FeatureAddContactCallback
import com.social.solution.feature_create_chat_api.FeatureCreateChatApi
import com.social.solution.feature_create_chat_api.FeatureCreateChatCallback


object MainObject {

    var mNetworkState: MutableLiveData<Boolean>? = null
    var mUserDao: UserDao? = null
    var mAddContactCallback: FeatureAddContactCallback? = null
    var mCallback: FeatureCreateChatCallback? = null
    var mUserApi: UserApi? = null
    var TAG = "CREATE_CHAT_MODULE"
}
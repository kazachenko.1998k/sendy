package com.social.solution.feature_create_chat.ui.picker.adapter.models

import com.hbb20.CCPCountry

fun CCPCountry.convertToCountryItem(): CountryItemModel {
    val countryModel = CountryModel(this.flagID, this.name, this.nameCode, this.phoneCode.toInt())
    return CountryItemModel(this.name[0], countryModel)
}
package com.social.solution.feature_create_chat.ui.picker.adapter.models

import com.social.solution.feature_create_chat.ui.picker.adapter.models.CountryModel

class CountryItemModel(val firstLetter: Char, val countryModel: CountryModel?)
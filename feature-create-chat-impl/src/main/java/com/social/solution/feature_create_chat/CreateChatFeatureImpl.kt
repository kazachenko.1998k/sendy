package com.social.solution.feature_create_chat

import android.content.Context
import com.morozov.core_backend_api.FeatureBackendApi
import com.social.solution.feature_create_chat_api.FeatureCreateChatApi
import com.social.solution.feature_create_chat_api.FeatureCreateChatStarter

class CreateChatFeatureImpl(
    private val starter: FeatureCreateChatStarter,
    private val context: Context,
    private val api: FeatureBackendApi
) : FeatureCreateChatApi {

    override fun createChatStarter(): FeatureCreateChatStarter = starter

}
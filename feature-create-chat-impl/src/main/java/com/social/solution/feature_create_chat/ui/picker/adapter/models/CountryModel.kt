package com.social.solution.feature_create_chat.ui.picker.adapter.models

data class CountryModel(val fragId: Int, val countryName: String, val countryNameCode: String, val countryCode: Int)
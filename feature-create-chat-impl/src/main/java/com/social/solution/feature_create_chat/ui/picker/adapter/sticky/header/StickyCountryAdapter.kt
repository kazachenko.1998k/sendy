package com.social.solution.feature_create_chat.ui.picker.adapter.sticky.header

import android.view.View
import com.social.solution.feature_create_chat.R
import com.social.solution.feature_create_chat.ui.picker.adapter.CountryAdapter
import kotlinx.android.synthetic.main.add_contact_item_header_letter_stick.view.*

class StickyCountryAdapter(countryAdapterInterface: CountryAdapterInterface): CountryAdapter(countryAdapterInterface), StickyHeaderInterface {
    override fun getHeaderPositionForItem(itemPositionCnst: Int): Int {
        var itemPosition = itemPositionCnst
        var headerPosition = 0
        do {
            if (isHeader(itemPosition)) {
                headerPosition = itemPosition
                break
            }
            itemPosition -= 1
        } while (itemPosition >= 0)
        return headerPosition
    }

    override fun getHeaderLayout(headerPosition: Int): Int = R.layout.add_contact_item_header_letter_stick

    override fun bindHeaderData(header: View?, headerPosition: Int) {
        header?:return
        header.textLetterStick.text = data()[headerPosition].firstLetter.toString()
    }

    override fun isHeader(itemPosition: Int): Boolean {
        return data()[itemPosition].countryModel == null
    }

}
package com.social.solution.feature_create_chat.utils

import androidx.recyclerview.widget.RecyclerView

abstract class ListAdapter<T, VH : RecyclerView.ViewHolder>: RecyclerView.Adapter<VH>() {

    private var data: List<T> = ArrayList()

    protected fun data(): List<T> = data

    fun setData(data: List<T>) {
        this.data = data
    }

    fun getData(): List<T> = data

    override fun getItemCount(): Int = data.size
}
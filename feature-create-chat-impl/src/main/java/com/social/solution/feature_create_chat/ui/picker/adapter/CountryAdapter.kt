package com.social.solution.feature_create_chat.ui.picker.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.social.solution.feature_create_chat.ui.picker.adapter.holders.BaseCountryViewHolder
import com.social.solution.feature_create_chat.ui.picker.adapter.holders.CountryViewHolder
import com.social.solution.feature_create_chat.ui.picker.adapter.holders.HeaderLetterViewHolder
import com.social.solution.feature_create_chat.ui.picker.adapter.holders.SearchViewHolder
import com.social.solution.feature_create_chat.ui.picker.adapter.models.CountryItemModel
import com.social.solution.feature_create_chat.ui.picker.adapter.models.HolderTypeModel
import com.social.solution.feature_create_chat.R
import com.social.solution.feature_create_chat.utils.ListAdapter

open class CountryAdapter(private val countryAdapterInterface: CountryAdapterInterface): ListAdapter<CountryItemModel, BaseCountryViewHolder>() {

    private lateinit var searchView: SearchViewHolder
    var listener: View.OnClickListener? = null

    override fun getItemViewType(position: Int): Int {
        if (position==0)
            return HolderTypeModel.SEARCH.value

        return if(data()[position].countryModel == null) {
            HolderTypeModel.HEADER.value
        } else {
            HolderTypeModel.COUNTRY.value
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BaseCountryViewHolder {
        return when(viewType) {
            HolderTypeModel.HEADER.value -> HeaderLetterViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.add_contact_item_header_letter, parent, false))
            HolderTypeModel.SEARCH.value -> SearchViewHolder(LayoutInflater.from(parent.context).inflate(
                R.layout.add_contact_item_search, parent, false))
            else -> CountryViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.add_contact_item_country, parent, false))
        }
    }

    override fun onBindViewHolder(holder: BaseCountryViewHolder, position: Int) {
        if (position == 0) {
            searchView = (holder as SearchViewHolder)
            searchView.populate(countryAdapterInterface)
        }
        else
            listener?.let { holder.populate(data()[position], it) }
    }

    interface CountryAdapterInterface {
        fun search(exp: String)
    }
}
package com.social.solution.feature_create_chat.ui.picker

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import com.hbb20.CCPCountry
import com.hbb20.CountryCodePicker
import com.reddit.indicatorfastscroll.FastScrollItemIndicator
import com.social.solution.feature_create_chat.R
import com.social.solution.feature_create_chat.ui.picker.adapter.CountryAdapter
import com.social.solution.feature_create_chat.ui.picker.adapter.models.CountryItemModel
import com.social.solution.feature_create_chat.ui.picker.adapter.models.convertToCountryItem
import com.social.solution.feature_create_chat.ui.picker.adapter.sticky.header.StickyCountryAdapter
import com.social.solution.feature_create_chat.ui.picker.adapter.sticky.header.StickyItemDecoration
import com.social.solution.feature_create_chat.utils.hideKeyboard
import kotlinx.android.synthetic.main.add_contact_fragment_auth_country_picker.*
import java.util.*

class CountryPickerFragment : Fragment() {

    companion object {
        var ccp: CountryCodePicker? = null
        var lastCountryName: String? = null
        const val NAME = "COUNTRY_PICKER_ADD_CONTACT"
    }

    lateinit var adapter: StickyCountryAdapter

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? =
        inflater.inflate(R.layout.add_contact_fragment_auth_country_picker, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        linearBack.setOnClickListener {
            activity?.onBackPressed()
        }

        adapter = StickyCountryAdapter(CountryCallback())

        adapter.listener = View.OnClickListener {
            activity?.onBackPressed()
        }
        val tmpAdapter = adapter
        if (ccp != null) {
            adapter.setDataCCP(getMasterCountries(ccp!!))
            adapter.notifyDataSetChanged()
        }
        recyclerCountry.adapter = adapter
        recyclerCountry.layoutManager = LinearLayoutManager(context)
        recyclerCountry.addItemDecoration(StickyItemDecoration(tmpAdapter))

        recyclerCountry.setOnTouchListener { v, event ->
            hideKeyboard(context!!)
            return@setOnTouchListener false
        }

        fastScrollerView.setupWithRecyclerView(
            recyclerCountry,
            { position ->
                val item = adapter.getData()[position] // Get your model object
                // or fetch the section at [position] from your database
                FastScrollItemIndicator.Text(
                    item.firstLetter.toString().toUpperCase() // Grab the first letter and capitalize it
                ) // Return a text indicator
            }
        )
    }

    inner class CountryCallback : CountryAdapter.CountryAdapterInterface {
        override fun search(exp: String) {
            applyQuery(exp)
        }
    }

    private fun applyQuery(query: String) {
        var queryTmp = query
        queryTmp = queryTmp.toLowerCase()
        //if query started from "+" ignore it
        if (queryTmp.isNotEmpty() && queryTmp[0] == '+') {
            queryTmp = queryTmp.substring(1)
        }
        val filteredCountries = getFilteredCountries(queryTmp) ?: return
        if (filteredCountries.isEmpty())
            return
        val lastSize = adapter.itemCount
        adapter.setDataCCP(filteredCountries)
        adapter.notifyItemRangeRemoved(1, lastSize-1)
        adapter.notifyItemRangeInserted(1, adapter.itemCount-1)
    }

    private fun CountryAdapter.setDataCCP(list: List<CCPCountry>) {
        val data = mutableListOf<CountryItemModel>()
        data.add(CountryItemModel(' ', null))
        var letter = ' '
        for (ccpCountry in list) {
            val tmpChar = ccpCountry.name[0].toUpperCase()
            if (letter != tmpChar) {
                letter = tmpChar
                data.add(CountryItemModel(letter, null))
            }
            data.add(ccpCountry.convertToCountryItem())
        }
        this.setData(data)
    }

    private fun getFilteredCountries(query: String): List<CCPCountry>? {
        if (ccp == null) return null
        val tempCCPCountryList = mutableListOf<CCPCountry>()
        for (item in getMasterCountries(ccp!!)) {
            if (item.isEligibleForQuery(query)) {
                tempCCPCountryList.add(item)
            }
        }
        return tempCCPCountryList
    }

    private fun getMasterCountries(codePicker: CountryCodePicker): List<CCPCountry> {
        return CCPCountry.getLibraryMasterCountryList(context, codePicker.languageToApply)
    }

    private fun CCPCountry.isEligibleForQuery(query: String): Boolean {
        var query = query
        query = query.toLowerCase()
        return containsQueryWord("Name", name, query) ||
                containsQueryWord("NameCode", nameCode, query) ||
                containsQueryWord("PhoneCode", phoneCode, query) ||
                containsQueryWord("EnglishName", englishName, query)
    }

    private fun containsQueryWord(
        fieldName: String,
        fieldValue: String?,
        query: String?
    ): Boolean {
        return try {
            if (fieldValue == null || query == null) {
                false
            } else {
                fieldValue.toLowerCase(Locale.ROOT).contains(query)
            }
        } catch (e: Exception) {
            Log.w(
                "CCPCountry", fieldName + ":" + fieldValue +
                        " failed to execute toLowerCase(Locale.ROOT).contains(query) " +
                        "for query:" + query
            )
            false
        }
    }


}
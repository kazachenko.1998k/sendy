package com.social.solution.feature_create_chat

import android.content.Context
import com.morozov.core_backend_api.FeatureBackendApi
import com.social.solution.feature_create_chat.start.CreateChatStarterImpl
import com.social.solution.feature_create_chat_api.FeatureCreateChatApi

object LibCreateChatDependency {

    fun featureCreateChatApi(context: Context, backend: FeatureBackendApi): FeatureCreateChatApi {
        return CreateChatFeatureImpl(CreateChatStarterImpl(), context, backend)
    }
}
package com.social.solution.feature_create_chat.start

import androidx.fragment.app.FragmentManager
import androidx.lifecycle.MutableLiveData
import com.example.util_cache.user.UserDao
import com.morozov.core_backend_api.user.UserApi
import com.social.solution.feature_create_chat.MainObject
import com.social.solution.feature_create_chat.ui.fragments.AddContactFragment
import com.social.solution.feature_create_chat.ui.fragments.CreateChatFragment
import com.social.solution.feature_create_chat_api.FeatureAddContactCallback
import com.social.solution.feature_create_chat_api.FeatureCreateChatCallback
import com.social.solution.feature_create_chat_api.FeatureCreateChatStarter

class CreateChatStarterImpl : FeatureCreateChatStarter {


    private lateinit var fragment: CreateChatFragment

    override fun start(
        manager: FragmentManager,
        parentContainer: Int,
        addToBackStack: Boolean,
        callback: FeatureCreateChatCallback,
        userApi: UserApi,
        userDao: UserDao,
        networkState: MutableLiveData<Boolean>
    ) {
        MainObject.mCallback = callback
        MainObject.mUserApi = userApi
        MainObject.mUserDao = userDao
        MainObject.mNetworkState = networkState
        fragment = CreateChatFragment()
        manager.beginTransaction()
            .replace(parentContainer, fragment)
            .addToBackStack(null)
            .commit()
    }

    override fun startAddContact(
        manager: FragmentManager,
        parentContainer: Int,
        addToBackStack: Boolean,
        callback: FeatureAddContactCallback,
        userApi: UserApi,
        userDao: UserDao,
        networkState: MutableLiveData<Boolean>
    ) {
        MainObject.mAddContactCallback = callback
        MainObject.mUserApi = userApi
        MainObject.mUserDao = userDao
        MainObject.mNetworkState = networkState
        val fragmentAddContact = AddContactFragment(parentContainer)
        manager.beginTransaction()
            .replace(parentContainer, fragmentAddContact)
            .addToBackStack(AddContactFragment.NAME)
            .commit()
    }

    override fun permissionContact(access: Boolean) {
        fragment.permissionContact(access)
    }
}
package com.social.solution.feature_create_chat.ui.fragments


import android.annotation.SuppressLint
import android.content.Context
import android.os.Bundle
import android.util.Log
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.activity.OnBackPressedCallback
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import com.example.util_cache.user.saveOrUpdateContactMini
import com.morozov.core_backend_api.contact.Contact
import com.social.solution.feature_create_chat.MainObject
import com.social.solution.feature_create_chat.MainObject.TAG
import com.social.solution.feature_create_chat.MainObject.mNetworkState
import com.social.solution.feature_create_chat.R
import com.social.solution.feature_create_chat.ui.picker.CountryPickerFragment
import com.social.solution.feature_create_chat.utils.*
import kotlinx.android.synthetic.main.add_contact_fragment.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch


class AddContactFragment(private val parentContainer: Int) : Fragment() {

    private lateinit var mCreateViewModel: CreateChatViewModel

    companion object {
        const val NAME = "ADD_CONTACT_FRAGMENT"
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        val callback: OnBackPressedCallback = object : OnBackPressedCallback(true) {
            override fun handleOnBackPressed() {
                fragmentManager?.popBackStack()
            }
        }
        requireActivity().onBackPressedDispatcher.addCallback(
            this,
            callback
        )
    }

    var maxRootViewHeight = 0
    var currentRootViewHeight = 0

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        setHasOptionsMenu(true)
        mCreateViewModel = ViewModelProviders.of(this).get(CreateChatViewModel::class.java)
        return inflater.inflate(R.layout.add_contact_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initListeners()

        view.viewTreeObserver
            .addOnGlobalLayoutListener {
                currentRootViewHeight = view.height
                if (currentRootViewHeight > maxRootViewHeight) {
                    maxRootViewHeight = currentRootViewHeight
                }
            }
    }

    @SuppressLint("CheckResult")
    private fun initListeners() {
        ccp?.registerCarrierNumberEditText(edit_text_carrier_number)
        CountryPickerFragment.lastCountryName?.let { ccp.setCountryForNameCode(it) }
        ccp?.overrideClickListener {
            CountryPickerFragment.ccp = ccp
            fragmentManager?.beginTransaction()?.replace(parentContainer, CountryPickerFragment())
                ?.addToBackStack(CountryPickerFragment.NAME)
                ?.commit()
        }
        add_contact_action_back?.setOnClickListener {
            fragmentManager?.popBackStack()
            hideKeyboard(context!!)
        }
        add_contact_fragment_button_continue?.setOnClickListener {
            if (edit_text_carrier_number.text.trim().length < 9) {
                val toast = Toast.makeText(context, "Введите номер телефона", Toast.LENGTH_SHORT)
                toast.setGravity(
                    Gravity.BOTTOM,
                    0,
                    maxRootViewHeight - currentRootViewHeight + dpToPx(80f, resources)
                )
                toast.show()
            } else {
                GlobalScope.launch(Dispatchers.Main) {
                    if (checkExistPhone(edit_text_carrier_number.text.trim().toString())) {
                        val toast = Toast.makeText(
                            context,
                            "Контакт с таким номером телефона уже существует",
                            Toast.LENGTH_SHORT
                        )
                        toast.setGravity(
                            Gravity.BOTTOM,
                            0,
                            maxRootViewHeight - currentRootViewHeight + dpToPx(80f, resources)
                        )
                        toast.show()
                    } else {
                        if (add_contact_input_name.text!!.trim().isNotEmpty()) {
                            addContact()
                        } else {
                            add_contact_input_name.error = "Введите имя"
                        }
                    }
                }
            }
        }
    }

    private suspend fun checkExistPhone(number: String): Boolean {
        return mCreateViewModel.checkPhoneNumberInLocalDB(number)
    }

    @SuppressLint("CheckResult")
    private fun addContact() {
        if (mNetworkState != null && mNetworkState!!.value!!) {
            mCreateViewModel.addContact(
                ccp.fullNumber,
                add_contact_input_name.text!!.trim().toString(),
                MainObject.mUserApi
            ).subscribe({ contact ->
                val userMini = contact.userMini
                Log.d(TAG, "USER_MINI_ADD = " + userMini)
                if (userMini != null) {
                    val toast = Toast.makeText(
                        context,
                        "Контакт добавлен",
                        Toast.LENGTH_SHORT
                    )
                    toast.setGravity(
                        Gravity.BOTTOM,
                        0,
                        dpToPx(
                            72f,
                            resources
                        )
                    )
                    toast.show()
                    hideKeyboard(context!!)
                    Log.d(TAG, "USER_MINI_ADD1")
                    saveContactLocalDB(
                        contact,
                        ccp.fullNumber,
                        add_contact_input_name.text!!.trim().toString()
                    )
                    MainObject.mAddContactCallback?.openChatP2P(contact.userMini!!)
                } else {
                    val toast = Toast.makeText(
                        context,
                        "Данного контакта не существует",
                        Toast.LENGTH_SHORT
                    )
                    toast.setGravity(
                        Gravity.BOTTOM,
                        0,
                        maxRootViewHeight - currentRootViewHeight + dpToPx(80f, resources)
                    )
                    toast.show()
                }
            }, { error: Throwable? ->
                error?.printStackTrace()
                if (error is CreateChatViewModel.AddSelfException) {
                    val toast = Toast.makeText(
                        context,
                        "Нельзя говорить с самим с собой — вас могут не понять",
                        Toast.LENGTH_SHORT
                    )
                    toast.setGravity(
                        Gravity.BOTTOM,
                        0,
                        maxRootViewHeight - currentRootViewHeight + dpToPx(80f, resources)
                    )
                    toast.show()
                } else {
                    val toast = Toast.makeText(
                        context,
                        "Данного контакта не существует",
                        Toast.LENGTH_SHORT
                    )
                    toast.setGravity(
                        Gravity.BOTTOM,
                        0,
                        maxRootViewHeight - currentRootViewHeight + dpToPx(80f, resources)
                    )
                    toast.show()
                }
            })
        }
        else{
            val toast = Toast.makeText(
                context,
                "Нет соединения. Проверьте подключение к интернету",
                Toast.LENGTH_SHORT
            )
            toast.setGravity(
                Gravity.BOTTOM,
                0,
                maxRootViewHeight - currentRootViewHeight + dpToPx(80f, resources)
            )
            toast.show()
        }
    }

    @SuppressLint("CheckResult")
    private fun saveContactLocalDB(
        contact: Contact,
        fullNumber: String,
        name: String
    ) {
        val saveUser = convertOwnerUserUIToContactDB(
            convertContactServerToOwnerUserUI(contact)!!
        )
        saveUser.firstName = name
        saveUser.phone = cleanNumber(fullNumber)

        MainObject.mUserDao?.saveOrUpdateContactMini(saveUser)?.subscribe({
        }, { t ->
            Log.e(TAG, "CAN'T SAVE CONTACT IN LOCAL_DB")
            t.printStackTrace()
        })
    }

}
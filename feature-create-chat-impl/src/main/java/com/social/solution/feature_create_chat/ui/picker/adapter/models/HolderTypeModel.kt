package com.social.solution.feature_create_chat.ui.picker.adapter.models

enum class HolderTypeModel(val value: Int) {
    SEARCH(0),
    HEADER(1),
    COUNTRY(2)
}
package com.social.solution.feature_create_chat.ui.picker.adapter.holders

import android.text.Editable
import android.text.TextWatcher
import android.view.View
import com.social.solution.feature_create_chat.ui.picker.adapter.CountryAdapter
import com.social.solution.feature_create_chat.ui.picker.adapter.models.CountryItemModel
import kotlinx.android.synthetic.main.add_contact_item_header_letter.view.*
import kotlinx.android.synthetic.main.add_contact_item_search.view.*

class SearchViewHolder(itemView: View): BaseCountryViewHolder(itemView) {
    override fun populate(countryItemModel: CountryItemModel, listener: View.OnClickListener) {}

    fun populate(countryAdapterInterface: CountryAdapter.CountryAdapterInterface) {
        itemView.editSearchCountry.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(p0: Editable?) {}
            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}
            override fun onTextChanged(string: CharSequence?, p1: Int, p2: Int, p3: Int) {
                countryAdapterInterface.search(string.toString())
                if (string!=null)
                    if (string.isNotEmpty()) {
                        itemView.imageClear.visibility = View.VISIBLE
                        return
                    }
                itemView.imageClear.visibility = View.GONE
            }
        })

        itemView.imageClear.setOnClickListener {
            itemView.editSearchCountry.text.clear()
        }
    }
}
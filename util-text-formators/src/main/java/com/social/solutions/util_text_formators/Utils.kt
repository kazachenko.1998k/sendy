package com.social.solutions.util_text_formators

import android.annotation.SuppressLint
import android.content.Context
import android.view.View
import java.text.SimpleDateFormat
import java.util.*
import java.util.regex.Pattern


@SuppressLint("SimpleDateFormat")
fun String.convertDateToLong(): Long {
    val df = SimpleDateFormat("dd.MM.yyyy HH:mm")
    return df.parse(this).time
}

@SuppressLint("SimpleDateFormat")
fun Long.convertLongToCurrentType(context: Context): String {
    val thisDay = Calendar.getInstance()
    val morning = Calendar.getInstance()
    morning.add(Calendar.HOUR_OF_DAY, -morning[Calendar.HOUR_OF_DAY])
    morning.add(Calendar.MINUTE, -morning[Calendar.MINUTE])
    morning.add(Calendar.SECOND, -morning[Calendar.SECOND])
    val yesterday = Calendar.getInstance()
    yesterday.add(Calendar.DATE, -1)
    yesterday.add(Calendar.HOUR_OF_DAY, -yesterday[Calendar.HOUR_OF_DAY])
    yesterday.add(Calendar.MINUTE, -yesterday[Calendar.MINUTE])
    yesterday.add(Calendar.SECOND, -yesterday[Calendar.SECOND])
    val weekBefore = Calendar.getInstance()
    weekBefore.add(Calendar.DATE, -6)
    weekBefore.add(Calendar.HOUR_OF_DAY, -weekBefore[Calendar.HOUR_OF_DAY])
    weekBefore.add(Calendar.MINUTE, -weekBefore[Calendar.MINUTE])
    weekBefore.add(Calendar.SECOND, -weekBefore[Calendar.SECOND])
    val startYear = Calendar.getInstance()
    startYear.add(Calendar.DAY_OF_YEAR, -startYear[Calendar.DAY_OF_YEAR])
    startYear.add(Calendar.HOUR_OF_DAY, -startYear[Calendar.HOUR_OF_DAY])
    startYear.add(Calendar.MINUTE, -startYear[Calendar.MINUTE])
    startYear.add(Calendar.SECOND, -startYear[Calendar.SECOND])
    val milliseconds = this * 1000
    return when {
        milliseconds > System.currentTimeMillis() -> ""
        milliseconds > morning.timeInMillis -> milliseconds.convertLongToTime()
        milliseconds > yesterday.timeInMillis -> milliseconds.convertLongToYesterday(context)
        milliseconds > weekBefore.timeInMillis -> milliseconds.convertLongToDayOfWeek()
        milliseconds > startYear.timeInMillis -> milliseconds.convertLongToDayOfYear()
        else -> milliseconds.convertLongToData()
    }
}

@SuppressLint("SimpleDateFormat")
fun Long.convertLongToTime(): String {
    val date = Date(this)
    val format = SimpleDateFormat("HH:mm")
    return format.format(date)
}

@SuppressLint("SimpleDateFormat")
fun Long.convertLongToDayOfWeek(): String {
    val date = Date(this)
    val format = SimpleDateFormat("E")
    return format.format(date)
}

@SuppressLint("SimpleDateFormat")
fun Long.convertLongToDayOfYear(): String {
    val date = Date(this)
    val format = SimpleDateFormat("dd MMM")
    return format.format(date)
}

@SuppressLint("SimpleDateFormat")
fun Long.convertLongToYesterday(context: Context): String {
    return context.getString(R.string.yesterday)
}

@SuppressLint("SimpleDateFormat")
fun Long.convertLongToMonth(): String {
    val date = Date(this)
    val format = SimpleDateFormat("MM.yyyy")
    return format.format(date)
}

@SuppressLint("SimpleDateFormat")
fun Long.convertLongToData(): String {
    val date = Date(this)
    val format = SimpleDateFormat("dd.MM.yyyy")
    return format.format(date)
}

fun Int.convertToShortString(): String {
    return when {
        this >= 10000000 -> "${this / 1000000}M"
        this >= 1000000 -> "${this / 1000000}.${this / 100000 % 10}M"
        this >= 10000 -> "${this / 1000}K"
        this >= 1000 -> "${this / 1000}.${this / 100 % 10}K"
        else -> this.toString()
    }
}

fun Boolean?.convertToVisible() =
    if (this == true) View.VISIBLE
    else View.GONE


@SuppressLint("SimpleDateFormat")
fun convertCalendarToData(date: Calendar): String {
    return String.format("%1\$td.%1\$tm.%1\$tY %1\$tH:%1\$tM", date)
}

fun String.emailCorrect(): Boolean {
    val regExpn = ("^(([\\w-]+\\.)+[\\w-]+|([a-zA-Z]{1}|[\\w-]{2,}))@"
            + "((([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\.([0-1]?"
            + "[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\."
            + "([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\.([0-1]?"
            + "[0-9]{1,2}|25[0-5]|2[0-4][0-9])){1}|"
            + "([a-zA-Z]+[\\w-]+\\.)+[a-zA-Z]{2,4})$")
    val pattern = Pattern.compile(regExpn, Pattern.CASE_INSENSITIVE)
    val matcher = pattern.matcher(this)
    return matcher.matches()
}

fun Long?.isMuteNow(): Boolean =
    this != null && this != 0L && this * 1000 > System.currentTimeMillis()

fun Long?.isOnline(): Boolean =
    this != null && this * 1000 + 5 * 60 * 1000 > System.currentTimeMillis()

fun Char.checkNickValid(): Boolean {
    return Character.isDigit(this) || this.isEnglish() ||
            this == '_'
}

private fun Char.isEnglish(): Boolean {
    if ((this in 'a'..'z') || (this in 'A'..'Z')) {
        return true
    }
    return false
}
package com.social.solutions.android.util_notification_push


object Constants {
    var classMainActivity: Class<*>? = null
    val USER_UID_EMPTY = 0L
    val spKey = "push"
    val action = "newPushEvent"
    val actionReply = "replayAction"
    val channelId = "com.social.solutions.android.sendy.notification"
    val KEY_REPLY = "key_reply"
    val notifyId = 1234565
    val CHAT_ID_FOR_ANSWER = "chat_id"
}
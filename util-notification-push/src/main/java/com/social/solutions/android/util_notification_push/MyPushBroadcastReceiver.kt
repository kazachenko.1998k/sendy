package com.social.solutions.android.util_notification_push

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.util.Log
import androidx.core.app.NotificationManagerCompat
import androidx.core.app.RemoteInput
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.morozov.core_backend_api.AnyInnerRequest
import com.morozov.core_backend_api.AnyRequest
import com.morozov.core_backend_api.message.requests.MessageSendRequest
import com.morozov.core_backend_api.push.PushResponse
import com.morozov.lib_backend.LibBackendDependency
import com.social.solutions.android.util_notification_push.Constants.KEY_REPLY
import com.social.solutions.android.util_notification_push.Constants.notifyId
import com.social.solutions.android.util_notification_push.Constants.spKey


class MyPushBroadcastReceiver : BroadcastReceiver() {

    override fun onReceive(context: Context, intent: Intent) {
        val sp = context.getSharedPreferences(spKey, Context.MODE_PRIVATE)
        val before = sp.getString(spKey, null)
        val type = object : TypeToken<MutableList<PushResponse>>() {}.type
        var list: MutableList<PushResponse>? = try {
            Gson().fromJson(
                before,
                type
            )
        } catch (ex: IllegalStateException) {
            mutableListOf()
        }
        if (list == null) {
            list = mutableListOf()
        }
        if (list.isNotEmpty()) {
            val last = list.last()
            sendReply(intent, last.data?.chat_id)
        }
        Log.d("WebSocketPush", before.toString())
        sp.edit().remove(spKey).apply()
        //Update the notification to show that the reply was received.
        val notificationManager = NotificationManagerCompat.from(context)
        notificationManager.cancel(notifyId)
    }

    private fun sendReply(
        intent: Intent?,
        chatId: Long?
    ) {
        intent ?: return
        intent.clipData ?: return
        Log.d("WebSocketPush", intent.clipData.toString())
        val remoteInput: Bundle = RemoteInput.getResultsFromIntent(intent)
        val reply = remoteInput.getCharSequence(
            KEY_REPLY
        ) ?: return
        Log.d("WebSocketPush", chatId.toString())
        if (chatId != null) {
            try {
                LibBackendDependency.featureBackendApi().messageApi().send(
                    AnyRequest(
                        AnyInnerRequest(
                            MessageSendRequest(
                                chatId,
                                message = reply.toString(),
                                type = 0
                            )
                        )
                    )
                )
            } catch (ex: IllegalStateException) {
                ex.printStackTrace()
            }
        }
    }
}

package com.social.solutions.android.util_notification_push

import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.graphics.Bitmap
import android.graphics.Color
import android.os.Build
import android.util.Log
import androidx.core.app.NotificationCompat
import androidx.core.app.NotificationManagerCompat
import androidx.core.app.Person
import androidx.core.app.RemoteInput
import androidx.core.graphics.drawable.IconCompat
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.google.firebase.messaging.RemoteMessage
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.morozov.core_backend_api.push.PushResponse
import com.morozov.core_backend_api.push.models.Data
import com.morozov.core_backend_api.push.models.Notification
import com.social.solutions.android.util_notification_push.Constants.KEY_REPLY
import com.social.solutions.android.util_notification_push.Constants.USER_UID_EMPTY
import com.social.solutions.android.util_notification_push.Constants.action
import com.social.solutions.android.util_notification_push.Constants.actionReply
import com.social.solutions.android.util_notification_push.Constants.channelId
import com.social.solutions.android.util_notification_push.Constants.classMainActivity
import com.social.solutions.android.util_notification_push.Constants.notifyId
import com.social.solutions.android.util_notification_push.Constants.spKey


class NotificationPush(val context: Context) {

    private fun syncWithShared(push: PushResponse): MutableList<PushResponse> {
        val sp = context.getSharedPreferences(spKey, Context.MODE_PRIVATE)
        val before = sp.getString(spKey, null)
        val type = object : TypeToken<MutableList<PushResponse>>() {}.type
        var list: MutableList<PushResponse>? = try {
            Gson().fromJson(
                before,
                type
            )
        } catch (ex: IllegalStateException) {
            clearData()
            mutableListOf()
        }
        if (list == null) {
            list = mutableListOf()
        }
        list.add(push)
        sp.edit().putString(spKey, Gson().toJson(list)).apply()
        return list
    }

    private fun clearData() {
        val sp = context.getSharedPreferences(spKey, Context.MODE_PRIVATE)
        sp.edit().remove(spKey).apply()
    }


    fun sendNotification(notification: RemoteMessage.Notification, data: Map<String, String>) {
        val push = PushResponse(
            Notification(notification.title.toString(), notification.body, notification.icon),
            Data(
                chat_id = data["chat_id"]?.toLongOrNull(),
                user_id = data["user_id"]?.toLongOrNull(),
                msg_id = data["msg_id"]?.toLongOrNull(),
                status = data["status"]?.toIntOrNull(),
                time_add = data["time_add"]?.toLongOrNull(),
                chat_title = data["chat_title"],
                name = data["name"],
                nick = data["nick"],
                icon_id = data["icon_id"],
                uniq_id = data["uniq_id"],
                type = data["type"].toString()
            )
        )
        Log.d("Firebase", push.toString())
        sendNotification(push)
    }

    /**
     * Create and show a custom notification containing the received messages.
     *
     * @param push new notification data received.
     */
    fun sendNotification(
        push: PushResponse
    ) {
        val list = syncWithShared(push)
        val deleteIntent = Intent(context, MyPushBroadcastReceiver::class.java)
        val activityIntent = Intent(context, classMainActivity)
        val replyIntent: Intent
        activityIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
        activityIntent.action = action
        val pendingIntent =
            PendingIntent.getActivity(context, 0, activityIntent, PendingIntent.FLAG_ONE_SHOT)
        val pendingDeleteIntent =
            PendingIntent.getBroadcast(context, 0, deleteIntent, 0)
        val remoteInput: RemoteInput = RemoteInput.Builder(KEY_REPLY)
            .setLabel("Ваш ответ...")
            .build()
        deleteIntent.action = action

        val replyPendingIntent: PendingIntent?

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            replyIntent = Intent(context, MyPushBroadcastReceiver::class.java)
            replyIntent.action = actionReply
            replyPendingIntent = PendingIntent.getBroadcast(
                context,
                0, replyIntent, 0
            )
        } else {
            replyPendingIntent = pendingIntent
        }

        val replyAction: NotificationCompat.Action =
            NotificationCompat.Action.Builder(
                R.mipmap.ic_launcher,
                "Ответить",
                replyPendingIntent
            ).addRemoteInput(remoteInput).build()

        val messagingStyle =
            NotificationCompat.MessagingStyle(
                Person
                    .Builder()
                    .setName("Вы")
                    .setIcon(IconCompat.createWithResource(context, R.mipmap.ic_launcher))
                    .setKey(getUidSP().toString())
                    .build()
            )

        messagingStyle.conversationTitle = push.notification.title
        for (chatMessage in list) {
            val person = Person
                .Builder()
            var name = chatMessage.data?.name
            if (name.isNullOrEmpty()) {
                name = chatMessage.data?.nick
                name = if (name != null) "@$name" else "Нет данных"
            }
            person.setName(name)
            val icon = chatMessage.data?.icon_id
            if (icon != null) {
                val server = getServerFileSP()
                if (server != null) {
                    val url = getServerFileSP() + icon
                    var bitmap: Bitmap? = null
                    try {
                        bitmap = Glide.with(context)
                            .asBitmap()
                            .load(url)
                            .apply(RequestOptions().circleCrop())
                            .submit()
                            .get()
                    } catch (e: Exception) {
                        e.printStackTrace()
                    }
                    if (bitmap != null) {
                        person.setIcon(IconCompat.createWithBitmap(bitmap))
                    }
                }
            }
            val key = chatMessage.data?.user_id
            if (key != null) {
                person.setKey(key.toString())
            }
            val notificationMessage: NotificationCompat.MessagingStyle.Message =
                NotificationCompat.MessagingStyle.Message(
                    chatMessage.notification.body ?: "",
                    chatMessage.data?.time_add ?: System.currentTimeMillis(),
                    person.build()
                )
            messagingStyle.addMessage(notificationMessage)
        }

        val notificationBuilder =
            NotificationCompat.Builder(context, channelId)
                .setSmallIcon(R.mipmap.ic_launcher)
                .setStyle(messagingStyle)
                .addAction(replyAction)
                .setDeleteIntent(pendingDeleteIntent)
                .setColor(Color.BLUE)
                .setPriority(NotificationCompat.PRIORITY_HIGH)
                .setCategory(NotificationCompat.CATEGORY_MESSAGE)
                .setContentIntent(pendingIntent)
                .setAutoCancel(true)

        val notificationManager = NotificationManagerCompat.from(context)
        notificationManager.notify(notifyId, notificationBuilder.build())
    }

    private fun getUidSP(): Long {
        val sharedPreferences = getMySharedPreferences()
        return sharedPreferences.getLong(context.getString(R.string.user_uid), USER_UID_EMPTY)
    }

    private fun getServerFileSP(): String? {
        val sharedPreferences = getMySharedPreferences()
        return sharedPreferences.getString(context.getString(R.string.server_file), null)
    }

    private fun getMySharedPreferences(): SharedPreferences =
        context.getSharedPreferences(context.getString(R.string.auth), Context.MODE_PRIVATE)

}
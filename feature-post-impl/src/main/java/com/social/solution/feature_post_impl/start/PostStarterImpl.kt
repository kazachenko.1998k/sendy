package com.social.solution.feature_post_impl.start

import androidx.fragment.app.FragmentManager
import androidx.lifecycle.MutableLiveData
import com.morozov.core_backend_api.FeatureBackendApi
import com.morozov.core_backend_api.feed.FeedApi
import com.morozov.core_backend_api.message.Message
import com.morozov.core_backend_api.message.MessageApi
import com.social.solution.feature_post_impl.ui.PostFragment
import com.social.solution.feature_post_api.FeaturePostCallback
import com.social.solution.feature_post_api.FeaturePostStarter
import com.social.solution.feature_post_impl.MainObject
import com.vanniktech.emoji.EmojiManager
import com.vanniktech.emoji.ios.IosEmojiProvider

class PostStarterImpl : FeaturePostStarter {

    private lateinit var fragment: PostFragment

    override fun start(
        manager: FragmentManager,
        container: Int,
        bottomNavContainer: Int,
        addToBackStack: Boolean,
        callback: FeaturePostCallback,
        backendApi: FeatureBackendApi,
        post: Message,
        networkState: MutableLiveData<Boolean>
    ) {
        EmojiManager.install(IosEmojiProvider())

        MainObject.mCallback = callback
        MainObject.mBackendApi = backendApi
        MainObject.mNetworkState = networkState

        fragment = PostFragment(container, post)
        manager.beginTransaction()
            .replace(container, fragment)
            .addToBackStack(PostFragment.NAME)
            .commit()
    }
}
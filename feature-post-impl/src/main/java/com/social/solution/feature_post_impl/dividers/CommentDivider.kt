package com.social.solution.feature_post_impl.dividers

import android.content.Context
import android.graphics.Rect
import android.view.View
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.RecyclerView
import com.social.solution.feature_post_impl.R

class CommentDivider(
    context: Context
) : DividerItemDecoration(context, VERTICAL) {

    init {
        val drawableDivider =
            ContextCompat.getDrawable(
                context,
                R.drawable.post_parrent_comment_recycler_view_decorator
            )
        if (drawableDivider != null)
            setDrawable(drawableDivider)
    }

    override fun getItemOffsets(
        outRect: Rect,
        view: View,
        parent: RecyclerView,
        state: RecyclerView.State
    ) {
        when (parent.getChildAdapterPosition(view)) {
            state.itemCount - 1 -> {
                outRect.set(0, 0, 0, 0)
            }
            else -> super.getItemOffsets(outRect, view, parent, state)
        }

    }
}
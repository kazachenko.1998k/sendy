package com.social.solution.feature_post_impl.ui.adapter

import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.net.Uri
import android.text.Spannable
import android.text.SpannableString
import android.text.style.ForegroundColorSpan
import android.util.Log
import android.view.LayoutInflater.from
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.load.resource.bitmap.BitmapTransitionOptions
import com.bumptech.glide.request.RequestOptions
import com.morozov.core_backend_api.SignData
import com.morozov.core_backend_api.feed.responses.FeedRepliesResponse
import com.morozov.core_backend_api.file.FileModel
import com.morozov.core_backend_api.message.responses.MessageLikeResponse
import com.morozov.core_backend_api.user.UserMini
import com.social.solution.feature_post_api.models.PostCommentUI
import com.social.solution.feature_post_api.models.PostUI
import com.social.solution.feature_post_impl.MainObject.TAG
import com.social.solution.feature_post_impl.R
import com.social.solution.feature_post_impl.utils.*
import com.social.solutions.android.transition_media_pager.manager.MediaPagerManager
import com.social.solutions.android.util_media_pager.models.MediaModel
import com.social.solutions.android.util_media_pager.models.MediaType
import io.reactivex.Single
import kotlinx.android.synthetic.main.post_card_child_comment.view.*
import kotlinx.android.synthetic.main.post_card_parent_comment.view.*
import kotlinx.android.synthetic.main.post_card_post.view.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import java.util.*


class ParentCommentAdapter(
    private val context: Context,
    private val post: PostUI,
    var data: MutableList<PostCommentUI>?,
    private val errorInterface: ErrorLoadingCommentsInterface,
    private val postInterface: PostInterface,
    private val expandInterface: ExpandCommentInterface,
    private val childInterface: ChildCommentInterface,
    private val parentInterface: ParentCommentInterface
) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private var recyclerView: RecyclerView? = null
    private var uiList: MutableList<Pair<Int, PostCommentUI?>> = mutableListOf()

    companion object {
        const val CLICK_TIME_INTERVAL = 500

        const val MAX_CHILD_PREVIEW = 2

        const val PARENT_COMMENT_TYPE = 1
        const val CHILD_COMMENT_TYPE = 2
        const val HEADER_TYPE = 0
        const val EXPAND_CHILD_TYPE = 3
        const val ERROR_LOADING_TYPE = -1
    }

    private var mLastClickTime = System.currentTimeMillis()

    private fun initUiList() {
        uiList.clear()
        uiList.add(Pair(HEADER_TYPE, null))
        if (data == null) uiList.add(Pair(ERROR_LOADING_TYPE, null))
        else
            data!!.forEach { comment ->
                if (!comment.replies.isNullOrEmpty()) {
                    uiList.add(Pair(PARENT_COMMENT_TYPE, comment))
                    var countOtherMessage = comment.replies?.count { it.newMessage }
                    if (countOtherMessage == null) countOtherMessage = 0
                    if (!comment.isExpanded && comment.repliesAmount != null && comment.repliesAmount!!.toInt() - countOtherMessage > MAX_CHILD_PREVIEW) {
                        repeat(MAX_CHILD_PREVIEW) { indexChildComment ->
                            uiList.add(
                                Pair(CHILD_COMMENT_TYPE, comment.replies!![indexChildComment])
                            )
                        }
                        comment.replies?.filter { it.newMessage }?.forEach {
                            uiList.add(
                                Pair(CHILD_COMMENT_TYPE, it)
                            )
                        }
                        uiList.add(Pair(EXPAND_CHILD_TYPE, null))
                    } else repeat(comment.replies!!.size) { indexChildComment ->
                        uiList.add(
                            Pair(
                                CHILD_COMMENT_TYPE,
                                comment.replies!![indexChildComment]
                            )
                        )
                    }
                } else {
                    uiList.add(Pair(PARENT_COMMENT_TYPE, comment))
                }
            }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return when (viewType) {
            EXPAND_CHILD_TYPE -> ExpandViewHolder(
                from(parent.context).inflate(
                    R.layout.post_card_child_expand,
                    parent,
                    false
                )
            )
            HEADER_TYPE -> HeaderViewHolder(
                from(parent.context).inflate(
                    R.layout.post_card_post,
                    parent,
                    false
                )
            )
            CHILD_COMMENT_TYPE -> ChildCommentViewHolder(
                from(parent.context).inflate(
                    R.layout.post_card_child_comment,
                    parent,
                    false
                )
            )
            ERROR_LOADING_TYPE -> ErrorLoadingViewHolder(
                from(parent.context).inflate(
                    R.layout.post_card_error_internet,
                    parent,
                    false
                )
            )
            else -> ParentCommentViewHolder(
                from(parent.context).inflate(
                    R.layout.post_card_parent_comment,
                    parent,
                    false
                )
            )
        }
    }


    fun notifyData() {
        initUiList()
        if (data != null)
            recyclerView?.post { notifyDataSetChanged() }
    }

    override fun onAttachedToRecyclerView(recyclerView: RecyclerView) {
        this.recyclerView = recyclerView
    }

    fun notifyErrorLoad() {
        if (data == null) {
            initUiList()
            notifyItemRangeChanged(1, uiList.size)
        }
    }

    fun refreshAndFocus(dataComments: MutableList<PostCommentUI>, newMessageUI: PostCommentUI) {
        recyclerView?.post {
            notifyItemChanged(0)
        }
        data = dataComments
        initUiList()
        if (data != null) {
            val message = uiList.find { it.second?.idComment == newMessageUI.idComment }
            recyclerView?.post {
                notifyItemInserted(uiList.indexOf(message))
            }
            recyclerView?.post {
                if (message != null)
                    recyclerView?.smoothScrollToPosition(uiList.indexOf(message))
            }
        }
    }

    override fun getItemViewType(position: Int): Int {
        return uiList[position].first
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        when (holder) {
            is HeaderViewHolder -> holder.bind(position)
            is ChildCommentViewHolder -> holder.bind(position)
            is ExpandViewHolder -> holder.bind(position)
            is ErrorLoadingViewHolder -> holder.bind()
            else -> {
                (holder as ParentCommentViewHolder).bind(position)
            }
        }
    }

    override fun getItemCount() = uiList.size

    inner class ErrorLoadingViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        fun bind() {
            itemView.parent_response_text_button.setOnClickListener {
                errorInterface.loadComment()
            }
        }
    }

    inner class ParentCommentViewHolder(view: View) : RecyclerView.ViewHolder(view) {

        fun bind(position: Int) {
            val comment = uiList[position].second
            if (comment != null) {
                refreshLikeState(context, comment)
                val fileId = if (comment.owner?.avatarFileId != null) {
                    (SignData.server_file + comment.owner!!.avatarFileId)
                } else ""
                Glide.with(context).asBitmap().transition(BitmapTransitionOptions.withCrossFade())
                    .placeholder(R.drawable.post_ic_placeholder_avatar)
                    .load(fileId).apply(RequestOptions().circleCrop())
                    .into(itemView.parent_user_icon)
                GlobalScope.launch(Dispatchers.Main) {
                    if (comment.isDeleted != null && comment.isDeleted == true) {
                        itemView.parent_text_comment.text = "Комментарий удален"
                    } else {
                        itemView.parent_text_comment.text = comment.text
                    }
                    if (comment.text != null) {
                        val commentText = SpannableString(comment.text)
                        commentText.split(' ').forEach {
                            if (it.contains('@')) {
                                it.split('@').forEachIndexed() { index1, it1 ->
                                    val pinItem = "@$it1"
                                    if (pinItem.length > 1) {
                                        val count = commentText.length
                                        var index = commentText.indexOf(pinItem, 0)
                                        while (index in 0 until count) {
                                            commentText.setSpan(
                                                ForegroundColorSpan(Color.parseColor("#245DE3")),
                                                index,
                                                index + pinItem.length,
                                                Spannable.SPAN_EXCLUSIVE_EXCLUSIVE
                                            )
                                            itemView.parent_text_comment.text = commentText
                                            if ((index + pinItem.length) < count)
                                                index = commentText.indexOf(
                                                    pinItem, index + pinItem.length
                                                )
                                            else
                                                break
                                        }
                                    }
                                }
                            }
                        }
                    }
                }

                if (comment.countLike == 0) {
                    itemView.parent_count_like.visibility = View.GONE
                } else {
                    itemView.parent_count_like.visibility = View.VISIBLE
                    itemView.parent_count_like.text = comment.countLike.toString()
                }
                itemView.parent_date_comment.text = getStringTimeAgoByLong(comment.date)
                if (comment.owner?.firstName != null)
                    itemView.parent_user_name.text = comment.owner!!.firstName
                else {
                    if (comment.owner?.nick != null)
                        itemView.parent_user_name.text = ("@" + comment.owner!!.nick)
                    else {
                        itemView.parent_user_name.text = null
                    }
                }
                itemView.parent_response_text_button.setOnClickListener {
                    //                    showKeyboard(context)
                    parentInterface.setReplyComment(uiList[position].second!!)
                }
                itemView.parent_like_frame.setOnClickListener {
                    parentInterface.likeComment(comment)?.subscribe({ result ->
                        if (result != null) {
                            comment.isLike = result.user_like
                            comment.countLike = result.likes
                            if (itemView.parent_count_like != null)
                                itemView.parent_count_like.text = result.likes.toString()
                            refreshLikeState(context, comment)
                        }
                    }, { throwable ->
                        throwable.printStackTrace()
                    })
                }
                itemView.parent_user_name.setOnClickListener {
                    childInterface.openProfile(comment.owner)
                }
                itemView.parent_user_icon.setOnClickListener {
                    childInterface.openProfile(comment.owner)
                }
            }
        }

        private fun refreshLikeState(context: Context, comment: PostCommentUI) {
            if (comment.isLike == null) return
            val likeImage = itemView.parent_like_icon
            val countLikeText = itemView.parent_count_like
            if (likeImage != null && countLikeText != null)
                if (comment.isLike!!) {
                    countLikeText.visibility = View.VISIBLE
                    likeImage.setImageResource(R.drawable.psot_ic_like_active)
                    countLikeText.setTextColor(context.resources.getColor(R.color.colorSecondary))
                } else {
                    if (comment.countLike == 0) {
                        countLikeText.visibility = View.GONE
                    }
                    likeImage.setImageResource(R.drawable.post_ic_like_noactive)
                    countLikeText.setTextColor(context.resources.getColor(R.color.santas_gray))
                }
        }
    }

    inner class ChildCommentViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        fun bind(position: Int) {
            val comment = uiList[position].second
            if (comment != null) {
                refreshLikeState(context, comment)
                val fileId = if (comment.owner?.avatarFileId != null) {
                    (SignData.server_file + comment.owner!!.avatarFileId)
                } else ""
                Glide.with(context).asBitmap().transition(BitmapTransitionOptions.withCrossFade())
                    .placeholder(R.drawable.post_ic_placeholder_avatar)
                    .load(fileId).apply(RequestOptions().circleCrop())
                    .into(itemView.child_comment_user_icon)
                GlobalScope.launch(Dispatchers.Main) {
                    if (comment.isDeleted != null && comment.isDeleted == true) {
                        itemView.child_comment_text_comment.text = "Комментарий удален"
                    } else {
                        itemView.child_comment_text_comment.text = comment.text
                    }
                    val commentText = SpannableString(comment.text)
                    commentText.split(' ').forEach {
                        if (it.contains('@')) {
                            it.split('@').forEachIndexed() { index1, it1 ->
                                val pinItem = "@$it1"
                                if (pinItem.length > 1) {
                                    val count = commentText.length
                                    var index = commentText.indexOf(pinItem, 0)
                                    while (index in 0 until count) {
                                        commentText.setSpan(
                                            ForegroundColorSpan(Color.parseColor("#245DE3")),
                                            index,
                                            index + pinItem.length,
                                            Spannable.SPAN_EXCLUSIVE_EXCLUSIVE
                                        )
                                        itemView.child_comment_text_comment.text = commentText
                                        if ((index + pinItem.length) < count)
                                            index = commentText.indexOf(
                                                pinItem, index + pinItem.length
                                            )
                                        else
                                            break
                                    }
                                }
                            }
                        }
                    }
                }
                if (comment.countLike == 0) {
                    itemView.child_comment_count_like.visibility = View.GONE
                } else {
                    itemView.child_comment_count_like.visibility = View.VISIBLE
                    itemView.child_comment_count_like.text = comment.countLike.toString()
                }
                itemView.child_comment_date_comment.text = getStringTimeAgoByLong(comment.date)
                if (comment.owner?.firstName != null)
                    itemView.child_comment_user_name.text = comment.owner!!.firstName
                else {
                    if (comment.owner?.nick != null)
                        itemView.child_comment_user_name.text = ("@" + comment.owner!!.nick)
                    else {
                        itemView.child_comment_user_name.text = null
                    }
                }
                itemView.child_comment_response_text_button.setOnClickListener {
                    if (uiList[position].second != null) {
                        childInterface.setReplyComment(uiList[position].second!!)
//                        showKeyboard(context)
                    }
                }
                itemView.child_comment_like_frame.setOnClickListener {
                    childInterface.likeComment(comment)?.subscribe({ result ->
                        if (result != null) {
                            comment.isLike = result.user_like
                            comment.countLike = result.likes
                            if (itemView.child_comment_count_like != null)
                                itemView.child_comment_count_like.text =
                                    result.likes.toString()
                            refreshLikeState(context, comment)

                        }
                    }, { throwable ->
                        throwable.printStackTrace()
                    })
                }
                itemView.child_comment_user_name.setOnClickListener {
                    childInterface.openProfile(comment.owner)
                }
                itemView.child_comment_user_icon.setOnClickListener {
                    childInterface.openProfile(comment.owner)
                }
            }
        }

        private fun refreshLikeState(context: Context, comment: PostCommentUI) {
            if (comment.isLike == null) return
            val likeImage = itemView.child_comment_like_icon
            val countLikeText = itemView.child_comment_count_like
            if (likeImage != null && countLikeText != null)
                if (comment.isLike!!) {
                    countLikeText.visibility = View.VISIBLE
                    likeImage.setImageResource(R.drawable.psot_ic_like_active)
                    countLikeText.setTextColor(context.resources.getColor(R.color.colorSecondary))
                } else {
                    if (comment.countLike == 0) {
                        countLikeText.visibility = View.GONE
                    }
                    likeImage.setImageResource(R.drawable.post_ic_like_noactive)
                    countLikeText.setTextColor(context.resources.getColor(R.color.santas_gray))
                }
        }
    }

    inner class ExpandViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        fun bind(position: Int) {
            itemView.setOnClickListener {
                val prefix = uiList.subList(0, position - MAX_CHILD_PREVIEW)
                val suffix = uiList.subList(position + 1, uiList.size)
                val parent =
                    uiList.find { it.second?.replies?.find { it1 -> it1.idComment == uiList[position - 1].second?.idComment } != null }
                expandInterface.getAllSubComments(parent!!.second!!)
                    ?.subscribe({ allSubComments ->
                        uiList = (prefix.plus(allSubComments!!.messages!!.messages!!.map {
                            Pair(CHILD_COMMENT_TYPE, convertToCommentUI(it))
                        }).plus(suffix).toMutableList())
                        this@ParentCommentAdapter.notifyItemRangeChanged(
                            position - MAX_CHILD_PREVIEW,
                            10
                        )
                        parent.second!!.isExpanded = true
                    }, { throwable ->
                        throwable.printStackTrace()
                    })
            }
        }
    }

    inner class HeaderViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        fun bind(position: Int) {
            val countComment = if (post.countComment != null) post.countComment else 0
            val caseWord = if (countComment!!.toInt() % 100 !in 10..20) {
                when (countComment.toInt() % 10) {
                    1 -> "Комментарий"
                    in (2..4) -> "Комментария"
                    else -> "Комментариев"
                }
            } else {
                "Комментариев"
            }
            val videoView = itemView.video_layout
            val imageView = itemView.post_image
            val countLikeView = itemView.post_count_like
            val descriptionText = itemView.post_text_description_post
            val textOnVideo = itemView.text_video_time
            val fileLayout = itemView.file_layout
            videoView.visibility = View.GONE
            imageView.visibility = View.GONE
            fileLayout.visibility = View.GONE
            itemView.post_title_comment.text = ("$countComment $caseWord")
            descriptionText.text = post.descriptionPost
            countLikeView.text = post.countLike.toString()
            refreshLikeState(context, post)
            itemView.post_like_frame.setOnClickListener {
                postInterface.likePost(post)?.subscribe({ result ->
                    post.isLiked = result!!.user_like
                    post.countLike = result.likes
                    if (countLikeView != null)
                        countLikeView.text = result.likes.toString()
                    refreshLikeState(context, post)
                    Log.d(TAG, "send id _ " + post.id)
                    postInterface.refreshLike(post.id!!, result.likes!!, result.user_like!!)
                }, { throwable ->
                    throwable.printStackTrace()
                })
            }
            val postContent = post.post!!.firstOrNull()
            val linkFile = postContent?.fileId ?: return
            when (postContent.type) {
                FileModel.FILE_TYPE_IMAGE -> {
                    imageView.visibility = View.VISIBLE
                    videoView.visibility = View.GONE
                    fileLayout.visibility = View.GONE
                    Glide
                        .with(itemView.post_image.context)
                        .asBitmap()
                        .placeholder(R.drawable.post_ic_placeholder)
                        .load(SignData.server_file + linkFile)
                        .into(itemView.post_image)
                    imageView.setOnClickListener {
                        val now = System.currentTimeMillis()
                        if (now - mLastClickTime > CLICK_TIME_INTERVAL) {
                            var name = ""
                            if (post.ownerUI?.firstName != null)
                                name = post.ownerUI!!.firstName!!
                            else {
                                if (post.ownerUI?.nick != null)
                                    name = ("@" + post.ownerUI!!.nick)
                            }

                            val date = getCalendar(post.date)
                            postInterface.showImage(
                                (SignData.server_file + linkFile),
                                name,
                                date
                            )
                            mLastClickTime = now
                        }
                    }
                }
                FileModel.FILE_TYPE_MP4 -> {
                    imageView.visibility = View.GONE
                    videoView.visibility = View.VISIBLE
                    fileLayout.visibility = View.GONE
                    textOnVideo.text = (post.post!!.first().length!! * 1000L).formatMillis()
                    Glide.with(context).asBitmap()
                        .diskCacheStrategy(DiskCacheStrategy.ALL)
                        .load(SignData.server_file + linkFile)
                        .into(itemView.video_choose)
                    videoView.setOnClickListener {
                        val tmpList = mutableListOf<MediaModel>()
                        tmpList.add(
                            MediaModel(
                                MediaType.VIDEO,
                                com.social.solutions.android.util_media_pager.models.FileModel(
                                    SignData.server_file + linkFile,
                                    null
                                )
                            )
                        )
                        val name = if (post.ownerUI?.firstName != null) {
                            post.ownerUI!!.firstName!!
                        } else {
                            if (post.ownerUI?.nick != null) {
                                ("@" + post.ownerUI?.nick)
                            } else {
                                "Неизвестно"
                            }
                        }
                        MediaPagerManager.with(context).setUserName(name).setItems(tmpList).start()
                    }
                }
                FileModel.FILE_TYPE_ANOTHER, FileModel.FILE_TYPE_TXT, FileModel.FILE_TYPE_ZIP, FileModel.FILE_TYPE_AUDIO, FileModel.FILE_TYPE_EXE -> {
                    imageView.visibility = View.GONE
                    videoView.visibility = View.GONE
                    fileLayout.visibility = View.VISIBLE
                    itemView.post_file_name.text = post.post?.first()?.title
                    itemView.post_file_weight_text.text = sizeFile(post.post?.first()?.size)
                    itemView.file_layout.setOnClickListener {
                        val i = Intent(Intent.ACTION_VIEW)
                        i.data = Uri.parse(SignData.server_file + linkFile)
                        ContextCompat.startActivity(context, i, null)
                    }
                }
                else -> {
                    imageView.visibility = View.GONE
                    videoView.visibility = View.GONE
                    fileLayout.visibility = View.GONE
                }
            }

        }

        private fun refreshLikeState(context: Context, post: PostUI) {
            if (post.isLiked == null) return
            val likeImage = itemView.post_like_image
            val countLikeText = itemView.post_count_like
            if (likeImage != null && countLikeText != null)
                if (post.isLiked!!) {
                    countLikeText.visibility = View.VISIBLE
                    likeImage.setImageResource(R.drawable.psot_ic_like_active)
                    countLikeText.setTextColor(context.resources.getColor(R.color.colorSecondary))
                } else {
                    if (post.countLike == 0) {
                        countLikeText.visibility = View.GONE
                    }
                    likeImage.setImageResource(R.drawable.post_ic_like_noactive)
                    countLikeText.setTextColor(context.resources.getColor(R.color.santas_gray))
                }
        }
    }

    interface ErrorLoadingCommentsInterface {
        fun loadComment()
    }

    interface ExpandCommentInterface {
        fun getAllSubComments(parentComment: PostCommentUI): Single<FeedRepliesResponse.Data?>?
    }

    interface ChildCommentInterface {
        fun setReplyComment(replyComment: PostCommentUI)
        fun likeComment(comment: PostCommentUI): Single<MessageLikeResponse.Data?>?
        fun openProfile(owner: UserMini?)
    }

    interface ParentCommentInterface {
        fun setReplyComment(replyComment: PostCommentUI)
        fun likeComment(comment: PostCommentUI): Single<MessageLikeResponse.Data?>?
        fun openProfile(owner: UserMini?)
    }

    interface PostInterface {
        fun showImage(url: String, name: String, date: Calendar)
        fun showVideo(url: String, position: Long, name: String, date: Calendar)
        fun likePost(post: PostUI): Single<MessageLikeResponse.Data?>?
        fun openShareView(post: PostUI)
        fun more(post: PostUI)
        fun refreshLike(id: Long, likes: Int, userLike: Boolean)
    }
}

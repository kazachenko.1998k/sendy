package com.social.solution.feature_post_impl.utils

import com.morozov.core_backend_api.message.Message
import com.social.solution.feature_post_api.models.PostCommentUI
import com.social.solution.feature_post_api.models.PostUI

fun convertToPostUI(post: Message): PostUI {
    val commentCount = post.repliesAmount ?: 0
    val likeCount = post.likes ?: 0
    return PostUI(
        post.id,
        post.owner,
        post.timeAdd,
        post.files,
        post.text,
        post.userLike,
        likeCount,
        commentCount
    )
}

fun convertToCommentUI(post: Message): PostCommentUI {
    val childMessages =
        if (post.replies != null) post.replies!!.messages!!.map { convertToCommentUI(it) }.toMutableList() else null
    return PostCommentUI(
        post.id,
        post.owner,
        post.timeAdd,
        post.files,
        post.text,
        post.userLike,
        post.likes ?: 0,
        post.repliesAmount ?: 0,
        childMessages,
        false,
        post.isDeleted
    )
}

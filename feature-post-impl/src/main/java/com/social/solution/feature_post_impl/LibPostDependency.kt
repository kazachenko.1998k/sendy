package com.social.solution.feature_post_impl

import android.content.Context
import com.morozov.core_backend_api.FeatureBackendApi
import com.social.solution.feature_post_api.FeaturePostApi
import com.social.solution.feature_post_impl.start.PostStarterImpl

object LibPostDependency {

    fun featurePostApi(context: Context, backend: FeatureBackendApi): FeaturePostApi {
        return PostFeatureImpl(PostStarterImpl(), context, backend)
    }
}
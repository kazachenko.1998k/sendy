package com.social.solution.feature_post_impl

import android.content.Context
import com.morozov.core_backend_api.FeatureBackendApi
import com.social.solution.feature_post_api.FeaturePostApi
import com.social.solution.feature_post_api.FeaturePostStarter

class PostFeatureImpl(
    private val starter: FeaturePostStarter,
    private val context: Context,
    private val api: FeatureBackendApi
) : FeaturePostApi {

    override fun postStarter(): FeaturePostStarter = starter

}
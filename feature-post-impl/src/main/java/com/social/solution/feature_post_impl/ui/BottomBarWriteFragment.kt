package com.social.solution.feature_post_impl.ui


import android.content.Context
import android.graphics.Color
import android.os.Bundle
import android.text.Editable
import android.text.Spannable
import android.text.SpannableString
import android.text.TextWatcher
import android.text.style.ForegroundColorSpan
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.activity.OnBackPressedCallback
import androidx.fragment.app.Fragment
import com.social.solution.feature_post_api.models.PostCommentUI
import com.social.solution.feature_post_api.models.PostUI
import com.social.solution.feature_post_impl.R
import com.social.solution.feature_post_impl.utils.hideKeyboard
import com.social.solution.feature_post_impl.utils.showKeyboard
import com.vanniktech.emoji.EmojiPopup
import kotlinx.android.synthetic.main.post_write_comment_fragment.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext


class BottomBarWriteFragment(private val post: PostUI, private val callback: WriteCallback) :
    Fragment() {

    var replyComment: PostCommentUI? = null

    companion object {
        const val NAME = "BOTTOM_BAR_WRITE_FRAGMENT"
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        val callback: OnBackPressedCallback = object : OnBackPressedCallback(true) {
            override fun handleOnBackPressed() {
                fragmentManager?.popBackStack()
            }
        }
        requireActivity().onBackPressedDispatcher.addCallback(
            this,
            callback
        )
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.post_write_comment_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        bottom_writer_edit_message?.addTextChangedListener(object : TextWatcher {
            var mutex = false
            override fun afterTextChanged(editable: Editable?) {
//                if (!mutex) {
//                    mutex = true
//                    GlobalScope.launch(Dispatchers.Main) {
                        if (editable.isNullOrEmpty()) bottom_writer_button_send_message.visibility =
                            View.GONE
                        else bottom_writer_button_send_message.visibility = View.VISIBLE
//                        withContext(Dispatchers.IO) {
//                            editable.toString().split(' ').forEach {
//                                if (it.contains('@')) {
//                                    it.split('@').forEachIndexed() { index1, it1 ->
//                                        val pinItem = "@$it1"
//                                        if (pinItem.length > 1) {
//                                            val count = editable!!.length
//                                            var index = editable.indexOf(pinItem, 0)
//                                            while (index in 0 until count) {
//                                                editable.setSpan(
//                                                    ForegroundColorSpan(Color.parseColor("#245DE3")),
//                                                    index,
//                                                    index + pinItem.length,
//                                                    Spannable.SPAN_EXCLUSIVE_EXCLUSIVE
//                                                )
//                                                if ((index + pinItem.length) < count)
//                                                    index = editable.indexOf(
//                                                        pinItem, index + pinItem.length
//                                                    )
//                                                else
//                                                    break
//                                            }
//                                        }
//                                    }
//                                }
//                            }
//                        }
//                    }
//                    mutex = false
//                }

            }

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, count: Int, p3: Int) {
            }

        })
        bottom_writer_button_send_message?.setOnClickListener {
            bottom_writer_image_emoji.setImageResource(R.drawable.post_ic_emoji)
            callback.sendMessage(bottom_writer_edit_message.text.toString(), replyComment)
        }
        val emojiPopup = EmojiPopup.Builder.fromRootView(bottom_writer_container)
            .setOnEmojiPopupDismissListener {
                bottom_writer_image_emoji?.setImageResource(R.drawable.post_ic_emoji)
            }.build(bottom_writer_edit_message)
        bottom_writer_image_emoji?.setOnClickListener {
            if (emojiPopup.isShowing) {
                bottom_writer_image_emoji.setImageResource(R.drawable.post_ic_emoji)
                emojiPopup.dismiss()
            } else {
                bottom_writer_image_emoji.setImageResource(R.drawable.post_ic_keyboard)
                emojiPopup.toggle()
            }
        }
        reply_view?.setOnClickListener {
            setText("")
        }
    }

    fun setText(text: String) {
        bottom_writer_edit_message?.setText(text)
        name_reply?.text = ""
        reply_view?.visibility = View.GONE
        this.replyComment = null
        hideKeyboard(context!!)
    }

    fun setReply(replyComment: PostCommentUI) {
        this.replyComment = replyComment
        reply_view?.visibility = View.VISIBLE
        if (replyComment.owner?.firstName != null)
            name_reply?.text = replyComment.owner?.firstName
        else {
            if (replyComment.owner?.nick != null)
                name_reply?.text = ("@" + replyComment.owner?.nick)
            else {
                name_reply?.text = " "
            }
        }
        setPrefix()
        bottom_writer_edit_message.requestFocus()
        showKeyboard(this.context!!)
    }

    private fun setPrefix() {
        if (replyComment?.owner?.nick != null) {
            val wordToSpan: Spannable = SpannableString("@${replyComment?.owner?.nick}, ")
            wordToSpan.setSpan(
                ForegroundColorSpan(Color.parseColor("#245DE3")),
                0,
                1 + replyComment!!.owner!!.nick.length,
                Spannable.SPAN_MARK_MARK
            )
            bottom_writer_edit_message?.setText(wordToSpan)
            bottom_writer_edit_message?.setSelection(bottom_writer_edit_message.text!!.length)
        }
    }

    interface WriteCallback {
        fun sendMessage(text: String, replyComment: PostCommentUI?)
    }

}
package com.social.solution.feature_post_impl

import androidx.lifecycle.MutableLiveData
import com.morozov.core_backend_api.FeatureBackendApi
import com.morozov.core_backend_api.feed.FeedApi
import com.morozov.core_backend_api.feedMemus.FeedMemusApi
import com.morozov.core_backend_api.message.MessageApi
import com.social.solution.feature_post_api.FeaturePostCallback


object MainObject {

    lateinit var mNetworkState: MutableLiveData<Boolean>
    var mCallback: FeaturePostCallback? = null
    var mBackendApi: FeatureBackendApi? = null
    var TAG = "POST_MODULE"
}
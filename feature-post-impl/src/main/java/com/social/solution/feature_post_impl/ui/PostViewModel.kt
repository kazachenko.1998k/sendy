package com.social.solution.feature_post_impl.ui

import android.annotation.SuppressLint
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.morozov.core_backend_api.AnyInnerRequest
import com.morozov.core_backend_api.AnyRequest
import com.morozov.core_backend_api.FeatureBackendApi
import com.morozov.core_backend_api.feed.requests.FeedRepliesRequest
import com.morozov.core_backend_api.feed.responses.FeedRepliesResponse
import com.morozov.core_backend_api.message.Message
import com.morozov.core_backend_api.message.requests.MessageLikeRequest
import com.morozov.core_backend_api.message.requests.MessageSendRequest
import com.morozov.core_backend_api.message.responses.MessageLikeResponse
import com.social.solution.feature_post_api.models.PostCommentUI
import com.social.solution.feature_post_api.models.PostUI
import io.reactivex.Single

class PostViewModel : ViewModel() {

    private var messagePost: Message? = null
    private val messagesComment = MutableLiveData<MutableSet<Message>>()

    @SuppressLint("CheckResult")
    fun loadComments(
        backendApi: FeatureBackendApi?,
        count: Int
    ): Single<FeedRepliesResponse.Data?>? {
        return Single.create { single ->
            if (messagePost != null) {
                val feedMessagesRequest = FeedRepliesRequest(
                    messagePost!!.chatId,
                    messagePost!!.id,
                    null,
                    count,
                    null,
                    true,
                    2,
                    0,
                    true
                )
                val anyInnerRequest = AnyInnerRequest(feedMessagesRequest)
                val anyRequest = AnyRequest(anyInnerRequest)
                backendApi?.feedApi()?.getFeedReplies(anyRequest) { resp ->
                    if (resp.result) {
                        if (resp.data != null) {
                            val resultSet = resp.data!!.messages?.messages?.toMutableSet()
                            val newSet = mutableSetOf<Message>()
                            if (resultSet != null) {
                                for (message in resultSet) {
                                    if (message.replies?.messages != null) {
                                        newSet.addAll(message.replies!!.messages!!)
                                    }
                                }
                                if (messagesComment.value == null) {
                                    messagesComment.value = resultSet.plus(newSet).toMutableSet()

                                } else {
                                    messagesComment.value!!.addAll(resultSet.plus(newSet).toMutableSet())
                                }
                            }
                            single.onSuccess(resp.data!!)
                        } else {
                            single.onError(Exception("DATA IS NULL"))
                        }
                    } else {
                        single.onError(Exception(resp.error?.msg))
                    }
                }
            }
        }
    }


    fun loadSubComments(
        backendApi: FeatureBackendApi?,
        parentComment: PostCommentUI,
        count: Int
    ): Single<FeedRepliesResponse.Data?>? {
        return Single.create { single ->
            if (messagePost != null) {
                val parentMessage = getMessageComment(parentComment.idComment)
                if (parentMessage != null) {
                    val feedMessagesRequest = FeedRepliesRequest(
                        parentMessage.chatId,
                        parentMessage.id,
                        null,
                        count,
                        null,
                        false,
                        2,
                        0,
                        true
                    )
                    val anyInnerRequest = AnyInnerRequest(feedMessagesRequest)
                    val anyRequest = AnyRequest(anyInnerRequest)
                    backendApi?.feedApi()?.getFeedReplies(anyRequest) { resp ->
                        if (resp.result) {
                            if (resp.data != null) {
                                if (messagesComment.value == null) {
                                    messagesComment.value =
                                        resp.data!!.messages?.messages?.toMutableSet()
                                } else {
                                    messagesComment.value!!.addAll(resp.data!!.messages?.messages!!)
                                }
                                single.onSuccess(resp.data!!)
                            } else {
                                single.onError(Exception("DATA IS NULL"))
                            }
                        } else {
                            single.onError(Exception(resp.error?.msg))
                        }
                    }
                }
            }
        }
    }

    @SuppressLint("CheckResult")
    fun likePost(backendApi: FeatureBackendApi?, post: PostUI): Single<MessageLikeResponse.Data?>? {
        return Single.create { single ->
            if (post.id != null) {
                messagePost
                if (messagePost != null) {
                    val feedMessagesRequest = MessageLikeRequest(messagePost!!.chatId, messagePost!!.id!!)
                    val anyInnerRequest = AnyInnerRequest(feedMessagesRequest)
                    val anyRequest = AnyRequest(anyInnerRequest)
                    backendApi?.messageApi()?.like(anyRequest) { resp ->
                        if (resp.result) {
                            if (resp.data != null) {
                                single.onSuccess(resp.data!!)
                            } else {
                                single.onError(Exception("DATA IS NULL"))
                            }
                        } else {
                            single.onError(Exception(resp.error?.msg))
                        }
                    }
                }
            }
        }
    }

    @SuppressLint("CheckResult")
    fun likeComment(
        backendApi: FeatureBackendApi?,
        comment: PostCommentUI
    ): Single<MessageLikeResponse.Data?>? {
        return Single.create { single ->
            val message = getMessageComment(comment.idComment)
            if (message != null) {
                val feedMessagesRequest = MessageLikeRequest(message.chatId, message.id!!)
                val anyInnerRequest = AnyInnerRequest(feedMessagesRequest)
                val anyRequest = AnyRequest(anyInnerRequest)
                backendApi?.messageApi()?.like(anyRequest) { resp ->
                    if (resp.result) {
                        if (resp.data != null) {
                            single.onSuccess(resp.data!!)
                        } else {
                            single.onError(Exception("DATA IS NULL"))
                        }
                    } else {
                        single.onError(Exception(resp.error?.msg))
                    }
                }
            }
        }
    }

    fun sendComment(
        backendApi: FeatureBackendApi?,
        replyComment: PostCommentUI?,
        text: String
    ): Single<Message>? {
        return Single.create { single ->
            if (messagePost != null) {
                var messageReply = getMessageComment(replyComment?.idComment)
                if (messageReply == null) messageReply = messagePost
                val feedMessagesRequest = MessageSendRequest(
                    messagePost!!.chatId,
                    messageReply!!.id,
                    null,
                    text,
                    false,
                    null,
                    messageReply.id,
                    false,
                    mutableListOf(),
                    null,
                    0,
                    null,
                    null,
                    mutableListOf("like")
                )
                val anyInnerRequest = AnyInnerRequest(feedMessagesRequest)
                val anyRequest = AnyRequest(anyInnerRequest)
                backendApi?.messageApi()?.send(anyRequest) { resp ->
                    if (resp.result) {
                        if (resp.data != null) {
                            if (messagesComment.value == null) {
                                messagesComment.value = mutableSetOf(resp.data!!)
                            } else {
                                messagesComment.value!!.add(resp.data!!)
                            }
                            single.onSuccess(resp.data!!)
                        } else {
                            single.onError(Exception("DATA IS NULL"))
                        }
                    } else {
                        single.onError(Exception(resp.error?.msg))
                    }
                }
            }
        }
    }

    private fun getMessageComment(id: Long?): Message? {
        if (id == null) return null
        if (messagesComment.value == null) return null
        return messagesComment.value!!.find { it.id == id }
    }

    fun setPost(post: Message) {
        messagePost = post
    }
}

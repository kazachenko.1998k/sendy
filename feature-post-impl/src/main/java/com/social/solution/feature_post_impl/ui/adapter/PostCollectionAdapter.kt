package com.social.solution.feature_post_impl.ui.adapter

import android.view.LayoutInflater.from
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.google.android.flexbox.FlexboxLayoutManager
import com.social.solution.feature_post_api.models.PostUI
import com.social.solution.feature_post_impl.R
import kotlinx.android.synthetic.main.post_card_collection_post.view.*


class PostCollectionAdapter(
    private var postCollection: PostUI
) :
    RecyclerView.Adapter<PostCollectionAdapter.PostCollectionViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PostCollectionViewHolder {
        val view = from(parent.context).inflate(R.layout.post_card_collection_post, parent, false)
        return PostCollectionViewHolder(view)
    }

    override fun getItemCount() = postCollection.post!!.size

    override fun onBindViewHolder(holder: PostCollectionViewHolder, position: Int) {
        holder.onBind(position, postCollection)
    }

    class PostCollectionViewHolder(view: View) : RecyclerView.ViewHolder(view) {

        fun onBind(
            position: Int,
            postCollection: PostUI
        ) {
            val imageLink = postCollection.post!![position].sourceUrl
            val postImage = itemView.post_collection_image
            Glide
                .with(postImage.context)
                .asDrawable()
                .placeholder(R.drawable.post_ic_placeholder)
                .load(imageLink)
                .into(postImage)
            postImage.setImageResource(R.drawable.post_ic_placeholder)
            val lp = postImage.layoutParams
            if (lp is FlexboxLayoutManager.LayoutParams) {
                lp.flexGrow = 2f
            }
        }
    }
}
package com.social.solution.feature_post_impl.ui


import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.graphics.drawable.BitmapDrawable
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.PopupWindow
import android.widget.Toast
import androidx.activity.OnBackPressedCallback
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.morozov.core_backend_api.SignData
import com.morozov.core_backend_api.SignData.uid
import com.morozov.core_backend_api.feed.responses.FeedRepliesResponse
import com.morozov.core_backend_api.message.Message
import com.morozov.core_backend_api.message.responses.MessageLikeResponse
import com.morozov.core_backend_api.push.PushResponse
import com.morozov.core_backend_api.user.UserMini
import com.social.solution.feature_post_api.models.PostCommentUI
import com.social.solution.feature_post_api.models.PostUI
import com.social.solution.feature_post_impl.MainObject
import com.social.solution.feature_post_impl.MainObject.mNetworkState
import com.social.solution.feature_post_impl.R
import com.social.solution.feature_post_impl.dividers.CommentDivider
import com.social.solution.feature_post_impl.ui.adapter.ParentCommentAdapter
import com.social.solution.feature_post_impl.utils.convertToCommentUI
import com.social.solution.feature_post_impl.utils.convertToPostUI
import com.social.solution.feature_post_impl.utils.getStringTimeAgoByLong
import com.social.solutions.android.transition_media_pager.manager.MediaPagerManager
import com.social.solutions.android.util_load_send_services.util.addSignUrl
import com.social.solutions.android.util_media_pager.models.FileModel
import com.social.solutions.android.util_media_pager.models.MediaModel
import com.social.solutions.android.util_media_pager.models.MediaType
import io.reactivex.Single
import io.reactivex.disposables.Disposable
import kotlinx.android.synthetic.main.post_fragment.*
import kotlinx.android.synthetic.main.post_me_menu.view.*
import kotlinx.android.synthetic.main.post_other_menu.view.first_tab
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import java.util.*

class PostFragment(private val bottomBarNavContainer: Int, private val post: Message) : Fragment() {


    private var pushObserver: Disposable? = null
    private var dataComments: MutableList<PostCommentUI> = mutableListOf()
    private lateinit var adapter: ParentCommentAdapter
    private lateinit var fragmentBottomWriteBar: BottomBarWriteFragment
    private lateinit var mPostViewModel: PostViewModel

    private val postUI = convertToPostUI(post)


    companion object {
        const val NAME = "FEED_FRAGMENT"
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        val callback: OnBackPressedCallback = object : OnBackPressedCallback(true) {
            override fun handleOnBackPressed() {
                fragmentManager?.popBackStack()
            }
        }
        requireActivity().onBackPressedDispatcher.addCallback(
            this,
            callback
        )
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        setHasOptionsMenu(true)
        mPostViewModel = ViewModelProviders.of(this).get(PostViewModel::class.java)
        setPost(post)
        return inflater.inflate(R.layout.post_fragment, container, false)
    }

    private fun setPost(post: Message) {
        mPostViewModel.setPost(post)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        GlobalScope.launch(Dispatchers.Main) {
            fragmentBottomWriteBar = BottomBarWriteFragment(postUI, WriteCommentCallback())
            parentFragmentManager.beginTransaction()
                .add(bottomBarNavContainer, fragmentBottomWriteBar)
                .commit()

            Glide
                .with(post_icon_image.context)
                .asDrawable()
                .placeholder(R.drawable.post_ic_placeholder_avatar)
                .load((SignData.server_file + postUI.ownerUI?.avatarFileId).addSignUrl(SignData.sign).toURI().toString())
                .apply(RequestOptions().circleCrop())
                .into(post_icon_image)
            if (postUI.ownerUI?.firstName != null)
                post_name_user?.text = postUI.ownerUI!!.firstName!!
            else {
                if (postUI.ownerUI?.nick != null)
                    post_name_user?.text = ("@" + postUI.ownerUI!!.nick)
            }
            post_date_post?.text = getStringTimeAgoByLong(postUI.date)
            initAdapter()
            loadComment()
            initListeners()
        }
        if (pushObserver == null) {
            pushObserver = MainObject.mBackendApi?.pushListener?.filter {
                it.data?.type in listOf(PushResponse.TYPE_MESSAGE_ADD)
            }?.subscribe({ event ->
                Log.d("UPDATE_M_and_C", event.toString())
//                updateNewDataFromServer()

            }, {
                it.printStackTrace()
            })
        }
    }

    private fun showPopupMe(v: View) {
        val popupView: View = layoutInflater.inflate(R.layout.post_me_menu, null)

        val popupWindow = PopupWindow(
            popupView,
            ViewGroup.LayoutParams.WRAP_CONTENT,
            ViewGroup.LayoutParams.WRAP_CONTENT
        )
//        popupView.first_tab?.apply {
//            setOnClickListener {
//                Toast.makeText(context!!, "Редактирвоание не доступно", Toast.LENGTH_SHORT).show()
//                popupWindow.dismiss()
//            }
//        }
//        popupView.second_tab?.apply {
//            setOnClickListener {
//                Toast.makeText(context!!, "Закрепление записи не доступно", Toast.LENGTH_SHORT).show()
//                popupWindow.dismiss()
//            }
//        }
        popupView.third_tab?.apply {
            setOnClickListener {
                Toast.makeText(context!!, "Поделиться не доступно", Toast.LENGTH_SHORT).show()
                popupWindow.dismiss()
            }
        }
        popupView.fourth_tab?.setOnClickListener {
            Toast.makeText(context!!, "Удалить не доступно", Toast.LENGTH_SHORT).show()
            popupWindow.dismiss()
        }
        popupWindow.isOutsideTouchable = true
        popupWindow.isFocusable = true
        popupWindow.showAsDropDown(v, 0, -v.height)
    }

    private suspend fun initAdapter() {
        adapter = withContext(Dispatchers.IO) {
            ParentCommentAdapter(
                context!!,
                postUI,
                mutableListOf(),
                ErrorCallback(),
                PostCallback(),
                ExpandCallback(),
                ChildCallback(),
                ParentCallback()
            )
        }
        post_comment_recycler?.adapter = adapter
        adapter.notifyData()
        swipe_container?.isRefreshing = false
        if (post_comment_recycler?.itemDecorationCount == 0)
            post_comment_recycler?.addItemDecoration(CommentDivider(context!!))
    }

    private fun initListeners() {
        action_back?.setOnClickListener {
            fragmentManager?.popBackStack()
        }
        profile_contour?.setOnClickListener {
            if (postUI.ownerUI != null) {
                MainObject.mCallback?.openProfile(postUI.ownerUI!!)
            }
        }

//        post_menu_icon_button?.setOnClickListener { showPopup(it) }
        swipe_container?.setOnRefreshListener {
            if (!mNetworkState.value!!) {
                swipe_container?.isRefreshing = false
            } else {
                loadComment()
            }
        }
        action_menu.setOnClickListener {
            if (uid != 0L){
                if (post.uid == uid) {
                    showPopupMe(it)
                } else {
                    showPopupOther(it)
                }
            }
        }
    }

    private fun showPopupOther(v: View) {
        val popupView: View =
            layoutInflater.inflate(R.layout.post_other_menu, null)

        val popupWindow = PopupWindow(
            popupView,
            ViewGroup.LayoutParams.WRAP_CONTENT,
            ViewGroup.LayoutParams.WRAP_CONTENT
        )
        popupView.first_tab?.setOnClickListener {
            popupWindow.dismiss()
        }
//        popupView.second_tab?.setOnClickListener {
//            popupWindow.dismiss()
//        }
//        popupView.third_tab?.setOnClickListener {
//            popupWindow.dismiss()
//        }
        popupWindow.setBackgroundDrawable(BitmapDrawable())
        popupWindow.isOutsideTouchable = true
        popupWindow.showAsDropDown(v, 0, -v.height)
    }

    @SuppressLint("CheckResult")
    private fun loadComment() {
        mPostViewModel.loadComments(MainObject.mBackendApi, 100)?.subscribe({ comments ->
            dataComments =
                comments?.messages?.messages?.map { convertToCommentUI(it) }?.toMutableList()
                    ?: mutableListOf()
            val newAmount = comments?.messages?.amount
            refreshComment(dataComments.toMutableList())
            swipe_container?.isRefreshing = false
        }, { throwable ->
            swipe_container?.isRefreshing = false
            adapter.notifyErrorLoad()
            Toast.makeText(
                context,
                "При загрузке данных произошла ошибка. Проверьте Ваше подключение к сети",
                Toast.LENGTH_SHORT
            ).show()
        })
    }

    fun refreshComment(newCollection: MutableList<PostCommentUI>?) {
        adapter.data = newCollection
        adapter.notifyData()
    }


    inner class ErrorCallback : ParentCommentAdapter.ErrorLoadingCommentsInterface {
        override fun loadComment() {
            this@PostFragment.loadComment()
        }
    }

    inner class ExpandCallback : ParentCommentAdapter.ExpandCommentInterface {
        override fun getAllSubComments(parentComment: PostCommentUI): Single<FeedRepliesResponse.Data?>? {
            return mPostViewModel.loadSubComments(MainObject.mBackendApi, parentComment, 1000)
        }
    }

    inner class ChildCallback : ParentCommentAdapter.ChildCommentInterface {
        override fun setReplyComment(replyComment: PostCommentUI) {
            fragmentBottomWriteBar.setReply(replyComment)
        }

        override fun likeComment(comment: PostCommentUI): Single<MessageLikeResponse.Data?>? {
            return mPostViewModel.likeComment(MainObject.mBackendApi, comment)
        }

        override fun openProfile(owner: UserMini?) {
            if (owner == null) return
            MainObject.mCallback?.openProfile(owner)
        }
    }

    inner class ParentCallback : ParentCommentAdapter.ParentCommentInterface {
        override fun setReplyComment(replyComment: PostCommentUI) {
            fragmentBottomWriteBar.setReply(replyComment)
        }

        override fun likeComment(comment: PostCommentUI): Single<MessageLikeResponse.Data?>? {
            return mPostViewModel.likeComment(MainObject.mBackendApi, comment)
        }

        override fun openProfile(owner: UserMini?) {
            if (owner == null) return
            MainObject.mCallback?.openProfile(owner)
        }
    }

    inner class PostCallback : ParentCommentAdapter.PostInterface {

        override fun showImage(url: String, name: String, date: Calendar) {
            val tmpList = mutableListOf<MediaModel>()
            tmpList.add(
                MediaModel(
                    MediaType.PHOTO,
                    FileModel(url, null)
                )
            )
            MediaPagerManager.with(context!!).setUserName(name).setDate(date).setItems(tmpList)
                .start()
        }

        override fun showVideo(url: String, position: Long, name: String, date: Calendar) {
            val tmpList = mutableListOf<MediaModel>()
            tmpList.add(
                MediaModel(
                    MediaType.VIDEO,
                    FileModel(url, null)
                )
            )
            MediaPagerManager.with(context!!).setUserName(name).setDate(date).setItems(tmpList)
                .setStartPosition(position.toInt()).start()
        }

        override fun likePost(post: PostUI): Single<MessageLikeResponse.Data?>? {
            return mPostViewModel.likePost(MainObject.mBackendApi, post)
        }

        override fun openShareView(post: PostUI) {

        }

        override fun more(post: PostUI) {

        }

        override fun  refreshLike(id: Long, likes: Int, userLike: Boolean) {
            post.likes = likes
            post.userLike = userLike
        }
    }

    inner class WriteCommentCallback : BottomBarWriteFragment.WriteCallback {
        @SuppressLint("CheckResult")
        override fun sendMessage(text: String, replyComment: PostCommentUI?) {
            mPostViewModel.sendComment(
                MainObject.mBackendApi,
                replyComment,
                text
            )?.subscribe({ newMessage ->
                if (newMessage != null) {
                    GlobalScope.launch(Dispatchers.Main) {
                        withContext(Dispatchers.IO) {
                            val newMessageUI = convertToCommentUI(newMessage)
                            newMessageUI.newMessage = true
                            postUI.countComment = postUI.countComment!! + 1
                            post.repliesAmount = postUI.countComment
                            val intent = Intent("COMMENT")
                            intent.putExtra("id", post.id)
                            intent.putExtra("countComment", postUI.countComment!!)
                            context!!.sendBroadcast(intent)
                            if (replyComment == null) {
                                dataComments.add(newMessageUI)
                                adapter.refreshAndFocus(dataComments, newMessageUI)
                            } else {
                                val firstLevelMessage =
                                    dataComments.find { replyComment.idComment == it.idComment }
                                if (firstLevelMessage != null) {
                                    if (firstLevelMessage.replies != null) {
                                        firstLevelMessage.repliesAmount =
                                            firstLevelMessage.repliesAmount!! + 1
                                        firstLevelMessage.replies!!.add(newMessageUI)
                                        adapter.refreshAndFocus(dataComments, newMessageUI)
                                    } else {
                                        firstLevelMessage.replies = mutableListOf(newMessageUI)
                                        adapter.refreshAndFocus(dataComments, newMessageUI)
                                    }
                                } else {
                                    val secondLevelMessage =
                                        dataComments.find { firsLevel -> firsLevel.replies?.find { replyComment.idComment == it.idComment } != null }
                                    if (secondLevelMessage != null) {
                                        secondLevelMessage.replies?.add(newMessageUI)
                                        adapter.refreshAndFocus(dataComments, newMessageUI)
                                    }
                                }
                            }
                        }
                        fragmentBottomWriteBar.setText("")
                    }
                }
            }, {
                Toast.makeText(
                    context!!,
                    "При загрузке данных произошла ошибка. Проверьте Ваше подключение к сети",
                    Toast.LENGTH_SHORT
                ).show()
            })
        }
    }
}
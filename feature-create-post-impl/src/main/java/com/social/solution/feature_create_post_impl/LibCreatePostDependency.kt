package com.social.solution.feature_create_post_impl

import android.content.Context
import com.morozov.core_backend_api.FeatureBackendApi
import com.social.solution.feature_create_post_api.FeatureCreatePostApi
import com.social.solution.feature_create_post_impl.start.CreatePostStarterImpl

object LibCreatePostDependency {

    fun featureCreatePostApi(context: Context, backend: FeatureBackendApi): FeatureCreatePostApi {
        return CreatePostFeatureImpl(CreatePostStarterImpl(), context, backend)
    }
}
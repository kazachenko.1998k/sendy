package com.social.solution.feature_create_post_impl.utils

import android.annotation.SuppressLint
import android.content.ContentUris
import android.content.Context
import android.database.Cursor
import android.net.Uri
import android.os.Environment
import android.provider.DocumentsContract
import android.provider.MediaStore
import android.provider.OpenableColumns
import android.util.Log
import com.social.solution.feature_create_post_impl.MainObject.TAG
import java.io.*
import java.net.URISyntaxException


fun getName(context: Context?, uri: Uri): String? {
    context ?: return null
    var fileName: String? = null
    val cursor = context.contentResolver
        .query(uri, null, null, null, null, null)
    cursor.use { cursor ->
        if (cursor != null && cursor.moveToFirst()) { // get file name
            fileName = cursor.getString(
                cursor.getColumnIndex(OpenableColumns.DISPLAY_NAME)
            )
        }
    }
    return fileName
}

fun getSize(context: Context?, uri: Uri): Int? {
    context ?: return null
    var fileSize: String? = null
    val cursor: Cursor? = context.contentResolver
        .query(uri, null, null, null, null, null)
    cursor.use { cursor ->
        if (cursor != null && cursor.moveToFirst()) { // get file size
            val sizeIndex: Int = cursor.getColumnIndex(OpenableColumns.SIZE)
            if (!cursor.isNull(sizeIndex)) {
                fileSize = cursor.getString(sizeIndex)
            }
        }
    }
    return fileSize?.toInt()
}

@SuppressLint("NewApi")
@Throws(URISyntaxException::class)
fun getFilePath(context: Context, uriTemp: Uri): String? {
    var uri = uriTemp
    var selection: String? = null
    var selectionArgs: Array<String>? = null
    when {
        isExternalStorageDocument(uri) -> {
            val docId = DocumentsContract.getDocumentId(uri)
            val split = docId.split(":").toTypedArray()
            return Environment.getExternalStorageDirectory().toString() + "/" + split[1]
        }
        isDownloadsDocument(uri) -> {
            val id = DocumentsContract.getDocumentId(uri)
            if (id.toIntOrNull() != null) {
                val contentUriPrefixesToTry = arrayOf(
                    "content://downloads/public_downloads",
                    "content://downloads/my_downloads",
                    "content://downloads/all_downloads"
                )

                for (contentUri in contentUriPrefixesToTry) {
                    val uriContent = ContentUris.withAppendedId(
                        Uri.parse("content://downloads/public_downloads"),
                        java.lang.Long.valueOf(id)
                    )
                    val result = getDataColumn(context, uriContent, selection, selectionArgs)
                    if (result != null) {
                        return result
                    }
                }
                val fileName = getName(context, uri)
                val cacheDir= getDocumentCacheDir(context)
                val file = generateFileName(fileName, cacheDir)
                var destinationPath: String? = null
                if (file != null) {
                    destinationPath = file.absolutePath
                    saveFileFromUri(context, uri, destinationPath)
                }
                return destinationPath
            } else {
                if (id.startsWith("raw:")) {
                    return id.replaceFirst("raw:", "")
                }
            }
        }
        isMediaDocument(uri) -> {
            val docId = DocumentsContract.getDocumentId(uri)
            val split = docId.split(":").toTypedArray()
            when (split[0]) {
                "image" -> {
                    uri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI
                }
                "video" -> {
                    uri = MediaStore.Video.Media.EXTERNAL_CONTENT_URI
                }
                "audio" -> {
                    uri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI
                }
            }
            selection = "_id=?"
            selectionArgs = arrayOf(
                split[1]
            )
        }
    }
    return getDataColumn(context, uri, selection, selectionArgs)
}

fun getDataColumn(
    context: Context,
    uri: Uri,
    selection: String?,
    selectionArgs: Array<String>?
): String? {
    if ("content".equals(uri.scheme, ignoreCase = true)) {
        if (isGooglePhotosUri(uri)) {
            Log.d(TAG, "PATH1 = " + uri.lastPathSegment)
            return uri.lastPathSegment
        }
        val projection = arrayOf(
            MediaStore.Images.Media.DATA
        )
        var cursor: Cursor? = null
        try {
            cursor = context.contentResolver
                .query(uri, projection, selection, selectionArgs, null)
            val columnIndex = cursor!!.getColumnIndexOrThrow(MediaStore.Images.Media.DATA)
            if (cursor.moveToFirst()) {
                Log.d(TAG, "PATH2 = " + cursor.getString(columnIndex))
                return cursor.getString(columnIndex)
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
    } else if ("file".equals(uri.scheme, ignoreCase = true)) {
        Log.d(TAG, "PATH3 = " + uri.path)
        return uri.path
    }
    Log.d(TAG, "PATH4")
    return null
}

fun isExternalStorageDocument(uri: Uri): Boolean {
    return "com.android.externalstorage.documents" == uri.authority
}

fun isDownloadsDocument(uri: Uri): Boolean {
    return "com.android.providers.downloads.documents" == uri.authority
}

fun isMediaDocument(uri: Uri): Boolean {
    return "com.android.providers.media.documents" == uri.authority
}

fun isGooglePhotosUri(uri: Uri): Boolean {
    return "com.google.android.apps.photos.content" == uri.authority
}

fun getDocumentCacheDir( context: Context): File? {
    val dir = File(context.cacheDir, "documents")
    if (!dir.exists()) {
        dir.mkdirs()
    }
    //        logDir(context.getCacheDir());
//        logDir(dir);
    return dir
}

fun generateFileName( name: String?, directory: File?): File? {
    var name = name ?: return null
    var file = File(directory, name)
    if (file.exists()) {
        var fileName = name
        var extension = ""
        val dotIndex = name.lastIndexOf('.')
        if (dotIndex > 0) {
            fileName = name.substring(0, dotIndex)
            extension = name.substring(dotIndex)
        }
        var index = 0
        while (file.exists()) {
            index++
            name = "$fileName($index)$extension"
            file = File(directory, name)
        }
    }
    try {
        if (!file.createNewFile()) {
            return null
        }
    } catch (e: IOException) { //Log.w(TAG, e);
        return null
    }
    //logDir(directory);
    return file
}

private fun saveFileFromUri(
    context: Context,
    uri: Uri,
    destinationPath: String
) {
    var `is`: InputStream? = null
    var bos: BufferedOutputStream? = null
    try {
        `is` = context.contentResolver.openInputStream(uri)
        bos = BufferedOutputStream(FileOutputStream(destinationPath, false))
        val buf = ByteArray(1024)
        `is`!!.read(buf)
        do {
            bos.write(buf)
        } while (`is`.read(buf) !== -1)
    } catch (e: IOException) {
        e.printStackTrace()
    } finally {
        try {
            if (`is` != null) `is`.close()
            if (bos != null) bos.close()
        } catch (e: IOException) {
            e.printStackTrace()
        }
    }
}

fun getFileName(context: Context?, uri: Uri): String? {
    val mimeType = context?.contentResolver?.getType(uri)
    var filename: String? = null
    if (mimeType == null && context != null) {
        val path = uri.path
        filename = if (path == null) {
            getName(uri.toString())
        } else {
            val file = File(path)
            file.name
        }
    } else {
        val returnCursor =
            context?.contentResolver?.query(uri, null, null, null, null)
        if (returnCursor != null) {
            val nameIndex = returnCursor.getColumnIndex(OpenableColumns.DISPLAY_NAME)
            returnCursor.moveToFirst()
            filename = returnCursor.getString(nameIndex)
            returnCursor.close()
        }
    }
    return filename
}

fun getName(filename: String?): String? {
    if (filename == null) {
        return null
    }
    val index = filename.lastIndexOf('/')
    return filename.substring(index + 1)
}
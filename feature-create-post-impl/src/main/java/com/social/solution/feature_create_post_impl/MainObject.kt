package com.social.solution.feature_create_post_impl

import com.example.util_cache.chat.ChatDao
import com.example.util_cache.user.UserDao
import com.morozov.core_backend_api.FeatureBackendApi
import com.morozov.core_backend_api.chat.ChatApi
import com.morozov.core_backend_api.message.MessageApi
import com.social.solution.feature_create_post_api.FeatureCreatePostCallback

object MainObject {

    lateinit var mResultCallback: (Boolean) -> Unit
    lateinit var mBackendApi: FeatureBackendApi
    var mUserDao: UserDao? = null
    var mCallback: FeatureCreatePostCallback? = null
    var TAG = "CREATE_POST_MODULE"
}
package com.social.solution.feature_create_post_impl.models

class AttachFileItem(val url: String, var name: String?, var size: Int?, var progress: Int = 0) : RecyclerItem
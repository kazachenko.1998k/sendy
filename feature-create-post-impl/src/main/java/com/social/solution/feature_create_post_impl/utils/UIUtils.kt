package com.social.solution.feature_create_post_impl.utils

import android.app.Activity
import android.content.Context
import android.view.inputmethod.InputMethodManager

fun hideKeyboard(context: Context?) {
    val inputMethodManager =
        (context as Activity).getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager?
    inputMethodManager!!.hideSoftInputFromWindow(context.currentFocus?.windowToken, 0)
}

fun showKeyboard(context: Context?) {
    val inputMethodManager =
        (context as Activity).getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager?
    inputMethodManager!!.showSoftInput(context.currentFocus, 0)

}

fun sizeFile(size: Int?): String {
    return when (size) {
        null -> "0 КБ"
        in 0..100 -> "0 КБ"
        in 100..1000000 -> "${size / 1000}.${size % 1000} КБ"
        in 1000000..1000000000 -> "${size / 1000000}.${size / 1000 % 1000} МБ"
        else -> "999.999 МБ"
    }
}
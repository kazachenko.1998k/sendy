package com.social.solution.feature_create_post_impl.start

import androidx.fragment.app.FragmentManager
import androidx.lifecycle.MutableLiveData
import com.example.util_cache.user.UserDao
import com.morozov.core_backend_api.FeatureBackendApi
import com.social.solution.feature_create_post_api.FeatureCreatePostCallback
import com.social.solution.feature_create_post_api.FeatureCreatePostStarter
import com.social.solution.feature_create_post_impl.MainObject
import com.social.solution.feature_create_post_impl.ui.fragment.CreatePostFragment

class CreatePostStarterImpl : FeatureCreatePostStarter {

    private lateinit var fragment: CreatePostFragment

    override fun start(
        manager: FragmentManager,
        parentContainer: Int,
        addToBackStack: Boolean,
        callback: FeatureCreatePostCallback,
        userDao: UserDao,
        backendApi: FeatureBackendApi,
        mNetworkState: MutableLiveData<Boolean>,
        resultCallback: (Boolean) -> Unit
    ) {
        MainObject.mCallback = callback
        MainObject.mUserDao = userDao
        MainObject.mBackendApi = backendApi
        MainObject.mResultCallback = resultCallback
        fragment = CreatePostFragment(parentContainer, mNetworkState)
        manager.beginTransaction()
            .replace(parentContainer, fragment)
            .addToBackStack(CreatePostFragment.NAME)
            .commit()
    }

}
package com.social.solution.feature_create_post_impl.ui.fragment

import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModel
import com.morozov.core_backend_api.AnyInnerRequest
import com.morozov.core_backend_api.AnyRequest
import com.morozov.core_backend_api.SignData.uid
import com.morozov.core_backend_api.message.Message
import com.morozov.core_backend_api.message.requests.MessageSendRequest
import com.morozov.core_backend_api.user.User
import com.morozov.core_backend_api.user.requests.UserGetByIdsRequest
import com.social.solution.feature_create_post_impl.MainObject
import com.social.solution.feature_create_post_impl.MainObject.TAG
import io.reactivex.Single
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import kotlin.coroutines.resume
import kotlin.coroutines.suspendCoroutine

class CreatePostViewModel : ViewModel() {

    private val serverUrl = MutableLiveData<String>()
    private val localUrl = MutableLiveData<String>()
    private val isSend = MutableLiveData<Boolean>()

    // main function createPost RX
    fun createPost(message: String, lifecycle: LifecycleOwner): Single<Message> {
        isSend.value = true
        return Single.create {
            CoroutineScope(Dispatchers.Main).launch {
                val chatId = getChatID()
                if (chatId == null) it.onError(Exception("Feed ID not found"))
                else sendMessage(serverUrl.value, message, chatId)
                    .observe(lifecycle, Observer { result ->
                        if (result != null) {
                            it.onSuccess(result)
                        } else {
                            it.onError(Exception("Result send post is NULL"))
                        }
                    })
            }
        }
    }

    // get my user from server. Convert callback to coroutine in IO Thread
    private suspend fun getUserByID(userId: Long): User? {
        return withContext(Dispatchers.IO) {
            suspendCoroutine<User?> { cont ->
                val requestBody =
                    AnyRequest(AnyInnerRequest(UserGetByIdsRequest(mutableListOf(userId), true)))
                val userApi = MainObject.mBackendApi.userApi()
                userApi.getByIds(requestBody) {
                    if (it.data?.users?.firstOrNull() != null) {
                        val myUser = it.data!!.users.first()
                        cont.resume(myUser)
                    } else {
                        cont.resume(null)
                    }
                }
            }
        }
    }

    // get feedId from myUser. Try get user from localDB, else get user from Server
    suspend fun getChatID(): Long? {
        val userMyLocal = MainObject.mUserDao?.getUserById(uid)?.firstOrNull()
        return if (userMyLocal != null) {
            userMyLocal.feedChatId
        } else {
            val userMyServer = getUserByID(uid)
            userMyServer?.feedChatId
        }
    }

    // send Message (POST) to Server. Response change LiveData.
    private fun sendMessage(
        mediaId: String?,
        message: String,
        chatId: Long
    ): MutableLiveData<Message> {
        val response = MutableLiveData<Message>()
        val messageApi = MainObject.mBackendApi.messageApi()
        val param = MessageSendRequest()
        if (mediaId != null)
            param.file_ids = mutableListOf(mediaId)
        param.chat_id = chatId
        param.type = 0
        param.message = message
        val sendMessage = AnyInnerRequest(param)
        val data = AnyRequest(sendMessage)
        messageApi.send(data) { event ->
            if (event.result && event.data != null)
                response.value = event.data
            else {
                response.value = null
            }
        }
        return response
    }

    fun setServerUrl(url: String?) {
        serverUrl.value = url
    }

    fun isSendPost(): Boolean {
        return isSend.value != null && isSend.value == true
    }

    fun setLocalUrl(url: String?) {
        localUrl.value = url
    }

    fun getLocalUrl(): String? {
        return localUrl.value
    }

}

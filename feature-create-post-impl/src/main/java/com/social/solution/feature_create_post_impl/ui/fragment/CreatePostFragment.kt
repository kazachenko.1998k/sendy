package com.social.solution.feature_create_post_impl.ui.fragment


import android.Manifest
import android.annotation.SuppressLint
import android.app.Activity.RESULT_OK
import android.content.ActivityNotFoundException
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.media.MediaPlayer
import android.net.Uri
import android.os.Bundle
import android.util.Log
import android.view.*
import android.widget.Toast
import androidx.activity.OnBackPressedCallback
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.example.util_load_send_workers.repository.DialogWorkRepository
import com.example.util_load_send_workers.repository.SendFileManager
import com.morozov.core_backend_api.SignData.uid
import com.morozov.core_backend_api.chat.converter.MessageConverter
import com.social.solution.feature_create_post_impl.MainObject
import com.social.solution.feature_create_post_impl.MainObject.TAG
import com.social.solution.feature_create_post_impl.MainObject.mResultCallback
import com.social.solution.feature_create_post_impl.R
import com.social.solution.feature_create_post_impl.models.*
import com.social.solution.feature_create_post_impl.ui.adapters.AdapterCreatePost
import com.social.solution.feature_create_post_impl.utils.*
import com.social.solutions.android.transition_media_pager.manager.MediaPagerManager
import com.social.solutions.android.util_media_pager.models.FileModel
import com.social.solutions.android.util_media_pager.models.MediaModel
import com.social.solutions.android.util_media_pager.models.MediaType
import kotlinx.android.synthetic.main.create_post_fragment.*
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import java.io.BufferedReader
import java.io.File
import java.io.IOException
import java.io.InputStreamReader


class CreatePostFragment(
    private val parentContainer: Int,
    private val networkState: MutableLiveData<Boolean>
) : Fragment() {

    private val FILE_SELECT_CODE = 18543
    private var dataItems: MutableList<RecyclerItem>? = null
    private var createPostAdapter: AdapterCreatePost? = null
    private var currentRootViewHeight = 0
    private var maxRootViewHeight = 0
    private lateinit var mCreateViewModel: CreatePostViewModel

    companion object {
        const val NAME = "CREATE_POST_FRAGMENT"
        private const val REQUEST_PERMISSIONS = 2909
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        val callback: OnBackPressedCallback = object : OnBackPressedCallback(true) {
            override fun handleOnBackPressed() {
                mResultCallback(false)
                fragmentManager?.popBackStack()
            }
        }
        requireActivity().onBackPressedDispatcher.addCallback(
            this,
            callback
        )
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        setHasOptionsMenu(true)
        mCreateViewModel = ViewModelProviders.of(this).get(CreatePostViewModel::class.java)
        return inflater.inflate(R.layout.create_post_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        create_post_action_accept?.isEnabled = false
        view.viewTreeObserver
            .addOnGlobalLayoutListener {
                currentRootViewHeight = view.height
                if (currentRootViewHeight > maxRootViewHeight) {
                    maxRootViewHeight = currentRootViewHeight
                }
            }
        initListeners()
        initAdapter()
    }

    private fun initAdapter() {
        dataItems = mutableListOf(WriteTextItem("", true))
        createPostAdapter = AdapterCreatePost(context!!, dataItems!!,
            object : AdapterCreatePost.CallbackCreatePostAdapter {
                override fun enabledChangeListener() {
                    enabledButtonAccent()
                }

                override fun removeImage() {
                    if (dataItems != null && dataItems!!.size > 1) {
                        dataItems?.removeAt(1)
                        mCreateViewModel.setServerUrl(null)
                        mCreateViewModel.setLocalUrl(null)
                        createPostAdapter?.notifyItemRemoved(1)
                        enabledButtonAccent()
                    }
                }

                override fun removeFile() {
                    if (dataItems != null && dataItems!!.size > 1) {
                        dataItems?.removeAt(1)
                        mCreateViewModel.setServerUrl(null)
                        mCreateViewModel.setLocalUrl(null)
                        createPostAdapter?.notifyItemRemoved(1)
                        enabledButtonAccent()
                    }
                }

                override fun notifyEnabledButton() {
                    enabledButtonAccent()
                }

                override fun cancelUploadImage() {
                    val localUrl = mCreateViewModel.getLocalUrl()
                    if (localUrl != null) {
                        try {
                            SendFileManager.with(context!!)
                                .setBackendApi(MainObject.mBackendApi)
                                .setChatId(uid)
                                .setUserId(uid)
                                .setFiles(listOf(localUrl))
                                .cancelLoad()
                        } catch (e: KotlinNullPointerException) {

                        } finally {
                            this.removeImage()
                        }
                    }
                }

                override fun cancelUploadFile() {
                    val localUrl = mCreateViewModel.getLocalUrl()
                    if (localUrl != null) {
                        try {
                            SendFileManager.with(context!!)
                                .setBackendApi(MainObject.mBackendApi)
                                .setChatId(uid)
                                .setUserId(uid)
                                .setFiles(listOf(localUrl))
                                .cancelLoad()
                        } catch (e: KotlinNullPointerException) {

                        } finally {
                            this.removeFile()
                        }
                    }
                }

                override fun openVideo(url: String) {
                    val tmpList = mutableListOf<MediaModel>()
                    tmpList.add(
                        MediaModel(
                            MediaType.VIDEO,
                            FileModel(url, null)
                        )
                    )
                    MediaPagerManager.with(context!!).setUserName(null).setItems(tmpList)
                        .setSinglePickMode { }.start()
                }

                override fun notifyButton() {
                    enabledButtonAccent()
                }
            })
        create_post_recycler?.adapter = createPostAdapter
    }

    @SuppressLint("CheckResult")
    private fun initListeners() {
        create_post_action_close?.setOnClickListener {
            hideKeyboard(context!!)
            mResultCallback(false)
            fragmentManager?.popBackStack()
        }
        create_post_action_accept?.setOnClickListener {
            hideKeyboard(context!!)
            if (create_post_action_accept.isEnabled) {
                if (allPictureAlready()) {
                    if (!mCreateViewModel.isSendPost()) {
                        if (networkState.value == true) {
                            mCreateViewModel.createPost(
                                (dataItems?.first() as WriteTextItem).text,
                                viewLifecycleOwner
                            )
                                .subscribe({ message ->
                                    val toast = Toast.makeText(
                                        context,
                                        "Запись успешно опубликована",
                                        Toast.LENGTH_SHORT
                                    )
                                    toast.setGravity(
                                        Gravity.BOTTOM,
                                        0,
                                        dpToPx(80f, resources)
                                    )
                                    toast.show()
                                    mResultCallback(true)
                                    val intent = Intent("CREATE_POST")
                                    intent.putExtra("message", MessageConverter().fromMessage(message))
                                    context!!.sendBroadcast(intent)
                                    Log.d("FEED_MODULE", "NEW POSTS DOUBLE2")
                                    fragmentManager?.popBackStack()
                                }, {
                                    it.printStackTrace()
                                    mResultCallback(false)
                                    fragmentManager?.popBackStack()
                                })
                        } else {
                            Toast.makeText(
                                context,
                                "Нет подключения к интернету",
                                Toast.LENGTH_SHORT
                            ).show()
                        }
                    }
                } else {
                    Toast.makeText(context, "Дождитесь загрузки фото", Toast.LENGTH_SHORT).show()
                }
            }
        }

        choose_from_gallery?.setOnClickListener {
            hideKeyboard(context!!)
            if (dataItems != null && dataItems!!.size == 1) {
                MainObject.mCallback?.chooseFromGallery { url ->
                    if (url != null) {
                        if (url.endsWith(".mp4")) {
                            Log.d(TAG, "URL = " + url)
                            startLoadVideo(url)
                        } else
                            startLoadImage(url)
                    }
                }
            } else {
                Toast.makeText(
                    context,
                    "Вы можете добавить только одно вложение",
                    Toast.LENGTH_SHORT
                )
                    .show()
            }
        }
        choose_file?.setOnClickListener {
            hideKeyboard(context!!)
            if (getPermissionMedia()) {
                startPickerFile()
            }
        }
        create_post_recycler?.setOnTouchListener { _, motionEvent ->
            if (motionEvent.action == MotionEvent.ACTION_UP) {
                if (!create_post_action_accept.isEnabled) {
                    if (dataItems?.size == 1) {
                        createPostAdapter?.requestFocus()
                        true
                    } else
                        false
                } else
                    false
            } else
                false
        }
    }


    private fun startLoadImage(url: String) {
        Log.d(TAG, "URL_IMAGE = " + url)
        dataItems?.add(1, AttachImageItem(url))
        createPostAdapter?.notifyItemRangeInserted(1, 1)
        sendImage(context!!, url, uid)
        enabledButtonAccent()
    }

    private fun startLoadVideo(url: String) {
        Log.d(TAG, "URL_VIDEO = " + url)
        dataItems?.add(1, AttachVideoItem(url))
        createPostAdapter?.notifyItemRangeInserted(1, 1)
        sendVideo(context!!, url, uid)
        enabledButtonAccent()
    }

    private fun sendImage(context: Context, url: String, uid: Long) {
        CoroutineScope(Dispatchers.Main).launch {
            val chatId = mCreateViewModel.getChatID()
            mCreateViewModel.setLocalUrl(url)
            SendFileManager.with(context)
                .setBackendApi(MainObject.mBackendApi)
                .setChatId(chatId)
                .setUserId(uid)
                .setMessageType(0)
                .setFilesExtended(listOf(url).map {
                    SendFileManager.Transaction.File(it)
                })
                .loadFiles()
            sendReceiversImage()
        }
    }

    private fun sendVideo(context: Context, url: String, uid: Long) {
        CoroutineScope(Dispatchers.Main).launch {
            val chatId = mCreateViewModel.getChatID()
            mCreateViewModel.setLocalUrl(url)
            SendFileManager.with(context)
                .setBackendApi(MainObject.mBackendApi)
                .setChatId(chatId)
                .setUserId(uid)
                .setMessageType(0)
                .setFilesExtended(listOf(url).map {
                    SendFileManager.Transaction.File(it)
                })
                .loadFiles()
            sendReceiversVideo()
        }
    }

    private fun startLoadFile(url: String, name: String?, size: Int?) {
        Log.d(TAG, "URL = " + url)
        dataItems?.add(1, AttachFileItem(url, name, size))
        createPostAdapter?.notifyItemRangeInserted(1, 1)
        sendFiles(context!!, url, uid, name, size)
        enabledButtonAccent()
    }

    private fun sendFiles(
        context: Context,
        url: String,
        uid: Long,
        name: String?,
        size: Int?
    ) {
        CoroutineScope(Dispatchers.Main).launch {
            val chatId = mCreateViewModel.getChatID()
            mCreateViewModel.setLocalUrl(url)
            SendFileManager.with(context)
                .setBackendApi(MainObject.mBackendApi)
                .setChatId(chatId)
                .setUserId(uid)
                .setMessageType(0)
                .setFilesExtended(listOf(url).map {
                    SendFileManager.Transaction.File(it, name, null, size)
                })
                .loadFiles()
            sendReceiversFile()
        }
    }

    private fun startPickerFile() {
        if (dataItems?.size == 1) {
            val intent = Intent(Intent.ACTION_GET_CONTENT)
            intent.type = "*/*"
            intent.addCategory(Intent.CATEGORY_OPENABLE)
            try {
                startActivityForResult(
                    Intent.createChooser(intent, "Select a File to Upload"),
                    FILE_SELECT_CODE
                )
            } catch (ex: ActivityNotFoundException) { // Potentially direct the user to the Market with a Dialog
                Toast.makeText(
                    context, "Please install a File Manager.",
                    Toast.LENGTH_SHORT
                ).show()
                val url = "https://play.google.com/store/apps/details?id=com.itel.filemanager"
                val i = Intent(Intent.ACTION_VIEW)
                i.data = Uri.parse(url)
                startActivity(i)
            }
        } else {
            Toast.makeText(
                context,
                "Вы можете добавить только одно вложение",
                Toast.LENGTH_SHORT
            )
                .show()
        }
    }

    private fun allPictureAlready(): Boolean {
        val imageItem =
            dataItems?.find { it is AttachImageItem } as AttachImageItem? ?: return true
        return imageItem.progress == 100
    }

    private fun enabledButtonAccent() {
        val textItem = dataItems?.find { it is WriteTextItem }!! as WriteTextItem
        val imageItem = dataItems?.find { it is AttachImageItem } as AttachImageItem?
        val fileItem = dataItems?.find { it is AttachFileItem } as AttachFileItem?
        val videoItem = dataItems?.find { it is AttachVideoItem } as AttachVideoItem?
        if ((textItem.text.isNotEmpty() && imageItem == null && fileItem == null) || (imageItem != null && imageItem.progress == 100) || (fileItem != null && fileItem.progress == 100) || (videoItem != null && videoItem.progress == 100)) {
            create_post_action_accept?.setColorFilter(
                ContextCompat.getColor(
                    context!!,
                    R.color.colorAccent
                )
            )
            create_post_action_accept?.isEnabled = true
        } else {
            create_post_action_accept?.setColorFilter(
                ContextCompat.getColor(
                    context!!,
                    R.color.pumice
                )
            )
            create_post_action_accept?.isEnabled = false
        }
    }

    private fun sendReceiversImage() {
        DialogWorkRepository.getLoadingLiveData()
            .observe(viewLifecycleOwner, Observer { observ ->
                val imageItem = dataItems?.find { it is AttachImageItem }
                if (imageItem != null) {
                    (imageItem as AttachImageItem).progress = observ.progress
                    val indexFile = dataItems?.indexOf(imageItem)
                    if (indexFile != null) {
                        createPostAdapter?.notifyItemChanged(indexFile)
                    }
                }
                if (observ.progress == 100) {
                    mCreateViewModel.setServerUrl(observ.fileId)
                    enabledButtonAccent()
                }
            })
    }

    private fun sendReceiversVideo() {
        DialogWorkRepository.getLoadingLiveData()
            .observe(viewLifecycleOwner, Observer { observ ->
                val imageItem = dataItems?.find { it is AttachVideoItem }
                if (imageItem != null) {
                    (imageItem as AttachVideoItem).progress = observ.progress
                    val indexFile = dataItems?.indexOf(imageItem)
                    if (indexFile != null) {
                        createPostAdapter?.notifyItemChanged(indexFile)
                    }
                }
                if (observ.progress == 100) {
                    mCreateViewModel.setServerUrl(observ.fileId)
                    enabledButtonAccent()
                }
            })
    }

    private fun sendReceiversFile() {
        DialogWorkRepository.getLoadingLiveData()
            .observe(viewLifecycleOwner, Observer { observ ->
                val fileItem = dataItems?.find { it is AttachFileItem }
                if (fileItem != null) {
                    (fileItem as AttachFileItem).progress = observ.progress
                    val indexFile = dataItems?.indexOf(fileItem)
                    if (indexFile != null) {
                        createPostAdapter?.notifyItemChanged(indexFile)
                    }
                }
                if (observ.progress == 100) {
                    mCreateViewModel.setServerUrl(observ.fileId)
                    enabledButtonAccent()
                }
            })
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (requestCode == FILE_SELECT_CODE && resultCode == RESULT_OK && data != null) {
            val uri = data.data
            val path = getFilePath(context!!, uri!!)
            Log.d(TAG, "PATH = " + path)
            val name = getName(context!!, uri)
            val size = getSize(context!!, uri)
            if (path != null) {
//                if (path.endsWith(".gif")) {
//                    val outputPath = getOutputPath(path)
//                    GlobalScope.launch(Dispatchers.Main) {
//                        when (val rc =
//                            FFmpeg.execute("-i $path -movflags faststart -pix_fmt yuv420p -vf \"scale=trunc(iw/2)*2:trunc(ih/2)*2\" $outputPath")) {
//                            RETURN_CODE_SUCCESS -> {
//                                startLoadFile(outputPath, name, size)
//                            }
//                            RETURN_CODE_CANCEL -> {
//                            }
//                            else -> {
//                                Log.i(
//                                    TAG,
//                                    String.format(
//                                        "Command execution failed with rc=%d and the output below.",
//                                        rc
//                                    )
//                                );
//                            }
//                        }
//                    }
//                } else {
                    startLoadFile(path, name, size)
//                }
            }
        }
        super.onActivityResult(requestCode, resultCode, data)
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        if (requestCode == REQUEST_PERMISSIONS && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            startPickerFile()
        }
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
    }

    private fun getPermissionMedia(): Boolean {
        return if (checkPermissionReadExSt()) {
            true
        } else {
            requestPermissions(
                listOf(
                    Manifest.permission.READ_EXTERNAL_STORAGE,
                    Manifest.permission.WRITE_EXTERNAL_STORAGE
                ).toTypedArray(),
                REQUEST_PERMISSIONS
            )
            false
        }
    }

    private fun checkPermissionReadExSt(): Boolean {
        return (ContextCompat.checkSelfPermission(
            context!!,
            Manifest.permission.READ_EXTERNAL_STORAGE
        ) == PackageManager.PERMISSION_GRANTED)
    }


    private fun getLibDir(): String? {
        return context?.applicationInfo?.dataDir.toString() + "/lib"
    }

    private fun getOutputPath(inputPath: String): String {
        val lastSlash = inputPath.lastIndexOf("/")
        if (lastSlash == -1) {
            Log.e(TAG, "impossible no lastSlash: $inputPath")
            throw RuntimeException("impossible no lastSlash: $inputPath")
        }
        val outputDirectory = inputPath.substring(0, lastSlash + 1)
        val inputFileName = inputPath.substring(lastSlash + 1)
        val lastDot = inputFileName.lastIndexOf(".")
        if (lastDot == -1) {
            Log.e(TAG, "impossible no lastDot: $inputPath")
            throw RuntimeException("impossible no lastDot")
        }
        val outputFileName = inputFileName.substring(0, lastDot) + ".mp4"
        return outputDirectory + outputFileName
    }

    private fun startAndWaitProcess(cmd: String): Int {
        Log.e(TAG, "cmd: $cmd")
        var p: Process? = null
        p = try {
            Runtime.getRuntime().exec(cmd)
        } catch (e: IOException) {
            Log.e(
                TAG,
                String.format("IOException occurs: %s", e.localizedMessage)
            )
            Toast.makeText(
                context, String.format("IOException occurs: %s", e.localizedMessage),
                Toast.LENGTH_SHORT
            ).show()
            return -1
        }
        var exitcode = -1
        try {
            exitcode = p.waitFor()
        } catch (e: InterruptedException) {
            Toast.makeText(
                context, "some one send a interrupt exception",
                Toast.LENGTH_SHORT
            ).show()
        }
        try {
            val errorReader = BufferedReader(InputStreamReader(p.errorStream))
            val errString = java.lang.String.format(
                "Error code: %d, error: %s",
                exitcode,
                errorReader.readLine()
            )
            Log.e(TAG, errString)
            Toast.makeText(
                context, errString,
                Toast.LENGTH_SHORT
            ).show()
        } catch (e: IOException) {
            e.printStackTrace()
        }
        return exitcode
    }

    private fun scanOutputFile(outputPath: String) {
        Log.d(TAG, outputPath)
        val intent = Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE)
        val fileUri = Uri.fromFile(File(outputPath))
        intent.data = fileUri
        context?.sendBroadcast(intent)
    }

}
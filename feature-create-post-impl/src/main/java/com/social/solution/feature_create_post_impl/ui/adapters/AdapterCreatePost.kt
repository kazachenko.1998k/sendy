package com.social.solution.feature_create_post_impl.ui.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.widget.addTextChangedListener
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.social.solution.feature_create_post_impl.R
import com.social.solution.feature_create_post_impl.models.*
import com.social.solution.feature_create_post_impl.utils.showKeyboard
import com.social.solution.feature_create_post_impl.utils.sizeFile
import kotlinx.android.synthetic.main.create_post_files_card.view.*
import kotlinx.android.synthetic.main.create_post_image_card.view.*
import kotlinx.android.synthetic.main.create_post_video_card.view.*
import kotlinx.android.synthetic.main.create_post_write_text_card.view.*


class AdapterCreatePost(
    private val context: Context,
    private val data: List<RecyclerItem>,
    val callback: CallbackCreatePostAdapter
) :
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private var lastTimeClick = 0L

    companion object {
        const val WRITE_TEXT_TYPE = 0
        const val PICTURE_TYPE = 1
        const val VIDEO_TYPE = 2
        const val FILE_TYPE = 3

        const val DEFAULT_TIME = 500
        const val THRESHOLD_STATUS = 1000 * 60 * 5 // delta 5 minutes for status online
    }

    override fun getItemCount() = data.size


    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): RecyclerView.ViewHolder {
        return when (viewType) {
            WRITE_TEXT_TYPE -> {
                val view = LayoutInflater.from(parent.context)
                    .inflate(R.layout.create_post_write_text_card, parent, false)
                WriteTextViewHolder(view)
            }
            PICTURE_TYPE -> {
                val view = LayoutInflater.from(parent.context)
                    .inflate(R.layout.create_post_image_card, parent, false)
                AttachPictureViewHolder(view)
            }
            VIDEO_TYPE -> {
                val view = LayoutInflater.from(parent.context)
                    .inflate(R.layout.create_post_video_card, parent, false)
                AttachVideoViewHolder(view)
            }
            else -> {
                val view = LayoutInflater.from(parent.context)
                    .inflate(R.layout.create_post_files_card, parent, false)
                AttachFileViewHolder(view)
            }
        }
    }

    override fun getItemViewType(position: Int): Int {
        return when (data[position]) {
            is WriteTextItem -> WRITE_TEXT_TYPE
            is AttachImageItem -> PICTURE_TYPE
            is AttachVideoItem -> VIDEO_TYPE
            else -> FILE_TYPE
        }
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        when (holder) {
            is WriteTextViewHolder -> holder.bind(position)
            is AttachPictureViewHolder -> holder.bind(position)
            is AttachVideoViewHolder -> holder.bind(position)
            is AttachFileViewHolder -> holder.bind(position)
        }
    }

    fun requestFocus() {
        (data[0] as WriteTextItem).focus = true
        notifyItemChanged(0)
    }

    inner class WriteTextViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        fun bind(position: Int) {
            val item = data[position] as WriteTextItem
            if (item.text.isNotEmpty()) itemView.write_text_field.setText(item.text)
            if (item.focus) {
                itemView.write_text_field.requestFocus()
                showKeyboard(context)
            }
            itemView.write_text_field.addTextChangedListener {
                if (!it.isNullOrEmpty()) {
                    item.text = it.toString()
                } else {
                    item.text = ""
                }
                callback.enabledChangeListener()
            }
        }
    }

    inner class AttachPictureViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        fun bind(position: Int) {
            val attachImageModel = data[position] as AttachImageItem
            val attachImage = itemView.image_choose
            Glide.with(context).asBitmap()
                .placeholder(R.drawable.create_post_ic_placeholder)
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .load(attachImageModel.url)
                .into(attachImage)
            itemView.image_unpin.setOnClickListener {}
            if (attachImageModel.progress == 100) {
                callback.notifyEnabledButton()
                itemView.image_progress_load.progress = attachImageModel.progress.toFloat()
                itemView.image_progress_load.visibility = View.GONE
                itemView.image_cancel.visibility = View.GONE
                itemView.image_icon_reload.visibility = View.GONE
                callback.notifyButton()
                itemView.image_unpin.setOnClickListener {
                    if (System.currentTimeMillis() - lastTimeClick > DEFAULT_TIME) {
                        lastTimeClick = System.currentTimeMillis()
                        callback.removeImage()
                    }
                }
            } else {
                if (attachImageModel.progress < 0) {
                    itemView.image_icon_reload.visibility = View.VISIBLE
                    itemView.image_progress_load.visibility = View.GONE
                    itemView.image_cancel.visibility = View.GONE
                } else {
                    itemView.image_progress_load.progress = attachImageModel.progress.toFloat()
                    itemView.image_progress_load.visibility = View.VISIBLE
                    itemView.image_cancel.visibility = View.VISIBLE
                    itemView.image_icon_reload.visibility = View.GONE
                    itemView.image_cancel.setOnClickListener {
                        if (System.currentTimeMillis() - lastTimeClick > DEFAULT_TIME) {
                            lastTimeClick = System.currentTimeMillis()
                            callback.cancelUploadImage()
                        }
                    }
                    itemView.image_unpin.setOnClickListener {
                        if (System.currentTimeMillis() - lastTimeClick > DEFAULT_TIME) {
                            lastTimeClick = System.currentTimeMillis()
                            callback.cancelUploadImage()
                        }
                    }
                }

            }
        }
    }

    inner class AttachVideoViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bind(position: Int) {
            val attachVideoModel = data[position] as AttachVideoItem
            itemView.video_unpin.setOnClickListener {}
            itemView.video_play_button.setOnClickListener {}
            itemView.video_play_button.setOnClickListener {
                callback.openVideo(attachVideoModel.url)
            }
            Glide.with(context).asBitmap()
                .placeholder(R.drawable.create_post_ic_placeholder)
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .load(attachVideoModel.url)
                .into(itemView.video_choose)
            if (attachVideoModel.progress == 100) {
                callback.notifyEnabledButton()
                itemView.video_progress_load.progress = attachVideoModel.progress.toFloat()
                itemView.video_progress_load.visibility = View.GONE
                itemView.video_cancel.visibility = View.GONE
                itemView.video_icon_reload.visibility = View.GONE
                itemView.video_play_button.visibility = View.VISIBLE
                callback.notifyButton()
                itemView.video_unpin.setOnClickListener {
                    if (System.currentTimeMillis() - lastTimeClick > DEFAULT_TIME) {
                        lastTimeClick = System.currentTimeMillis()
                        callback.removeImage()
                    }
                }
            } else {
                if (attachVideoModel.progress < 0) {
                    itemView.video_play_button.visibility = View.GONE
                    itemView.video_icon_reload.visibility = View.VISIBLE
                    itemView.video_progress_load.visibility = View.GONE
                    itemView.video_cancel.visibility = View.GONE
                } else {
                    itemView.video_play_button.visibility = View.GONE
                    itemView.video_icon_reload.visibility = View.GONE
                    itemView.video_progress_load.progress = attachVideoModel.progress.toFloat()
                    itemView.video_progress_load.visibility = View.VISIBLE
                    itemView.video_cancel.visibility = View.VISIBLE
                    itemView.video_cancel.setOnClickListener {
                        if (System.currentTimeMillis() - lastTimeClick > DEFAULT_TIME) {
                            lastTimeClick = System.currentTimeMillis()
                            callback.cancelUploadImage()
                        }
                    }
                    itemView.video_unpin.setOnClickListener {
                        if (System.currentTimeMillis() - lastTimeClick > DEFAULT_TIME) {
                            lastTimeClick = System.currentTimeMillis()
                            callback.cancelUploadImage()
                        }
                    }
                }
            }
        }
    }

    inner class AttachFileViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bind(position: Int) {
            val attachFileModel = data[position] as AttachFileItem
            if (attachFileModel.name != null) {
                itemView.file_name.text = attachFileModel.name
            } else {
                itemView.file_name.text = "File"
            }
            itemView.file_weight_text.text = sizeFile(attachFileModel.size)
            if (attachFileModel.progress == 100) {
                callback.notifyEnabledButton()
                itemView.file_progress_load.progress = attachFileModel.progress.toFloat()
                itemView.file_icon_reload.visibility = View.GONE
                itemView.file_progress_load.visibility = View.GONE
                itemView.file_cancel.visibility = View.GONE
                itemView.file_icon.visibility = View.VISIBLE
                callback.notifyButton()
                itemView.file_unpin.setOnClickListener {
                    if (System.currentTimeMillis() - lastTimeClick > DEFAULT_TIME) {
                        lastTimeClick = System.currentTimeMillis()
                        callback.removeFile()
                    }
                }
            } else {
                if (attachFileModel.progress < 0) {
                    itemView.file_icon_reload.visibility = View.VISIBLE
                    itemView.file_progress_load.visibility = View.GONE
                    itemView.file_cancel.visibility = View.GONE
                    itemView.file_icon.visibility = View.GONE
                } else {
                    itemView.file_progress_load.progress = attachFileModel.progress.toFloat()
                    itemView.file_icon_reload.visibility = View.GONE
                    itemView.file_progress_load.visibility = View.VISIBLE
                    itemView.file_cancel.visibility = View.VISIBLE
                    itemView.file_icon.visibility = View.GONE
                    itemView.file_cancel.setOnClickListener {
                        if (System.currentTimeMillis() - lastTimeClick > DEFAULT_TIME) {
                            lastTimeClick = System.currentTimeMillis()
                            callback.cancelUploadFile()
                        }
                    }
                    itemView.file_unpin.setOnClickListener {
                        if (System.currentTimeMillis() - lastTimeClick > DEFAULT_TIME) {
                            lastTimeClick = System.currentTimeMillis()
                            callback.cancelUploadFile()
                        }
                    }
                }
            }
        }
    }

    interface CallbackCreatePostAdapter {
        fun enabledChangeListener()
        fun removeImage()
        fun removeFile()
        fun notifyEnabledButton()
        fun cancelUploadImage()
        fun cancelUploadFile()
        fun openVideo(url: String)
        fun notifyButton()
    }
}


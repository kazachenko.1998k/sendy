package com.social.solution.feature_create_post_impl

import android.content.Context
import com.morozov.core_backend_api.FeatureBackendApi
import com.social.solution.feature_create_post_api.FeatureCreatePostApi
import com.social.solution.feature_create_post_api.FeatureCreatePostStarter

class CreatePostFeatureImpl(
    private val starter: FeatureCreatePostStarter,
    private val context: Context,
    private val api: FeatureBackendApi
) : FeatureCreatePostApi {

    override fun createPostStarter(): FeatureCreatePostStarter = starter

}
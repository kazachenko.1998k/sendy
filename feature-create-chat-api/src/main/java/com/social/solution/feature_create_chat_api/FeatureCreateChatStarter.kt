package com.social.solution.feature_create_chat_api

import androidx.fragment.app.FragmentManager
import androidx.lifecycle.MutableLiveData
import com.example.util_cache.user.UserDao
import com.morozov.core_backend_api.user.UserApi

interface FeatureCreateChatStarter {

    fun start(
        manager: FragmentManager,
        parentContainer: Int,
        addToBackStack: Boolean,
        callback: FeatureCreateChatCallback,
        userApi: UserApi,
        userDao: UserDao,
        networkState: MutableLiveData<Boolean>
    )

    fun startAddContact(
        manager: FragmentManager,
        parentContainer: Int,
        addToBackStack: Boolean,
        callback: FeatureAddContactCallback,
        userApi: UserApi,
        userDao: UserDao,
        networkState: MutableLiveData<Boolean>)

        fun permissionContact(access: Boolean)
}

package com.social.solution.feature_create_chat_api

import com.morozov.core_backend_api.user.UserMini


interface FeatureAddContactCallback {
    fun openChatP2P(user: UserMini)
}
package com.social.solution.feature_create_chat_api

interface FeatureCreateChatApi {
    fun createChatStarter(): FeatureCreateChatStarter
}
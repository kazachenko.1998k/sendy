package com.social.solution.feature_create_chat_api

import com.morozov.core_backend_api.user.UserMini


interface FeatureCreateChatCallback {
    fun openChatP2P(user: UserMini)
    fun createChannel()
    fun createGroup()
    fun shareApp()
    fun createContact()
    fun searchChats()
    fun getPermissionContact():Boolean
    fun checkMyPhone(cleanNumber: String): Boolean
}
package com.social.solution.feature_search_impl

import android.content.Context
import com.morozov.core_backend_api.FeatureBackendApi
import com.social.solution.feature_search_api.FeatureSearchApi
import com.social.solution.feature_search_impl.start.SearchStarterImpl

object LibSearchDependency {

    fun featureSearchApi(context: Context, backend: FeatureBackendApi): FeatureSearchApi {
        return SearchFeatureImpl(SearchStarterImpl(), context, backend)
    }
}
package com.social.solution.feature_search_impl.utils

import android.content.res.Resources
import android.util.TypedValue

fun dpToPx(dip: Float, resources: Resources): Int {
    return TypedValue.applyDimension(
        TypedValue.COMPLEX_UNIT_DIP,
        dip,
        resources.displayMetrics
    ).toInt()
}
package com.social.solution.feature_search_impl

import androidx.lifecycle.MutableLiveData
import com.example.util_cache.chat.ChatDao
import com.example.util_cache.user.UserDao
import com.morozov.core_backend_api.FeatureBackendApi
import com.morozov.core_backend_api.chat.Chat
import com.morozov.core_backend_api.search.SearchApi
import com.morozov.core_backend_api.user.UserApi
import com.social.solution.feature_search_api.FeatureSearchCallback
import com.social.solution.feature_search_api.models.ConfigSearch

object MainObject {

    lateinit var mConfig: ConfigSearch
    lateinit var mNetworkState: MutableLiveData<Boolean>
    var mChatDao: ChatDao? = null
    var mCallback: FeatureSearchCallback? = null
    var mBackendApi: FeatureBackendApi? = null
    var mUserDao: UserDao? = null
    var TAG = "SEARCH_MODULE"
}
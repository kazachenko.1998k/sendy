package com.social.solution.feature_search_impl

import android.content.Context
import com.morozov.core_backend_api.FeatureBackendApi
import com.social.solution.feature_search_api.FeatureSearchApi
import com.social.solution.feature_search_api.FeatureSearchStarter

class SearchFeatureImpl(
    private val starter: FeatureSearchStarter,
    private val context: Context,
    private val api: FeatureBackendApi
) : FeatureSearchApi {
    override fun searchStarter(): FeatureSearchStarter  = starter
}
package com.social.solution.feature_search_impl.ui.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.request.RequestOptions
import com.morozov.core_backend_api.SignData
import com.social.solution.feature_search_api.models.*
import com.social.solution.feature_search_impl.R
import com.social.solution.feature_search_impl.utils.getParticipantText
import com.social.solution.feature_search_impl.utils.getStringTimeAgoByLong
import com.social.solution.feature_search_impl.utils.getSubscribersText
import kotlinx.android.synthetic.main.search_card_channel_message.view.*
import kotlinx.android.synthetic.main.search_card_global_channel.view.*
import kotlinx.android.synthetic.main.search_card_global_group.view.*
import kotlinx.android.synthetic.main.search_card_global_user.view.*
import kotlinx.android.synthetic.main.search_card_group_message.view.*
import kotlinx.android.synthetic.main.search_card_local_channel.view.*
import kotlinx.android.synthetic.main.search_card_local_group.view.*
import kotlinx.android.synthetic.main.search_card_local_user.view.*
import kotlinx.android.synthetic.main.search_card_type_label.view.*
import kotlinx.android.synthetic.main.search_card_user_message.view.*


class AdapterSearch(
    private val context: Context,
    var data: MutableList<RecyclerItem>,
    val multiSelection: Boolean,
    val callback: CallbackSearchAdapter
) :
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    companion object {
        const val SEARCH_TYPE_LABEL = 0
        const val USER_MESSAGE_TYPE = 1
        const val CHANNEL_MESSAGE_TYPE = 2
        const val GROUP_MESSAGE_TYPE = 3
        const val LOCAL_USER_TYPE = 4
        const val GLOBAL_USER_TYPE = 5
        const val LOCAL_CHANNEL_TYPE = 6
        const val GLOBAL_CHANNEL_TYPE = 7
        const val LOCAL_GROUP_TYPE = 8
        const val GLOBAL_GROUP_TYPE = 9

        const val BUTTON_ADD_PARTICIPANT_TYPE = 25

        const val THRESHOLD_STATUS = 60 * 5 // delta 5 minutes for status online
    }

    override fun getItemCount() = data.size


    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): RecyclerView.ViewHolder {
        return when (viewType) {
            USER_MESSAGE_TYPE -> {
                val view = LayoutInflater.from(parent.context)
                    .inflate(R.layout.search_card_user_message, parent, false)
                UserMessageViewHolder(view)
            }
            CHANNEL_MESSAGE_TYPE -> {
                val view = LayoutInflater.from(parent.context)
                    .inflate(R.layout.search_card_channel_message, parent, false)
                ChannelMessageViewHolder(view)
            }
            GROUP_MESSAGE_TYPE -> {
                val view = LayoutInflater.from(parent.context)
                    .inflate(R.layout.search_card_group_message, parent, false)
                GroupMessageViewHolder(view)
            }
            LOCAL_USER_TYPE -> {
                val view = LayoutInflater.from(parent.context)
                    .inflate(R.layout.search_card_local_user, parent, false)
                LocalUserViewHolder(view)
            }
            GLOBAL_USER_TYPE -> {
                val view = LayoutInflater.from(parent.context)
                    .inflate(R.layout.search_card_global_user, parent, false)
                GlobalUserViewHolder(view)
            }
            LOCAL_CHANNEL_TYPE -> {
                val view = LayoutInflater.from(parent.context)
                    .inflate(R.layout.search_card_local_channel, parent, false)
                LocalChannelViewHolder(view)
            }
            GLOBAL_CHANNEL_TYPE -> {
                val view = LayoutInflater.from(parent.context)
                    .inflate(R.layout.search_card_global_channel, parent, false)
                GlobalChannelViewHolder(view)
            }
            LOCAL_GROUP_TYPE -> {
                val view = LayoutInflater.from(parent.context)
                    .inflate(R.layout.search_card_local_group, parent, false)
                LocalGroupViewHolder(view)
            }
            GLOBAL_GROUP_TYPE -> {
                val view = LayoutInflater.from(parent.context)
                    .inflate(R.layout.search_card_global_group, parent, false)
                GlobalGroupViewHolder(view)
            }
            BUTTON_ADD_PARTICIPANT_TYPE -> {
                val view = LayoutInflater.from(parent.context)
                    .inflate(R.layout.search_card_add_participant, parent, false)
                AddParticipantViewHolder(view)
            }
            else -> {
                val view = LayoutInflater.from(parent.context)
                    .inflate(R.layout.search_card_type_label, parent, false)
                SearchTypeLabelViewHolder(view)
            }
        }
    }

    override fun getItemViewType(position: Int): Int {
        return when (data[position]) {
            is SearchUserMessageUI -> USER_MESSAGE_TYPE
            is SearchChannelMessageUI -> CHANNEL_MESSAGE_TYPE
            is SearchGroupMessageUI -> GROUP_MESSAGE_TYPE
            is SearchLocalUserUI -> LOCAL_USER_TYPE
            is SearchGlobalUserUI -> GLOBAL_USER_TYPE
            is SearchLocalChannelUI -> LOCAL_CHANNEL_TYPE
            is SearchGlobalChannelUI -> GLOBAL_CHANNEL_TYPE
            is SearchLocalGroupUI -> LOCAL_GROUP_TYPE
            is SearchGlobalGroupUI -> GLOBAL_GROUP_TYPE
            is ButtonAddParticipantUI -> BUTTON_ADD_PARTICIPANT_TYPE
            else -> SEARCH_TYPE_LABEL
        }
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        when (holder) {
            is UserMessageViewHolder -> holder.bind(position)
            is ChannelMessageViewHolder -> holder.bind(position)
            is GroupMessageViewHolder -> holder.bind(position)
            is LocalUserViewHolder -> holder.bind(position)
            is GlobalUserViewHolder -> holder.bind(position)
            is LocalChannelViewHolder -> holder.bind(position)
            is GlobalChannelViewHolder -> holder.bind(position)
            is LocalGroupViewHolder -> holder.bind(position)
            is GlobalGroupViewHolder -> holder.bind(position)
            is AddParticipantViewHolder -> holder.bind()
            is SearchTypeLabelViewHolder ->
                holder.bind(position)

        }
    }

    inner class UserMessageViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bind(position: Int) {
            val dataItem = data[position] as SearchUserMessageUI
            val userIcon = itemView.search_message_user_icon
            val nameUser = itemView.search_message_user_name
            val message = itemView.search_message_user_text
            Glide.with(context).asBitmap()
                .placeholder(R.drawable.search_ic_avatar_placeholder)
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .load(SignData.server_file + dataItem.avatar)
                .apply(RequestOptions().circleCrop())
                .into(userIcon)
            nameUser.text = dataItem.firstName
            message.text = dataItem.textMessage
            itemView.setOnClickListener {
                callback.select(dataItem)
            }
        }

    }

    inner class ChannelMessageViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bind(position: Int) {
            val dataItem = data[position] as SearchChannelMessageUI
            val channelIcon = itemView.search_message_channel_icon
            val nameChannel = itemView.search_message_channel_name
            val message = itemView.search_message_channel_text
            Glide.with(context).asBitmap()
                .placeholder(R.drawable.search_ic_avatar_placeholder)
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .load(SignData.server_file + dataItem.avatar)
                .apply(RequestOptions().circleCrop())
                .into(channelIcon)
            nameChannel.text = dataItem.nameChannel
            message.text = dataItem.textMessage
            itemView.setOnClickListener {
                callback.select(dataItem)
            }
        }

    }

    inner class GroupMessageViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bind(position: Int) {
            val dataItem = data[position] as SearchGroupMessageUI
            val groupIcon = itemView.search_message_group_icon
            val nameGroup = itemView.search_message_group_name
            val message = itemView.search_message_group_text
            Glide.with(context).asBitmap()
                .placeholder(R.drawable.search_ic_avatar_placeholder)
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .load(SignData.server_file + dataItem.avatar)
                .apply(RequestOptions().circleCrop())
                .into(groupIcon)
            nameGroup.text = dataItem.nameGroup
            message.text = dataItem.textMessage
            itemView.setOnClickListener {
                callback.select(dataItem)
            }
        }
    }

    inner class LocalUserViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bind(position: Int) {
            val dataItem = data[position] as SearchLocalUserUI
            val userIcon = itemView.search_local_user_icon
            val nameUser = itemView.search_local_user_name
            val statusUser = itemView.search_local_user_status
            val selectedView = itemView.local_user_selected_item
            Glide.with(context).asBitmap()
                .placeholder(R.drawable.search_ic_avatar_placeholder)
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .load(SignData.server_file + dataItem.avatar)
                .apply(RequestOptions().circleCrop())
                .into(userIcon)
            if (!dataItem.firstName.isNullOrEmpty()) {
                nameUser.text = dataItem.firstName
            } else {
                nameUser.text = ("@${dataItem.login}")
            }
            if (System.currentTimeMillis() / 1000 - dataItem.dateLastVisit < THRESHOLD_STATUS) {
                statusUser.text = "В сети"
                statusUser.setTextColor(ContextCompat.getColor(context, R.color.colorAccent))
            } else {
                val date = getStringTimeAgoByLong(dataItem.dateLastVisit * 1000L)
                statusUser.text = ("Был в сети $date")
                statusUser.setTextColor(ContextCompat.getColor(context, R.color.santas_gray))
            }
            if (multiSelection) {
                selected(dataItem, selectedView)
                itemView.setOnClickListener {
                    dataItem.isSelected = !dataItem.isSelected
                    selected(dataItem, selectedView)
                    if (dataItem.isSelected)
                        callback.multiSelect(dataItem)
                    else
                        callback.multiUnselect(dataItem)
                }
            } else {
                itemView.setOnClickListener {
                    callback.select(dataItem)
                }
            }
        }

        private fun selected(
            dataItem: SearchLocalUserUI,
            selectedView: ImageView
        ) {
            if (dataItem.isSelected) {
                selectedView.visibility = View.VISIBLE
            } else {
                selectedView.visibility = View.GONE
            }
        }

    }

    inner class GlobalUserViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bind(position: Int) {
            val dataItem = data[position] as SearchGlobalUserUI
            val userIcon = itemView.search_global_user_icon
            val nameUser = itemView.search_global_user_name
            val loginUser = itemView.search_global_user_login
            val selectedView = itemView.global_user_selected_item
            Glide.with(context).asBitmap()
                .placeholder(R.drawable.search_ic_avatar_placeholder)
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .load(SignData.server_file + dataItem.avatar)
                .apply(RequestOptions().circleCrop())
                .into(userIcon)
            if (!dataItem.firstName.isNullOrEmpty()) {
                nameUser.text = dataItem.firstName
                loginUser.text = ("@${dataItem.login}")
            } else {
                nameUser.text = ("@${dataItem.login}")
                loginUser.text = ""
            }
            if (multiSelection) {
                selected(dataItem, selectedView)
                itemView.setOnClickListener {
                    dataItem.isSelected = !dataItem.isSelected
                    selected(dataItem, selectedView)
                    if (dataItem.isSelected)
                        callback.multiSelect(dataItem)
                    else
                        callback.multiUnselect(dataItem)
                }
            } else {
                itemView.setOnClickListener {
                    callback.select(dataItem)
                }
            }
        }

        private fun selected(
            dataItem: SearchGlobalUserUI,
            selectedView: ImageView
        ) {
            if (dataItem.isSelected) {
                selectedView.visibility = View.VISIBLE
            } else {
                selectedView.visibility = View.GONE
            }
        }

    }

    inner class LocalChannelViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bind(position: Int) {
            val dataItem = data[position] as SearchLocalChannelUI
            val channelIcon = itemView.search_local_channel_icon
            val nameChannel = itemView.search_local_channel_name
            val countSubscribers = itemView.search_local_channel_status
            Glide.with(context).asBitmap()
                .placeholder(R.drawable.search_ic_avatar_placeholder)
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .load(SignData.server_file + dataItem.avatar)
                .apply(RequestOptions().circleCrop())
                .into(channelIcon)
            nameChannel.text = dataItem.nameChannel
            if (dataItem.countSubscribers != null)
                countSubscribers.text = getSubscribersText(dataItem.countSubscribers!!)
            itemView.setOnClickListener {
                callback.select(dataItem)
            }
        }

    }

    inner class GlobalChannelViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bind(position: Int) {
            val dataItem = data[position] as SearchGlobalChannelUI
            val channelIcon = itemView.search_global_channel_icon
            val nameChannel = itemView.search_global_channel_name
            val countSubscribers = itemView.search_global_channel_status
            Glide.with(context).asBitmap()
                .placeholder(R.drawable.search_ic_avatar_placeholder)
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .load(SignData.server_file + dataItem.avatar)
                .apply(RequestOptions().circleCrop())
                .into(channelIcon)
            nameChannel.text = dataItem.nameChannel
            if (dataItem.countSubscribers != null)
                countSubscribers.text = getSubscribersText(dataItem.countSubscribers!!)
            itemView.setOnClickListener {
                callback.select(dataItem)
            }
        }

    }

    inner class LocalGroupViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bind(position: Int) {
            val dataItem = data[position] as SearchLocalGroupUI
            val groupIcon = itemView.search_local_group_icon
            val nameGroup = itemView.search_local_group_name
            val countParticipant = itemView.search_local_group_status
            Glide.with(context).asBitmap()
                .placeholder(R.drawable.search_ic_avatar_placeholder)
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .load(SignData.server_file + dataItem.avatar)
                .apply(RequestOptions().circleCrop())
                .into(groupIcon)
            nameGroup.text = dataItem.nameGroup
            if (dataItem.countParticipant != null)
                countParticipant.text = getParticipantText(dataItem.countParticipant!!)
            itemView.setOnClickListener {
                callback.select(dataItem)
            }
        }

    }

    inner class GlobalGroupViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bind(position: Int) {
            val dataItem = data[position] as SearchGlobalGroupUI
            val groupIcon = itemView.search_global_group_icon
            val nameGroup = itemView.search_global_group_name
            val countParticipant = itemView.search_global_group_text
            Glide.with(context).asBitmap()
                .placeholder(R.drawable.search_ic_avatar_placeholder)
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .load(SignData.server_file + dataItem.avatar)
                .apply(RequestOptions().circleCrop())
                .into(groupIcon)
            nameGroup.text = dataItem.nameGroup
            if (dataItem.countParticipant != null)
                countParticipant.text = getParticipantText(dataItem.countParticipant!!)
            itemView.setOnClickListener {
                callback.select(dataItem)
            }
        }

    }

    inner class SearchTypeLabelViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bind(position: Int) {
            val dataItem = data[position] as SearchTypeLabel
            val searchLabelView = itemView.search_label
            if (position == 0) {
                val newLayoutParams =
                    itemView.layoutParams as RecyclerView.LayoutParams
                newLayoutParams.topMargin = 0
                itemView.layoutParams = newLayoutParams
            }
            when (dataItem.searchTarget) {
                SearchTarget.LOCAL_USER -> searchLabelView.text = "Люди"
                SearchTarget.LOCAL_CHANNEL -> searchLabelView.text = "Каналы"
                SearchTarget.LOCAL_GROUP -> searchLabelView.text = "Группы"
                SearchTarget.GLOBAL_USER -> searchLabelView.text = "Глобальный поиск"
                SearchTarget.GLOBAL_CHANNEL -> searchLabelView.text = "Глобальный поиск"
                SearchTarget.GLOBAL_GROUP -> searchLabelView.text = "Глобальный поиск"
                SearchTarget.MESSAGE -> searchLabelView.text = "Сообщения"
                else -> {
                }
            }
        }
    }

    inner class AddParticipantViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bind() {
            itemView.setOnClickListener {

                callback.addParticipant()
            }
        }
    }

    interface CallbackSearchAdapter {
        fun addParticipant()
        fun select(item: RecyclerItem)
        fun multiSelect(item: SelectingItem)
        fun multiUnselect(dataItem: SelectingItem)
    }
}

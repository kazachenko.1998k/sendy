package com.social.solution.feature_search_impl.start

import androidx.fragment.app.FragmentManager
import androidx.lifecycle.MutableLiveData
import com.example.util_cache.chat.ChatDao
import com.example.util_cache.user.UserDao
import com.morozov.core_backend_api.FeatureBackendApi
import com.morozov.core_backend_api.chat.Chat
import com.social.solution.feature_search_api.FeatureSearchCallback
import com.social.solution.feature_search_api.FeatureSearchStarter
import com.social.solution.feature_search_api.models.ConfigSearch
import com.social.solution.feature_search_api.models.SearchTarget
import com.social.solution.feature_search_impl.MainObject
import com.social.solution.feature_search_impl.ui.fragments.SearchFragment


class SearchStarterImpl : FeatureSearchStarter {
    override fun start(
        manager: FragmentManager,
        parentContainer: Int,
        addToBackStack: Boolean,
        config: ConfigSearch,
        callback: FeatureSearchCallback,
        userDao: UserDao,
        chatDao: ChatDao,
        backendApi: FeatureBackendApi,
        networkState: MutableLiveData<Boolean>
    ) {
        MainObject.mCallback = callback
        MainObject.mBackendApi = backendApi
        MainObject.mUserDao = userDao
        MainObject.mChatDao = chatDao
        MainObject.mNetworkState = networkState
        MainObject.mConfig = config
        val fragment = SearchFragment()

        manager.beginTransaction()
            .replace(parentContainer, fragment)
            .addToBackStack(SearchFragment.NAME)
            .commit()
    }
}
package com.social.solution.feature_search_impl.ui.fragments

import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.util_cache.chat.ChatDao
import com.example.util_cache.user.UserDao
import com.example.util_cache.user.saveOrUpdateContactMini
import com.morozov.core_backend_api.AnyInnerRequest
import com.morozov.core_backend_api.AnyRequest
import com.morozov.core_backend_api.chat.Chat
import com.morozov.core_backend_api.chat.Chat.Companion.CHANNEL
import com.morozov.core_backend_api.chat.Chat.Companion.GROUP_CHAT
import com.morozov.core_backend_api.chat.ChatApi
import com.morozov.core_backend_api.chat.ChatMini
import com.morozov.core_backend_api.chat.requests.ChatGetMembersRequest
import com.morozov.core_backend_api.search.SearchApi
import com.morozov.core_backend_api.search.requests.SearchGlobalRequest
import com.morozov.core_backend_api.search.responses.SearchGlobalResponse
import com.morozov.core_backend_api.user.ContactMiniDB
import com.morozov.core_backend_api.user.UserApi
import com.morozov.core_backend_api.user.UserMini
import com.social.solution.feature_search_api.models.RecyclerItem
import com.social.solution.feature_search_api.models.SearchLocalUserUI
import com.social.solution.feature_search_api.models.SelectingItem
import com.social.solution.feature_search_impl.MainObject
import com.social.solution.feature_search_impl.MainObject.TAG
import com.social.solution.feature_search_impl.utils.convertContactDBToLocalUser
import com.social.solution.feature_search_impl.utils.convertContactServerToSearchUserUI
import com.social.solution.feature_search_impl.utils.convertSearchUserUIToContactDB
import com.social.solution.feature_search_impl.utils.convertToLocalUser
import io.reactivex.Single
import kotlinx.coroutines.*
import java.util.*
import kotlin.coroutines.resume
import kotlin.coroutines.suspendCoroutine

class SearchViewModel : ViewModel() {
    private val selectedUsers = MutableLiveData<MutableList<SelectingItem>>()
    private val users = MutableLiveData<MutableList<RecyclerItem>>()
    private var searchContacts = false

    private fun searchGlobal(
        searchApi: SearchApi,
        searchGlobalRequest: SearchGlobalRequest
    ): Single<SearchGlobalResponse?> {
        val anyInnerRequest = AnyInnerRequest(searchGlobalRequest)
        val anyRequest = AnyRequest(anyInnerRequest)
        Log.d(TAG, "DATA SEND = $anyRequest")
        return Single.create { single ->
            if (MainObject.mNetworkState.value!!)
                searchApi.global(anyRequest) {
                    if (it.result) {
                        Log.d(TAG, "DATA RECEIVE = ${it.data}")
                        single.onSuccess(it)
                    } else {
                        single.onError(Throwable(it.error!!.msg))
                    }
                }
            else
                single.onError(Throwable("Not connection"))
        }
    }

    fun searchLocalUsers(
        userDao: UserDao,
        userApi: UserApi,
        exp: String
    ): Single<List<SearchLocalUserUI>> {
        val chat = MainObject.mConfig.chat
        return Single.create { single ->
            CoroutineScope(Dispatchers.Main).launch {
                var filteredList = mutableListOf<UserMini>()
                Log.d(TAG, "FilteredList = " + chat)
                if (chat != null) {
                    if (MainObject.mConfig.inGroupSearch){
                        single.onSuccess(getMembersFromChat(MainObject.mBackendApi!!.chatApi(), chat).mapNotNull { convertToLocalUser(it) })
                        return@launch
                    }else {
                        filteredList = getMembersFromChat(MainObject.mBackendApi!!.chatApi(), chat)
                    }
                }
                Log.d(TAG, "FilteredList = " + filteredList)
                CoroutineScope(Dispatchers.IO).launch {
                    val expToLowerCase = exp.toLowerCase(Locale.getDefault())
                    val localResult =
                        userDao.getAllContactsMini().filter {
                            it.firstName != null &&
                                    it.firstName!!.toLowerCase(Locale.getDefault()).contains(
                                        expToLowerCase
                                    ) && filteredList.find { find -> find.uid == it.uid } == null
                        }
                    Log.d(TAG, "FilteredList = " + localResult)
                    val localResultUI = localResult.mapNotNull {
                        convertContactDBToLocalUser(it)
                    }
                    single.onSuccess(localResultUI)
                    if (!searchContacts) {
                        userApi.getContacts {
                            if (it.result) {
                                if (it.data != null) {
                                    searchContacts = true
                                    val serverResults = it.data!!.contacts.filter { filterServ ->
                                        filterServ.userMini?.firstName != null &&
                                                filterServ.userMini!!.firstName!!.toLowerCase(Locale.getDefault())
                                                    .contains(expToLowerCase) && filteredList.find { find -> find.uid == filterServ.userMini!!.uid } == null
                                    }
                                    val updateListUI = serverResults
                                        .filter { contactServer ->
                                            localResult.find { contactDB -> contactDB.uid == contactServer.userMini?.uid } == null
                                        }
                                        .mapNotNull { element ->
                                            convertContactServerToSearchUserUI(element)
                                        }
//                                Log.d(TAG, "Contacts from server (filtered by new) = $updateListUI")
                                    val resultMerge = localResultUI.toMutableList()
                                    resultMerge.addAll(updateListUI)
                                    single.onSuccess(resultMerge)
                                    insertContactServerUser(userDao, updateListUI.map { contactUI ->
                                        convertSearchUserUIToContactDB(contactUI)
                                    })
                                } else
                                    single.onError(Exception("DATA IS NULL"))
                            } else {
                                single.onError(Exception(it.error?.msg))
                            }
                        }
                    }
                }
            }
        }
    }

    private suspend fun getMembersFromChat(chatApi: ChatApi, chat: Chat): MutableList<UserMini> {
        return withContext(Dispatchers.IO) {
            suspendCoroutine<MutableList<UserMini>> { cont ->
                val requestBody =
                    AnyRequest(
                        AnyInnerRequest(
                            ChatGetMembersRequest(
                                chat.id,
                                null,
                                null,
                                null,
                                true
                            )
                        )
                    )
                MainObject.mBackendApi?.chatApi()?.getMembers(requestBody) {
                    Log.d(TAG, "Response = " + it.data)
                    Log.d(TAG, "Response1 = " + it.data?.users)
                    if (it.data?.users?.firstOrNull() != null) {
                        cont.resume(it.data!!.users!!)
                    } else {
                        cont.resume(mutableListOf())
                    }
                }
            }
        }
    }


    private fun insertContactServerUser(userDao: UserDao?, contactMiniDB: List<ContactMiniDB>) {
        GlobalScope.launch(Dispatchers.IO) {
            contactMiniDB.forEach { contactDB ->
                userDao?.saveOrUpdateContactMini(contactDB)?.subscribe({
                    //                    Log.d(TAG, "New contact received from server INSERTED in DB = $contactDB")
                }, { t -> t.printStackTrace() })
            }
        }
    }

    fun searchGlobalUsers(
        searchApi: SearchApi,
        exp: String
    ): Single<SearchGlobalResponse?> {
        val chat = MainObject.mConfig.chat
        val searchGlobalRequest = SearchGlobalRequest(
            exp,
            in_nick = true,
            in_title = false,
            in_description = false,
            users = true,
            groups = false,
            channels = false,
            amount = 10,
            page = 0,
            chat_id = chat?.id,
            in_group = MainObject.mConfig.inGroupSearch
        )
        return searchGlobal(searchApi, searchGlobalRequest)
    }

    //todo: change to request for room
    fun searchLocalChannels(
        chatDao: ChatDao,
        searchApi: SearchApi,
        exp: String
    ): Single<List<ChatMini>?> {
        val searchGlobalRequest = SearchGlobalRequest(
            exp,
            in_nick = false,
            in_title = true,
            in_description = false,
            users = false,
            groups = false,
            channels = true,
            amount = 10,
            page = 0
        )
        return Single.create { single ->
            CoroutineScope(Dispatchers.IO).launch {
                val localChat = chatDao.getAllChatsMini().filter {
                    it.userRole != null && it.type == CHANNEL && it.title != null && it.title!!.contains(
                        exp
                    )
                }
//                Log.d(TAG, "localChats = $localChat")
                single.onSuccess(localChat)
            }
        }
    }


    fun searchGlobalChannels(
        searchApi: SearchApi,
        exp: String
    ): Single<SearchGlobalResponse?> {
        val searchGlobalRequest = SearchGlobalRequest(
            exp,
            in_nick = false,
            in_title = true,
            in_description = false,
            users = false,
            groups = false,
            channels = true,
            amount = 10,
            page = 0
        )
        return searchGlobal(searchApi, searchGlobalRequest)
    }

    //todo: change to request for room
    fun searchLocalGroups(
        chatDao: ChatDao,
        searchApi: SearchApi,
        exp: String
    ): Single<List<ChatMini>?> {
        val searchGlobalRequest = SearchGlobalRequest(
            exp,
            in_nick = false,
            in_title = true,
            in_description = false,
            users = false,
            groups = true,
            channels = false,
            amount = 10,
            page = 0
        )
        return Single.create { single ->
            CoroutineScope(Dispatchers.IO).launch {
                val localChat = chatDao.getAllChatsMini()
                    .filter {
                        it.userRole != null && it.type == GROUP_CHAT && it.title != null && it.title!!.contains(
                            exp
                        )
                    }
//                Log.d(TAG, "localGroup = $localChat")
                single.onSuccess(localChat)
            }
        }
    }

    fun searchGlobalGroup(searchApi: SearchApi, exp: String): Single<SearchGlobalResponse?> {
        val searchGlobalRequest = SearchGlobalRequest(
            exp,
            in_nick = false,
            in_title = true,
            in_description = false,
            users = false,
            groups = true,
            channels = false,
            amount = 10,
            page = 0
        )
        return searchGlobal(searchApi, searchGlobalRequest)
    }

    fun getSelectedUsers(): MutableList<SelectingItem>? {
        return this.selectedUsers.value
    }

    fun setSelectedUsers(selectedUser: MutableList<SelectingItem>) {
        this.selectedUsers.value = selectedUser
    }

    fun getUsers(): MutableList<RecyclerItem>? {
        return this.users.value
    }

    fun setUsers(users: MutableList<RecyclerItem>) {
        this.users.value = users
    }
}
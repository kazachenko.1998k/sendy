package com.social.solution.feature_search_impl.utils

import com.morozov.core_backend_api.chat.ChatMini
import com.morozov.core_backend_api.contact.Contact
import com.morozov.core_backend_api.user.ContactMiniDB
import com.morozov.core_backend_api.user.UserMini
import com.social.solution.feature_search_api.models.*

fun convertToGlobalUser(userMini: UserMini): SearchGlobalUserUI {
    return SearchGlobalUserUI(
        userMini.uid,
        userMini.firstName,
        userMini.secondName,
        userMini.nick,
        userMini.lastActive,
        userMini.avatarFileId,
        isSubscribe = true,
        isSelected = false
    )
}

fun convertToLocalChannel(chatMini: ChatMini): SearchLocalChannelUI {
    return SearchLocalChannelUI(
        chatMini.chatId,
        chatMini.title,
        "",
        chatMini.iconFileId,
        true,
        chatMini.membersAmount
    )
}

fun convertToGlobalChannel(chatMini: ChatMini): SearchGlobalChannelUI {
    return SearchGlobalChannelUI(
        chatMini.chatId,
        chatMini.title,
        "",
        chatMini.iconFileId,
        false,
        chatMini.membersAmount
    )
}

fun convertToLocalGroup(chatMini: ChatMini): SearchLocalGroupUI {
    return SearchLocalGroupUI(
        chatMini.chatId,
        chatMini.title,
        "",
        chatMini.iconFileId,
        true,
        chatMini.membersAmount
    )
}

fun convertToGlobalGroup(chatMini: ChatMini): SearchGlobalGroupUI {
    return SearchGlobalGroupUI(
        chatMini.chatId,
        chatMini.title,
        "",
        chatMini.iconFileId,
        false,
        chatMini.membersAmount
    )
}

fun convertToLocalUser(userMini: UserMini): SearchLocalUserUI? {
    return SearchLocalUserUI(
        userMini.uid,
        userMini.firstName,
        userMini.secondName,
        userMini.nick,
        userMini.lastActive,
        userMini.avatarFileId,
        isSubscribe = true,
        isSelected = false
    )
}

fun convertContactDBToLocalUser(contact: ContactMiniDB): SearchLocalUserUI? {
    return SearchLocalUserUI(
        contact.uid,
        contact.firstName,
        contact.secondName,
        contact.nick,
        contact.lastActive,
        contact.avatarFileId,
        isSubscribe = true,
        isSelected = false
    )
}

fun convertContactServerToSearchUserUI(contact: Contact): SearchLocalUserUI? {
    val userMini = contact.userMini
    return if (userMini != null)
        SearchLocalUserUI(
            userMini.uid,
            userMini.firstName,
            userMini.secondName,
            userMini.nick,
            userMini.lastActive,
            userMini.avatarFileId,
            isSubscribe = true,
            isSelected = false
        )
    else null
}

fun convertSearchUserUIToContactDB(searchLocalUserUI: SearchLocalUserUI): ContactMiniDB {
    val contactMini = ContactMiniDB()
    contactMini.uid = searchLocalUserUI.id
    contactMini.firstName = searchLocalUserUI.firstName
    contactMini.secondName = searchLocalUserUI.lastName
    contactMini.nick = searchLocalUserUI.login
    contactMini.lastActive = searchLocalUserUI.dateLastVisit
    contactMini.avatarFileId = searchLocalUserUI.avatar
    contactMini.isSubscribe = searchLocalUserUI.isSubscribe
    return contactMini
}

//fun convertFrom(modelMessageUser: ModelMessageUser): SearchUserMessageUI {
//    return SearchUserMessageUI(
//        modelMessageUser.id,
//        modelMessageUser.firstName,
//        modelMessageUser.secondName,
//        modelMessageUser.textMessage,
//        modelMessageUser.data,
//        false,
//        modelMessageUser.avatar,
//        false
//    )
//}
//
//fun convertFrom(modelMessageChannel: ModelMessageChannel): SearchChannelMessageUI {
//    return SearchChannelMessageUI(modelMessageChannel.id,
//        modelMessageChannel.nameChannel,
//        modelMessageChannel.textMessage,
//        modelMessageChannel.data,
//        false,
//        modelMessageChannel.avatar,
//        false
//    )
//}
//
//fun convertFrom(modelMessageGroup: ModelMessageGroup): SearchGroupMessageUI {
//    return SearchGroupMessageUI(modelMessageGroup.id,
//        modelMessageGroup.nameGroup,
//        modelMessageGroup.textMessage,
//        modelMessageGroup.data,
//        false,
//        modelMessageGroup.avatar,
//        false
//    )
//}
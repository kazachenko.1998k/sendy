package com.social.solution.feature_search_impl.utils

fun getSubscribersText(value: Int): String {
    if (value < 10000) {
        return when (value % 10) {
            1 -> "$value подписчик"
            in (2..4) -> "$value подписчика"
            else -> "$value подписчиков"
        }
    } else {
        return if (value in (10000..1000000)) {
            val suffix = (value/100) % 10
            val prefix = (value/1000)
            "${prefix}.${suffix}К подписчиков"
        } else {
            val suffix = (value/100000) % 10
            val prefix = (value/1000000)
            "${prefix}.${suffix}М подписчиков"
        }
    }
}

fun getParticipantText(value: Int): String {
    if (value < 10000) {
        return when (value % 10) {
            1 -> "$value участник"
            in (2..4) -> "$value участника"
            else -> "$value участников"
        }
    } else {
        return if (value in (10000..1000000)) {
            val suffix = (value/100) % 10
            val prefix = (value/1000)
            "${prefix}.${suffix}К участников"
        } else {
            val suffix = (value/100000) % 10
            val prefix = (value/1000000)
            "${prefix}.${suffix}М участников"
        }
    }
}
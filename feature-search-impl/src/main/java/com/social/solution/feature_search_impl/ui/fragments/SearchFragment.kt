package com.social.solution.feature_search_impl.ui.fragments

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.activity.OnBackPressedCallback
import androidx.core.content.ContextCompat
import androidx.core.widget.addTextChangedListener
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.morozov.core_backend_api.SignData.uid
import com.social.solution.feature_search_api.models.*
import com.social.solution.feature_search_impl.MainObject
import com.social.solution.feature_search_impl.MainObject.TAG
import com.social.solution.feature_search_impl.MainObject.mNetworkState
import com.social.solution.feature_search_impl.R
import com.social.solution.feature_search_impl.ui.adapters.AdapterSearch
import com.social.solution.feature_search_impl.utils.*
import kotlinx.android.synthetic.main.search_fragment.*
import kotlinx.android.synthetic.main.search_fragment.search_users_recycler
import kotlinx.android.synthetic.main.search_fragment_with_title.*
import kotlinx.android.synthetic.main.search_fragment_with_title.search_action_back
import kotlinx.android.synthetic.main.search_fragment_with_title.search_view
import kotlinx.coroutines.*
import java.util.*
import kotlin.coroutines.CoroutineContext


class SearchFragment : Fragment(), CoroutineScope {

    override val coroutineContext: CoroutineContext = Dispatchers.Main
    private var isSearchTaskLocalUser = false
    private var isSearchTaskGlobalUser = false
    private var isSearchTaskLocalChannel = false
    private var isSearchTaskGlobalChannel = false
    private var isSearchTaskLocalGroup = false
    private var isSearchTaskGlobalGroup = false
    private lateinit var searchAdapter: AdapterSearch
    private lateinit var mSearchViewModel: SearchViewModel
    private var data = mutableListOf<RecyclerItem>()
    private var selectedUser = mutableListOf<SelectingItem>()
    var maxRootViewHeight = 0
    var currentRootViewHeight = 0
    var placeholderHeight = 0
    private var placeholderTitleHeight = 0

    companion object {
        const val NAME = "SEARCH_FRAGMENT"
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        val callback: OnBackPressedCallback = object : OnBackPressedCallback(true) {
            override fun handleOnBackPressed() {
                fragmentManager?.popBackStack()
            }
        }
        requireActivity().onBackPressedDispatcher.addCallback(this, callback)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        setHasOptionsMenu(true)
        mSearchViewModel = ViewModelProviders.of(this).get(SearchViewModel::class.java)
        return if (MainObject.mConfig.title == null) {
            inflater.inflate(R.layout.search_fragment, container, false)
        } else {
            inflater.inflate(R.layout.search_fragment_with_title, container, false)
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        view.viewTreeObserver
            .addOnGlobalLayoutListener {
                currentRootViewHeight = view.height
                if (currentRootViewHeight > maxRootViewHeight) {
                    maxRootViewHeight = currentRootViewHeight
                }
                if (maxRootViewHeight - currentRootViewHeight > 1) {
                    placeholderHeight = (0.30 * maxRootViewHeight).toInt()
                    placeholderTitleHeight = (0.20 * maxRootViewHeight).toInt()
                    showKeyboardEvent()
                }else{
                    placeholderHeight = (0.40 * maxRootViewHeight).toInt()
                    placeholderTitleHeight = (0.40 * maxRootViewHeight).toInt()
                    hideKeyboardEvent()
                }
            }
        mNetworkState.observe(viewLifecycleOwner, Observer {
            if (it) {
                showOnline()
            } else {
                showNoInternet()
            }
        })
        GlobalScope.launch(Dispatchers.Main) {
            initListeners()
            recoverData()
            initAdapter(this@SearchFragment.data)
            search_users_recycler?.adapter = searchAdapter
            if (this@SearchFragment.data.isEmpty()) {
                search("")
                search_users_recycler?.adapter = searchAdapter
            }
            search_view.requestFocus()
        }
    }

    private fun showKeyboardEvent(){
        updateImagePlaceholderSize()
    }

    private fun hideKeyboardEvent(){
       updateImagePlaceholderSize()
    }

    private fun updateImagePlaceholderSize(){
        search_picture_request_not_found?.layoutParams?.height = placeholderHeight
        search_picture_request_not_found?.layoutParams?.width =
            ViewGroup.LayoutParams.MATCH_PARENT
        with_title_search_picture_request_not_found?.layoutParams?.height =
            placeholderTitleHeight
        with_title_search_picture_request_not_found?.layoutParams?.width =
            ViewGroup.LayoutParams.MATCH_PARENT
        search_picture_request_not_found?.requestLayout()
        with_title_search_picture_request_not_found?.requestLayout()
    }

    private fun showNoInternet() {
        search_fragment_button_continue?.setBackgroundColor(
            ContextCompat.getColor(
                context!!,
                R.color.pumice
            )
        )
        search_fragment_button_continue?.isEnabled = false
//        search_title?.text = "Ожидание сети..."
    }

    private fun showOnline() {
        search_fragment_button_continue?.setBackgroundColor(
            ContextCompat.getColor(
                context!!,
                R.color.colorAccent
            )
        )
        search_fragment_button_continue?.isEnabled = true
//        search_title?.text = MainObject.mConfig.title
    }

    private fun recoverData() {
        val data = mSearchViewModel.getUsers()
        if (data != null) {
            this.data.addAll(data)
        }
        val selectedUser = mSearchViewModel.getSelectedUsers()
        if (selectedUser != null)
            this.selectedUser = selectedUser
    }

    override fun onPause() {
        super.onPause()
        hideKeyboard(context!!)
        mSearchViewModel.setUsers(data)
        mSearchViewModel.setSelectedUsers(selectedUser)
    }

    private fun initListeners() {
        search_action_back?.setOnClickListener {
            fragmentManager?.popBackStack()
        }
        var searchFor = ""
        search_view?.addTextChangedListener {
            val searchText = it.toString().trim()
            if (searchText != searchFor) {
                searchFor = searchText
                launch {
                    delay(300)  //debounce timeOut
                    if (searchText != searchFor)
                        return@launch
                    search(searchFor)
                }
            }
            if (!it.isNullOrEmpty()) {
                image_clear_1?.visibility = View.VISIBLE
                image_clear_2?.visibility = View.VISIBLE
            } else {
                image_clear_1?.visibility = View.GONE
                image_clear_2?.visibility = View.GONE
            }
        }
        image_clear_1?.setOnClickListener {
            search_view.setText("")
        }
        image_clear_2?.setOnClickListener {
            search_view.setText("")
        }
//        if (MainObject.mConfig.title != null) {
//            if (mNetworkState.value == null || mNetworkState.value!!) {
//                search_title?.text = MainObject.mConfig.title
//            } else {
//                search_title?.text = "Ожидание сети..."
//            }
//        }
        if (MainObject.mConfig.searchView) {
            search_constraint?.visibility = View.VISIBLE
            search_layout?.visibility = View.VISIBLE
        } else {
            search_constraint?.visibility = View.GONE
            search_layout?.visibility = View.GONE
        }
        if (!MainObject.mConfig.textButton.isNullOrEmpty()) {
            search_layout_continue?.visibility = View.VISIBLE
            if (mNetworkState.value == null || mNetworkState.value!!) {
                search_fragment_button_continue?.setBackgroundColor(
                    ContextCompat.getColor(
                        context!!,
                        R.color.colorAccent
                    )
                )
                search_fragment_button_continue?.isEnabled = true
            } else {
                search_fragment_button_continue?.setBackgroundColor(
                    ContextCompat.getColor(
                        context!!,
                        R.color.pumice
                    )
                )
                search_fragment_button_continue?.isEnabled = false
            }
            search_fragment_button_continue?.setOnClickListener {
                if (search_fragment_button_continue.isEnabled) {
                    search_fragment_button_continue.isEnabled = false
                    MainObject.mCallback?.multiSelectUser(selectedUser)
                } else {
                    val toast = Toast.makeText(
                        context,
                        "Проблемы с доступом к интернету",
                        Toast.LENGTH_SHORT
                    )
                    toast.setGravity(
                        Gravity.BOTTOM,
                        0,
                        maxRootViewHeight - currentRootViewHeight + dpToPx(80f, resources)
                    )
                    toast.show()
                }
            }
            search_fragment_button_continue?.text = MainObject.mConfig.textButton
        } else
            showKeyboard(context!!)
    }

    private suspend fun initAdapter(data: MutableList<RecyclerItem>) {
        searchAdapter = withContext(Dispatchers.IO) {
            val initData = mutableListOf<RecyclerItem>()
            initData.addAll(data)
            AdapterSearch(
                context!!,
                initData,
                MainObject.mConfig.multiSelection,
                SearchAdapterCallback()
            )
        }
    }

    private fun search(exp: String) {
        GlobalScope.launch(Dispatchers.Main) {
            withContext(Dispatchers.IO) {
                data.clear()
                if (MainObject.mBackendApi?.searchApi() != null)
                    MainObject.mConfig.targets.forEach { searchTarget ->
                        when (searchTarget) {
                            SearchTarget.LOCAL_USER -> {
                                isSearchTaskLocalUser = true
                                searchLocalUsers(exp)
                            }
                            SearchTarget.LOCAL_CHANNEL -> {
                                isSearchTaskLocalChannel = true
                                searchLocalChannels(exp)
                            }
                            SearchTarget.LOCAL_GROUP -> {
                                isSearchTaskLocalGroup = true
                                searchLocalGroup(exp)
                            }
                            SearchTarget.GLOBAL_USER -> {
                                isSearchTaskGlobalUser = true
                                searchGlobalUsers(exp)
                            }
                            SearchTarget.GLOBAL_CHANNEL -> {
                                isSearchTaskGlobalChannel = true
                                searchGlobalChannels(exp)
                            }
                            SearchTarget.GLOBAL_GROUP -> {
                                isSearchTaskGlobalGroup = true
                                searchGlobalGroup(exp)
                            }
                            SearchTarget.MESSAGE -> {
                            }
                        }
                    }
            }
        }
    }

    @SuppressLint("CheckResult")
    private fun searchLocalUsers(exp: String) {
        mSearchViewModel.searchLocalUsers(
            MainObject.mUserDao!!,
            MainObject.mBackendApi?.userApi()!!,
            exp
        )
            .subscribe({ localUsers ->
                var sorted = localUsers.sortedBy { sort ->
                    if (sort.firstName != null)
                        sort.firstName!!.toLowerCase(Locale.getDefault())
                    else
                        sort.login
                }
                if (MainObject.mConfig.filteredMe){
                    sorted = sorted.filter { it.id != uid }
                }
                if (MainObject.mConfig.maxCountSearch) {
                    insertLocalUsers(sorted.toMutableList())
                } else {
                    insertLocalUsers(sorted.take(4).toMutableList())
                }
                isSearchTaskLocalUser = false
                collectData(false)
            }, { throwable: Throwable? ->
                throwable?.printStackTrace()
                isSearchTaskLocalUser = false
                collectData(false)
            })
    }

    @SuppressLint("CheckResult")
    private fun searchGlobalUsers(exp: String) {
        if (exp.length > 4)
            mSearchViewModel.searchGlobalUsers(
                MainObject.mBackendApi?.searchApi()!!,
                exp
            ).subscribe({ resp ->
                val globalUsers = resp?.data?.users?.users
                var result = globalUsers?.map { convertToGlobalUser(it) }?.toMutableList()!!
                if (MainObject.mConfig.filteredMe){
                    result = result.filter { it.id != uid }.toMutableList()
                }
                result.sortByDescending { it.dateLastVisit }
                insertGlobal(result)
                isSearchTaskGlobalUser = false
                collectData(true)
            }, { throwable: Throwable? ->
                isSearchTaskGlobalUser = false
                collectData(true)
                throwable?.printStackTrace()
            })
        else {
            isSearchTaskGlobalUser = false
        }
    }

    @SuppressLint("CheckResult")
    private fun searchLocalChannels(exp: String) {
        mSearchViewModel.searchLocalChannels(
            MainObject.mChatDao!!,
            MainObject.mBackendApi?.searchApi()!!,
            exp
        )
            .subscribe({ localChannels ->
                if (MainObject.mConfig.maxCountSearch) {
                    insertLocalChannels(localChannels?.map {
                        convertToLocalChannel(it)
                    }?.toMutableList()!!)
                } else {
                    insertLocalChannels(localChannels?.map {
                        convertToLocalChannel(it)
                    }?.take(4)!!.toMutableList())
                }
                isSearchTaskLocalChannel = false
                collectData(false)
            }, { throwable: Throwable? ->
                throwable?.printStackTrace()
                isSearchTaskLocalChannel = false
                collectData(false)
            })
    }

    @SuppressLint("CheckResult")
    private fun searchGlobalChannels(exp: String) {
        if (exp.length > 4)
            mSearchViewModel.searchGlobalChannels(
                MainObject.mBackendApi?.searchApi()!!,
                exp
            ).subscribe({ resp ->
                val globalChannels =
                    resp?.data?.channels?.channels?.filter { it.userRole == null }
                insertGlobal(globalChannels?.map {
                    convertToGlobalChannel(it)
                }?.toMutableList()!!)
                isSearchTaskGlobalChannel = false
                collectData(true)
            }, { throwable: Throwable? ->
                throwable?.printStackTrace()
                isSearchTaskGlobalChannel = false
                collectData(true)
            })
        else {
            isSearchTaskGlobalChannel = false
        }
    }

    @SuppressLint("CheckResult")
    private fun searchLocalGroup(exp: String) {
        mSearchViewModel.searchLocalGroups(
            MainObject.mChatDao!!,
            MainObject.mBackendApi?.searchApi()!!,
            exp
        )
            .subscribe({ localGroups ->
                if (MainObject.mConfig.maxCountSearch) {
                    insertLocalGroup(localGroups?.map {
                        convertToLocalGroup(it)
                    }?.toMutableList()!!)
                } else {
                    insertLocalGroup(
                        localGroups?.map { convertToLocalGroup(it) }?.take(
                            4
                        )!!.toMutableList()
                    )
                }
                isSearchTaskLocalGroup = false
                collectData(false)
            }, { throwable: Throwable? ->
                throwable?.printStackTrace()
                isSearchTaskLocalGroup = false
                collectData(false)
            })
    }

    @SuppressLint("CheckResult")
    private fun searchGlobalGroup(exp: String) {
        if (exp.length > 4)
            mSearchViewModel.searchGlobalGroup(
                MainObject.mBackendApi?.searchApi()!!,
                exp
            ).subscribe({ resp ->
                val globalGroups =
                    resp?.data?.groups?.groups?.filter { it.userRole == null }
                insertGlobal(globalGroups?.map {
                    convertToGlobalGroup(it)
                }?.toMutableList()!!)
                isSearchTaskGlobalGroup = false
                collectData(true)
            }, { throwable: Throwable? ->
                throwable?.printStackTrace()
                isSearchTaskGlobalGroup = false
                collectData(true)
            })
        else {
            isSearchTaskGlobalGroup = false
        }
    }

    private fun insertGlobal(globals: List<RecyclerItem>) {
        if (globals.isNotEmpty()) {
            val indexLabelGlobalUser =
                data.indexOfFirst { it == SearchTypeLabel(SearchTarget.GLOBAL_USER) }
            if (indexLabelGlobalUser < 0) {
                val indexLabelMessage =
                    data.indexOfFirst { it == SearchTypeLabel(SearchTarget.MESSAGE) }
                if (indexLabelMessage < 0) {
                    data.add(data.size, SearchTypeLabel(SearchTarget.GLOBAL_USER))
                    data.addAll(data.size, globals)
                } else {
                    data.add(indexLabelMessage, SearchTypeLabel(SearchTarget.GLOBAL_USER))
                    data.addAll(indexLabelMessage + 1, globals)
                }
            } else {
                val indexLabelMessage =
                    data.indexOfFirst { it == SearchTypeLabel(SearchTarget.MESSAGE) }
                if (indexLabelMessage < 0) {
                    data.addAll(data.size - 1, globals)
                } else {
                    data.addAll(indexLabelGlobalUser + 1, globals)
                }
            }
        }
    }

    private fun insertLocalGroup(groups: MutableList<SearchLocalGroupUI>) {
        if (groups.isNotEmpty()) {
            val indexLabel =
                data.indexOfLast { it is SearchLocalUserUI }
            if (indexLabel < 0) {
                data.addAll(0, groups)
            } else {
                data.addAll(indexLabel + 1, groups)
            }
        }
    }

    private fun insertLocalChannels(channels: MutableList<SearchLocalChannelUI>) {
        if (channels.isNotEmpty()) {
            val indexLastLocalUser =
                data.indexOfLast { it is SearchLocalUserUI }
            if (indexLastLocalUser < 0) {
                data.addAll(0, channels)
            } else {
                val indexLastLocalGroup =
                    data.indexOfLast { it is SearchLocalGroupUI }
                if (indexLastLocalGroup < 0) {
                    data.addAll(indexLastLocalUser + 1, channels)
                } else {
                    data.addAll(indexLastLocalGroup + 1, channels)
                }
            }
        }
    }

    private fun insertLocalUsers(users: MutableList<SearchLocalUserUI>) {
        if (users.isNotEmpty()) {
            users.sortByDescending { it.dateLastVisit }
            data.addAll(0, users)
        }
    }

    private fun insertMessages(messages: List<RecyclerItem>) {
        if (messages.isNotEmpty()) {
            if (!data.contains(SearchTypeLabel(SearchTarget.MESSAGE)))
                data.add(data.size, SearchTypeLabel(SearchTarget.MESSAGE))
            data.addAll(data.size, messages)
        }
    }

    private fun collectData(isGlobalSearch: Boolean) {
        GlobalScope.launch(Dispatchers.Main) {
            search_users_recycler?.post {
                if (isFinishedAllTaskSearch()) {
                    searchAdapter.data.clear()
                    searchAdapter.data.addAll(data.toSet())
                    if (searchAdapter.data.isEmpty()) {
                        updateImagePlaceholderSize()
                        layout_request_not_found?.visibility = View.VISIBLE
                        with_title_layout_request_not_found?.visibility = View.VISIBLE
                        search_users_recycler?.visibility = View.GONE
                    } else {
                        layout_request_not_found?.visibility = View.GONE
                        with_title_layout_request_not_found?.visibility = View.GONE
                        search_users_recycler?.visibility = View.VISIBLE
                    }
                    if (MainObject.mConfig.addParticipant)
                        searchAdapter.data.add(0, ButtonAddParticipantUI())
                    searchAdapter.notifyDataSetChanged()
                }
            }
        }
    }

    private fun isFinishedAllTaskSearch(): Boolean {
        return (!isSearchTaskGlobalChannel && !isSearchTaskGlobalGroup && !isSearchTaskGlobalUser
                && !isSearchTaskLocalGroup && !isSearchTaskLocalUser && !isSearchTaskLocalChannel)
    }

    inner class SearchAdapterCallback : AdapterSearch.CallbackSearchAdapter {
        override fun addParticipant() {
            MainObject.mCallback?.addParticipant()
        }

        override fun select(item: RecyclerItem) {
            fragmentManager?.popBackStack()

            when (item) {
                is SearchLocalUserUI -> MainObject.mCallback?.selectLocalUser(item)
                is SearchGlobalUserUI -> MainObject.mCallback?.selectGlobalUser(item)
                is SearchLocalChannelUI -> MainObject.mCallback?.selectLocalChannel(item)
                is SearchGlobalChannelUI -> MainObject.mCallback?.selectGlobalChannel(item)
                is SearchLocalGroupUI -> MainObject.mCallback?.selectLocalGroup(item)
                is SearchGlobalGroupUI -> MainObject.mCallback?.selectGlobalGroup(item)
                is SearchUserMessageUI -> MainObject.mCallback?.selectUserMessage(item)
                is SearchChannelMessageUI -> MainObject.mCallback?.selectChannelMessage(item)
                is SearchGroupMessageUI -> MainObject.mCallback?.selectGroupMessage(item)
            }
        }

        override fun multiSelect(item: SelectingItem) {
            selectedUser.add(item)
        }

        override fun multiUnselect(dataItem: SelectingItem) {
            selectedUser.remove(dataItem)
        }
    }
}
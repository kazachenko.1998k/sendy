package com.example.feature_profile_settings_api

import androidx.fragment.app.FragmentManager
import com.morozov.core_backend_api.FeatureBackendApi

interface ProfileSettingsStarterApi {
    fun start(manager: FragmentManager, container: Int,
              backendApi: FeatureBackendApi,
              addToBackStack: Boolean, callback: FeatureProfileSettingsCallback)
}
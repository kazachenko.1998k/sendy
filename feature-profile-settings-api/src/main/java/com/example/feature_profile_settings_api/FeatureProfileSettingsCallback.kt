package com.example.feature_profile_settings_api

interface FeatureProfileSettingsCallback {
    fun onLoggedOut()
    fun onDestroy()
}
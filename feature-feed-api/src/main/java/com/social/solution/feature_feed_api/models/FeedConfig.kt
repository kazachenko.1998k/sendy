package com.social.solution.feature_feed_api.models

data class FeedConfig(
    val showCreatePostButton: Boolean,
    val amountPostForPage: Int,
    val feedTypeFeed: FeedTypeFeed,
    val feedId: Long,
    val isMyProfile: Boolean
)
package com.social.solution.feature_feed_api

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.lifecycle.LiveData
import com.example.util_cache.AppDatabase
import com.morozov.core_backend_api.FeatureBackendApi
import com.social.solution.feature_feed_api.models.FeedConfig
import com.social.solution.feature_feed_api.models.FeedItemPostUI

interface FeatureFeedStarter {

    fun startCommonFeed(
        manager: FragmentManager,
        container: Int,
        addToBackStack: Boolean,
        networkState: LiveData<Boolean>,
        backendApi: FeatureBackendApi,
        localDB: AppDatabase,
        callback: FeatureFeedCallback
    )

    fun startUserFeed(
        networkState: LiveData<Boolean>,
        backendApi: FeatureBackendApi,
        localDB: AppDatabase,
        callback: FeatureFeedCallback,
        config: FeedConfig
    ): Fragment

    fun notifyRemove(postUI: FeedItemPostUI)
}

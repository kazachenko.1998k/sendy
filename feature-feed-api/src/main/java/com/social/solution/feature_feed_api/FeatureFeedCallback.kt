package com.social.solution.feature_feed_api

import com.morozov.core_backend_api.message.Message
import com.morozov.core_backend_api.user.UserMini
import com.social.solution.feature_feed_api.models.FeedItemPostUI

interface FeatureFeedCallback {

    fun share(post: Message)
    fun openPost(post: Message)
    fun openComment(post: Message)
    fun openProfile(user: UserMini)
    fun createPost(resultCallback: (Boolean) -> Unit)
    fun openNotification()
    fun removeNotify(postUI: FeedItemPostUI)
}
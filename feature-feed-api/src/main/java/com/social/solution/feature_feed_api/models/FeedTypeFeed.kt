package com.social.solution.feature_feed_api.models

enum class FeedTypeFeed {
    SUBSCRIPTION,
    BEST,
    USER
}
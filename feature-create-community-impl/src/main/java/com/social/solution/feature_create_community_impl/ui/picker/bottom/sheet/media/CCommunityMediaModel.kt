package com.social.solution.feature_create_community_impl.ui.picker.bottom.sheet.media

import android.graphics.Bitmap
import com.github.vivchar.rendererrecyclerviewadapter.ViewModel
import java.net.URL

open class CCommunityMediaModel(val position: Int,
                                val url: URL?, val filePath: String?,
                                var loadProgress: Int?,
                                var preview: Bitmap?): ViewModel {
    companion object{
        const val MEDIA_LOADED = 100
    }
}

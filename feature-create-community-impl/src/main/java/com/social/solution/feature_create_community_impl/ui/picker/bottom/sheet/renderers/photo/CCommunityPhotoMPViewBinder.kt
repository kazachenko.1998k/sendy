package com.social.solution.feature_create_community_impl.ui.picker.bottom.sheet.renderers.photo

import android.widget.ImageView
import com.bumptech.glide.Glide
import com.github.vivchar.rendererrecyclerviewadapter.binder.ViewBinder
import com.github.vivchar.rendererrecyclerviewadapter.binder.ViewFinder
import com.social.solution.feature_create_community_impl.R
import com.social.solution.feature_create_community_impl.ui.picker.bottom.sheet.interaction.MediaInnerAdapterCallback

class CCommunityPhotoMPViewBinder(private val mCallback: MediaInnerAdapterCallback)
    : ViewBinder.Binder<CCommunityPhotoMPModel> {

    override fun bindView(model: CCommunityPhotoMPModel, finder: ViewFinder, payloads: MutableList<Any>) {
        when {
            payloads.isEmpty() -> {
                if (model.filePath.isEmpty())
                    return
                val imagePreview = finder.find<ImageView>(R.id.imagePreview)
                val previewSize = imagePreview.resources.getDimensionPixelSize(R.dimen.media_preview_size)
                imagePreview.clipToOutline = true
                Glide.with(imagePreview)
                    .load(model.filePath)
                    .override(previewSize, previewSize)
                    .centerCrop()
                    .into(imagePreview)

                imagePreview.setOnClickListener {
                    mCallback.showMedia(model.position)
                }
            }
        }
    }
}
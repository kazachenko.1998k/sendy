package com.social.solution.feature_create_community_impl.utils

fun getCountUsers(value: Int): String {
    return when (value % 10) {
        0 -> "Нет участников"
        1 -> "$value участник"
        in (2..4) -> "$value участника"
        else -> "$value участников"
    }
}

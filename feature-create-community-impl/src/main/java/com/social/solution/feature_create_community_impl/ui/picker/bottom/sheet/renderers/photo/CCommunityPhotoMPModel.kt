package com.social.solution.feature_create_community_impl.ui.picker.bottom.sheet.renderers.photo

import com.social.solution.feature_create_community_impl.ui.picker.bottom.sheet.media.photo.CCommunityPhotoUI
import com.social.solution.feature_create_community_impl.ui.picker.bottom.sheet.renderers.CCommunityViewModelSelectable

class CCommunityPhotoMPModel(val filePath: String, val position: Int, selectedNumber: Int?) : CCommunityViewModelSelectable(selectedNumber) {
    fun convertToPhotoModel(index: Int): CCommunityPhotoUI {
        return CCommunityPhotoUI(index, null, null, filePath, null)
    }
}
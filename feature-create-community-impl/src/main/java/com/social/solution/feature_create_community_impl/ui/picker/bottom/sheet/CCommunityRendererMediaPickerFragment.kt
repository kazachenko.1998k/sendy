package com.social.solution.feature_create_community_impl.ui.picker.bottom.sheet

import android.Manifest
import android.annotation.SuppressLint
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.recyclerview.widget.GridLayoutManager
import com.github.vivchar.rendererrecyclerviewadapter.RendererRecyclerViewAdapter
import com.github.vivchar.rendererrecyclerviewadapter.ViewModel
import com.github.vivchar.rendererrecyclerviewadapter.binder.ViewBinder
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.social.solution.feature_create_community_impl.ui.picker.bottom.sheet.interaction.MediaInnerAdapterCallback
import com.social.solution.feature_create_community_impl.ui.picker.bottom.sheet.renderers.CCommunityViewModelSelectable
import com.social.solution.feature_create_community_impl.ui.picker.bottom.sheet.renderers.photo.CCommunityPhotoMPModel
import com.social.solution.feature_create_community_impl.ui.picker.bottom.sheet.renderers.photo.CCommunityPhotoMPViewBinder
import com.social.solution.feature_create_community_impl.R
import com.social.solution.feature_create_community_impl.ui.picker.bottom.sheet.utils.getMediaCursor
import com.social.solution.feature_create_community_impl.ui.picker.bottom.sheet.utils.getMediaModels
import com.social.solution.feature_create_community_impl.ui.utility.OnBackPressedFragment
import com.social.solution.feature_create_community_impl.utils.hideKeyboard
import com.social.solutions.android.util_media_pager.select.media.CCommunityMediaActivity
import com.social.solutions.android.util_media_pager.select.media.renderers.photo.PhotoModel
import com.social.solutions.android.util_media_pager.select.media.renderers.utils.MediaSelectCallback
import com.social.solutions.android.util_media_pager.select.media.renderers.utils.SerializableViewModel
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.create_community_fragment_media_picker.*
import java.util.concurrent.TimeUnit

class CCommunityRendererMediaPickerFragment: Fragment(),
    MediaInnerAdapterCallback, CCommunityMediaPickerApi, OnBackPressedFragment {

    companion object{
        private val REQUIRED_PERMISSIONS = arrayOf(Manifest.permission.READ_EXTERNAL_STORAGE)
        private const val MY_PERMISSON_REQUEST_CODE = 10
        private var isPermissionsGranted = false

        private const val PICK_CONTENT_REQUEST_CODE = 11

        private const val TAG = "RendererMediaPickerFragment_TAG"

        fun start(manager: FragmentManager, container: Int, callback: CCommunityMediaPickerCallback): CCommunityMediaPickerApi {
            val fragment = CCommunityRendererMediaPickerFragment()
            fragment.mManager = manager
            fragment.mContainer = container
            fragment.mCallback = callback

            manager.beginTransaction()
                .add(container, fragment)
                .addToBackStack(TAG)
                .commit()

            return fragment
        }
    }

    private lateinit var mManager: FragmentManager
    private var mContainer: Int? = null

    private lateinit var mCallback: CCommunityMediaPickerCallback

    private lateinit var mAdapter: RendererRecyclerViewAdapter
    private val mItems = mutableListOf<ViewModel>()

    private lateinit var mSheetBehavior: BottomSheetBehavior<LinearLayout>

    private val mSpanCount = 3

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? =
        inflater.inflate(R.layout.create_community_fragment_media_picker, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        initRecycler()

        mSheetBehavior = initBottomSheet(view)

        checkPermission()
        picker_main_constraint.setOnClickListener {
            bottomSheet.post{
                val sheetBehavior = BottomSheetBehavior.from(bottomSheet)
                sheetBehavior.state = BottomSheetBehavior.STATE_HIDDEN
            }
        }
    }

    private fun initRecycler() {
        mAdapter = RendererRecyclerViewAdapter()
        mAdapter.registerRenderers()
        mAdapter.enableDiffUtil()
        mAdapter.setItems(mItems)

        recyclerMedia.adapter = mAdapter
        recyclerMedia.layoutManager = GridLayoutManager(context, mSpanCount)
    }

    private fun RendererRecyclerViewAdapter.registerRenderers() {
        registerRenderer(ViewBinder(R.layout.create_community_item_med_pick_photo, CCommunityPhotoMPModel::class.java, CCommunityPhotoMPViewBinder(this@CCommunityRendererMediaPickerFragment)))
    }

    private fun initBottomSheet(view: View): BottomSheetBehavior<LinearLayout> {
        val sheetBehavior = BottomSheetBehavior.from(bottomSheet)
        sheetBehavior.peekHeight = 0
        sheetBehavior.addBottomSheetCallback(object : BottomSheetBehavior.BottomSheetCallback() {
            @SuppressLint("SwitchIntDef")
            override fun onStateChanged(bottomSheet: View, newState: Int) {
                when (newState) {
                    BottomSheetBehavior.STATE_HIDDEN -> {
                        fragmentManager?.popBackStack()
                    }
                    BottomSheetBehavior.STATE_EXPANDED -> {
                    }
                    BottomSheetBehavior.STATE_COLLAPSED -> {
                    }
                }
            }
            override fun onSlide(bottomSheet: View, slideOffset: Float) {

            }
        })
        sheetBehavior.isHideable = true
        bottomSheet.post {
            sheetBehavior.setPeekHeight(view.measuredHeight/2, true)
            sheetBehavior.state = BottomSheetBehavior.STATE_COLLAPSED
        }
        return sheetBehavior
    }

    // Permissions
    private fun checkPermission() {
        val mContext = context ?: return
        if (ContextCompat.checkSelfPermission(mContext, REQUIRED_PERMISSIONS[0]) == PackageManager.PERMISSION_GRANTED) {
            isPermissionsGranted = true
            loadMediaData()
        } else {
            requestPermissions(REQUIRED_PERMISSIONS, MY_PERMISSON_REQUEST_CODE)
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        if (requestCode == MY_PERMISSON_REQUEST_CODE) {
            if (permissions.size == 1 &&
                permissions[0] == REQUIRED_PERMISSIONS[0] &&
                grantResults[0] == PackageManager.PERMISSION_GRANTED
            ) {
                isPermissionsGranted = true
                loadMediaData()
            } else {
                Log.d(this.javaClass.simpleName, "My Location permission denied")
            }
        } else
            super.onRequestPermissionsResult(requestCode, permissions, grantResults)
    }

    // OnBackPressedFragment
    override fun onBackPressed() {
        bottomSheet.post{
            val sheetBehavior = BottomSheetBehavior.from(bottomSheet)
            sheetBehavior.state = BottomSheetBehavior.STATE_HIDDEN
        }
    }

    // Media data
    private val mPlaceholdersNumber = 10

    private fun loadMediaData() {
        if (isPermissionsGranted.not())
            return

        mItems.addPlaceholders()

        var index = 0
        val mediaCursor = getMediaCursor(context) ?:return
        val disp = mediaCursor.getMediaModels()
            .delay(430L, TimeUnit.MILLISECONDS)
            .subscribeOn(Schedulers.io())
            .observeOn(Schedulers.io())
            .subscribe({model ->
                if (index == 0)
                    mItems.removePlaceholders()

                mItems.addItemLast(model.convertToViewModel(context, index))
                index++
            }, {
                it.printStackTrace()
            })
    }

    private fun MutableList<ViewModel>.addItemFirst(item: ViewModel) {
        add(0, item)
        recyclerMedia.post {
            mAdapter.setItems(mItems)
        }
    }

    private fun MutableList<ViewModel>.addItemLast(item: ViewModel) {
        add(item)
        recyclerMedia.post {
            mAdapter.setItems(mItems)
        }
    }

    private fun MutableList<ViewModel>.removePlaceholders() {
        clear()
        recyclerMedia.post {
            mAdapter.setItems(mItems)
        }
    }

    private fun MutableList<ViewModel>.addPlaceholders() {
        for (i in 1 until mPlaceholdersNumber) {
            addItemLast(CCommunityPhotoMPModel("", i, null))
        }
    }

    private val mClickedItemsList = mutableListOf<Int>()
    private val mMaxItems = 1

    override fun onItemClicked(position: Int) {
        when {
            mClickedItemsList.contains(position) -> {
                val removedIndex = mClickedItemsList.indexOf(position)
                mClickedItemsList.remove(position)

                val clickedModel = mItems[position]
                if (clickedModel is CCommunityViewModelSelectable) {
                    clickedModel.selectedNumber = null
                    mAdapter.notifyItemChanged(position)
                }
                for ((index, mItem) in mClickedItemsList.withIndex()) {
                    if (index >= removedIndex) {
                        val changeModel = mItems[mItem]
                        if (changeModel is CCommunityViewModelSelectable) {
                            changeModel.selectedNumber = index+1
                            mAdapter.notifyItemChanged(mItem)
                        }
                    }
                }
                if (mClickedItemsList.isEmpty()) {
                    context?.let { hideKeyboard(it) }
                }
            }
            mClickedItemsList.size == mMaxItems -> {
                mAdapter.notifyItemChanged(position)
                return
            }
            else -> {
                mClickedItemsList.add(position)
                val changeModel = mItems[position]
                if (changeModel is CCommunityViewModelSelectable) {
                    changeModel.selectedNumber = mClickedItemsList.size
                    mAdapter.notifyItemChanged(position)
                }
                if (mClickedItemsList.size == 1) {
                }
            }
        }
    }

    override fun showMedia(position: Int) {
        val tmpList = mutableListOf<SerializableViewModel>()
        for ((index, mItem) in mItems.withIndex()) {
            if (index == position)
                when(mItem) {
                    is CCommunityPhotoMPModel -> tmpList.add(PhotoModel(mItem.filePath))
                }
        }
        if (tmpList.isEmpty()) return
        CCommunityMediaActivity.start(this, tmpList, object : MediaSelectCallback {
            override fun onSelected(data: List<SerializableViewModel>, sign: String) {
                val resList = mutableListOf<ViewModel>()
                for ((index, datum) in data.withIndex()) {
                    when(datum){
                        is PhotoModel -> resList.add(CCommunityPhotoMPModel(datum.filePath, index, null))
                    }
                }
                mCallback.onSelected(resList, if(sign.isEmpty()) null else sign)
                mSheetBehavior.state = BottomSheetBehavior.STATE_HIDDEN
            }
        })
    }

    private fun getSelectedData(): List<ViewModel> {
        val result = mutableListOf<ViewModel>()
        for ((index, mDatum) in mItems.withIndex()) {
            if (mClickedItemsList.contains(index))
                result.add(mDatum)
        }
        return result
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (requestCode == PICK_CONTENT_REQUEST_CODE) {
            data ?: return
            val uri = data.data as Uri
            mCallback.onFileSelected(uri)
            mSheetBehavior.state = BottomSheetBehavior.STATE_HIDDEN
        }
        super.onActivityResult(requestCode, resultCode, data)
    }
}
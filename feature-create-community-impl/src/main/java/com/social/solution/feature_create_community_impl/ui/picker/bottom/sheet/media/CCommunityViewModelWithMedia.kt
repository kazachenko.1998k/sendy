package com.social.solution.feature_create_community_impl.ui.picker.bottom.sheet.media

import com.github.vivchar.rendererrecyclerviewadapter.ViewModel
import java.util.*

open class CCommunityViewModelWithMedia(var media: List<CCommunityMediaModel>, date: Calendar): ViewModel
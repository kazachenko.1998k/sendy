package com.social.solution.feature_create_community_impl.ui.picker.bottom.sheet.utils

import android.view.animation.Interpolator
import kotlin.math.cos
import kotlin.math.pow

class MyBounceInterpolator(val amplitude: Double, val frequency: Double): Interpolator {

    override fun getInterpolation(time: Float): Float {
        return (-1 * Math.E.pow(-time / amplitude) *
                cos(frequency * time) + 1).toFloat()
    }
}
package com.social.solution.feature_create_community_impl

import com.example.util_cache.chat.ChatDao
import com.morozov.core_backend_api.FeatureBackendApi
import com.morozov.core_backend_api.chat.ChatApi
import com.morozov.core_backend_api.common.CommonApi
import com.social.solution.feature_create_community_api.FeatureCreateChannelCallback
import com.social.solution.feature_create_community_api.FeatureCreateGroupCallback
import com.social.solution.feature_create_community_api.FeatureSettingChannelCallback
import com.social.solution.feature_create_community_api.FeatureSettingGroupCallback


object MainObject {

    var mCallbackSettingChannel: FeatureSettingChannelCallback? = null
    var mCallbackSettingGroup: FeatureSettingGroupCallback? = null
    var mCallbackChannel: FeatureCreateChannelCallback? = null
    var mCallbackGroup: FeatureCreateGroupCallback? = null
    var mChatDao: ChatDao? = null
    var mCommonApi: CommonApi? = null
    var mBackendApi: FeatureBackendApi? = null
    var TAG = "CREATE_GROUP_MODULE"
}
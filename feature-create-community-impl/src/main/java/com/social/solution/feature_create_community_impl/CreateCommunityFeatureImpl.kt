package com.social.solution.feature_create_community_impl

import android.content.Context
import com.morozov.core_backend_api.FeatureBackendApi
import com.social.solution.feature_create_community_api.FeatureCreateGroupApi
import com.social.solution.feature_create_community_api.FeatureCreateGroupStarter

class CreateCommunityFeatureImpl(
    private val starter: FeatureCreateGroupStarter,
    private val context: Context,
    private val api: FeatureBackendApi
) : FeatureCreateGroupApi {

    override fun createGroupStarter(): FeatureCreateGroupStarter = starter

}
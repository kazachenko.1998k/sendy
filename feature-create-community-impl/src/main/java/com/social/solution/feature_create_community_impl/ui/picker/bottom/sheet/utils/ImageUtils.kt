package com.social.solution.feature_create_community_impl.ui.picker.bottom.sheet.utils

import android.graphics.Bitmap
import android.graphics.drawable.Drawable
import android.widget.ImageView
import com.bumptech.glide.Glide
import com.bumptech.glide.request.target.CustomTarget
import com.bumptech.glide.request.transition.Transition
import com.social.solution.feature_create_community_impl.ui.picker.bottom.sheet.media.CCommunityMediaModel
import com.social.solution.feature_create_community_impl.ui.picker.bottom.sheet.media.CCommunityOnMediaClicked
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers

fun Bitmap.scaleDown(): Bitmap? {
    val maxImageSize = 550f
    val filter = true
    val ratio = Math.min(
        maxImageSize / width,
        maxImageSize / height
    )
    val width = Math.round(ratio * width)
    val height = Math.round(ratio * height)
    return Bitmap.createScaledBitmap(
        this, width,
        height, filter
    )
}

fun ImageView.loadImageScaledDown(url: String, model: CCommunityMediaModel, callback: CCommunityOnMediaClicked): Disposable {
    return Observable.create<Bitmap> {
        Glide.with(context)
            .asBitmap()
            .load(url)
            .into(object : CustomTarget<Bitmap>() {
                override fun onLoadCleared(placeholder: Drawable?) {}
                override fun onResourceReady(resource: Bitmap, transition: Transition<in Bitmap>?) {
                    it.onNext(resource)
                }
            })
    }
        .observeOn(AndroidSchedulers.mainThread())
        .subscribeOn(Schedulers.io())
        .subscribe({ resource ->
            model.preview = resource
            model.loadProgress = CCommunityMediaModel.MEDIA_LOADED
            callback.onUpdateItem(model.position)
        }, {
            it.printStackTrace()
        })
}

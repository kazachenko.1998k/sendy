package com.social.solution.feature_create_community_impl.ui.fragments.channel


import android.annotation.SuppressLint
import android.content.Context
import android.os.Bundle
import android.text.method.ScrollingMovementMethod
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Scroller
import android.widget.Toast
import androidx.activity.OnBackPressedCallback
import androidx.core.content.ContextCompat
import androidx.core.widget.addTextChangedListener
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.bitmap.BitmapTransitionOptions
import com.bumptech.glide.request.RequestOptions
import com.example.util_cache.chat.saveOrUpdateChatMini
import com.example.util_load_send_workers.repository.DialogWorkRepository
import com.example.util_load_send_workers.repository.SendFileManager
import com.morozov.core_backend_api.chat.Chat
import com.social.solution.feature_create_community_api.models.CreateCommunityAbstractItemRecycler
import com.social.solution.feature_create_community_api.models.CreateCommunityLocalUserUI
import com.social.solution.feature_create_community_impl.MainObject
import com.social.solution.feature_create_community_impl.MainObject.TAG
import com.social.solution.feature_create_community_impl.MainObject.mCallbackChannel
import com.social.solution.feature_create_community_impl.R
import com.social.solution.feature_create_community_impl.utils.hideKeyboard
import kotlinx.android.synthetic.main.create_channel_fragment.*

class CreateChannelFragment(private val parentContainer: Int, private val uid: Long) : Fragment() {

    private lateinit var mCreateViewModel: CreateChannelViewModel

    companion object {
        const val NAME = "CREATE_CHANNEL_FRAGMENT"
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        val callback: OnBackPressedCallback = object : OnBackPressedCallback(true) {
            override fun handleOnBackPressed() {
                fragmentManager?.popBackStack()
            }
        }
        requireActivity().onBackPressedDispatcher.addCallback(
            this,
            callback
        )
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        setHasOptionsMenu(true)
        mCreateViewModel = ViewModelProviders.of(this).get(CreateChannelViewModel::class.java)
        return inflater.inflate(R.layout.create_channel_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        this.initListeners()
    }

    override fun onResume() {
        super.onResume()
        this.backupData()
    }

    private fun backupData() {
        val bitmapPath = mCreateViewModel.getAvatarLocalPath().value
        if (bitmapPath != null) {
            this.setPhoto(bitmapPath)
        }
    }

    private fun initListeners() {
        create_channel_input_name?.addTextChangedListener {
            val name = if (it?.trim() != null) it.trim() else ""
            if (name.isEmpty()) {
                create_channel_fragment_button_continue.setBackgroundColor(
                    ContextCompat.getColor(
                        context!!,
                        R.color.pumice
                    )
                )
                create_channel_fragment_button_continue.isEnabled = false
            } else {
                create_channel_fragment_button_continue.setBackgroundColor(
                    ContextCompat.getColor(
                        context!!,
                        R.color.colorAccent
                    )
                )
                create_channel_fragment_button_continue.isEnabled = true
                mCreateViewModel.refreshName(name.toString())
            }
        }

        create_channel_input_description?.setScroller(Scroller(context))
        create_channel_input_description?.maxLines = 7
        create_channel_input_description?.isVerticalScrollBarEnabled = true
        create_channel_input_description?.movementMethod = ScrollingMovementMethod()
        create_channel_input_description?.addTextChangedListener {
            val description = if (it?.trim() != null) it.trim() else ""
            mCreateViewModel.refreshDescription(description.toString())
        }
        create_channel_add_avatar?.setOnClickListener {
            hideKeyboard(context!!)
            mCallbackChannel?.onAvatarChangeClickListener { url ->
                if (url != null)
                    sendAvatar(url, it.context)
            }
        }
        create_channel_fragment_button_continue?.setOnClickListener {
            if (create_channel_fragment_button_continue.isEnabled) {
                hideKeyboard(context!!)
                mCallbackChannel?.addUser()
            }
        }
        create_channel_action_back?.setOnClickListener {
            fragmentManager?.popBackStack()
            hideKeyboard(context!!)
        }
    }

    @SuppressLint("CheckResult")
    private fun sendAvatar(url: String, context: Context) {
        SendFileManager.with(context)
            .setBackendApi(MainObject.mBackendApi)
            .setUserId(uid)
            .setMessageType(0)
            .setFilesExtended(listOf(url).map {
                SendFileManager.Transaction.File(it)
            })
            .loadFiles()
        sendReceiversImage(url)
    }

    private fun sendReceiversImage(url: String) {
        DialogWorkRepository.getLoadingLiveData()
            .observe(viewLifecycleOwner, Observer { observ ->
                Log.d(TAG, "fileId = " + observ.fileId)
                mCreateViewModel.setAvatarPath(observ.fileId)
                mCreateViewModel.setAvatarLocalPath(url)
                setPhoto(url)
            })
    }

    fun setPhoto(filePath: String) {
        Glide.with(context!!).asBitmap().transition(BitmapTransitionOptions.withCrossFade())
            .load(filePath).apply(RequestOptions().circleCrop())
            .into(create_channel_add_avatar)
    }

    @SuppressLint("CheckResult")
    fun addUsers(selectedUser: MutableList<CreateCommunityAbstractItemRecycler>) {
        val listLocalUsers =
            selectedUser.filterIsInstance<CreateCommunityLocalUserUI>().toMutableList()
        mCreateViewModel.createChannel(listLocalUsers, MainObject.mBackendApi!!.chatApi()).subscribe({ chat ->
            saveChannelToDB(chat)
        }, { throwable: Throwable? ->
            Log.e(TAG, "Error add User")
            Toast.makeText(context, "Вы не можете создать канал", Toast.LENGTH_SHORT).show()
            throwable?.printStackTrace()
        })
    }

    @SuppressLint("CheckResult")
    private fun saveChannelToDB(chat: Chat) {
        MainObject.mChatDao?.saveOrUpdateChatMini(chat)?.subscribe({
            Log.d(TAG, "CHANNEL added to localdb = $chat")
            mCallbackChannel?.openChat(chat)
        }, { t ->
            Log.d(TAG, "ERROR SAVE CHANNEL")
            mCallbackChannel?.openChat(chat)
            t.printStackTrace()
        })
    }

}
package com.social.solution.feature_create_community_impl.utils

import java.util.regex.Matcher
import java.util.regex.Pattern

fun String.containsNotLinkSymbols(): Boolean {
    val p: Pattern = Pattern.compile("[^a-z0-9]", Pattern.CASE_INSENSITIVE)
    val m: Matcher = p.matcher(this)
    while (m.find()) {
        return true
    }
    return false
}
package com.social.solution.feature_create_community_impl.ui.fragments.channel

import android.graphics.Bitmap
import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.morozov.core_backend_api.AnyInnerRequest
import com.morozov.core_backend_api.AnyRequest
import com.morozov.core_backend_api.chat.Chat
import com.morozov.core_backend_api.chat.ChatApi
import com.morozov.core_backend_api.chat.requests.ChatCreateNewRequest
import com.morozov.core_backend_api.chat.requests.ChatSetChatPrivacyRequest
import com.morozov.core_backend_api.common.CommonApi
import com.morozov.core_backend_api.common.request.CommonCheckFreeNickRequest
import com.social.solution.feature_create_community_api.models.CreateCommunityLocalUserUI
import com.social.solution.feature_create_community_impl.MainObject.TAG
import io.reactivex.Single

class CreateChannelViewModel : ViewModel() {
    private val name = MutableLiveData<String>()
    private val description = MutableLiveData<String>()
    private val avatarPath = MutableLiveData<String>()
    private val avatarLocalPath = MutableLiveData<String>()
    private val link = MutableLiveData<String>()

    fun createChannel(
        selectedUser: MutableList<CreateCommunityLocalUserUI>,
        chatApi: ChatApi?
    ): Single<Chat> {

        return Single.create { single ->
            var descriptionText = ""
            if (chatApi == null) single.onError(Exception("Chat Api is null"))
            else {
                if (description.value != null) descriptionText = description.value!!
                //todo: change id_avatar
                Log.d(TAG, "fileId11 = " + avatarPath.value)
                if (name.value != null) {
                    val chatCreateNewRequest = ChatCreateNewRequest(
                        2,
                        null,
                        name.value!!.trim(),
                        descriptionText.trim(),
                        avatarPath.value,
                        selectedUser.map { it.id.toLong() }.toMutableList()
                    )
                    val anyInnerRequest = AnyInnerRequest(chatCreateNewRequest)
                    val anyRequest = AnyRequest(anyInnerRequest)
                    chatApi.createNew(anyRequest) {
                        Log.d(TAG, "fileId333445 = " +  it.data)
                        if (it.result) {
                            if (it.data != null) {
                                Log.d(TAG, "fileId33344 = ")
                                single.onSuccess(it.data!!.chat)
                            }
                            else
                                single.onError(Exception(it.error?.msg))
                        } else {
                            single.onError(Exception(it.error?.msg))
                        }
                    }
                } else {
                    single.onError(Exception("Name is Empty"))
                }
            }
        }
    }

    fun refreshName(name: String) {
        this.name.value = name
    }

    fun refreshDescription(description: String) {
        this.description.value = description
    }


    fun getAvatarPath(): LiveData<String> {
        return avatarPath
    }

    fun setAvatarPath(filePath: String) {
        avatarPath.value = filePath
    }

    fun getAvatarLocalPath(): LiveData<String> {
        return avatarLocalPath
    }

    fun setAvatarLocalPath(filePath: String) {
        avatarLocalPath.value = filePath
    }

    fun setSettings(isPrivate: Boolean, chatApi: ChatApi?, chat: Chat): Single<Chat> {
        return Single.create { single ->
            if (chatApi != null) {
                val privateKey = if (isPrivate) 3 else 1
                if (isPrivate) link.value = null
                val commonCheckFreeNickRequest =
                    ChatSetChatPrivacyRequest(chat.id, privateKey, link.value)
                val anyInnerRequest = AnyInnerRequest(commonCheckFreeNickRequest)
                val anyRequest = AnyRequest(anyInnerRequest)
                Log.d(TAG, anyRequest.toString())
                chatApi.setChatPrivacy(anyRequest) {
                    if (it.result) {
                        chat.private = privateKey
                        if (link.value != null) {
                            chat.links = mutableListOf("sendy.ru/${link.value}")
                            chat.nick = link.value
                        }
                        single.onSuccess(chat)
                    } else {
                        single.onError(Exception(it.error?.msg))
                    }
                }
            } else {
                single.onError(Exception("Chat api is null"))
            }
        }
    }

    fun checkLinkUnique(link: String, commonApi: CommonApi?): Single<Boolean> {
        return Single.create { single ->
            if (commonApi != null) {
                val commonCheckFreeNickRequest = CommonCheckFreeNickRequest(link)
                val anyInnerRequest = AnyInnerRequest(commonCheckFreeNickRequest)
                val anyRequest = AnyRequest(anyInnerRequest)
                commonApi.checkFreeNick(anyRequest) {
                    if (it.result) {
                        if (it.data != null) {
                            if (it.data!!.status) {
                                this@CreateChannelViewModel.link.value = link
                                single.onSuccess(true)
                            } else {
                                single.onError(Exception("nick not unique"))
                            }
                        } else {
                            single.onError(Exception("Data is null"))
                        }
                    } else {
                        single.onError(Exception(it.error?.msg))
                    }
                }
            } else {
                single.onError(Exception("Common api is null"))
            }
        }
    }

    fun setLink(inviteHash: String) {
//        link.value = inviteHash
    }
}

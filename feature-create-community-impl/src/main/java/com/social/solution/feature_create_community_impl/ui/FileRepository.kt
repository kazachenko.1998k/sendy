package com.social.solution.feature_create_community_impl.ui

import android.content.Context
import android.content.Intent
import android.net.Uri
import androidx.core.app.JobIntentService
import com.morozov.core_backend_api.AnyInnerRequest
import com.morozov.core_backend_api.AnyRequest
import com.morozov.core_backend_api.file.request.FileUploadRequest
import com.morozov.lib_backend.LibBackendDependency
import com.social.solution.feature_create_community_impl.utils.getName
import com.social.solution.feature_create_community_impl.utils.getSize
import com.social.solutions.android.util_load_send_services.media.send.services.SendMediaService
import io.reactivex.Single
import okhttp3.MediaType.Companion.toMediaTypeOrNull

object FileRepository {
    fun sendImage(context: Context, imagePath: String, userId: Long): Single<String> {
        return Single.create { emitter ->
            val fileApi = LibBackendDependency.featureBackendApi().fileApi()
            val uri = Uri.parse(imagePath)
            val fileType = "image/*".toMediaTypeOrNull() ?: return@create
            val param = FileUploadRequest(
                uri.getName(context) ?: "empty_file",
                (uri.getSize(context) ?: 0) / 1000,
                fileType.type
            )
            val sendMessage = AnyInnerRequest(param)
            val data = AnyRequest(sendMessage)
            fileApi.uploadFile(data) { event ->
                val dataResponse = event.data ?: return@uploadFile
                emitter.onSuccess(dataResponse.file_id)
                uploadImageService(
                    context,
                    imagePath,
                    dataResponse.file_id,
                    userId,
                    dataResponse.file_upload_to
                )
            }
        }
    }

    private fun uploadImageService(
        context: Context,
        imagePath: String,
        imageId: String,
        userId: Long,
        baseUrl: String
    ) {
        val serviceIntent = Intent().apply {
            putExtra(SendMediaService.PHOTO_PATH, imagePath)
            putExtra(SendMediaService.FILE_ID, imageId)
            putExtra(SendMediaService.USER_ID, userId)
            putExtra(SendMediaService.BASE_URL, baseUrl)
        }
        JobIntentService.enqueueWork(
            context, SendMediaService::class.java,
            SendMediaService.SEND_PHOTO_JOB_ID, serviceIntent
        )
    }
}
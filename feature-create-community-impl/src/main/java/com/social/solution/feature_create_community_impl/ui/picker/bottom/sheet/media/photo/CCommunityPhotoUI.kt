package com.social.solution.feature_create_community_impl.ui.picker.bottom.sheet.media.photo

import android.graphics.Bitmap
import com.social.solution.feature_create_community_impl.ui.picker.bottom.sheet.media.CCommunityMediaModel
import java.net.URL

class CCommunityPhotoUI(position: Int, url: URL?, loadProgress: Int?, filePath: String?, preview: Bitmap?): CCommunityMediaModel(position, url, filePath, loadProgress, preview) {
}
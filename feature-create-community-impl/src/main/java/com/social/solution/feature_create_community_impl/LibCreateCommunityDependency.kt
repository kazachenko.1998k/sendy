package com.social.solution.feature_create_community_impl

import android.content.Context
import com.morozov.core_backend_api.FeatureBackendApi
import com.social.solution.feature_create_community_api.FeatureCreateGroupApi
import com.social.solution.feature_create_community_impl.start.CreateCommunityStarterImpl

object LibCreateCommunityDependency {

    fun featureCreateGroupApi(context: Context, backend: FeatureBackendApi): FeatureCreateGroupApi {
        return CreateCommunityFeatureImpl(CreateCommunityStarterImpl(), context, backend)
    }
}
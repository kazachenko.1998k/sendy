package com.social.solution.feature_create_community_impl.ui.picker.bottom.sheet.utils

import android.net.Uri

interface OnFileClickListener {
    fun onFileClicked(uri: Uri)
}
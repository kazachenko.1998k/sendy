package com.social.solution.feature_create_community_impl.ui.picker.bottom.sheet.utils

interface SelectableModel {
    var isSelected: Boolean
}
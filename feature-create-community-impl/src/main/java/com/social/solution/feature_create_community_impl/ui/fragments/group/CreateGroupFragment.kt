package com.social.solution.feature_create_community_impl.ui.fragments.group


import android.annotation.SuppressLint
import android.content.Context
import android.os.Bundle
import android.text.method.ScrollingMovementMethod
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Scroller
import android.widget.Toast
import androidx.activity.OnBackPressedCallback
import androidx.core.content.ContextCompat
import androidx.core.widget.addTextChangedListener
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.bitmap.BitmapTransitionOptions
import com.bumptech.glide.request.RequestOptions
import com.example.util_cache.chat.saveOrUpdateChatMini
import com.example.util_load_send_workers.repository.DialogWorkRepository
import com.example.util_load_send_workers.repository.SendFileManager
import com.morozov.core_backend_api.chat.Chat
import com.social.solution.feature_create_community_api.models.CreateCommunityAbstractItemRecycler
import com.social.solution.feature_create_community_api.models.CreateCommunityLocalUserUI
import com.social.solution.feature_create_community_impl.MainObject
import com.social.solution.feature_create_community_impl.MainObject.TAG
import com.social.solution.feature_create_community_impl.R
import com.social.solution.feature_create_community_impl.ui.FileRepository
import com.social.solution.feature_create_community_impl.utils.hideKeyboard
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.create_group_fragment.*

class CreateGroupFragment(private val parentContainer: Int, private val uid: Long) : Fragment() {

    private lateinit var mCreateViewModel: CreateGroupViewModel

    companion object {
        const val NAME = "CREATE_GROUP_FRAGMENT"
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        val callback: OnBackPressedCallback = object : OnBackPressedCallback(true) {
            override fun handleOnBackPressed() {
                fragmentManager?.popBackStack()
            }
        }
        requireActivity().onBackPressedDispatcher.addCallback(
            this,
            callback
        )
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        setHasOptionsMenu(true)
        mCreateViewModel = ViewModelProviders.of(this).get(CreateGroupViewModel::class.java)
        return inflater.inflate(R.layout.create_group_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        this.initListeners()
    }

    override fun onResume() {
        super.onResume()
        this.backupData()
    }

    private fun backupData() {
        val bitmapPath = mCreateViewModel.getAvatarLocalPath().value
        if (bitmapPath != null) {
            this.setPhoto(bitmapPath)
        }
    }

    private fun initListeners() {
        create_group_input_name?.addTextChangedListener {
            val name = if (it?.trim() != null) it.trim() else ""

            if (name.isEmpty()) {
                create_group_fragment_button_continue.setBackgroundColor(
                    ContextCompat.getColor(
                        context!!,
                        R.color.pumice
                    )
                )
                create_group_fragment_button_continue.isEnabled = false
            } else {
                create_group_fragment_button_continue.setBackgroundColor(
                    ContextCompat.getColor(
                        context!!,
                        R.color.colorAccent
                    )
                )
                create_group_fragment_button_continue.isEnabled = true
                mCreateViewModel.refreshName(name.toString())
            }
        }
        create_group_input_description?.setScroller(Scroller(context))
        create_group_input_description?.maxLines = 7
        create_group_input_description?.isVerticalScrollBarEnabled = true
        create_group_input_description?.movementMethod = ScrollingMovementMethod()
        create_group_input_description?.addTextChangedListener {
            val description = if (it?.trim() != null) it.trim() else ""
            mCreateViewModel.refreshDescription(description.toString())
        }
        create_group_add_avatar?.setOnClickListener {
            MainObject.mCallbackGroup?.onAvatarChangeClickListener { url ->
                if (url != null)
                    sendAvatar(url, it.context)
            }
        }
        create_group_fragment_button_continue?.setOnClickListener {
            if (create_group_fragment_button_continue.isEnabled) {
                MainObject.mCallbackGroup?.addUser()
            }
        }
        create_group_action_back?.setOnClickListener {
            fragmentManager?.popBackStack()
            hideKeyboard(context!!)
        }
    }

    @SuppressLint("CheckResult")
    private fun sendAvatar(url: String, context: Context) {
        SendFileManager.with(context)
            .setBackendApi(MainObject.mBackendApi)
            .setUserId(uid)
            .setMessageType(0)
            .setFilesExtended(listOf(url).map {
                SendFileManager.Transaction.File(it)
            })
            .loadFiles()
        sendReceiversImage(url)
    }

    private fun sendReceiversImage(url: String) {
        DialogWorkRepository.getLoadingLiveData()
            .observe(viewLifecycleOwner, Observer { observ ->
                Log.d(TAG, "fileId = " + observ.fileId)
                mCreateViewModel.setAvatarPath(observ.fileId)
                mCreateViewModel.setAvatarLocalPath(url)
                setPhoto(url)
            })
    }

    private fun setPhoto(filePath: String) {
        Glide.with(context!!).asBitmap().transition(BitmapTransitionOptions.withCrossFade())
            .load(filePath).apply(RequestOptions().circleCrop())
            .into(create_group_add_avatar)
    }

    @SuppressLint("CheckResult")
    fun addUsers(selectedUser: MutableList<CreateCommunityAbstractItemRecycler>) {
        val listLocalUsers =
            selectedUser.filterIsInstance<CreateCommunityLocalUserUI>().toMutableList()
        mCreateViewModel.createGroup(listLocalUsers, MainObject.mBackendApi!!.chatApi()).subscribe({ chat ->
            saveGroupToDB(chat)
        }, { throwable: Throwable? ->
            Log.e(TAG, "Create group error")
            Toast.makeText(context, "Вы не можете создать группу", Toast.LENGTH_SHORT).show()
            throwable?.printStackTrace()
        })
    }

    @SuppressLint("CheckResult")
    private fun saveGroupToDB(chat: Chat) {
        MainObject.mChatDao?.saveOrUpdateChatMini(chat)?.subscribe({
            Log.d(TAG, "GROUP added to localdb = $chat")
            MainObject.mCallbackGroup?.openChat(chat)
        }, { t ->
            Log.d(TAG, "ERROR SAVE GROUP")
            MainObject.mCallbackGroup?.openChat(chat)
            t.printStackTrace()
        })
    }

}
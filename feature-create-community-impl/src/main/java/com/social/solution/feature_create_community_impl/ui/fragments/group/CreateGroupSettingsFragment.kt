package com.social.solution.feature_create_community_impl.ui.fragments.group

import android.content.ClipData
import android.content.ClipboardManager
import android.content.Context
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.activity.OnBackPressedCallback
import androidx.core.widget.addTextChangedListener
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import com.morozov.core_backend_api.chat.Chat
import com.social.solution.feature_create_community_impl.MainObject
import com.social.solution.feature_create_community_impl.R
import com.social.solution.feature_create_community_impl.utils.containsNotLinkSymbols
import com.social.solution.feature_create_community_impl.utils.hideKeyboard
import kotlinx.android.synthetic.main.setting_group_fragment.*

class CreateGroupSettingsFragment(private val chat: Chat) : Fragment() {

    private lateinit var mCreateViewModel: CreateGroupViewModel

    companion object {
        const val NAME = "SETTINGS_GROUP_FRAGMENT"
        const val COUNT_SYMBOLS_IN_LINK = 30
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        val callback: OnBackPressedCallback = object : OnBackPressedCallback(true) {
            override fun handleOnBackPressed() {
                fragmentManager?.popBackStack()
            }
        }
        requireActivity().onBackPressedDispatcher.addCallback(
            this,
            callback
        )
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        setHasOptionsMenu(true)
        mCreateViewModel = ViewModelProviders.of(this).get(CreateGroupViewModel::class.java)
        return inflater.inflate(R.layout.setting_group_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        this.initListeners()
        this.initData()
        this.initStateRadioButton()
        mCreateViewModel.setLink(chat.inviteHash.toString())
    }

    private fun initStateRadioButton() {
        if (chat.private == 3) {
            radio_button_public_group.isChecked = false
            radio_button_private_group.isChecked = true
            public_link_layout.visibility = View.GONE
            private_link_layout.visibility = View.VISIBLE
        } else {
            radio_button_public_group.isChecked = true
            radio_button_private_group.isChecked = false
            public_link_layout.visibility = View.VISIBLE
            private_link_layout.visibility = View.GONE
        }
    }

    private fun initData() {
        setting_group_private_link?.setText("https://sendy.ru/joinchat/${chat.inviteHash}")
    }

    private fun initListeners() {
        setting_group_action_back?.setOnClickListener {
            fragmentManager?.popBackStack()
            MainObject.mCallbackSettingGroup?.openGroup(chat)
        }
        public_radio_layout?.setOnClickListener {
            radio_button_private_group.isChecked = false
            radio_button_public_group.isChecked = true
            public_link_layout.visibility = View.VISIBLE
            private_link_layout.visibility = View.GONE
        }
        private_radio_layout?.setOnClickListener {
            radio_button_public_group.isChecked = false
            radio_button_private_group.isChecked = true
            public_link_layout.visibility = View.GONE
            private_link_layout.visibility = View.VISIBLE
        }
        radio_button_public_group?.setOnClickListener {
            radio_button_private_group.isChecked = false
            radio_button_public_group.isChecked = true
            public_link_layout.visibility = View.VISIBLE
            private_link_layout.visibility = View.GONE
        }
        radio_button_private_group?.setOnClickListener {
            radio_button_public_group.isChecked = false
            radio_button_private_group.isChecked = true
            public_link_layout.visibility = View.GONE
            private_link_layout.visibility = View.VISIBLE
        }
        setting_group_input_description?.addTextChangedListener { it ->
            if (it!!.length > 5) {
                if (it.toString().containsNotLinkSymbols()) {
                    setting_group_fragment_button_continue.isEnabled = false
                    setting_group_input_description.error =
                        "Допустимы только символы A-z и цифры от 0..9"
                } else {
                    mCreateViewModel.checkLinkUnique(it.toString(), MainObject.mCommonApi)
                        .subscribe({
                            setting_group_fragment_button_continue.isEnabled = true

                        }, { t: Throwable? ->
                            setting_group_fragment_button_continue.isEnabled = false
                            setting_group_input_description.error =
                                "Ссылка уже занята"
                        })
                }
            } else {
                setting_group_fragment_button_continue.isEnabled = false
                setting_group_input_description.error =
                    "Ссылка содержит меньше 6 символов"
            }
        }
        setting_group_fragment_button_continue?.setOnClickListener {
            if (radio_button_public_group.isChecked && setting_group_input_description.text!!.isEmpty()){
                setting_group_fragment_button_continue.isEnabled = false
                setting_group_input_description.error =
                    "Введите короткую ссылку. A-z, 0..9"
                return@setOnClickListener
            }
            Log.d("TEST_UPDATE_GROUP", "privateStart = " + chat.private)
            mCreateViewModel.setSettings(
                radio_button_private_group.isChecked,
                MainObject.mBackendApi!!.chatApi(),
                chat
            ).subscribe({ chatNew ->
                Toast.makeText(context, "Данные группы сохранены", Toast.LENGTH_SHORT).show()
                fragmentManager?.popBackStack()
                MainObject.mCallbackSettingGroup?.openGroup(chat)
            }, { t: Throwable? ->
                Log.e(MainObject.TAG, "setSettings group error")
                t?.printStackTrace()
            })
            hideKeyboard(context!!)
        }
        copy_link?.setOnClickListener {
            copyLink()
        }
        setting_group_private_link?.setOnClickListener {
            copyLink()
        }
        share_link?.setOnClickListener {
            MainObject.mCallbackSettingGroup?.shareLink(setting_group_input_description.text.toString())
        }
    }

    private fun copyLink() {
        val clipboard: ClipboardManager =
            context!!.getSystemService(Context.CLIPBOARD_SERVICE) as ClipboardManager
        val clip: ClipData =
            ClipData.newPlainText("", setting_group_input_description.text.toString())
        clipboard.setPrimaryClip(clip)
        Toast.makeText(context, "Ссылка скопирована", Toast.LENGTH_SHORT).show()
    }
}



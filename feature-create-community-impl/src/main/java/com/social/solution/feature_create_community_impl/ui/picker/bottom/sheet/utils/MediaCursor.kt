package com.social.solution.feature_create_community_impl.ui.picker.bottom.sheet.utils

import android.content.Context
import android.database.Cursor
import android.net.Uri
import android.provider.MediaStore
import androidx.loader.content.CursorLoader
import com.github.vivchar.rendererrecyclerviewadapter.ViewModel
import com.social.solution.feature_create_community_impl.ui.picker.bottom.sheet.renderers.photo.CCommunityPhotoMPModel
import io.reactivex.Observable

fun getMediaCursor(context: Context?): Cursor? {
    context ?: return null

    // Get relevant columns for use later.
    val projection = arrayOf(
        MediaStore.Files.FileColumns._ID,
        MediaStore.Files.FileColumns.DATA,
        MediaStore.Files.FileColumns.DATE_ADDED,
        MediaStore.Files.FileColumns.MEDIA_TYPE,
        MediaStore.Files.FileColumns.MIME_TYPE,
        MediaStore.Files.FileColumns.TITLE
    )

    // Return only video and image metadata.
    val selection = (MediaStore.Files.FileColumns.MEDIA_TYPE + "="
            + MediaStore.Files.FileColumns.MEDIA_TYPE_IMAGE
            + " OR "
            + MediaStore.Files.FileColumns.MEDIA_TYPE + "="
            + MediaStore.Files.FileColumns.MEDIA_TYPE_VIDEO)

    val queryUri: Uri = MediaStore.Files.getContentUri("external")

    val cursorLoader = CursorLoader(
        context,
        queryUri,
        projection,
        selection,
        null,  // Selection args (none).
        MediaStore.Files.FileColumns.DATE_ADDED + " DESC" // Sort order.
    )

    return cursorLoader.loadInBackground()
}

enum class MediaType {
    VIDEO,
    PHOTO
}

data class MediaModel(val type: MediaType, val filePath: String) {
    fun convertToViewModel(context: Context?, position: Int): ViewModel {
        return CCommunityPhotoMPModel(filePath, position, null)
    }
}

fun Cursor.getMediaModels(): Observable<MediaModel> {
    val projectionId = arrayOf(
        getColumnIndex(MediaStore.Files.FileColumns._ID),
        getColumnIndex(MediaStore.Files.FileColumns.DATA),
        getColumnIndex(MediaStore.Files.FileColumns.DATE_ADDED),
        getColumnIndex(MediaStore.Files.FileColumns.MEDIA_TYPE),
        getColumnIndex(MediaStore.Files.FileColumns.MIME_TYPE),
        getColumnIndex(MediaStore.Files.FileColumns.TITLE)
    )

    moveToFirst()

    return Observable.create<MediaModel> {
        while (moveToNext()) {
            when (getInt(projectionId[3])) {
                MediaStore.Files.FileColumns.MEDIA_TYPE_IMAGE ->
                    it.onNext(MediaModel(MediaType.PHOTO, getString(projectionId[1])))
                MediaStore.Files.FileColumns.MEDIA_TYPE_VIDEO ->
                    it.onNext(MediaModel(MediaType.VIDEO, getString(projectionId[1])))
            }
        }
    }
}
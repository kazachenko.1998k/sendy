package com.social.solution.feature_create_community_impl.ui.picker.bottom.sheet

import android.net.Uri
import com.github.vivchar.rendererrecyclerviewadapter.ViewModel

interface CCommunityMediaPickerCallback {
    fun onSelected(media: List<ViewModel>, text: String?)
    fun onFileSelected(uri: Uri)
    fun onExit()
}
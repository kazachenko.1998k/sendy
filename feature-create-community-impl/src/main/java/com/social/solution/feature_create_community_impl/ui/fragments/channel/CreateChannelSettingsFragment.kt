package com.social.solution.feature_create_community_impl.ui.fragments.channel

import android.content.ClipData
import android.content.ClipboardManager
import android.content.Context
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.activity.OnBackPressedCallback
import androidx.core.content.ContextCompat
import androidx.core.widget.addTextChangedListener
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import com.morozov.core_backend_api.chat.Chat
import com.social.solution.feature_create_community_impl.MainObject
import com.social.solution.feature_create_community_impl.MainObject.TAG
import com.social.solution.feature_create_community_impl.R
import com.social.solution.feature_create_community_impl.utils.containsNotLinkSymbols
import com.social.solution.feature_create_community_impl.utils.hideKeyboard
import kotlinx.android.synthetic.main.setting_channel_fragment.*


class CreateChannelSettingsFragment(private val chat: Chat) : Fragment() {

    private lateinit var mCreateViewModel: CreateChannelViewModel

    companion object {
        const val NAME = "SETTING_CHANNEL_FRAGMENT"
        const val COUNT_SYMBOLS_IN_LINK = 30
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        val callback: OnBackPressedCallback = object : OnBackPressedCallback(true) {
            override fun handleOnBackPressed() {
                fragmentManager?.popBackStack()
            }
        }
        requireActivity().onBackPressedDispatcher.addCallback(
            this,
            callback
        )
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        setHasOptionsMenu(true)
        mCreateViewModel = ViewModelProviders.of(this).get(CreateChannelViewModel::class.java)
        return inflater.inflate(R.layout.setting_channel_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        this.initListeners()
        this.initData()
        this.initStateRadioButton()
        mCreateViewModel.setLink(chat.inviteHash.toString())
    }

    private fun initStateRadioButton() {
        if (chat.private == 3){
            radio_button_public_channel.isChecked = false
            radio_button_private_channel.isChecked = true
            public_link_layout.visibility = View.GONE
            private_link_layout.visibility = View.VISIBLE
        }else{
            radio_button_public_channel.isChecked = true
            radio_button_private_channel.isChecked = false
            public_link_layout.visibility = View.VISIBLE
            private_link_layout.visibility = View.GONE
        }
    }
    private fun initData() {
        setting_channel_private_link?.setText("https://sendy.ru/joinchat/${chat.inviteHash}")
    }


    private fun initListeners() {
        setting_channel_action_back?.setOnClickListener {
            fragmentManager?.popBackStack()
            MainObject.mCallbackSettingChannel?.openChannel(chat)
        }
        public_radio_layout?.setOnClickListener {
            radio_button_private_channel.isChecked = false
            radio_button_public_channel.isChecked = true
            public_link_layout.visibility = View.VISIBLE
            private_link_layout.visibility = View.GONE
        }
        private_radio_layout?.setOnClickListener {
            radio_button_public_channel.isChecked = false
            radio_button_private_channel.isChecked = true
            public_link_layout.visibility = View.GONE
            private_link_layout.visibility = View.VISIBLE
        }
        radio_button_public_channel?.setOnClickListener {
            radio_button_private_channel.isChecked = false
            radio_button_public_channel.isChecked = true
            public_link_layout.visibility = View.VISIBLE
            private_link_layout.visibility = View.GONE
        }
        radio_button_private_channel?.setOnClickListener {
            radio_button_public_channel.isChecked = false
            radio_button_private_channel.isChecked = true
            public_link_layout.visibility = View.GONE
            private_link_layout.visibility = View.VISIBLE
        }
        setting_channel_input_link?.addTextChangedListener {
            if (it!!.length > 5) {
                if (it.toString().containsNotLinkSymbols()) {
                    setting_channel_fragment_button_continue.isEnabled = false
                    setting_channel_input_link.error =
                        "Допустимы только символы A-z и цифры от 0..9"
                    Log.d(TAG, "CNLS false")
                } else {
                    mCreateViewModel.checkLinkUnique(it.toString(), MainObject.mCommonApi)
                        .subscribe({
                            setting_channel_fragment_button_continue.isEnabled = true
                            Log.d(TAG, "CL true")

                        }, { t: Throwable? ->
                            setting_channel_fragment_button_continue.isEnabled = false
                            setting_channel_input_link.error =
                                "Ссылка уже занята"

                            Log.d(TAG, "LOCK false")
                        })
                }
            } else {
                setting_channel_fragment_button_continue.isEnabled = false
                setting_channel_input_link.error =
                    "Ссылка содержит меньше 6 символов"
            }
        }
        setting_channel_fragment_button_continue?.setOnClickListener {
            if (radio_button_public_channel.isChecked && setting_channel_input_link.text!!.isEmpty()){
                setting_channel_fragment_button_continue.isEnabled = false
                setting_channel_input_link.error =
                    "Введите короткую ссылку. A-z, 0..9"
                return@setOnClickListener
            }
            mCreateViewModel.setSettings(
                radio_button_private_channel.isChecked,
                MainObject.mBackendApi!!.chatApi(),
                chat
            ).subscribe({ chatNew ->
                Toast.makeText(context, "Данные канала сохранены", Toast.LENGTH_SHORT).show()
                fragmentManager?.popBackStack()
                Log.d("TEST_UPDATE_GROUP", "private1 = " + chat.private)
                MainObject.mCallbackSettingChannel?.openChannel(chat)
            }, { t: Throwable? ->
                Log.e(TAG, "setSettings channel error")
                t?.printStackTrace()
            })
            hideKeyboard(context!!)
        }
        copy_link?.setOnClickListener {
            copyLink()
        }
        setting_channel_private_link?.setOnClickListener {
            copyLink()
        }
        share_link?.setOnClickListener {
            MainObject.mCallbackSettingChannel?.shareLink(setting_channel_input_link.text.toString())
        }
    }

    private fun copyLink() {
        val clipboard: ClipboardManager =
            context!!.getSystemService(Context.CLIPBOARD_SERVICE) as ClipboardManager
        val clip: ClipData =
            ClipData.newPlainText("", setting_channel_input_link.text.toString())
        clipboard.setPrimaryClip(clip)
        Toast.makeText(context, "Ссылка скопирована", Toast.LENGTH_SHORT).show()
    }
}



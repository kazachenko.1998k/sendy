package com.social.solution.feature_create_community_impl.ui.picker.bottom.sheet.media

interface CCommunityOnMediaClicked {
    fun onMediaClicked(position: Int)
    fun onUpdateItem(position: Int)
}
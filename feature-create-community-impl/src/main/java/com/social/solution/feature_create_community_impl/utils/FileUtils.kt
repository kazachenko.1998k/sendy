package com.social.solution.feature_create_community_impl.utils

import android.content.ContentResolver
import android.content.Context
import android.database.Cursor
import android.net.Uri
import android.provider.OpenableColumns
import android.webkit.MimeTypeMap

fun Uri.getName(context: Context?): String? {
    context ?: return null
    var fileName: String? = null
    val cursor = context.contentResolver
        .query(this, null, null, null, null, null)
    try {
        if (cursor != null && cursor.moveToFirst()) { // get file name
            fileName = cursor.getString(
                cursor.getColumnIndex(OpenableColumns.DISPLAY_NAME)
            )
        }
    } finally {
        cursor?.close()
    }
    return fileName
}

fun Uri.getSize(context: Context?): Int? {
    context ?: return null
    var fileSize: String? = null
    val cursor: Cursor? = context.contentResolver
        .query(this, null, null, null, null, null)
    cursor.use { cursor ->
        if (cursor != null && cursor.moveToFirst()) { // get file size
            val sizeIndex: Int = cursor.getColumnIndex(OpenableColumns.SIZE)
            if (!cursor.isNull(sizeIndex)) {
                fileSize = cursor.getString(sizeIndex)
            }
        }
    }
    return fileSize?.toInt()
}
package com.social.solution.feature_create_community_impl.ui.utility

import android.view.View

interface OnItemClickListener {

    fun onItemClicked(view: View, position: Int)
}
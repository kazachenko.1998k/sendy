package com.social.solution.feature_create_community_impl.ui.picker.bottom.sheet.interaction

interface MediaInnerAdapterCallback {
    fun onItemClicked(position: Int)
    fun showMedia(position: Int)
}
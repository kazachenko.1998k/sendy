package com.social.solution.feature_create_community_impl.ui.utility

interface OnBackPressedFragment {
    fun onBackPressed()
}
package com.social.solution.feature_create_community_impl.start

import androidx.fragment.app.FragmentManager
import androidx.lifecycle.MutableLiveData
import com.example.util_cache.chat.ChatDao
import com.morozov.core_backend_api.FeatureBackendApi
import com.morozov.core_backend_api.chat.Chat
import com.morozov.core_backend_api.chat.ChatApi
import com.morozov.core_backend_api.common.CommonApi
import com.social.solution.feature_create_community_api.*
import com.social.solution.feature_create_community_api.models.CreateCommunityAbstractItemRecycler
import com.social.solution.feature_create_community_impl.MainObject
import com.social.solution.feature_create_community_impl.ui.fragments.channel.CreateChannelFragment
import com.social.solution.feature_create_community_impl.ui.fragments.channel.CreateChannelSettingsFragment
import com.social.solution.feature_create_community_impl.ui.fragments.group.CreateGroupFragment
import com.social.solution.feature_create_community_impl.ui.fragments.group.CreateGroupSettingsFragment

class CreateCommunityStarterImpl : FeatureCreateGroupStarter {


    private var fragmentGroup: CreateGroupFragment? = null
    private var fragmentChannel: CreateChannelFragment? = null

    override fun startCreateGroup(
        manager: FragmentManager,
        parentContainer: Int,
        addToBackStack: Boolean,
        backendApi: FeatureBackendApi,
        callback: FeatureCreateGroupCallback,
        chatDao: ChatDao,
        uid: Long,
        networkState: MutableLiveData<Boolean>
    ) {
        MainObject.mCallbackGroup = callback
        MainObject.mChatDao = chatDao
        MainObject.mBackendApi = backendApi
        fragmentGroup = CreateGroupFragment(parentContainer, uid)
        manager.beginTransaction()
            .replace(parentContainer, fragmentGroup!!)
            .addToBackStack(CreateGroupFragment.NAME)
            .commit()
    }

    override fun startCreateChannel(
        manager: FragmentManager,
        parentContainer: Int,
        addToBackStack: Boolean,
        backendApi: FeatureBackendApi,
        callback: FeatureCreateChannelCallback,
        chatDao: ChatDao,
        uid: Long,
        networkState: MutableLiveData<Boolean>
    ) {
        MainObject.mCallbackChannel = callback
        MainObject.mChatDao = chatDao
        MainObject.mBackendApi = backendApi
        fragmentChannel = CreateChannelFragment(parentContainer, uid)
        manager.beginTransaction()
            .replace(parentContainer, fragmentChannel!!)
            .addToBackStack(CreateChannelFragment.NAME)
            .commit()
    }

    override fun startSettingGroup(
        manager: FragmentManager,
        parentContainer: Int,
        addToBackStack: Boolean,
        chat: Chat,
        backendApi: FeatureBackendApi,
        backendCommonApi: CommonApi,
        callback: FeatureSettingGroupCallback
    ) {
        MainObject.mCallbackSettingGroup = callback
        MainObject.mCommonApi = backendCommonApi
        MainObject.mBackendApi = backendApi
        manager.beginTransaction()
            .replace(
                parentContainer,
                CreateGroupSettingsFragment(chat)
            )
            .addToBackStack(CreateGroupSettingsFragment.NAME)
            .commit()
    }

    override fun startSettingChannel(
        manager: FragmentManager,
        parentContainer: Int,
        addToBackStack: Boolean,
        chat: Chat,
        backendApi: FeatureBackendApi,
        backendCommonApi: CommonApi,
        callback: FeatureSettingChannelCallback
    ) {
        MainObject.mCallbackSettingChannel = callback
        MainObject.mCommonApi = backendCommonApi
        MainObject.mBackendApi = backendApi
        manager.beginTransaction()
            .replace(
                parentContainer,
                CreateChannelSettingsFragment(chat)
            )
            .addToBackStack(CreateChannelSettingsFragment.NAME)
            .commit()
    }

    override fun selectUsersGroup(selectedUserForCreatingChat: MutableList<CreateCommunityAbstractItemRecycler>) {
        fragmentGroup?.addUsers(selectedUserForCreatingChat)
    }

    override fun selectUsersChannel(selectedUserForCreatingChat: MutableList<CreateCommunityAbstractItemRecycler>) {
        fragmentChannel?.addUsers(selectedUserForCreatingChat)
    }
}